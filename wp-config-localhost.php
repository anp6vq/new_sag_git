<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'saglass');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '!->8B42%b8`O+U`zSE&iV[CghAV;Rr)-n8XmaY[&FP8VDM2vdmx6tB{pVA(|F&K^');
define('SECURE_AUTH_KEY',  'Au@1`yznfc2a-Kdh:Ad8u-nyACcrcy,<.Mj(,1>H[PTEB/M?8Z3f2o@v/X+b KkW');
define('LOGGED_IN_KEY',    '?|=oKDTu*Gz>TyM+jBTq+mLh+(9!0|#j~k:%(F<;-%~btTemBzi4=tlvhts/|z2 ');
define('NONCE_KEY',        'N!!/(%J 0FChwm[$*^O[tdq*9:6)K_^%W6xwIbn Nr+g+xX^k:T&i%z0BmhXtxda');
define('AUTH_SALT',        'GU#5)u@w-K#fr^Pv|0v:ByP`AKn-70#?ls`P`=:RF`(ZVCSz eGVo-1&ZZ&:^i&%');
define('SECURE_AUTH_SALT', 'KZVvh&k_+N-h)!z{0Mj))Re+.-Ig1^6h8+tJ_8||k@W4[z]|XV, E`JoW2|sdVA:');
define('LOGGED_IN_SALT',   'L-r@pc zry$nQ_5>OVdK~No348[H.o4|L.pm4~aMrGbr*]&nhFiC.wII >}lmnrR');
define('NONCE_SALT',       'wEf{Kl@8&LgH,Yu{OL^i-.qmV|D{iqU--PqW3XR!>*aM%uK~vb?(!}NmZJCv(h[-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_23';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
