<div class="form-horizontal">
   <div class="form-group">
      <div class="col-xs-12">
         <div class="input-group">
            <div class="input-group-btn">
               <button id="btn_twsd_first" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_previous" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
               </button>
            </div>
            <input id="text_twsd_item_count" type="text" class="form-control text-center" placeholder="0/0" value="0/0">
            <div class="input-group-btn">
               <button id="btn_twsd_next" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_last" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_add" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_save" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_delete" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_refresh" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_copy" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
               </button>
               <button id="btn_twsd_paste" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-paste" aria-hidden="true"></span>
               </button>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="radio_twsd_screen_type">Screen Type</label>
         <ul class="list-group">
            <li class="list-group-item">
               <div class="radio" style="text-align:left">
                  <label>
                     <input type="radio" name="radio_twsd_screen_type" id="radio_twsd_screen_type_not_a_value" value="not_a_value"> No answer
                  </label>
               </div>
            </li>
            <li class="list-group-item">
               <div class="radio" style="text-align:left">
                  <label>
                     <input type="radio" name="radio_twsd_screen_type" id="radio_twsd_screen_type_window_screen" value="window_screen"> Window screen
                  </label>
               </div>
            </li>
            <li class="list-group-item">
               <div class="radio" style="text-align:left">
                  <label>
                     <input type="radio" name="radio_twsd_screen_type" id="radio_twsd_screen_type_casement_screen" value="casement_screen"> Casement screen
                  </label>
               </div>
            </li>
            <li class="list-group-item">
               <div class="radio" style="text-align:left">
                  <label>
                     <input type="radio" name="radio_twsd_screen_type" id="radio_twsd_screen_type_patio_screen_door" value="patio_screen_door"> Patio screen door
                  </label>
               </div>
            </li>
         </ul>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 col-xs-12">
         <label for="text_twsd_exact_width">Exact width</label>
         <div class="input-group">
            <input id="text_twsd_exact_width" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-sm-6 col-xs-12">
         <label for="text_twsd_exact_height">Exact height</label>
         <div class="input-group">
            <input id="text_twsd_exact_height" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 col-xs-12">
         <label for="select_twsd_frame_thickness">Frame thickness</label>
         <select id="select_twsd_frame_thickness" class="form-control">
            <option value="not_a_value" disabled selected>Please select frame thickness</option>
            <option value="1_over_4">1/4</option>
            <option value="5_over_16">5/16</option>
            <option value="3_over_8">3/8</option>
            <option value="7_over_16">7/16</option>
            <option value="7_over_16_sd">7/16 SD</option>
         </select>
      </div>
      <div class="col-sm-6 col-xs-12">
         <label for="select_twsd_frame_finish">Frame finish</label>
         <select id="select_twsd_frame_finish" class="form-control">
            <option value="not_a_value" disabled selected>Please select frame finish</option>
            <option value="mill">Mill</option>
            <option value="white">White</option>
            <option value="bronze">Bronze</option>
         </select>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 col-xs-12">
         <label for="select_twsd_type_of_mesh">Type of mesh</label>
         <select id="select_twsd_type_of_mesh" class="form-control">
            <option value="not_a_value" disabled selected>Please select type of mesh</option>
            <option value="aluminium">Aluminium</option>
            <option value="fiberglass">Fiberglass</option>
         </select>
      </div>
      <div class="col-sm-6 col-xs-12">
         <label for="select_twsd_color_of_mesh">Color of mesh</label>
         <select id="select_twsd_color_of_mesh" class="form-control">
            <option value="not_a_value" disabled selected>Please select color of mesh</option>
            <option value="brite_kote">Brite-Kote</option>
            <option value="charcoal">Charcoal</option>
            <option value="silver_gray">Silver-Gray</option>
         </select>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-4 col-xs-12">
         <label for="text_twsd_hardware_type_line1">Hardware type</label>
         <input id="text_twsd_hardware_type_line1" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_quantity_or_side_line1">Quantity/Side</label>
         <input id="text_twsd_quantity_or_side_line1" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_number_of_sides_line1">Number of sides</label>
         <input id="text_twsd_number_of_sides_line1" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_frame_side_line1">Frame side</label>
         <input id="text_twsd_frame_side_line1" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_distance_from_end_line1">Distance from end</label>
         <input id="text_twsd_distance_from_end_line1" type="text" class="form-control" placeholder="">
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-4 col-xs-12">
         <label for="text_twsd_hardware_type_line2">Hardware type</label>
         <input id="text_twsd_hardware_type_line2" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_quantity_or_side_line2">Quantity/Side</label>
         <input id="text_twsd_quantity_or_side_line2" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_number_of_sides_line2">Number of sides</label>
         <input id="text_twsd_number_of_sides_line2" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_frame_side_line2">Frame side</label>
         <input id="text_twsd_frame_side_line2" type="text" class="form-control" placeholder="">
      </div>
      <div class="col-sm-2 col-xs-12">
         <label for="text_twsd_distance_from_end_line2">Distance from end</label>
         <input id="text_twsd_distance_from_end_line2" type="text" class="form-control" placeholder="">
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="textarea_twsd_comments">Comments</label>
         <textarea id="textarea_twsd_comments" class="form-control" rows="3"></textarea>
      </div>
   </div>
   <div class="row form-group">
     <div class="col-md-12">
       <h5 class="col-header">Hardware Available No Extra Charge</h5>
     </div>
     <div class="col-md-3">
       VL-Vinyl Lift
     </div>
     <div class="col-md-3">
       WLL-Wire Loop Latch
     </div>
     <div class="col-md-3">
       FTS-Flat Tension Spring
     </div>
     <div class="col-md-3">
       WTS-Wire Tension Spring
     </div>
     <div class="col-md-3">
       FH-Friction Hanger for 7/16 Frame
     </div>
     <div class="col-md-3">
       DS-Ball Head Drive Screw
     </div>
     <div class="col-md-3">
       KLL-Knife Latch Left Hand
     </div>
     <div class="col-md-3">
       KLL-Knife Latch Right Hand
     </div>
     <div class="col-md-3">
       NSP-Nylon Spring Plunger
     </div>
     <div class="col-md-3">
       CS-10-24 x 5/8" C Clip
     </div>
     <div class="col-md-3">
       SL-Slide Latches
     </div>
     <div class="col-md-3">
       CC-C Clip
     </div>
     <div class="col-md-3">
       NPP-Nylon Pin Plunger
     </div>
     <div class="col-md-3">
       LPP-Large Metal Pin Plunger
     </div>
     <div class="col-md-3">
       SPP-Small Metal Pin Plunger
     </div>
   </div>
   <div class="row form-group">
     <div class="col-md-12">
       <h5 class="col-header">Hardware Available Extra Charge</h5>
     </div>
     <div class="col-md-3">
       SW-Swival Latch w/ Bushing
     </div>
     <div class="col-md-3">
       EX-Expander
     </div>
     <div class="col-md-3">
       CB-Center Bar
     </div>
     <div class="col-md-3">
       TH-Top Hanger
     </div>
     <div class="col-md-3">
       SN-Snugger
     </div>
     <div class="col-md-3">
       WW-Wide Wheel
     </div>
     <div class="col-md-3">
       ST-Sponge Tape
     </div>
     <div class="col-md-3">
       DR-Diagonal Routes
     </div>
     <div class="col-md-3">
       PL-Pointer Latch
     </div>
   </div>
   <div class="row form-group">
     <div class="col-md-12">
       <h5 class="col-header">Standard Defaults</h5>
     </div>
     <div class="col-md-3">
       ??
     </div>
     <div class="col-md-3">
     </div>
     <div class="col-md-3">
     </div>
     <div class="col-md-3">
     </div>
     <div class="col-md-3">
     </div>
     <div class="col-md-3">
     </div>
     <div class="col-md-3">
     </div>
     <div class="col-md-3">
     </div>
   </div>
</div>
