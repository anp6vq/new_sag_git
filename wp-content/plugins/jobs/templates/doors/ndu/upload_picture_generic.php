<div class="row">
   <div class="col-xs-12">
      <div id="div_upload_picture_generic">
         <div class="col-xs-10 col-xs-offset-1 col-sm-12 col-sm-offset-0 col-md-6 col-md-offset-0">
            <label class="leftalign" for="uploadFile">Select file(s) to upload</label>
            <div class="row">
               <div class="col-xs-7 col-sm-9">
                  <input placeholder="Choose File" disabled="disabled" class="form-control" />
               </div>
               <div class="col-xs-5 col-sm-3">
                  <div class="fileUpload btn btn-primary">
                     <span>Choose file(s)</span>
                     <input id="file_nwdr_glass_picture" type="file" multiple class="upload" accept="image/*" />
                  </div>
               </div>
            </div>
            <div class="row">
               <div class="col-xs-12">
                  <div id="div_nwdr_picture_filelist_holder">
                     <div id="div_nwdr_picture_filelist"></div>
                     <div id="div_nwdr_picture_filelist_controls">
                        <div class="btn-group" role="group" aria-label="Picture File List Toolbar">
                           <button id="btn_nwdr_clear_picture_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Pictures">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                           </button>
                           <button id="btn_nwdr_upload_picture_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Pictures">
                              <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>