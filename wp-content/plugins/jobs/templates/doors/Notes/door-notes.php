<div class="row">
<div id="div_upload_picture_generic">
  <div class="col-xs-10 col-sm-8 col-md-8 col-lg-6">
    <label class="leftalign" for="uploadFile">Select file(s) to upload</label>
    <div class="row">
      <div class="col-xs-7 col-sm-9">
        <input placeholder="Choose File" disabled="disabled" class="form-control" />
      </div>
      <div class="col-xs-5 col-sm-3">
        <div class="fileUpload btn btn-primary"> <span>Choose file(s)</span>
          <input id="file_tgpd_notes_picture" type="file" multiple class="upload" accept="image/*" />
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12">
        <div id="div_upg_picture_filelist_holder">
          <div id="div_upg_picture_filelist_notes"></div>
          <div id="div_upg_picture_filelist_controls_notes">
            <div class="btn-group" role="group" aria-label="Picture File List Toolbar">
              <button id="btn_upg_clear_picture_filelist_note" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Pictures"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </button>
              <button id="btn_upg_upload_picture_filelist_notes" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Pictures"> <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span> </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div id="div_upload_sketch_generic_03">
  <div class="col-xs-12">
    <label class="leftalign">Click below for sketch canvas</label>
    <div id="usg-canvas-wrap">
      <h5 id="h5notes">Sketch Diagram or Fabrication.</h5>
      <div class="row">
        <div class="col-xs-12">
          <div id="div_usg_canvas_holder_notes">
            <div id="div_usg_canvas_controls_notes">
              <div class="btn-group" role="group" aria-label="Sketch Toolbar"> <a id="btn_usg_canvas_eraser_tool_notes" class="btn btn-default" href="#canvas_usg_glass_sketch_notes" role="button" data-tool="eraser" title="Eraser Tool"> <span class="glyphicon glyphicon-erase" aria-hidden="true"></span> </a> <a id="btn_usg_canvas_marker_tool" class="btn btn-default" href="#canvas_usg_glass_sketch_notes" role="button" data-tool="marker" title="Marker Tool"> <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span> </a>
                <button id="btn_usg_clear_canvas_notes" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Canvas"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </button>
                <button id="btn_usg_canvas_to_sketch_filelist_notes" type="button" class="btn btn-default" aria-label="Left Align" title="Canvas To Sketch File List"> <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span> </button>
              </div>
              <div class="btn-group" role="group" aria-label="Canvas Toolbar">
                <button id="btn_usg_canvas_marker_size_notes" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Size"> <span class="glyphicon glyphicon-text-size" aria-hidden="true"></span> <span class="caret"></span> </button>
                <ul id="list_usg_canvas_marker_size_notes" class="dropdown-menu" aria-labelledby="btn_usg_canvas_marker_size">
                  <li><a id="btn_usg_canvas_marker_size_3" href="#canvas_usg_glass_sketch_notes" data-size="3">3 points</a></li>
                  <li><a id="btn_usg_canvas_marker_size_5" href="#canvas_usg_glass_sketch_notes" data-size="5">5 points</a></li>
                  <li><a id="btn_usg_canvas_marker_size_10" href="#canvas_usg_glass_sketch_notes" data-size="10">10 points</a></li>
                  <li><a id="btn_usg_canvas_marker_size_15" href="#canvas_usg_glass_sketch_notes" data-size="15">15 points</a></li>
                </ul>
              </div>
              <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                <button id="btn_usg_canvas_marker_color" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Color"> <span class="glyphicon glyphicon-text-color" aria-hidden="true"></span> <span class="caret"></span> </button>
                <ul id="list_usg_canvas_marker_color" class="dropdown-menu" aria-labelledby="btn_usg_canvas_marker_color">
                  <li><a href="#canvas_usg_glass_sketch_notes" data-color="#000" style="width:100%;height:30px;background:#000;"></a></li>
                  <li><a href="#canvas_usg_glass_sketch_notes" data-color="#555" style="width:100%;height:30px;background:#555;"></a></li>
                  <li><a href="#canvas_usg_glass_sketch_notes" data-color="#f00" style="width:100%;height:30px;background:#f00;"></a></li>
                  <li><a href="#canvas_usg_glass_sketch_notes" data-color="#ff0" style="width:100%;height:30px;background:#ff0;"></a></li>
                  <li><a href="#canvas_usg_glass_sketch_notes" data-color="#CCC" style="width:100%;height:30px;background:#CCC;"></a></li>
                  <li><a href="#canvas_usg_glass_sketch_notes" data-color="#fff" style="width:100%;height:30px;background:#fff;"></a></li>
                </ul>
              </div>
            </div>
            <canvas id="canvas_usg_glass_sketch_notes" width="800" height="300"></canvas>
          </div>
        </div>
      </div>
      <!--perhaps row here...with sketch images ready for upload so they show and hide with canvas-->
    </div>
  </div>
</div>
</div>
<div class="row">
<div id="div_upload_sketch_generic_02">
  <div class="col-xs-12">
    <div id="div_usg_sketch_filelist_holder">
      <div id="div_usg_sketch_filelist_notes"></div>
      <div id="div_usg_sketch_filelist_controls_notes">
        <div class="btn-group" role="group" aria-label="Sketch File List Toolbar">
          <button id="btn_usg_clear_sketch_filelist_notes" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Sketches"> <span class="glyphicon glyphicon-remove" aria-hidden="true"></span> </button>
          <button id="btn_usg_upload_sketch_filelist_notes" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Sketches"> <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span> </button>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
<div class="row">
<div id="div_upload_sketch_generic_02">
  <div class="col-xs-10 col-sm-8 col-md-8 col-lg-6">
    <div class="row">
      <div class="form-group">
        <div class="col-xs-12">
          <label class="leftalign" for="textarea_tgpd_notes">General Notes</label>
          <textarea id="textarea_tgpd_notes" name='' class="form-control" rows="3" autocomplete="on" placeholder="General Notes"></textarea>
        </div>
      </div>
    </div>
  </div>
</div>
</div>
