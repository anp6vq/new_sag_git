<div id="sfd-<?php echo basename(__FILE__, '.php'); ?>-container" class="container-fluid door-container hidden sfd-type_offset_pivots_single_pair-container">
  <div class="form-group door-form-group">
    <div class="col-sm-5 door-diagram-col">
      <div class="door-diagram-img-wrapper">
        <img src="<?php echo plugins_url(); ?>/jobs/img/doors/repair/aluminum_storefront/SfDoor---Center-Hung-Pair.jpg" class="door-diagram-img" />
      </div>
      <div class="col-sm-12">
         <label class="leftalign" for="textarea_tagd_sfd_offset_pivots_notes">Instructions</label>
         <textarea id="textarea_tagd_sfd_offset_pivots_notes" maxlength="300" class="form-control" rows="3" autocomplete="on" placeholder="Instructions"></textarea>
      </div>
    </div>
    <div class="col-sm-7 door-fields-col">
      <div class="fields-sub-form-group form-group row">
        <div class="col-sm-6 fields-left-half">
		
		<div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <label>Location</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
            <input id="select_tagd_sfd_offset_pivots_location" class="form-control" type="text">
              
          </div>
		  
          <div class="field-wrapper">
            <div class="row door-field-status-row">
          
		  <div class="col-xs-6">
<label>Width</label>
<input type="text" placeholder="Width" class="form-control user-success" id="text_tagd_sfd_offset_pivots_width" />
</div>
<div class="col-xs-2"></div>
<div class="col-xs-6">
<label>Height</label>
<input type="text" class="form-control user-success"  placeholder="Height" id="text_tagd_sfd_offset_pivots_height" />
</div>
		  </div></div>
          
		  
		  
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">A</span><label>Brand</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_brand" class="form-control">
              <option value="not_applicable"  selected>Select Brand</option>
              <option value="Vistawall">Vistawall</option>
              <option value="Kawneer">Kawneer</option>
              <option value="YKK">YKK</option>
              <option value="US Aluminum">US Aluminum</option>
              <option value="EFCO">EFCO</option>
              <option value="Other">Other</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">B</span><label>COC Closer</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_coc_closer_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_coc_closer" class="form-control">
              <option value="not_applicable"  selected>Select COC Closer</option>
              <option value="Rixson 800 90 NHO">Rixson 800 90 NHO</option>
              <option value="Rixson 800 105 NHO">Rixson 800 105 NHO</option>
              <option value="Rixson 800 90 HO COC Closer">Rixson 800 90 HO COC Closer</option>
              <option value="Rixson 800 105 HO COC Closer">Rixson 800 105 HO COC Closer</option>
              <option value="Dorma RTS88 105 NHO SZ3">Dorma RTS88 105 NHO SZ3</option>
              <option value="Dorma RTS88 90 NHO SZ3">Dorma RTS88 90 NHO SZ3</option>
              <option value="Dorma RTS88 105 HO SZ3">Dorma RTS88 105 HO SZ3</option>
              <option value="Dorma RTS88 90 HO SZ3">Dorma RTS88 90 HO SZ3</option>
              <option value="Dorma RTS88 105 NHO SZ3 5MM (extended Spindle)">Dorma RTS88 105 NHO SZ3 5MM (extended Spindle)</option>
              <option value="Dorma RTS88 90 NHO SZ3 5MM (extended spindle)">Dorma RTS88 90 NHO SZ3 5MM (extended spindle)</option>
              <option value="Dorma RTS88 105 HO SZ3 5MM (extended spindle)">Dorma RTS88 105 HO SZ3 5MM (extended spindle)</option>
              <option value="Dorma RTS88 90 HO SZ3 5MM (extended spindle)">Dorma RTS88 90 HO SZ3 5MM (extended spindle)</option>
              <option value="Dorma RTS88 105 NHO BFI (Interior) ">Dorma RTS88 105 NHO BFI (Interior) </option>  
              <option value="Dorma RTS88 105 HO BFI (Interior) ">Dorma RTS88 105 HO BFI (Interior) </option> 
              <option value="Dorma RTS88 105 NHO BFE (Exterior) ">Dorma RTS88 105 NHO BFE (Exterior) </option> 
              <option value="Dorma RTS88 105 HO BFE (Exterior) ">Dorma RTS88 105 HO BFE (Exterior) </option> 
              <option value="Dorma RTS88 105 NHO BFI (Interior) (Extended Spindle)">Dorma RTS88 105 NHO BFI (Interior) (Extended Spindle)</option>  
              <option value="Dorma RTS88 105 HO BFI (Interior) (Extended Spindle)">Dorma RTS88 105 HO BFI (Interior) (Extended Spindle)</option> 
              <option value="Dorma RTS88 105 NHO BFE (Exterior) (Extended Spindle)">Dorma RTS88 105 NHO BFE (Exterior) (Extended Spindle)</option> 
              <option value="Dorma RTS88 105 HO BFE (Exterior) (Extended Spindle) ">Dorma RTS88 105 HO BFE (Exterior) (Extended Spindle) </option> 

              <option value="Jackson 105 NHO">Jackson 105 NHO</option>
              <option value="Jackson 90 NHO">Jackson 90 NHO</option>
              <option value="Jackson 105 HO">Jackson 105 HO</option>
              <option value="Jackson 90 HO">Jackson 90 HO</option>
              <option value="Jackson 105 NHO Extended Spindle">Jackson 105 NHO Extended Spindle</option>
              <option value="Jackson 90 NHO Extended Spindle">Jackson 90 NHO Extended Spindle</option>
              <option value="Jackson 105 HO Extended Spindle">Jackson 105 HO Extended Spindle</option>
              <option value="Jackson 90 HO Extended Spindle">Jackson 90 HO Extended Spindle</option>
              <option value="Jackson 105 NHO 3 Valve with Back Check">Jackson 105 NHO 3 Valve with Back Check</option>
              <option value="Jackson 90 NHO 3 Valve with Back Check">Jackson 90 NHO 3 Valve with Back Check</option>
              <option value='Jackson Retro Fit Kit for 4" frame (Old Husky)'>Jackson Retro Fit Kit for 4" frame (Old Husky)</option>
              <option value='Jackson Retro Fit Kit for 4 1/2" frame (Old Husky)'>Jackson Retro Fit Kit for 4 1/2" frame (Old Husky)</option>
              <option value="International 230 105 HO">International 230 105 HO</option>  
              <option value="International 231 105 NHO">International 231 105 NHO</option> 
              <option value="International 232 90 HO">International 232 90 HO</option> 
              <option value="International 233 90 HO">International 233 90 HO</option>
              <option value="Sentinel 3031 105 NHO">Sentinel 3031 105 NHO</option>
              <option value="Sentinel 3030 105 HO">Sentinel 3030 105 HO</option>
              <option value="Sentinel 3033 90 NHO">Sentinel 3033 90 NHO</option>
              <option value="Sentinel 3032 90 HO">Sentinel 3032 90 HO</option>
              <option value="Global TC7031 105 NHO">Global TC7031 105 NHO</option>
              <option value="Global TC7030 105 HO">Global TC7030 105 HO</option>
              <option value="Global TC7033 90 NHO">Global TC7033 90 NHO</option>
              <option value="Global TC7032 90 HO">Global TC7032 90 HO</option>
              <option value="LCN 2030 Series R/H Aluminum">LCN 2030 Series R/H Aluminum</option>
              <option value="LCN 2030 Series R/H Bronze">LCN 2030 Series R/H Bronze</option>
              <option value="LCN 2030 Series L/H Aluminum">LCN 2030 Series L/H Aluminum</option>
              <option value="LCN 2030 Series L/H Bronze">LCN 2030 Series L/H Bronze</option>
              <option value="LCN 2010 Series R/H Aluminum">LCN 2010 Series R/H Aluminum</option>
              <option value="LCN 2010 Series R/H Bronze">LCN 2010 Series R/H Bronze</option>
              <option value="LCN 2010 Series L/H Aluminum">LCN 2010 Series L/H Aluminum</option>
              <option value="LCN 2010 Series L/H Bronze">LCN 2010 Series L/H Bronze</option>
              <option value="LCN 3031 Series R/H Aluminum">LCN 3031 Series R/H Aluminum</option>
              <option value="LCN 3031 Series R/H Bronze">LCN 3031 Series R/H Bronze</option>
              <option value="LCN 3031 Series L/H Aluminum">LCN 3031 Series L/H Aluminum</option>
              <option value="LCN 3031 Series L/H Bronze">LCN 3031 Series L/H Bronze</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">D</span><label>Surface Mounted Closer</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_surface_mounted_closer_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_surface_mounted_closer" class="form-control">
              <option value="not_applicable"  selected>Select Mounted Closer</option>
              <option value='Norton 1601 Surface Closer Bronze'>Norton 1601 Surface Closer Bronze</option>
              <option value='Norton 1601 Surface Closer Aluminum'>Norton 1601 Surface Closer Aluminum</option>
              <option value='Norton Hold Open Arm for 1601'>Norton Hold Open Arm for 1601</option>
              <option value='LCN 4040XP Surface Closer Bronze'>LCN 4040XP Surface Closer Bronze</option>
              <option value='LCN 4040XP Surface Closer Aluminum'>LCN 4040XP Surface Closer Aluminum</option>
              <option value='LCN Hold Open Arm For 4040XP'>LCN Hold Open Arm For 4040XP</option>
              <option value='Dorma 7414 Surface Closer Bronze'>Dorma 7414 Surface Closer Bronze</option>
              <option value='Dorma 7414 Surface Closer Aluminum'>Dorma 7414 Surface Closer Aluminum</option>
              <option value='Dorma 8616 Surface Closer Bronze'>Dorma 8616 Surface Closer Bronze</option>
              <option value='Dorma 8616 Surface Closer Aluminum'>Dorma 8616 Surface Closer Aluminum</option>
              <option value='Dorma FH-Friction Hold Open Arm Bronze'>Dorma FH-Friction Hold Open Arm Bronze</option>
              <option value='Dorma FH-Friction Hold Open Arm Aluminum'>Dorma FH-Friction Hold Open Arm Aluminum</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">E</span><label>Offset Arm</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_offset_arm_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_offset_arm" class="form-control">
              <option value="not_applicable"  selected>Select Offset Arm</option>
              <option value='Dorma Offset Top Arm'>Dorma Offset Top Arm</option>
              <option value='Jackson Offset Arm 20-1312'>Jackson Offset Arm 20-1312</option>
              <option value='Jackson Offset Arm 20-900'>Jackson Offset Arm 20-900</option>
              <option value='Husky Offset Arm'>Husky Offset Arm</option>
              <option value='Sam 2 kit Offset Arm'>Sam 2 kit Offset Arm</option>
			  <option value='Rixson 27 Bottom Arm'>Rixson 27 Bottom Arm</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">F</span><label>Drop Plate</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_drop_plate_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_drop_plate" class="form-control">
              <option value="not_applicable"  selected>Select Drop Plate</option>
              <option value='Norton Drop Plate for Header Bronze'>Norton Drop Plate for Header Bronze</option>
              <option value='Norton Drop Plate for Header Aluminum'>Norton Drop Plate for Header Aluminum</option>
              <option value='Norton Drop Plate for Door Bronze'>Norton Drop Plate for Door Bronze</option>
              <option value='Norton Drop Plate for Door Aluminum'>Norton Drop Plate for Door Aluminum</option>
              <option value='LCN Drop Plate For Header Bronze'>LCN Drop Plate For Header Bronze</option>
              <option value='LCN Drop Plate For Header Aluminum'>LCN Drop Plate For Header Aluminum</option>
              <option value='LCN Drop Plate For Door Bronze'>LCN Drop Plate For Door Bronze</option>
              <option value='LCN Drop Plate For Door Aluminum'>LCN Drop Plate For Door Aluminum</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">G</span><label>Type of Glass</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_type_of_glass_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_type_of_glass" class="form-control">
              <option value="not_applicable"  selected>Select Glass Type</option>
              <option value='1/4" Clear Laminated'>1/4" Clear Laminated</option>
              <option value='1/4" Clear Tempered'>1/4" Clear Tempered</option>
              <option value='1/4" Bronze Laminated'>1/4" Bronze Laminated</option>
              <option value='1/4" Bronze Tempered'>1/4" Bronze Tempered</option>
              <option value='1/4" Grey Laminated'>1/4" Grey Laminated</option>
              <option value='1/4" Grey Tempered'>1/4" Grey Tempered</option>
              <option value='3/8" Clear Laminated'>3/8" Clear Laminated</option>
              <option value='3/8" Clear Tempered'>3/8" Clear Tempered</option>
              <option value='3/8" Bronze Laminated'>3/8" Bronze Laminated</option>
              <option value='3/8" Bronze Tempered'>3/8" Bronze Tempered</option>
              <option value='3/8" Grey Laminated'>3/8" Grey Laminated</option>
              <option value='3/8" Grey Tempered'>3/8" Grey Tempered</option>
              <option value='1/2" Clear Laminated'>1/2" Clear Laminated</option>
              <option value='1/2" Clear Tempered'>1/2" Clear Tempered</option>
              <option value='1/2" Bronze Laminated'>1/2" Bronze Laminated</option>
              <option value='1/2" Bronze Tempered'>1/2" Bronze Tempered</option>
              <option value='1/2" Grey Laminated'>1/2" Grey Laminated</option>
              <option value='1/2" Grey Tempered'>1/2" Grey Tempered</option>
              <option value='1" IG Clear Tempered 1/4" lites'>1" IG Clear Tempered 1/4" lites</option>
              <option value='1" IG Clear Tempered 3/16" lites'>1" IG Clear Tempered 3/16" lites</option>
              <option value='1" IG Bronze Tempered 1/4" lites'>1" IG Bronze Tempered 1/4" lites</option>
              <option value='1" IG Bronze Tempered 3/16" lites'>1" IG Bronze Tempered 3/16" lites</option>
              <option value='1" IG Grey Tempered 1/4" lites'>1" IG Grey Tempered 1/4" lites</option>
              <option value='1" IG Grey Tempered 3/16" lites'>1" IG Grey Tempered 3/16" lites</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">H</span><label>Handles</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_handles_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_handles" class="form-control">
              <option value="not_applicable"  selected>Select Handles</option>
              <option value='Standard Push Pull with Pull 8" CTC Aluminum Finish'>Standard Push Pull with Pull 8" CTC Aluminum Finish</option>
              <option value='Standard Push Pull with Pull 9" CTC Aluminum Finish'>Standard Push Pull with Pull 9" CTC Aluminum Finish</option>
              <option value='Standard Push Pull with Pull 12" CTC Aluminum Finish'>Standard Push Pull with Pull 12" CTC Aluminum Finish</option>
              <option value='Standard Push Pull with Pull 8" CTC Bronze Finish'>Standard Push Pull with Pull 8" CTC Bronze Finish</option>
              <option value='Standard Push Pull with Pull 9" CTC Bronze Finish'>Standard Push Pull with Pull 9" CTC Bronze Finish</option>
              <option value='Standard Push Pull with Pull 12" CTC Bronze Finish'>Standard Push Pull with Pull 12" CTC Bronze Finish</option>
              <option value='Standard Push Pull with Pull 8" CTC Black Finish'>Standard Push Pull with Pull 8" CTC Black Finish</option>
              <option value='Standard Push Pull with Pull 9" CTC Black Finish'>Standard Push Pull with Pull 9" CTC Black Finish</option>
              <option value='Standard Push Pull with Pull 12" CTC Black Finish'>Standard Push Pull with Pull 12" CTC Black Finish</option>
              <option value='Back to back Pulls 8" CTC Aluminum Finish'>Back to back Pulls 8" CTC Aluminum Finish</option>
              <option value='Back to Back Pulls 8" CTC Bronze Finish'>Back to Back Pulls 8" CTC Bronze Finish</option>
              <option value='Back to Back Pulls 8" CTC Black Finish'>Back to Back Pulls 8" CTC Black Finish</option>
              <option value='Back to back Pulls 9" CTC Aluminum Finish'>Back to back Pulls 9" CTC Aluminum Finish</option>
              <option value='Back to Back Pulls 9" CTC Bronze Finish'>Back to Back Pulls 9" CTC Bronze Finish</option>
              <option value='Back to Back Pulls 9" CTC Black Finish'>Back to Back Pulls 9" CTC Black Finish</option>
              <option value='Back to back Pulls 12" CTC Aluminum Finish'>Back to back Pulls 12" CTC Aluminum Finish</option>
              <option value='Back to Back Pulls 12" CTC Bronze Finish'>Back to Back Pulls 12" CTC Bronze Finish</option>
              <option value='Back to Back Pulls 12" CTC Black Finish'>Back to Back Pulls 12" CTC Black Finish</option>
              <option value='Pull Only 8" CTC Aluminum Finish'>Pull Only 8" CTC Aluminum Finish</option>
              <option value='Pull Only 8" CTC Bronze Finish'>Pull Only 8" CTC Bronze Finish</option>
              <option value='Pull Only 8" CTC Black Finish'>Pull Only 8" CTC Black Finish</option>
              <option value='Pull Only 9" CTC Aluminum Finish'>Pull Only 9" CTC Aluminum Finish</option>
              <option value='Pull Only 9" CTC Bronze Finish'>Pull Only 9" CTC Bronze Finish</option>
              <option value='Pull Only 9" CTC Black Finish'>Pull Only 9" CTC Black Finish</option>
              <option value='Pull Only 12" CTC Aluminum Finish'>Pull Only 12" CTC Aluminum Finish</option>
              <option value='Pull Only 12" CTC Bronze Finish'>Pull Only 12" CTC Bronze Finish</option>
              <option value='Pull Only 12" CTC Black Finish'>Pull Only 12" CTC Black Finish</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">I</span><label>Panic Device</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_panic_device_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_panic_device" class="form-control">
              <option value="not_applicable"  selected>Select Panic Device</option>
              <option value='Rim Panic device'>Rim Panic device</option>
              <option value='Panic Device with Concealed Vertical Rods'>Panic Device with Concealed Vertical Rods</option>
              <option value='Panic Device with Surface Mounted Vertical Rods'>Panic Device with Surface Mounted Vertical Rods</option>
              <option value='Blumcraft Panic device'>Blumcraft Panic device</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">J</span><label>Weather Stripping</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_weather_stripping_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_weather_stripping" class="form-control">
              <option value="not_applicable"  selected>Select Weather Stripping</option>
              <option value='Kawneer Astragal'>Kawneer Astragal</option>
              <option value='WS125360GR Tall Gray Weatherstripping'>WS125360GR Tall Gray Weatherstripping</option>
              <option value='WS125500BL Tall Black Weatherstripping'>WS125500BL Tall Black Weatherstripping</option>
              <option value='WS187187BL Black Weatherstripping'>WS187187BL Black Weatherstripping</option>
              <option value='WS187250GRF Tall Gray Weatherstripping W/FIN'>WS187250GRF Tall Gray Weatherstripping W/FIN</option>
              <option value='WS281250GR Gray Weatherstripping'>WS281250GR Gray Weatherstripping</option>
            </select>
          </div>
        </div>
        <div class="col-sm-6 fields-right-half">
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">M</span><label>Floor Closer</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_floor_closer_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_floor_closer" class="form-control">
              <option value="not_applicable"  selected>Select Floor Closer</option>
              <option value='Rixson 27 L/H 105 NHO'>Rixson 27 L/H 105 NHO</option>
              <option value='Rixson 27 R/H 105 NHO'>Rixson 27 R/H 105 NHO</option>
              <option value='Rixson 27 L/H 90 NHO'>Rixson 27 L/H 90 NHO</option>
              <option value='Rixson 27 R/H 90 NHO'>Rixson 27 R/H 90 NHO</option>
              <option value='Rixson 27 L/H 105 HO'>Rixson 27 L/H 105 HO</option>
              <option value='Rixson 27 R/H 105 HO'>Rixson 27 R/H 105 HO</option>
              <option value='Rixson 27 L/H 90 HO'>Rixson 27 L/H 90 HO</option>
              <option value='Rixson 27 R/H 90 HO'>Rixson 27 R/H 90 HO</option>
              <option value='Dorma BTS80 Floor Door Closer'>Dorma BTS80 Floor Door Closer</option>
              <option value='Dorma BTS75 Floor Door Closer'>Dorma BTS75 Floor Door Closer</option>
              <option value='Rixson Bottom Offset Arm L/H (Rixson 27)'>Rixson Bottom Offset Arm L/H (Rixson 27)</option>
              <option value='Rixson Bottom Offset Arm R/H (Rixson 27)'>Rixson Bottom Offset Arm R/H (Rixson 27)</option>
              <option value='Dorma Offset Bottom Arm'>Dorma Offset Bottom Arm</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">N</span><label>Threshold</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_threshold_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_threshold" class="form-control">
              <option value="not_applicable"  selected>Select Threshold</option>
              <option value='4" x 36" x 1/4" Aluminum Threshold'>4" x 36" x 1/4" Aluminum Threshold</option>
              <option value='5" x 36" x 1/4" Aluminum Threshold'>5" x 36" x 1/4" Aluminum Threshold</option>
              <option value='7" x 36" x 1/4" Aluminum Threshold'>7" x 36" x 1/4" Aluminum Threshold</option>
              <option value='8" x 36" x 1/4" Aluminum Threshold'>8" x 36" x 1/4" Aluminum Threshold</option>
              <option value='4" x 48" x 1/4" Aluminum Threshold'>4" x 48" x 1/4" Aluminum Threshold</option>
              <option value='5" x 48" x 1/4" Aluminum Threshold'>5" x 48" x 1/4" Aluminum Threshold</option>
              <option value='7" x 48" x 1/4" Aluminum Threshold'>7" x 48" x 1/4" Aluminum Threshold</option>
              <option value='8" x 48" x 1/4" Aluminum Threshold'>8" x 48" x 1/4" Aluminum Threshold</option>
              <option value='4" x 72" x 1/4" Aluminum Threshold'>4" x 72" x 1/4" Aluminum Threshold</option>
              <option value='5" x 72" x 1/4" Aluminum Threshold'>5" x 72" x 1/4" Aluminum Threshold</option>
              <option value='7" x 72" x 1/4" Aluminum Threshold'>7" x 72" x 1/4" Aluminum Threshold</option>
              <option value='8" x 72" x 1/4" Aluminum Threshold'>8" x 72" x 1/4" Aluminum Threshold</option>
              <option value='4" x 36" x 1/2" Aluminum Threshold'>4" x 36" x 1/2" Aluminum Threshold</option>
              <option value='5" x 36" x 1/2" Aluminum Threshold'>5" x 36" x 1/2" Aluminum Threshold</option>
              <option value='7" x 36" x 1/2" Aluminum Threshold'>7" x 36" x 1/2" Aluminum Threshold</option>
              <option value='8" x 36" x 1/2" Aluminum Threshold'>8" x 36" x 1/2" Aluminum Threshold</option>
              <option value='4" x 48" x 1/2" Aluminum Threshold'>4" x 48" x 1/2" Aluminum Threshold</option>
              <option value='5" x 48" x 1/2" Aluminum Threshold'>5" x 48" x 1/2" Aluminum Threshold</option>
              <option value='7" x 48" x 1/2" Aluminum Threshold'>7" x 48" x 1/2" Aluminum Threshold</option>
              <option value='8" x 48" x 1/2" Aluminum Threshold'>8" x 48" x 1/2" Aluminum Threshold</option>
              <option value='4" x 72" x 1/2" Aluminum Threshold'>4" x 72" x 1/2" Aluminum Threshold</option>
              <option value='5" x 72" x 1/2" Aluminum Threshold'>5" x 72" x 1/2" Aluminum Threshold</option>
              <option value='7" x 72" x 1/2" Aluminum Threshold'>7" x 72" x 1/2" Aluminum Threshold</option>
              <option value='8" x 72" x 1/2" Aluminum Threshold'>8" x 72" x 1/2" Aluminum Threshold</option>
              <option value='4" x 36" x 1/4" Bronze Threshold'>4" x 36" x 1/4" Bronze Threshold</option>
              <option value='5" x 36" x 1/4" Bronze Threshold'>5" x 36" x 1/4" Bronze Threshold</option>
              <option value='7" x 36" x 1/4" Bronze Threshold'>7" x 36" x 1/4" Bronze Threshold</option>
              <option value='8" x 36" x 1/4" Bronze Threshold'>8" x 36" x 1/4" Bronze Threshold</option>
              <option value='4" x 48" x 1/4" Bronze Threshold'>4" x 48" x 1/4" Bronze Threshold</option>
              <option value='5" x 48" x 1/4" Bronze Threshold'>5" x 48" x 1/4" Bronze Threshold</option>
              <option value='7" x 48" x 1/4" Bronze Threshold'>7" x 48" x 1/4" Bronze Threshold</option>
              <option value='8" x 48" x 1/4" Bronze Threshold'>8" x 48" x 1/4" Bronze Threshold</option>
              <option value='4" x 72" x 1/4" Bronze Threshold'>4" x 72" x 1/4" Bronze Threshold</option>
              <option value='5" x 72" x 1/4" Bronze Threshold'>5" x 72" x 1/4" Bronze Threshold</option>
              <option value='7" x 72" x 1/4" Bronze Threshold'>7" x 72" x 1/4" Bronze Threshold</option>
              <option value='8" x 72" x 1/4" Bronze Threshold'>8" x 72" x 1/4" Bronze Threshold</option>
              <option value='4" x 36" x 1/2" Bronze Threshold'>4" x 36" x 1/2" Bronze Threshold</option>
              <option value='5" x 36" x 1/2" Bronze Threshold'>5" x 36" x 1/2" Bronze Threshold</option>
              <option value='7" x 36" x 1/2" Bronze Threshold'>7" x 36" x 1/2" Bronze Threshold</option>
              <option value='8" x 36" x 1/2" Bronze Threshold'>8" x 36" x 1/2" Bronze Threshold</option>
              <option value='4" x 48" x 1/2" Bronze Threshold'>4" x 48" x 1/2" Bronze Threshold</option>
              <option value='5" x 48" x 1/2" Bronze Threshold'>5" x 48" x 1/2" Bronze Threshold</option>
              <option value='7" x 48" x 1/2" Bronze Threshold'>7" x 48" x 1/2" Bronze Threshold</option>
              <option value='8" x 48" x 1/2" Bronze Threshold'>8" x 48" x 1/2" Bronze Threshold</option>
              <option value='4" x 72" x 1/2" Bronze Threshold'>4" x 72" x 1/2" Bronze Threshold</option>
              <option value='5" x 72" x 1/2" Bronze Threshold'>5" x 72" x 1/2" Bronze Threshold</option>
              <option value='7" x 72" x 1/2" Bronze Threshold'>7" x 72" x 1/2" Bronze Threshold</option>
              <option value='8" x 72" x 1/2" Bronze Threshold'>8" x 72" x 1/2" Bronze Threshold</option>
              <option value='4" x 36" x 1/4" Brass Threshold'>4" x 36" x 1/4" Brass Threshold</option>
              <option value='5" x 36" x 1/4" Brass Threshold'>5" x 36" x 1/4" Brass Threshold</option>
              <option value='7" x 36" x 1/4" Brass Threshold'>7" x 36" x 1/4" Brass Threshold</option>
              <option value='8" x 36" x 1/4" Brass Threshold'>8" x 36" x 1/4" Brass Threshold</option>
              <option value='4" x 48" x 1/4" Brass Threshold'>4" x 48" x 1/4" Brass Threshold</option>
              <option value='5" x 48" x 1/4" Brass Threshold'>5" x 48" x 1/4" Brass Threshold</option>
              <option value='7" x 48" x 1/4" Brass Threshold'>7" x 48" x 1/4" Brass Threshold</option>
              <option value='8" x 48" x 1/4" Brass Threshold'>8" x 48" x 1/4" Brass Threshold</option>
              <option value='4" x 72" x 1/4" Brass Threshold'>4" x 72" x 1/4" Brass Threshold</option>
              <option value='5" x 72" x 1/4" Brass Threshold'>5" x 72" x 1/4" Brass Threshold</option>
              <option value='7" x 72" x 1/4" Brass Threshold'>7" x 72" x 1/4" Brass Threshold</option>
              <option value='8" x 72" x 1/4" Brass Threshold'>8" x 72" x 1/4" Brass Threshold</option>
              <option value='4" x 36" x 1/2" Brass Threshold'>4" x 36" x 1/2" Brass Threshold</option>
              <option value='5" x 36" x 1/2" Brass Threshold'>5" x 36" x 1/2" Brass Threshold</option>
              <option value='7" x 36" x 1/2" Brass Threshold'>7" x 36" x 1/2" Brass Threshold</option>
              <option value='8" x 36" x 1/2" Brass Threshold'>8" x 36" x 1/2" Brass Threshold</option>
              <option value='4" x 48" x 1/2" Brass Threshold'>4" x 48" x 1/2" Brass Threshold</option>
              <option value='5" x 48" x 1/2" Brass Threshold'>5" x 48" x 1/2" Brass Threshold</option>
              <option value='7" x 48" x 1/2" Brass Threshold'>7" x 48" x 1/2" Brass Threshold</option>
              <option value='8" x 48" x 1/2" Brass Threshold'>8" x 48" x 1/2" Brass Threshold</option>
              <option value='4" x 72" x 1/2" Brass Threshold'>4" x 72" x 1/2" Brass Threshold</option>
              <option value='5" x 72" x 1/2" Brass Threshold'>5" x 72" x 1/2" Brass Threshold</option>
              <option value='7" x 72" x 1/2" Brass Threshold'>7" x 72" x 1/2" Brass Threshold</option>
              <option value='8" x 72" x 1/2" Brass Threshold'>8" x 72" x 1/2" Brass Threshold</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">T</span><label>Type of Beads</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_type_of_beads_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_type_of_beads" class="form-control">
              <option value="not_applicable"  selected>Select Beads Type</option>
              <option value='Square Beads For 1/4" glass, Aluminum Finish'>Square Beads For 1/4" glass, Aluminum Finish</option>
              <option value='Square Beads For 3/8" glass, Aluminum Finish'>Square Beads For 3/8" glass, Aluminum Finish</option>
              <option value='Square Beads For 1/2" glass, Aluminum Finish'>Square Beads For 1/2" glass, Aluminum Finish</option>
              <option value='Square Beads For 1" glass, Aluminum Finish'>Square Beads For 1" glass, Aluminum Finish</option>
              <option value='Square Beads For 1/4" glass, Bronze Finish'>Square Beads For 1/4" glass, Bronze Finish</option>
              <option value='Square Beads For 3/8" glass, Bronze Finish'>Square Beads For 3/8" glass, Bronze Finish</option>
              <option value='Square Beads For 1/2" glass, Bronze Finish'>Square Beads For 1/2" glass, Bronze Finish</option>
              <option value='Square Beads For 1" glass, Bronze Finish'>Square Beads For 1" glass, Bronze Finish</option>
              <option value='Square Beads For 1/4" glass, Black Finish'>Square Beads For 1/4" glass, Black Finish</option>
              <option value='Square Beads For 3/8" glass, Black Finish'>Square Beads For 3/8" glass, Black Finish</option>
              <option value='Square Beads For 1/2" glass, Black Finish'>Square Beads For 1/2" glass, Black Finish</option>
              <option value='Square Beads For 1" glass, Black Finish'>Square Beads For 1" glass, Black Finish</option>
              <option value='Bevel Beads For 1/4" glass, Aluminum Finish'>Bevel Beads For 1/4" glass, Aluminum Finish</option>
              <option value='Bevel Beads For 1/4" glass, Bronze Finish'>Bevel Beads For 1/4" glass, Bronze Finish</option>
              <option value='Bevel Beads For 1/4" glass, Black Finish'>Bevel Beads For 1/4" glass, Black Finish</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">U</span><label>Thumbturn</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_thumbturn_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_thumbturn" class="form-control">
              <option value="not_applicable"  selected>Select Thumbturn</option>
              <option value='Thumb Turn Cylinder Bronze'>Thumb Turn Cylinder Bronze</option>
              <option value='Thumb Turn Cylinder Aluminum'>Thumb Turn Cylinder Aluminum</option>
              <option value='Thumb Turn Cylinder Brass'>Thumb Turn Cylinder Brass</option>
              <option value='Key Cylinder Bronze'>Key Cylinder Bronze</option>
              <option value='Key Cylinder Aluminum'>Key Cylinder Aluminum</option>
              <option value='Key Cylinder Brass'>Key Cylinder Brass</option>
              <option value='Dummy Cylinder Bronze'>Dummy Cylinder Bronze</option>
              <option value='Dummy Cylinder Aluminum'>Dummy Cylinder Aluminum</option>
              <option value='Dummy Cylinder Brass'>Dummy Cylinder Brass</option>
            </select>
          </div>

          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">V</span><label>Key Cylinder</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_key_cylinder_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_key_cylinder" class="form-control">
              <option value="not_applicable"  selected>Select Key Cylinder</option>
              <option value='Key Cylinder Bronze'>Key Cylinder Bronze</option>
              <option value='Key Cylinder Aluminum'>Key Cylinder Aluminum</option>
              <option value='Key Cylinder Brass'>Key Cylinder Brass</option>
              <option value='Dummy Cylinder Bronze'>Dummy Cylinder Bronze</option>
              <option value='Dummy Cylinder Aluminum'>Dummy Cylinder Aluminum</option>
              <option value='Dummy Cylinder Brass'>Dummy Cylinder Brass</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">W</span><label>Lock</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_lock_type_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_lock_type" class="form-control">
              <option value="not_applicable"  selected>Select Lock</option>
              <option value='Adams Rite Dead Latch lock 4511 1 1/8 back set'>Adams Rite Dead Latch lock 4511 1 1/8 back set</option>
              <option value='Adams Rite Dead Latch lock 4511 1 1/2 back set'>Adams Rite Dead Latch lock 4511 1 1/2 back set</option>
              <option value='Adams Rite Dead Latch lock 4511 31/32 back set'>Adams Rite Dead Latch lock 4511 31/32 back set</option>
              <option value='Adams Rite Dead Latch lock 4511 7/8 Back set'>Adams Rite Dead Latch lock 4511 7/8 Back set</option>
              <option value='Adams Rite Dead Bolt lock 31/32 back set'>Adams Rite Dead Bolt lock 31/32 back set</option>
              <option value='Adams Rite Dead Bolt lock 1 1/8 back set'>Adams Rite Dead Bolt lock 1 1/8 back set</option>
              <option value='Adams Rite Dead Bolt lock 1 1/2 back set'>Adams Rite Dead Bolt lock 1 1/2 back set</option>
              <option value='Adams Rite Hook Dead Bolt lock 31/32 back set'>Adams Rite Hook Dead Bolt lock 31/32 back set</option>
              <option value='Adams Rite Hook Dead Bolt lock 1 1/8 back set'>Adams Rite Hook Dead Bolt lock 1 1/8 back set</option>
              <option value='Adams Rite Hook Dead Bolt lock 1 1/2 back set'>Adams Rite Hook Dead Bolt lock 1 1/2 back set</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">X</span><label>Push Paddle</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_path_paddle_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_path_paddle" class="form-control">
              <option value="not_applicable"  selected>Select Push Paddle</option>
              <option value='Adams Rite Push Paddle L/H Bronze'>Adams Rite Push Paddle L/H Bronze</option>
              <option value='Adams Rite Push Paddle L/H Aluminum'>Adams Rite Push Paddle L/H Aluminum</option>
              <option value='Adams Rite Push Paddle R/H Bronze'>Adams Rite Push Paddle R/H Bronze</option>
              <option value='Adams Rite Push Paddle R/H Aluminum'>Adams Rite Push Paddle R/H Aluminum</option>
              <option value='Adams Rite Lever handle for dead latch lock Bronze'>Adams Rite Lever handle for dead latch lock Bronze</option>
              <option value='Adams Rite Lever handle for dead latch lock Aluminum'>Adams Rite Lever handle for dead latch lock Aluminum</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Y1</span><label>Top Flush Bolt</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_top_flush_bolt_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_top_flush_bolt" class="form-control">
              <option value="not_applicable"  selected>Select Top Flush Bolt</option>
              <option value='Flush Bolt Aluminum'>Flush Bolt Aluminum</option>
              <option value='Flush Bolt Bronze'>Flush Bolt Bronze</option>
              <option value='Flush Bolt Brass'>Flush Bolt Brass</option>
              <option value='Sliding Bolt'>Sliding Bolt</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Y2</span><label>Bottom Flush Bolt</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_bottom_flush_bolt_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_bottom_flush_bolt" class="form-control">
              <option value="not_applicable"  selected>Select Bottom Flush Bolt</option>
              <option value='Flush Bolt Aluminum'>Flush Bolt Aluminum</option>
              <option value='Flush Bolt Bronze'>Flush Bolt Bronze</option>
              <option value='Flush Bolt Brass'>Flush Bolt Brass</option>
              <option value='Sliding Bolt'>Sliding Bolt</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Z1</span><label>Top Pivot Offset</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_top_pivot_offset_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_top_pivot_offset" class="form-control">
              <option value="not_applicable"  selected>Select Top Pivot Offset</option>
              <option value='Rixson top Offset Pivot'>Rixson top Offset Pivot</option>
              <option value='Vistawall Offset Top pivot Bronze'>Vistawall Offset Top pivot Bronze</option>
              <option value='Vistawall Offset Top pivot Aluminum'>Vistawall Offset Top pivot Aluminum</option>
              <option value='Kawneer Offset Top pivot Bronze'>Kawneer Offset Top pivot Bronze</option>
              <option value='Kawneer Offset Top pivot Aluminum'>Kawneer Offset Top pivot Aluminum</option>
              <option value='YKK Offset Top pivot Bronze'>YKK Offset Top pivot Bronze</option>
              <option value='YKK Offset Top pivot Aluminum'>YKK Offset Top pivot Aluminum</option>
              <option value='US Aluminum Offset Top pivot Bronze'>US Aluminum Offset Top pivot Bronze</option>
              <option value='US Aluminum Offset Top pivot Aluminum'>US Aluminum Offset Top pivot Aluminum</option>
              <option value='Architectural Design Offset Top pivot Bronze'>Architectural Design Offset Top pivot Bronze</option>
              <option value='Architectural Design Offset Top pivot Aluminum'>Architectural Design Offset Top pivot Aluminum</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Z2</span><label>Intermediate Pivot</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_intermediate_pivot_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_intermediate_pivot" class="form-control">
              <option value="not_applicable"  selected>Select Top Pivot Offset</option>
              <option value='Rixson Intermediate pivot L/H'>Rixson Intermediate pivot L/H</option>
              <option value='Rixson Intermediate pivot R/H'>Rixson Intermediate pivot R/H</option>
              <option value='Vistawall Intermediate pivot L/H Bronze'>Vistawall Intermediate pivot L/H Bronze</option>
              <option value='Vistawall Intermediate pivot R/H Bronze'>Vistawall Intermediate pivot R/H Bronze</option>
              <option value='Vistawall Intermediate pivot L/H Aluminum'>Vistawall Intermediate pivot L/H Aluminum</option>
              <option value='Vistawall Intermediate pivot R/H aluminum'>Vistawall Intermediate pivot R/H aluminum</option>
              <option value='Kawneer Intermediate pivot L/H Bronze'>Kawneer Intermediate pivot L/H Bronze</option>
              <option value='Kawneer Intermediate pivot R/H Bronze'>Kawneer Intermediate pivot R/H Bronze</option>
              <option value='Kawneer Intermediate pivot L/H Aluminum'>Kawneer Intermediate pivot L/H Aluminum</option>
              <option value='Kawneer Intermediate pivot R/H aluminum'>Kawneer Intermediate pivot R/H aluminum</option>
              <option value='YKK Intermediate pivot L/H Bronze'>YKK Intermediate pivot L/H Bronze</option>
              <option value='YKK Intermediate pivot R/H Bronze'>YKK Intermediate pivot R/H Bronze</option>
              <option value='YKK Intermediate pivot L/H Aluminum'>YKK Intermediate pivot L/H Aluminum</option>
              <option value='YKK Intermediate pivot R/H aluminum'>YKK Intermediate pivot R/H aluminum</option>
              <option value='US Aluminum Intermediate pivot L/H Bronze'>US Aluminum Intermediate pivot L/H Bronze</option>
              <option value='US Aluminum Intermediate pivot R/H Bronze'>US Aluminum Intermediate pivot R/H Bronze</option>
              <option value='US Aluminum Intermediate pivot L/H Aluminum'>US Aluminum Intermediate pivot L/H Aluminum</option>
              <option value='US Aluminum Intermediate pivot R/H aluminum'>US Aluminum Intermediate pivot R/H aluminum</option>
              <option value='Architectural Design Intermediate pivot L/H Bronze'>Architectural Design Intermediate pivot L/H Bronze</option>
              <option value='Architectural Design Intermediate pivot R/H Bronze'>Architectural Design Intermediate pivot R/H Bronze</option>
              <option value='Architectural Design Intermediate pivot L/H Aluminum'>Architectural Design Intermediate pivot L/H Aluminum</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Z3</span><label>Bottom Pivot Offset</label>
              </div>
              <div class="col-sm-5">
                <select id="select_tagd_sfd_offset_pivots_bottom_pivot_offset_status" class="form-control">
                  <option value="not_applicable" selected>Details</option>
                  <option value="info_only">Info Only </option>
                  <option value="adjust_align">Adjust/Align</option>
                  <option value="replace">Replace</option>
                </select>
              </div>
            </div>
            <select id="select_tagd_sfd_offset_pivots_bottom_pivot_offset" class="form-control">
              <option value="not_applicable"  selected>Select Bottom Pivot Offset</option>
              <option value='Rixson Bottom Offset Pivot L/H'>Rixson Bottom Offset Pivot L/H</option>
              <option value='Rixson Bottom Offset Pivot R/H'>Rixson Bottom Offset Pivot R/H</option>
              <option value='Vistawall Offset Bottom pivot L/H Bronze'>Vistawall Offset Bottom pivot L/H Bronze</option>
              <option value='Vistawall Offset Bottom pivot R/H Bronze'>Vistawall Offset Bottom pivot R/H Bronze</option>
              <option value='Vistawall Offset Bottom pivot L/H Aluminum'>Vistawall Offset Bottom pivot L/H Aluminum</option>
              <option value='Vistawall Offset Bottom pivot R/H Aluminum'>Vistawall Offset Bottom pivot R/H Aluminum</option>
              <option value='kawneer Offset Bottom pivot L/H Bronze'>kawneer Offset Bottom pivot L/H Bronze</option>
              <option value='Kawneer Offset Bottom pivot R/H Bronze'>Kawneer Offset Bottom pivot R/H Bronze</option>
              <option value='Kawneer Offset Bottom pivot L/H Aluminum'>Kawneer Offset Bottom pivot L/H Aluminum</option>
              <option value='Kawneer Offset Bottom pivot R/H Aluminum'>Kawneer Offset Bottom pivot R/H Aluminum</option>
              <option value='YKK Offset Bottom pivot L/H Bronze'>YKK Offset Bottom pivot L/H Bronze</option>
              <option value='YKK Offset Bottom pivot R/H Bronze'>YKK Offset Bottom pivot R/H Bronze</option>
              <option value='US Aluminum Offset Bottom pivot L/H Bronze'>US Aluminum Offset Bottom pivot L/H Bronze</option>
              <option value='US Aluminum Offset Bottom pivot R/H Bronze'>US Aluminum Offset Bottom pivot R/H Bronze</option>
              <option value='US Aluminum Offset Bottom pivot L/H Aluminum'>US Aluminum Offset Bottom pivot L/H Aluminum</option>
              <option value='US Aluminum Offset Bottom pivot R/H Aluminum'>US Aluminum Offset Bottom pivot R/H Aluminum</option>
              <option value='Architectural Design Offset Bottom pivot L/H Bronze'>Architectural Design Offset Bottom pivot L/H Bronze</option>
              <option value='Architectural Design Offset Bottom pivot R/H Bronze'>Architectural Design Offset Bottom pivot R/H Bronze</option>
              <option value='Architectural Design Offset Bottom pivot L/H Aluminum'>Architectural Design Offset Bottom pivot L/H Aluminum</option>
              <option value='Architectural Design Offset Bottom pivot R/H Aluminum'>Architectural Design Offset Bottom pivot R/H Aluminum</option>
            </select>
          </div>
          <!-- <div class="field-wrapper">
            <button id="button_tagd_sfd_offset_pivots_clear" type="button" class="btn btn-default">Clear</button>
            <button id="button_tagd_sfd_offset_pivots_save" type="button" class="btn btn-default">Save</button>
          </div> -->
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="col-xs-6">
   <div class="form-group">
            <div class="col-xs-12">
               <label class="leftalign">Glass materials</label>
            </div>
         </div>
<div class="form-group">
  <div class="col-xs-12 leftalign">
    <div class="panel panel-default">
      <div class="panel-body chaulksection materials-section" >
         <div class="form-group addqty"  id="add_quntity_0">
                    <div  class="add_quntity" id="1" >
                      <div class="myCustom" id="add_id_0_0">
                        <fieldset class="col-xs-12 margin-top_culk_minus" id="fieldset_tgpd_glass_materials_caulk">
                          <div class="row add">
                            <div class="col-xs-3 col-md-4 col-lg-4 gls_caulk">
                              <label class="leftalign add" for="text_tgpd_glass_materials_caulk_amount">Amount</label>
                              <div class="input-group">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_caulk_amount_0_0">
                                    <span class="glyphicon glyphicon-minus">                                 </span>
                                </button>
                              </span>
                              <input type="text" id="text_tgpd_glass_materials_caulk_amount_0_0" value="0" name="text_tgpd_glass_materials_caulk_amount_0_0" class="form-control input-number-plus-minus qty-align-new user-success" min="0" max="100" /><span class="input-group-btn">
                              <button data-field="text_tgpd_glass_materials_caulk_amount_0_0" type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"><span class="glyphicon glyphicon-plus">                             
                              </span>
                            </button>
                          </span>
                        </div>
                      </div>
                      <div class="col-xs-8 col-md-6 col-lg-6 add">
                        <label class="leftalign add" for="select_tgpd_glass_materials_caulk_type">CAULK</label>
                        <select id="select_tgpd_glass_materials_caulk_type_0_0" class="form-control add user-success">
                          <option value="not_applicable">Caulk type</option>
                          <option value="799-Clear (T)">799-Clear (T)</option>
                          <option value="999-A-Clear (T)">999-A-Clear (T)</option>
                          <option value="790-Black (S)">790-Black (S)</option>
                          <option value="790-Black (T)">790-Black (T)</option>
                          <option value="795-Black (S)">795-Black (S)</option>
                          <option value="795-Black (T)">795-Black (T)</option>
                          <option value="995-Black (S)">995-Black (S)</option>
                          <option value="999-A-Black (T)">999-A-Black (T)</option>
                          <option value="Dymonic-Black (S)">Dymonic-Black (S)</option>
                          <option value="Dymonic-Black (T)">Dymonic-Black (T)</option>
                          <option value="790-Bronze (S)">790-Bronze (S)</option>
                          <option value="790-Bronze (T)">790-Bronze (T)</option>
                          <option value="795-Bronze (S)">795-Bronze (S)</option>
                          <option value="795-Bronze (T)">795-Bronze (T)</option>
                          <option value="999-A-Bronze (T)">999-A-Bronze (T)</option>
                          <option value="Dymonic-Bronze (S)">Dymonic-Bronze (S)</option>
                          <option value="Dymonic-Bronze (T)">Dymonic-Bronze (T)</option>
                          <option value="795-Anodized Aluminum (S)">795-Anodized Aluminum (S)</option>
                          <option value="795-Anodized Aluminum (T)">795-Anodized Aluminum (T)</option>
                          <option value="999-A-Aluminum (T)">999-A-Aluminum (T)</option>
                          <option value="Dymonic-Anodized Aluminum (S)">Dymonic-Anodized Aluminum (S)</option>
                          <option value="Dymonic-Anodized Aluminum (T)">Dymonic-Anodized Aluminum (T)</option>
                          <option value="790-White (S)">790-White (S)</option>
                          <option value="790-White (T)">790-White (T)</option>
                          <option value="795-White (S)">795-White (S)</option>
                          <option value="795-White (T)">795-White (T)</option>
                          <option value="995-White (S)">995-White (S)</option>
                          <option value="999-A-White (T)">999-A-White (T)</option>
                          <option value="Dymonic-White (S)">Dymonic-White (S)</option>
                          <option value="Dymonic-White (T)">Dymonic-White (T)</option>
                          <option value="Rockite (Bucket)">Rockite (Bucket)</option>
                          <option value="Rockite (Gallon)">Rockite (Gallon)</option>
                          <option value="Epoxy (Gallon)">Epoxy (Gallon)</option>
                          <option value="Putty White (Pint) - Wood">Putty White (Pint) - Wood</option>
                          <option value="Putty White (Gallon) - Wood">Putty White (Gallon) - Wood</option>
                          <option value="Putty Grey (Pint) - Metal">Putty Grey (Pint) - Metal</option>
                          <option value="Putty Grey (Gallon) - Metal">Putty Grey (Gallon) - Metal</option>
                          <option value="790-Adobe Tan (S)">790-Adobe Tan (S)</option>
                          <option value="790-Adobe Tan (T)">790-Adobe Tan (T)</option>
                          <option value="790-Blue Spruce (S)">790-Blue Spruce (S)</option>
                          <option value="790-Blue Spruce (T)">790-Blue Spruce (T)</option>
                          <option value="790-Charcoal (S)">790-Charcoal (S)</option>
                          <option value="790-Charcoal (T)">790-Charcoal (T)</option>
                          <option value="790-Dusty Rose (S)">790-Dusty Rose (S)</option>
                          <option value="790-Dusty Rose (T)">790-Dusty Rose (T)</option>
                          <option value="790-Gray (S)">790-Gray (S)</option>
                          <option value="790-Gray (T)">790-Gray (T)</option>
                          <option value="790-Limestone (S)">790-Limestone (S)</option>
                          <option value="790-Limestone (T)">790-Limestone (T)</option>
                          <option value="790-Natural Stone (S)">790-Natural Stone (S)</option>
                          <option value="790-Natural Stone (T)">790-Natural Stone (T)</option>
                          <option value="790-Precast White (S)">790-Precast White (S)</option>
                          <option value="790-Precast White (T)">790-Precast White (T)</option>
                          <option value="790-Rustic Brick (S)">790-Rustic Brick (S)</option>
                          <option value="790-Rustic Brick (T)">790-Rustic Brick (T)</option>
                          <option value="790-Sandstone (S)">790-Sandstone (S)</option>
                          <option value="790-Sandstone (T)">790-Sandstone (T)</option>
                          <option value="795-Adobe Tan (S)">795-Adobe Tan (S)</option>
                          <option value="795-Adobe Tan (T)">795-Adobe Tan (T)</option>
                          <option value="795-Blue Spruce (S)">795-Blue Spruce (S)</option>
                          <option value="795-Blue Spruce (T)">795-Blue Spruce (T)</option>
                          <option value="795-Champagne (S)">795-Champagne (S)</option>
                          <option value="795-Champagne (T)">795-Champagne (T)</option>
                          <option value="795-Charcoal (S)">795-Charcoal (S)</option>
                          <option value="795-Charcoal (T)">795-Charcoal (T)</option>
                          <option value="795-Dusty Rose (S)">795-Dusty Rose (S)</option>
                          <option value="795-Dusty Rose (T)">795-Dusty Rose (T)</option>
                          <option value="795-Gray (S)">795-Gray (S)</option>
                          <option value="795-Gray (T)">795-Gray (T)</option>
                          <option value="795-Limestone (S)">795-Limestone (S)</option>
                          <option value="795-Limestone (T)">795-Limestone (T)</option>
                          <option value="795-Natural Stone (S)">795-Natural Stone (S)</option>
                          <option value="795-Natural Stone (T)">795-Natural Stone (T)</option>
                          <option value="795-Rustic Brick (S)">795-Rustic Brick (S)</option>
                          <option value="795-Rustic Brick (T)">795-Rustic Brick (T)</option>
                          <option value="795-Sandstone (S)">795-Sandstone (S)</option>
                          <option value="795-Sandstone (T)">795-Sandstone (T)</option>
                          <option value="995-Gray (S)">995-Gray (S)</option>
                          <option value="999-A-Light Bronze (T)">999-A-Light Bronze (T)</option>
                          <option value="Dymonic-Almond (S)">Dymonic-Almond (S)</option>
                          <option value="Dymonic-Almond (T)">Dymonic-Almond (T)</option>
                          <option value="Dymonic-Aluminum Stone (S)">Dymonic-Aluminum Stone (S)</option>
                          <option value="Dymonic-Aluminum Stone (T)">Dymonic-Aluminum Stone (T)</option>
                          <option value="Dymonic-Beige (S)">Dymonic-Beige (S)</option>
                          <option value="Dymonic-Beige (T)">Dymonic-Beige (T)</option>
                          <option value="Dymonic-Buff (S)">Dymonic-Buff (S)</option>
                          <option value="Dymonic-Buff (T)">Dymonic-Buff (T)</option>
                          <option value="Dymonic-Dark Bronze (S)">Dymonic-Dark Bronze (S)</option>
                          <option value="Dymonic-Dark Bronze (T)">Dymonic-Dark Bronze (T)</option>
                          <option value="Dymonic-Gray (S)">Dymonic-Gray (S)</option>
                          <option value="Dymonic-Gray (T)">Dymonic-Gray (T)</option>
                          <option value="Dymonic-Gray Stone (S)">Dymonic-Gray Stone (S)</option>
                          <option value="Dymonic-Gray Stone (T)">Dymonic-Gray Stone (T)</option>
                          <option value="Dymonic-Hartford Green (S)">Dymonic-Hartford Green (S)</option>
                          <option value="Dymonic-Hartford Green (T)">Dymonic-Hartford Green (T)</option>
                          <option value="Dymonic-Ivory (S)">Dymonic-Ivory (S)</option>
                          <option value="Dymonic-Ivory (T)">Dymonic-Ivory (T)</option>
                          <option value="Dymonic-Light Bronze (S)">Dymonic-Light Bronze (S)</option>
                          <option value="Dymonic-Light Bronze (T)">Dymonic-Light Bronze (T)</option>
                          <option value="Dymonic-Limestone (S)">Dymonic-Limestone (S)</option>
                          <option value="Dymonic-Limestone (T)">Dymonic-Limestone (T)</option>
                          <option value="Dymonic-Natural Clay (S)">Dymonic-Natural Clay (S)</option>
                          <option value="Dymonic-Natural Clay (T)">Dymonic-Natural Clay (T)</option>
                          <option value="Dymonic-Off White (S)">Dymonic-Off White (S)</option>
                          <option value="Dymonic-Off White (T)">Dymonic-Off White (T)</option>
                          <option value="Dymonic-Precast White (S)">Dymonic-Precast White (S)</option>
                          <option value="Dymonic-Precast White (T)">Dymonic-Precast White (T)</option>
                          <option value="Dymonic-Redwood Tan (S)">Dymonic-Redwood Tan (S)</option>
                          <option value="Dymonic-Redwood Tan (T)">Dymonic-Redwood Tan (T)</option>
                          <option value="Dymonic-Sandalwood (S)">Dymonic-Sandalwood (S)</option>
                          <option value="Dymonic-Sandalwood (T)">Dymonic-Sandalwood (T)</option>
                          <option value="Dymonic-Stone (S)">Dymonic-Stone (S)</option>
                          <option value="Dymonic-Stone (T)">Dymonic-Stone (T)</option>
                        </select>
                      </div>
                      <div class="col-xs-1">
                        <a title="<?php echo basename(__FILE__, '.php'); ?>" href="javascript:void(0);" class="btn_pluse btn12_patio" >+</a>
                      </div>
                    </div>
                  </fieldset>
                  </div>


                    </div>
                     <div class="add_tap_amount" id="1">
                      <div class="myCustom1" id="add_id_tap_0">
                        <div class="col-xs-12">
                          <div class="row plus-minus-btn-row">
                            <div class="col-xs-12">
                              <fieldset id="fieldset_tgpd_glass_materials_tape">
                                <div class="row">
                                  <div class="col-xs-3 col-md-4 gls_caulk">
                                    <label class="leftalign">Amount</label>
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_tape_amount_0_0">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                      </span>
                                      <input type="text" id="text_tgpd_glass_materials_tape_amount_0_0" value="0" name="text_tgpd_glass_materials_tape_amount_0_0" class="form-control input-number-plus-minus qty-align-new user-success" min="0" max="100"><span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="text_tgpd_glass_materials_tape_amount_0_0">
                                          <span class="glyphicon glyphicon-plus"></span>
                                        </button></span>
                                      </div>
                                    </div>
                                    <div class="col-xs-8 col-md-6 add">
                                      <label class="leftalign">TAPE</label>
                                      <select id="select_tgpd_glass_materials_tape_type_0_0" class="form-control">
                                        <option value="not_applicable">Tape type</option>
                                        <option value="1_over_8_440">1/8 440 tape</option>
                                        <option value="1_over_4_440">1/4 440 tape</option>
                                        <option value="1_over_2_440">1/2 440 tape</option>
                                        <option value="3_over_4_440">3/4 440 tape</option>
                                        <option value="foam">Foam tape</option>
                                        <option value="double_faced">Double faced tape</option>
                                        <option value="cladding">Cladding tape</option>
                                      </select>
                                    </div> 
                                    <div class="col-xs-1">
                                      <a title="<?php echo basename(__FILE__, '.php'); ?>" href="javascript:void(0);" class="btn_pluse btn11_patio" >+</a>
                                    </div> 
                                  </div>
                                   </fieldset>
                                 </div>
                               </div>
                             </div>
                           </div>

                     </div>
                     <div class="add_tap_scaff" id="1">
                       <div class="myCustom1" id="add_id_scaff_0">
                        <div class="col-xs-12 add">
                          <div class="row add plus-minus-btn-row">
                            <div class="col-xs-12 add">
                              <fieldset id="fieldset_tgpd_glass_materials_scaffolding">
                                <div class="row add">
                                  <div class="col-xs-3 col-md-4  gls_caulk">
                                   <label class="leftalign smaller" for="select_tgpd_glass_materials_quantity">Quantity</label>  <div class="input-group col-xs-12">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="select_tgpd_glass_materials_quantity_0_0">
                                        <span class="glyphicon glyphicon-minus"></span>
                                      </button></span>
                                       <input id="select_tgpd_glass_materials_quantity_0_0" name="select_tgpd_glass_materials_quantity_0_0" type="text" class="form-control text-num input-number-plus-minus qty-align-new user-success" autocomplete="on" min="0" max="100" value="0">
                                       <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_quantity_0_0">
                                          <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                      </span>
                                    </div> 
                                  </div>
                                  <div class="col-xs-8 col-md-6 add">
                                    <label class="leftalign add" for="select_tgpd_glass_materials_scaffolding_type">EQUIPMENT</label>
                                    <select id="select_tgpd_glass_materials_scaffolding_type_0_0" class="form-control add user-success">
                                      <option value="not_applicable">Equipment type</option>
                                      <option value="Swing Stage (SAG)">Swing Stage (SAG)</option>
                                      <option value="Swing Stage (SUB)">Swing Stage (SUB)</option>
                                      <option value="Scaffolding (SAG)">Scaffolding (SAG)</option>
                                      <option value="Scaffolding-HALF (SAG)">Scaffolding-HALF (SAG)</option
                                        ><option value="Scaffolding (SUB, RENT)">Scaffolding (SUB, RENT)</option>
                                        <option value="Scaffolding (SUB, SETUP)">Scaffolding (SUB, SETUP)</option>
                                        <option value="Scaffolding-BAKER (SUB.RENT)">Scaffolding-BAKER (SUB.RENT)</option>
                                        <option value="Scaffolding-BAKER (SUB.SETUP)">Scaffolding-BAKER (SUB.SETUP)</option>
                                        <option value="40FT Articulating Lift">40FT Articulating Lift</option>
                                        <option value="60FT Articulating Lift">60FT Articulating Lift</option>
                                        <option value="80FT Articulating Lift">80FT Articulating Lift</option>
                                        <option value="120FT Articulating Lift">120FT Articulating Lift</option>
                                        <option value="135FT Articulating Lift">135FT Articulating Lift</option>
                                        <option value="40FT STICK BOOM">40FT STICK BOOM</option>
                                        <option value="60FT STICK BOOM">60FT STICK BOOM</option>
                                        <option value="80FT STICK BOOM">80FT STICK BOOM</option>
                                        <option value="120FT STICK BOOM">120FT STICK BOOM</option>
                                        <option value="135FT STICK BOOM">135FT STICK BOOM</option>
                                        <option value="19FT Scissor Lift">19FT Scissor Lift</option>
                                        <option value="24-26FT Scissor Lift">24-26FT Scissor Lift</option>
                                        <option value="30-35FT Scissor Lift">30-35FT Scissor Lift</option>
                                        <option value="39-40FT Scissor Lift">39-40FT Scissor Lift</option>
                                        <option value="25-27T 4WD Scissor Lift">25-27T 4WD Scissor Lift</option>
                                        <option value="36-49FT 4WD Scissor Lift">36-49FT 4WD Scissor Lift</option>
                                        <option value="Towable Boom">Towable Boom</option>
                                        <option value="Generator">Generator</option>
                                        <option value="Power Cup">Power Cup</option>
                                        <option value="Crane">Crane</option>
                                        <option value="Lift My Glass">Lift My Glass</option><option value="LULL LIFT">LULL LIFT</option>
                                        <option value="Fork Boom">Fork Boom</option>
                                        <option value="Chain Fall">Chain Fall</option>
                                        <option value="Traffic Control">Traffic Control</option>
                                        <option value="Street Closure Permit">Street Closure Permit</option>
                                        <option value="New Part- TEXT BOX">New Part- TEXT BOX</option>
                                      </select>
                                    </div>
                                    <div class="col-xs-1"><a href="javascript:void(0);" title="<?php echo basename(__FILE__, '.php'); ?>" class="btn_pluse btn14_patio" >+</a>
                                    </div>         
                                     </div>
                                   </fieldset>
                                 </div>
                               </div>
                             </div>
                           </div>
                     </div>
                     <div class="add_tap_channel" id="1">
                       <div class="myCustom1">
                        <div class="col-xs-12">
                          <div class="row add plus-minus-btn-row">
                            <div class="col-xs-12 add">
                              <fieldset id="fieldset_tgpd_glass_materials_channel">
                                <div class="row add">
                                <div class="col-xs-3 col-md-4  gls_caulk" >
                                  <label class="leftalign smaller" for="select_tgpd_glass_materials_channel_quantity">Quantity</label>
                                  <div class="input-group col-xs-12">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number-plus-minus bt_for_minus" data-type="minus" data-field="select_tgpd_glass_materials_channel_quantity_0_0">
                                        <span class="glyphicon glyphicon-minus">
                                        
                                      </span>
                                    </button>
                                  </span>
                                  <input id="select_tgpd_glass_materials_channel_quantity_0_0" name="select_tgpd_glass_materials_channel_quantity_0_0" type="text" class="form-control text-num input-number-plus-minusqty-align-new text-center user-success" autocomplete="on" min="0" max="100" value="0">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_channel_quantity_0_0">
                                      <span class="glyphicon glyphicon-plus">
                                      
                                    </span>
                                  </button>
                                </span>
                              </div> 
                            </div>
                            <div class=" col-xs-8 col-md-6 add" id="add_id_channel_0">
                              <label class="leftalign" style="">CHANNEL</label>
                              <select id="text_tgpd_glass_materials_channel_0_0" class="form-control add user-success">
                                <option value="not_applicable">Channel Type</option>
                                   <option value="J-Channel 5/8 Chrome">J-Channel 5/8 Chrome</option>
                                   <option value="J-Channel 5/8 Gold">J-Channel 5/8 Gold</option>
                                   <option value="J-Channel 5/8 Brushed Nickel">J-Channel 5/8 Brushed Nickel</option>
                                   <option value="J-Channel 5/8 Oil Rubbed Bronze">J-Channel 5/8 Oil Rubbed Bronze</option>
                                   <option value="J-Channel 3/8 Chrome">J-Channel 3/8 Chrome</option>
                                   <option value="J-Channel 3/8 Gold">J-Channel 3/8 Gold</option>
                                   <option value="J-Channel 3/8 Brushed Nickel">J-Channel 3/8 Brushed Nickel</option>
                                   <option value="J-Channel 3/8 Oil Rubbed Bronze">J-Channel 3/8 Oil Rubbed Bronze</option>
                                   <option value="L-Channel Chrome">L-Channel Chrome</option>
                                   <option value="L-Channel Gold">L-Channel Gold</option>
                                   <option value="L-Channel Brushed Nickel">L-Channel Brushed Nickel</option>
                                   <option value="L-Channel Oil Rubbed Bronze">L-Channel Oil Rubbed Bronze</option>
                                   <option value="U-Channel 3/4 x 3/4 Clear Anodized/ Satin">U-Channel 3/4 x 3/4 Clear Anodized/ Satin</option>

                                   <option value="U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin">U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 1 Clear Anodized/ Satin">U-Channel 1 x 1 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 1 Polished Brite Silver">U-Channel 1 x 1 Polished Brite Silver</option>
                                   <option value="U-Channel 1 x 1 Brushed Stainless">U-Channel 1 x 1 Brushed Stainless</option>
                                   <option value="U-Channel 1 x 2 Clear Anodized/ Satin">U-Channel 1 x 2 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 2 Polished Brite Silver">U-Channel 1 x 2 Polished Brite Silver</option>
                                   <option value="U-Channel 1 x 2 Brushed Stainless">U-Channel 1 x 2 Brushed Stainless</option>
                                   <option value="Tube - Chrome">Tube - Chrome</option>
                                   <option value="Tube - Brushed Nickel">Tube - Brushed Nickel</option>
                                   <option value="Tube - Oil Rubbed Bronze">Tube - Oil Rubbed Bronze</option>
                                   <option value="Glazing Rubber for U Channel, 1/4 Glass - Black">Glazing Rubber for U Channel, 1/4 Glass - Black</option>
                                   <option value="Glazing Rubber for U Channel, 3/8 Glass - Black">Glazing Rubber for U Channel, 3/8 Glass - Black</option>
                                   <option value="Glazing Rubber for U Channel, 1/2 Glass - Black">Glazing Rubber for U Channel, 1/2 Glass - Black</option>
                                 </select>
                               </div>
                               <div class=" col-xs-1"><a href="javascript:void(0);" title="<?php echo basename(__FILE__, '.php'); ?>" class="btn_pluse btn13_patio" >+</a>
                               </div>
                             </div>
                           </fieldset>
                         </div>
                       </div>
                     </div>
                   </div>
                    </div>
         </div>
         <div class="form-group">
      <div class="col-xs-12 col-lg-10">
               <label class="leftalign" for="text_tgpd_miscellaneous">Miscellaneous</label>
         <input id="text_tgpd_miscellaneous" class="form-control" type="text" autocomplete="on" maxlength="255" placeholder="Miscellaneous">
                &nbsp;
      </div>
         </div>
      </div>
         </div>
      </div>
      </div>
      </div>
      <div class="col-xs-6">
      <div class="form-group">
            <div class="col-xs-12 col-md-12">
               <label class="leftalign">Glass reminders</label>
            </div>
         </div>
      <div class="form-group">
            <div class="col-xs-12 col-sm-12">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header">Solar film?</h5>
            <label class="switch-light switch-candy">
               <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_solar_film" value="solar_film" door_type="<?php echo basename(__FILE__, '.php'); ?>">
               <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
               </span>
            </label>
         </div>
      </div>
                     </div>
                        <div class="col-xs-12">
                              <div class="form-group">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film1" style="display: none;">
                 <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_sag" value="sag" door_type="<?php echo basename(__FILE__, '.php'); ?>">SAG
                                       </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_customer" value="customer" door_type="<?php echo basename(__FILE__, '.php'); ?>">Customer
                                       </label>
                                    </div>
                  </div>
                                 </fieldset>
                              </div>
                           </div>
                           <div class="col-xs-12">
                              <div class="row">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film2" style="display: none;">
                                    <div class="form-group">
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_type" type="text" class="form-control"  placeholder="Film type">
                                       </div>
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_source" type="text" class="form-control" autocomplete="on" placeholder="Film source">
                                       </div>
                                    </div>
                                 </fieldset>
                              </div>
                           </div>

         <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header">Wet seal?</h5>
                              <label class="switch-light switch-candy" onclick="">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_wet_seal" value="wet_seal" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12">
            <div class="row">
                              <fieldset id="fieldset_tgpd_glass_reminders_wet_seal" style="display: none;">
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag">SAG
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_half_day">SAG Sub Half Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_full_day">SAG Sub Full Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_customer" value="customer">Customer
                                    </label>
                                 </div>
                              </fieldset>
            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Furniture to Move?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_furniture_to_move" value="furniture_to_move" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12 col-sm-12">
            <div class="row">
         <div class="col-sm-12">
         <fieldset id="fieldset_tgpd_glass_reminders_furniture_to_move" style="display: none;">
            <input id="text_tgpd_glass_reminders_furniture_to_move_comment" class="form-control" type="text"   placeholder="Comment">
         </fieldset>
         </div>
            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Walls/ceilings cut?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut" value="walls_or_ceilings_to_cut" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>

                           <div class="col-xs-12 col-sm-12">
                              <div class="row">
                                 <div class="">
                                    <div class="form-group">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1" style="display: none;">
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_sag" value="sag">SAG
                                             </label>
                                          </div>
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_customer" value="customer">Customer
                                             </label>
                                          </div>
                                       </fieldset>
                                    </div>
                                 </div>
                                 <div class="col-sm-12 col-md-12 leftalign">
                                    <div class="row">
               <div class="col-sm-12">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2" style="display: none;">
                                          <input id="text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment" class="form-control" type="text" autocomplete="on"  placeholder="Comment">
                                       </fieldset>
               </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
         <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Blind needs Removing?</h5>
            <label class="switch-light switch-candy">
         <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_blind_needs_removing" door_type="<?php echo basename(__FILE__, '.php'); ?>" value="blind_needs_removing">
         <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
         </span>
            </label>
         </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                              <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Glass Fits Elevator?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_glass_fits_elevator" value="glass_fits_elevator" door_type="<?php echo basename(__FILE__, '.php'); ?>" checked>
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                              </div>
                           <div class="col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
         <div class="col-xs-12 col-sm-12">
                        <div class="row">
          <!--  lucky-->
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Color Waiver ?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_color_waiver" id="checkbox_tgpd_glass_reminders_add_color_waiver" value="add_color_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                                 </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
         <div class="col-sm-12">
                        <div class="row">
         <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Damage Waiver ?</h5>
            <label class="switch-light switch-candy">
         <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_damage_waiver_pactio" id="checkbox_tgpd_glass_reminders_add_damage_waiver" value="damage_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>" >
         <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
         </span>
            </label>
         </div>
          <div class="col-sm-12 col-md-12 dwmargin">
            <div class="row">
              <div class="col-xs-12">
              <fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section_for_select" style="display: none;" >
                <select class="form-control" id="select_tgpd_glass_damage_waiver" name="select_tgpd_glass_damage_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                   <option value="not_applicable" >Damage Waiver</option>
                  <option value="Removal/Reinstall of Glass Currently Installed">Removal/Reinstall of Glass Currently Installed </option>
                  <option value="Handling of Customers Materials">Handling of Customers Materials </option>
                  <option value="Adjacent Glass">Adjacent Glass</option>
                  <option value="select_tgpd_glass_damage_waiver_for_text">Other</option>
                </select>
                </fieldset>
              </div>
            </div>
          </div>
        <div class="col-sm-12 col-md-12 leftalign dwmargin">
          <div class="row">
            <div class="col-sm-12">
              <fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section" style="display: none;">
                <input id="text_tgpd_glass_damage_waiver_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_damage_waiver_reminder_section">
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>
        




    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
          <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Disclaimers ?</h5>
              <label class="switch-light switch-candy">
                <input door_type="<?php echo basename(__FILE__, '.php'); ?>" type="checkbox" name="checkbox_tgpd_glass_reminders_add_disclamers_pactio" id="checkbox_tgpd_glass_reminders_add_disclamers" value="disclamers" >
                  <span>
                    <span>No</span>
                    <span>Yes</span>
                    <a></a>
                  </span>
              </label>
          </div>

          <div class="col-sm-12 col-md-12 dwmargin">
            <div class="row">
              <div class="col-xs-12">
              <fieldset id="fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers" style="display: none;" >
                <select door_type="<?php echo basename(__FILE__, '.php'); ?>"  class="form-control" id="select_tgpd_glass_disclamers" name="select_tgpd_glass_disclamers">
                   <option value="not_applicable">Disclaimers</option>
                  <option value="Wood Bead">Wood Bead</option>
                  <option value="Painted Frames (AFTERMARKET)">Painted Frames (AFTERMARKET)</option>
                  <option value="TBD">TBD</option>
                  <option value="select_tgpd_glass_disclamers_for_text">Other</option>
                </select>
                </fieldset>
              </div>
            </div>
          </div>
        
        <div class="col-sm-12 col-md-12 leftalign dwmargin">
          <div class="row">
            <div class="col-sm-12 damage_comment">
              <fieldset id="fieldset_tgpd_glass_disclamers_reminder_section" style="display: none;">
                <input id="text_tgpd_glass_disclamers_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_disclamers_reminder_section">
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Inside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="text_lift_inside_position" value="lift_inside" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                    <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_inside_section1" style="display: none;">
                           <input id="text_tgpd_glass_inside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_inside_lift_with_glass_type" type="text" placeholder="Lift Inside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Outside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="text_lift_outside_position" value="lift_outside" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                    <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_outside" style="display: none;">
                           <input id="text_tgpd_glass_outside_lift_with_glass_type" class="form-control text-num"  name="text_tgpd_glass_outside_lift_with_glass_type" type="text" placeholder="Lift Outside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div></div>
               </div>
            </div>
            </div>
            </div>
            </div>
</div>