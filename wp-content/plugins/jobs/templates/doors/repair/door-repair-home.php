<?php include 'dopr_repair_nav.php'; ?>
<?php include 'select-repair.php' ?>

<?php include 'glass/type_a_single.php' ?>
<?php include 'glass/type_a_single_right.php' ?>
<?php include 'glass/type_a_pair.php' ?>
<?php include 'glass/type_bp_single.php' ?>
<?php include 'glass/type_bp_single_right.php' ?>
<?php include 'glass/type_bp_pair.php' ?>
<?php include 'glass/type_p_single.php' ?>
<?php include 'glass/type_p_single_right.php' ?>
<?php include 'glass/type_p_pair.php' ?>
<?php include 'glass/type_f_single.php' ?>
<?php include 'glass/type_f_single_right.php' ?>
<?php include 'glass/type_f_pair.php' ?>
<?php include 'glass/type_wp_single.php' ?>
<?php include 'glass/type_wp_single_right.php' ?>
<?php include 'glass/type_wp_pair.php' ?>

<?php include 'glass_replacement/type_a_single.php' ?>
<?php include 'glass_replacement/type_bp_single.php' ?>
<?php include 'glass_replacement/type_p_single.php' ?>
<?php include 'glass_replacement/type_f_single.php' ?>
<?php include 'glass_replacement/type_wp_single.php' ?>

<?php include 'aluminum_storefront/type_center_hung_single.php' ?>
<?php include 'aluminum_storefront/type_center_hung_single_right.php' ?>
<?php include 'aluminum_storefront/type_center_hung_single_pair.php' ?>
<?php include 'aluminum_storefront/type_offset_pivots_single.php' ?>
<?php include 'aluminum_storefront/type_offset_pivots_single_right.php' ?>
<?php include 'aluminum_storefront/type_offset_pivots_single_pair.php' ?>
<?php include 'aluminum_storefront/type_continuous_hinge_single.php' ?>
<?php include 'aluminum_storefront/type_continuous_hinge_single_right.php' ?>
<?php include 'aluminum_storefront/type_continuous_hinge_single_pair.php' ?>

<?php include 'shower_door/sdbg_left_2_hing.php' ?>
<?php include 'shower_door/sdbg_left_3_hing.php' ?>
<?php include 'shower_door/sdbg_right_3_hing.php' ?>
<?php include 'shower_door/sdbg_right_2_hing.php' ?>

<?php include 'hollow_metal/type_butt_hinge_single.php' ?>
<?php include 'hollow_metal/type_butt_hinge_single_pair.php' ?>
<?php include 'hollow_metal/type_butt_hinge_single_right.php' ?>
<?php include 'hollow_metal/type_continuous_hinge_single_pair.php' ?>
<?php include 'hollow_metal/type_continuous_hinge_single_right.php' ?>
<?php include 'hollow_metal/type_continuous_hinge_single.php' ?>


  <?php include dirname(__FILE__).'/../all/upload_picture_generic.php' ?>
  <?php include dirname(__FILE__).'/../all/upload_sketch_generic.php' ?>