<div class="form-group">
  <div class="row">
    <div class="col-sm-6">
      <select id="select-repair-door-category" class="form-control select-door-category">
			<option value="not_applicable" selected ="selected">Select Category</option>
			<option value="gdr">All Glass Door - Parts Repair</option>
			<option value="gdn">All Glass Door - Broken Glass</option>
			<option value="sfd">Storefront Door - Parts Repair</option>
			<option value="wmd">Hollow Metal Door - Parts Repair</option>
			<option value="sdbg">Shower Door - Broken Glass</option>
      </select>
    </div>
    <div class="col-sm-6">
      <select id="select-repair-door-type" class="form-control select-door-type">
	<option value="not_applicable" selected ="selected">Select type</option>
	<option value="a-single">A-Single</option>
	<option value="a-single-opp">A-Single</option>
	<option value="a-pair">A-Pair</option>
	<option value="f-single">F-Single</option>
	<option value="f-single-opp">F-Single</option>
	<option value="f-pair">F-Pair</option>
	<option value="bp-single">BP-Single</option>
	<option value="bp-single-opp">BP-Single</option>
	<option value="bp-pair">BP-Pair</option>
	<option value="p-single">P-Single</option>
	<option value="p-single-opp">P-Single</option>
	<option value="p-pair">P-Pair</option>
	<option value="wp-single">WP-Single</option>
	<option value="wp-single-opp">WP-Single</option>
	<option value="wp-pair">WP-Pair</option>
      </select>
    </div>
    <div class="col-sm-4 hidden_repair_sub_type" style="display:none">
      <select id="select-repair-door-subtype" class="form-control select-door-subtype">
	<option value="not_applicable"  selected>Select Subtype</option>
	<option value="single">Single</option>
	<option value="single_right">Single</option>
	<option value="pair">Pair</option>
      </select>
    </div>
  </div>
</div>

