<div id="gdn-<?php echo basename(__FILE__, '.php'); ?>-container" class="container-fluid door-container hidden">
  <div class="form-group door-form-group">
    <div class="col-sm-5 door-diagram-col">
      <div class="door-diagram-img-wrapper">
        <img src="<?php echo plugins_url(); ?>/jobs/img/doors/repair/glass_replacement/type_f_single.jpg" class="door-diagram-img" />
      </div>
      <div class="col-sm-12">
         <label class="leftalign" for="textarea_tagd_gdn_f_single_notes">Instructions</label>
         <textarea id="textarea_tagd_gdn_f_single_notes" maxlength="300" class="form-control" maxlength="300" rows="3" autocomplete="on" placeholder="Instructions"></textarea>
      </div>
	   <div class="col-sm-12">
		<div class="type_f_sigle_image">


		</div>
      </div>
	  <div class="col-sm-12">
         <label class="leftalign" for="textarea_tagd_gdn_f_single_notes">Handle</label>
          <select id="all_replacement_heandle_type_f_single_door" class="form-control">
       <option value="not_applicable"  selected>Select Handle </option>
           <option value="pull_handle_up_push_bar_3_holes" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-1.jpg" id="1"> A Pull Handle, UP & Push Bar (3 Holes) </option>
          <option value="pull_handle_up_push_bar_3_holes_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-1-opp.jpg" id="8">B Pull Handle, UP & Push Bar (3 Holes) </option>
          <option value="pull_handle_down_push_bar_3_holes" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-2.jpg" id="2"> A Pull Handle, DOWN & Push Bar (3 Holes)</option>
           <option value="pull_handle_down_push_bar_3_holes_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-2-opp.jpg" id="9"> B Pull Handle, DOWN & Push Bar (3 Holes)</option>
          <option value="pull_handle_2_holes" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-3.jpg" id="3" >A Pull Handle (2 Holes)</option>
           <option value="pull_handle_2_holes_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-3-opp.jpg" id="10" >B Pull Handle (2 Holes)</option>
          <option value="pull_bar_2_holes" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-4.jpg" id="4"  >A Push Bar (2 Holes)</option>
          <option value="pull_bar_2_holes_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-4.jpg" id="11"  >B Push Bar (2 Holes)</option>
          <option value="ladder_pull_3_holes" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-5.jpg"  id="5">A Ladder Pull (3 Holes)</option>
           <option value="ladder_pull_3_holes_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-5-opp.jpg"  id="12">B Ladder Pull (3 Holes)</option>
          <option value="ladder_pull_2_holes" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-6.jpg" id="6"> A Ladder Pull (2 Holes)</option>
          <option value="ladder_pull_2_holes_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-6-opp.jpg" id="13"> B Ladder Pull (2 Holes)</option>
          <option value="single_pull_1_hole" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-7.jpg"  id="7">A Single Pull (1 Hole)</option>
          <option value="single_pull_1_hole_opp" imgurl="https://s3.amazonaws.com/sag-website-assets/Handle-7-opp.jpg"  id="14">B Single Pull (1 Hole)</option>
            </select>
      </div>
    </div>
    <div class="col-sm-7 door-fields-col">
      <div class="fields-sub-form-group form-group row">
        <div class="col-sm-6 fields-left-half">
		
		<div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <label>Location</label>
              </div>
              
            </div>
            <div class="row">
              <div class="col-sm-7">
                <input id="text_tagd_gdn_f_single_glass_location" class="form-control" type="text"/>
              </div>
              
            </div>
          </div>
		  
		  
		  
		  
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">A</span><label>Glass Width</label>
              </div>
              <div class="col-sm-1" style="visibility:hidden;">X</div>
              <div class="col-sm-5">
                <span class="letter">B</span><label>Glass Height</label>
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input id="text_tagd_gdn_f_single_glass_width" class="form-control" type="text"/>
              </div>
              <div class="col-sm-1" style="">X</div>
              <div class="col-sm-5">
                <input id="text_tagd_gdn_f_single_glass_height" class="form-control" type="text"/>
              </div>
            </div>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">C/D</span><label>Glass Color/Thickness</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="text_tagd_gdn_f_single_glass_thickness" class="form-control" >
              <option value="not_applicable" selected>Select Thickness</option>
              <option value='3/8" Clear'>3/8" Clear</option>
              <option value='3/8" Bronze'>3/8" Bronze</option>
              <option value='3/8" Gray'>3/8" Gray</option>
              <option value='3/8" Lami Clear'>3/8" Lami Clear</option>
              <option value='3/8" Lami Bronze'>3/8" Lami Bronze</option>
              <option value='3/8" Lami Gray'>3/8" Lami Gray</option>
              <option value='3/8" Acid Etched'>3/8" Acid Etched</option>
              <option value='3/8" Low Iron'>3/8" Low Iron</option>

              <option value='1/2" Clear'>1/2" Clear </option>
              <option value='1/2" Bronze'>1/2" Bronze </option>
			  <option value='1/2" Gray'>1/2" Gray</option>
              <option value='1/2" Lami Clear'>1/2" Lami Clear</option>
			   <option value='1/2" Lami Bronze'>1/2" Lami Bronze</option>
              <option value='1/2" Lami Gray'>1/2" Lami Gray</option>
			   <option value='1/2" Acid Etched'>1/2" Acid Etched</option>
              <option value='1/2" Low Iron'>1/2" Low Iron</option>


			  <option value='5/8" Clear'>5/8" Clear</option>
              <option value='5/8" Bronze'>5/8" Bronze </option>
			    <option value='5/8" Gray'>5/8" Gray</option>
			  <option value='5/8" Lami Clear'>5/8" Lami Clear</option>
              <option value='5/8" Lami Bronze'>5/8" Lami Bronze</option>
			   <option value='5/8" Lami Clear'>5/8" Lami Gray</option>
              <option value='5/8" Acid Etched'>5/8" Acid Etched</option>
			    <option value='5/8" Low Iron'>5/8" Low Iron </option>


              <option value='3/4" Clear'>3/4" Clear</option>
			  <option value='3/4" Bronze'>3/4" Bronze</option>
              <option value='3/4" Gray'>3/4" Gray</option>
			   <option value='3/4" Lami Clear'>3/4" Lami Clear</option>
              <option value='3/4" Lami Bronze'>3/4" Lami Bronze</option>
			    <option value='3/4" Lami Gray'>3/4" Lami Gray</option>
              <option value='3/4" Acid Etched'>3/4" Acid Etched</option>
			  <option value='3/4" Low Iron '>3/4" Low Iron </option>


              <option value='1" Clear'>1" Clear</option>
			   <option value='1" Bronze'>1" Bronze</option>
              <option value='1" Gray'>1" Gray</option>
			   <option value='1" Lami Clear'>1" Lami Clear</option>
              <option value='1" Lami Bronze'>1" Lami Bronze</option>
			  <option value='1" Lami Gray'>1" Lami Gray</option>
              <option value='1" Acid Etched'>1" Acid Etched</option>
			   <option value='1" Low Iron'>1" Low Iron</option>
            </select>
          </div>
          <!--<div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">D</span><label>Glass Color</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="select_tagd_gdn_f_single_glass_color" class="form-control">
              <option value="not_applicable"  selected>Select Glass Color</option>
              <option value="Clear">Clear</option>
              <option value="Bronze">Bronze </option>
              <option value="Gray">Gray</option>
              <option value="Custom">Custom</option>
            </select>
          </div>-->
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">E</span><label>Total Glass Door Height</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <input id="text_tagd_gdn_f_single_overall_door_height" type="text" class="form-control">
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">K</span><label>Door Opening</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <input type="text" id="text_tagd_gdn_f_single_door_opening" class="form-control" />
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">L</span><label>Type of Set</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="select_tagd_gdn_f_single_type_of_set" class="form-control">
              <option value="not_applicable"  selected>Select Type Set</option>
              <option value='Dry'>Dry</option>
              <option value='Rockite'>Rockite</option>
              <option value='Epoxy'>Epoxy</option>
              <option value='Silicone'>Silicone</option>
            </select>
          </div>
          <!-- <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">M</span><label>Type of Handles</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="select_tagd_gdn_f_single_type_of_handle" class="form-control">
              <option value="not_applicable"  selected>Select Handles Type</option>
              <option value="option-1">Option 1</option>
            </select>
          </div> -->
        </div>
        <div class="col-sm-6 fields-right-half">
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">N</span><label>Hole Location</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="select_tagd_gdn_f_single_hole_location" class="form-control">
              <option value="not_applicable"  selected>Select Hole Location</option>
              <option value="option-1">Option 1</option>
            </select>
          </div>
                    <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">O</span><label>Hole Size</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="text_tagd_gdn_f_single_hole_size" class="form-control">
              <option value="not_applicable" selected>Select Hole Size</option>
              <option value='1/2"'>1/2"</option>
              <option value='5/8"'>5/8"</option>
              <option value='3/4"'>3/4"</option>
              <option value='1"'>1"</option>
              <option value='Custom'>Custom</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">P</span><label>Patch Fitting Manufacturer</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="select_tagd_gdn_f_single_patch_fitting_manufacturer" class="form-control">
              <option value="not_applicable"  selected>Select Manufacturer</option>
              <option value="Dorma">Dorma</option>
              <option value="Casma">Casma</option>
              <option value="CRL">CRL</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Q</span><label>Bottom Patch Lock Manufacturer</label>
              </div>
              <div class="col-sm-5">
              </div>
            </div>
            <select id="select_tagd_gdn_f_single_bottom_patch_lock_manufacturer" class="form-control">
              <option value="not_applicable" disabled selected>Bottom Patch Lock Manufacturer</option>
              <option value='Adams Rite'>Adams Rite</option>
              <option value='Dorma'>Dorma</option>
              <option value='CRL'>CRL</option>
            </select>
          </div>
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">R</span><label>Lock Notch Width</label>
              </div>
              <div class="col-sm-1" style="visibility:hidden;">X</div>
              <div class="col-sm-5">
                <span class="letter">S</span><label>Lock Notch Height</label>
              </div>
            </div>
            <div class="col-sm-5">
              <input type="text" id="text_tagd_gdn_f_single_lock_notch_width" class="form-control">
            </div>
            <div class="col-sm-1" style="">X</div>
            <div class="col-sm-5">
              <input type="text" id="text_tagd_gdn_f_single_lock_notch_height" class="form-control">
            </div>
          </div>
        </div>
      </div>

	  <div class="fields-sub-form-group form-group row">
        <div class="hendleshow_single_a_1 hendleshow_single_a">
          <div class="col-sm-6 fields-left-half">
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_f_a" id="handlea_label" > <span class="letter">A</span>
                  <label>Handle A</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="handlea" value="" class="form-control handle_f_a"  style="display:none"/>
            </div>
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_f_b"  id="handleb_label"> <span class="letter">B</span>
                  <label>Handle B</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="handleb" value="" class="form-control handle_f_b" style="display:none" />
            </div>
          </div>
          <div class="col-sm-6 fields-right-half">
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_f_c" id="handlec_label"> <span class="letter">C</span>
                  <label>Handle C</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="handlec" value="" class="form-control handle_f_c" style="display:none" />
            </div>
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_f_d" id="handled_label"  > <span class="letter">D</span>
                  <label>Handle D</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="handled" value="" class="form-control handle_f_d"  style="display:none">
            </div>
			<div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 hendle_f_e" id="handlee_label"> <span class="letter">E</span>
                  <label>Handle E</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="handlee" value="" class="form-control hendle_f_e" style="display:none" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="row" style="clear:both;">
    <div class="col-xs-6">
  <div class="form-group">
            <div class="col-xs-12">
               <label class="leftalign">Glass materials</label>
            </div>
         </div>
<div class="form-group">
  <div class="col-xs-12 leftalign">
    <div class="panel panel-default">
      <div class="panel-body chaulksection materials-section" >
         <div class="form-group addqty"  id="add_quntity_0">
                    <div  class="add_quntity" id="1" >
                      <div class="myCustom" id="add_id_0_0">
                        <fieldset class="col-xs-12 margin-top_culk_minus" id="fieldset_tgpd_glass_materials_caulk">
                          <div class="row add">
                            <div class="col-xs-3 col-md-4 col-lg-4 gls_caulk">
                              <label class="leftalign add" for="text_tgpd_glass_materials_caulk_amount">Amount</label>
                              <div class="input-group">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_caulk_amount_0_0">
                                    <span class="glyphicon glyphicon-minus">                                 </span>
                                </button>
                              </span>
                              <input type="text" id="text_tgpd_glass_materials_caulk_amount_0_0" value="0" name="text_tgpd_glass_materials_caulk_amount_0_0" class="form-control input-number-plus-minus qty-align-new user-success" min="0" max="100" /><span class="input-group-btn">
                              <button data-field="text_tgpd_glass_materials_caulk_amount_0_0" type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"><span class="glyphicon glyphicon-plus">                             
                              </span>
                            </button>
                          </span>
                        </div>
                      </div>
                      <div class="col-xs-8 col-md-6 col-lg-6 add">
                        <label class="leftalign add" for="select_tgpd_glass_materials_caulk_type">CAULK</label>
                        <select id="select_tgpd_glass_materials_caulk_type_0_0" class="form-control add user-success">
                          <option value="not_applicable">Caulk type</option>
                          <option value="799-Clear (T)">799-Clear (T)</option>
                          <option value="999-A-Clear (T)">999-A-Clear (T)</option>
                          <option value="790-Black (S)">790-Black (S)</option>
                          <option value="790-Black (T)">790-Black (T)</option>
                          <option value="795-Black (S)">795-Black (S)</option>
                          <option value="795-Black (T)">795-Black (T)</option>
                          <option value="995-Black (S)">995-Black (S)</option>
                          <option value="999-A-Black (T)">999-A-Black (T)</option>
                          <option value="Dymonic-Black (S)">Dymonic-Black (S)</option>
                          <option value="Dymonic-Black (T)">Dymonic-Black (T)</option>
                          <option value="790-Bronze (S)">790-Bronze (S)</option>
                          <option value="790-Bronze (T)">790-Bronze (T)</option>
                          <option value="795-Bronze (S)">795-Bronze (S)</option>
                          <option value="795-Bronze (T)">795-Bronze (T)</option>
                          <option value="999-A-Bronze (T)">999-A-Bronze (T)</option>
                          <option value="Dymonic-Bronze (S)">Dymonic-Bronze (S)</option>
                          <option value="Dymonic-Bronze (T)">Dymonic-Bronze (T)</option>
                          <option value="795-Anodized Aluminum (S)">795-Anodized Aluminum (S)</option>
                          <option value="795-Anodized Aluminum (T)">795-Anodized Aluminum (T)</option>
                          <option value="999-A-Aluminum (T)">999-A-Aluminum (T)</option>
                          <option value="Dymonic-Anodized Aluminum (S)">Dymonic-Anodized Aluminum (S)</option>
                          <option value="Dymonic-Anodized Aluminum (T)">Dymonic-Anodized Aluminum (T)</option>
                          <option value="790-White (S)">790-White (S)</option>
                          <option value="790-White (T)">790-White (T)</option>
                          <option value="795-White (S)">795-White (S)</option>
                          <option value="795-White (T)">795-White (T)</option>
                          <option value="995-White (S)">995-White (S)</option>
                          <option value="999-A-White (T)">999-A-White (T)</option>
                          <option value="Dymonic-White (S)">Dymonic-White (S)</option>
                          <option value="Dymonic-White (T)">Dymonic-White (T)</option>
                          <option value="Rockite (Bucket)">Rockite (Bucket)</option>
                          <option value="Rockite (Gallon)">Rockite (Gallon)</option>
                          <option value="Epoxy (Gallon)">Epoxy (Gallon)</option>
                          <option value="Putty White (Pint) - Wood">Putty White (Pint) - Wood</option>
                          <option value="Putty White (Gallon) - Wood">Putty White (Gallon) - Wood</option>
                          <option value="Putty Grey (Pint) - Metal">Putty Grey (Pint) - Metal</option>
                          <option value="Putty Grey (Gallon) - Metal">Putty Grey (Gallon) - Metal</option>
                          <option value="790-Adobe Tan (S)">790-Adobe Tan (S)</option>
                          <option value="790-Adobe Tan (T)">790-Adobe Tan (T)</option>
                          <option value="790-Blue Spruce (S)">790-Blue Spruce (S)</option>
                          <option value="790-Blue Spruce (T)">790-Blue Spruce (T)</option>
                          <option value="790-Charcoal (S)">790-Charcoal (S)</option>
                          <option value="790-Charcoal (T)">790-Charcoal (T)</option>
                          <option value="790-Dusty Rose (S)">790-Dusty Rose (S)</option>
                          <option value="790-Dusty Rose (T)">790-Dusty Rose (T)</option>
                          <option value="790-Gray (S)">790-Gray (S)</option>
                          <option value="790-Gray (T)">790-Gray (T)</option>
                          <option value="790-Limestone (S)">790-Limestone (S)</option>
                          <option value="790-Limestone (T)">790-Limestone (T)</option>
                          <option value="790-Natural Stone (S)">790-Natural Stone (S)</option>
                          <option value="790-Natural Stone (T)">790-Natural Stone (T)</option>
                          <option value="790-Precast White (S)">790-Precast White (S)</option>
                          <option value="790-Precast White (T)">790-Precast White (T)</option>
                          <option value="790-Rustic Brick (S)">790-Rustic Brick (S)</option>
                          <option value="790-Rustic Brick (T)">790-Rustic Brick (T)</option>
                          <option value="790-Sandstone (S)">790-Sandstone (S)</option>
                          <option value="790-Sandstone (T)">790-Sandstone (T)</option>
                          <option value="795-Adobe Tan (S)">795-Adobe Tan (S)</option>
                          <option value="795-Adobe Tan (T)">795-Adobe Tan (T)</option>
                          <option value="795-Blue Spruce (S)">795-Blue Spruce (S)</option>
                          <option value="795-Blue Spruce (T)">795-Blue Spruce (T)</option>
                          <option value="795-Champagne (S)">795-Champagne (S)</option>
                          <option value="795-Champagne (T)">795-Champagne (T)</option>
                          <option value="795-Charcoal (S)">795-Charcoal (S)</option>
                          <option value="795-Charcoal (T)">795-Charcoal (T)</option>
                          <option value="795-Dusty Rose (S)">795-Dusty Rose (S)</option>
                          <option value="795-Dusty Rose (T)">795-Dusty Rose (T)</option>
                          <option value="795-Gray (S)">795-Gray (S)</option>
                          <option value="795-Gray (T)">795-Gray (T)</option>
                          <option value="795-Limestone (S)">795-Limestone (S)</option>
                          <option value="795-Limestone (T)">795-Limestone (T)</option>
                          <option value="795-Natural Stone (S)">795-Natural Stone (S)</option>
                          <option value="795-Natural Stone (T)">795-Natural Stone (T)</option>
                          <option value="795-Rustic Brick (S)">795-Rustic Brick (S)</option>
                          <option value="795-Rustic Brick (T)">795-Rustic Brick (T)</option>
                          <option value="795-Sandstone (S)">795-Sandstone (S)</option>
                          <option value="795-Sandstone (T)">795-Sandstone (T)</option>
                          <option value="995-Gray (S)">995-Gray (S)</option>
                          <option value="999-A-Light Bronze (T)">999-A-Light Bronze (T)</option>
                          <option value="Dymonic-Almond (S)">Dymonic-Almond (S)</option>
                          <option value="Dymonic-Almond (T)">Dymonic-Almond (T)</option>
                          <option value="Dymonic-Aluminum Stone (S)">Dymonic-Aluminum Stone (S)</option>
                          <option value="Dymonic-Aluminum Stone (T)">Dymonic-Aluminum Stone (T)</option>
                          <option value="Dymonic-Beige (S)">Dymonic-Beige (S)</option>
                          <option value="Dymonic-Beige (T)">Dymonic-Beige (T)</option>
                          <option value="Dymonic-Buff (S)">Dymonic-Buff (S)</option>
                          <option value="Dymonic-Buff (T)">Dymonic-Buff (T)</option>
                          <option value="Dymonic-Dark Bronze (S)">Dymonic-Dark Bronze (S)</option>
                          <option value="Dymonic-Dark Bronze (T)">Dymonic-Dark Bronze (T)</option>
                          <option value="Dymonic-Gray (S)">Dymonic-Gray (S)</option>
                          <option value="Dymonic-Gray (T)">Dymonic-Gray (T)</option>
                          <option value="Dymonic-Gray Stone (S)">Dymonic-Gray Stone (S)</option>
                          <option value="Dymonic-Gray Stone (T)">Dymonic-Gray Stone (T)</option>
                          <option value="Dymonic-Hartford Green (S)">Dymonic-Hartford Green (S)</option>
                          <option value="Dymonic-Hartford Green (T)">Dymonic-Hartford Green (T)</option>
                          <option value="Dymonic-Ivory (S)">Dymonic-Ivory (S)</option>
                          <option value="Dymonic-Ivory (T)">Dymonic-Ivory (T)</option>
                          <option value="Dymonic-Light Bronze (S)">Dymonic-Light Bronze (S)</option>
                          <option value="Dymonic-Light Bronze (T)">Dymonic-Light Bronze (T)</option>
                          <option value="Dymonic-Limestone (S)">Dymonic-Limestone (S)</option>
                          <option value="Dymonic-Limestone (T)">Dymonic-Limestone (T)</option>
                          <option value="Dymonic-Natural Clay (S)">Dymonic-Natural Clay (S)</option>
                          <option value="Dymonic-Natural Clay (T)">Dymonic-Natural Clay (T)</option>
                          <option value="Dymonic-Off White (S)">Dymonic-Off White (S)</option>
                          <option value="Dymonic-Off White (T)">Dymonic-Off White (T)</option>
                          <option value="Dymonic-Precast White (S)">Dymonic-Precast White (S)</option>
                          <option value="Dymonic-Precast White (T)">Dymonic-Precast White (T)</option>
                          <option value="Dymonic-Redwood Tan (S)">Dymonic-Redwood Tan (S)</option>
                          <option value="Dymonic-Redwood Tan (T)">Dymonic-Redwood Tan (T)</option>
                          <option value="Dymonic-Sandalwood (S)">Dymonic-Sandalwood (S)</option>
                          <option value="Dymonic-Sandalwood (T)">Dymonic-Sandalwood (T)</option>
                          <option value="Dymonic-Stone (S)">Dymonic-Stone (S)</option>
                          <option value="Dymonic-Stone (T)">Dymonic-Stone (T)</option>
                        </select>
                      </div>
                      <div class="col-xs-1">
                        <a title="<?php echo basename(__FILE__, '.php'); ?>" href="javascript:void(0);" class="btn_pluse btn12_patio" >+</a>
                      </div>
                    </div>
                  </fieldset>
                  </div>


                    </div>
                     <div class="add_tap_amount" id="1">
                      <div class="myCustom1" id="add_id_tap_0">
                        <div class="col-xs-12">
                          <div class="row plus-minus-btn-row">
                            <div class="col-xs-12">
                              <fieldset id="fieldset_tgpd_glass_materials_tape">
                                <div class="row">
                                  <div class="col-xs-3 col-md-4 gls_caulk">
                                    <label class="leftalign">Amount</label>
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_tape_amount_0_0">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                      </span>
                                      <input type="text" id="text_tgpd_glass_materials_tape_amount_0_0" value="0" name="text_tgpd_glass_materials_tape_amount_0_0" class="form-control input-number-plus-minus qty-align-new user-success" min="0" max="100"><span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="text_tgpd_glass_materials_tape_amount_0_0">
                                          <span class="glyphicon glyphicon-plus"></span>
                                        </button></span>
                                      </div>
                                    </div>
                                    <div class="col-xs-8 col-md-6 add">
                                      <label class="leftalign">TAPE</label>
                                      <select id="select_tgpd_glass_materials_tape_type_0_0" class="form-control">
                                        <option value="not_applicable">Tape type</option>
                                        <option value="1_over_8_440">1/8 440 tape</option>
                                        <option value="1_over_4_440">1/4 440 tape</option>
                                        <option value="1_over_2_440">1/2 440 tape</option>
                                        <option value="3_over_4_440">3/4 440 tape</option>
                                        <option value="foam">Foam tape</option>
                                        <option value="double_faced">Double faced tape</option>
                                        <option value="cladding">Cladding tape</option>
                                      </select>
                                    </div> 
                                    <div class="col-xs-1">
                                      <a title="<?php echo basename(__FILE__, '.php'); ?>" href="javascript:void(0);" class="btn_pluse btn11_patio" >+</a>
                                    </div> 
                                  </div>
                                   </fieldset>
                                 </div>
                               </div>
                             </div>
                           </div>

                     </div>
                     <div class="add_tap_scaff" id="1">
                       <div class="myCustom1" id="add_id_scaff_0">
                        <div class="col-xs-12 add">
                          <div class="row add plus-minus-btn-row">
                            <div class="col-xs-12 add">
                              <fieldset id="fieldset_tgpd_glass_materials_scaffolding">
                                <div class="row add">
                                  <div class="col-xs-3 col-md-4  gls_caulk">
                                   <label class="leftalign smaller" for="select_tgpd_glass_materials_quantity">Quantity</label>  <div class="input-group col-xs-12">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="select_tgpd_glass_materials_quantity_0_0">
                                        <span class="glyphicon glyphicon-minus"></span>
                                      </button></span>
                                       <input id="select_tgpd_glass_materials_quantity_0_0" name="select_tgpd_glass_materials_quantity_0_0" type="text" class="form-control text-num input-number-plus-minus qty-align-new user-success" autocomplete="on" min="0" max="100" value="0">
                                       <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_quantity_0_0">
                                          <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                      </span>
                                    </div> 
                                  </div>
                                  <div class="col-xs-8 col-md-6 add">
                                    <label class="leftalign add" for="select_tgpd_glass_materials_scaffolding_type">EQUIPMENT</label>
                                    <select id="select_tgpd_glass_materials_scaffolding_type_0_0" class="form-control add user-success">
                                      <option value="not_applicable">Equipment type</option>
                                      <option value="Swing Stage (SAG)">Swing Stage (SAG)</option>
                                      <option value="Swing Stage (SUB)">Swing Stage (SUB)</option>
                                      <option value="Scaffolding (SAG)">Scaffolding (SAG)</option>
                                      <option value="Scaffolding-HALF (SAG)">Scaffolding-HALF (SAG)</option
                                        ><option value="Scaffolding (SUB, RENT)">Scaffolding (SUB, RENT)</option>
                                        <option value="Scaffolding (SUB, SETUP)">Scaffolding (SUB, SETUP)</option>
                                        <option value="Scaffolding-BAKER (SUB.RENT)">Scaffolding-BAKER (SUB.RENT)</option>
                                        <option value="Scaffolding-BAKER (SUB.SETUP)">Scaffolding-BAKER (SUB.SETUP)</option>
                                        <option value="40FT Articulating Lift">40FT Articulating Lift</option>
                                        <option value="60FT Articulating Lift">60FT Articulating Lift</option>
                                        <option value="80FT Articulating Lift">80FT Articulating Lift</option>
                                        <option value="120FT Articulating Lift">120FT Articulating Lift</option>
                                        <option value="135FT Articulating Lift">135FT Articulating Lift</option>
                                        <option value="40FT STICK BOOM">40FT STICK BOOM</option>
                                        <option value="60FT STICK BOOM">60FT STICK BOOM</option>
                                        <option value="80FT STICK BOOM">80FT STICK BOOM</option>
                                        <option value="120FT STICK BOOM">120FT STICK BOOM</option>
                                        <option value="135FT STICK BOOM">135FT STICK BOOM</option>
                                        <option value="19FT Scissor Lift">19FT Scissor Lift</option>
                                        <option value="24-26FT Scissor Lift">24-26FT Scissor Lift</option>
                                        <option value="30-35FT Scissor Lift">30-35FT Scissor Lift</option>
                                        <option value="39-40FT Scissor Lift">39-40FT Scissor Lift</option>
                                        <option value="25-27T 4WD Scissor Lift">25-27T 4WD Scissor Lift</option>
                                        <option value="36-49FT 4WD Scissor Lift">36-49FT 4WD Scissor Lift</option>
                                        <option value="Towable Boom">Towable Boom</option>
                                        <option value="Generator">Generator</option>
                                        <option value="Power Cup">Power Cup</option>
                                        <option value="Crane">Crane</option>
                                        <option value="Lift My Glass">Lift My Glass</option><option value="LULL LIFT">LULL LIFT</option>
                                        <option value="Fork Boom">Fork Boom</option>
                                        <option value="Chain Fall">Chain Fall</option>
                                        <option value="Traffic Control">Traffic Control</option>
                                        <option value="Street Closure Permit">Street Closure Permit</option>
                                        <option value="New Part- TEXT BOX">New Part- TEXT BOX</option>
                                      </select>
                                    </div>
                                    <div class="col-xs-1"><a href="javascript:void(0);" title="<?php echo basename(__FILE__, '.php'); ?>" class="btn_pluse btn14_patio" >+</a>
                                    </div>         
                                     </div>
                                   </fieldset>
                                 </div>
                               </div>
                             </div>
                           </div>
                     </div>
                     <div class="add_tap_channel" id="1">
                       <div class="myCustom1">
                        <div class="col-xs-12">
                          <div class="row add plus-minus-btn-row">
                            <div class="col-xs-12 add">
                              <fieldset id="fieldset_tgpd_glass_materials_channel">
                                <div class="row add">
                                <div class="col-xs-3 col-md-4  gls_caulk" >
                                  <label class="leftalign smaller" for="select_tgpd_glass_materials_channel_quantity">Quantity</label>
                                  <div class="input-group col-xs-12">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number-plus-minus bt_for_minus" data-type="minus" data-field="select_tgpd_glass_materials_channel_quantity_0_0">
                                        <span class="glyphicon glyphicon-minus">
                                        
                                      </span>
                                    </button>
                                  </span>
                                  <input id="select_tgpd_glass_materials_channel_quantity_0_0" name="select_tgpd_glass_materials_channel_quantity_0_0" type="text" class="form-control text-num input-number-plus-minusqty-align-new text-center user-success" autocomplete="on" min="0" max="100" value="0">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_channel_quantity_0_0">
                                      <span class="glyphicon glyphicon-plus">
                                      
                                    </span>
                                  </button>
                                </span>
                              </div> 
                            </div>
                            <div class=" col-xs-8 col-md-6 add" id="add_id_channel_0">
                              <label class="leftalign" style="">CHANNEL</label>
                              <select id="text_tgpd_glass_materials_channel_0_0" class="form-control add user-success">
                                <option value="not_applicable">Channel Type</option>
                                   <option value="J-Channel 5/8 Chrome">J-Channel 5/8 Chrome</option>
                                   <option value="J-Channel 5/8 Gold">J-Channel 5/8 Gold</option>
                                   <option value="J-Channel 5/8 Brushed Nickel">J-Channel 5/8 Brushed Nickel</option>
                                   <option value="J-Channel 5/8 Oil Rubbed Bronze">J-Channel 5/8 Oil Rubbed Bronze</option>
                                   <option value="J-Channel 3/8 Chrome">J-Channel 3/8 Chrome</option>
                                   <option value="J-Channel 3/8 Gold">J-Channel 3/8 Gold</option>
                                   <option value="J-Channel 3/8 Brushed Nickel">J-Channel 3/8 Brushed Nickel</option>
                                   <option value="J-Channel 3/8 Oil Rubbed Bronze">J-Channel 3/8 Oil Rubbed Bronze</option>
                                   <option value="L-Channel Chrome">L-Channel Chrome</option>
                                   <option value="L-Channel Gold">L-Channel Gold</option>
                                   <option value="L-Channel Brushed Nickel">L-Channel Brushed Nickel</option>
                                   <option value="L-Channel Oil Rubbed Bronze">L-Channel Oil Rubbed Bronze</option>
                                   <option value="U-Channel 3/4 x 3/4 Clear Anodized/ Satin">U-Channel 3/4 x 3/4 Clear Anodized/ Satin</option>

                                   <option value="U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin">U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 1 Clear Anodized/ Satin">U-Channel 1 x 1 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 1 Polished Brite Silver">U-Channel 1 x 1 Polished Brite Silver</option>
                                   <option value="U-Channel 1 x 1 Brushed Stainless">U-Channel 1 x 1 Brushed Stainless</option>
                                   <option value="U-Channel 1 x 2 Clear Anodized/ Satin">U-Channel 1 x 2 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 2 Polished Brite Silver">U-Channel 1 x 2 Polished Brite Silver</option>
                                   <option value="U-Channel 1 x 2 Brushed Stainless">U-Channel 1 x 2 Brushed Stainless</option>
                                   <option value="Tube - Chrome">Tube - Chrome</option>
                                   <option value="Tube - Brushed Nickel">Tube - Brushed Nickel</option>
                                   <option value="Tube - Oil Rubbed Bronze">Tube - Oil Rubbed Bronze</option>
                                   <option value="Glazing Rubber for U Channel, 1/4 Glass - Black">Glazing Rubber for U Channel, 1/4 Glass - Black</option>
                                   <option value="Glazing Rubber for U Channel, 3/8 Glass - Black">Glazing Rubber for U Channel, 3/8 Glass - Black</option>
                                   <option value="Glazing Rubber for U Channel, 1/2 Glass - Black">Glazing Rubber for U Channel, 1/2 Glass - Black</option>
                                 </select>
                               </div>
                               <div class=" col-xs-1"><a href="javascript:void(0);" title="<?php echo basename(__FILE__, '.php'); ?>" class="btn_pluse btn13_patio" >+</a>
                               </div>
                             </div>
                           </fieldset>
                         </div>
                       </div>
                     </div>
                   </div>
                    </div>
         </div>
         <div class="form-group">
      <div class="col-xs-12 col-lg-10">
               <label class="leftalign" for="text_tgpd_miscellaneous">Miscellaneous</label>
         <input id="text_tgpd_miscellaneous" class="form-control" type="text" autocomplete="on" maxlength="255" placeholder="Miscellaneous">
                &nbsp;
      </div>
         </div>
      </div>
         </div>
      </div>
      </div>
      </div>
      <div class="col-xs-6">
      <div class="form-group">
            <div class="col-xs-12 col-md-12">
               <label class="leftalign">Glass reminders</label>
            </div>
         </div>
      <div class="form-group">
            <div class="col-xs-12 col-sm-12">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header">Solar film?</h5>
            <label class="switch-light switch-candy">
               <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_solar_film" value="solar_film" door_type="<?php echo basename(__FILE__, '.php'); ?>">
               <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
               </span>
            </label>
         </div>
      </div>
                     </div>
                        <div class="col-xs-12">
                              <div class="form-group">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film1" style="display: none;">
                 <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_sag" value="sag" door_type="<?php echo basename(__FILE__, '.php'); ?>">SAG
                                       </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_customer" value="customer" door_type="<?php echo basename(__FILE__, '.php'); ?>">Customer
                                       </label>
                                    </div>
                  </div>
                                 </fieldset>
                              </div>
                           </div>
                           <div class="col-xs-12">
                              <div class="row">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film2" style="display: none;">
                                    <div class="form-group">
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_type" type="text" class="form-control"  placeholder="Film type">
                                       </div>
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_source" type="text" class="form-control" autocomplete="on" placeholder="Film source">
                                       </div>
                                    </div>
                                 </fieldset>
                              </div>
                           </div>

         <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header">Wet seal?</h5>
                              <label class="switch-light switch-candy" onclick="">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_wet_seal" value="wet_seal" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12">
            <div class="row">
                              <fieldset id="fieldset_tgpd_glass_reminders_wet_seal" style="display: none;">
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag">SAG
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_half_day">SAG Sub Half Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_full_day">SAG Sub Full Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_customer" value="customer">Customer
                                    </label>
                                 </div>
                              </fieldset>
            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Furniture to Move?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_furniture_to_move" value="furniture_to_move" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12 col-sm-12">
            <div class="row">
         <div class="col-sm-12">
         <fieldset id="fieldset_tgpd_glass_reminders_furniture_to_move" style="display: none;">
            <input id="text_tgpd_glass_reminders_furniture_to_move_comment" class="form-control" type="text"   placeholder="Comment">
         </fieldset>
         </div>
            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Walls/ceilings cut?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut" value="walls_or_ceilings_to_cut" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>

                           <div class="col-xs-12 col-sm-12">
                              <div class="row">
                                 <div class="">
                                    <div class="form-group">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1" style="display: none;">
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_sag" value="sag">SAG
                                             </label>
                                          </div>
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_customer" value="customer">Customer
                                             </label>
                                          </div>
                                       </fieldset>
                                    </div>
                                 </div>
                                 <div class="col-sm-12 col-md-12 leftalign">
                                    <div class="row">
               <div class="col-sm-12">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2" style="display: none;">
                                          <input id="text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment" class="form-control" type="text" autocomplete="on"  placeholder="Comment">
                                       </fieldset>
               </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
         <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Blind needs Removing?</h5>
            <label class="switch-light switch-candy">
         <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_blind_needs_removing" door_type="<?php echo basename(__FILE__, '.php'); ?>" value="blind_needs_removing">
         <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
         </span>
            </label>
         </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                              <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Glass Fits Elevator?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_glass_fits_elevator" value="glass_fits_elevator" door_type="<?php echo basename(__FILE__, '.php'); ?>" checked>
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                              </div>
                           <div class="col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
         <div class="col-xs-12 col-sm-12">
                        <div class="row">
          <!--  lucky-->
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Color Waiver ?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_color_waiver" id="checkbox_tgpd_glass_reminders_add_color_waiver" value="add_color_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                                 </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
         <div class="col-sm-12">
                        <div class="row">
         <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Damage Waiver ?</h5>
            <label class="switch-light switch-candy">
         <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_damage_waiver_pactio" id="checkbox_tgpd_glass_reminders_add_damage_waiver" value="damage_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>" >
         <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
         </span>
            </label>
         </div>
          <div class="col-sm-12 col-md-12 dwmargin">
            <div class="row">
              <div class="col-xs-12">
              <fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section_for_select" style="display: none;" >
                <select class="form-control" id="select_tgpd_glass_damage_waiver" name="select_tgpd_glass_damage_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                   <option value="not_applicable" >Damage Waiver</option>
                  <option value="Removal/Reinstall of Glass Currently Installed">Removal/Reinstall of Glass Currently Installed </option>
                  <option value="Handling of Customers Materials">Handling of Customers Materials </option>
                  <option value="Adjacent Glass">Adjacent Glass</option>
                  <option value="select_tgpd_glass_damage_waiver_for_text">Other</option>
                </select>
                </fieldset>
              </div>
            </div>
          </div>
        <div class="col-sm-12 col-md-12 leftalign dwmargin">
          <div class="row">
            <div class="col-sm-12">
              <fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section" style="display: none;">
                <input id="text_tgpd_glass_damage_waiver_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_damage_waiver_reminder_section">
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>
        




    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
          <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Disclaimers ?</h5>
              <label class="switch-light switch-candy">
                <input door_type="<?php echo basename(__FILE__, '.php'); ?>" type="checkbox" name="checkbox_tgpd_glass_reminders_add_disclamers_pactio" id="checkbox_tgpd_glass_reminders_add_disclamers" value="disclamers" >
                  <span>
                    <span>No</span>
                    <span>Yes</span>
                    <a></a>
                  </span>
              </label>
          </div>

          <div class="col-sm-12 col-md-12 dwmargin">
            <div class="row">
              <div class="col-xs-12">
              <fieldset id="fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers" style="display: none;" >
                <select door_type="<?php echo basename(__FILE__, '.php'); ?>"  class="form-control" id="select_tgpd_glass_disclamers" name="select_tgpd_glass_disclamers">
                   <option value="not_applicable">Disclaimers</option>
                  <option value="Wood Bead">Wood Bead</option>
                  <option value="Painted Frames (AFTERMARKET)">Painted Frames (AFTERMARKET)</option>
                  <option value="TBD">TBD</option>
                  <option value="select_tgpd_glass_disclamers_for_text">Other</option>
                </select>
                </fieldset>
              </div>
            </div>
          </div>
        
        <div class="col-sm-12 col-md-12 leftalign dwmargin">
          <div class="row">
            <div class="col-sm-12 damage_comment">
              <fieldset id="fieldset_tgpd_glass_disclamers_reminder_section" style="display: none;">
                <input id="text_tgpd_glass_disclamers_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_disclamers_reminder_section">
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Inside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="text_lift_inside_position" value="lift_inside" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                    <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_inside_section1" style="display: none;">
                           <input id="text_tgpd_glass_inside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_inside_lift_with_glass_type" type="text" placeholder="Lift Inside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Outside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="text_lift_outside_position" value="lift_outside" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                    <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_outside" style="display: none;">
                           <input id="text_tgpd_glass_outside_lift_with_glass_type" class="form-control text-num"  name="text_tgpd_glass_outside_lift_with_glass_type" type="text" placeholder="Lift Outside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div></div>
               </div>
            </div>
            </div>

            </div>
            </div>
</div>