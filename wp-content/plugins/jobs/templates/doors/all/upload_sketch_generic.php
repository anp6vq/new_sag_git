<div class="row">
   <div class="col-xs-12">
   <div id="div_upload_sketch_generic_01">
         <div class="col-xs-12">
         <label class="leftalign">Click below for sketch canvas</label>
         <div id="usg-canvas-wrap">
            <h5>Sketch Diagram or Fabrication.</h5>
            <div class="row">
               <div class="col-xs-12">
                  <div id="div_usg_canvas_holder">
                     <div id="div_usg_canvas_controls">
                        <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                           <a id="btn_usg_canvas_eraser_tool" class="btn btn-default" href="#canvas_usg_glass_sketch" role="button" data-tool="eraser" title="Eraser Tool">
                              <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                           </a>
                           <a id="btn_usg_canvas_marker_tool" class="btn btn-default" href="#canvas_usg_glass_sketch" role="button" data-tool="marker" title="Marker Tool">
                              <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                           </a>
                           <button id="btn_usg_clear_canvas" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Canvas">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                           </button>
                           <button id="btn_usg_canvas_to_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Canvas To Sketch File List">
                              <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                           </button>
                        </div>
                        <div class="btn-group" role="group" aria-label="Canvas Toolbar">
                           <button id="btn_usg_canvas_marker_size" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Size">
                              <span class="glyphicon glyphicon-text-size" aria-hidden="true"></span>
                              <span class="caret"></span>
                           </button>
                           <ul id="list_usg_canvas_marker_size" class="dropdown-menu" aria-labelledby="btn_usg_canvas_marker_size">
                              <li><a id="btn_usg_canvas_marker_size_3" href="#canvas_usg_glass_sketch" data-size="3">3 points</a></li>
                              <li><a id="btn_usg_canvas_marker_size_5" href="#canvas_usg_glass_sketch" data-size="5">5 points</a></li>
                              <li><a id="btn_usg_canvas_marker_size_10" href="#canvas_usg_glass_sketch" data-size="10">10 points</a></li>
                              <li><a id="btn_usg_canvas_marker_size_15" href="#canvas_usg_glass_sketch" data-size="15">15 points</a></li>
                           </ul>
                        </div>
                        <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                           <button id="btn_usg_canvas_marker_color" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Color">
                              <span class="glyphicon glyphicon-text-color" aria-hidden="true"></span>
                              <span class="caret"></span>
                           </button>
                           <ul id="list_usg_canvas_marker_color" class="dropdown-menu" aria-labelledby="btn_usg_canvas_marker_color">
                             <li><a href="#canvas_usg_glass_sketch" data-color="#000" style="width:100%;height:30px;background:#000;"></a></li>
                             <li><a href="#canvas_usg_glass_sketch" data-color="#555" style="width:100%;height:30px;background:#555;"></a></li>
                             <li><a href="#canvas_usg_glass_sketch" data-color="#f00" style="width:100%;height:30px;background:#f00;"></a></li>
                             <li><a href="#canvas_usg_glass_sketch" data-color="#ff0" style="width:100%;height:30px;background:#ff0;"></a></li>
                             <li><a href="#canvas_usg_glass_sketch" data-color="#CCC" style="width:100%;height:30px;background:#CCC;"></a></li>
                             <li><a href="#canvas_usg_glass_sketch" data-color="#fff" style="width:100%;height:30px;background:#fff;"></a></li>
                           </ul>
                        </div>
                     </div>
                     <canvas id="canvas_usg_glass_sketch" width="800" height="300"></canvas>
                  </div>
               </div>
            </div>
            <!--perhaps row here...with sketch images ready for upload so they show and hide with canvas-->
         </div>
      </div>
   </div>
   </div>
</div>
<div class="row">
   <div class="col-xs-12">
      <div id="div_upload_sketch_generic_02">
               <div class="col-xs-12">
                  <div id="div_usg_sketch_filelist_holder">
                     <div id="div_usg_sketch_filelist"></div>
                     <div id="div_usg_sketch_filelist_controls">
                        <div class="btn-group" role="group" aria-label="Sketch File List Toolbar">
                           <button id="btn_usg_clear_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Sketches">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                           </button>
                           <button id="btn_usg_upload_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Sketches">
                              <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                           </button>
                        </div>
                     </div>
                  </div>
               </div>
      </div>
   </div>
</div>