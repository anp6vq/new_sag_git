<div class="form-group">
   <div class="col-sm-6">
      <label class="leftalign" for="uploadFile">Select file(s) to upload</label>
      <div class="row">
         <div class="col-sm-9">
            <input placeholder="Choose File" disabled="disabled" class="form-control" />
         </div>
         <div class="col-sm-3">
            <div class="fileUpload btn btn-primary">
               <span>Choose file(s)</span>
               <input id="file_<?php echo $doorVar ?>_glass_picture" type="file" multiple class="upload" accept="image/*" />
            </div>
         </div>
      </div>
      <div class="row">
         <div class="col-sm-12">
            <div id="<?php echo $doorVar ?>_picture_filelist_holder">
               <div id="<?php echo $doorVar ?>_picture_filelist"></div>
               <div id="<?php echo $doorVar ?>_picture_filelist_controls">
                  <div class="btn-group" role="group" aria-label="Picture File List Toolbar">
                     <button id="btn_<?php echo $doorVar ?>_clear_picture_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Pictures">
                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                     </button>
                     <button id="btn_<?php echo $doorVar ?>_upload_picture_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Pictures">
                        <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                     </button>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   <div class="col-sm-6">
     <div class="form-group">
        <div class="col-sm-12">
           <label class="leftalign">Click below for sketch canvas</label>
           <div id="canvas-wrap">
              <h5>Sketch Diagram or Fabrication.</h5>
              <div class="row">
                 <div class="col-sm-12">
                    <div id="<?php echo $doorVar ?>_canvas_holder">
                       <div id="<?php echo $doorVar ?>_canvas_controls">
                          <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                             <a id="btn_<?php echo $doorVar ?>_canvas_eraser_tool" class="btn btn-default" href="#canvas_<?php echo $doorVar ?>_glass_sketch" role="button" data-tool="eraser" title="Eraser Tool">
                                <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                             </a>
                             <a id="btn_<?php echo $doorVar ?>_canvas_marker_tool" class="btn btn-default" href="#canvas_<?php echo $doorVar ?>_glass_sketch" role="button" data-tool="marker" title="Marker Tool">
                                <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                             </a>
                             <button id="btn_<?php echo $doorVar ?>_clear_canvas" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Canvas">
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                             </button>
                             <button id="btn_<?php echo $doorVar ?>_canvas_to_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Canvas To Sketch File List">
                                <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                             </button>
                          </div>
                          <div class="btn-group" role="group" aria-label="Canvas Toolbar">
                             <button id="btn_<?php echo $doorVar ?>_canvas_marker_size" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Size">
                                <span class="glyphicon glyphicon-text-size" aria-hidden="true"></span>
                                <span class="caret"></span>
                             </button>
                             <ul id="list_<?php echo $doorVar ?>_canvas_marker_size" class="dropdown-menu" aria-labelledby="btn_<?php echo $doorVar ?>_canvas_marker_size">
                                <li><a id="btn_<?php echo $doorVar ?>_canvas_marker_size_3" href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-size="3">3 points</a></li>
                                <li><a id="btn_<?php echo $doorVar ?>_canvas_marker_size_5" href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-size="5">5 points</a></li>
                                <li><a id="btn_<?php echo $doorVar ?>_canvas_marker_size_10" href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-size="10">10 points</a></li>
                                <li><a id="btn_<?php echo $doorVar ?>_canvas_marker_size_15" href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-size="15">15 points</a></li>
                             </ul>
                          </div>
                          <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                             <button id="btn_<?php echo $doorVar ?>_canvas_marker_color" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Color">
                                <span class="glyphicon glyphicon-text-color" aria-hidden="true"></span>
                                <span class="caret"></span>
                             </button>
                             <ul id="list_<?php echo $doorVar ?>_canvas_marker_color" class="dropdown-menu" aria-labelledby="btn_<?php echo $doorVar ?>_canvas_marker_color">
                               <li><a href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-color="#000" style="width:100%;height:30px;background:#000;"></a></li>
                               <li><a href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-color="#555" style="width:100%;height:30px;background:#555;"></a></li>
                               <li><a href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-color="#f00" style="width:100%;height:30px;background:#f00;"></a></li>
                               <li><a href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-color="#ff0" style="width:100%;height:30px;background:#ff0;"></a></li>
                               <li><a href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-color="#CCC" style="width:100%;height:30px;background:#CCC;"></a></li>
                               <li><a href="#canvas_<?php echo $doorVar ?>_glass_sketch" data-color="#fff" style="width:100%;height:30px;background:#fff;"></a></li>
                             </ul>
                          </div>
                       </div>
                       <canvas id="canvas_<?php echo $doorVar ?>_glass_sketch" width="800" height="300"></canvas>
                    </div>
                 </div>
              </div>
              <!--perhaps row here...with sketch images ready for upload so they show and hide with canvas-->
           </div>
        </div>
     </div>
     <div class="form-group">
        <div class="col-sm-12">
           <div id="<?php echo $doorVar ?>_sketch_filelist_holder">
              <div id="<?php echo $doorVar ?>_sketch_filelist"></div>
              <div id="<?php echo $doorVar ?>_sketch_filelist_controls">
                 <div class="btn-group" role="group" aria-label="Sketch File List Toolbar">
                    <button id="btn_<?php echo $doorVar ?>_clear_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Sketches">
                       <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                    </button>
                    <button id="btn_<?php echo $doorVar ?>_upload_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Sketches">
                       <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                    </button>
                 </div>
              </div>
           </div>
        </div>
     </div>
   </div>
</div>
