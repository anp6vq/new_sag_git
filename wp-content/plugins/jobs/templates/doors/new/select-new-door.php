<div class="form-group">
<div class="row">
  <div class="col-sm-6">
    <select id="select-new-door-category" class="form-control select-new-door-category">
		  <option value="not_applicable"  selected  >Select Category</option>
		  <option value="Gdoor"  attr-class="select-new-door-category_type1">Glass Doors</option>
		  <option value="Aludoor"  attr-class="select-new-door-category_type2" >Aluminum Doors</option>
		  <option value="hmdoor" attr-class="select-new-door-category_type3">Hollow Metal Doors</option>
      <option value="patio"  attr-class="select-new-door-category_type4">Patio Door</option>
    </select>
  </div>
  <div class="col-sm-6">
    <select id="select-new-door-type" class="form-control select-new-door-category_type1 door_hide_function" >
		  <option value="not_applicable" selected="selected" >Select Type</option>
		  <option value="Agndsing"  attr-div="new_door_container_glass_dooor_single">Single Door</option>
      <option value="Single Door w/ Sidelite Left"  attr-div="new_door_container_glass_dooor_single-2">Single Door w/ Sidelite Left</option>
      <option value="Single Door w/ Sidelite Right"  attr-div="new_door_container_glass_dooor_single-3">Single Door w/ Sidelite Right</option>
      <option value="Single Door w/ Sidelite Left & Right"  attr-div="new_door_container_glass_dooor_single-4">Single Door w/ Sidelite Left & Right</option>
      <option value="Agndpar" attr-div="new_door_container_glass_dooor_pair">Pair of Doors</option>
      <option value="Pair Doors w/ Sidelite Left"  attr-div="new_door_container_glass_dooor_pair-2">Pair Doors w/ Sidelite Left</option>
      <option value="Pair Doors w/ Sidelite Right"  attr-div="new_door_container_glass_dooor_pair-3">Pair Doors w/ Sidelite Right</option>
      <option value="Pair Doors w/ Sidelite Left & Right"  attr-div="new_door_container_glass_dooor_pair-4">Pair Doors w/ Sidelite Left & Right</option>
	</select>

	<select id="select-new-door-type" class="form-control select-new-door-category_type2 door_hide_function">
		  <option value="not_applicable"  selected>Select Type</option>
		  <option value="Aludnfsig" attr-div="new_door_container_aluminum_freme_dooor_single">Aluminum Doors and Frame with Transom Single</option>
		  <option value="Aludnfpar" attr-div="new_door_container_aluminum_freme_dooor_pair">Aluminum Doors and Frame with Transom Pair</option>
		  <option value="Alufsig" attr-div="new_door_container_aluminum_dooor_single">Aluminum Doors and Frame Single</option>
		  <option value="Alufpar" attr-div="new_door_container_aluminum_dooor_pair">Aluminum Doors and Frame Pair</option>
      <option value="Aluosig" attr-div="new_door_container_aluminum_door_only_single">Aluminum Doors - Door Only Single</option>
      <option value="Aluopar" attr-div="new_door_container_aluminum_door_only_pair">Aluminum Doors - Door Only Pair</option>
	</select>

	 <select id="select-new-door-type" class="form-control select-new-door-category_type3 door_hide_function">
        <option value="not_applicable"  selected>Select Type</option>
     	  <option value="Hmdndsig" attr-div="new_door_container_hellowmetar_dooor_single">Hollow Metal Doors - Door Only Single</option>
        <option value="hmdndpar" attr-div="new_door_container_hellowmetar_dooor_pair">Hollow Metal Doors - Door Only Pair</option>
	      <option value="hmdnfsig" attr-div="new_door_container_hellowmetar_freme_dooor_single">Hollow Metal Doors - Door and Frame Single</option>
        <option value="hmdnfpar" attr-div="new_door_container_hellowmetar_freme_dooor_pair">Hollow Metal Doors - Door and Frame Pair</option>
    </select>



     <select id="select-new-door-type" class="form-control select-new-door-category_type4 door_hide_function">
        <option value="not_applicable"  selected>Select Type</option>
        <option value="ox" attr-div="new_door_container_patio_ox"  >OX</option>
        <option value="xo" attr-div="new_door_container_patio_xo" >XO</option>
        <option value="oxo_sr" attr-div="new_door_container_patio_oxo_sr" >OXO (SLIDE RIGHT)</option>
        <option value="oxo_sl" attr-div="new_door_container_patio_oxo_sl" >OXO (SLIDE LEFT)</option>
        <option value="xoo" attr-div="new_door_container_patio_xoo">XOO</option>
        <option value="oox" attr-div="new_door_container_patio_oox" >OOX</option>
        <option value="oxxo" attr-div="new_door_container_patio_oxxo" >OXXO</option>
      </select> 

  </div>

</div>
</div>
<div style="clear:both"></div>