<?php
	include_once('dopr_new-door_nav.php');
	include_once('select-new-door.php');
	include_once('glass/single_door.php');
	include_once('glass/single_door_sidelite_left.php');
	include_once('glass/single_door_sidelite_right.php');
	include_once('glass/single_door_sidelite_left_right.php');
	include_once('glass/pair_doors.php');
	include_once('glass/pair_doors_sidelite_left.php');
	include_once('glass/pair_doors_sidelite_right.php');
	include_once('glass/pair_doors_sidelite_left_right.php');


  //Aluminum Doors and Frame with Transom - Single And Pair
	include_once('Aluminum/Aluminum_doors_and_frame_with_transom_pair.php');
	include_once('Aluminum/Aluminum_doors_and_frame_with_transom_single.php');

	//Aluminum Doors Door Only -  Single and Pair
	include_once('Aluminum/Aluminum_doors_door_only_pair.php');
	include_once('Aluminum/Aluminum_doors_door_only_single.php');

	//Aluminum Doors And Frame - Single And Pair
	include_once('Aluminum/Aluminum_doors_and_frame_pair.php');
	include_once('Aluminum/Aluminum_doors_and_frame_single.php');


	include_once('Hollow-Metal/door_and_frame_pair.php');
	include_once('Hollow-Metal/door_and_frame_single.php');
	include_once('Hollow-Metal/door_only_single.php');
	include_once('Hollow-Metal/door_only_pair.php');

	include_once('Patio/ox.php');
	include_once('Patio/xo.php');  
	include_once('Patio/oxo_sr.php');
	include_once('Patio/oxo_sl.php');  

	include_once('Patio/oox.php');
	include_once('Patio/xoo.php');
	include_once('Patio/oxxo.php');
?>

<!-- Image and sketch -->
<div class="container">
<div class="upload_picture_and_generic test">
    <?php include dirname(__FILE__).'/../ndu/upload_picture_generic.php'; ?>
    <?php include dirname(__FILE__).'/../ndu/upload_sketch_generic.php'; ?>
</div></div>
<!-- / Image and sketch  -->
