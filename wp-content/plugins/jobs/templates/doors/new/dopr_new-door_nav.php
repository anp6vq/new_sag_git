<div class="input-group input-group-lg form-group">
   <div class="input-group-btn">
      <button id="btn_new_door_first" type="button" class="btn btn-default" aria-label="Left Align" title="First">
         <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_previous" type="button" class="btn btn-default" aria-label="Left Align" title="Previous">
         <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
      </button>
   </div>
   <input id="text_new_door_item_count" type="text" class="form-control text-center" placeholder="0/0" value="0/0">
   <div class="input-group-btn custom_nav_add">
      <button id="btn_new_door_next" type="button" class="btn btn-default" aria-label="Left Align" title="Next">
         <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_last" type="button" class="btn btn-default" aria-label="Left Align" title="Last">
         <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_add" type="button" class="btn btn-default" aria-label="Left Align" title="Add">
         <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_save" type="button" class="btn btn-default" aria-label="Left Align" title="Save">
         <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_delete" type="button" class="btn btn-default" aria-label="Left Align" title="Delete">
         <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_refresh" type="button" class="btn btn-default" aria-label="Left Align" title="Refresh">
         <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
      </button>
      <button id="btn_new_door_copy_specs_plus_size" type="button" class="btn btn-default img_btn" aria-label="Left Align" title="Copy Specs & Size">
          
          <span class="glyphicon fon_ss" aria-hidden="true">SS</span>
         <!--<span class="glyphicon" aria-hidden="true"><img src="<?php echo WP_CONTENT_URL;?>/plugins/jobs/img/ic_2.png" /></span>-->
      </button>
      <button id="btn_new_door_copy_specs" type="button" class="btn btn-default img_btn" aria-label="Left Align" title="Copy Specs">
          <span class="glyphicon fon_ss" aria-hidden="true">S</span>
         <!--<span class="glyphicon" aria-hidden="true"><img src="<?php echo WP_CONTENT_URL;?>/plugins/jobs/img/ic_1.png" /></span>-->
      </button>
      <button id="btn_new_door_paste" type="button" class="btn btn-default" aria-label="Left Align" title="Paste">
         <span class="glyphicon glyphicon-paste" aria-hidden="true"></span>
      </button>
   </div>
</div>

