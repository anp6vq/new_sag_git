<div id="gdr-<?php echo basename(__FILE__, '.php'); ?>-container" class="container-fluid new_door_container_glass_dooor_pair-4 cuurent_active_div">
   <div class="form-group door-form-group">
    <div class="col-sm-5 door-diagram-col">
      <div class="door-diagram-img-wrapper">
      <img src="<?php echo plugins_url(); ?>/jobs/img/doors/new/glass/DD-4.jpg" class="door-diagram-img" />
      </div>
      <div class="col-sm-12">
         <label class="leftalign" for="textarea_tagd_newdoor_pair_notes">Instructions</label>
         <textarea id="textarea_pdlr_pair_notes" maxlength="300" class="form-control" rows="3" autocomplete="on" placeholder="Instructions"></textarea>
      </div>
	    <div class="col-sm-12">
        <div class="type_a_new_glass_door_single_image"> </div>
      </div>
      <div class="col-sm-12">
        <label class="leftalign" for="textarea_tagd_gdn_wp_single_notes">Handle</label>
        <select id="all_replacement_heandle_type_pdlr_pair_door" class="form-control heneldecontrool pairleftright">
         <option value="not_applicable"  selected>Select Handle </option>
          <option value="pull_handle_up_push_bar_3_holes" imgurl="<?php echo plugins_url();?>/jobs/img/doors/handle/Handle-1.jpg" id="1"> Pull Handle, UP & Push Bar (3 Holes) </option>
          <option value="pull_handle_down_push_bar_3_holes" imgurl="<?php echo plugins_url();?>/jobs/img/doors/handle/Handle-2.jpg" id="2"> Pull Handle, DOWN & Push Bar (3 Holes)</option>
          <option value="pull_handle_2_holes" imgurl="<?php echo plugins_url(); ?>/jobs/img/doors/handle/Handle-3.jpg" id="3" > Pull Handle (2 Holes)</option>
          <option value="pull_bar_2_holes" imgurl="<?php echo plugins_url(); ?>/jobs/img/doors/handle/Handle-4.jpg" id="4"  > Push Bar (2 Holes)</option>
          <option value="ladder_pull_3_holes" imgurl="<?php echo plugins_url(); ?>/jobs/img/doors/handle/Handle-5.jpg"  id="5"> Ladder Pull (3 Holes)</option>
          <option value="ladder_pull_2_holes" imgurl="<?php echo plugins_url(); ?>/jobs/img/doors/handle/Handle-6.jpg" id="6"> Ladder Pull (2 Holes)</option>
          <option value="single_pull_1_hole" imgurl="<?php echo plugins_url(); ?>/jobs/img/doors/handle/Handle-7.jpg"  id="7"> Single Pull (1 Hole)</option>
        </select>
      </div>
    </div>
    <div class="col-sm-7 door-fields-col">
      <div class="fields-sub-form-group form-group row">
        <div class="col-sm-6 fields-left-half">
          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">A</span><label>Door Style</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
            <select id="select_pdlr_a_pair_door_style" class="form-control">
              <option value="not_applicable"  selected>Select Door Style</option>
              <option value="Type A (TOP & BOTTOM PATCH) - SINGLE">Type A (TOP & BOTTOM PATCH) - SINGLE</option>
              <option value="Type A (TOP & BOTTOM PATCH) - PAIR">Type A (TOP & BOTTOM PATCH) - PAIR </option>
              <option value="Type BP (BOTTOM RAIL & TOP PATCH) - SINGLE">Type BP (BOTTOM RAIL & TOP PATCH) - SINGLE</option>
              <option value="Type BP (BOTTOM RAIL & TOP PATCH - PAIR">Type BP (BOTTOM RAIL & TOP PATCH - PAIR</option>
              <option value="Type P (TOP & BOTTOM RAILS) -  PAIR">Type P (TOP & BOTTOM RAILS) -  PAIR</option>
			        <option value="Type P (TOP & BOTTOM RAILS) - SINGLE">Type P (TOP & BOTTOM RAILS) - SINGLE</option>
              <option value="Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE">Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE</option>
              <option value="Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - PAIR">Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - PAIR</option>
              <option value="WP Single - (TOP & BOTTOM RAILS with Side Rails)">WP Single - (TOP & BOTTOM RAILS with Side Rails)</option>
              <option value="Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - PAIR">WP Pair - (TOP & BOTTOM RAILS with Side Rails)</option>
			</select>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">B</span><label>Rough Opening</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input id="text_pdlr_b_pair_rough_opening_width" class="form-control user-success" type="text" placeholder="Width">
              </div>
              <div class="col-sm-1" style="">X</div>
              <div class="col-sm-5">
                <input id="text_pdlr_b_pair_rough_opening_height" class="form-control" type="text" placeholder="Height">
              </div>
            </div>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">C</span><label>Door Opening</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input id="text_pdlr_c_pair_door_opening_width" class="form-control user-success" type="text" placeholder="Width">
              </div>
              <div class="col-sm-1" style="">X</div>
              <div class="col-sm-5">
                <input id="text_pdlr_c_pair_door_opening_height" class="form-control" type="text" placeholder="Height">
              </div>
            </div>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">D</span><label>Door Size</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-5">
                <input id="text_pdlr_d_door_pair_size_width" class="form-control user-success" type="text" placeholder="Width">
              </div>
              <div class="col-sm-1" style="">X</div>
              <div class="col-sm-5">
                <input id="text_pdlr_d_door_pair_size_height" class="form-control" type="text" placeholder="Height">
              </div>
            </div>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">E</span><label>Door Hand</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
			<input type="text" id="text_pdlr_e_door_pair_hand_right" class="form-control" />

		<!--lucky	<select id="text_pdlr_e_door_pair_hand_right" class="form-control">
              <option value="not_applicable"  selected>Select Door Hand</option>
              <option value="Right Hand">Right Hand</option>
              <option value="Left Hand">Left Hand</option>
              <option value="Double Acting">Double Acting</option>

           </select>-->

           <!-- <div class="row">
              <div class="col-sm-5">
                <input id="text_pdlr_e_door_pair_hand_right" class="form-control user-success" type="number" placeholder="Right Hand">
              </div>
              <div class="col-sm-1" style="">X</div>
              <div class="col-sm-6">
                <input id="text_pdlr_e_door_pair_hand_left" class="form-control" type="number" placeholder="Left Hand">
              </div>
            </div>-->
          </div>

          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">F</span><label>Glass Type</label>
              </div>
              <div class="col-sm-7">

              </div>
            </div>
            <select id="select_pdlr_f_glass_type" class="form-control">
              <option value="not_applicable"  selected>Select Glass Type</option>
              <option value='3/8" Clear'>3/8" Clear</option>
              <option value='3/8" Bronze'>3/8" Bronze</option>
              <option value='3/8" Gray'>3/8" Gray</option>
              <option value='3/8" Lami Clear'>3/8" Lami Clear</option>
              <option value='3/8" Lami Bronze'>3/8" Lami Bronze</option>
              <option value='3/8" Lami Gray'>3/8" Lami Gray</option>
              <option value='3/8" Acid Etched'>3/8" Acid Etched</option>
              <option value='3/8" Low Iron'>3/8" Low Iron</option>
              <option value='1/2" Clear'>1/2" Clear</option>
              <option value='1/2" Bronze'>1/2" Bronze</option>
              <option value='1/2" Gray'>1/2" Gray</option>
              <option value='1/2" Lami Clear'>1/2" Lami Clear</option>
              <option value='1/2" Lami Bronze'>1/2" Lami Bronze</option>
              <option value='1/2" Lami Gray'>1/2" Lami Gray</option>
              <option value='1/2" Acid Etched'>1/2" Acid Etched</option>
              <option value='1/2" Low Iron'>1/2" Low Iron</option>
              <option value='5/8" Clear'>5/8" Clear</option>
              <option value='5/8" Bronze'>5/8" Bronze</option>
              <option value='5/8" Gray'>5/8" Gray</option>
              <option value='5/8" Lami Clear'>5/8" Lami Clear</option>
              <option value='5/8" Lami Bronze'>5/8" Lami Bronze</option>
              <option value='5/8" Lami Gray'>5/8" Lami Gray</option>
              <option value='5/8" Acid Etched'>5/8" Acid Etched</option>
              <option value='5/8" Low Iron'>5/8" Low Iron</option>
              <option value='3/4" Clear'>3/4" Clear</option>
              <option value='3/4" Bronze'>3/4" Bronze</option>
              <option value='3/4" Gray'>3/4" Gray</option>
              <option value='3/4" Lami Clear'>3/4" Lami Clear</option>
              <option value='3/4" Lami Bronze'>3/4" Lami Bronze</option>
              <option value='3/4" Lami Gray'>3/4" Lami Gray</option>
              <option value='3/4" Acid Etched'>3/4" Acid Etched</option>
              <option value='3/4" Low Iron'>3/4" Low Iron</option>
              <option value='1" Clear'>1" Clear</option>
              <option value='1" Bronze'>1" Bronze</option>
              <option value='1" Gray'>1" Gray</option>
              <option value='1" Lami Clear'>1" Lami Clear</option>
              <option value='1" Lami Bronze'>1" Lami Bronze</option>
              <option value='1" Lami Gray'>1" Lami Gray</option>
              <option value='1" Acid Etched'>1" Acid Etched</option>
              <option value='1" Low Iron'>1" Low Iron</option>
            </select>
          </div>

          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">G</span><label>Top Rail Size</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>
		<div id="text_pdlr_g_top_rail_width_pairsett">	<input type="text" id="text_pdlr_g_top_rail_width_pair" class="form-control"></div>

			<!--<select id="text_pdlr_g_top_rail_width_pair" class="form-control">
              <option value="not_applicable"  selected>Select Top Rail</option>
              <option value="Height">Height</option>
              <option value="Tapered">Tapered</option>
			        <option value="Square">Square</option>

            </select>-->
          
          </div>
		  
		  
		   <div class="field-wrapper" id="text_pdlr_g_top_rail_width_pairsettype" style="display:none">
            <div class="row door-field-status-row" >
              <div class="col-sm-5">
                <span class="letter">G</span><label>Top Rail Type</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>

			<select id="text_pdlr_g_top_rail_width_type" class="form-control">
              <option value="not_applicable"  selected>Select Top Rail</option>
              <option value="Tapered">Tapered</option>
			        <option value="Square">Square</option>

            </select>
          
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">H</span><label>Bottom Rail Size</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>

<div id="text_pdlr_h_bottom_rail_widthsst">	<input type="text" id="text_pdlr_h_bottom_rail_width" class="form-control">
</div>

		<!--	<select id="text_Agndsing_h_bottom_rail_width" class="form-control">
              <option value="not_applicable"  selected>Select Bottom Rail</option>
              <option value="Height">Height</option>
              <option value="Tapered">Tapered</option>
			  <option value="Square">Square</option>

            </select>-->
          
          </div>
		  
		  
		  
		  <div class="field-wrapper" id="text_pdlr_h_bottom_rail_pairttype" style="display:none">
            <div class="row door-field-status-row">
              <div class="col-sm-5">
                <span class="letter">H</span><label>Bottom Rail Type</label>
              </div>
              <div class="col-sm-7">
              </div>
            </div>



			<select id="text_pdlr_h_bottom_rail_type" class="form-control">
              <option value="not_applicable"  selected>Select Bottom Rail</option>
              <option value="Tapered">Tapered</option>
			  <option value="Square">Square</option>

            </select>
          
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">I</span><label>Metal Finish</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_i_metal_finish_pair" class="form-control">
              <option value="not_applicable"  selected>Select Metal Finish</option>
              <option value="Clear Anodized">Clear Anodized</option>
              <option value="Bronze Anodized">Bronze Anodized</option>
              <option value="Polished Stainless Steel">Polished Stainless Steel</option>
              <option value="Satin Stainless">Satin Stainless</option>
              <option value="Polished Brass">Polished Brass</option>
              <option value="Satin Brass">Satin Brass</option>
              <option value="Clear Anodized (Standard  .4 mils)">Clear Anodized (Standard  .4 mils)</option>
              <option value="Dark Bronze Anodized">Dark Bronze Anodized</option>
              <option value="Black Anodized">Black Anodized</option>
              <option value="Clear Anodized ( .7 mils)">Clear Anodized ( .7 mils)</option>
              <option value="Champagne Anodized">Champagne Anodized</option>
              <option value="Light Bronze Anodized">Light Bronze Anodized</option>
              <option value="Medium Bronze Anodized">Medium Bronze Anodized</option>
              <option value="Polished Chrome (US26)">Polished Chrome (US26)</option>
              <option value="Satin Chrome (US26D)">Satin Chrome (US26D)</option>
              <option value="Polished Stainless Steel (US32)">Polished Stainless Steel (US32)</option>
              <option value="Satin Stainless Steel (US32D)">Satin Stainless Steel (US32D)</option>
              <option value="Satin Nickel (US15)">Satin Nickel (US15)</option>
              <option value="Oxidized Satin Nickel (US15A)">Oxidized Satin Nickel (US15A)</option>
              <option value="Polished Brass (US3)">Polished Brass (US3)</option>
              <option value="Satin Brass (US4)">Satin Brass (US4)</option>
              <option value="Oxidized Satin Brass (US5)">Oxidized Satin Brass (US5)</option>
              <option value="Polished Bronze (US9)">Polished Bronze (US9)</option>
              <option value="Satin Bronze (US10)">Satin Bronze (US10)</option>
              <option value="Oil Rubbed Bronze (US10B)">Oil Rubbed Bronze (US10B)</option>

              <option value="Other">Other</option>
            </select>
          </div>

          </div>
        <div class="col-sm-6 fields-right-half">

          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">J</span><label>Closers</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_j_closers_pair" class="form-control">
              <option value="not_applicable"  selected>Select Closers</option>
              <option value="Jackson COC with Floor Pivot">Jackson COC with Floor Pivot</option>
      			  <option value="Dorma RTS88 with Floor Pivot">Dorma RTS88 with Floor Pivot</option>
      			  <option value="Dorma BTS80 with Top Pivot">Dorma BTS80 with Top Pivot</option>
      			  <option value="Pivots Only">Pivots Only</option>
      			  <option value="Prep Only - Closer by Others">Prep Only - Closer by Others</option>
      			  <option value="Other">Other</option>
		   </select>
          </div>

          <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">K</span><label>Locks</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_k_looks_pair" class="form-control">
              <option value="not_applicable"  selected>Select Locks</option>
              <option value='Bottom Rail Lock (P or Bp)'>Bottom Rail Lock (P or Bp)</option>
              <option value='Dorma Patch Lock (F)'>Dorma Patch Lock (F)</option>
			        <option value='Mag Lock By Others'>Mag Lock By Others</option>
              <option value='Mag Drop Lock By Others'>Mag Drop Lock By Others</option>
              <option value='No Lock'>No Lock</option>
            </select>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">L</span><label>HANDLES</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
          <select id="select_pdlr_l_pull_hardware_pair" class="form-control">
              <option value="not_applicable"  selected>Select Handles</option>
              <option value='Standard Push Pull with Pull 8" CTC Aluminum Finish'>Standard Push Pull with Pull 8" CTC Aluminum Finish</option>
              <option value='Standard Push Pull with Pull 9" CTC Aluminum Finish'>Standard Push Pull with Pull 9" CTC Aluminum Finish</option>
              <option value='Standard Push Pull with Pull 12" CTC Aluminum Finish'>Standard Push Pull with Pull 12" CTC Aluminum Finish</option>
              <option value='Standard Push Pull with Pull 8" CTC Bronze Finish'>Standard Push Pull with Pull 8" CTC Bronze Finish</option>
              <option value='Standard Push Pull with Pull 9" CTC Bronze Finish'>Standard Push Pull with Pull 9" CTC Bronze Finish</option>
              <option value='Standard Push Pull with Pull 12" CTC Bronze Finish'>Standard Push Pull with Pull 12" CTC Bronze Finish</option>
              <option value='Standard Push Pull with Pull 8" CTC Black Finish'>Standard Push Pull with Pull 8" CTC Black Finish</option>
              <option value='Standard Push Pull with Pull 9" CTC Black Finish'>Standard Push Pull with Pull 9" CTC Black Finish</option>
              <option value='Standard Push Pull with Pull 12" CTC Black Finish'>Standard Push Pull with Pull 12" CTC Black Finish</option>
              <option value='Back to back Pulls 8" CTC Aluminum Finish'>Back to back Pulls 8" CTC Aluminum Finish</option>
              <option value='Back to Back Pulls 8" CTC Bronze Finish'>Back to Back Pulls 8" CTC Bronze Finish</option>
              <option value='Back to Back Pulls 8" CTC Black Finish'>Back to Back Pulls 8" CTC Black Finish</option>
              <option value='Back to back Pulls 9" CTC Aluminum Finish'>Back to back Pulls 9" CTC Aluminum Finish</option>
              <option value='Back to Back Pulls 9" CTC Bronze Finish'>Back to Back Pulls 9" CTC Bronze Finish</option>
              <option value='Back to Back Pulls 9" CTC Black Finish'>Back to Back Pulls 9" CTC Black Finish</option>
              <option value='Back to back Pulls 12" CTC Aluminum Finish'>Back to back Pulls 12" CTC Aluminum Finish</option>
              <option value='Back to Back Pulls 12" CTC Bronze Finish'>Back to Back Pulls 12" CTC Bronze Finish</option>
              <option value='Back to Back Pulls 12" CTC Black Finish'>Back to Back Pulls 12" CTC Black Finish</option>
              <option value='Pull Only 8" CTC Aluminum Finish'>Pull Only 8" CTC Aluminum Finish</option>
              <option value='Pull Only 8" CTC Bronze Finish'>Pull Only 8" CTC Bronze Finish</option>
              <option value='Pull Only 8" CTC Black Finish'>Pull Only 8" CTC Black Finish</option>
              <option value='Pull Only 9" CTC Aluminum Finish'>Pull Only 9" CTC Aluminum Finish</option>
              <option value='Pull Only 9" CTC Bronze Finish'>Pull Only 9" CTC Bronze Finish</option>
              <option value='Pull Only 9" CTC Black Finish'>Pull Only 9" CTC Black Finish</option>
              <option value='Pull Only 12" CTC Aluminum Finish'>Pull Only 12" CTC Aluminum Finish</option>
              <option value='Pull Only 12" CTC Bronze Finish'>Pull Only 12" CTC Bronze Finish</option>
              <option value='Pull Only 12" CTC Black Finish'>Pull Only 12" CTC Black Finish</option>
          </select>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">M</span><label>Header</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_m_header_pair" class="form-control">
              <option value="not_applicable"  selected>Select Header</option>
              <option value='1 3/4" x 4 1/2"'>1 3/4" x 4 1/2"</option>
              <option value='1 3/4" x 4"'>1 3/4" x 4"</option>
              <option value='No Header'>No Header</option>
			        <option value='Other'>Other</option>

            </select>
          </div>

		      <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">N</span><label>Threshold</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_n_threshold_pair" class="form-control">
              <option value="not_applicable"  selected>Select Threshold</option>
              <option value='4" x 36" x 1/4" Aluminum Threshold'>4" x 36" x 1/4" Aluminum Threshold</option>
              <option value='5" x 36" x 1/4" Aluminum Threshold'>5" x 36" x 1/4" Aluminum Threshold</option>
              <option value='7" x 36" x 1/4" Aluminum Threshold'>7" x 36" x 1/4" Aluminum Threshold</option>
              <option value='8" x 36" x 1/4" Aluminum Threshold'>8" x 36" x 1/4" Aluminum Threshold</option>
              <option value='4" x 48" x 1/4" Aluminum Threshold'>4" x 48" x 1/4" Aluminum Threshold</option>
              <option value='5" x 48" x 1/4" Aluminum Threshold'>5" x 48" x 1/4" Aluminum Threshold</option>
              <option value='7" x 48" x 1/4" Aluminum Threshold'>7" x 48" x 1/4" Aluminum Threshold</option>
              <option value='8" x 48" x 1/4" Aluminum Threshold'>8" x 48" x 1/4" Aluminum Threshold</option>
              <option value='4" x 72" x 1/4" Aluminum Threshold'>4" x 72" x 1/4" Aluminum Threshold</option>
              <option value='5" x 72" x 1/4" Aluminum Threshold'>5" x 72" x 1/4" Aluminum Threshold</option>
              <option value='7" x 72" x 1/4" Aluminum Threshold'>7" x 72" x 1/4" Aluminum Threshold</option>
              <option value='8" x 72" x 1/4" Aluminum Threshold'>8" x 72" x 1/4" Aluminum Threshold</option>
              <option value='4" x 36" x 1/2" Aluminum Threshold'>4" x 36" x 1/2" Aluminum Threshold</option>
              <option value='5" x 36" x 1/2" Aluminum Threshold'>5" x 36" x 1/2" Aluminum Threshold</option>
              <option value='7" x 36" x 1/2" Aluminum Threshold'>7" x 36" x 1/2" Aluminum Threshold</option>
              <option value='8" x 36" x 1/2" Aluminum Threshold'>8" x 36" x 1/2" Aluminum Threshold</option>
              <option value='4" x 48" x 1/2" Aluminum Threshold'>4" x 48" x 1/2" Aluminum Threshold</option>
              <option value='5" x 48" x 1/2" Aluminum Threshold'>5" x 48" x 1/2" Aluminum Threshold</option>
              <option value='7" x 48" x 1/2" Aluminum Threshold'>7" x 48" x 1/2" Aluminum Threshold</option>
              <option value='8" x 48" x 1/2" Aluminum Threshold'>8" x 48" x 1/2" Aluminum Threshold</option>
              <option value='4" x 72" x 1/2" Aluminum Threshold'>4" x 72" x 1/2" Aluminum Threshold</option>
              <option value='5" x 72" x 1/2" Aluminum Threshold'>5" x 72" x 1/2" Aluminum Threshold</option>
              <option value='7" x 72" x 1/2" Aluminum Threshold'>7" x 72" x 1/2" Aluminum Threshold</option>
              <option value='8" x 72" x 1/2" Aluminum Threshold'>8" x 72" x 1/2" Aluminum Threshold</option>
              <option value='4" x 36" x 1/4" Bronze Threshold'>4" x 36" x 1/4" Bronze Threshold</option>
              <option value='5" x 36" x 1/4" Bronze Threshold'>5" x 36" x 1/4" Bronze Threshold</option>
              <option value='7" x 36" x 1/4" Bronze Threshold'>7" x 36" x 1/4" Bronze Threshold</option>
              <option value='8" x 36" x 1/4" Bronze Threshold'>8" x 36" x 1/4" Bronze Threshold</option>
              <option value='4" x 48" x 1/4" Bronze Threshold'>4" x 48" x 1/4" Bronze Threshold</option>
              <option value='5" x 48" x 1/4" Bronze Threshold'>5" x 48" x 1/4" Bronze Threshold</option>
              <option value='7" x 48" x 1/4" Bronze Threshold'>7" x 48" x 1/4" Bronze Threshold</option>
              <option value='8" x 48" x 1/4" Bronze Threshold'>8" x 48" x 1/4" Bronze Threshold</option>
              <option value='4" x 72" x 1/4" Bronze Threshold'>4" x 72" x 1/4" Bronze Threshold</option>
              <option value='5" x 72" x 1/4" Bronze Threshold'>5" x 72" x 1/4" Bronze Threshold</option>
              <option value='7" x 72" x 1/4" Bronze Threshold'>7" x 72" x 1/4" Bronze Threshold</option>
              <option value='8" x 72" x 1/4" Bronze Threshold'>8" x 72" x 1/4" Bronze Threshold</option>
              <option value='4" x 36" x 1/2" Bronze Threshold'>4" x 36" x 1/2" Bronze Threshold</option>
              <option value='5" x 36" x 1/2" Bronze Threshold'>5" x 36" x 1/2" Bronze Threshold</option>
              <option value='7" x 36" x 1/2" Bronze Threshold'>7" x 36" x 1/2" Bronze Threshold</option>
              <option value='8" x 36" x 1/2" Bronze Threshold'>8" x 36" x 1/2" Bronze Threshold</option>
              <option value='4" x 48" x 1/2" Bronze Threshold'>4" x 48" x 1/2" Bronze Threshold</option>
              <option value='5" x 48" x 1/2" Bronze Threshold'>5" x 48" x 1/2" Bronze Threshold</option>
              <option value='7" x 48" x 1/2" Bronze Threshold'>7" x 48" x 1/2" Bronze Threshold</option>
              <option value='8" x 48" x 1/2" Bronze Threshold'>8" x 48" x 1/2" Bronze Threshold</option>
              <option value='4" x 72" x 1/2" Bronze Threshold'>4" x 72" x 1/2" Bronze Threshold</option>
              <option value='5" x 72" x 1/2" Bronze Threshold'>5" x 72" x 1/2" Bronze Threshold</option>
              <option value='7" x 72" x 1/2" Bronze Threshold'>7" x 72" x 1/2" Bronze Threshold</option>
              <option value='8" x 72" x 1/2" Bronze Threshold'>8" x 72" x 1/2" Bronze Threshold</option>
              <option value='4" x 36" x 1/4" Brass Threshold'>4" x 36" x 1/4" Brass Threshold</option>
              <option value='5" x 36" x 1/4" Brass Threshold'>5" x 36" x 1/4" Brass Threshold</option>
              <option value='7" x 36" x 1/4" Brass Threshold'>7" x 36" x 1/4" Brass Threshold</option>
              <option value='8" x 36" x 1/4" Brass Threshold'>8" x 36" x 1/4" Brass Threshold</option>
              <option value='4" x 48" x 1/4" Brass Threshold'>4" x 48" x 1/4" Brass Threshold</option>
              <option value='5" x 48" x 1/4" Brass Threshold'>5" x 48" x 1/4" Brass Threshold</option>
              <option value='7" x 48" x 1/4" Brass Threshold'>7" x 48" x 1/4" Brass Threshold</option>
              <option value='8" x 48" x 1/4" Brass Threshold'>8" x 48" x 1/4" Brass Threshold</option>
              <option value='4" x 72" x 1/4" Brass Threshold'>4" x 72" x 1/4" Brass Threshold</option>
              <option value='5" x 72" x 1/4" Brass Threshold'>5" x 72" x 1/4" Brass Threshold</option>
              <option value='7" x 72" x 1/4" Brass Threshold'>7" x 72" x 1/4" Brass Threshold</option>
              <option value='8" x 72" x 1/4" Brass Threshold'>8" x 72" x 1/4" Brass Threshold</option>
              <option value='4" x 36" x 1/2" Brass Threshold'>4" x 36" x 1/2" Brass Threshold</option>
              <option value='5" x 36" x 1/2" Brass Threshold'>5" x 36" x 1/2" Brass Threshold</option>
              <option value='7" x 36" x 1/2" Brass Threshold'>7" x 36" x 1/2" Brass Threshold</option>
              <option value='8" x 36" x 1/2" Brass Threshold'>8" x 36" x 1/2" Brass Threshold</option>
              <option value='4" x 48" x 1/2" Brass Threshold'>4" x 48" x 1/2" Brass Threshold</option>
              <option value='5" x 48" x 1/2" Brass Threshold'>5" x 48" x 1/2" Brass Threshold</option>
              <option value='7" x 48" x 1/2" Brass Threshold'>7" x 48" x 1/2" Brass Threshold</option>
              <option value='8" x 48" x 1/2" Brass Threshold'>8" x 48" x 1/2" Brass Threshold</option>
              <option value='4" x 72" x 1/2" Brass Threshold'>4" x 72" x 1/2" Brass Threshold</option>
              <option value='5" x 72" x 1/2" Brass Threshold'>5" x 72" x 1/2" Brass Threshold</option>
              <option value='7" x 72" x 1/2" Brass Threshold'>7" x 72" x 1/2" Brass Threshold</option>
              <option value='8" x 72" x 1/2" Brass Threshold'>8" x 72" x 1/2" Brass Threshold</option>
            </select>
          </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">O</span><label>Sidelight Head Condition</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_o_sidelight_head_condition_pair" class="form-control">
              <option value="not_applicable"  selected>Select Sidelight Head Condition</option>
              <option value='Rail Under the Header'>Rail Under the Header</option>
              <option value='Rail to the Ceiling'>Rail to the Ceiling</option>
              <option value='Undercut Header'>Undercut Header</option>
              <option value='2" x 1" Channel'>2" x 1" Channel</option>
            </select>
            </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">P</span><label>Sidelight Sill Condition</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_p_sidelight_sill_condition_pair" class="form-control">
              <option value="not_applicable"  selected>Select Sidelight Sill Condition</option>
              <option value='Rail to the Floor'>Rail to the Floor</option>
              <option value='Rail on Top of Threshold'>Rail on Top of Threshold</option>
              <option value='1" x 1" Channel'>1" x 1" Channel</option>
            </select>
            </div>

		  <div class="field-wrapper">
            <div class="row door-field-status-row">
              <div class="col-sm-7">
                <span class="letter">Q</span><label>Sidelites Configuration</label>
              </div>
              <div class="col-sm-5">

              </div>
            </div>
            <select id="select_pdlr_q_sidelites_configuration_pair" class="form-control">
              <option value="not_applicable"  selected>Select Sidelites Configuration</option>
              <option value='Sidelite on the Left'>Sidelite on the Left</option>
              <option value='Sidelite on the Right'>Sidelite on the Right</option>
              <option value='Sidelite Both Sides'>Sidelite Both Sides</option>
           </select>
            </div>

        </div>
      </div>

	  <div class="fields-sub-form-group form-group row">
       <div class="hendleshow_single_a_1 hendleshow_single_a">
          <div class="col-sm-6 fields-left-half">
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_single_a" id="handlea_label"> <span class="letter">A</span>
                  <label>Handle A</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="pdlrhandlea" value="" class="form-control handle_single_a"  style="display:none"/>
            </div>
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_single_b"  id="handleb_label"> <span class="letter">B</span>
                  <label>Handle B</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="pdlrhandleb" value="" class="form-control handle_single_b" style="display:none" />
            </div>
          </div>
          <div class="col-sm-6 fields-right-half">
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_single_c" id="handlec_label"> <span class="letter">C</span>
                  <label>Handle C</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="pdlrhandlec" value="" class="form-control handle_single_c" style="display:none" />
            </div>
            <div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_single_d" id="handled_label" > <span class="letter">D</span>
                  <label>Handle D</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="pdlrhandled" value="" class="form-control handle_single_d"  style="display:none">
            </div>
			<div class="field-wrapper">
              <div class="row door-field-status-row">
                <div class="col-sm-7 handle_single_e" id="handlee_label"> <span class="letter">E</span>
                  <label>Handle E</label>
                </div>
                <div class="col-sm-5"> </div>
              </div>
              <input type="text" id="pdlrhandlee" value="" class="form-control handle_single_e" style="display:none" />
            </div>
          </div>
        </div>

	         </div>
    </div>
    <div class="col-xs-6">
   <div class="form-group">
            <div class="col-xs-12">
               <label class="leftalign">Glass materials</label>
            </div>
         </div>
<div class="form-group">
  <div class="col-xs-12 leftalign">
    <div class="panel panel-default">
      <div class="panel-body chaulksection materials-section" >
         <div class="form-group addqty"  id="add_quntity_0">
                    <div  class="add_quntity" id="1" >
                      <div class="myCustom" id="add_id_0_0">
                        <fieldset class="col-xs-12 margin-top_culk_minus" id="fieldset_tgpd_glass_materials_caulk">
                          <div class="row add">
                            <div class="col-xs-3 col-md-4 col-lg-4 gls_caulk">
                              <label class="leftalign add" for="text_tgpd_glass_materials_caulk_amount">Amount</label>
                              <div class="input-group">
                                <span class="input-group-btn">
                                  <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_caulk_amount_0_0">
                                    <span class="glyphicon glyphicon-minus">                                 </span>
                                </button>
                              </span>
                              <input type="text" id="text_tgpd_glass_materials_caulk_amount_0_0" value="0" name="text_tgpd_glass_materials_caulk_amount_0_0" class="form-control input-number-plus-minus qty-align-new user-success" min="0" max="100" /><span class="input-group-btn">
                              <button data-field="text_tgpd_glass_materials_caulk_amount_0_0" type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"><span class="glyphicon glyphicon-plus">                             
                              </span>
                            </button>
                          </span>
                        </div>
                      </div>
                      <div class="col-xs-8 col-md-6 col-lg-6 add">
                        <label class="leftalign add" for="select_tgpd_glass_materials_caulk_type">CAULK</label>
                        <select id="select_tgpd_glass_materials_caulk_type_0_0" class="form-control add user-success">
                          <option value="not_applicable">Caulk type</option>
                          <option value="799-Clear (T)">799-Clear (T)</option>
                          <option value="999-A-Clear (T)">999-A-Clear (T)</option>
                          <option value="790-Black (S)">790-Black (S)</option>
                          <option value="790-Black (T)">790-Black (T)</option>
                          <option value="795-Black (S)">795-Black (S)</option>
                          <option value="795-Black (T)">795-Black (T)</option>
                          <option value="995-Black (S)">995-Black (S)</option>
                          <option value="999-A-Black (T)">999-A-Black (T)</option>
                          <option value="Dymonic-Black (S)">Dymonic-Black (S)</option>
                          <option value="Dymonic-Black (T)">Dymonic-Black (T)</option>
                          <option value="790-Bronze (S)">790-Bronze (S)</option>
                          <option value="790-Bronze (T)">790-Bronze (T)</option>
                          <option value="795-Bronze (S)">795-Bronze (S)</option>
                          <option value="795-Bronze (T)">795-Bronze (T)</option>
                          <option value="999-A-Bronze (T)">999-A-Bronze (T)</option>
                          <option value="Dymonic-Bronze (S)">Dymonic-Bronze (S)</option>
                          <option value="Dymonic-Bronze (T)">Dymonic-Bronze (T)</option>
                          <option value="795-Anodized Aluminum (S)">795-Anodized Aluminum (S)</option>
                          <option value="795-Anodized Aluminum (T)">795-Anodized Aluminum (T)</option>
                          <option value="999-A-Aluminum (T)">999-A-Aluminum (T)</option>
                          <option value="Dymonic-Anodized Aluminum (S)">Dymonic-Anodized Aluminum (S)</option>
                          <option value="Dymonic-Anodized Aluminum (T)">Dymonic-Anodized Aluminum (T)</option>
                          <option value="790-White (S)">790-White (S)</option>
                          <option value="790-White (T)">790-White (T)</option>
                          <option value="795-White (S)">795-White (S)</option>
                          <option value="795-White (T)">795-White (T)</option>
                          <option value="995-White (S)">995-White (S)</option>
                          <option value="999-A-White (T)">999-A-White (T)</option>
                          <option value="Dymonic-White (S)">Dymonic-White (S)</option>
                          <option value="Dymonic-White (T)">Dymonic-White (T)</option>
                          <option value="Rockite (Bucket)">Rockite (Bucket)</option>
                          <option value="Rockite (Gallon)">Rockite (Gallon)</option>
                          <option value="Epoxy (Gallon)">Epoxy (Gallon)</option>
                          <option value="Putty White (Pint) - Wood">Putty White (Pint) - Wood</option>
                          <option value="Putty White (Gallon) - Wood">Putty White (Gallon) - Wood</option>
                          <option value="Putty Grey (Pint) - Metal">Putty Grey (Pint) - Metal</option>
                          <option value="Putty Grey (Gallon) - Metal">Putty Grey (Gallon) - Metal</option>
                          <option value="790-Adobe Tan (S)">790-Adobe Tan (S)</option>
                          <option value="790-Adobe Tan (T)">790-Adobe Tan (T)</option>
                          <option value="790-Blue Spruce (S)">790-Blue Spruce (S)</option>
                          <option value="790-Blue Spruce (T)">790-Blue Spruce (T)</option>
                          <option value="790-Charcoal (S)">790-Charcoal (S)</option>
                          <option value="790-Charcoal (T)">790-Charcoal (T)</option>
                          <option value="790-Dusty Rose (S)">790-Dusty Rose (S)</option>
                          <option value="790-Dusty Rose (T)">790-Dusty Rose (T)</option>
                          <option value="790-Gray (S)">790-Gray (S)</option>
                          <option value="790-Gray (T)">790-Gray (T)</option>
                          <option value="790-Limestone (S)">790-Limestone (S)</option>
                          <option value="790-Limestone (T)">790-Limestone (T)</option>
                          <option value="790-Natural Stone (S)">790-Natural Stone (S)</option>
                          <option value="790-Natural Stone (T)">790-Natural Stone (T)</option>
                          <option value="790-Precast White (S)">790-Precast White (S)</option>
                          <option value="790-Precast White (T)">790-Precast White (T)</option>
                          <option value="790-Rustic Brick (S)">790-Rustic Brick (S)</option>
                          <option value="790-Rustic Brick (T)">790-Rustic Brick (T)</option>
                          <option value="790-Sandstone (S)">790-Sandstone (S)</option>
                          <option value="790-Sandstone (T)">790-Sandstone (T)</option>
                          <option value="795-Adobe Tan (S)">795-Adobe Tan (S)</option>
                          <option value="795-Adobe Tan (T)">795-Adobe Tan (T)</option>
                          <option value="795-Blue Spruce (S)">795-Blue Spruce (S)</option>
                          <option value="795-Blue Spruce (T)">795-Blue Spruce (T)</option>
                          <option value="795-Champagne (S)">795-Champagne (S)</option>
                          <option value="795-Champagne (T)">795-Champagne (T)</option>
                          <option value="795-Charcoal (S)">795-Charcoal (S)</option>
                          <option value="795-Charcoal (T)">795-Charcoal (T)</option>
                          <option value="795-Dusty Rose (S)">795-Dusty Rose (S)</option>
                          <option value="795-Dusty Rose (T)">795-Dusty Rose (T)</option>
                          <option value="795-Gray (S)">795-Gray (S)</option>
                          <option value="795-Gray (T)">795-Gray (T)</option>
                          <option value="795-Limestone (S)">795-Limestone (S)</option>
                          <option value="795-Limestone (T)">795-Limestone (T)</option>
                          <option value="795-Natural Stone (S)">795-Natural Stone (S)</option>
                          <option value="795-Natural Stone (T)">795-Natural Stone (T)</option>
                          <option value="795-Rustic Brick (S)">795-Rustic Brick (S)</option>
                          <option value="795-Rustic Brick (T)">795-Rustic Brick (T)</option>
                          <option value="795-Sandstone (S)">795-Sandstone (S)</option>
                          <option value="795-Sandstone (T)">795-Sandstone (T)</option>
                          <option value="995-Gray (S)">995-Gray (S)</option>
                          <option value="999-A-Light Bronze (T)">999-A-Light Bronze (T)</option>
                          <option value="Dymonic-Almond (S)">Dymonic-Almond (S)</option>
                          <option value="Dymonic-Almond (T)">Dymonic-Almond (T)</option>
                          <option value="Dymonic-Aluminum Stone (S)">Dymonic-Aluminum Stone (S)</option>
                          <option value="Dymonic-Aluminum Stone (T)">Dymonic-Aluminum Stone (T)</option>
                          <option value="Dymonic-Beige (S)">Dymonic-Beige (S)</option>
                          <option value="Dymonic-Beige (T)">Dymonic-Beige (T)</option>
                          <option value="Dymonic-Buff (S)">Dymonic-Buff (S)</option>
                          <option value="Dymonic-Buff (T)">Dymonic-Buff (T)</option>
                          <option value="Dymonic-Dark Bronze (S)">Dymonic-Dark Bronze (S)</option>
                          <option value="Dymonic-Dark Bronze (T)">Dymonic-Dark Bronze (T)</option>
                          <option value="Dymonic-Gray (S)">Dymonic-Gray (S)</option>
                          <option value="Dymonic-Gray (T)">Dymonic-Gray (T)</option>
                          <option value="Dymonic-Gray Stone (S)">Dymonic-Gray Stone (S)</option>
                          <option value="Dymonic-Gray Stone (T)">Dymonic-Gray Stone (T)</option>
                          <option value="Dymonic-Hartford Green (S)">Dymonic-Hartford Green (S)</option>
                          <option value="Dymonic-Hartford Green (T)">Dymonic-Hartford Green (T)</option>
                          <option value="Dymonic-Ivory (S)">Dymonic-Ivory (S)</option>
                          <option value="Dymonic-Ivory (T)">Dymonic-Ivory (T)</option>
                          <option value="Dymonic-Light Bronze (S)">Dymonic-Light Bronze (S)</option>
                          <option value="Dymonic-Light Bronze (T)">Dymonic-Light Bronze (T)</option>
                          <option value="Dymonic-Limestone (S)">Dymonic-Limestone (S)</option>
                          <option value="Dymonic-Limestone (T)">Dymonic-Limestone (T)</option>
                          <option value="Dymonic-Natural Clay (S)">Dymonic-Natural Clay (S)</option>
                          <option value="Dymonic-Natural Clay (T)">Dymonic-Natural Clay (T)</option>
                          <option value="Dymonic-Off White (S)">Dymonic-Off White (S)</option>
                          <option value="Dymonic-Off White (T)">Dymonic-Off White (T)</option>
                          <option value="Dymonic-Precast White (S)">Dymonic-Precast White (S)</option>
                          <option value="Dymonic-Precast White (T)">Dymonic-Precast White (T)</option>
                          <option value="Dymonic-Redwood Tan (S)">Dymonic-Redwood Tan (S)</option>
                          <option value="Dymonic-Redwood Tan (T)">Dymonic-Redwood Tan (T)</option>
                          <option value="Dymonic-Sandalwood (S)">Dymonic-Sandalwood (S)</option>
                          <option value="Dymonic-Sandalwood (T)">Dymonic-Sandalwood (T)</option>
                          <option value="Dymonic-Stone (S)">Dymonic-Stone (S)</option>
                          <option value="Dymonic-Stone (T)">Dymonic-Stone (T)</option>
                        </select>
                      </div>
                      <div class="col-xs-1">
                        <a title="<?php echo basename(__FILE__, '.php'); ?>" href="javascript:void(0);" class="btn_pluse btn12_patio" >+</a>
                      </div>
                    </div>
                  </fieldset>
                  </div>


                    </div>
                     <div class="add_tap_amount" id="1">
                      <div class="myCustom1" id="add_id_tap_0">
                        <div class="col-xs-12">
                          <div class="row plus-minus-btn-row">
                            <div class="col-xs-12">
                              <fieldset id="fieldset_tgpd_glass_materials_tape">
                                <div class="row">
                                  <div class="col-xs-3 col-md-4 gls_caulk">
                                    <label class="leftalign">Amount</label>
                                    <div class="input-group">
                                      <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_tape_amount_0_0">
                                          <span class="glyphicon glyphicon-minus"></span>
                                        </button>
                                      </span>
                                      <input type="text" id="text_tgpd_glass_materials_tape_amount_0_0" value="0" name="text_tgpd_glass_materials_tape_amount_0_0" class="form-control input-number-plus-minus qty-align-new user-success" min="0" max="100"><span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="text_tgpd_glass_materials_tape_amount_0_0">
                                          <span class="glyphicon glyphicon-plus"></span>
                                        </button></span>
                                      </div>
                                    </div>
                                    <div class="col-xs-8 col-md-6 add">
                                      <label class="leftalign">TAPE</label>
                                      <select id="select_tgpd_glass_materials_tape_type_0_0" class="form-control">
                                        <option value="not_applicable">Tape type</option>
                                        <option value="1_over_8_440">1/8 440 tape</option>
                                        <option value="1_over_4_440">1/4 440 tape</option>
                                        <option value="1_over_2_440">1/2 440 tape</option>
                                        <option value="3_over_4_440">3/4 440 tape</option>
                                        <option value="foam">Foam tape</option>
                                        <option value="double_faced">Double faced tape</option>
                                        <option value="cladding">Cladding tape</option>
                                      </select>
                                    </div> 
                                    <div class="col-xs-1">
                                      <a title="<?php echo basename(__FILE__, '.php'); ?>" href="javascript:void(0);" class="btn_pluse btn11_patio" >+</a>
                                    </div> 
                                  </div>
                                   </fieldset>
                                 </div>
                               </div>
                             </div>
                           </div>

                     </div>
                     <div class="add_tap_scaff" id="1">
                       <div class="myCustom1" id="add_id_scaff_0">
                        <div class="col-xs-12 add">
                          <div class="row add plus-minus-btn-row">
                            <div class="col-xs-12 add">
                              <fieldset id="fieldset_tgpd_glass_materials_scaffolding">
                                <div class="row add">
                                  <div class="col-xs-3 col-md-4  gls_caulk">
                                   <label class="leftalign smaller" for="select_tgpd_glass_materials_quantity">Quantity</label>  <div class="input-group col-xs-12">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="select_tgpd_glass_materials_quantity_0_0">
                                        <span class="glyphicon glyphicon-minus"></span>
                                      </button></span>
                                       <input id="select_tgpd_glass_materials_quantity_0_0" name="select_tgpd_glass_materials_quantity_0_0" type="text" class="form-control text-num input-number-plus-minus qty-align-new user-success" autocomplete="on" min="0" max="100" value="0">
                                       <span class="input-group-btn">
                                        <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_quantity_0_0">
                                          <span class="glyphicon glyphicon-plus"></span>
                                        </button>
                                      </span>
                                    </div> 
                                  </div>
                                  <div class="col-xs-8 col-md-6 add">
                                    <label class="leftalign add" for="select_tgpd_glass_materials_scaffolding_type">EQUIPMENT</label>
                                    <select id="select_tgpd_glass_materials_scaffolding_type_0_0" class="form-control add user-success">
                                      <option value="not_applicable">Equipment type</option>
                                      <option value="Swing Stage (SAG)">Swing Stage (SAG)</option>
                                      <option value="Swing Stage (SUB)">Swing Stage (SUB)</option>
                                      <option value="Scaffolding (SAG)">Scaffolding (SAG)</option>
                                      <option value="Scaffolding-HALF (SAG)">Scaffolding-HALF (SAG)</option
                                        ><option value="Scaffolding (SUB, RENT)">Scaffolding (SUB, RENT)</option>
                                        <option value="Scaffolding (SUB, SETUP)">Scaffolding (SUB, SETUP)</option>
                                        <option value="Scaffolding-BAKER (SUB.RENT)">Scaffolding-BAKER (SUB.RENT)</option>
                                        <option value="Scaffolding-BAKER (SUB.SETUP)">Scaffolding-BAKER (SUB.SETUP)</option>
                                        <option value="40FT Articulating Lift">40FT Articulating Lift</option>
                                        <option value="60FT Articulating Lift">60FT Articulating Lift</option>
                                        <option value="80FT Articulating Lift">80FT Articulating Lift</option>
                                        <option value="120FT Articulating Lift">120FT Articulating Lift</option>
                                        <option value="135FT Articulating Lift">135FT Articulating Lift</option>
                                        <option value="40FT STICK BOOM">40FT STICK BOOM</option>
                                        <option value="60FT STICK BOOM">60FT STICK BOOM</option>
                                        <option value="80FT STICK BOOM">80FT STICK BOOM</option>
                                        <option value="120FT STICK BOOM">120FT STICK BOOM</option>
                                        <option value="135FT STICK BOOM">135FT STICK BOOM</option>
                                        <option value="19FT Scissor Lift">19FT Scissor Lift</option>
                                        <option value="24-26FT Scissor Lift">24-26FT Scissor Lift</option>
                                        <option value="30-35FT Scissor Lift">30-35FT Scissor Lift</option>
                                        <option value="39-40FT Scissor Lift">39-40FT Scissor Lift</option>
                                        <option value="25-27T 4WD Scissor Lift">25-27T 4WD Scissor Lift</option>
                                        <option value="36-49FT 4WD Scissor Lift">36-49FT 4WD Scissor Lift</option>
                                        <option value="Towable Boom">Towable Boom</option>
                                        <option value="Generator">Generator</option>
                                        <option value="Power Cup">Power Cup</option>
                                        <option value="Crane">Crane</option>
                                        <option value="Lift My Glass">Lift My Glass</option><option value="LULL LIFT">LULL LIFT</option>
                                        <option value="Fork Boom">Fork Boom</option>
                                        <option value="Chain Fall">Chain Fall</option>
                                        <option value="Traffic Control">Traffic Control</option>
                                        <option value="Street Closure Permit">Street Closure Permit</option>
                                        <option value="New Part- TEXT BOX">New Part- TEXT BOX</option>
                                      </select>
                                    </div>
                                    <div class="col-xs-1"><a href="javascript:void(0);" title="<?php echo basename(__FILE__, '.php'); ?>" class="btn_pluse btn14_patio" >+</a>
                                    </div>         
                                     </div>
                                   </fieldset>
                                 </div>
                               </div>
                             </div>
                           </div>
                     </div>
                     <div class="add_tap_channel" id="1">
                       <div class="myCustom1">
                        <div class="col-xs-12">
                          <div class="row add plus-minus-btn-row">
                            <div class="col-xs-12 add">
                              <fieldset id="fieldset_tgpd_glass_materials_channel">
                                <div class="row add">
                                <div class="col-xs-3 col-md-4  gls_caulk" >
                                  <label class="leftalign smaller" for="select_tgpd_glass_materials_channel_quantity">Quantity</label>
                                  <div class="input-group col-xs-12">
                                    <span class="input-group-btn">
                                      <button type="button" class="btn btn-default btn-number-plus-minus bt_for_minus" data-type="minus" data-field="select_tgpd_glass_materials_channel_quantity_0_0">
                                        <span class="glyphicon glyphicon-minus">
                                        
                                      </span>
                                    </button>
                                  </span>
                                  <input id="select_tgpd_glass_materials_channel_quantity_0_0" name="select_tgpd_glass_materials_channel_quantity_0_0" type="text" class="form-control text-num input-number-plus-minusqty-align-new text-center user-success" autocomplete="on" min="0" max="100" value="0">
                                  <span class="input-group-btn">
                                    <button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_channel_quantity_0_0">
                                      <span class="glyphicon glyphicon-plus">
                                      
                                    </span>
                                  </button>
                                </span>
                              </div> 
                            </div>
                            <div class=" col-xs-8 col-md-6 add" id="add_id_channel_0">
                              <label class="leftalign" style="">CHANNEL</label>
                              <select id="text_tgpd_glass_materials_channel_0_0" class="form-control add user-success">
                                <option value="not_applicable">Channel Type</option>
                                   <option value="J-Channel 5/8 Chrome">J-Channel 5/8 Chrome</option>
                                   <option value="J-Channel 5/8 Gold">J-Channel 5/8 Gold</option>
                                   <option value="J-Channel 5/8 Brushed Nickel">J-Channel 5/8 Brushed Nickel</option>
                                   <option value="J-Channel 5/8 Oil Rubbed Bronze">J-Channel 5/8 Oil Rubbed Bronze</option>
                                   <option value="J-Channel 3/8 Chrome">J-Channel 3/8 Chrome</option>
                                   <option value="J-Channel 3/8 Gold">J-Channel 3/8 Gold</option>
                                   <option value="J-Channel 3/8 Brushed Nickel">J-Channel 3/8 Brushed Nickel</option>
                                   <option value="J-Channel 3/8 Oil Rubbed Bronze">J-Channel 3/8 Oil Rubbed Bronze</option>
                                   <option value="L-Channel Chrome">L-Channel Chrome</option>
                                   <option value="L-Channel Gold">L-Channel Gold</option>
                                   <option value="L-Channel Brushed Nickel">L-Channel Brushed Nickel</option>
                                   <option value="L-Channel Oil Rubbed Bronze">L-Channel Oil Rubbed Bronze</option>
                                   <option value="U-Channel 3/4 x 3/4 Clear Anodized/ Satin">U-Channel 3/4 x 3/4 Clear Anodized/ Satin</option>

                                   <option value="U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin">U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 1 Clear Anodized/ Satin">U-Channel 1 x 1 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 1 Polished Brite Silver">U-Channel 1 x 1 Polished Brite Silver</option>
                                   <option value="U-Channel 1 x 1 Brushed Stainless">U-Channel 1 x 1 Brushed Stainless</option>
                                   <option value="U-Channel 1 x 2 Clear Anodized/ Satin">U-Channel 1 x 2 Clear Anodized/ Satin</option>
                                   <option value="U-Channel 1 x 2 Polished Brite Silver">U-Channel 1 x 2 Polished Brite Silver</option>
                                   <option value="U-Channel 1 x 2 Brushed Stainless">U-Channel 1 x 2 Brushed Stainless</option>
                                   <option value="Tube - Chrome">Tube - Chrome</option>
                                   <option value="Tube - Brushed Nickel">Tube - Brushed Nickel</option>
                                   <option value="Tube - Oil Rubbed Bronze">Tube - Oil Rubbed Bronze</option>
                                   <option value="Glazing Rubber for U Channel, 1/4 Glass - Black">Glazing Rubber for U Channel, 1/4 Glass - Black</option>
                                   <option value="Glazing Rubber for U Channel, 3/8 Glass - Black">Glazing Rubber for U Channel, 3/8 Glass - Black</option>
                                   <option value="Glazing Rubber for U Channel, 1/2 Glass - Black">Glazing Rubber for U Channel, 1/2 Glass - Black</option>
                                 </select>
                               </div>
                               <div class=" col-xs-1"><a href="javascript:void(0);" door_type="<?php echo basename(__FILE__, '.php'); ?>" class="btn_pluse btn13_patio" >+</a>
                               </div>
                             </div>
                           </fieldset>
                         </div>
                       </div>
                     </div>
                   </div>
                    </div>
         </div>
         <div class="form-group">
      <div class="col-xs-12 col-lg-10">
               <label class="leftalign" for="text_tgpd_miscellaneous">Miscellaneous</label>
         <input id="text_tgpd_miscellaneous" class="form-control" type="text" autocomplete="on" maxlength="255" placeholder="Miscellaneous">
                &nbsp;
      </div>
         </div>
      </div>
         </div>
      </div>
      </div>
      </div>
    <div class="col-xs-6">
      <div class="form-group">
            <div class="col-xs-12 col-md-12">
               <label class="leftalign">Glass reminders</label>
            </div>
         </div>
      <div class="form-group">
            <div class="col-xs-12 col-sm-12">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header">Solar film?</h5>
            <label class="switch-light switch-candy">
               <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_solar_film" value="solar_film" door_type="<?php echo basename(__FILE__, '.php'); ?>">
               <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
               </span>
            </label>
         </div>
      </div>
                     </div>
                        <div class="col-xs-12">
                              <div class="form-group">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film1" style="display: none;">
                 <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_sag" value="sag" door_type="<?php echo basename(__FILE__, '.php'); ?>">SAG
                                       </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_customer" value="customer" door_type="<?php echo basename(__FILE__, '.php'); ?>">Customer
                                       </label>
                                    </div>
                  </div>
                                 </fieldset>
                              </div>
                           </div>
                           <div class="col-xs-12">
                              <div class="row">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film2" style="display: none;">
                                    <div class="form-group">
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_type" type="text" class="form-control"  placeholder="Film type">
                                       </div>
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_source" type="text" class="form-control" autocomplete="on" placeholder="Film source">
                                       </div>
                                    </div>
                                 </fieldset>
                              </div>
                           </div>

         <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header">Wet seal?</h5>
                              <label class="switch-light switch-candy" onclick="">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_wet_seal" value="wet_seal" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12">
            <div class="row">
                              <fieldset id="fieldset_tgpd_glass_reminders_wet_seal" style="display: none;">
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag">SAG
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_half_day">SAG Sub Half Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_full_day">SAG Sub Full Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_customer" value="customer">Customer
                                    </label>
                                 </div>
                              </fieldset>
            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Furniture to Move?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_furniture_to_move" value="furniture_to_move" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12 col-sm-12">
            <div class="row">
         <div class="col-sm-12">
         <fieldset id="fieldset_tgpd_glass_reminders_furniture_to_move" style="display: none;">
            <input id="text_tgpd_glass_reminders_furniture_to_move_comment" class="form-control" type="text"   placeholder="Comment">
         </fieldset>
         </div>
            </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Walls/ceilings cut?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut" value="walls_or_ceilings_to_cut" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>

                           <div class="col-xs-12 col-sm-12">
                              <div class="row">
                                 <div class="">
                                    <div class="form-group">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1" style="display: none;">
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_sag" value="sag">SAG
                                             </label>
                                          </div>
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_customer" value="customer">Customer
                                             </label>
                                          </div>
                                       </fieldset>
                                    </div>
                                 </div>
                                 <div class="col-sm-12 col-md-12 leftalign">
                                    <div class="row">
               <div class="col-sm-12">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2" style="display: none;">
                                          <input id="text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment" class="form-control" type="text" autocomplete="on"  placeholder="Comment">
                                       </fieldset>
               </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
         <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Blind needs Removing?</h5>
            <label class="switch-light switch-candy">
         <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_blind_needs_removing" door_type="<?php echo basename(__FILE__, '.php'); ?>" value="blind_needs_removing">
         <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
         </span>
            </label>
         </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                              <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Glass Fits Elevator?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="checkbox_tgpd_glass_reminders_glass_fits_elevator" value="glass_fits_elevator" door_type="<?php echo basename(__FILE__, '.php'); ?>" checked>
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                              </div>
                           <div class="col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
         <div class="col-xs-12 col-sm-12">
                        <div class="row">
          <!--  lucky-->
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Color Waiver ?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_color_waiver" id="checkbox_tgpd_glass_reminders_add_color_waiver" value="add_color_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                                 </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
         <div class="col-sm-12">
                        <div class="row">
         <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Damage Waiver ?</h5>
            <label class="switch-light switch-candy">
         <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_damage_waiver_pactio" id="checkbox_tgpd_glass_reminders_add_damage_waiver" value="damage_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>" >
         <span>
            <span>No</span>
            <span>Yes</span>
            <a></a>
         </span>
            </label>
         </div>
          <div class="col-sm-12 col-md-12 dwmargin">
            <div class="row">
              <div class="col-xs-12">
              <fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section_for_select" style="display: none;" >
                <select class="form-control" id="select_tgpd_glass_damage_waiver" name="select_tgpd_glass_damage_waiver" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                   <option value="not_applicable" >Damage Waiver</option>
                  <option value="Removal/Reinstall of Glass Currently Installed">Removal/Reinstall of Glass Currently Installed </option>
                  <option value="Handling of Customers Materials">Handling of Customers Materials </option>
                  <option value="Adjacent Glass">Adjacent Glass</option>
                  <option value="select_tgpd_glass_damage_waiver_for_text">Other</option>
                </select>
                </fieldset>
              </div>
            </div>
          </div>
        <div class="col-sm-12 col-md-12 leftalign dwmargin">
          <div class="row">
            <div class="col-sm-12">
              <fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section" style="display: none;">
                <input id="text_tgpd_glass_damage_waiver_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_damage_waiver_reminder_section">
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>
        




    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
          <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
            <h5 class="col-header smaller">Disclaimers ?</h5>
              <label class="switch-light switch-candy">
                <input door_type="<?php echo basename(__FILE__, '.php'); ?>" type="checkbox" name="checkbox_tgpd_glass_reminders_add_disclamers_pactio" id="checkbox_tgpd_glass_reminders_add_disclamers" value="disclamers" >
                  <span>
                    <span>No</span>
                    <span>Yes</span>
                    <a></a>
                  </span>
              </label>
          </div>

          <div class="col-sm-12 col-md-12 dwmargin">
            <div class="row">
              <div class="col-xs-12">
              <fieldset id="fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers" style="display: none;" >
                <select door_type="<?php echo basename(__FILE__, '.php'); ?>"  class="form-control" id="select_tgpd_glass_disclamers" name="select_tgpd_glass_disclamers">
                   <option value="not_applicable">Disclaimers</option>
                  <option value="Wood Bead">Wood Bead</option>
                  <option value="Painted Frames (AFTERMARKET)">Painted Frames (AFTERMARKET)</option>
                  <option value="TBD">TBD</option>
                  <option value="select_tgpd_glass_disclamers_for_text">Other</option>
                </select>
                </fieldset>
              </div>
            </div>
          </div>
        
        <div class="col-sm-12 col-md-12 leftalign dwmargin">
          <div class="row">
            <div class="col-sm-12 damage_comment">
              <fieldset id="fieldset_tgpd_glass_disclamers_reminder_section" style="display: none;">
                <input id="text_tgpd_glass_disclamers_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_disclamers_reminder_section">
              </fieldset>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Inside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="text_lift_inside_position" value="lift_inside" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                    <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_inside_section1" style="display: none;">
                           <input id="text_tgpd_glass_inside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_inside_lift_with_glass_type" type="text" placeholder="Lift Inside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
        
    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Outside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders_patio" id="text_lift_outside_position" value="lift_outside" door_type="<?php echo basename(__FILE__, '.php'); ?>">
                    <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_outside" style="display: none;">
                           <input id="text_tgpd_glass_outside_lift_with_glass_type" class="form-control text-num"  name="text_tgpd_glass_outside_lift_with_glass_type" type="text" placeholder="Lift Outside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div></div>
               </div>
            </div>
            </div>
            </div>
            </div>
              </div>
  </div>
</div>
