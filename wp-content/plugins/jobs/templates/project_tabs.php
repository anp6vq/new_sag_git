<h2>Select Job Pieces</h2>
<ul class="nav nav-tabs nav-justified" role="tablist">
  <li role="presentation" class="active" id="glass-piece-detailsset"><a href="#glass-piece-details" aria-controls="glass-piece-details" role="tab" data-toggle="tab">Glass Pieces</a></li>
  <li role="presentation" id="door-repairsset"><a class="" href="#door-repairs" aria-controls="door-repairs" role="tab" data-toggle="tab">Repair Doors</a></li>
  <li role="presentation" id="door-newset"><a class="" href="#door-new" aria-controls="window-screen-details" role="tab" data-toggle="tab">New Doors</a></li>
  <li role="presentation" id="door-noteset"><a class="" href="#door-note" aria-controls="window-screen-details" role="tab" data-toggle="tab">Notes</a></li>
</ul>

<!-- Tab panes -->
<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="glass-piece-details">
    <?php include 'tab_glass_piece_details.php' ?>
  </div>
  <div role="tabpanel" class="tab-pane fade" id="door-repairs">
    <?php include 'doors/repair/door-repair-home.php' ?>
  </div>
  <div role="tabpanel" class="tab-pane fade new_door_section" id="door-new">
    <?php include 'doors/new/door-new-home.php' ?>
  </div>
   <div role="tabpanel" class="tab-pane fade" id="door-note" style="display: none;">
    <?php include 'doors/Notes/door-notes.php' ?>
  </div>
</div>

<!--
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/css/bootstrap-select.min.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>
<script>
  $(document).ready(function(){
    $('select.form-control').selectpicker({
    style: 'btn-infos',
    size: 4
    })
  });
</script>-->

<script type="text/javascript">
 jQuery(window).load(function() {
 
 
    $('#door-repairs select.form-control').on('change', function() {
		
     
    });
	
	
	$( "body" ).delegate( "#door-repairs select.form-control", "change", function() {
  
  	 if(this.value == 'not_applicable'){
        $(this).css('background','#ffffff');
        $(this).css('color','#555555');
      }else{
        $(this).css('background','#5cb85c');
        $(this).css('color','#ffffff');
      }
  
});

	$( "body" ).delegate( "#door-repairs select.form-control", "change", function() {

    
	
	});
    $('#door-repairs select.form-control').each(function(){
	//alert(this.value);
      if(this.value == 'not_applicable'){
        $(this).css('background','#ffffff');
        $(this).css('color','#555555');
      }else{
        $(this).css('background','#5cb85c');
        $(this).css('color','#ffffff');
      }
    });
    
    
    $('#door-new select.form-control').on('change', function() {
      if(this.value == 'not_applicable'){
        $(this).css('background','#ffffff');
        $(this).css('color','#555555');
      }else{
        $(this).css('background','#5cb85c');
        $(this).css('color','#ffffff');
      }
    });
    
    $('#door-new select.form-control').each(function(){
      if(this.value == 'not_applicable'){
        $(this).css('background','#ffffff');
        $(this).css('color','#555555');
      }else{
        $(this).css('background','#5cb85c');
        $(this).css('color','#ffffff');
      }
    });
    
  }); 
</script>
<style>
select#select-new-door-category.form-control,
select#select-new-door-type.form-control,
select#select-repair-door-type.form-control,
select#select-repair-door-category.form-control{
	background:#ffffff!important;
	color:#555555!important;
}

.btn.btn-infos{
    background:#ffffff;
    border:1px solid #cccccc;
    border-radius:4px;
    box-shadow:0px 1px 1px rgba(0, 0, 0, 0.075) inset;
}
.btn.btn-infos:focus{
    border:1px solid #CC3300;
}
.user-success .btn.btn-infos{
    background:#5cb85c!important;
    border:1px solid #5cb85c;
    color:#ffffff!important;
}
.bootstrap-select .dropdown-toggle:focus {
    outline:none!important;
}
</style>

<script type="text/javascript">
 jQuery(window).load(function() {
    
    glass_piece_disabled();
    repair_doors_disable();
    new_doors_disable();
    jQuery('#btn_new_door_add').click(function(){
        new_doors_enabled();
    });
    jQuery('#btn_repair_add').click(function(){
        repair_doors_enabled();
    });
    
     
 })
 </script>