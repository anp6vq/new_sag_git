<option value="not_applicable">Tape type</option>
<option value="1_over_8_440">1/8 440 tape</option>
<option value="1_over_4_440">1/4 440 tape</option>
<option value="1_over_2_440">1/2 440 tape</option>
<option value="3_over_4_440">3/4 440 tape</option>
<option value="foam">Foam tape</option>
<option value="double_faced">Double faced tape</option>
<option value="cladding">Cladding tape</option>
