<!--sag 155-->
<option value="not_applicable">Glass color</option>
<option value="1-CLEAR" selected>1-CLEAR</option>
<option value="2-GREEN">2-GREEN</option>
<option value="3-GRAY">3-GRAY</option>
<option value="4-BRONZE">4-BRONZE</option>
<option value="6-BLUE.GREEN">6-BLUE.GREEN</option>
<option value="7-AZURIA">7-AZURIA</option>
<option value="8-EVERGREEN">8-EVERGREEN</option>
<option value="13-STARFIRE (low iron)">13-STARFIRE (low iron)</option>
<option value="19-CRYSTALGRAY">19-CRYSTALGRAY</option>
<option value="24-OPTIWHITE (low iron)">24-OPTIWHITE (low iron)</option>
<option value="26-SOLARBLUE">26-SOLARBLUE</option>
<option value="27-PACIFICA">27-PACIFICA</option>
<option value="29-GRAPHITE ">29-GRAPHITE</option>
<option value="30-OPTIGRAY">30-OPTIGRAY</option>
