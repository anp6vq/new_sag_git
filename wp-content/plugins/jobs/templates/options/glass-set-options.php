<option value="not_applicable">Glass set</option>
<option value="snap_beads_aluminium_interior">Snap Beads, Aluminium, Interior</option>
<option value="snap_beads_vinyl_interior">Snap Beads,Vinyl, Interior </option>
<option value="snap_beads_aluminium_exterior">Snap Beads, Aluminium, Exterior </option>
<option value="snap_beads_vinyl_exterior">Snap Beads, Vinyl, Exterior </option>
<option value="snap_beads_wood_interior">Snap Beads, Wood, Interior </option>
<option value="snap_beads_wood_exterior">Snap Beads, Wood, Exterior </option>
<option value="flush_1_over_4_top_exterior">Flush 1/4, Top, Exterior </option>
<option value="flush_1_over_4_top_interior">Flush 1/4, Top, Interior</option>
<option value="flush_1_over_4_bottom_exterior">Flush 1/4, Bottom, Exterior</option>
<option value="flush_1_over_4_bottom_interior">Flush 1/4, Bottom, Interior</option>
<option value="flush_1_over_4_left_right_exterior">Flush 1/4, Left/Right, Exterior</option>
<option value="flush_1_over_4_left_right_interior">Flush 1/4, Left/Right, Interior</option>
<option value="flush_igu_top_exterior">Flush (IGU), Top, Exterior</option>
<option value="flush_igu_top_interior">Flush (IGU), Top, Interior</option>
<option value="flush_igu_bottom_exterior">Flush (IGU), Bottom, Exterior</option>
<option value="flush_igu_bottom_interior">Flush (IGU), Bottom, Interior</option>
<option value="flush_igu_left_right_exterior">Flush (IGU), Left/Right, Exterior</option>
<option value="flush_igu_left_right_interior">Flush (IGU), Left/Right, Interior</option>
<option value="wrap_around_window_vinyl_single_pane">Wrap Around *WINDOW* Vinyl (Single Pane)</option>
<option value="wrap_around_window_aluminum_single_pane">Wrap Around *WINDOW* Aluminum (Single Pane)</option>
<option value="wrap_around_window_wood_single_pane">Wrap Around *WINDOW* Wood (Single Pane)</option>
<option value="wrap_around_window_vinyl_igu">Wrap Around *WINDOW* Vinyl (IGU) </option>
<option value="wrap_around_window_aluminum_igu">Wrap Around *WINDOW* Aluminum (IGU)</option>
<option value="wrap_around_window_wood_igu">Wrap Around *WINDOW* Wood (IGU)</option>
<option value="wrap_around_patio_door_single_pane">Wrap Around *PATIO DOOR*  (Single Pane)</option>
<option value="wrap_around_patio_door_igu">Wrap Around *PATIO DOOR*  (IGU)</option>
<option value="curtain_wall_all_sides_captured_exterior">Curtain Wall, All Sides Captured, Exterior</option>
<option value="curtain_wall_all_sides_captured_interior">Curtain Wall, All Sides Captured, Interior</option>
<option value="curtain_wall_butt_glazed_left_only">Curtain Wall, Butt Glazed, Left Only </option>
<option value="curtain_wall_butt_glazed_right_only">Curtain Wall, Butt Glazed, Right Only </option>
<option value="curtain_wall_butt_glazed_left_right">Curtain Wall, Butt Glazed, Left/Right</option>
<option value="curtain_wall_butt_glazed_top_only">Curtain Wall, Butt Glazed, Top Only </option>
<option value="curtain_wall_butt_glazed_bottom_only">Curtain Wall, Butt Glazed, Bottom Only</option>
<option value="curtain_wall_butt_glazed_top_bottom">Curtain Wall, Butt Glazed, Top/Bottom </option>
<option value="structurally_glazed_all_sides_butt_glazed">Structurally Glazed, 4 Sided Butt Glazed </option>
<option value="zipper_wall">Zipper wall </option>
<option value="screw_beads">Screw Beads </option>
<option value="u_channel_b_t">U-channel, Bottom (1x1), Top (2x1)</option>
<option value="sash_and_division_bar">Sash & Division Bar </option>
<option value="sash_single_pane">Sash (Single Pane) </option>
<option value="sagh_igu">Sash (IGU)</option>
<option value="lug_sash">Lug Sash</option>
<option value="hack_and_glaze_steel_interior">Hack & Glaze, Steel, Interior</option>
<option value="hack_and_glaze_steel_exterior">Hack & Glaze, Steel, Exterior</option>
<option value="hack_and_glaze_wood_interior">Hack & Glaze, Wood, Interior</option>
<option value="hack_and_glaze_wood_exterior">Hack & Glaze, Wood, Exterior</option>
<option value="table_top">Table Top</option>
<option value="stand_Off">Stand Off’s</option>
<option value="lay_in_canopy">Lay In (Canopy)</option>
<option value="handrail">Handrail </option>
<option value="vision_kit_all_new">Vision Kit (ALL NEW)</option>
<option value="vision_kit_glass_only">Vision Kit (GLASS ONLY)</option>
<option value="j_channel_b">J-Channel (BOTTOM)</option>
<option value="j_channel_t">J-Channel (TOP)</option>
<option value="j_channel_t_b">J-Channel (TOP/BOTTOM)</option>
<option value="j_channel_t_b_l_r">J-Channel (T/B/L/R)</option>
<option value="l_channel_b">L-Channel (BOTTOM) </option>
<option value="l_channel_t">L-Channel (TOP)</option>
<option value="l_channel_t_b">L-Channel (TOP/BOTTOM)</option>
<option value="l_channel_t_b_l_r">L-Channel (T/B/L/R)</option>
<option value="caulk_mastic_only">Caulk/Mastic Only</option>

