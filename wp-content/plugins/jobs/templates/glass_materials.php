<div id="materials-wrapper">
  <div class="form-group row">
    <h3 class="header-outside-col">Glass Installation</h3>
    <hr>
    <h5 class="header-outside-col" style="font-size:22px; font-weight:bold;">Materials</h5>
    <div class="col-md-4 caulk-col">
      <div class="row sub-row">
        <div class="col-md-6 col-sm-6">
          <h5 class="col-header">Caulk?</h5>
        </div>
        <div class="col-md-6 col-sm-6">
          <label class="switch-light switch-candy" onclick="">
            <input type="checkbox" class="caulk-checkbox">
            <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
            </span>
          </label>
        </div>
      </div>
    </div> <!-- end col-md-4-->
    <div class="col-md-4 scaffold-col">
      <div class="row sub-row">
        <div class="col-md-6 col-sm-6">
          <h5 class="col-header">Scaffolding/Lifts?</h5>
        </div>
        <div class="col-md-6 col-sm-6">
          <label class="switch-light switch-candy" onclick="">
            <input type="checkbox" class="scaffold-checkbox">
            <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
            </span>
          </label>
        </div>
      </div>
    </div>
    <div class="col-md-4 tape-col">
      <div class="row sub-row ">
        <div class="col-md-6 col-sm-6">
          <h5 class="col-header">Tape?</h5>
        </div>
        <div class="col-md-6 col-sm-6">
          <label class="switch-light switch-candy" onclick="">
            <input type="checkbox" class="tape-checkbox">
            <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
            </span>
          </label>
        </div>
      </div>
    </div>
  </div>
</div> <!-- end materials wrapper -->
