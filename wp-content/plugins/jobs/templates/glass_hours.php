<div class="row sub-row">
  <label>Labor</label>
  <div class="col-md-6">
    <select id="glasses-hours-type" class="form-control">
        <option value="" disabled selected>Select Hours Type </option>
        <option value="regular">
          Regular
        </option>
        <option value="overtime">
          Overtime
        </option>
        <option value="weekend">
          Weekend
       </option>
    </select>
  </div>
  <div class="col-md-6">
    <div class="row sub-row">
      <div class="col-md-6">
        <input id="glasses-number-hours" class="form-control" type="text" placeholder="# Hours">
      </div>
      <div class="col-md-6">
        <input id="glasses-number-men" class="form-control" type="text" placeholder="# Men">
      </div>
    </div>
  </div>
</div>
