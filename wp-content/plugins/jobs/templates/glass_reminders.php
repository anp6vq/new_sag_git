<div id="reminders-wrapper">
  <div class="form-group">
    <div class="col-md-6 col-md-offset-0 col-sm-12 col-sm-offset-0">
      <h3>Instructions</h3>
      <hr>
      <textarea type="text" class="form-control" id="project-instructions" placeholder="Write down any special instructions about project."></textarea>
      <?php include 'glass_hours.php'; ?>
    </div>
    <div class="col-md-6 col-md-offset-0 col-sm-12 col-sm-offset-0">
      <h3>Reminders</h3>
      <hr>
      <div class="row sub-row reminder-row">
        <div class="col-md-9 col-sm-6">
          <h5 class="reminder-title">Will Glass Fit in Elevator ? </h5>
        </div>
        <div class="col-md-3 col-sm-6">
          <label class="switch-light switch-candy" onclick="">
            <input type="checkbox">
            <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
            </span>
          </label>
        </div>
      </div>
      <div class="row sub-row reminder-row">
        <div class="col-md-9 col-sm-6">
          <h5 class="reminder-title">Will furniture have to be moved ? </h5>
        </div>
        <div class="col-md-3 col-sm-6">
          <label class="switch-light switch-candy" onclick="">
            <input type="checkbox">
            <span>
              <span>No</span>
              <span>Yes</span>
              <a></a>
            </span>
          </label>
        </div>
      </div>
      <div class="reminder-row-wrapper">
        <div class="row sub-row reminder-row">
          <div class="col-md-9 col-sm-6">
            <h5 class="reminder-title">Any walls/ceilings need to be cut ? </h5>
          </div>
          <div class="col-md-3 col-sm-6 ceiling-col">
            <label class="switch-light switch-candy" onclick="">
              <input class="ceiling-checkbox" type="checkbox">
              <span>
                <span>No</span>
                <span>Yes</span>
                <a></a>
              </span>
            </label>
          </div>
        </div>
      </div>
      <div class="reminder-row-wrapper">
        <div class="row sub-row reminder-row">
          <div class="col-md-9 col-sm-6">
            <h5 class="reminder-title"> Solar Film ? </h5>
          </div>
          <div class="col-md-3 col-sm-6">
            <label class="switch-light switch-candy" onclick="">
              <input type="checkbox" class="solarfilm-checkbox">
              <span>
                <span>No</span>
                <span>Yes</span>
                <a></a>
              </span>
            </label>
          </div>
        </div>
      </div>
      <div class="reminder-row-wrapper">
        <div class="row sub-row reminder-row">
          <div class="col-md-9 col-sm-6">
            <h5 class="reminder-title"> Wetseal? </h5>
          </div>
          <div class="col-md-3 col-sm-6">
            <label class="switch-light switch-candy" onclick="">
              <input class="wetseal-checkbox" type="checkbox">
              <span>
                <span>No</span>
                <span>Yes</span>
                <a></a>
              </span>
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
