<div id="doors-wrapper">
   <h4>Doors: (<span id="doors-count"><?php /* {{doorsCount}} */ ?></span> Total)</h4>
   <hr>
   <div class="button-wrapper row">
      <div class="col-md-2">
         <button id="add-new-door" class="btn btn-success btn-lg"><i class="fa fa-plus"></i> New Door</button>
      </div>
      <div class="col-md-2">
         <button id="duplicate-specs" class="btn btn-info btn-lg"><i class="fa fa-plus"></i>Duplicate Specs</button>
      </div>
      <div class="col-md-2">
         <button id="duplicate-specs-size" class="btn btn-warning btn-lg"><i class="fa fa-plus"></i>Duplicate Specs & Size</button>
      </div>
   </div>
   <div id="doors-pieces-wrapper">
      <div class="form-group glass-piece-row">
         <h5 class="door-number">Door <b>1</b> <i class="fa fa-trash-o right remove-piece"></i></h5>
         <hr>
         <div class="form-group row">
          <div class="col-md-2">
            <input type="radio" id="sf-door-a" class="sf-door-a sf-door image-radio" name="sf-door" checked/>
             <label for="sf-door-a"><span></span></label>
          </div>
          <div class="col-md-2">
             <input type="radio" id="sf-door-b" class="sf-door-b sf-door image-radio" name="sf-door" checked/>
             <label for="sf-door-b"><span></span></label>
           </div>
           <div class="col-md-2">
             <input type="radio" id="sf-door-c" class="sf-door-c sf-door image-radio" name="sf-door" checked/>
             <label for="sf-door-c"><span></span></label>
           </div>
           <div class="col-md-2">
             <input type="radio" id="sf-door-d" class="sf-door-d sf-door image-radio" name="sf-door" checked/>
             <label for="sf-door-d"><span></span></label>
           </div>
           <div class="col-md-2">
             <input type="radio" id="sf-door-e" class="sf-door-e sf-door image-radio" name="sf-door" checked/>
             <label for="sf-door-e"><span></span></label>
           </div>
            <div class="col-md-2">
             <input type="radio" id="sf-door-f" class="sf-door-f sf-door image-radio" name="sf-door" checked/>
             <label for="sf-door-f"><span></span></label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
               <div class="row sub-row">
                  <div class="col-md-6 col-sm-6">
                    <div class="row sub-row">
                      <label>Frame Opening</label>
                      <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="sf-frame-opening-width" placeholder="Width">
                      </div>
                      <div class="col-md-6 col-sm-6">
                       <input type="text" class="form-control" id="sf-frame-opening-height" placeholder="Height">
                      </div>
                      <span class="x">X</span>
                    </div>
                  </div>
                  <div class="col-md-6 sol-sm-6">
                    <div class="row sub-row">
                      <label>Door Size</label>
                      <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="door-size-width" placeholder="Width">
                      </div>
                      <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="door-size-height" placeholder="Height">
                      </div>
                      <span class="x">X</span>
                    </div>
                 </div>
               </div>
               <input type="file" accept="image/*" />
               <?php include 'doors_hours.php'; ?>
            </div>

            <div class="col-md-6">
               <div class="row sub-row">
                </div>
                <div id="canvas-wrapper">
                  <h5 class="header-with-icon sketch-title"><i class="fa fa-pencil"></i> Sketch Diagram or Fabrication.</h5>
                  <canvas class="simple_sketch"></canvas>
                </div>
              </div>
            </div>
          </div>
  </div>
</div>
