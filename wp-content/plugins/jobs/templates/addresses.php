<div class="address-wrapper row">
  <div class="col-md-6 col-sm-12">
    <div class="form-group">
      <h3 class="adjust-height">Billing Address </h3>
      <hr>
      <label for="text_project_postal_code" class="control-label">Bill To Name</label>
      <div class="controls">
          <input id="billtoid_data" name="billtoid" class="form-control" type="text" placeholder="Billing Name" >
      </div>
    </div>
    <div class="form-group">

        <label for="text_billing_address_line1" class="control-label">Address Line 1</label>
        <div class="controls">
            <input id="text_billing_address_line1" name="text_billing_address_line1" class="form-control" type="text" autocomplete="on" maxlength="255" required placeholder="Address">
        </div>
    </div>
    <!-- address-line2 input-->
    <div class="form-group">
      <div class="row sub-row">
        <div class="col-md-6">
          <label for="text_billing_address_line2" class="control-label">Address Line 2</label>
          <div class="controls">
              <input id="text_billing_address_line2" name="text_billing_address_line2" class="form-control" type="text" autocomplete="on" maxlength="255" placeholder="Apt #">
           </div>
         </div>
         <div class="col-md-6">
           <label for="text_billing_city" class="control-label">City</label>
           <div class="controls">
               <input id="text_billing_city" name="text_billing_city" class="form-control" type="text" autocomplete="on" maxlength="30" required placeholder="city">
               <p class="help-block"></p>
           </div>
         </div>
       </div>
    </div>
    <div class="row sub-address-wrapper">
      <div class="form-group col-md-3 col-sm-3">
          <label for="select_billing_state" class="control-label">State</label>
          <div class="controls">
            <select id="select_billing_state" name="select_billing_state" class="form-control">
              <option value="not_applicable" disabled selected>State</option>
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
            </select>
          </div>
      </div>
      <div class="form-group col-md-3 col-sm-3">
        <label for="text_billing_postal_code" class="control-label">Zipcode</label>
        <div class="controls">
            <input id="text_billing_postal_code" name="text_billing_postal_code" class="form-control" type="number" autocomplete="on" min="1" max="999999999" maxlength="10" required placeholder="zipcode">
        </div>
      </div>
      <!--<div class="form-group col-md-6 col-sm-6">
        <label for="text_billing_email" class="control-label">Billing Email Address</label>
        <input id="text_billing_email" name="text_billing_email" class="form-control" type="email" autocomplete="on" maxlength="255" required placeholder="Project Email Address">

      </div>-->



	   <div class="form-group col-md-6 col-sm-6">

      </div>
      <!-- country select -->
    </div>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="form-group">
      <h3 class="adjust-height">Job Address <button id="button_same_addresses" class="same-address-label btn btn-sm btn-primary">Same As Billing?</button></h3>
      <hr>
        <label for="text_project_postal_code" class="control-label">Location Name</label>
        <div class="controls">
            <input id="addresslocation_data" name="addresslocation" class="form-control" placeholder="Location Name" type="text" >
        </div>
      </div>
    <div class="form-group">
        <label for="text_project_address_line1" class="control-label">Address Line 1</label>
        <div class="controls">
            <input id="text_project_address_line1" name="text_project_address_line1" class="form-control" type="text" autocomplete="on" maxlength="255" required placeholder="Address">
        </div>
    </div>
    <!-- address-line2  input-->
    <div class="form-group">
      <div class="row sub-row">
        <div class="col-md-6">
          <label for="text_project_address_line2" class="control-label">Address Line 2</label>
          <div class="controls">
              <input id="text_project_address_line2" name="text_project_address_line2" class="form-control" type="text" autocomplete="on" maxlength="255" placeholder="Apt #">
          </div>
        </div>
        <div class="col-md-6">
          <label for="text_project_city" class="control-label">City</label>
          <div class="controls">
              <input id="text_project_city" name="text_project_city" class="form-control" type="text" autocomplete="on" maxlength="30" required placeholder="city">
              <p class="help-block"></p>
          </div>
        </div>
      </div>
    </div>
    <div class="row sub-address-wrapper">
      <div class="form-group col-md-3 col-sm-3">
          <label for="select_project_state" class="control-label">State</label>
          <div class="controls">
            <select id="select_project_state" name="select_project_state" class="form-control">
              <option value="not_applicable" disabled selected>State</option>
              <option value="AL">Alabama</option>
              <option value="AK">Alaska</option>
              <option value="AZ">Arizona</option>
              <option value="AR">Arkansas</option>
              <option value="CA">California</option>
              <option value="CO">Colorado</option>
              <option value="CT">Connecticut</option>
              <option value="DE">Delaware</option>
              <option value="DC">District of Columbia</option>
              <option value="FL">Florida</option>
              <option value="GA">Georgia</option>
              <option value="HI">Hawaii</option>
              <option value="ID">Idaho</option>
              <option value="IL">Illinois</option>
              <option value="IN">Indiana</option>
              <option value="IA">Iowa</option>
              <option value="KS">Kansas</option>
              <option value="KY">Kentucky</option>
              <option value="LA">Louisiana</option>
              <option value="ME">Maine</option>
              <option value="MD">Maryland</option>
              <option value="MA">Massachusetts</option>
              <option value="MI">Michigan</option>
              <option value="MN">Minnesota</option>
              <option value="MS">Mississippi</option>
              <option value="MO">Missouri</option>
              <option value="MT">Montana</option>
              <option value="NE">Nebraska</option>
              <option value="NV">Nevada</option>
              <option value="NH">New Hampshire</option>
              <option value="NJ">New Jersey</option>
              <option value="NM">New Mexico</option>
              <option value="NY">New York</option>
              <option value="NC">North Carolina</option>
              <option value="ND">North Dakota</option>
              <option value="OH">Ohio</option>
              <option value="OK">Oklahoma</option>
              <option value="OR">Oregon</option>
              <option value="PA">Pennsylvania</option>
              <option value="RI">Rhode Island</option>
              <option value="SC">South Carolina</option>
              <option value="SD">South Dakota</option>
              <option value="TN">Tennessee</option>
              <option value="TX">Texas</option>
              <option value="UT">Utah</option>
              <option value="VT">Vermont</option>
              <option value="VA">Virginia</option>
              <option value="WA">Washington</option>
              <option value="WV">West Virginia</option>
              <option value="WI">Wisconsin</option>
              <option value="WY">Wyoming</option>
            </select>
          </div>
      </div>
      <div class="form-group col-md-3 col-sm-3">
        <label for="text_project_postal_code" class="control-label">Zipcode</label>
        <div class="controls">
            <input id="text_project_postal_code" name="text_project_postal_code" class="form-control" type="number" autocomplete="on" min="1" max="999999999" maxlength="10" required placeholder="zipcode">
        </div>
      </div>
      
        
	  
      
      
      <!--<div class="form-group col-md-6 col-sm-6">
	  
	  <label for="select_project_status">Job status</label>
          <?php
           if ( !is_super_admin() ) {
              ?>
              <select disabled id="select_project_status" class="form-control">
                <option value="not applicable" disabled selected>Please select project status</option>
                <option value="new ticket" selected>New Ticket</option>
                <option value="research">Research</option>
                <option value="submitted">Submitted</option>
                <option value="quote">Quote</option>
                <option value="na">N/A</option>
              </select>
              <?php
          } else {
           ?>
           <select id="select_project_status" class="form-control">
                <option value="not applicable" disabled selected>Please select project status</option>
                <option value="new ticket" selected>New Ticket</option>
                <option value="research">Research</option>
                <option value="submitted">Submitted</option>
                <option value="quote">Quote</option>
                 <option value="na">N/A</option>
              </select>
            <?php
          }?>
		</div>-->  
        <!--<label for="text_project_email" class="control-label">Job Email Address</label>
        <input id="text_project_email" name="text_project_email" class="form-control" type="email" autocomplete="on" maxlength="255" required placeholder="Job Email Address">-->
      

      <!-- country select -->


    </div>
  </div>
  
</div>