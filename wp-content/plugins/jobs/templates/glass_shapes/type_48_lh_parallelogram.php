<div id="tgpd-<?php echo basename(__FILE__, '.php'); ?>-container" class="container-fluid shape-container"  style="display:none;">
  <div class="form-group door-form-group">
    <div class="col-sm-6 door-diagram-col">
      <div class="door-diagram-img-wrapper">
        <img src="<?php echo plugins_url(); ?>/jobs/img/glass_shapes/type_48_lh_parallelogram.jpg" class="door-diagram-img" />
      </div>
      
    </div>
    <div class="col-sm-6 door-fields-col">
      <div class="fields-sub-form-group form-group">
	  <div class="row">
        <div class="col-sm-12 fields-left-half">
		<div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Height (H)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_height_48_lh_parallelogram" class="form-control text_tagd_shape_base" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		  <div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Width (W)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_width_48_lh_parallelogram" class="form-control text_tagd_shape_base" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		  <div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Left Leg (LL)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_left_leg_48_lh_parallelogram" class="form-control text_tagd_shape_base" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		  <div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Right Offset (RO)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_right_offset_48_lh_parallelogram" class="form-control text_tagd_shape_base" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		  <div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Base (B)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_base_48_lh_parallelogram" class="form-control text_tagd_shape_base" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		  
		
		  
		 
		  
		  
		  
		  
		   
		  
		   
		   
		  <style>
		  .input-group.my_inc .input-group-addon{
		  		border:none!important;
				background:none!important;
		   }
		   .input-group.my_inc input.form-control{
		   border-radius:3px;
		   }
		  </style>
		  
		  
		  
          
          

          
          
          
          
          
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
