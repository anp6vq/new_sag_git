<div id="tgpd-<?php echo basename(__FILE__, '.php'); ?>-container" class="container-fluid shape-container"  style="display:none;">
  <div class="form-group door-form-group">
    <div class="col-sm-6 door-diagram-col">
      <div class="door-diagram-img-wrapper">
        <img src="<?php echo plugins_url(); ?>/jobs/img/glass_shapes/type_4_lh_angle_corner.jpg" class="door-diagram-img" />
      </div>
      
    </div>
    <div class="col-sm-6 door-fields-col">
      <div class="fields-sub-form-group form-group">
	  <div class="row">
        <div class="col-sm-12 fields-left-half">
		<div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Right Leg (RL)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_right_leg_4_lh_angle_corner" class="form-control text_tagd_shape_right_leg" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		  <div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Base (B)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_base_4_lh_angle_corner" class="form-control text_tagd_shape_width" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		<div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Left Leg (LL)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_left_leg_4_lh_angle_corner" class="form-control text_tagd_shape_left_leg" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		
		  
		 
		  
		  <div class="field-wrapper">
            <div class="row door-field-status-row">
             
              <div class="col-sm-7">
              </div>
            </div>
            
			<label>Top (T)</label>
		  <div class="input-group my_inc">
			<input id="text_tagd_shape_top_4_lh_angle_corner" class="form-control text_tagd_shape_top" type="text" />
            <div class="input-group-addon">Inches</div>
          </div>
		  
          </div>
		   
		  <style>
		  .input-group.my_inc .input-group-addon{
		  		border:none!important;
				background:none!important;
		   }
		   .input-group.my_inc input.form-control{
		   border-radius:3px;
		   }
		  </style>
		  
		  
		  
          
          

          
          
          
          
          
        </div>
        </div>
      </div>
    </div>
  </div>
</div>
