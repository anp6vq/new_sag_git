<div class="form-horizontal">
   <div class="form-group">
      <div class="col-xs-12">
         <div class="input-group">
            <div class="input-group-btn">
               <button id="btn_twmd_first" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_previous" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
               </button>
            </div>
            <input id="text_twmd_item_count" type="text" class="form-control text-center" placeholder="0/0" value="0/0">
            <div class="input-group-btn">
               <button id="btn_twmd_next" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_last" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_add" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_save" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_delete" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_refresh" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_copy" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
               </button>
               <button id="btn_twmd_paste" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-paste" aria-hidden="true"></span>
               </button>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="radio_twmd_door_type">Door Type</label>
         <ul class="list-group">
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_door_type" id="radio_twmd_door_type_not_a_value" value="not_a_value" checked>No answer
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_door_type" id="radio_twmd_door_type_hollow_metal_18ga" value="hollow_metal_18ga">Hollow metal (18 GA)
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_door_type" id="radio_twmd_door_type_solid_core_wood" value="solid_core_wood">Solid core wood
                        </label>
                     </div>
                  </div>
                  <div class="col-sm-9">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <input id="text_twmd_door_type_veneer" type="text" class="form-control" placeholder="Veneer">
                        </div>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_door_type" id="radio_twmd_door_type_other" value="other">Other
                        </label>
                     </div>
                  </div>
                  <div class="col-sm-9">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <input id="text_twmd_door_type_other" type="text" class="form-control" placeholder="Specify">
                        </div>
                     </div>
                  </div>
               </div>
            </li>
         </ul>
      </div>
   </div>
   <div class="form-group">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_dimension_of_framewidth">Dimension of Frame Width</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Inside dimension of frame width (e.g. 30 inches)">
            <input id="text_twmd_dimension_of_framewidth" type="text" class="form-control" placeholder="e.g. 30">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_actual_door_height">Actual Door Height</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Actual door height (e.g. 83 1/8 inches)">
            <input id="text_twmd_actual_door_height" type="text" class="form-control" placeholder="e.g. 83 1/8">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_head_to_hinge_top_1">Head to Hinge Top #1</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Underside of frame head to top of hinge cutout #1">
            <input id="text_twmd_head_to_hinge_top_1" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_head_to_hinge_top_2">Head to Hinge Top #2</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Underside of frame head to top of hinge cutout #2">
            <input id="text_twmd_head_to_hinge_top_2" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_head_to_hinge_top_3">Head to Hinge Top #3</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Underside of frame head to top of hinge cutout #3"">
            <input id="text_twmd_head_to_hinge_top_3" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_head_to_frame_lock">Head to Frame Lock</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Underside of frame head to center-left of frame lock prep">
            <input id="text_twmd_head_to_frame_lock" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_head_to_dead_bolt">Head to Deadbolt</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Underside of frame head to center-left of deadbolt cutout">
            <input id="text_twmd_head_to_dead_bolt" type="text" class="form-control" placeholder="">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_twmd_lock_backset">Lock Backset</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Lock backset (e.g. 2 3/4 inches or 2 3/8 inches)">
            <input id="text_twmd_lock_backset" type="text" class="form-control" placeholder="e.g. 2 3/4 or 2 3/8">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 col-xs-12">
         <label for="text_twmd_door_thickness">Door Thickness</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" title="Door thickness (e.g. 1 3/4 inches or 1 3/8 inches">
            <input id="text_twmd_door_thickness" type="text" class="form-control" placeholder="e.g. 1 3/4 or 1 3/8">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
      <div class="col-sm-6 col-xs-12">
         <label for="text_twmd_hinge_height">Hinge Height</label>
         <div class="input-group" data-toggle="tooltip" data-placement="auto bottom" data-animation="true" title="Hinge height (e.g. 4 1/2 inches">
            <input id="text_twmd_hinge_height" type="text" class="form-control" placeholder="e.g. 4 1/2">
            <div class="input-group-addon">Inches</div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="radio_group_twmd_door_hand">Door Hand</label>
         <div id="radio_group_twmd_door_hand" class="btn-group btn-group-justified"" data-toggle="buttons">
            <label class="btn btn-default active">
               <input type="radio" name="radio_twmd_door_hand" id="radio_twmd_door_hand_lh" autocomplete="off" value="lh" checked>
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/lh.png" width="102px" height="102px" alt="LH">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_twmd_door_hand" id="radio_twmd_door_hand_lhr" autocomplete="off" value="lhr">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/lhr.png" width="102px" height="102px" alt="LHR">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_twmd_door_hand" id="radio_twmd_door_hand_rh" autocomplete="off" value="rh">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/rh.png" width="102px" height="102px" alt="RH">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_twmd_door_hand" id="radio_twmd_door_hand_rhr" autocomplete="off" value="rhr">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/rhr.png" width="102px" height="102px" alt="RHR">
            </label>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-md-4">
         <label for="select_twmd_fire_rating">Fire Rating</label>
         <select id="select_twmd_fire_rating" class="form-control">
            <option value="not_a_value" disabled selected>Please select fire rating</option>
            <option value="20_min">20 Minutes</option>
            <option value="45_min">45 Minutes</option>
            <option value="60_min">60 Minutes</option>
            <option value="90_min">90 Minutes</option>
            <option value="3_hr">3 Hours</option>
         </select>
      </div>
      <div class="col-md-4">
        <label for="radio_twmd_lite_kit">Lite Kit (Exposed Glass)</label>
         <select id="radio_twmd_lite_kit" name="radio_twmd_lite_kit">
           <option id="radio_twmd_lite_kit_none" value="none">None</option>
           <option id="radio_twmd_lite_kit_10x10" value="10x10">10 x 10 inches</option>
           <option id="radio_twmd_lite_kit_5x10" value="5x10">5 x 10 inches</option>
           <option id="radio_twmd_lite_kit_4x25" value="4x25">4 x 25 inches</option>
           <option id="radio_twmd_lite_kit_22x28" value="22x28">22 x 28 inches Half Glass</option>
           <option id="radio_twmd_lite_kit_full_glass" value="full_glass">Full glass</option>
           <option id="radio_twmd_lite_kit_other" value="other">Other</option>
         </select>
         <input id="text_twmd_lite_kit_other" type="text" class="form-control" placeholder="Specify">
      </div>
   </div>

   <div class="form-group">
      <div class="col-xs-12">
         <label for="radio_twmd_louver_size">Louver Size</label>
         <ul class="list-group">
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_not_a_value" value="not_a_value" checked>No answer
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_none" value="none" checked>None
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_12x12" value="12x12">12 x 12 inches
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_12x24" value="12x24">12 x 24 inches
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_24x24" value="24x24">24 x 24 inches
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_full_louver" value="full_louver">Full louver
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_louver_size" id="radio_twmd_louver_size_other" value="other">Other
                        </label>
                     </div>
                  </div>
                  <div class="col-sm-9">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <input id="text_twmd_louver_size_other" type="text" class="form-control" placeholder="Specify">
                        </div>
                     </div>
                  </div>
               </div>
            </li>
         </ul>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="checkbox_twmd_hardware">Hardware</label>
         <div class="form-group">
            <div class="col-xs-12">
               <ul class="list-group">
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-sm-3">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_hinges" value="hinges">Hinges
                              </label>
                           </div>
                        </div>
                        <div class="col-sm-9">
                           <div class="form-group">
                              <div class="col-xs-12">
                                 <input id="text_twmd_hardware_hinges_type" type="text" class="form-control" placeholder="Type">
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-sm-3">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_lock_set" value="lock_set">Lockset
                              </label>
                           </div>
                        </div>
                        <div class="col-sm-9">
                           <div class="form-group">
                              <div class="col-xs-12">
                                 <input id="text_twmd_hardware_lock_set_function" type="text" class="form-control" placeholder="Function">
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_dead_bolt" value="dead_bolt">Deadbolt
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-sm-3">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_panic_bar" value="panic_bar">Panic bar
                              </label>
                           </div>
                        </div>
                        <div class="col-sm-9">
                           <div class="form-group">
                              <div class="col-xs-12">
                                 <input id="text_twmd_hardware_panic_bar_trim" type="text" class="form-control" placeholder="Trim">
                              </div>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_door_closer" value="door_closer">Door closer
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_push_pull_set" value="push_pull_set">Push/Pull set
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_latch_guard" value="latch_guard">Latch guard
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_kick_plate" value="kick_plate">Kickplate
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_threshold" value="threshold">Threshold
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_weather_strip" value="weather_strip">Weatherstrip
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_door_sweep" value="door_sweep">Door sweep
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_drip_cap" value="drip_cap">Drip cap
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_wall_floor_stop" value="wall_floor_stop">Wall/Floor stop
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
                  <li class="list-group-item">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <div class="checkbox" style="text-align:left">
                              <label>
                                 <input type="checkbox" name="checkbox_twmd_hardware" id="checkbox_twmd_hardware_door_viewer" value="door_viewer">Door viewer
                              </label>
                           </div>
                        </div>
                     </div>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="radio_twmd_hardware_finish">Hardware Finish</label>
         <ul class="list-group">
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_not_a_value" value="not_a_value" checked>No answer
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_us26d" value="us26d">US26D
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_us32d" value="us32d">US32D
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_us4" value="us4">US4
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_us3" value="us3">US3
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_us10" value="us10">US10
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-xs-12">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_us10b" value="us10b">US10B
                        </label>
                     </div>
                  </div>
               </div>
            </li>
            <li class="list-group-item">
               <div class="form-group">
                  <div class="col-sm-3">
                     <div class="radio" style="text-align:left">
                        <label>
                           <input type="radio" name="radio_twmd_hardware_finish" id="radio_twmd_hardware_finish_other" value="other">Other
                        </label>
                     </div>
                  </div>
                  <div class="col-sm-9">
                     <div class="form-group">
                        <div class="col-xs-12">
                           <input id="text_twmd_hardware_finish_other" type="text" class="form-control" placeholder="Specify">
                        </div>
                     </div>
                  </div>
               </div>
            </li>
         </ul>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="textarea_twmd_additional_info">Additional Info</label>
         <textarea id="textarea_twmd_additional_info" class="form-control" rows="3"></textarea>
      </div>
   </div>
</div>
