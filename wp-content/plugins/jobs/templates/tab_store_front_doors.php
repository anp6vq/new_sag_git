<div class="form-horizontal">
   <div class="form-group">
      <div class="col-xs-12">
         <div class="input-group">
            <div class="input-group-btn">
               <button id="btn_tsfd_first" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-fast-backward" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_previous" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-backward" aria-hidden="true"></span>
               </button>
            </div>
            <input id="text_tsfd_item_count" type="text" class="form-control text-center" placeholder="0/0" value="0/0">
            <div class="input-group-btn">
               <button id="btn_tsfd_next" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-forward" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_last" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-fast-forward" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_add" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_save" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_delete" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_refresh" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-refresh" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_copy" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-copy" aria-hidden="true"></span>
               </button>
               <button id="btn_tsfd_paste" type="button" class="btn btn-default" aria-label="Left Align">
                  <span class="glyphicon glyphicon-paste" aria-hidden="true"></span>
               </button>
            </div>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="radio_group_tsfd_elevation_type">Elevation type (viewed from exterior)</label>
         <div id="radio_group_tsfd_elevation_type" class="btn-group btn-group-justified"" data-toggle="buttons">
            <label class="btn btn-default active">
               <input type="radio" name="radio_tsfd_elevation_type" id="radio_tsfd_elevation_type_a" autocomplete="off" value="a" checked>
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/a.png" width="66px" height="180px" alt="A">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_elevation_type" id="radio_tsfd_elevation_type_b" autocomplete="off" value="b">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/b.png" width="66px" height="180px" alt="B">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_elevation_type" id="radio_tsfd_elevation_type_c" autocomplete="off" value="c">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/c.png" width="66px" height="180px" alt="C">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_elevation_type" id="radio_tsfd_elevation_type_d" autocomplete="off" value="d">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/d.png" width="66px" height="180px" alt="D">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_elevation_type" id="radio_tsfd_elevation_type_e" autocomplete="off" value="e">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/e.png" width="66px" height="180px" alt="E">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_elevation_type" id="radio_tsfd_elevation_type_f" autocomplete="off" value="f">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/f.png" width="66px" height="180px" alt="F">
            </label>
         </div>
      </div>
   </div>
   <div id="row_tsfd_door_opening" class="form-group">
      <div class="col-xs-12">
         <label for="radio_group_tsfd_door_opening">Door opening</label>
         <div id="radio_group_tsfd_door_opening" class="btn-group btn-group-justified"" data-toggle="buttons">
            <label class="btn btn-default active">
               <input type="radio" name="radio_tsfd_door_opening" id="radio_tsfd_door_opening_lh" autocomplete="off" value="lh" checked>
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/lh.png" width="102px" height="102px" alt="LH">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_door_opening" id="radio_tsfd_door_opening_lhr" autocomplete="off" value="lhr">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/lhr.png" width="102px" height="102px" alt="LHR">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_door_opening" id="radio_tsfd_door_opening_rh" autocomplete="off" value="rh">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/rh.png" width="102px" height="102px" alt="RH">
            </label>
            <label class="btn btn-default">
               <input type="radio" name="radio_tsfd_door_opening" id="radio_tsfd_door_opening_rhr" autocomplete="off" value="rhr">
               <img src="<?php bloginfo('stylesheet_directory'); ?>/img/storefront_doors/rhr.png" width="102px" height="102px" alt="RHR">
            </label>
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_tsfd_quantity">Quantity</label>
         <input id="text_tsfd_quantity" type="text" class="form-control" placeholder="Quantity">
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="text_tsfd_mark_number">Mark #</label>
         <input id="text_tsfd_mark_number" type="text" class="form-control" placeholder="Mark #">
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="select_tsfd_door_style">Door Style</label>
         <select id="select_tsfd_door_style" class="form-control">
            <option value="not_a_value" disabled selected>Please select door style</option>
            <option value="212_narrow">212 Narrow</option>
            <option value="375_medium">375 Medium</option>
            <option value="500_wide">500 Wide</option>
         </select>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="select_tsfd_door_glass">Door Glass</label>
         <select id="select_tsfd_door_glass" class="form-control">
            <option value="not_a_value" disabled selected>Please select door glass</option>
            <option value="quarter_inch">1/4 inch</option>
            <option value="one_inch">1 inch</option>
         </select>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-4 col-xs-12">
         <label for="text_tsfd_frame_mrp_number">Frame Mrp #</label>
         <input id="text_tsfd_frame_mrp_number" type="text" class="form-control" placeholder="Frame Mrp #">
      </div>
      <div class="col-sm-4 col-xs-12">
         <label for="select_tsfd_frame">Frame</label>
         <select id="select_tsfd_frame" class="form-control">
            <option value="not_a_value" disabled selected>Please select frame</option>
            <option value="fg_2000_ob">FG-2000 OB</option>
            <option value="fg_3000_ob">FG-3000 OB</option>
            <option value="none">None</option>
         </select>
      </div>
      <div class="col-sm-4">
        <div class="form-group">
          <label for="radio_tsfd_finish">Finish</label>
          <select id="radio_tsfd_finish" name="radio_tsfd_finish" class="form-control">
            <option  id="radio_tsfd_finish_not_a_value" value="not_a_value" disabled selected>Please select finish</option>
            <option  id="radio_tsfd_finish_clear_glass" value="clear_glass">Clear glass</option>
            <option  id="radio_tsfd_finish_dark_bronze" value="dark_bronze">Dark bronze</option>
            <option   id="radio_tsfd_finish_other" value="other">Other </option>
         </select>
           <input id="text_tsfd_finish_other" type="text" class="form-control" placeholder="Specify">

         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="select_tsfd_hanging_hardware">Hanging hardware</label>
         <select id="select_tsfd_hanging_hardware" class="form-control">
            <option value="not_a_value" disabled selected>Please select hanging hardware</option>
            <option value="one_pair_butts">(1) Pair butts</option>
            <option value="one_half_pair_butts">(1 - 1/2) Pair butts</option>
            <option value="top_bottom_offset_pivots">Top & bottom offset pivots</option>
            <option value="top_bottom_interior_pivots">Top, bottom & interior pivots</option>
            <option value="continuous_hinge">Continuous hinge</option>
         </select>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="select_tsfd_threshold">Threshold</label>
         <select id="select_tsfd_threshold" class="form-control">
            <option value="not_a_value" disabled selected>Please select threshold</option>
            <option value="4_inches">4 inches</option>
            <option value="7_inches">7 inches</option>
         </select>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="select_tsfd_closer">Closer</label>
         <select id="select_tsfd_closer" class="form-control">
            <option value="not_a_value" disabled selected>Please select closer</option>
            <option value="std_surface">Standard surface</option>
            <option value="none">None</option>
         </select>
      </div>
      <div class="col-md-3 col-sm-6 col-xs-12">
         <label for="select_tsfd_lock_panic">Lock/panic</label>
         <select id="select_tsfd_lock_panic" class="form-control">
            <option value="not_a_value" disabled selected>Please select lock/panic</option>
            <option value="ms_lock">MS Lock</option>
            <option value="std_cvr_panic">Standard CVR panic</option>
            <option value="latch_lock">Latch lock</option>
            <option value="std_rim_panic">Standard RIM panic (SGL doors only)</option>
            <option value="thumb_turn">Thumb turn</option>
            <option value="paddle">Paddle</option>
            <option value="lever">Lever</option>
            <option value="none">None</option>
         </select>
      </div>
   </div>
   <div class="form-group">
      <div class="col-sm-6 col-xs-12">
         <label for="select_tsfd_push_pull">Push/pull</label>
         <select id="select_tsfd_push_pull" class="form-control">
            <option value="not_a_value" disabled selected>Please select push/pull</option>
            <option value="std_ph20_pb21">Standard PH20/PB21</option>
            <option value="std_ph21_panic_pull">Standard PH21 Panic Pull</option>
            <option value="ph20_btb">PH20 BTB</option>
            <option value="ph12_btb">PH12 BTB</option>
         </select>
      </div>
      <div class="col-sm-6 col-xs-12">
         <label for="select_tsfd_mid_panel">Mid panel</label>
         <select id="select_tsfd_mid_panel" class="form-control">
            <option value="not_a_value" disabled selected>Please select mid panel</option>
            <option value="d_135">D-135 (1/2 inch)</option>
            <option value="d_11">D-11 (1 - 7/8 inch)</option>
            <option value="d_2">D-2 (4 inches)</option>
            <option value="d_21">D-21 (6 - 1/2 inches)</option>
            <option value="d_71">D-71 (8 inches)</option>
            <option value="d_61">D-61 (10 inches)</option>
         </select>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label for="select_tsfd_bottom_rail">Bottom rail</label>
         <select id="select_tsfd_bottom_rail" class="form-control">
            <option value="not_a_value" disabled selected>Please select bottom rail</option>
            <option value="d_2">D-2 (4 inches)</option>
            <option value="d_21">D-21 (6 - 1/2 inches)</option>
            <option value="d_71">D-71 (8 inches)</option>
            <option value="d_61">D-61 (10 inches)</option>
            <option value="d_71_d_19">D-71 & D-19 Stacked (12 inches)</option>
         </select>
      </div>
   </div>
</div>
