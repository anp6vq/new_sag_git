<div class="form-horizontal">
   <div class="form-group">
      <div class="col-xs-12">
         <?php include 'project-items-nav.php'; ?>
      </div>
   </div> <!-- ******** end nav ****** -->
   <div class="form-group mar_top10">
      <div class="form-section col-md-12 col-lg-7"> <!-- ****** START LEFT SECTION  ***** -->
      <div class="row">
      <div class="form-group">
	  	<div class="col-xs-12">
				  <div class="row">
					<div class="col-xs-12">
					  <div class="input-group custom_addon">
						<div class="input-group-addon" style="opacity:0;">
						  <input class="user-success verify-hidden-checkbox" id="" value="verify_tgpd_glass_color" name="" type="checkbox">
						  <label class="leftalign verify-label" for=""> </label>
						</div>
						<label class="leftalign" for="select_tgpd_servicetype">Service Type</label>
						<select class="form-control test" id="select_tgpd_servicetype" name="select_tgpd_servicetype">
						  <option value="not_applicable">Please select service type</option>
						  <option value="replace" selected="selected">Replace</option>
						  <option value="all_new">All New</option>
						  <option value="removal_only">Removal Only</option>
                        <option value="Removal/Reinstall_same_day">Removal/Reinstall Same Day</option>
						  <option value="Removal/Reinstall_different_day">Removal/Reinstall Different Day</option>
						  <option value="Deliver_Only">Deliver Only</option>
<option value="Install_from_Customer_Stock">Install from Customer Stock </option>
						</select>
						<input type="hidden" name="childjobid" value="" id="childjobid" />
					  </div>
					</div>
				  </div>
				</div>
			<div class="col-sm-12 col-md-12" id="secondryquote" style="display:none;">
			  <div class="row">
				<div class="col-xs-12">
				  <div class="input-group custom_addon">
					<div class="input-group-addon" style="opacity:0;">
					  <input class="user-success verify-hidden-checkbox" id="" value="verify_tgpd_glass_color" name="" type="checkbox">
					  <label class="leftalign verify-label" for=""> </label>
					</div>
					<label class="leftalign" for="text_tgpd_glass_secondryquote">Quote Number</label>
					<input id="text_tgpd_glass_secondryquote" name="text_tgpd_glass_secondryquote" type="text" class="form-control" autocomplete="on" maxlength="30" placeholder="Previous Quote Number">
				  </div>
				</div>
			  </div>
			</div>
         <div class="col-sm-12 col-md-12">
		  <div class="row">
			<div class="col-xs-12">
			  <div class="input-group custom_addon">
				<div class="input-group-addon" style="opacity:0;">
				  <input class="user-success verify-hidden-checkbox" id="" value="verify_tgpd_glass_color" name="" type="checkbox">
				  <label class="leftalign verify-label" for=""> </label>
				</div>
				<label class="leftalign" for="text_tgpd_glass_location">Location</label>
				<input id="text_tgpd_glass_location" name="text_tgpd_glass_location" type="text" class="form-control" autocomplete="on" maxlength="30" placeholder="Glass location">
			  </div>
			</div>
		  </div>
		</div>
			<style type="text/css">.qty-align-new{text-align: center;}</style>
		<div class="col-xs-12">
		  <div class="row">
			<div class="col-xs-12">
			  <div class="input-group custom_addon">
				<div class="input-group-addon" style="opacity:0;">
				  <input class="user-success verify-hidden-checkbox" id="" value="verify_tgpd_glass_color" name="" type="checkbox">
				  <label class="leftalign verify-label" for=""> </label>
				</div>
				<label class="leftalign" for="text_tgpd_glass_quantity">Quantity</label>
				<div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_quantity"><span class="glyphicon glyphicon-minus"></span></button></span><input id="text_tgpd_glass_quantity" text-align="center" name="text_tgpd_glass_quantity" type="text" class="form-control input-number-plus-minus qty-align-new" min="0" max="100" value="1"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="text_tgpd_glass_quantity"><span class="glyphicon glyphicon-plus"></span></button></span></div>
			  </div>
			</div>
		  </div>
	  </div>

			<div class="col-sm-12 col-md-12">
			  <div class="row">
				<div class="col-xs-12">
				  <div class="input-group custom_addon">
					<div class="input-group-addon" style="opacity:0;">
					  <input class="user-success verify-hidden-checkbox" id="" value="verify_tgpd_glass_color" name="" type="checkbox">
					  <label class="leftalign verify-label" for=""> </label>
					</div>
					<label class="leftalign" for="select_tgpd_glass_shape">Shape</label>
					<select class="form-control" id="select_tgpd_glass_shape" name="select_tgpd_glass_shape">
					  <?php include 'options/glass-shape-options.php'; ?>
					</select>
				  </div>
				</div>
			  </div>
			</div>

			<?php include 'glass_shapes/type_1_rh_gable.php' ?>
			<?php include 'glass_shapes/type_2_lh_gable.php' ?>
			<?php include 'glass_shapes/type_3_rh_angle_corner.php' ?>
			<?php include 'glass_shapes/type_4_lh_angle_corner.php' ?>
			<?php include 'glass_shapes/type_5_lh_trapezoid.php' ?>
			<?php include 'glass_shapes/type_6_rh_trapezoid.php' ?>
			<?php include 'glass_shapes/type_7_rh_irregular_angle_crn.php' ?>
			<?php include 'glass_shapes/type_8_lh_irregular_angle_crn.php' ?>
			<?php include 'glass_shapes/type_9_lh_angle_corner_offset.php' ?>
			<?php include 'glass_shapes/type_10_rh_angle_corner_offset.php' ?>
			<?php include 'glass_shapes/type_11_o_s_left_up-out.php' ?>.
			<?php include 'glass_shapes/type_12_o_s_right_up-out.php' ?>
			<?php include 'glass_shapes/type_13_o_s_right_up-in.php' ?>
			<?php include 'glass_shapes/type_14_o_s_right_down-out.php' ?>
			<?php include 'glass_shapes/type_15_o_s_left_down-out.php' ?>
			<?php include 'glass_shapes/type_16_o_s_left_up-in.php' ?>
			<?php include 'glass_shapes/type_18_o_s_right_down.php' ?>
			<?php include 'glass_shapes/type_19_irregular_octagon.php' ?>
			<?php include 'glass_shapes/type_20_equilateral_triangle.php' ?>
			<?php include 'glass_shapes/type_21_regular_pentagon.php' ?>
			<?php include 'glass_shapes/type_22_regular_hexagon.php' ?>
			<?php include 'glass_shapes/type_24_regular_octagon.php' ?>
			<?php include 'glass_shapes/type_25_rh_triangle.php' ?>
			<?php include 'glass_shapes/type_26_lh_triangle.php' ?>
			<?php include 'glass_shapes/type_37_rh_dog_house.php' ?>
			<?php include 'glass_shapes/type_38_lh_dog_house.php' ?>
			<?php include 'glass_shapes/type_39_lh_irreg_trapezoid.php' ?>
			<?php include 'glass_shapes/type_40_rh_irreg_trapezoid.php' ?>
			<?php include 'glass_shapes/type_45_rh_right_triangle.php' ?>
			<?php include 'glass_shapes/type_46_lh_right_triangle.php' ?>
			<?php include 'glass_shapes/type_47_rh_parallelogram.php' ?>
			<?php include 'glass_shapes/type_48_lh_parallelogram.php' ?>
			<?php include 'glass_shapes/type_51_oblique_triangle.php' ?>
			<?php include 'glass_shapes/type_53_rh_gable_angle.php' ?>
			<?php include 'glass_shapes/type_54_lh_gable_angle.php' ?>
			<?php include 'glass_shapes/type_60_circle.php' ?>
			<?php include 'glass_shapes/type_61_half_circle.php' ?>
			<?php include 'glass_shapes/type_62_arch_with_radius.php' ?>
			<?php include 'glass_shapes/type_63_arch.php' ?>
			<?php include 'glass_shapes/type_64_archtop_with_radius.php' ?>
			<?php include 'glass_shapes/type_65_archtop.php' ?>
			<?php include 'glass_shapes/type_66_rh_arch_gable.php' ?>
			<?php include 'glass_shapes/type_67_lh_arch_gable.php' ?>
			<?php include 'glass_shapes/type_70_rh_qtr_arch.php' ?>
			<?php include 'glass_shapes/type_71_lh_qtr_arch.php' ?>
			<?php include 'glass_shapes/type_74_2_radius_corners.php' ?>
			<?php include 'glass_shapes/type_75_4_radius_corners.php' ?>
			<?php include 'glass_shapes/type_78_round_top.php' ?>
			<?php include 'glass_shapes/type_81_ellipse.php' ?>
<?php include 'glass_shapes/type_doghouse.php' ?>

		<div class="col-sm-12 col-md-12">
		  <div class="row">
			<div class=" col-xs-12">
			  <div class="input-group custom_addon">
				<div class="input-group-addon" style="opacity:0;">
				  <input class="user-success verify-hidden-checkbox" id="" value="verify_tgpd_glass_color" name="" type="checkbox">
				  <label class="leftalign verify-label" for=""> </label>
				</div>
				<label class="leftalign" for="select_tgpd_glass_type">Type</label>
				<select class="form-control" id="select_tgpd_glass_type" name="select_tgpd_glass_type">
				  <?php include 'options/glass-type-options.php'; ?>
				</select>
			  </div>
			</div>
		  </div>
		</div>
	</div>
	
			
			
			<br>
			<div class="col-sm-12 col-md-12 verification-field" id="extra_type_coating_treatment">	
			<label class="leftalign group_option">Exterior Lite</label>			
			<div class="row">
			<div style="opacity: 0;float:left;" class="col-xs-hidden input-group-addon">
				  <input type="checkbox" name="" value="verify_tgpd_glass_color" id="" class="user-success verify-hidden-checkbox">
				  <label for="" class="leftalign verify-label"> </label>
				</div>
				<div class="col-sm-2 col-md-2 thikness"  style="padding-left:27px;padding-right:0;">				
					<input type="checkbox" name="verify_tgpd_exterior_glass_type" value="verify_tgpd_exterior_glass_type" id="verify_tgpd_exterior_glass_type" class="user-success verify-hidden-checkbox">
					<label  for="verify_tgpd_exterior_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_exterior_glass_type">Thickness</label>
					<select class="form-control customck lite-select" id="select_tgpd_exterior_glass_type" name="select_tgpd_exterior_glass_type">
					  <?php include 'options/glass-overall-new-thickness-igu.php'; ?>
					</select>
				</div>
				<div class="col-sm-2 col-md-2 thikness" style="padding-right:0;">
					<input type="checkbox" name="verify_tgpd_treatment_exterior_glass" value="verify_tgpd_treatment_exterior_glass" id="verify_tgpd_treatment_exterior_glass" class="user-success verify-hidden-checkbox">
					<label  for="verify_tgpd_treatment_exterior_glass"> </label>
					<label class="lite-label" for="select_tgpd_treatment_exterior_glass">Treatment</label>
					<select class="form-control lite-select" id="select_tgpd_treatment_exterior_glass" name="select_tgpd_treatment_exterior_glass" enabled>
            <?php include 'options/glass-treatment.php'; ?>
					</select>
				</div>
				<div class="col-sm-5 col-lg-5 exterior_lite non_vari_col non_vari-lolor-m">
					<input type="checkbox" name="verify_tgpd_exterior_color_glass_type" value="verify_tgpd_exterior_color_glass_type" id="verify_tgpd_exterior_color_glass_type" class="user-success verify-hidden-checkbox">
					<label  for="verify_tgpd_exterior_color_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_exterior_color_glass_type">Color</label>
					<select  class="form-control lite-select" id="select_tgpd_exterior_color_glass_type" name="select_tgpd_exterior_color_glass_type">
						  <?php include 'options/glass-color-options.php'; ?>
					</select>
				</div>
					<div class="col-sm-2 col-lg-2 vari_col vari_color-s" style="display:none;padding-right:0;">
					<input type="checkbox" name="verify_tgpd_exterior_coating_family_color_glass_type" value="verify_tgpd_exterior_coating_family_color_glass_type" id="verify_tgpd_exterior_coating_family_color_glass_type" class="user-success verify-hidden-checkbox ">
					<label  for="verify_tgpd_exterior_coating_family_color_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_exterior_color_glass_type">Color</label>
					<select  class="form-control lite-select" id="select_tgpd_exterior_coating_family_color_glass_type" name="select_tgpd_exterior_coating_family_color_glass_type">
						  <?php include 'options/glass_type_igu_viracon_color.php'; ?>
					</select>
				</div>

					<div class="col-sm-3 col-lg-3 vari_col" style="display:none;padding-right:0;">
					<input type="checkbox" name="verify_tgpd_exterior_coating_family_glass_type" value="verify_tgpd_exterior_coating_family_glass_type" id="verify_tgpd_exterior_coating_family_glass_type" class="user-success verify-hidden-checkbox ">
					<label  for="verify_tgpd_exterior_coating_family_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_exterior_color_glass_type">Coating Family</label>
					<select  class="form-control lite-select" id="select_tgpd_exterior_coating_family_glass_type" name="select_tgpd_exterior_coating_family_glass_type">
						  <?php include 'options/glass_type_igu_viracon.php'; ?>
					</select>
				</div>
				<div class="col-sm-2 a col-2-width-15-3-per coating-right">
					<div class="position_rel clearfix">
					<div class="custom_check_radio custom_text_align_right pull-left">
						<div class="left-cus-align">
							<input class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_tgpd_coating_first_position" id="text_tgpd_coating_first_position" type="checkbox">
							<label class="text_tgpd_coating_first_position" for="text_tgpd_coating_first_position" style="visibility:hidden;"></label>
						</div>
					</div>
					<div class="verify_tgpd_coating_exterior_type pull-left">
						<input type="checkbox" name="verify_tgpd_coating_exterior_type" value="verify_tgpd_coating_exterior_type" id="verify_tgpd_coating_exterior_type" class="user-success verify-hidden-checkbox">
						<label id="verify_tgpd_coating_exterior_type" for="verify_tgpd_coating_exterior_type"> </label>	
						<label class="lite-label" for="text_tgpd_coating_exterior_type" id="coating_first_hide_glass_type">Coating</label>	
					</div>							
					</div>					
					<select class="form-control lite-select" id="text_tgpd_coating_exterior_type" name="text_tgpd_coating_exterior_type">
							<option value="N/A">N/A</option> 
							<option value="#1">#1</option>
							<option value="#2">#2</option>
							<option value="#3">#3</option>
							<option value="#4">#4</option>
					</select>
				</div>
								
				</div>
			</div>
		
			<div class="col-sm-12 col-md-12 verification-field" id="extra_type_coating_treatment_second">
			<label class="leftalign group_option" id="hide_for_non_igu" style="display:none;">Interior Lite</label>			
			<div class="row">
				<div style="opacity: 0;float:left;" class="col-xs-hidden input-group-addon">
				</div>
				<div class="col-sm-2 glass_pading_sag" style="padding-left:27px;padding-right:0;">
				<input type="checkbox" name="text_tgpd_coating_exterior_type" value="verify_tgpd_interior_glass_type" id="verify_tgpd_interior_glass_type" class="user-success verify-hidden-checkbox verify-active">
				  <label  for="verify_tgpd_interior_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_interior_glass_type">Thickness</label>
					<select class="form-control customck-thickness_int lite-select" id="select_tgpd_interior_glass_type" name="select_tgpd_interior_glass_type">
					  <?php include 'options/glass-overall-new-thickness-igu.php'; ?>
					</select>					
					<select class="form-control customck-thickness_int_non_igu lite-select" id="select_tgpd_interior_glass_type_non_igu" name="select_tgpd_interior_glass_type">
					  <?php include 'options/glass-overall-new-thickness-non-igu.php'; ?>
					</select>
				</div>
				<div class="col-sm-2" style="padding-right:0;">
				<input type="checkbox" name="verify_tgpd_treatment_interior_glass" value="verify_tgpd_treatment_interior_glass" id="verify_tgpd_treatment_interior_glass" class="user-success verify-hidden-checkbox verify-active">
				  <label  for="verify_tgpd_treatment_interior_glass"> </label>
					<label class="lite-label" for="select_tgpd_treatment_interior_glass">Treatment</label>
					<select class="form-control lite-select" id="select_tgpd_treatment_interior_glass" name="select_tgpd_treatment_interior_glass" enabled>
            <?php include 'options/glass-treatment.php'; ?>
					</select>
				</div>
				<div class="col-sm-5 col-lg-5 exterior_lite non_vari_col non_vari-lolor-m" style="padding-right:0;">
				<input type="checkbox" name="verify_tgpd_interior_color_glass_type" value="verify_tgpd_interior_color_glass_type" id="verify_tgpd_interior_color_glass_type" class="user-success verify-hidden-checkbox verify-active">
				  <label  for="verify_tgpd_interior_color_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_interior_color_glass_type">Color</label>
					<select class="form-control lite-select" id="select_tgpd_interior_color_glass_type" name="select_tgpd_interior_color_glass_type">
						  <?php include 'options/glass-color-options.php'; ?>
					</select>
				</div>
					<div class="col-sm-2 col-lg-2 vari_col vari_color-s" style="display:none;padding-right:0;">
				<input type="checkbox" name="verify_tgpd_interior_coating_family_color_glass_type" value="verify_tgpd_interior_coating_family_color_glass_type" id="verify_tgpd_interior_coating_family_color_glass_type" class="user-success verify-hidden-checkbox  verify-active ">
				  <label  for="verify_tgpd_interior_coating_family_color_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_interior_coating_family_color_glass_type">Color</label>
					<select class="form-control lite-select" id="select_tgpd_interior_oating_family_color_glass_type" name="select_tgpd_interior_oating_family_color_glass_type">
						  <?php include 'options/glass_type_igu_viracon_color.php'; ?>
					</select>
				</div>

					<div class="col-sm-3 col-lg-3 vari_col" style="display:none;padding-right:0;">
				<input type="checkbox" name="verify_tgpd_interior_coating_family_glass_type" value="verify_tgpd_interior_coating_family_glass_type" id="verify_tgpd_interior_coating_family_glass_type" class="user-success verify-hidden-checkbox verify-active  ">
				  <label  for="verify_tgpd_interior_coating_family_glass_type"> </label>
					<label class="lite-label" for="select_tgpd_interior_coating_family_glass_type">Coating Family</label>
					<select class="form-control lite-select" id="select_tgpd_interior_coating_family_glass_type" name="select_tgpd_interior_coating_family_glass_type">
						  <?php include 'options/glass_type_igu_viracon.php'; ?>
					</select>
				</div>
				<div class="col-sm-2 a col-2-width-15-3-per coating-right">
				<div class="position_rel clearfix">
					<div class="custom_check_radio custom_text_align_right pull-left">
							<div class="left-cus-align">
								<input class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_tgpd_coating_second_position" id="text_tgpd_coating_second_position" type="checkbox">
								<label class="text_tgpd_coating_second_position" for="text_tgpd_coating_second_position" style="visibility:hidden;"></label>
							</div>
						</div>
					<div class="verify_tgpd_coating_interior_type pull-left">
						<input type="checkbox" name="verify_tgpd_coating_interior_type" value="verify_tgpd_coating_interior_type" id="verify_tgpd_coating_interior_type" class="user-success verify-hidden-checkbox verify-active">
						<label  for="verify_tgpd_coating_interior_type"> </label>
						<label  class="lite-label" for="text_tgpd_coating_interior_type" id="coating_second_hide_glass_type">Coating</label>
					</div>		
				</div>		
				  <select class="form-control lite-select" id="text_tgpd_coating_interior_type" name="text_tgpd_coating_interior_type">
							<option value="N/A">N/A</option> 
							<option value="#1">#1</option>
							<option value="#2">#2</option>
							<option value="#3">#3</option>
							<option value="#4">#4</option>
					</select>
				</div>
				</div>
			</div>



		
	 <div id="lamidrophide" style="display:none;">
		<div class="col-xs-12 verification-field">
		  <div class="row">
			<div class="col-xs-12 custom_checkbox_move verification-field">
			  <label class="leftalign group_option" for="select_tgpd_overall_thickness">Exterior Lite Thickness</label>
			  <div class="input-group custom_addon">
				<div class="input-group-addon custom_checkbox_add">
				  <input type="checkbox" class="user-success verify-hidden-checkbox" value="verify_tgpd_exterior_lite_thickness" name="verify_tgpd_exterior_lite_thickness" id="verify_tgpd_exterior_lite_thickness" />
				  <label class="verify-label" for="verify_tgpd_exterior_lite_thickness" class="rightalign" >
				  </label>
				</div>
				<input type="text" value="" id="exterior_light_thickness" class="form-control text-num" placeholder="Exterior Lite Thickness"  />
			  </div>
			</div>
		  </div>
		</div>

			<div class="col-xs-12 verification-field">
			  <div class="row">
				<div class="col-xs-12 custom_checkbox_move verification-field">
				  <label class="leftalign group_option" for="select_tgpd_overall_thickness">Interior Lite Thickness</label>
				  <div class="input-group custom_addon">
					<div class="input-group-addon custom_checkbox_add">
					  <input class="user-success verify-hidden-checkbox" type="checkbox" value="verify_tgpd_interior_lite_thickness"  name="verify_tgpd_interior_lite_thickness" id="verify_tgpd_interior_lite_thickness" />
					  <label for="verify_tgpd_interior_lite_thickness" class="verify-label leftalign"> </label>
					</div>
					<input type="text" value=""  id="interior_light_thickness"class="form-control text-num" placeholder="Interior Lite Thickness" />
				  </div>
				</div>
			  </div>
			</div>


		 <div class="col-xs-12 verification-field">
		  <div class="row">
			<div class="col-xs-12 custom_checkbox_move verification-field">
			  <label class="leftalign group_option" for="select_tgpd_overall_thickness">Inner Layer Thickness</label>
			  <div class="input-group custom_addon">
				<div class="input-group-addon custom_checkbox_add">
				  <input  class="user-success verify-hidden-checkbox" type="checkbox" value="verify_tgpd_inner_layer_thickness"  name="verify_tgpd_inner_layer_thickness" id="verify_tgpd_inner_layer_thickness" />
				  <label class="verify-label leftalign" for="verify_tgpd_inner_layer_thickness" > </label>
				</div>
				<select class="form-control" id="select_tgpd_innner_layer_thickness" name="select_tgpd_innner_layer_thickness">
				  <option value="not_applicable">Inner Layer Thickness </option>
				  <option value="030">030</option>
				  <option value="060">060</option>
				  <option value="inner_thickness_custom">Custom</option>
				</select>
			  </div>
			</div>
		  </div>
		</div>

    		 <div class="col-xs-12 verification-field" id="custom_thick_without_glass_type">
    			 <label class="leftalign custom_inner_leval_thinkness" for="select_tgpd_overall_thickness" >Custom</label>
    			 <input type="text" value=""  id="custom_layer_thickness" class="form-control custom_inner_leval_thinkness"  placeholder="Custom"/>
				</div>
			</div>

			<div class="col-xs-12 verification-field" id="overall_thickness_without_glass_type">
			  <div class="row">
				<div class="col-xs-12 custom_checkbox_move">
				  <label class="leftalign group_option" for="select_tgpd_overall_thickness">Overall Thickness</label>
				  <div class="input-group custom_addon">
					<div class="input-group-addon custom_checkbox_add">
					  <input type="checkbox" class="user-success verify-hidden-checkbox" value="verify_tgpd_overall_thickness"  name="verify_tgpd_overall_thickness" id="verify_tgpd_overall_thickness" />
					  <label class="leftalign verify-label" for="verify_tgpd_overall_thickness"> </label>
					</div>
					<select class="form-control customck lite-select" id="select_tgpd_overall_thickness" name="select_tgpd_overall_thickness">
					  <?php include 'options/glass-overall-thickness.php'; ?>
					</select>
				  </div>
				</div>
			  </div>
			</div>

			<div id="litethick_see"  >
			  <div class="col-xs-12 verification-field">
				<div class="row">
				  <div class="col-xs-12 custom_checkbox_move ">
					<label class="leftalign group_option" for="select_tgpd_lite_thickness">Lite thickness</label>
					<div class="input-group custom_addon">
					  <div class="input-group-addon custom_checkbox_add">
						<input type="checkbox" class="user-success verify-hidden-checkbox" value="verify_tgpd_lite_thickness" id="verify_tgpd_lite_thickness" name="verify_tgpd_lite_thickness" />
						<label class="lefttalign verify-label" for="verify_tgpd_lite_thickness" id="vtlt"> </label>
					  </div>
					  <select class="form-control customck" id="select_tgpd_lite_thickness" name="select_tgpd_lite_thickness">
						<option value="not_applicable">Lite thickness</option>
						<option value="1/8">1/8</option>
						<option value="3/16">3/16</option>
						<option value="1/4">1/4</option>
						<option value="3/8">3/8</option>
						<option value="1/2">1/2</option>
					  </select>
					</div>
				  </div>
				</div>
			  </div>
			</div>

			 <div class="col-sm-12 col-md-12 verification-field" id="treatment_without_glass_type">
			  <div class="row">
				<div class="col-xs-12 custom_checkbox_move">
				  <label class="leftalign group_option" for="select_tgpd_glass_treatment">Treatment</label>
				  <div class="input-group custom_addon">
					<div class="input-group-addon custom_checkbox_add">
					  <input type="checkbox" class="user-success verify-hidden-checkbox"  id="verify_tgpd_glass_treatment" value="verify_tgpd_glass_treatment" name="verify_tgpd_glass_treatment" />
					  <label class="leftalign verify-label" for="verify_tgpd_glass_treatment"> </label>
					</div>
					<select class="form-control" id="select_tgpd_glass_treatment" name="select_tgpd_glass_treatment" enabled>
            <?php include 'options/glass-treatment.php'; ?>
					</select>
				  </div>
				</div>
				<div   id="verify_tgpd_glass_treatment_toggle"> </div>
			  </div>
			</div>

				 <div class="col-sm-12 col-md-12 verification-field" id="glass_color_with_glass_type">
				  <div class="row">
					<div class="col-xs-12 custom_checkbox_move verification-field">
					  <label class="leftalign group_option" for="select_tgpd_glass_color">Glass Color</label>
					  <div class="input-group custom_addon">
						<div class="input-group-addon custom_checkbox_add">
						  <input class="user-success verify-hidden-checkbox" type="checkbox" id="verify_tgpd_glass_color" value="verify_tgpd_glass_color" name="verify_tgpd_glass_color" >
						  <label class="leftalign verify-label" for="verify_tgpd_glass_color"> </label>
						</div>
						<select class="form-control" id="select_tgpd_glass_color" name="select_tgpd_glass_color">
						  <?php include 'options/glass-color-options.php'; ?>
						</select>
					  </div>
					</div>
				  </div>
				</div>

				<div id="div_tgpd_glass_color" class="col-sm-12 col-md-12 verification-field">
				  <div class="row">
					<div class="col-xs-12 custom_checkbox_move verification-field">
					  <label class="leftalign group_option" for="text_tgpd_glass_color_note">Color Note</label>
					  <div class="input-group custom_addon">
						<div class="input-group-addon custom_checkbox_add">
						  <input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_glass_color_note" value="verify_tgpd_glass_color_note" name="verify_tgpd_glass_color_note" />
						  <label class="leftalign verify-label" for="verify_tgpd_glass_color_note"></label>
						</div>
						<input id="text_tgpd_glass_color_note" name="text_tgpd_glass_color_note" type="text" class="form-control" autocomplete="on" maxlength="150" placeholder="Color note">
					  </div>
					</div>
				  </div>
				</div>
				
				<!-- <div class="col-sm-12 col-md-12 verification-field shape-container-day" id="coating_without_glass_type" style="display:none;">
					<div id="daylightsetglasspiece">
					<div class="clearfix"></div>
					<label class="leftalign group_option" for="">Coating</label>
						<div class="col-cus-6-1 custom_check_radio custom_text_align_right">
							<div class="left-cus-align">
								<input type="checkbox" class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_tgpd_coating"  id="text_tgpd_coating"/>
								<label class="text_tgpd_coating_int" for="text_tgpd_coating" ></label>
							</div>
							
							<label for="text_tgpd_coating_int " id="interior_ttdw">Interior</label>
						</div>
					<div class="col-cus-6-2">
						<label for="text_tgpd_coating_ext"  id="exterior_ttdh">Exterior</label>
					</div>
						<div class="" id="hide_coating_exterior" style="display:none;">
							<div class="col-cus-6-1">
								<div class="left-cus-align">
									<input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_coating_interior" value="verify_tgpd_coating_interior" name="verify_tgpd_coating_interior"  />
									<label for="verify_tgpd_coating_interior" class="verify-label"> </label>
								</div>
								<input id="text_tgpd_coating_interior" type="text" class="form-control user-success size-input" placeholder="Coating Interior">
							</div>
							<div class="col-cus-6-2 col-close">
								<input id="text_tgpd_coating_exterior"  onkeypress="" type="text" class="form-control user-success size-input"  placeholder="Coating Exterior">
								<div class="right-cus-align">
									<input type="checkbox" class="verify-hidden-checkbox" id="verify_tgpd_coating_exterior" value="verify_tgpd_coating_exterior" name="verify_tgpd_coating_exterior" />
									<label for="verify_tgpd_coating_exterior" class="verify-label"> </label>
								</div>
							</div>
						</div>
					</div>
				</div> -->

				<div class="col-sm-12 col-md-12 verification-field" id="glass_set_without_glass_select">
				  <div class="row">
					<div class="col-xs-12 custom_checkbox_move verification-field">
						<label class="leftalign group_option" for="select_tgpd_glass_set">Set</label>
					  <div class="input-group custom_addon">
						<div class="input-group-addon custom_checkbox_add">
						  <input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_glass_set" value="verify_tgpd_glass_set" name="verify_tgpd_glass_set" />
						  <label class="leftalign verify-label" for="verify_tgpd_glass_set"> </label>
						</div>

						<select class="form-control" id="select_tgpd_glass_set">
						  <?php include 'options/glass-set-options.php'; ?>
						</select>
					  </div>
					</div>
				  </div>
				</div>

		     <div class="col-xs-12 verification-field" id="lift_without_glass_type">
			  <div class="row">
				<div class="col-xs-12 custom_checkbox_move verification-field">
				  <label class="leftalign group_option" for="text_tgpd_glass_lift">Lift</label>
				  <div class="input-group custom_addon">
					<div class="input-group-addon custom_checkbox_add">
					  <input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_glass_lift" value="verify_tgpd_glass_lift" name="verify_tgpd_glass_lift" />
					  <label class="leftalign verify-label" for="verify_tgpd_glass_lift"> </label>
					</div>
					  <input id="text_tgpd_glass_lift" class="form-control text-num" name="text_tgpd_glass_lift" type="text" placeholder="Glass Lift" />
				  </div>
				</div>
			  </div>
			</div>

			 <div class="col-xs-12" id="spacer_color_without_glass_type">
			  <fieldset id="fieldset_tgpd_glass_type">
				<div class="row">
				  <div class="col-xs-12  verification-field">
					<div class="row">
					  <div class="col-xs-12 custom_checkbox_move">
						<label class="leftalign group_option" for="select_tgpd_spacer_color">Spacer color</label>
						<div class="input-group custom_addon">
						  <div class="input-group-addon custom_checkbox_add">
							<input type="checkbox" class="user-success verify-hidden-checkbox"  id="verify_tgpd_spacer_color" value="verify_tgpd_spacer_color" name="verify_tgpd_spacer_color" />
							<label class="leftalign verify-label" for="verify_tgpd_spacer_color"> </label>
						  </div>
						  <select class="form-control" id="select_tgpd_spacer_color" name="select_tgpd_spacer_color">
							<option value="not_applicable">Spacer color</option>
							<option value="Clear" selected="selected">Clear</option>
							<option value="Black">Black</option>
							<option value="Bronze">Bronze</option>
							<option value="Clear (Low Profile)">Clear (Low Profile)</option>
							<option value="Black (Low Profile)">Black (Low Profile)</option>
							<option value="Bronze (Low Profile)">Bronze (Low Profile)</option>
						  </select>
						</div>
					  </div>
					</div>
				  </div>
				</div>
			  </fieldset>
			</div>

       <!-- MODEL -->

           <div class="col-sm-12 col-md-12 verification-field shape-container-day">
          <div id="daylightsetglasspiece">
            <label class="leftalign group_option" for="">Glass Sizes</label>
            <br>
            <div class="col-cus-6-1 custom_check_radio custom_text_align_right">
			<div class="left-cus-align">
				  <input type="checkbox" class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_tgpd_daylight"  id="text_tgpd_daylight"/>
				  <label class="text_tgpd_daylight_width" for="text_tgpd_daylight" ></label>
			</div>
            <label for="text_tgpd_daylight_width " id="ttdw">DLO Width</label>
			
            
            
            
            
            </div>
            <div class="col-cus-6-2">
              <label for="text_tgpd_go_height"  id="ttdh" >DLO Height</label>
            </div>
            
			<div class="" id="hide_width_height">

            <div class="col-cus-6-1">
			   <div class="left-cus-align">
                <input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_daylight_width" value="verify_tgpd_daylight_width" name="verify_tgpd_daylight_width"  />
                <label for="verify_tgpd_daylight_width" class="verify-label"> </label>
              </div>

              <input id="text_tgpd_daylight_width" type="text" class="form-control user-success size-input" placeholder="Width">
            </div>
            <div class="col-cus-6-2 col-close">
              <input id="text_tgpd_daylight_height"  onkeypress="" type="text" class="form-control user-success size-input"  placeholder="Height">
			  <div class="right-cus-align">
                <input type="checkbox" class="verify-hidden-checkbox" id="verify_tgpd_daylight_height" value="verify_tgpd_daylight_height" name="verify_tgpd_daylight_height" />
              <label for="verify_tgpd_daylight_height" class="verify-label"> </label>
              </div>
            </div>
			</div>
          </div>
        </div>
        <div class="col-sm-12 verification-field shape-container-go">
          <div id="div_tgpd_day_size_plus">
            <div class="col-cus-6-1 center" style="padding-top:7px; padding-bottom:11px;">
              <b>+</b>
              <div class="left-cus-align">
              </div>
            </div>
            <div class="col-cus-6-2 center" style="padding-top:7px; padding-bottom:11px;">
              <b>+</b>
            <div class="right-cus-align">
            </div>
          </div></div>

		<div id="extra_width_extra_hieght">
            <div class="col-cus-6-1">
				<div class="left-cus-align">
				  <input type="checkbox"  id="verify_set_width" class="user-success verify-hidden-checkbox" name="verify_set_width" />
				  <label class="verify-label" for="verify_set_width"></label>
				</div>
				<select id="text_tgpd_extra_width" type="text" class="form-control user-success size-input"  >
					<?php include"options/set-formula.php";  ?>
				</select>
            </div>
            <div class="col-cus-6-2 col-close">
              <select id="text_tgpd_extra_height" type="text" class="form-control user-success size-input" >
					<?php include"options/set-formula.php";  ?>
              </select>
			  <div class="right-cus-align">
				<input type="checkbox" class="verify-hidden-checkbox"  id="verify_set_height" name="verify_set_height" />
				<label for="verify_set_height" class="verify-label"></label>
			  </div>
            </div>
		</div>
         
        </div>
        <div class="col-sm-12 col-md-12 verification-field shape-container-go">
          <div id="div_tgpd_go_size">
            <div class="col-cus-6-1 custom_check_radio custom_text_align_right">
				<div class="left-cus-align">
					  <input type="checkbox" class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_tgpd_gosize"  id="text_tgpd_gosize"/>
					  <label class="verify_tgpd_daylight_height_check" for="text_tgpd_gosize" ></label>
				</div>
              <label for="verify_go_size_width_go_width"  id="verify_go_size_width_go_width">Go Width</label>
            </div>
            <div class="col-cus-6-2">
              <label for="text_tgpd_go_height"  id="ttgh" >Go Height</label>
            </div>
			<div id="hide_height_width">
            <div class="col-cus-6-1">
				<div class="left-cus-align">
				  <input type="checkbox"  id="verify_go_size_width" class="user-success verify-hidden-checkbox " value="" name="verify_go_size_width" />
				  <label class="verify-label" for="verify_go_size_width"></label>
				</div>
				<input id="text_tgpd_go_width"  type="text" class="form-control user-success size-input" placeholder="Go Width" style="height:44px">
            </div>
            <div class="col-cus-6-2 col-close">
              <input id="text_tgpd_go_height" type="text" class="form-control user-success size-input" placeholder="Go Height" style="height:44px">
				  <div class="right-cus-align">
					   <input type="checkbox" class="verify-hidden-checkbox"  id="verify_go_size_height" value="" name="verify_go_size_height" />
					  <label for="verify_go_size_height" class="verify-label"></label>
					</div>
				</div>
			</div>
          </div>
        </div>
            <div class="col-xs-12" id="div_select_tgpd_size_verification_status" style="display:none;">
               <label class="leftalign" for="select_tgpd_size_verification_status">Size Verification Status</label>
               <select class="form-control" id="select_tgpd_size_verification_status" name="select_tgpd_size_verification_status">
                  <option value="not_applicable" disabled selected>Size verification status</option>
                  <option value=""></option>
                  <option value="verify_on_order">Verify on Order</option>
                  <option value="block_size">Block Size</option>
               </select>
            </div>
     <!-- </div>
       <div class="form-group">
              <div class="col-sm-12 col-md-12 ">

               <div id="div_tgpd_go_size" class="row">
                  <div class="col-xs-5">
                  <label class="leftalign">Set Width Formula</label>
                     <input id="text_tgpd_extra_width"   type="text" name="text_tgpd_extra_width" class="form-control user-success" placeholder="Width">
                  </div>
                  <div class="col-xs-2" style="height:30px;">

                  </div>
                  <div class="col-xs-5">
                  <label class="leftalign">Set Height Formula</label>
                     <input id="text_tgpd_extra_height"   name="text_tgpd_extra_height"  type="text" class="form-control user-success" placeholder="Height">
                   </div>
               </div>
            </div>
            </div>---->
			<!--<div class="col-xs-12 verification-field round_cricle" id="lift_inside_with_glass_type">
			  <div class="row">
				<div class="col-xs-12 custom_checkbox_move verification-field">
					<div class="col-cus-6-1 custom_check_radio custom_text_align_left" id="text_tgpd_in_lift_position">
						<div class="left-cus-align">
							<input class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_lift_inside_position" id="text_lift_inside_position" type="checkbox">
							<label class="text_lift_inside_position" for="text_lift_inside_position"></label>
							<label class="leftalign group_option" for="text_tgpd_glass_lift" id="lift_inside">Lift Inside</label>
						</div> 
					</div>
				  <br>
				  <div class="input-group custom_addon" id="lift_inside_hide_on_glass_type">
					<div class="input-group-addon custom_checkbox_add">
					  <input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_glass_inside_lift_with_glass_type" value="verify_tgpd_glass_inside_lift_with_glass_type" name="verify_tgpd_glass_inside_lift_with_glass_type" />
					  <label class="leftalign verify-label" for="verify_tgpd_glass_inside_lift_with_glass_type"> </label>
					</div>
					  <input id="text_tgpd_glass_inside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_inside_lift_with_glass_type" type="text" placeholder="Lift Inside" />
				  </div>
				</div>
			  </div>
			</div>
			
			
			<div class="col-xs-12 verification-field round_cricle2" id="lift_outside_with_glass_type">
			  <div class="row">
				<div class="col-xs-12 custom_checkbox_move verification-field">
					<div class="col-cus-6-1 custom_check_radio custom_text_align_left" id="text_tgpd_out_lift_position">
						<div class="left-cus-align">
							<input class="user-success verify-hidden-checkbox verify-overide verify-active" name="text_lift_outside_position" id="text_lift_outside_position" type="checkbox">
							<label class="text_lift_outside_position" for="text_lift_outside_position"></label>
							<label class="leftalign group_option" for="text_tgpd_glass_lift" id="lift_outside">Lift Outside</label>
						</div> 
					</div>
				  <br>
				  <div class="input-group custom_addon"  id="lift_outside_hide_on_glass_type">
					<div class="input-group-addon custom_checkbox_add">
					  <input type="checkbox" class="user-success verify-hidden-checkbox" id="verify_tgpd_glass_outside_lift_with_glass_type" value="verify_tgpd_glass_outside_lift_with_glass_type" name="verify_tgpd_glass_outside_lift_with_glass_type" />
					  <label class="leftalign verify-label" for="verify_tgpd_glass_outside_lift_with_glass_type"> </label>
					</div>
					  <input id="text_tgpd_glass_outside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_outside_lift_with_glass_type" type="text" placeholder="Lift Outside" />
				  </div>
				</div>
			  </div>
			</div>-->
			
			
			
            <div class="form-group">
              <div class="col-xs-12 no-padding fabrication">
              <div class="col-xs-6 col-xs-offset-1 col-sm-4 col-sm-offset-1 no-padding">
                 <label class="leftalign">Fabrication?</label>
                 <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_fabrication" id="checkbox_tgpd_glass_fabrication" value="glass_fabrication">
                    <span>
                       <span>No</span>
                       <span>Yes</span>
                       <a></a>
                    </span>
                 </label>
              </div>
              <div class="col-xs-12 round_cricle" style="padding:0px;">
                 <fieldset id="fieldset_tgpd_glass_fabrication">
                    <div class="form-group">
                       <div class="col-sm-offset-1 no-padding-left col-sm-11 custom_check_radio polished-chech">
					   <div class="left-cus-align custom_check_radio circle_move">
						   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_polished_edges_hide" name="text_tgpd_fabrication_polished_edges_hide" type="checkbox">
								<label class="text_lift_outside_positio" for="text_tgpd_fabrication_polished_edges_hide"></label>
						</div>
                          <label class="leftalign" style="font-weight:700;" for="text_tgpd_fabrication_polished_edges">Polished Edges</label>
                          <input id="text_tgpd_fabrication_polished_edges" name="text_tgpd_fabrication_polished_edges" type="text" class="form-control"  placeholder="Polished Edges">
                       
					  </div>
                       <div class="col-sm-offset-1 no-padding-left col-sm-11 pattern_sec round_cricle pattern-check">
					   <div class="left-cus-align custom_check_radio circle_move">
						   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_pattern_hide" name="text_tgpd_fabrication_pattern_hide" type="checkbox">
								<label class="text_lift_outside_position21" for="text_tgpd_fabrication_pattern_hide"></label>
						</div>
                          <label class="leftalign" style="font-weight:700;" for="text_tgpd_fabrication_pattern">Pattern</label>
                          <input id="text_tgpd_fabrication_pattern" name="text_tgpd_fabrication_pattern" type="text" class="form-control" autocomplete="on" placeholder="Pattern">
                       </div>
                       <div class="col-sm-offset-1 col-xs-11 no-padding-left holes_sec round_cricle holes-check"  id="glass_piece_holes_lbl">
					   <div class="left-cus-align custom_check_radio circle_move custom_move_radio">
							   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_holes_hide_new" name="text_tgpd_fabrication_holes_hide_new" type="checkbox">
									<label class="text_tgpd_fabrication_holes_hide_new" for="text_tgpd_fabrication_holes_hide_new"></label>
							</div>
                         <label class="leftalign label-header" for="text_tgpd_fabrication_holes">Holes</label>
                       </div>
					   <div class="col-sm-offset-1 no-padding-left col-sm-11 holesold holes1-check" >
						   <div class="left-cus-align custom_check_radio circle_move">
							   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_holes_hide_old" name="text_tgpd_fabrication_holes_hide_old" type="checkbox">
									<label class="text_tgpd_fabrication_holes_hide_old" for="text_tgpd_fabrication_holes_hide_old"></label>
							</div>
                          <label class="leftalign " for="text_tgpd_fabrication_holes">Holes</label>
                          <input id="text_tgpd_fabrication_holes" name="text_tgpd_fabrication_holes" type="text" class="form-control"  placeholder="Holes">
                       </div>
<!---------------------------------------------------------------------------------------->
				<div id="glass_piece_holes_div" class="location_sec">
					   
					   </div>
<!------------------------------------------------------------------------------------------>

<!--/*sag-152*/-->
<!--/*sag-154*/-->
 <div class="col-sm-offset-1 no-padding-left col-sm-11 grid_sec round_cricle grids-check">
					   <div class="left-cus-align custom_check_radio circle_move">
						   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_gride_hide" name="text_tgpd_fabrication_gride_hide" type="checkbox">
								<label class="text_lift_outside_position21" for="text_tgpd_fabrication_gride_hide"></label>
						</div>
                          <label class="leftalign " style="font-weight:700;" for="text_tgpd_fabrication_gride">Grids</label>
                          <div class="form-group fabrication" id="grids_and_fabrication"> 
                            <div class="col-xs-12 fabrication fab-rihgt" style="padding-left:0px;padding-right: 10px;">
                                  <fieldset id="fieldset_tgpd_glass_grids">
                                     <div class="form-group">
                                     	    <div class="col-xs-8 no-padding-left padd-width-b">
                                           <label class="leftalign" for="select_tgpd_grids_thickness">Thickness</label>
                                           <select class="form-control" id="select_tgpd_grids_thickness" name="select_tgpd_grids_thickness">
                                              <option value="not_applicable">Grids thickness</option>
                                              <option value="3/8">3/8</option>
                                              <option value="7/16">7/16</option>
                                              <option value="1/2">1/2</option>
                                              <option value="5/8">5/8</option>
                                              <option value="9/16">9/16</option>
                                           </select>
                                        </div>
                                        <div class="col-xs-8 no-padding-left padd-width-b">
                                           <label class="leftalign" for="text_tgpd_grids_color">Grids Color</label><!--sag 158 -->
                                           <select class="form-control" id="text_tgpd_grids_color"" name="text_tgpd_grids_color">
                                         <option value="not_applicable">Grid Color</option>
                                          <option value="white">white</option>
                                          <option value="bronze">bronze</option>
                                          <option value="tan">tan</option>
                                          <option value="green">green</option>
                                          <option value="mill">mill</option>
                                          <option value="custom">custom</option>
                                           </select>
                                        <label class="leftalign text_tgpd_grids_color_custom_label" style="display:none;" for="text_tgpd_grids_color_custom">Grids Color Custom</label>   
                                        <input style="display:none;"id="text_tgpd_grids_color_custom" name="text_tgpd_grids_color_custom" type="text" class="form-control" placeholder="Custom Color">
                                        <!--sag 158 -->
                                        </div>
                
                                        <div class="col-xs-8 no-padding-left padd-width-b">
                                           <label class="leftalign smaller" for="number_tgpd_pattern_horizontal_grids">Horizontal Grids</label>
                                          <div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="number_tgpd_pattern_horizontal_grids"><span class="glyphicon glyphicon-minus"></span></button></span> <input id="number_tgpd_pattern_horizontal_grids" name="number_tgpd_pattern_horizontal_grids" type="text" class="form-control text-num input-number-plus-minus qty-align-new" autocomplete="on"  placeholder="Hor. Grids" min="0" max="100" ><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="number_tgpd_pattern_horizontal_grids"><span class="glyphicon glyphicon-plus"></span></button></span></div>
                                        </div>
                                        <div class="col-xs-8 no-padding-left padd-width-b">
                                           <label class="leftalign" for="number_tgpd_pattern_vertical_grids">Vertical Grids</label>
                                          <div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="number_tgpd_pattern_vertical_grids"><span class="glyphicon glyphicon-minus"></span></button></span> <input id="number_tgpd_pattern_vertical_grids" name="number_tgpd_pattern_vertical_grids" type="text" class="form-control text-num input-number-plus-minus qty-align-new" autocomplete="on"    placeholder="Ver. Grids" min="0" max="100" ><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="number_tgpd_pattern_vertical_grids"><span class="glyphicon glyphicon-plus"></span></button>
                                          </span>
                                          </div>
                                        </div>
                                    
                                     </div>
                                  </fieldset>
                               </div>
                            </div>
                       </div>
                    <!-- this is for Offset -->
                    <div class="col-sm-offset-1 col-xs-11 no-padding-left offset_sec round_cricle offset-check"  id="glass_piece_offset_lbl">
					   <div class="left-cus-align custom_check_radio circle_move custom_move_radio">
							   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_offset_hide_new" name="text_tgpd_fabrication_offset_hide_new" type="checkbox">
									<label class="text_tgpd_fabrication_offset_hide_new" for="text_tgpd_fabrication_offset_hide_new"></label>
							</div>
                         <label class="leftalign label-header" for="text_tgpd_fabrication_offset">Offset</label>
                       </div>
					   <div class="col-sm-offset-1 no-padding-left col-sm-11 holesold offset1-check" >
						   <div class="left-cus-align custom_check_radio circle_move">
							   <input class="user-success verify-hidden-checkbox verify-overide verify-active" id="text_tgpd_fabrication_offset_hide_old" name="text_tgpd_fabrication_offset_hide_old" type="checkbox">
									<label class="text_tgpd_fabrication_offset_hide_old" for="text_tgpd_fabrication_offset_hide_old"></label>
							</div>
                          <label class="leftalign  " for="text_tgpd_fabrication_holes">Offset</label>
                          <input id="text_tgpd_fabrication_offset" name="text_tgpd_fabrication_offset" type="text" class="form-control"  placeholder="Offset">
                       </div>



<!---------------------------------------------------------------------------------------->
                <div id="glass_piece_offset_div" class="location_sec off_loc_sec">
					   
				</div>
<!------------------------------------------------------------------------------------------>
                    </div>
                 </fieldset>
              </div>
           </div>
        </div>
         <!--<div class="form-group fabrication" id="grids_and_fabrication"> <!-- ** only show on igu selected ** -->
              <!-- <div class="col-xs-12 no-padding">
                 <div class="col-xs-6 col-xs-offset-1 col-sm-4 col-sm-offset-1 no-padding">
                    <label class="leftalign">Grids?</label>
                    <label class="switch-light switch-candy">
                       <input type="checkbox" name="checkbox_tgpd_glass_grids" id="checkbox_tgpd_glass_grids" value="glass_grids" />
                       <span>
                          <span>No</span>
                          <span>Yes</span>
                          <a></a>
                       </span>
                    </label>
                  </div>
               </div>
               <div class="col-xs-12 fabrication" style="padding:0px;">
                  <fieldset id="fieldset_tgpd_glass_grids">
                     <div class="form-group">
                     	    <div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left">
                           <label class="leftalign" for="select_tgpd_grids_thickness">Thickness</label>
                           <select class="form-control" id="select_tgpd_grids_thickness" name="select_tgpd_grids_thickness">
                              <option value="not_applicable">Grids thickness</option>
                              <option value="3/8">3/8</option>
                              <option value="7/16">7/16</option>
                              <option value="1/2">1/2</option>
                              <option value="5/8">5/8</option>
                              <option value="9/16">9/16</option>
                           </select>
                        </div>
                        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left">
                           <label class="leftalign" for="text_tgpd_grids_color">Grids Color</label>
                           <input id="text_tgpd_grids_color" name="text_tgpd_grids_color" type="text" class="form-control" autocomplete="on"  placeholder="Color">
                        </div>

                        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left">
                           <label class="leftalign smaller" for="number_tgpd_pattern_horizontal_grids">Horizontal Grids</label>
                          <div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="number_tgpd_pattern_horizontal_grids"><span class="glyphicon glyphicon-minus"></span></button></span> <input id="number_tgpd_pattern_horizontal_grids" name="number_tgpd_pattern_horizontal_grids" type="text" class="form-control text-num input-number-plus-minus qty-align-new" autocomplete="on"  placeholder="Hor. Grids" min="0" max="100" ><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="number_tgpd_pattern_horizontal_grids"><span class="glyphicon glyphicon-plus"></span></button></span></div>
                        </div>
                        <div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left">
                           <label class="leftalign" for="number_tgpd_pattern_vertical_grids">Vertical Grids</label>
                          <div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="number_tgpd_pattern_vertical_grids"><span class="glyphicon glyphicon-minus"></span></button></span> <input id="number_tgpd_pattern_vertical_grids" name="number_tgpd_pattern_vertical_grids" type="text" class="form-control text-num input-number-plus-minus qty-align-new" autocomplete="on"    placeholder="Ver. Grids" min="0" max="100" ><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="number_tgpd_pattern_vertical_grids"><span class="glyphicon glyphicon-plus"></span></button>
                          </span>
                          </div>
                        </div>
                    
                     </div>
                  </fieldset>
               </div>
            </div>-->
			<div id="glass_pricing_with_glass_type_select_glass" class="fabrication">
         <div class="form-group" id="glass_pricing_with_glass_type">
            <div class="col-xs-6 col-xs-offset-1 col-sm-4 col-sm-offset-1 no-padding">
              <label class="leftalign">Glass pricing</label>
               <label class="switch-light switch-candy">
                  <input type="checkbox" name="checkbox_tgpd_glass_pricing_sag_or_quote" id="checkbox_tgpd_glass_pricing_sag_or_quote" value="sag_or_quote">
                  <span>
                     <span>SAG</span>
                     <span>Quote</span>
                     <a></a>
                  </span>
               </label>
            </div>
            <div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left">
               <fieldset id="fieldset_tgpd_glass_pricing_sag_or_quote">
                  <div class="form-group">
                     <div class=" col-xs-12" style="padding:0px;">
                       <label class="leftalign">Manufacturer</label>
                        <input id="text_tgpd_glass_pricing_sag_or_quote_manufacturer" type="text" class="form-control"   placeholder="Manufacturer">
                     </div>
                  </div>
               </fieldset>
            </div>
         </div>
		</div>


	 <div class="form-group check_price_insert">
            <div class="col-xs-12">
               <label class="leftalign" for="textarea_tgpd_instructions">Instructions</label>
               <textarea id="textarea_tgpd_instructions" maxlength="300" class="form-control" rows="3" autocomplete="on" placeholder="Instructions"></textarea>
            </div>
         </div>
         <div class="form-group">
            <div class="col-sm-12 col-md-12">
               <label class="leftalign" for="uploadFile">Select file(s) to upload</label>
               <div class="row">
                  <div class="col-xs-7 col-sm-9">
                     <input placeholder="Choose File" disabled="disabled" class="form-control" />
                  </div>
                  <div class="col-xs-5 col-sm-3">
                     <div class="fileUpload btn btn-primary">
                        <span>Choose file(s)</span>
                        <input id="file_tgpd_glass_picture" type="file" multiple class="upload" accept="image/*" />
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-xs-12">
                     <div id="div_tgpd_picture_filelist_holder">
                        <div id="div_tgpd_picture_filelist"></div>
                        <div id="div_tgpd_picture_filelist_controls">
                           <div class="btn-group" role="group" aria-label="Picture File List Toolbar">
                              <button id="btn_tgpd_clear_picture_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Pictures">
                                 <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                              </button>
                              <button id="btn_tgpd_upload_picture_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Pictures">
                                 <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
                              </button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> <!-- *******END OF LEFT SECTION****** -->
      </div>
      <div class="form-section col-md-12 col-lg-5"> <!-- ****** START RIGHT SECTION  ******* -->
        <div class="row">
         <div class="form-group">
            <div class="col-xs-12 col-md-12">
               <label class="leftalign">Glass reminders</label>
            </div>
         </div>
         <div class="form-group">
            <div class="col-xs-12 col-sm-12">
               <div class="panel panel-default">
                  <div class="panel-body">
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
				    <h5 class="col-header">Solar film?</h5>
				    <label class="switch-light switch-candy">
				       <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="checkbox_tgpd_glass_reminders_solar_film" value="solar_film">
				       <span>
					  <span>No</span>
					  <span>Yes</span>
					  <a></a>
				       </span>
				    </label>
			   </div>
			</div>
                     </div>
                        <div class="col-xs-12">
                              <div class="form-group">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film1">
								 <div class="row">
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_sag" value="sag">SAG
                                       </label>
                                    </div>
                                    <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6">
                                       <label>
                                          <input type="radio" name="radio_tgpd_glass_reminders_solar_film_responsibility" id="radio_tgpd_glass_reminders_solar_film_responsibility_customer" value="customer">Customer
                                       </label>
                                    </div>
									</div>
                                 </fieldset>
                              </div>
                           </div>
                           <div class="col-xs-12">
                              <div class="row">
                                 <fieldset id="fieldset_tgpd_glass_reminders_solar_film2">
                                    <div class="form-group">
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_type" type="text" class="form-control"  placeholder="Film type">
                                       </div>
                                       <div class="col-sm-6">
                                          <input id="text_tgpd_glass_reminders_solar_film_source" type="text" class="form-control" autocomplete="on" placeholder="Film source">
                                       </div>
                                    </div>
                                 </fieldset>
                              </div>
                           </div>

		     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header">Wet seal?</h5>
                              <label class="switch-light switch-candy" onclick="">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="checkbox_tgpd_glass_reminders_wet_seal" value="wet_seal">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12">
			      <div class="row">
                              <fieldset id="fieldset_tgpd_glass_reminders_wet_seal">
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag">SAG
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_half_day">SAG Sub Half Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_sag" value="sag_sub_full_day">SAG Sub Full Day
                                    </label>
                                 </div>
                                 <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                    <label>
                                       <input type="radio" name="radio_tgpd_glass_reminders_wet_seal_responsibility" id="radio_tgpd_glass_reminders_wet_seal_responsibility_customer" value="customer">Customer
                                    </label>
                                 </div>
                              </fieldset>
			      </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Furniture to Move?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="checkbox_tgpd_glass_reminders_furniture_to_move" value="furniture_to_move">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>
                           <div class="col-xs-12 col-sm-12">
			      <div class="row">
				 <div class="col-sm-12">
				 <fieldset id="fieldset_tgpd_glass_reminders_furniture_to_move">
				    <input id="text_tgpd_glass_reminders_furniture_to_move_comment" class="form-control" type="text"   placeholder="Comment">
				 </fieldset>
				 </div>
			      </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                              <h5 class="col-header smaller">Walls/ceilings cut?</h5>
                              <label class="switch-light switch-candy">
                                 <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut" value="walls_or_ceilings_to_cut">
                                 <span>
                                    <span>No</span>
                                    <span>Yes</span>
                                    <a></a>
                                 </span>
                              </label>
                           </div>

                           <div class="col-xs-12 col-sm-12">
                              <div class="row">
                                 <div class="">
                                    <div class="form-group">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1">
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_sag" value="sag">SAG
                                             </label>
                                          </div>
                                          <div class="col-xs-6 col-sm-3 col-md-6 col-lg-6">
                                             <label>
                                                <input type="radio" name="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility" id="radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_customer" value="customer">Customer
                                             </label>
                                          </div>
                                       </fieldset>
                                    </div>
                                 </div>
                                 <div class="col-sm-12 col-md-12 leftalign">
                                    <div class="row">
				       <div class="col-sm-12">
                                       <fieldset id="fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2">
                                          <input id="text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment" class="form-control" type="text" autocomplete="on"  placeholder="Comment">
                                       </fieldset>
				       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
			   <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
			      <h5 class="col-header smaller">Blind needs Removing?</h5>
			      <label class="switch-light switch-candy">
				 <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="checkbox_tgpd_glass_reminders_blind_needs_removing" value="blind_needs_removing">
				 <span>
				    <span>No</span>
				    <span>Yes</span>
				    <a></a>
				 </span>
			      </label>
			   </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
                     <div class="col-sm-12">
                        <div class="row">
                              <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Glass Fits Elevator?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="checkbox_tgpd_glass_reminders_glass_fits_elevator" value="glass_fits_elevator" checked>
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                              </div>
                           <div class="col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
		     <div class="col-xs-12 col-sm-12">
                        <div class="row">
					<!--	lucky-->
                           <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                                    <h5 class="col-header smaller">Color Waiver ?</h5>
                                    <label class="switch-light switch-candy">
                                       <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_color_waiver" id="checkbox_tgpd_glass_reminders_add_color_waiver" value="add_color_waiver">
                                       <span>
                                          <span>No</span>
                                          <span>Yes</span>
                                          <a></a>
                                       </span>
                                    </label>
                                 </div>
                           <div class="col-xs-12 col-sm-12 col-md-7 leftalign">
                           </div>
                        </div>
                     </div>
		     <div class="col-sm-12">
                        <div class="row">
			   <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
			      <h5 class="col-header smaller">Damage Waiver ?</h5>
			      <label class="switch-light switch-candy">
				 <input type="checkbox" name="checkbox_tgpd_glass_reminders_add_damage_waiver" id="checkbox_tgpd_glass_reminders_add_damage_waiver" value="damage_waiver" >
				 <span>
				    <span>No</span>
				    <span>Yes</span>
				    <a></a>
				 </span>
			      </label>
			   </div>
					<div class="col-sm-12 col-md-12 dwmargin">
						<div class="row">
							<div class="col-xs-12">
							<fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section_for_select">
								<select class="form-control" id="select_tgpd_glass_damage_waiver" name="select_tgpd_glass_damage_waiver">
									 <option value="not_applicable">Damage Waiver</option>
									<option value="Removal/Reinstall of Glass Currently Installed">Removal/Reinstall of Glass Currently Installed </option>
									<option value="Handling of Customers Materials">Handling of Customers Materials </option>
									<option value="Adjacent Glass">Adjacent Glass</option>
									<option value="select_tgpd_glass_damage_waiver_for_text">Other</option>
								</select>
								</fieldset>
							</div>
						</div>
					</div>
				<div class="col-sm-12 col-md-12 leftalign dwmargin">
					<div class="row">
						<div class="col-sm-12">
							<fieldset id="fieldset_tgpd_glass_damage_waiver_reminder_section">
								<input id="text_tgpd_glass_damage_waiver_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_damage_waiver_reminder_section">
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>
				




		<div class="col-sm-12 disclaimers_sec">
				<div class="row">
					<div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
						<h5 class="col-header smaller">Disclaimers ?</h5>
							<label class="switch-light switch-candy">
								<input type="checkbox" name="checkbox_tgpd_glass_reminders_add_disclamers" id="checkbox_tgpd_glass_reminders_add_disclamers" value="disclamers" >
									<span>
										<span>No</span>
										<span>Yes</span>
										<a></a>
									</span>
							</label>
					</div>

					<div class="col-sm-12 col-md-12 dwmargin">
						<div class="row">
							<div class="col-xs-12">
							<fieldset id="fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers">
								<select class="form-control" id="select_tgpd_glass_disclamers" name="select_tgpd_glass_disclamers">
									 <option value="not_applicable">Disclaimers</option>
									<option value="Wood Bead">Wood Bead</option>
									<option value="Painted Frames (AFTERMARKET)">Painted Frames (AFTERMARKET)</option>
									<option value="TBD">TBD</option>
									<option value="select_tgpd_glass_disclamers_for_text">Other</option>
								</select>
								</fieldset>
							</div>
						</div>
					</div>
				
				<div class="col-sm-12 col-md-12 leftalign dwmargin">
					<div class="row">
						<div class="col-sm-12 damage_comment">
							<fieldset id="fieldset_tgpd_glass_disclamers_reminder_section">
								<input id="text_tgpd_glass_disclamers_reminder_section" class="form-control" type="text" autocomplete="on"  placeholder="Comment" name="text_tgpd_glass_disclamers_reminder_section">
							</fieldset>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Inside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="text_lift_inside_position" value="lift_inside">
                    <span>
    					<span>No</span>
    					<span>Yes</span>
    					<a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_inside_section1">
                           <input id="text_tgpd_glass_inside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_inside_lift_with_glass_type" type="text" placeholder="Lift Inside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    		
    <div class="col-sm-12 disclaimers_sec">
        <div class="row">
            <div class="col-xs-10 col-sm-4 col-md-6 col-lg-4">
                <h5 class="col-header smaller">Lift Outside</h5>
                <label class="switch-light switch-candy">
                    <input type="checkbox" name="checkbox_tgpd_glass_reminders" id="text_lift_outside_position" value="lift_outside">
                    <span>
    					<span>No</span>
    					<span>Yes</span>
    					<a></a>
                    </span>
                </label>
            </div>
          
            <div class="col-sm-12 col-md-12 leftalign dwmargin">
                <div class="row">
                    <div class="col-sm-12 damage_comment">
                        <fieldset id="fieldset_tgpd_glass_lift_outside">
                           <input id="text_tgpd_glass_outside_lift_with_glass_type" class="form-control text-num" name="text_tgpd_glass_outside_lift_with_glass_type" type="text" placeholder="Lift Outside" />
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
    

          
    		
          
    
<!------------------------------------------------------------------------------------------------------------------------------------------>					
					 
					 
					 
					 
					 
                  </div>
               </div>
            </div>
         </div>
		 <div class="form-group">
            <div class="col-xs-12">
               <label class="leftalign">Glass materials</label>
            </div>
         </div>
		 <div class="form-group">
	    <div class="col-xs-12">
	       <div class="panel panel-default">
		  <div class="panel-body chaulksection materials-section" >
		     <div class="form-group addqty"  id="add_quntity_0">
					<div class="add_quntity"></div>
                     <div class="add_tap_scaff"></div>
                     <div class="add_tap_amount"></div>
                     <div class="add_tap_channel"></div>
		     </div>
		     <div class="form-group">
			<div class="col-xs-12 col-lg-10">
               <label class="leftalign" for="text_tgpd_miscellaneous">Miscellaneous</label>
			   <input id="text_tgpd_miscellaneous" class="form-control" type="text" autocomplete="on" maxlength="255" placeholder="Miscellaneous">
                &nbsp;
			</div>
		     </div>
		  </div>
	       </div>
	    </div>


	 </div>

       </div><!-- ****** END OF RIGHT SECTION  ****** -->
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
         <label class="leftalign">Click below for sketch canvas</label>
         <div id="canvas-wrap">
            <h5>Sketch Diagram or Fabrication.</h5>
            <div class="row">
               <div class="col-xs-12">
                  <div id="div_tgpd_canvas_holder">
                     <div id="div_tgpd_canvas_controls">
                        <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                           <a id="btn_tgpd_canvas_eraser_tool" class="btn btn-default" href="#canvas_tgpd_glass_sketch" role="button" data-tool="eraser" title="Eraser Tool">
                              <span class="glyphicon glyphicon-erase" aria-hidden="true"></span>
                           </a>
                           <a id="btn_tgpd_canvas_marker_tool" class="btn btn-default" href="#canvas_tgpd_glass_sketch" role="button" data-tool="marker" title="Marker Tool">
                              <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                           </a>
                           <button id="btn_tgpd_clear_canvas" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Canvas">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                           </button>
                           <button id="btn_tgpd_canvas_to_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Canvas To Sketch File List">
                              <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
                           </button>
                        </div>
                        <div class="btn-group" role="group" aria-label="Canvas Toolbar">
                           <button id="btn_tgpd_canvas_marker_size" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Size">
                              <span class="glyphicon glyphicon-text-size" aria-hidden="true"></span>
                              <span class="caret"></span>
                           </button>
                           <ul id="list_tgpd_canvas_marker_size" class="dropdown-menu" aria-labelledby="btn_tgpd_canvas_marker_size">
                              <li><a id="btn_tgpd_canvas_marker_size_3" href="#canvas_tgpd_glass_sketch" data-size="3">3 points</a></li>
                              <li><a id="btn_tgpd_canvas_marker_size_5" href="#canvas_tgpd_glass_sketch" data-size="5">5 points</a></li>
                              <li><a id="btn_tgpd_canvas_marker_size_10" href="#canvas_tgpd_glass_sketch" data-size="10">10 points</a></li>
                              <li><a id="btn_tgpd_canvas_marker_size_15" href="#canvas_tgpd_glass_sketch" data-size="15">15 points</a></li>
                           </ul>
                        </div>
                        <div class="btn-group" role="group" aria-label="Sketch Toolbar">
                           <button id="btn_tgpd_canvas_marker_color" type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" title="Marker Color">
                              <span class="glyphicon glyphicon-text-color" aria-hidden="true"></span>
                              <span class="caret"></span>
                           </button>
                           <ul id="list_tgpd_canvas_marker_color" class="dropdown-menu" aria-labelledby="btn_tgpd_canvas_marker_color">
                             <li><a href="#canvas_tgpd_glass_sketch" data-color="#000" style="width:100%;height:30px;background:#000;"></a></li>
                             <li><a href="#canvas_tgpd_glass_sketch" data-color="#555" style="width:100%;height:30px;background:#555;"></a></li>
                             <li><a href="#canvas_tgpd_glass_sketch" data-color="#f00" style="width:100%;height:30px;background:#f00;"></a></li>
                             <li><a href="#canvas_tgpd_glass_sketch" data-color="#ff0" style="width:100%;height:30px;background:#ff0;"></a></li>
                             <li><a href="#canvas_tgpd_glass_sketch" data-color="#CCC" style="width:100%;height:30px;background:#CCC;"></a></li>
                             <li><a href="#canvas_tgpd_glass_sketch" data-color="#fff" style="width:100%;height:30px;background:#fff;"></a></li>
                           </ul>
                        </div>
                     </div>
                     <canvas id="canvas_tgpd_glass_sketch" width="800" height="300"></canvas>
                  </div>
               </div>
            </div>
            <!--perhaps row here...with sketch images ready for upload so they show and hide with canvas-->
         </div>
      </div>
   </div>
   <div class="form-group">
      <div class="col-xs-12">
	 <div id="div_tgpd_sketch_filelist_holder">
	    <div id="div_tgpd_sketch_filelist"></div>
	    <div id="div_tgpd_sketch_filelist_controls">
	       <div class="btn-group" role="group" aria-label="Sketch File List Toolbar">
		  <button id="btn_tgpd_clear_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Clear Sketches">
		     <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
		  </button>
		  <button id="btn_tgpd_upload_sketch_filelist" type="button" class="btn btn-default" aria-label="Left Align" title="Upload Sketches">
		     <span class="glyphicon glyphicon-cloud-upload" aria-hidden="true"></span>
		  </button>
	       </div>
	    </div>
	 </div>
      </div>
   </div>

</div>



<style>
	.top_m{
		margin-top: 3%
	}
	.remove_offset{
		margin: 0px;
    padding: 0 0px 0px 10px;
    background: #f7291b;
	}
	a#new_offset {
    margin-top: 20px;
}

</style>