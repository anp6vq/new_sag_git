function tagr_on_type_and_subtype_change(tagr_type, tagr_subtype) {
   console.log('tagr_on_type_and_subtype_change: tagr_type => ' + tagr_type + ', tagr_subtype => ' + tagr_subtype);
   if (typeof tagr_type === 'undefined' || tagr_type == null || typeof tagr_subtype === 'undefined' || tagr_subtype == null) return;
   jQuery('.door-container.visible').removeClass('visible').addClass('hidden');
   jQuery('#type_' + tagr_type + '_' + tagr_subtype + '-container').removeClass('hidden').addClass('visible');
}
jQuery(document).ready(function($) {
  //  $('.select-door-type').val('a');
  //  $('.select-door-subtype').val('single');
  //  $('.select-door-type').change(function(event) {
  //     var tagr_type = $(this).val();
  //     var tagr_subtype = $('#select-repair-door-subtype').val();
  //     tagr_on_type_and_subtype_change(tagr_type, tagr_subtype);
  //  });
  //  $('.select-door-subtype').change(function(event) {
  //     var tagr_type = $('#select-repair-door-type').val();
  //     var tagr_subtype = $(this).val();
  //     tagr_on_type_and_subtype_change(tagr_type, tagr_subtype);
  //  });
  $('.select-door-type').change(function(event) {
  });
  $('.select-door-subtype').change(function(event) {
    var subtype = $(this).val();
    var type    = $('#select-repair-door-type').val();
    var cat     = $('#select-repair-door-category').val();
    console.log('type is ' + type);
    console.log('subtype is ' + subtype);
    console.log('cat is ' + cat);
    $('.door-container.visible').removeClass('visible').addClass('hidden');
    console.log($('#' + cat + '-type_' + type + '_' + subtype + '-container'));
    $('#' + cat + '-type_' + type + '_' + subtype + '-container').removeClass('hidden').addClass('visible');
  });
});
