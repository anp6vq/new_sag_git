//sitewide helpers.
jQuery(document).ready(function($) {
  printHandleValues = function(handleTypeElem, handleTypeVal,doorItem){
    console.log("inside printHandleValues")
    switch (handleTypeVal) {
      case "null"           :
      case ""               :
      case "not_applicable" :
      case null             :
        console.log('handle type is null so will not be printed');
        break;
      default:
        console.log("handle type is NOTTTT null so will not be printed")
        handleTypeElem.val(handleTypeVal);
        if(handleTypeVal=="pull_handle_up_push_bar_3_holes"){

          $('#handlea').show();
          $('#handleb').show();
          $('#handlec').show();
          $('#handled').hide();
          $('#handlee').show();

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').show();
          $('#handled_label').hide();
          $('#handlee_label').show();

          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
          jQuery('#handlec').val(doorItem.handle_c);
          jQuery('#handlee').val(doorItem.handle_e);
        }


        if(handleTypeVal=="pull_handle_down_push_bar_3_holes"){
          $('#handlea').show();
          $('#handleb').show();
          $('#handlec').show();
          $('#handled').hide();
          $('#handlee').show();

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').show();
          $('#handled_label').hide();
          $('#handlee_label').show();

          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
          jQuery('#handlec').val(doorItem.handle_c);
          jQuery('#handlee').val(doorItem.handle_e);

        }


        if(handleTypeVal=="pull_handle_2_holes"){
          $('#handlea').show();
          $('#handleb').show()
          $('#handlec').show()
          $('#handled').hide()
          $('#handlee').hide()

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').show();
          $('#handled_label').hide();
          $('#handlee_label').hide();

          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
          jQuery('#handlec').val(doorItem.handle_c);

        }

        if(handleTypeVal=="pull_bar_2_holes"){

          $('#handlea').show();
          $('#handleb').show();
          $('#handlec').hide();
          $('#handled').hide();
          $('#handlee').show();

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').hide();
          $('#handled_label').hide();
          $('#handlee_label').show();

          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
          jQuery('#handlee').val(doorItem.handle_e);

        }


        if(handleTypeVal=="ladder_pull_3_holes"){
          $('#handlea').show();
          $('#handleb').show();
          $('#handlec').show();
          $('#handled').show();
          $('#handlee').hide();

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').show();
          $('#handled_label').show();
          $('#handlee_label').hide();

          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
          jQuery('#handlec').val(doorItem.handle_c);
          jQuery('#handled').val(doorItem.handle_d);

        }

        if(handleTypeVal=="ladder_pull_2_holes"){

          $('#handlea').show();
          $('#handleb').show();
          $('#handlec').show();
          $('#handled').hide();
          $('#handlee').hide();

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').show();
          $('#handled_label').hide();
          $('#handlee_label').hide();
          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
          jQuery('#handlec').val(doorItem.handle_c);

        }

        if(handleTypeVal =="single_pull_1_hole"){

          $('#handlea_label').show();
          $('#handleb_label').show();
          $('#handlec_label').hide();
          $('#handled_label').hide();
          $('#handlee_label').hide();

          $('#handlea').show();
          $('#handleb').show();
          $('#handlec').hide();
          $('#handled').hide();
          $('#handlee').hide();
          jQuery('#handlea').val(doorItem.handle_a);
          jQuery('#handleb').val(doorItem.handle_b);
        }
        break;
    }
  }
});
