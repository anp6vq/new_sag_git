var tgpd_current_item = -1;
var tgpd_items = [];
var tgpd_db_val =false;
var tgpd_editing = false;
var tgpd_editingsave = false;
var tgpd_copying = false;
//var back_date_entry = 'false';
var tgpd_copy_operation = -1;
var tgpd_clipboard = {};
var tgpd_pictures_download_url = [];
var tgpd_pictures_download_url_object = {};
var tgpd_sketches_download_url = [];
var maxChaulks=[];
//maxChaulks[0][0]=0;

var maxTapes=[];
var culkAdded=false;
var tapeAdded=false;
var tabGlassPieceCurrentForm=0;
var tab_glass_piece_form_number = 0;

var glassPieceHolesTypes=[];
var glassPieceHolesSizes=[];
var glasspieceHolesXlocations=[];
var glasspieceHolesYlocations=[];
var glasspieceHolesxlocationscls=[];
var glasspieceHolesylocationscls=[];
var glasspieceHolesNotes=[];
var glassHolesCounter=0;
var glassOffsetCounter=0;

	//addAndUpdateScaffolding();
	//addAndUpdateChannel();

var maxChannels=[];

var maxScaffoldings=[];

var maxScaffolding;	
var maxChannel;		


var maxHoles=[];
var maxHole;

maxHole=0;
//maxTapes[0][0]=0;

var maxChaulk;
maxChaulk=0;
var maxTape ;
maxTape=0;

maxScaffolding=0;
maxChannel=0;

var glassPieceHolesTypes;
glassPieceHolesTypes=0;

var glassPieceHolesSizes;
glassPieceHolesSizes=0;

var glasspieceHolesXlocations;
glasspieceHolesXlocations=0;

var glasspieceHolesYlocations;
glasspieceHolesYlocations=0;

var glasspieceHolesxlocationscls;
glasspieceHolesxlocationscls=0;

var glasspieceHolesylocationscls;
glasspieceHolesylocationscls=0;

var glasspieceHolesNotes;
glasspieceHolesNotes=0;


var tgpd_map_glass_set_value_to_fraction = {
   snap_beads_aluminium_interior : '+3/4',
   screw_beads_metal : '+3/4',
   snap_beads_vinyl_interior : '+3/4',
   snap_beads_aluminium_exterior : '+3/4',
   snap_beads_vinyl_exterior : '+3/4',
   snap_beads_wood_interior : '+3/4',
   snap_beads_wood_exterior : '+3/4',
   flush_1_over_4_top_exterior : '+5/8',
   flush_1_over_4_top_interior : '+5/8',
   flush_1_over_4_bottom_exterior : '+5/8',
   flush_1_over_4_bottom_interior : '+5/8',
   flush_1_over_4_left_right_exterior : '+5/8',
   flush_1_over_4_left_right_interior : '+5/8',
   flush_igu_top_exterior : '+7/8',
   flush_igu_top_interior : '+7/8',
   flush_igu_bottom_exterior : '+7/8',
   flush_igu_bottom_interior : '+7/8',
   flush_igu_left_right_exterior : '+7/8',
   flush_igu_left_right_interior : '+7/8',
   wrap_around_window_vinyl_single_pane: '+3/4',
   wrap_around_window_aluminum_single_pane: '+3/4',
   wrap_around_window_wood_single_pane: '+3/4',
   wrap_around_window_vinyl_igu: '+7/8',
   wrap_around_window_aluminum_igu: '+7/8',
   wrap_around_window_wood_igu: '+7/8',
   wrap_around_patio_door_single_pane: '+3/4',
   wrap_around_patio_door_igu: '+1',
   curtain_wall_all_sides_captured_exterior: '+1',
   curtain_wall_all_sides_captured_interior: '+1',
   curtain_wall_butt_glazed_left_only: '+3/2::1',
   curtain_wall_butt_glazed_right_only: '+3/2::1',
   curtain_wall_butt_glazed_left_right: '+2::1',
   curtain_wall_butt_glazed_top_only: '+1::3/2',
   curtain_wall_butt_glazed_bottom_only: '+1::3/2',
   curtain_wall_butt_glazed_top_bottom: '+1::2',
   structurally_glazed_all_sides_butt_glazed: '+2',
   zipper_wall : '+7/8',
   screw_beads: '+3/4', 
   u_channel_b_t: '0::5/4', 
   sash_and_division_bar: '+3/4', 
   sash_single_pane: '+5/8', 
   sagh_igu: '+7/8', 
   lug_sash: '+3/4', 
   hack_and_glaze_steel_interior: '+4', 
   hack_and_glaze_steel_exterior: '+4', 
   hack_and_glaze_wood_interior: '+4', 
   hack_and_glaze_wood_exterior: '+4', 
   table_top: '0', 
   stand_Off: '0',
   lay_in_canopy: '0',
   handrail: '', 
   vision_kit_all_new: '', 
   vision_kit_glass_only: '+3/4', 
   j_channel_b: '', 
   j_channel_t: '', 
   j_channel_t_b: '', 
   j_channel_t_b_l_r: '', 
   l_channel_b: '', 
   l_channel_t: '', 
   l_channel_t_b: '', 
   l_channel_t_b_l_r: '', 
   caulk_mastic_only: '',
   /*  wrap_around_vinyl : '+3/4',
   wrap_around_aluminium : '+3/4',
   wrap_around_wood : '+3/4',
   wrap_around_patio_door : '+3/4',
   curtain_wall_pressure_plate_all_sides_captured : '+1',
   curtain_wall_pressure_plate_butt_glazed_left_only : '',
   curtain_wall_pressure_plate_butt_glazed_right_only : '',
   curtain_wall_pressure_plate_butt_glazed_left_right : '',
   curtain_wall_pressure_plate_butt_glazed_top_only : '',
   curtain_wall_pressure_plate_butt_glazed_bottom_only : '',
   curtain_wall_pressure_plate_without_clips_all_sides_captured : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_left_only : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_right_only : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_left_right : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_top_only : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_bottom_only : '',
   structurally_glazed_all_sides_butt_glazed : '',
   j_channel_bottom : '',
   j_channel_top : '',
   j_channel_top_bottom : '',
   j_channel_top_bottom_left_right : '',
   l_channel_bottom : '',
   l_channel_top : '',
   l_channel_top_bottom : '',
   l_channel_top_bottom_left_right : '',
  caulk_mastic_only : '',
   hack_and_glaze_steel_interior : '',
   hack_and_glaze_steel_exterior : '',
   hack_and_glaze_wood_interior : '',
   hack_and_glaze_wood_exterior : '',
   vision_kit_all_new : '',
   vision_kit_glass_only : '',
   u_channel_top_bottom : '',
   sash_bar : '',
   division_bar : '',
   seamless_mullion_7_8 : '+7/8' */
};
var tgpd_picture_list = '<ul></ul>';
var tgpd_picture_list_item = '<li>' +
                                '<img src="#" alt="Glass Piece Photo">' +
                                '<span class="status"></span>' +
                                '<span class="info"></span>' +
                                '<div class="progressHolder">' +
                                   '<div class="progressBar"></div>' +
                                '</div>' +
                             '</li>';
var tgpd_sketch_list = '<ul></ul>';
var tgpd_sketch_list_item = '<li>' +
                                '<img src="#" alt="Glass Piece Sketch">' +
                                '<span class="status"></span>' +
                                '<span class="info"></span>' +
                                '<div class="progressHolder">' +
                                   '<div class="progressBar"></div>' +
                                '</div>' +
                             '</li>';

function tgpd_set_toolbar_navigation_item_count() {

   if (tgpd_editing) {
      jQuery('#text_tgpd_item_count').val('Add Item');
   } else {
      jQuery('#text_tgpd_item_count').val((tgpd_current_item + 1) + '/' + tgpd_items.length);
   }
}
function tgpd_init_form_state() {	
   jQuery('#fieldset_tgpd_glass_type').hide();
   jQuery('#div_tgpd_glass_color').hide();
   jQuery('#fieldset_tgpd_glass_pricing_sag_or_quote').hide();
   jQuery('#div_tgpd_picture_filelist_controls').hide();
   jQuery('#div_upg_picture_filelist_controls_notes').hide();
   jQuery('#canvas_tgpd_glass_sketch').sketch();
   jQuery('#canvas_usg_glass_sketch_notes').sketch();
   jQuery('#div_tgpd_canvas_holder').hide();
   jQuery('#div_usg_canvas_holder_notes').hide();

   //----temp or lami glass pieces hide-----

   jQuery('#lamidrophide').hide();
   jQuery('#extra_type_coating_treatment_second-non-igu').hide();
  

   jQuery('#div_tgpd_sketch_filelist_controls').hide();
   jQuery('#div_usg_sketch_filelist_controls_notes').hide();
	  /*sag-152*/
   //jQuery('#grids_and_fabrication').show();
   //jQuery('#fieldset_tgpd_glass_grids').hide();
   jQuery('#fieldset_tgpd_glass_fabrication').hide();
   jQuery('#fieldset_tgpd_glass_reminders_solar_film1').hide();
   jQuery('#fieldset_tgpd_glass_reminders_solar_film2').hide();
   jQuery('#fieldset_tgpd_glass_reminders_wet_seal').hide();
   jQuery('#fieldset_tgpd_glass_reminders_furniture_to_move').hide();
   jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1').hide();
   jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2').hide();
	if(back_date_entry ==true){
		tgpd_init_form_state_for_back_date_entry();
		$('#vtlt').show();		
	}
}

function tgpd_init_toolbar_navigation_buttons_state() {
   jQuery('#btn_tgpd_first').attr('disabled','disabled');
   jQuery('#btn_tgpd_previous').attr('disabled','disabled');
   jQuery('#text_tgpd_item_count').attr('readonly','readonly');
   jQuery('#btn_tgpd_next').attr('disabled','disabled');
   jQuery('#btn_tgpd_last').attr('disabled','disabled');
   tgpd_set_toolbar_navigation_item_count();
}

function tgpd_init_toolbar_editing_buttons_state() {
   jQuery('#btn_tgpd_add').removeAttr('disabled');
   jQuery('#btn_tgpd_save').attr('disabled','disabled');
   jQuery('#btn_tgpd_delete').attr('disabled','disabled');
   jQuery('#btn_tgpd_refresh').attr('disabled','disabled');
   tgpd_init_toolbar_copy_paste_buttons_state();
}

function tgpd_init_toolbar_copy_paste_buttons_state() {
   jQuery('#btn_tgpd_copy_specs_plus_size').attr('disabled','disabled');
   jQuery('#btn_tgpd_copy_specs').attr('disabled','disabled');
   jQuery('#btn_tgpd_paste').attr('disabled','disabled');
}

function tgpd_set_toolbar_navigation_buttons_state() {//alert('line 188');
   if (tgpd_current_item == -1 || tgpd_items.length == 0 || tgpd_editing) {
      tgpd_init_toolbar_navigation_buttons_state();
      return;
   }
   if (tgpd_items.length == 1) {//alert('line 193');
      jQuery('#btn_tgpd_first').attr('disabled','disabled');
      jQuery('#btn_tgpd_previous').attr('disabled','disabled');
      jQuery('#btn_tgpd_next').attr('disabled','disabled');
      jQuery('#btn_tgpd_last').attr('disabled','disabled');
   } else {
      if (tgpd_current_item == 0) {
         jQuery('#btn_tgpd_first').attr('disabled','disabled');
         jQuery('#btn_tgpd_previous').attr('disabled','disabled');
         jQuery('#btn_tgpd_next').removeAttr('disabled');
         jQuery('#btn_tgpd_last').removeAttr('disabled');
      } else if (tgpd_current_item == (tgpd_items.length - 1)) {
         jQuery('#btn_tgpd_first').removeAttr('disabled');
         jQuery('#btn_tgpd_previous').removeAttr('disabled');
         jQuery('#btn_tgpd_next').attr('disabled','disabled');
         jQuery('#btn_tgpd_last').attr('disabled','disabled');
      } else {
         jQuery('#btn_tgpd_first').removeAttr('disabled');
         jQuery('#btn_tgpd_previous').removeAttr('disabled');
         jQuery('#btn_tgpd_next').removeAttr('disabled');
         jQuery('#btn_tgpd_last').removeAttr('disabled');
      }
   }
   tgpd_set_toolbar_navigation_item_count();
}

function tgpd_set_toolbar_editing_buttons_state() {
   if ((!tgpd_editing) && (!tgpd_copying) && (!(tgpd_current_item >= 0 && tgpd_current_item < tgpd_items.length))) {
      tgpd_init_toolbar_editing_buttons_state();
      jQuery('#btn_submit_new_job').removeAttr('disabled');
      return;
   }
   if (tgpd_editing) {
     //lucky jQuery('#btn_tgpd_add').attr('disabled','disabled');
	  jQuery('#btn_tgpd_save').attr('disabled','disabled');
      jQuery('#btn_tgpd_save').removeAttr('disabled');
      jQuery('#btn_tgpd_delete').removeAttr('disabled');
      jQuery('#btn_tgpd_refresh').attr('disabled','disabled');
		  jQuery('#btn_submit_new_job').attr('disabled','disabled');
   } else {
	    glass_piece_enabled();
      jQuery('#btn_tgpd_add').removeAttr('disabled');
      if (tgpd_current_item >= 0 && tgpd_current_item < tgpd_items.length) {
         jQuery('#btn_tgpd_save').removeAttr('disabled');
         jQuery('#btn_tgpd_delete').removeAttr('disabled');
         jQuery('#btn_tgpd_refresh').removeAttr('disabled');
      }
      jQuery('#btn_submit_new_job').removeAttr('disabled');
   }
   tgpd_set_toolbar_copy_paste_buttons_state();
}

function tgpd_set_toolbar_copy_paste_buttons_state() {
   if (tgpd_editing) {
      jQuery('#btn_tgpd_copy_specs_plus_size').attr('disabled','disabled');
      jQuery('#btn_tgpd_copy_specs').attr('disabled','disabled');
      if (tgpd_copying) {
         jQuery('#btn_tgpd_paste').removeAttr('disabled');
      } else {
         jQuery('#btn_tgpd_paste').attr('disabled','disabled');
      }
   } else {
      if (tgpd_current_item >= 0 && tgpd_current_item < tgpd_items.length) {
         jQuery('#btn_tgpd_copy_specs_plus_size').removeAttr('disabled');
         jQuery('#btn_tgpd_copy_specs').removeAttr('disabled');
         if (tgpd_copying) {
            jQuery('#btn_tgpd_paste').removeAttr('disabled');
         } else {
            jQuery('#btn_tgpd_paste').attr('disabled','disabled');
         }
      }
   }
}

function tgpd_clear_form() { 
  // alert(2);
   jQuery('#text_tgpd_glass_location').val('');
   jQuery('#text_tgpd_coating_exterior_type').val('');
   jQuery('#text_tgpd_coating_interior_type').val('');
   jQuery('#text_tgpd_glass_quantity').val('1');
   jQuery('.text_tagd_shape_left_leg').val('');
   jQuery('.text_tagd_shape_width').val('');
   jQuery('.text_tagd_shape_right_leg').val('');
   jQuery('.text_tagd_shape_base').val('');
   jQuery('.text_tagd_shape_top').val('');
  // jQuery('#select_tgpd_servicetype').val('');
   jQuery('#select_tgpd_glass_type').val(['not_applicable']);
   jQuery('#select_tgpd_glass_damage_waiver').val(['not_applicable']);
   jQuery('#select_tgpd_glass_disclamers').val(['not_applicable']);
   jQuery('#text_tgpd_glass_damage_waiver_reminder_section').val('');
   jQuery('#text_tgpd_glass_disclamers_reminder_section').val('');
   jQuery('#select_tgpd_glass_shape').val(['Rectangular/Square']);
   jQuery('#select_tgpd_glass_shape').trigger("change");
   tgpd_set_glass_type_state('not_applicable', false);
   jQuery('#select_tgpd_glass_treatment').val(['not_applicable']);
   jQuery('#select_tgpd_glass_color').val(['not_applicable']);
   tgpd_set_glass_color_state('not_applicable', false);
   jQuery('#text_tgpd_glass_lift').val('');
   jQuery('#text_tgpd_glass_inside_lift_with_glass_type').val('');
   jQuery('#text_tgpd_glass_outside_lift_with_glass_type').val('');
   jQuery('#select_tgpd_glass_set').val(['not_applicable']);
   jQuery('#text_tgpd_daylight_width').val('');
   jQuery('#text_tgpd_coating_int').val('');
   jQuery('#text_tgpd_coating_exterior').val('');
   jQuery('#text_tgpd_daylight_height').val('');
   jQuery('#text_tgpd_go_width').val('');
   jQuery('#text_tgpd_go_height').val('');
   jQuery('#text_tgpd_extra_width').val('');
    jQuery('#text_tgpd_extra_height').val('');
    jQuery('#exterior_light_thickness').val('');
    jQuery('#interior_light_thickness').val('');
    jQuery('#select_tgpd_innner_layer_thickness').val('not_applicable');
	jQuery('#verify_tgpd_glass_treatment').prop('checked', false).removeClass('verify-active')
	jQuery('#verify_tgpd_glass_color').prop('checked', false).removeClass('verify-active')
	jQuery('#verify_tgpd_glass_lift').prop('checked', false).removeClass('verify-active')
   jQuery('#select_tgpd_size_verification_status').val(['not_applicable']);
   tgpd_set_size_verification_status_state('not_applicable');
   jQuery('#select_tgpd_overall_thickness').val(['not_applicable']);
   jQuery('#select_tgpd_exterior_glass_type').val(['not_applicable']);
   jQuery('#select_tgpd_treatment_exterior_glass').val(['not_applicable']);
   jQuery('#select_tgpd_treatment_interior_glass').val(['not_applicable']);  
   jQuery('#select_tgpd_interior_glass_type').val(['not_applicable']);
   jQuery('#select_tgpd_interior_glass_type_non_igu').val(['not_applicable']);
   jQuery('#select_tgpd_exterior_color_glass_type').val(['not_applicable']);
   jQuery('#select_tgpd_interior_color_glass_type').val(['not_applicable']);
   jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked', false);
   tgpd_set_glass_pricing_sag_or_quote_state(false, false);
   tgpd_clear_glass_picture_filelist();
   btn_upg_clear_picture_filelist_notes();
   tgpd_clear_glass_sketch_filelist({ clear_canvas:true, hide_canvas:true });
   tgpd_clear_glass_sketch_filelist_notes({ clear_canvas:true, hide_canvas:true });
   jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked', false);
   tgpd_set_glass_reminders_solar_film1_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked', false);
   tgpd_set_glass_reminders_wet_seal_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked', false);
  jQuery('#text_lift_inside_position').prop('checked', false);
   jQuery('#text_lift_outside_position').prop('checked', false);
   jQuery('#checkbox_tgpd_glass_reminders_add_disclamers').prop('checked', false);
   jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').prop('checked',false);
   tgpd_set_glass_reminders_furniture_to_move_state(false, false);
   tgpd_set_glass_reminders_disclamers_to_move_state(false, false);
   tgpd_set_glass_reminders_damage_waiver_to_move_state(false, false);
   tgpd_set_glass_reminders_damage_waiver_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked', false);
   tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_blind_needs_removing').prop('checked', false);
   jQuery('#checkbox_tgpd_glass_reminders_glass_fits_elevator').prop('checked', true);
   jQuery('#textarea_tgpd_instructions').val('');
   var tgpcf=	tgpd_current_item; //+tgpcf+'_'
   for(var i=0; i< maxChaulks[tgpcf]+1;i++){
   jQuery('#text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+i).val('');
    jQuery('#select_tgpd_glass_materials_caulk_type_'+tgpcf+'_'+i).val(['not_applicable']);
	 jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+i).val(['not_applicable']);
	 jQuery('#select_tgpd_glass_materials_quantity_'+tgpcf+'_'+i).val('0');
   }
   
   
     for(var i=0; i< maxScaffoldings[tgpcf]+1;i++){
 	 jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+i).val(['not_applicable']);
 	 jQuery('#select_tgpd_glass_materials_quantity_'+tgpcf+'_'+i).val('0');
   }
   
  
   
//   jQuery('#select_tgpd_glass_materials_caulk_type').val(['not_applicable']);
 //  jQuery('#select_tgpd_glass_materials_scaffolding_type').val(['not_applicable']);
	
   for(var i=0; i< maxTapes[tgpcf]+1;i++){
   jQuery('#text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+i).val('');
    jQuery('#select_tgpd_glass_materials_tape_type_'+tgpcf+'_'+i).val(['not_applicable']);
	
   }
   	/*
	maxChannels=[];var maxScaffoldings=[];var maxScaffolding;	var maxChannel;		
	*/
	 for(var i=0; i< maxChannels[tgpcf]+1;i++){
		jQuery('#text_tgpd_glass_materials_channel_'+tgpcf+'_'+i).val('');
   }
	
	 for(var i=0; i< glassPieceHolesTypes[tgpcf];i++){
		jQuery('#glass_piece_holes_type_text_'+i).val('');
	} 
	
   //jQuery('#text_tgpd_glass_materials_tape_amount').val('');
   //jQuery('#select_tgpd_glass_materials_tape_type').val(['not_applicable']);
   //jQuery('#text_tgpd_glass_materials_channel').val('');
   jQuery('#text_tgpd_miscellaneous').val('');
   jQuery('#checkbox_tgpd_glass_fabrication').prop('checked', false);
   jQuery('#verify_tgpd_overall_thickness').prop('checked', false);
   jQuery('#verify_tgpd_exterior_glass_type').prop('checked', false);
   jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked', false);
   jQuery('#verify_tgpd_exterior_color_glass_type').prop('checked', false);
   jQuery('#verify_tgpd_coating_exterior_type').prop('checked', false);
   jQuery('#verify_tgpd_spacer_color').prop('checked', false);
   jQuery('#verify_tgpd_interior_glass_type').prop('checked', false);
   jQuery('#verify_tgpd_treatment_interior_glass').prop('checked', false);
   jQuery('#verify_tgpd_interior_color_glass_type').prop('checked', false);
   jQuery('#verify_tgpd_coating_interior_type').prop('checked', false);
   jQuery('#verify_tgpd_glass_set').prop('checked', false);
   jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').prop('checked', false);
   jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').prop('checked', false);
   
   jQuery('#text_tgpd_coating_exterior_type').val('#2');
   jQuery('#text_tgpd_coating_interior_type').val('#2');
   jQuery('#verify_tgpd_spacer_color').prop('checked',true).addClass('verify-active');;
   jQuery('#verify_tgpd_interior_lite_thickness').prop('checked', false);
   jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked', false);
   jQuery('#verify_tgpd_inner_layer_thickness').prop('checked', false);
   jQuery('#verify_tgpd_glass_treatment').prop('checked', false);
   jQuery('#verify_tgpd_glass_color').prop('checked', false);
   jQuery('#verify_tgpd_glass_lift').prop('checked', false);
   
   tgpd_set_glass_fabrication_state(false, false);

	var tgpd_glass_fabrication = jQuery('#fieldset_tgpd_glass_fabrication');
	var tgpd_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges');
	var tgpd_fabrication_holes = jQuery('#text_tgpd_fabrication_holes');
	var tgpd_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern');

	tgpd_fabrication_polished_edges.val('');
	tgpd_fabrication_holes.val('');
	tgpd_fabrication_pattern.val('');

	var tgpd_glass_grids = jQuery('#fieldset_tgpd_glass_grids');
	var tgpd_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids');
	var tgpd_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids');
	var tgpd_grids_thickness = jQuery('#select_tgpd_grids_thickness');
	var tgpd_grids_color = jQuery('#text_tgpd_grids_color');
 /***158***/
   var tgpd_grids_color_custom = jQuery('#text_tgpd_grids_color_custom');
   /***158***/
	tgpd_pattern_horizontal_grids.val('');
	tgpd_pattern_vertical_grids.val('');
	tgpd_grids_thickness.val(['not_applicable']);
	tgpd_grids_color.val('');
tgpd_grids_color_custom.val('');
		
	/***************************REMOVE ALL CHECKBOXES UNCHECK *************************************/
	jQuery('#verify_tgpd_overall_thickness').prop('checked',false).removeClass('verify-active');;
	jQuery('#verify_tgpd_exterior_glass_type').prop('checked',false).removeClass('verify-active');;
	jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked',false).removeClass('verify-active');;
	jQuery('#verify_tgpd_exterior_color_glass_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_coating_exterior_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_interior_glass_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_treatment_interior_glass').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_interior_color_glass_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_coating_interior_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_glass_set').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_daylight_width').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_daylight_height').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_go_size_width').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_go_size_height').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_set_height').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_set_width').prop('checked', false).removeClass('verify-active');;
	jQuery('#text_lift_inside_position').prop('checked', false).removeClass('verify-active');;
	jQuery('#lift_inside_hide_on_glass_type').show();
	jQuery('#text_lift_outside_position').prop('checked', false).removeClass('verify-active');;
	jQuery('#text_tgpd_fabrication_polished_edges_hide').prop('checked', false).removeClass('verify-active');;
	jQuery('#text_tgpd_fabrication_pattern_hide').prop('checked', false).removeClass('verify-active');;
	jQuery('#text_tgpd_fabrication_holes_hide_new').prop('checked', false).removeClass('verify-active');;
	jQuery('#text_tgpd_fabrication_holes_hide_old').prop('checked', false).removeClass('verify-active');;
	jQuery('#lift_outside_hide_on_glass_type').show();
	
	/**********************************************************************/
	jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_interior_lite_thickness').prop('checked', false).removeClass('verify-active');;
	jQuery('#verify_tgpd_inner_layer_thickness').prop('checked', false).removeClass('verify-active');;
	
	}
	

function tgpd_set_glass_type_state(tgpd_glass_type, tgpd_focus) {
   //alert(1);
   var tgpd_glass_type_fields = jQuery('#fieldset_tgpd_glass_type');
   var tgpd_lite_thickness = jQuery('#select_tgpd_lite_thickness');
   var tgpd_spacer_color = jQuery('#select_tgpd_spacer_color');
   var tgpd_grids_and_fabrication = jQuery('#grids_and_fabrication');
   var tgpd_glass_grids = jQuery('#checkbox_tgpd_glass_grids');
   
   console.log(tgpd_glass_type + " tester ");
   
   if (typeof tgpd_glass_type!='undefined' && tgpd_glass_type.indexOf('igu') > -1  || tgpd_glass_type.indexOf('IGU') > -1 ) {
      
        /*sag-155*/
      $('#select_tgpd_exterior_coating_family_color_glass_type').val('not_applicable');
      $('#select_tgpd_exterior_coating_family_glass_type').val('-');
      $('#select_tgpd_interior_oating_family_color_glass_type').val('not_applicable');
      $('#select_tgpd_interior_coating_family_glass_type').val('-');
      $('#verify_tgpd_exterior_coating_family_color_glass_type').removeClass('verify-active');
      $('#verify_tgpd_exterior_coating_family_color_glass_type').attr('checked',false);
      $('#verify_tgpd_exterior_coating_family_glass_type').removeClass('verify-active');
      $('#verify_tgpd_exterior_coating_family_glass_type').attr('checked',false);


      $('#verify_tgpd_interior_coating_family_color_glass_type').removeClass('verify-active');
      $('#verify_tgpd_interior_coating_family_color_glass_type').attr('checked',false);
      $('#verify_tgpd_interior_coating_family_glass_type').removeClass('verify-active');
      $('#verify_tgpd_interior_coating_family_glass_type').attr('checked',false);
       /*sag-155*/
      tgpd_glass_type_fields.show();
     // tgpd_grids_and_fabrication.show();
      if (tgpd_focus) {
         tgpd_lite_thickness[0].focus();
      }
       $('.grid_sec').show();
      $('#fieldset_tgpd_glass_grids').show();
      $('#grids_and_fabrication').hide();
      $('#glass_piece_offset_lbl').show();
      $('.off_loc_sec').show();
   } else {
       $('#glass_piece_offset_lbl').hide();
      $('.off_loc_sec').hide();
      tgpd_lite_thickness.val(['not_applicable']);
      tgpd_spacer_color.val(['not_applicable']);
      tgpd_glass_type_fields.hide();
  $('.grid_sec').hide();
  
      tgpd_set_glass_grids_state(false, false);
      tgpd_glass_grids.prop('checked', false);
       }
//tgpd_grids_and_fabrication.hide();
//jQuery('#verify_tgpd_interior_coating_family_color_glass_type ').prop('checked',true).addClass('verify-active'); 
     //jQuery('#verify_tgpd_interior_glass_type').prop('checked',true).addClass('verify-active');
     //jQuery('#verify_tgpd_treatment_interior_glass').prop('checked',true).addClass('verify-active');
      //jQuery('#verify_tgpd_interior_coating_family_glass_type').prop('checked',true).addClass('verify-active');
      
     //jQuery('#verify_tgpd_coating_interior_type').prop('checked',true).addClass('verify-active');

     //jQuery('#verify_tgpd_interior_color_glass_type').prop('checked',true).addClass('verify-active');
       if(tgpd_glass_type == 'IGU (VIRACON)') {

        
         jQuery('.non_vari_col').hide();
         jQuery('.vari_col').show();
         jQuery('#fieldset_tgpd_glass_type').show();
      } else {

          jQuery('.non_vari_col').show();
         jQuery('.vari_col').hide();
      }
}

function tgpd_temp_lami_state(tgpd_glass_type){
  console.log("---inside tgpd_temp_lami_state")
  if(tgpd_glass_type  == "igu_sky_lite_temp_or_lami" || tgpd_glass_type  == "igu_lami_temp_lami"){ //requires extra thickness fields
    jQuery('#lamidrophide').hide();

  } else{
    console.log("not sky lite tempLami and not IGU Temp Lami")
  }
}

function tgpd_set_glass_color_state(tgpd_glass_color, tgpd_focus) {
   var tgpd_glass_color_fields = jQuery('#div_tgpd_glass_color');
   var tgpd_glass_color_note = jQuery('#text_tgpd_glass_color_note');
   if (tgpd_glass_color.localeCompare('custom') == 0) {
      tgpd_glass_color_fields.show();
      if (tgpd_focus) {
         tgpd_glass_color_note[0].focus();
      }
   } else {
      tgpd_glass_color_note.val('');
      tgpd_glass_color_fields.hide();
   }
}

function tgpd_set_glass_grids_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_grids = jQuery('#fieldset_tgpd_glass_grids');
   var tgpd_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids');
   var tgpd_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids');
   var tgpd_grids_thickness = jQuery('#select_tgpd_grids_thickness');
   var tgpd_grids_color = jQuery('#text_tgpd_grids_color');
   if (tgpd_checked) {
      tgpd_glass_grids.show();
      if (tgpd_focus) {
         tgpd_pattern_horizontal_grids[0].focus();
      }
   } else {
     // tgpd_pattern_horizontal_grids.val('');
      //tgpd_pattern_vertical_grids.val('');
     // tgpd_grids_thickness.val(['not_applicable']);
     // tgpd_grids_color.val('');
      tgpd_glass_grids.hide();
   }
}

function tgpd_set_glass_fabrication_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_fabrication = jQuery('#fieldset_tgpd_glass_fabrication');
   var tgpd_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges');
   var tgpd_fabrication_holes = jQuery('#text_tgpd_fabrication_holes');
   var tgpd_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern');
   /*sag-152*/
   jQuery('#fieldset_tgpd_glass_grids').show();
   jQuery('#grids_and_fabrication').hide();
   if (tgpd_checked) {
      tgpd_glass_fabrication.show();
      if (tgpd_focus) {
         tgpd_fabrication_polished_edges[0].focus();
      }
   } else {
      //tgpd_fabrication_polished_edges.val('');
     // tgpd_fabrication_holes.val('');
     // tgpd_fabrication_pattern.val('');
      tgpd_glass_fabrication.hide();
   }
}

function is_valid_fraction(value) {
   var is_valid = true;
   try {
      var f = new Fraction(value);
   } catch (e) {
      is_valid = false;
   }
   return is_valid;
}

function set_text_tgpd_go_width(fromSetDropdown) {
   
   var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
        
   var text_tgpd_daylight_width = jQuery('#text_tgpd_daylight_width');
    var text_tgpd_coating_int = jQuery('#text_tgpd_coating_int');
   var text_tgpd_coating_interior = jQuery('#text_tgpd_coating_interior');
   var text_tgpd_coating_exterior = jQuery('#text_tgpd_coating_exterior');
     var verify_go_size_width = jQuery('#verify_go_size_width');
   var tgpd_daylight_width = text_tgpd_daylight_width[0].checkValidity() ? text_tgpd_daylight_width.val() : '';
   //var tgpd_coating_int = text_tgpd_coating_int[0].checkValidity() ? text_tgpd_coating_int.val() : '';
   var text_tgpd_go_width = jQuery('#text_tgpd_go_width');

   
   text_tgpd_go_width.val('');verify_go_size_width.prop("checked",false);
   console.log('set_text_tgpd_go_width: tgpd_glass_set => ' + tgpd_glass_set + ', tgpd_daylight_width => ' + tgpd_daylight_width + ', text_tgpd_daylight_width.checkValidity() => ' + text_tgpd_daylight_width[0].checkValidity());

   if ((!(tgpd_glass_set.localeCompare('not_applicable')==0)) && (!(tgpd_daylight_width.localeCompare('')==0)) && is_valid_fraction(tgpd_daylight_width)) {
     
     var tgpd_glass_set_fraction;
    
        // console.log('testing on sag line number658>><<'+tgpd_glass_set_fraction);
     if(fromSetDropdown === true){
       console.log("fromSetDropdown === true");
       tgpd_glass_set_fraction = tgpd_map_glass_set_value_to_fraction[tgpd_glass_set];
     } else{
       
       console.log("fromSetDropdown === true");
       tgpd_glass_set_fraction = jQuery('#text_tgpd_extra_width option:selected').val();
       //from manual set width formula
     }

      console.log('set_text_tgpd_go_width: tgpd_glass_set_fraction => ' + tgpd_glass_set_fraction + ', is_valid_fraction(tgpd_glass_set_fraction) => ' + is_valid_fraction(tgpd_glass_set_fraction));
      
        if(tgpd_glass_set_fraction.indexOf('::') != -1){
			var res = tgpd_glass_set_fraction.split("::");
			tgpd_glass_set_fraction = res[0];
			if(tgpd_glass_set_fraction!=0){
				if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
			}
			var f = new Fraction(tgpd_glass_set_fraction);
		}else{
			if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
			var f = new Fraction(tgpd_glass_set_fraction);
		}
      //if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
      //var f = new Fraction(tgpd_glass_set_fraction);
      
      console.log('set_text_tgpd_go_width: tgpd_glass_set_fraction => ' + tgpd_glass_set_fraction + ', f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      f = f.add(tgpd_daylight_width);
      console.log('set_text_tgpd_go_width: f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      text_tgpd_go_width.val(f.toFraction(true));
      
      verify_go_size_width.prop("checked",true);
   }
}

function set_text_tgpd_go_height(fromSetDropdown) {
	
   var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
   var text_tgpd_daylight_height = jQuery('#text_tgpd_daylight_height');
   
     var verify_go_size_height = jQuery('#verify_go_size_height');
     
   var tgpd_daylight_height = text_tgpd_daylight_height[0].checkValidity() ? text_tgpd_daylight_height.val() : '';
   var text_tgpd_go_height = jQuery('#text_tgpd_go_height');
   text_tgpd_go_height.val('');verify_go_size_height.prop("checked",false);
   console.log('set_text_tgpd_go_height: tgpd_glass_set => ' + tgpd_glass_set + ', tgpd_daylight_height => ' + tgpd_daylight_height + ', text_tgpd_daylight_height.checkValidity() => ' + text_tgpd_daylight_height[0].checkValidity());
   if ((!(tgpd_glass_set.localeCompare('not_applicable')==0)) && (!(tgpd_daylight_height.localeCompare('')==0)) && is_valid_fraction(tgpd_daylight_height)) {
     var tgpd_glass_set_fraction;
     if(fromSetDropdown === true){
       tgpd_glass_set_fraction = tgpd_map_glass_set_value_to_fraction[tgpd_glass_set];
     } else{ // from manual set height formula
       tgpd_glass_set_fraction = jQuery('#text_tgpd_extra_height option:selected').val();
     }

        if(tgpd_glass_set_fraction.indexOf('::') != -1){
			var res = tgpd_glass_set_fraction.split("::");
			tgpd_glass_set_fraction = res[1];
			if(tgpd_glass_set_fraction!=0){
				if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
			}
			var f = new Fraction(tgpd_glass_set_fraction);
		}else{
			if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
			var f = new Fraction(tgpd_glass_set_fraction);
		}
		
      //if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
      //var f = new Fraction(tgpd_glass_set_fraction);
      
      console.log('set_text_tgpd_go_height: tgpd_glass_set_fraction => ' + tgpd_glass_set_fraction + ', f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      f = f.add(tgpd_daylight_height);
      console.log('set_text_tgpd_go_height: f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      text_tgpd_go_height.val(f.toFraction(true));
      verify_go_size_height.prop("checked",true);
   }
}

function fillSetFormula(){
  var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
  if ((!(tgpd_glass_set.localeCompare('not_applicable')==0)) ) {
    var tgpd_glass_set_fraction = tgpd_map_glass_set_value_to_fraction[tgpd_glass_set];
    
      var res = tgpd_glass_set_fraction.split("::");
    if(tgpd_glass_set_fraction.indexOf('::') != -1){
            
		
        //alert('test: '+res);
        tgpd_glass_set_fraction_w = res[0];
        tgpd_glass_set_fraction_h = res[1];
        //alert("w : "+tgpd_glass_set_fraction_w+"   h : "+tgpd_glass_set_fraction_h);

        if(tgpd_glass_set_fraction_w!=0){
            if (!is_valid_fraction(tgpd_glass_set_fraction_w)) return;
        }
        if(tgpd_glass_set_fraction_h!=0){
            if (!is_valid_fraction(tgpd_glass_set_fraction_h)) return;
        }
        var f_h = new Fraction(tgpd_glass_set_fraction_w);
        var f_w = new Fraction(tgpd_glass_set_fraction_h);
        jQuery('#text_tgpd_extra_width').val(f_h.toFraction(true));
        jQuery('#text_tgpd_extra_height').val(f_w.toFraction(true));
    }
    else{
        if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
        var f = new Fraction(tgpd_glass_set_fraction);
        jQuery('#text_tgpd_extra_width').val(f.toFraction(true));
        jQuery('#text_tgpd_extra_height').val(f.toFraction(true));
        
    }
    
     /*if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
     var f = new Fraction(tgpd_glass_set_fraction);
     jQuery('#text_tgpd_extra_width').val(f.toFraction(true));
     jQuery('#text_tgpd_extra_height').val(f.toFraction(true));*/
  }
}

function tgpd_set_size_verification_status_state(tgpd_size_verification_status) {
   var $tgpd_go_width = jQuery('#text_tgpd_go_width');
   var $tgpd_go_height = jQuery('#text_tgpd_go_height');
   /* switch (tgpd_size_verification_status) {
      case "verified":
         $tgpd_go_width.css({'background-color': '#64B058', 'color': 'white'});
         $tgpd_go_height.css({'background-color': '#64B058', 'color': 'white'});
         break;
      case "verify_on_order":
        $tgpd_go_width.css({'background-color': '#F0B94B', 'color': 'white'});
        $tgpd_go_height.css({'background-color': '#F0B94B', 'color': 'white'});
        break;
      case "block_size":
         $tgpd_go_width.css({'background-color': '#87cefa', 'color': 'white'});
         $tgpd_go_height.css({'background-color': '#87cefa', 'color': 'white'});
         break;
      default:
         $tgpd_go_width.css('background-color', '#ffffff');
         $tgpd_go_height.css('background-color', '#ffffff');
   } */
}

function tgpd_set_glass_pricing_sag_or_quote_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_pricing_sag_or_quote = jQuery('#fieldset_tgpd_glass_pricing_sag_or_quote');
   var tgpd_glass_pricing_sag_or_quote_manufacturer = jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer');
   if (tgpd_checked) {
      tgpd_glass_pricing_sag_or_quote.show();
      if (tgpd_focus) {
         tgpd_glass_pricing_sag_or_quote_manufacturer[0].focus();
      }
   } else {
      tgpd_glass_pricing_sag_or_quote_manufacturer.val('');
      tgpd_glass_pricing_sag_or_quote.hide();
   }
}

function tgpd_set_glass_reminders_solar_film1_state(tgpd_checked, tgpd_focus) {
	
  	if(tgpd_checked === 'undefined'){
		return;
	}

   var tgpd_glass_reminders_solar_film1 = jQuery('#fieldset_tgpd_glass_reminders_solar_film1');
   var tgpd_glass_reminders_solar_film_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]');
   var tgpd_glass_treatment = jQuery('#select_tgpd_glass_treatment');
   if (tgpd_checked) {
      tgpd_glass_reminders_solar_film1.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_solar_film_responsibility[0].focus();
      }
      tgpd_set_glass_reminders_solar_film2_state(tgpd_glass_reminders_solar_film_responsibility.val(), true);
      //tgpd_glass_treatment.val(['tempered']);
      //tgpd_glass_treatment.prop('disabled', true);
   } else {
      // tgpd_glass_reminders_solar_film_responsibility.val(['sag']);
      tgpd_glass_reminders_solar_film_responsibility.val(['']);
      tgpd_set_glass_reminders_solar_film2_state('', false);
      tgpd_glass_reminders_solar_film1.hide();
      tgpd_glass_treatment.prop('disabled', false);
      // tgpd_glass_treatment.val(['not_applicable']);
   }
}

function tgpd_set_glass_reminders_solar_film2_state(tgpd_solar_film_responsibility, tgpd_focus) {
   var tgpd_glass_reminders_solar_film2 = jQuery('#fieldset_tgpd_glass_reminders_solar_film2');
   var tgpd_glass_reminders_solar_film_type = jQuery('#text_tgpd_glass_reminders_solar_film_type');
   var tgpd_glass_reminders_solar_film_source = jQuery('#text_tgpd_glass_reminders_solar_film_source');
   
   
   if ( typeof tgpd_solar_film_responsibility!='undefined' ){
   if (  tgpd_solar_film_responsibility.localeCompare('sag') == 0) {
      tgpd_glass_reminders_solar_film2.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_solar_film_type[0].focus();
      }
   } else {
      tgpd_glass_reminders_solar_film_type.val('');
      tgpd_glass_reminders_solar_film_source.val('');
      tgpd_glass_reminders_solar_film2.hide();
   }
} 
}

function tgpd_set_glass_reminders_wet_seal_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_reminders_wet_seal = jQuery('#fieldset_tgpd_glass_reminders_wet_seal');
   var tgpd_glass_reminders_wet_seal_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]');
   if (tgpd_checked) {
      tgpd_glass_reminders_wet_seal.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_wet_seal_responsibility[0].focus();
      }
   } else {
      // tgpd_glass_reminders_wet_seal_responsibility.val(['sag']);
      tgpd_glass_reminders_wet_seal_responsibility.val(['']);
      tgpd_glass_reminders_wet_seal.hide();
   }
}

function tgpd_set_glass_reminders_furniture_to_move_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_reminders_furniture_to_move = jQuery('#fieldset_tgpd_glass_reminders_furniture_to_move');
   var tgpd_glass_reminders_furniture_to_move_comment = jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment');
   if (tgpd_checked) {
      tgpd_glass_reminders_furniture_to_move.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_furniture_to_move_comment[0].focus();
      }
   } else {
      tgpd_glass_reminders_furniture_to_move_comment.val('');
      tgpd_glass_reminders_furniture_to_move.hide();
   }
}

/******************************************************************************************************************************/
function tgpd_set_lift_inside_glass_reminders(tgpd_checked) {
   //var tgpd_glass_reminders_disclamers_to_move = jQuery('#fieldset_tgpd_glass_lift_inside_section1');
   console.log('here_lift_inside');
   var liftinside_remainder = jQuery('#fieldset_tgpd_glass_lift_inside_section1');
      if (tgpd_checked) {
         liftinside_remainder.show();
         } else {
         jQuery('#text_tgpd_glass_inside_lift_with_glass_type').val('');   
         liftinside_remainder.hide();
         }
}


function tgpd_set_lift_outside_glass_reminders(tgpd_checked,tgpd_focus) {
   //var tgpd_glass_reminders_disclamers_to_move = jQuery('#fieldset_tgpd_glass_lift_inside_section1');
   var liftoutside_remainder = jQuery('#fieldset_tgpd_glass_lift_outside');
     if (tgpd_checked) {
         liftoutside_remainder.show();
        } else {
         jQuery('#text_tgpd_glass_outside_lift_with_glass_type').val('');  
         liftoutside_remainder.hide();
        }
   
}

/***********************************************************************************************************************************/

function tgpd_set_glass_reminders_disclamers_to_move_state(tgpd_checked) {
   var tgpd_glass_reminders_disclamers_to_move = jQuery('#fieldset_tgpd_glass_disclamers_reminder_section');
   var tgpd_glass_reminders_disclamers_to_move_select = jQuery('#fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers');
   
   if (tgpd_checked) {
      tgpd_glass_reminders_disclamers_to_move.show();
      tgpd_glass_reminders_disclamers_to_move_select.show();
     
   } else {
     
      tgpd_glass_reminders_disclamers_to_move.hide();
      tgpd_glass_reminders_disclamers_to_move_select.hide();
   }
}


function tgpd_set_glass_reminders_damage_waiver_to_move_state(tgpd_checked) {
   var tgpd_glass_reminders_damage_waiver_to_move = jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section_for_select');
   var tgpd_glass_reminders_damage_waiver_to_move_select = jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section');
   
   if (tgpd_checked) {
	 
      tgpd_glass_reminders_damage_waiver_to_move.show();
      tgpd_glass_reminders_damage_waiver_to_move_select.show();
     
   } else {
		
      tgpd_glass_reminders_damage_waiver_to_move.hide();
      tgpd_glass_reminders_damage_waiver_to_move_select.hide();
   }
}


function tgpd_set_glass_reminders_damage_waiver_state(tgpd_checked){
	var tgpd_glass_remindes_damage_waiver_to_move = jQuery('#select_tgpd_glass_damage_waiver');
	if(tgpd_checked){
		//tgpd_glass_remindes_damage_waiver_to_move.show();
	}else{
		//tgpd_glass_remindes_damage_waiver_to_move.hide();
	}
}

function tgpd_set_glass_reminders_disclamers_state(tgpd_checked){
	var tgpd_glass_remindes_disclamers_to_move = jQuery('#select_tgpd_glass_disclamers');
	if(tgpd_checked){
		//tgpd_glass_remindes_disclamers_to_move.show();
	}else{
		//tgpd_glass_remindes_disclamers_to_move.hide();
	}
}

function tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_reminders_walls_or_ceilings_to_cut1 = jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1');
   var tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]');
   if (tgpd_checked) {
      tgpd_glass_reminders_walls_or_ceilings_to_cut1.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility[0].focus();
      }
      tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.val(), true);
   } else {
      // tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.val(['sag']);
      tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.val(['']);
      tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state('', true);
      tgpd_glass_reminders_walls_or_ceilings_to_cut1.hide();
   }
}

function tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_walls_or_ceilings_to_cut_responsibility, tgpd_focus) {
   var tgpd_glass_reminders_walls_or_ceilings_to_cut2 = jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2');
   var tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_comment = jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment');
  
if ( typeof tgpd_walls_or_ceilings_to_cut_responsibility!='undefined' ){
  if (tgpd_walls_or_ceilings_to_cut_responsibility.localeCompare('sag') == 0 || tgpd_walls_or_ceilings_to_cut_responsibility.localeCompare('customer') == 0) {
      tgpd_glass_reminders_walls_or_ceilings_to_cut2.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_comment[0].focus();
      }
   } else {
      tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_comment.val('');
      tgpd_glass_reminders_walls_or_ceilings_to_cut2.hide();
   }
}
}

function tgpd_show_current_item() {//alert('line 760');
   tgpd_clear_form();

   var tgpd_item = tgpd_items[tgpd_current_item];

   tgpd_print_item_to_form(tgpd_item);

}

function tgpd_get_item_from_form() {	
   	var tgpd_glass_location = jQuery('#text_tgpd_glass_location').val();
   	var tgpd_coating_exterior_type = jQuery('#text_tgpd_coating_exterior_type').val();
   	var tgpd_coating_interior_type = jQuery('#text_tgpd_coating_interior_type').val();
      /*sag 124**/

      var tgpd_coating_family_exterior_type = jQuery('#select_tgpd_exterior_coating_family_glass_type').val();
      var tgpd_coating_family_interior_type = jQuery('#select_tgpd_interior_coating_family_glass_type').val();
      var tgpd_coating_family_color_exterior_type = jQuery('#select_tgpd_exterior_coating_family_color_glass_type').val();
      var tgpd_coating_family_color_interior_type = jQuery('#select_tgpd_interior_oating_family_color_glass_type').val();
      /*sag 124**/ 
   	var tgpd_glass_quantity = jQuery('#text_tgpd_glass_quantity').val();

	/*verify code start from here data 02 Nov, 2017*/
	var verify_tgpd_overall_thickness = jQuery('#verify_tgpd_overall_thickness').prop('checked') ? 1 : 0;
	var verify_tgpd_exterior_glass_type = jQuery('#verify_tgpd_exterior_glass_type').prop('checked') ? 1 : 0;
	var verify_tgpd_interior_glass_type = jQuery('#verify_tgpd_interior_glass_type').prop('checked') ? 1 : 0;
	var verify_tgpd_treatment_exterior_glass = jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked') ? 1 : 0;
	var verify_tgpd_treatment_interior_glass = jQuery('#verify_tgpd_treatment_interior_glass').prop('checked') ? 1 : 0;
	var verify_tgpd_exterior_color_glass_type = jQuery('#verify_tgpd_exterior_color_glass_type').prop('checked') ? 1 : 0;
	var verify_tgpd_interior_color_glass_type = jQuery('#verify_tgpd_interior_color_glass_type').prop('checked') ? 1 : 0;
	var verify_tgpd_coating_exterior_type = jQuery('#verify_tgpd_coating_exterior_type').prop('checked') ? 1 : 0;
	var verify_tgpd_coating_interior_type = jQuery('#verify_tgpd_coating_interior_type').prop('checked') ? 1 : 0;
    /***sag 124***/
   var verify_tgpd_exterior_coating_family_glass_type = jQuery('#verify_tgpd_exterior_coating_family_glass_type').prop('checked') ? 1 : 0;
   var verify_tgpd_interior_coating_family_glass_type = jQuery('#verify_tgpd_interior_coating_family_glass_type').prop('checked') ? 1 : 0;

   var verify_tgpd_exterior_coating_family_color_glass_type = jQuery('#verify_tgpd_exterior_coating_family_glass_type').prop('checked') ? 1 : 0;
   var verify_tgpd_interior_coating_family_color_glass_type = jQuery('#verify_tgpd_interior_coating_family_glass_type').prop('checked') ? 1 : 0;

   
   /*****sag 124***/
	var verify_tgpd_exterior_lite_thickness   =jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked') ? 1 : 0;
	var verify_tgpd_interior_lite_thickness   = jQuery('#verify_tgpd_interior_lite_thickness').prop('checked') ? 1 : 0;
	var verify_tgpd_inner_layer_thickness   = jQuery('#verify_tgpd_inner_layer_thickness').prop('checked') ? 1 : 0;
	var verify_tgpd_lite_thickness  = jQuery('#verify_tgpd_lite_thickness').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_treatment = jQuery('#verify_tgpd_glass_treatment').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_color  = jQuery('#verify_tgpd_glass_color').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_color_note = jQuery('#verify_tgpd_glass_color_note').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_set = jQuery('#verify_tgpd_glass_set').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_lift = jQuery('#verify_tgpd_glass_lift').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_inside_lift_with_glass_type = jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').prop('checked') ? 1 : 0;
	var text_tgpd_text_lift_inside_position = jQuery("#text_lift_inside_position").prop( "checked")? 1 : 0;
	var text_tgpd_text_lift_outside_position = jQuery("#text_lift_outside_position").prop( "checked")? 1 : 0;
	var text_tgpd_fabrication_polished_edges_hide = jQuery("#text_tgpd_fabrication_polished_edges_hide").prop( "checked")? 1 : 0;
	var text_tgpd_fabrication_pattern_hide = jQuery("#text_tgpd_fabrication_pattern_hide").prop( "checked")? 1 : 0;
    /*sag-152*/
   var text_tgpd_fabrication_gride_hide = jQuery("#text_tgpd_fabrication_gride_hide").prop( "checked")? 1 : 0; 
	var text_tgpd_fabrication_holes_hide_new = jQuery("#text_tgpd_fabrication_holes_hide_new").prop( "checked")? 1 : 0;
	var text_tgpd_fabrication_holes_hide_old = jQuery("#text_tgpd_fabrication_holes_hide_old").prop( "checked")? 1 : 0;
	var verify_tgpd_glass_outside_lift_with_glass_type = jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').prop('checked') ? 1 : 0;
	var verify_tgpd_spacer_color = jQuery('#verify_tgpd_spacer_color').prop('checked') ? 1 : 0;
	var verify_tgpd_daylight_height = jQuery('#verify_tgpd_daylight_height').prop('checked') ? 1 : 0;
	var verify_tgpd_daylight_width = jQuery('#verify_tgpd_daylight_width').prop('checked') ? 1 : 0;
	var verify_set_height = jQuery('#verify_set_height').prop('checked') ? 1 : 0;
	var verify_set_width = jQuery('#verify_set_width').prop('checked') ? 1 : 0;




	var verify_go_size_height = jQuery('#verify_go_size_height').prop('checked') ? 1 : 0;
	var verify_go_size_width = jQuery('#verify_go_size_width').prop('checked') ? 1 : 0;

	var text_tgpd_extra_width = jQuery('#text_tgpd_extra_width').val();
	var text_tgpd_extra_height = jQuery('#text_tgpd_extra_height').val();
	/*verify code end at here data 02 Nov, 2017*/

	var tgpd_glass_servicetype = jQuery('#select_tgpd_servicetype').val();
   	var tgpd_glass_type = jQuery('#select_tgpd_glass_type').val();
   	var tgpd_glass_shape = jQuery('#select_tgpd_glass_shape').val();

   if (typeof tgpd_glass_shape === 'undefined' || tgpd_glass_shape == null) tgpd_glass_shape = 'not_applicable';
   if (tgpd_glass_shape.localeCompare('not_applicable') != 0 && tgpd_glass_shape != 'Rectangular/Square' ) {
	   if(jQuery('#text_tagd_shape_left_leg_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_left_leg = jQuery('#text_tagd_shape_left_leg_'+tgpd_glass_shape).val();
	   }
	   if(jQuery('#text_tagd_shape_right_leg_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_right_leg = jQuery('#text_tagd_shape_right_leg_'+tgpd_glass_shape).val();
	   }
	   if(jQuery('#text_tagd_shape_height_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_height = jQuery('#text_tagd_shape_height_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_base_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_base = jQuery('#text_tagd_shape_base_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_side_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_side = jQuery('#text_tagd_shape_side_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_diameter_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_diameter = jQuery('#text_tagd_shape_diameter_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_radius_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_radius = jQuery('#text_tagd_shape_radius_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_leg_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_leg = jQuery('#text_tagd_shape_leg_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_radius_one_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_radius_one = jQuery('#text_tagd_shape_radius_one_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_radius_two_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_radius_two = jQuery('#text_tagd_shape_radius_two_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_length_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_length = jQuery('#text_tagd_shape_length_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_left_side_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_left_side = jQuery('#text_tagd_shape_left_side_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_right_side_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_right_side = jQuery('#text_tagd_shape_right_side_'+tgpd_glass_shape).val();
		}
    
		if(jQuery('#text_tagd_shape_left_offset_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_left_offset = jQuery('#text_tagd_shape_left_offset_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_top_offset_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_top_offset = jQuery('#text_tagd_shape_top_offset_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_right_offset_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_right_offset = jQuery('#text_tagd_shape_right_offset_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_top_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_top = jQuery('#text_tagd_shape_top_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_width_'+tgpd_glass_shape).is(":visible")){
		var tgpd_glass_shape_width = jQuery('#text_tagd_shape_width_'+tgpd_glass_shape).val();
		}
   }

   if (typeof tgpd_glass_type === 'undefined' || tgpd_glass_type == null) tgpd_glass_type = 'not_applicable';
   var tgpd_lite_thickness = jQuery('#select_tgpd_lite_thickness').val();
   if (typeof tgpd_lite_thickness === 'undefined' || tgpd_lite_thickness == null) tgpd_lite_thickness = 'not_applicable';
   var tgpd_spacer_color = jQuery('#select_tgpd_spacer_color').val();
   if (typeof tgpd_spacer_color === 'undefined' || tgpd_spacer_color == null) tgpd_spacer_color = 'not_applicable';
   var tgpd_glass_treatment = jQuery('#select_tgpd_glass_treatment').val();
   if (typeof tgpd_glass_treatment === 'undefined' || tgpd_glass_treatment == null) tgpd_glass_treatment = 'not_applicable';
   var tgpd_glass_color = jQuery('#select_tgpd_glass_color').val();
   if (typeof tgpd_glass_color === 'undefined' || tgpd_glass_color == null) tgpd_glass_color = 'not_applicable';
   var tgpd_glass_color_note = jQuery('#text_tgpd_glass_color_note').val();
   var tgpd_glass_lift = jQuery('#text_tgpd_glass_lift').val();
   var tgpd_glass_inside_lift_with_glass_type = jQuery('#text_tgpd_glass_inside_lift_with_glass_type').val();
   var tgpd_glass_outside_lift_with_glass_type = jQuery('#text_tgpd_glass_outside_lift_with_glass_type').val();
   var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
   if (typeof tgpd_glass_set === 'undefined' || tgpd_glass_set == null) tgpd_glass_set = 'not_applicable';
   var tgpd_daylight_width = jQuery('#text_tgpd_daylight_width').val();
   var tgpd_daylight_height = jQuery('#text_tgpd_daylight_height').val();
   var tgpd_go_width = jQuery('#text_tgpd_go_width').val();
   var tgpd_go_height = jQuery('#text_tgpd_go_height').val();
   var tgpd_size_verification_status = jQuery('#select_tgpd_size_verification_status').val();
   if (typeof tgpd_size_verification_status === 'undefined' || tgpd_size_verification_status == null) tgpd_size_verification_status = 'not_applicable';
   
   var tgpd_overall_thickness = jQuery('#select_tgpd_overall_thickness').val();   
   if (typeof tgpd_overall_thickness === 'undefined' || tgpd_overall_thickness == null) tgpd_overall_thickness = 'not_applicable';
   
  var tgpd_damage_waiver_select = jQuery('#select_tgpd_glass_damage_waiver').val();   
   if (typeof tgpd_damage_waiver_select === 'undefined' || tgpd_damage_waiver_select == null) tgpd_damage_waiver_select = 'not_applicable';
   
   var tgpd_disclamers_select = jQuery('#select_tgpd_glass_disclamers').val();   
   if (typeof tgpd_disclamers_select === 'undefined' || tgpd_disclamers_select == null) tgpd_disclamers_select = 'not_applicable';
   
	var tgpd_exterior_glass_type = jQuery('#select_tgpd_exterior_glass_type').val();
	if (typeof tgpd_exterior_glass_type === 'undefined' || tgpd_exterior_glass_type == null) tgpd_exterior_glass_type = 'not_applicable';
	
	var tgpd_interior_glass_type = jQuery('#select_tgpd_interior_glass_type').val();
	if (typeof tgpd_interior_glass_type === 'undefined' || tgpd_interior_glass_type == null) tgpd_interior_glass_type = 'not_applicable';
	
	var tgpd_interior_glass_type_non_igu = jQuery('#select_tgpd_interior_glass_type_non_igu').val();
	if (typeof tgpd_interior_glass_type_non_igu === 'undefined' || tgpd_interior_glass_type_non_igu == null) tgpd_interior_glass_type_non_igu = 'not_applicable';
	
	var tgpd_treatment_exterior_glass = jQuery('#select_tgpd_treatment_exterior_glass').val();
	if (typeof tgpd_treatment_exterior_glass === 'undefined' || tgpd_treatment_exterior_glass == null) tgpd_treatment_exterior_glass = 'not_applicable';
	
	var tgpd_treatment_interior_glass = jQuery('#select_tgpd_treatment_interior_glass').val();
	if (typeof tgpd_treatment_interior_glass === 'undefined' || tgpd_treatment_interior_glass == null) tgpd_treatment_interior_glass = 'not_applicable';
	
	var tgpd_exterior_color_glass_type = jQuery('#select_tgpd_exterior_color_glass_type').val();
	if (typeof tgpd_exterior_color_glass_type === 'undefined' || tgpd_exterior_color_glass_type == null) tgpd_exterior_color_glass_type = 'not_applicable';
	
	var tgpd_interior_color_glass_type = jQuery('#select_tgpd_interior_color_glass_type').val();
	if (typeof tgpd_interior_color_glass_type === 'undefined' || tgpd_interior_color_glass_type == null) tgpd_interior_color_glass_type = 'not_applicable';

   var tgpd_sag_or_quote = jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked') ? 1 : 0;
   var tgpd_manufacturer = jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer').val();
   var tgpd_solar_film = jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked') ? 1 : 0;
   var tgpd_solar_film_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]:checked').val();
   if (typeof tgpd_solar_film_responsibility === 'undefined' || tgpd_solar_film_responsibility == null) tgpd_solar_film_responsibility = '';
   var tgpd_solar_film_type = jQuery('#text_tgpd_glass_reminders_solar_film_type').val();
   var tgpd_solar_film_source = jQuery('#text_tgpd_glass_reminders_solar_film_source').val();
   var tgpd_wet_seal = jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked') ? 1 : 0;
   var tgpd_wet_seal_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]:checked').val();
   if (typeof tgpd_wet_seal_responsibility === 'undefined' || tgpd_wet_seal_responsibility == null) tgpd_wet_seal_responsibility = '';
   var tgpd_furniture_to_move = jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked') ? 1 : 0;
 /**************************************************************************************************************************/
  /* var tgpd_lift_inside_glass_section_reminder = jQuery('#text_lift_inside_position').prop('checked') ? 1 : 0;
   var tgpd_lift_outside_glass_section_reminder = jQuery('#text_lift_outside_position').prop('checked') ? 1 : 0;*/
   /***************************************************************************************************************************/
   var tgpd_disclamers_to_move = jQuery('#checkbox_tgpd_glass_reminders_add_disclamers').prop('checked') ? 1 : 0;
   var tgpd_furniture_to_move_comment = jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment').val();
   
   
   /******************************************************************************************************************************/
   var tgpd_damage_waiver_select = jQuery('#select_tgpd_glass_damage_waiver').val();
   var tgpd_disclamers_select = jQuery('#select_tgpd_glass_disclamers').val();
   var tgpd_damage_waiver_text = jQuery('#text_tgpd_glass_damage_waiver_reminder_section').val();
   var tgpd_disclamers_text = jQuery('#text_tgpd_glass_disclamers_reminder_section').val();
   /******************************************************************************************************************************/
   
   var tgpd_walls_or_ceilings_to_cut = jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked') ? 1 : 0;
   var tgpd_walls_or_ceilings_to_cut_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]:checked').val();
   if (typeof tgpd_walls_or_ceilings_to_cut_responsibility === 'undefined' || tgpd_walls_or_ceilings_to_cut_responsibility == null) tgpd_walls_or_ceilings_to_cut_responsibility = '';
   var tgpd_walls_or_ceilings_to_cut_comment = jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment').val();
   var tgpd_blind_needs_removing = jQuery('#checkbox_tgpd_glass_reminders_blind_needs_removing').prop('checked') ? 1 : 0;
   var tgpd_glass_fits_elevator = jQuery('#checkbox_tgpd_glass_reminders_glass_fits_elevator').prop('checked') ? 1 : 0;
   var tgpd_instructions = jQuery('#textarea_tgpd_instructions').val();
//   var tgpd_caulk_amount = jQuery('#text_tgpd_glass_materials_caulk_amount').val(); 

  
  var tgpd_caulk_amount=[];
  var tgpd_caulk_type=[];
  var tgpd_scaffolding_type=[];
  var tgpd_quantity_type=[];
  
  
var tgpd_glass_holes_type=[];
var tgpd_glass_holes_size=[];
var tgpd_holes_x_location=[];
var tgpd_holes_y_location=[];
var tgpd_holes_x_location_cl=[];
var tgpd_holes_y_location_cl=[];
var tgpd_holes_notes=[];
  
		 
  var tgpcf=	tgpd_current_item ;
	//	alert('tgpd_current_item tgpcf >> ' + tgpcf);
		//alert(maxChaulks[tgpcf+1]);
	//	alert(maxChaulks[tgpcf]);
	
	/*
	maxChannels=[];
var maxScaffoldings=[];

var maxScaffolding;	
var maxChannel;		
	*/
	
function checkValidateText(txt){
	if(txt=='not_applicable' || txt=='' || txt=='undefined' || typeof txt == 'undefined' || txt == null){
		txt = '';
	}
	return txt;
}
/*********SAG 131 *********/

var offset_data = [];

 var glass_piece_offset_lite_text = $("select#glass_piece_offset_lite_text")
              .map(function(){return $(this).val();}).get();

  var glass_piece_offset_Fill_text = $("select#glass_piece_offset_Fill_text")
              .map(function(){return $(this).val();}).get();
              
var glass_piece_offset_side_text = $("select#glass_piece_offset_side_text")
.map(function(){return $(this).val();}).get();

var glass_piece_offset_offset_text = $("select#glass_piece_offset_offset_text")
.map(function(){return $(this).val();}).get();  


var glass_piece_offset_notes_text = $("input#glass_piece_offset_notes_text")
.map(function(){return $(this).val();}).get();






offset_data.push(glass_piece_offset_lite_text);
offset_data.push(glass_piece_offset_Fill_text);

offset_data.push(glass_piece_offset_side_text);
offset_data.push(glass_piece_offset_offset_text);
offset_data.push(glass_piece_offset_notes_text);

              console.log('offset_data',offset_data);

/*********SAG 131 *********/  
	
	 for( var i=0;i<maxHoles[tgpcf]+1; i++ ){
		 
		/* // var tgpd_holes_glass_type= jQuery('#glass_piece_holes_type_text_'+tgpcf+'_'+i).val();
		 //alert(" tgpd_holes_glass_type "+tgpd_holes_glass_type);
		 var holeTyp = jQuery('#glass_piece_holes_type_text_'+tgpcf+'_'+i).val();
		var startXLoc =  jQuery('#glass_piece_holes_x_location_starting_point_text_'+tgpcf+'_'+i).val();
		var startYLoc =  jQuery('#glass_piece_holes_y_location_starting_point_text_'+tgpcf+'_'+i).val();
		var clLocX = jQuery('#glass_piece_holes_x_location_cl_text_'+tgpcf+'_'+i).val();
		var clLocY = jQuery('#glass_piece_holes_y_location_cl_text_'+tgpcf+'_'+i).val();
		var holeNote  = jQuery('#glass_piece_holes_notes_text_'+tgpcf+'_'+i).val();
		
		
		
		 //if(holeTyp!='not_applicable' && startXLoc!='not_applicable' && !!clLocX && !!clLocY && !!holeNote ){
			 
			tgpd_glass_holes_type.push(jQuery('#glass_piece_holes_type_text_'+tgpcf+'_'+i).val());
			tgpd_glass_holes_size.push(jQuery('#glass_piece_holes_size_text_'+tgpcf+'_'+i).val());
			tgpd_holes_x_location.push(jQuery('#glass_piece_holes_x_location_starting_point_text_'+tgpcf+'_'+i).val());
			tgpd_holes_y_location.push(jQuery('#glass_piece_holes_y_location_starting_point_text_'+tgpcf+'_'+i).val());
			tgpd_holes_x_location_cl.push(jQuery('#glass_piece_holes_x_location_cl_text_'+tgpcf+'_'+i).val());
			tgpd_holes_y_location_cl.push(jQuery('#glass_piece_holes_y_location_cl_text_'+tgpcf+'_'+i).val());
			tgpd_holes_notes.push(jQuery('#glass_piece_holes_notes_text_'+tgpcf+'_'+i).val());
			 
		// }*/



        //if(jQuery('#glass_piece_holes_type_text_'+tgpcf+'_'+i).length){
    		var holeTyp = checkValidateText(jQuery('#glass_piece_holes_type_text_'+tgpcf+'_'+i).val());
         Circle_type_hole = [];
       if(holeTyp == 'Counter Sunk (Circle)'){
            
            var holeTypSize_interior = checkValidateText(jQuery('#glass_piece_holes_exterior_size_text_'+tgpcf+'_'+i).val());
            var holeTypSize_exteror = checkValidateText(jQuery('#glass_piece_holes_interior_size_text_'+tgpcf+'_'+i).val());
     
            Circle_type_hole.push(holeTypSize_interior);
            Circle_type_hole.push(holeTypSize_exteror);


      } else {
    		var holeTypSize = checkValidateText(jQuery('#glass_piece_holes_size_text_'+tgpcf+'_'+i).val());
      }
    		var startXLoc =  checkValidateText(jQuery('#glass_piece_holes_x_location_starting_point_text_'+tgpcf+'_'+i).val());
    		var startYLoc =  checkValidateText(jQuery('#glass_piece_holes_y_location_starting_point_text_'+tgpcf+'_'+i).val());
    		var clLocX = checkValidateText(jQuery('#glass_piece_holes_x_location_cl_text_'+tgpcf+'_'+i).val());
    		var clLocY = checkValidateText(jQuery('#glass_piece_holes_y_location_cl_text_'+tgpcf+'_'+i).val());
    		var holeNote  = checkValidateText(jQuery('#glass_piece_holes_notes_text_'+tgpcf+'_'+i).val());
    		if(holeTyp!='' || startXLoc!='' || startYLoc!='' || clLocX!='' || clLocY!='' || holeNote!='' || holeTypSize!=''){
    		    //console.log(holeTyp+' : '+holeTypSize+' : '+startXLoc+' : '+clLocX+' : '+startYLoc+' : '+clLocY+' : '+holeNote); 
    			tgpd_glass_holes_type.push(holeTyp);
    			if(holeTyp == 'Counter Sunk (Circle)'){
         tgpd_glass_holes_size.push(Circle_type_hole);
      } else {
         tgpd_glass_holes_size.push(holeTypSize);

      }
    			tgpd_holes_x_location.push(startXLoc);
    			tgpd_holes_y_location.push(startYLoc);
    			tgpd_holes_x_location_cl.push(clLocX);
    			tgpd_holes_y_location_cl.push(clLocY);
    			tgpd_holes_notes.push(holeNote);
    			 
    		 }
		 //}
		 
	 }
	 
	 
	
	
	 for( var i=0;i<maxScaffoldings[tgpcf]+1; i++ ){
		  
		 var culkScaff= jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+i).val();
		 var culkqty= jQuery('#select_tgpd_glass_materials_quantity_'+tgpcf+'_'+i).val();
		
		 
		 if(!!!culkqty && (culkScaff == 'undefined' || culkScaff =='not_applicable')){
			 continue;
		 }else if((culkqty=='' || culkqty == null || culkqty== 'undefined')&& (typeof culkScaff == 'undefined' || culkScaff == null || culkScaff == '')){
			continue;
		 }else{
				tgpd_quantity_type.push(culkqty);
				
				console.log(culkScaff + ' culkScaff tester');
				if(typeof culkScaff == 'undefined' ||  culkScaff == null || culkScaff == ''){
					tgpd_scaffolding_type.push('not_applicable');
				}else{
					tgpd_scaffolding_type.push(culkScaff);
				}

		 }
		 
		
		
	   }
	   
	   console.log(tgpd_quantity_type + ' culkqty tester');
	
	
	
		 for( var i=0;i<maxChaulks[tgpcf]+1; i++ ){
		  
		 var culkAmt = jQuery('#text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+i).val();	
		 var culkTyp =jQuery('#select_tgpd_glass_materials_caulk_type_'+tgpcf+'_'+i).val();	
		// alert(" culkTyp >>>> "+ culkTyp + " culkAmt >>>> "+ culkAmt);
	//	 var culkScaff= jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+i).val();
	//text_tgpd_glass_materials_caulk_amount_0_0
		 if(!!!culkAmt && (culkTyp=='undefined' || culkTyp== 'not_applicable' )){
			 
			 continue;
		 }
			 else if((culkAmt=='' || culkAmt == null) && (typeof culkTyp  == 'undefined' || culkTyp == null || culkTyp=='')){
				continue;	 
				 
			 }
			 else
			 {
				tgpd_caulk_amount.push(culkAmt);
				console.log(culkAmt + ' culkAmt tester');
				console.log(culkTyp + ' culkTyp tester');
				if (typeof culkTyp  == 'undefined' || culkTyp == null || culkTyp=='')
				{ 
					tgpd_caulk_type.push('not_applicable');
				}
				else
				{
					 tgpd_caulk_type.push(culkTyp);
				}
				
			 }
	   }
	   
	    var tgpd_tape_amount=[];
		var tgpd_tape_type=[];
		var tgpd_channel=[];
  var tgpd_quantity_channel=[];
  
  	/*
	maxChannels=[];var maxScaffoldings=[];var maxScaffolding;	var maxChannel;		
	*/
    for( var i=0;i<maxChannels[tgpcf]+1; i++ ){
		   
			var tapeChannel= jQuery('#text_tgpd_glass_materials_channel_'+tgpcf+'_'+i).val()
		  var tapeChannel_quanitity = jQuery('#select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+i).val();
			 
		//alert("tapeChannel >> "+ tapeChannel);
		
		if(tapeChannel == 'undefined'){
			 
			 continue;
		 }else if(( typeof tapeChannel == 'undefined' || tapeChannel == null || tapeChannel=='') ){
				continue;	 
			}
			 else
			 {
				tgpd_channel.push(jQuery('#text_tgpd_glass_materials_channel_'+tgpcf+'_'+i).val());
			
			 }
		
  if(tapeChannel_quanitity == 'undefined'){
         continue;
      }else if(( typeof tapeChannel_quanitity == 'undefined' || tapeChannel_quanitity == null || tapeChannel_quanitity=='')){
         continue;
      }else{
         tgpd_quantity_channel.push(jQuery('#select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+i).val());
      }
      	   }
	   
  
  
  
  
	   for( var i=0;i<maxTapes[tgpcf]+1; i++ ){
		   
		var tapeTyp = jQuery('#select_tgpd_glass_materials_tape_type_'+tgpcf+'_'+i).val();
		
		var tapeAmt =	jQuery('#text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+i).val()
	//	var tapeChannel= jQuery('#text_tgpd_glass_materials_channel_'+tgpcf+'_'+i).val()
		
		//alert("tapeChannel >> "+ tapeChannel + " >> tapeAmt >> "+ tapeAmt);
		
		if(tapeTyp == 'undefined' && tapeAmt == 'undefined' ){
			 
			 continue;
		 }else if((tapeTyp=='' || tapeTyp == null) && (tapeAmt=='' || tapeAmt == null) ){
			 
				continue;	 
				 
			 }
			 else
			 {
				tgpd_tape_amount.push(jQuery('#text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+i).val());
				
				if (typeof tapeTyp == 'undefined' || tapeTyp == null || tapeTyp==''){
					tgpd_tape_type.push('not_applicable');
				}else{
					
					tgpd_tape_type.push(tapeTyp);	
				} 
			 }
		
		
		
	   }
	   
	/*   for( var i=0;i<maxChaulks[tgpcf]+1; i++ ){
		  
		 var jQuery('#text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+i).val();	
		 var culkTyp =jQuery('#select_tgpd_glass_materials_caulk_type_'+tgpcf+'_'+i).val();	
		 var culkScaff= jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+i).val();
		 
		 
		tgpd_caulk_amount.push();

		
		console.log(culkTyp + ' culkTyp tester');
		if (typeof culkTyp  == 'undefined' || culkTyp == null || culkTyp=='')
		{ 
			tgpd_caulk_type.push('not_applicable');
		}else{
			 tgpd_caulk_type.push(culkTyp);
		}
		 
		
		 if (typeof culkScaff == 'undefined' || culkScaff == null || culkScaff==''){
			 tgpd_scaffolding_type.push('not_applicable');
			 
		 }else{
			 		tgpd_scaffolding_type.push(culkScaff);
		
			} //tgpd_scaffolding_type = 'not_applicable';
		
	   }
	   
	    var tgpd_tape_amount=[];
		var tgpd_tape_type=[];
		var tgpd_channel=[];
  
	   for( var i=0;i<maxTapes[tgpcf]+1; i++ ){
		   
		tgpd_tape_amount.push(jQuery('#text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+i).val());
		tgpd_channel.push(jQuery('#text_tgpd_glass_materials_channel_'+tgpcf+'_'+i).val());
		
		var tapeTyp = jQuery('#select_tgpd_glass_materials_tape_type_'+tgpcf+'_'+i).val();
		
	

		
		if (typeof tapeTyp == 'undefined' || tapeTyp == null || tapeTyp==''){
			tgpd_tape_type.push('not_applicable');
		}else{
			
			tgpd_tape_type.push(tapeTyp);	
		} //tgpd_tape_type = 'not_applicable';
		
		
		
		
	   }
	   
	   */
  
   
  // var tgpd_caulk_amount = jQuery('#text_tgpd_glass_materials_caulk_amount').val(); 
 //  var tgpd_caulk_type = jQuery('#select_tgpd_glass_materials_caulk_type').val();
 
  // var tgpd_scaffolding_type = jQuery('#select_tgpd_glass_materials_scaffolding_type').val();
//   if (typeof tgpd_scaffolding_type === 'undefined' || tgpd_scaffolding_type == null) tgpd_scaffolding_type = 'not_applicable';
 //  var tgpd_tape_amount = jQuery('#text_tgpd_glass_materials_tape_amount').val();
  // var tgpd_tape_type = jQuery('#select_tgpd_glass_materials_tape_type').val();
  // if (typeof tgpd_tape_type === 'undefined' || tgpd_tape_type == null) tgpd_tape_type = 'not_applicable';
//   var tgpd_channel = jQuery('#text_tgpd_glass_materials_channel').val();
   var tgpd_miscellaneous = jQuery('#text_tgpd_miscellaneous').val();
   var tgpd_glass_grids = jQuery('#checkbox_tgpd_glass_grids').prop('checked') ? 1 : 0;
   var tgpd_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids').val();
   var tgpd_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids').val();
   var tgpd_grids_thickness = jQuery('#select_tgpd_grids_thickness').val();
   if (typeof tgpd_grids_thickness === 'undefined' || tgpd_grids_thickness == null) tgpd_grids_thickness = 'not_applicable';
   var tgpd_grids_color = jQuery('#text_tgpd_grids_color').val();
      /*sag-158*/
   var tgpd_grids_color_custom = jQuery('#text_tgpd_grids_color_custom').val();
   /*sag-158*/
   var tgpd_glass_fabrication = jQuery('#checkbox_tgpd_glass_fabrication').prop('checked') ? 1 : 0;
   var tgpd_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges').val();
   var tgpd_fabrication_holes = jQuery('#text_tgpd_fabrication_holes').val();
   var tgpd_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern').val();




var exterior_light_thickness = jQuery('#exterior_light_thickness').val();
   var interior_light_thickness = jQuery('#interior_light_thickness').val();
   var select_tgpd_innner_layer_thickness = jQuery('#select_tgpd_innner_layer_thickness').val();
   var custom_layer_thickness = jQuery('#custom_layer_thickness').val();

  if(job_id>0)
  	{
		//tgpd_pictures_download_url=[];

				jQuery("#div_tgpd_picture_filelist").find("ul li").each(function(){

				var htmlset=jQuery(this).html();

				var srcdd=	jQuery(this).find("img").attr("src");
			if(srcdd.indexOf("blob:http")<0 && jQuery.inArray(srcdd, tgpd_pictures_download_url)  == -1)
				{
				tgpd_pictures_download_url.push(srcdd);
				}

				});

	}

	 if(job_id>0)
  	{
		//tgpd_sketches_download_url=[];
				jQuery("#div_tgpd_sketch_filelist").find("ul li").each(function(){

				var htmlset=jQuery(this).html();

				var srcdd=	jQuery(this).find("img").attr("src");
				
				if(srcdd.indexOf("blob:http")<0 && jQuery.inArray(srcdd, tgpd_sketches_download_url) == -1)
				{
				tgpd_sketches_download_url.push(srcdd);
				}

				});


	}



   var tgpd_item = {};

 	/*verify code start from here data 02 Nov, 2017*/
	tgpd_item.verify_tgpd_overall_thickness =verify_tgpd_overall_thickness ;
	tgpd_item.verify_tgpd_exterior_glass_type =verify_tgpd_exterior_glass_type ;
	tgpd_item.verify_tgpd_interior_glass_type =verify_tgpd_interior_glass_type ;
	tgpd_item.verify_tgpd_treatment_exterior_glass   =verify_tgpd_treatment_exterior_glass;
	tgpd_item.verify_tgpd_treatment_interior_glass   =verify_tgpd_treatment_interior_glass;
	tgpd_item.verify_tgpd_exterior_color_glass_type   =verify_tgpd_exterior_color_glass_type;
	tgpd_item.verify_tgpd_interior_color_glass_type   =verify_tgpd_interior_color_glass_type;
	tgpd_item.verify_tgpd_coating_exterior_type   =verify_tgpd_coating_exterior_type;
	tgpd_item.verify_tgpd_coating_interior_type   =verify_tgpd_coating_interior_type;
	tgpd_item.verify_tgpd_exterior_lite_thickness   = verify_tgpd_exterior_lite_thickness;
	tgpd_item.verify_tgpd_interior_lite_thickness   =verify_tgpd_interior_lite_thickness;	
	tgpd_item.verify_tgpd_inner_layer_thickness  = verify_tgpd_inner_layer_thickness;
	tgpd_item.verify_tgpd_lite_thickness  = verify_tgpd_lite_thickness;
	tgpd_item.verify_tgpd_glass_treatment = verify_tgpd_glass_treatment;
	tgpd_item.verify_tgpd_glass_color  = verify_tgpd_glass_color;
	tgpd_item.verify_tgpd_glass_color_note = verify_tgpd_glass_color_note;
	tgpd_item.verify_tgpd_glass_set = verify_tgpd_glass_set;
	tgpd_item.verify_tgpd_glass_lift = verify_tgpd_glass_lift;
	tgpd_item.verify_tgpd_glass_inside_lift_with_glass_type = verify_tgpd_glass_inside_lift_with_glass_type;
	tgpd_item.text_tgpd_text_lift_inside_position = text_tgpd_text_lift_inside_position;
	tgpd_item.text_tgpd_text_lift_outside_position = text_tgpd_text_lift_outside_position;
	tgpd_item.text_tgpd_fabrication_polished_edges_hide = text_tgpd_fabrication_polished_edges_hide;
	tgpd_item.text_tgpd_fabrication_pattern_hide = text_tgpd_fabrication_pattern_hide;
	tgpd_item.text_tgpd_fabrication_holes_hide_new = text_tgpd_fabrication_holes_hide_new;
	tgpd_item.text_tgpd_fabrication_holes_hide_old = text_tgpd_fabrication_holes_hide_old;
	
	
	
	
	tgpd_item.verify_tgpd_glass_outside_lift_with_glass_type = verify_tgpd_glass_outside_lift_with_glass_type;
	tgpd_item.verify_tgpd_spacer_color = verify_tgpd_spacer_color;
	tgpd_item.verify_tgpd_daylight_height = verify_tgpd_daylight_height;
	tgpd_item.verify_tgpd_daylight_width = verify_tgpd_daylight_width;
	tgpd_item.verify_go_size_height = verify_go_size_height;
	tgpd_item.verify_go_size_width = verify_go_size_width;
	tgpd_item.verify_set_height = verify_set_height;
	tgpd_item.verify_set_width = verify_set_width;
	tgpd_item.text_tgpd_extra_height = text_tgpd_extra_height;
	tgpd_item.text_tgpd_extra_width = text_tgpd_extra_width;
	tgpd_item.id = 0;
	tgpd_item.job_id = job_id;
	tgpd_item.glass_location = tgpd_glass_location;
	tgpd_item.coating_exterior_type = tgpd_coating_exterior_type;
	tgpd_item.coating_interior_type = tgpd_coating_interior_type;
  /*sag 124**/
   tgpd_item.tgpd_coating_family_exterior_type = tgpd_coating_family_exterior_type;
   tgpd_item.tgpd_coating_family_interior_type = tgpd_coating_family_interior_type;
   tgpd_item.tgpd_coating_family_color_exterior_type = tgpd_coating_family_color_exterior_type;
   tgpd_item.tgpd_coating_family_color_interior_type = tgpd_coating_family_color_interior_type;
  
    tgpd_item.verify_tgpd_exterior_coating_family_glass_type = verify_tgpd_exterior_coating_family_glass_type;
   tgpd_item.verify_tgpd_interior_coating_family_glass_type = verify_tgpd_interior_coating_family_glass_type;
  

    tgpd_item.verify_tgpd_exterior_coating_family_color_glass_type = verify_tgpd_exterior_coating_family_glass_type;
   tgpd_item.verify_tgpd_interior_coating_family_color_glass_type = verify_tgpd_interior_coating_family_glass_type;
  
/*sag 124**/
	tgpd_item.exterior_glass_type = tgpd_exterior_glass_type;
	tgpd_item.interior_glass_type = tgpd_interior_glass_type;
	tgpd_item.interior_glass_type_non_igu = tgpd_interior_glass_type_non_igu;
	tgpd_item.treatment_exterior_glass = tgpd_treatment_exterior_glass;
	tgpd_item.treatment_interior_glass = tgpd_treatment_interior_glass;
	tgpd_item.exterior_color_glass_type = tgpd_exterior_color_glass_type;
	tgpd_item.interior_color_glass_type = tgpd_interior_color_glass_type;
	tgpd_item.glass_quantity = tgpd_glass_quantity;
  if ( (tgpd_glass_shape != 'not_applicable' ) && (tgpd_glass_shape != 'Rectangular/Square' ) ) {
  	if(jQuery('#text_tagd_shape_left_leg_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_left_leg = tgpd_glass_shape_left_leg;
     }
     if(jQuery('#text_tagd_shape_right_leg_'+tgpd_glass_shape).is(":visible")){
     		tgpd_item.glass_shape_right_leg = tgpd_glass_shape_right_leg;
     }
     if(jQuery('#text_tagd_shape_width_'+tgpd_glass_shape).is(":visible")){
     		tgpd_item.glass_shape_width = tgpd_glass_shape_width;
     }
     if(jQuery('#text_tagd_shape_height_'+tgpd_glass_shape).is(":visible")){
     		tgpd_item.glass_shape_height = tgpd_glass_shape_height;
     }

     if(jQuery('#text_tagd_shape_base_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_base = tgpd_glass_shape_base;
     }
     if(jQuery('#text_tagd_shape_side_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_side = tgpd_glass_shape_side;
     }
     if(jQuery('#text_tagd_shape_diameter_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_diameter = tgpd_glass_shape_diameter;
     }
     if(jQuery('#text_tagd_shape_radius_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_radius = tgpd_glass_shape_radius;
     }
     if(jQuery('#text_tagd_shape_radius_one_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_radius_one = tgpd_glass_shape_radius_one;
     }
     if(jQuery('#text_tagd_shape_radius_two_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_radius_two = tgpd_glass_shape_radius_two;
     }
     if(jQuery('#text_tagd_shape_length_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_length = tgpd_glass_shape_length;
     }
     if(jQuery('#text_tagd_shape_leg_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_leg = tgpd_glass_shape_leg;
     }
     if(jQuery('#text_tagd_shape_left_side_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_left_side = tgpd_glass_shape_left_side;
     }
     if(jQuery('#text_tagd_shape_right_side_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_right_side = tgpd_glass_shape_right_side;
     }
     if(jQuery('#text_tagd_shape_left_offset_'+tgpd_glass_shape).is(":visible")){
     		tgpd_item.glass_shape_left_offset = tgpd_glass_shape_left_offset;
     }
     if(jQuery('#text_tagd_shape_top_offset_'+tgpd_glass_shape).is(":visible")){
     		tgpd_item.glass_shape_top_offset = tgpd_glass_shape_top_offset;
     }
     if(jQuery('#text_tagd_shape_right_offset_'+tgpd_glass_shape).is(":visible")){
     		tgpd_item.glass_shape_right_offset = tgpd_glass_shape_right_offset;
     }
     if(jQuery('#text_tagd_shape_top_'+tgpd_glass_shape).is(":visible")){
     	tgpd_item.glass_shape_top = tgpd_glass_shape_top;
     }
   }
   tgpd_item.glass_servicetype = tgpd_glass_servicetype;
   tgpd_item.glass_type = tgpd_glass_type;
   tgpd_item.glass_shape = tgpd_glass_shape;
   tgpd_item.lite_thickness = tgpd_lite_thickness;
   tgpd_item.spacer_color = tgpd_spacer_color;
   tgpd_item.glass_treatment = tgpd_glass_treatment;
   tgpd_item.glass_color = tgpd_glass_color;
   tgpd_item.glass_color_note = tgpd_glass_color_note;
   tgpd_item.glass_lift = tgpd_glass_lift;
   tgpd_item.glass_side_lift_with_glass_type = tgpd_glass_outside_lift_with_glass_type;
   tgpd_item.glass_inside_lift_with_glass_type = tgpd_glass_inside_lift_with_glass_type;
   tgpd_item.glass_outside_lift_with_glass_type = tgpd_glass_outside_lift_with_glass_type;
   tgpd_item.glass_set = tgpd_glass_set;
   tgpd_item.daylight_width = tgpd_daylight_width;
   tgpd_item.daylight_height = tgpd_daylight_height;
   tgpd_item.go_width = tgpd_go_width;
   tgpd_item.go_height = tgpd_go_height;
   tgpd_item.size_verification_status = tgpd_size_verification_status;
   tgpd_item.overall_thickness = tgpd_overall_thickness;   
   tgpd_item.sag_or_quote = tgpd_sag_or_quote;
   tgpd_item.manufacturer = tgpd_manufacturer;
   tgpd_item.solar_film = tgpd_solar_film;
   tgpd_item.solar_film_responsibility = tgpd_solar_film_responsibility;
   tgpd_item.solar_film_type = tgpd_solar_film_type;
   tgpd_item.solar_film_source = tgpd_solar_film_source;
   tgpd_item.wet_seal = tgpd_wet_seal;
   tgpd_item.wet_seal_responsibility = tgpd_wet_seal_responsibility;
   tgpd_item.furniture_to_move = tgpd_furniture_to_move;
   tgpd_item.disclamers = tgpd_disclamers_to_move;
   tgpd_item.furniture_to_move_comment = tgpd_furniture_to_move_comment;
   tgpd_item.damage_waiver_select = tgpd_damage_waiver_select;
   tgpd_item.disclamers_select = tgpd_disclamers_select;
   tgpd_item.damage_waiver_text = tgpd_damage_waiver_text;
   tgpd_item.disclamers_text = tgpd_disclamers_text;
   tgpd_item.walls_or_ceilings_to_cut = tgpd_walls_or_ceilings_to_cut;
   tgpd_item.walls_or_ceilings_to_cut_responsibility = tgpd_walls_or_ceilings_to_cut_responsibility;
   tgpd_item.walls_or_ceilings_to_cut_comment = tgpd_walls_or_ceilings_to_cut_comment;
   tgpd_item.blind_needs_removing = tgpd_blind_needs_removing;
   tgpd_item.glass_fits_elevator = tgpd_glass_fits_elevator;
   tgpd_item.pictures_download_url = tgpd_pictures_download_url.length > 0 ? tgpd_pictures_download_url.join() : '';
   tgpd_item.sketches_download_url = tgpd_sketches_download_url.length > 0 ? tgpd_sketches_download_url.join() : '';
   tgpd_item.instructions = tgpd_instructions;
   //tgpd_item.caulk_amount = tgpd_caulk_amount;
   tgpd_item.caulk_amount = tgpd_caulk_amount;
   tgpd_item.caulk_type = tgpd_caulk_type;
   tgpd_item.scaffolding_type = tgpd_scaffolding_type;
   tgpd_item.quantity_type = tgpd_quantity_type;
   tgpd_item.tape_amount = tgpd_tape_amount;
   tgpd_item.tape_type = tgpd_tape_type;
   tgpd_item.channel = tgpd_channel;
tgpd_item.quantity_channel = tgpd_quantity_channel;
   tgpd_item.miscellaneous = tgpd_miscellaneous;
   tgpd_item.glass_grids = tgpd_glass_grids;
   tgpd_item.pattern_horizontal_grids = tgpd_pattern_horizontal_grids;
   tgpd_item.pattern_vertical_grids = tgpd_pattern_vertical_grids;
   tgpd_item.grids_thickness = tgpd_grids_thickness;
   tgpd_item.grids_color = tgpd_grids_color;
  /*sag-158*/
   tgpd_item.grids_color_custom = tgpd_grids_color_custom;
   /*sag-158*/
   tgpd_item.glass_fabrication = tgpd_glass_fabrication;
   tgpd_item.fabrication_polished_edges = tgpd_fabrication_polished_edges;
   tgpd_item.fabrication_holes = tgpd_fabrication_holes;
   tgpd_item.fabrication_pattern = tgpd_fabrication_pattern;
   console.log("tgpd_glass_holes_type >> "+tgpd_glass_holes_type );
   console.log("tgpd_holes_x_location_cl >> "+tgpd_holes_x_location_cl );
   
    console.log("tgpd_holes_y_location_cl >> "+tgpd_holes_y_location_cl );
   
   
    
   tgpd_item.glass_holes_type =  tgpd_glass_holes_type;
   tgpd_item.glass_holes_size = tgpd_glass_holes_size;
   tgpd_item.holes_x_location=tgpd_holes_x_location;
   tgpd_item.holes_y_location=tgpd_holes_y_location;
   tgpd_item.holes_x_location_cl=tgpd_holes_x_location_cl;
   tgpd_item.holes_y_location_cl=tgpd_holes_y_location_cl;
   tgpd_item.holes_notes =tgpd_holes_notes;
    tgpd_item.offset_data = offset_data;
   

 /****************************************************************************************************************/ 

//  tgpd_item.glass_holes_type = tgpd_holes_glass_type;


/*****************************************************************************************************************/

   tgpd_item.exterior_light_thickness = exterior_light_thickness;
   tgpd_item.interior_light_thickness = interior_light_thickness;
   tgpd_item.select_tgpd_innner_layer_thickness = select_tgpd_innner_layer_thickness;
   tgpd_item.custom_layer_thickness = custom_layer_thickness;
   tgpd_item.daylight_enable  = jQuery('#text_tgpd_daylight').prop('checked')?1:0;
   tgpd_item.coating_position_enable  = jQuery('#text_tgpd_coating_first_position').prop('checked')?1:0;
   tgpd_item.coating_position_enable1  = jQuery('#text_tgpd_coating_second_position').prop('checked')?1:0;
   tgpd_item.gosize_enable   = jQuery('#text_tgpd_gosize').prop('checked')?1:0;

   tgpd_item.color_viewer   =  jQuery('#checkbox_tgpd_glass_reminders_add_color_waiver').prop('checked')?jQuery('#checkbox_tgpd_glass_reminders_add_color_waiver').val():0;

   tgpd_item.damage_viewer    =  jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').prop('checked')?jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').val():0;


   tgpd_print_item_to_console('tgpd_get_item_from_form', tgpd_item);

   return tgpd_item;
}

function tgpd_print_item_to_form(tgpd_item) { 

	/**************************************************/
	
	var liftinside = tgpd_item.glass_inside_lift_with_glass_type;
	if(!!liftinside){
	liftinside = liftinside.replace(/\\/g, "");
	liftinside = liftinside.replace(/_/g, "");
	liftinside = liftinside.replace(/'/g, '');
	liftinside = liftinside.replace(/\//g, "");
	}
	
	var lift_outside = tgpd_item.glass_outside_lift_with_glass_type;
	if(!!lift_outside){
	lift_outside = lift_outside.replace(/\\/g, "");
	lift_outside = lift_outside.replace(/_/g, "");
	lift_outside = lift_outside.replace(/'/g, '');
	lift_outside = lift_outside.replace(/\//g, "");
	}
	
	var fabrication_polished_edges = tgpd_item.fabrication_polished_edges;
	if(!!fabrication_polished_edges){
	fabrication_polished_edges = fabrication_polished_edges.replace(/\\/g, "");
	fabrication_polished_edges = fabrication_polished_edges.replace(/_/g, "");
	fabrication_polished_edges = fabrication_polished_edges.replace(/'/g, '');
	fabrication_polished_edges = fabrication_polished_edges.replace(/\//g, "");
	}
	
	var fabrication_holes = tgpd_item.fabrication_holes;
	if(!!fabrication_holes){
	fabrication_holes = fabrication_holes.replace(/\\/g, "");
	fabrication_holes = fabrication_holes.replace(/_/g, "");
	fabrication_holes = fabrication_holes.replace(/'/g, '');
	fabrication_holes = fabrication_holes.replace(/\//g, "");
	}
	
	var fabrication_pattern = tgpd_item.fabrication_pattern;
	if(!!fabrication_pattern){
	fabrication_pattern = fabrication_pattern.replace(/\\/g, "");
	fabrication_pattern = fabrication_pattern.replace(/_/g, "");
	fabrication_pattern = fabrication_pattern.replace(/'/g, '');
	fabrication_pattern = fabrication_pattern.replace(/\//g, "");
	}
	
	var manufacturer = tgpd_item.manufacturer;
	if(!!manufacturer){
	manufacturer = manufacturer.replace(/\\/g, ""); 
	manufacturer = manufacturer.replace(/_/g, "");
	manufacturer = manufacturer.replace(/'/g, '');
	manufacturer = manufacturer.replace(/\//g, ""); 
	}
	
	var grids_color = tgpd_item.grids_color;
	if(!!grids_color){
	grids_color = grids_color.replace(/\\/g, ""); 
	grids_color = grids_color.replace(/_/g, "");
	grids_color = grids_color.replace(/'/g, '');
	grids_color = grids_color.replace(/\//g, "");  
	}
  /*sag-158*/
   var grids_color_custom = tgpd_item.grids_color_custom;
   if(!!grids_color_custom){
   grids_color_custom = grids_color_custom.replace(/\\/g, ""); 
   grids_color_custom = grids_color_custom.replace(/_/g, "");
   grids_color_custom = grids_color_custom.replace(/'/g, '');
   grids_color_custom = grids_color_custom.replace(/\//g, "");  
   }
   /*sag-158*/
	
	var instructions = tgpd_item.instructions;
	if(!!instructions){
	instructions = instructions.replace(/\\/g, ""); 
	instructions = instructions.replace(/_/g, "");
	instructions = instructions.replace(/'/g, '');
	instructions = instructions.replace(/\//g, "");
	}
	
	var solar_film_type = tgpd_item.solar_film_type;
	if(!!solar_film_type){
	solar_film_type = solar_film_type.replace(/\\/g, "");
	solar_film_type = solar_film_type.replace(/_/g, "");
	solar_film_type = solar_film_type.replace(/'/g, '');
	solar_film_type = solar_film_type.replace(/\//g, "");
	}
	
	var solar_film_source = tgpd_item.solar_film_source;
	if(!!solar_film_source){
	solar_film_source = solar_film_source.replace(/\\/g, "");
	solar_film_source = solar_film_source.replace(/_/g, "");
	solar_film_source = solar_film_source.replace(/'/g, '');
	solar_film_source = solar_film_source.replace(/\//g, "");
	}
	
	var furniture_to_move_comment = tgpd_item.furniture_to_move_comment;
	if(!!furniture_to_move_comment){
	furniture_to_move_comment = furniture_to_move_comment.replace(/\\/g, "");
	furniture_to_move_comment = furniture_to_move_comment.replace(/_/g, "");
	furniture_to_move_comment = furniture_to_move_comment.replace(/'/g, '');
	furniture_to_move_comment = furniture_to_move_comment.replace(/\//g, "");
	}
	
	var walls_or_ceilings_to_cut_comment = tgpd_item.walls_or_ceilings_to_cut_comment;
	if(!!walls_or_ceilings_to_cut_comment){
	walls_or_ceilings_to_cut_comment = walls_or_ceilings_to_cut_comment.replace(/\\/g, "");
	walls_or_ceilings_to_cut_comment = walls_or_ceilings_to_cut_comment.replace(/_/g, "");
	walls_or_ceilings_to_cut_comment = walls_or_ceilings_to_cut_comment.replace(/'/g, '');
	walls_or_ceilings_to_cut_comment = walls_or_ceilings_to_cut_comment.replace(/\//g, "");
	}
	
	var damage_waiver_text = tgpd_item.damage_waiver_text;
	if(!!damage_waiver_text){
	damage_waiver_text = damage_waiver_text.replace(/\\/g, "");
	damage_waiver_text = damage_waiver_text.replace(/_/g, "");
	damage_waiver_text = damage_waiver_text.replace(/'/g, '');
	damage_waiver_text = damage_waiver_text.replace(/\//g, "");
	}
	
	var disclamers_text = tgpd_item.disclamers_text;
	if(!!disclamers_text){
	disclamers_text = disclamers_text.replace(/\\/g, "");
	disclamers_text = disclamers_text.replace(/_/g, "");
	disclamers_text = disclamers_text.replace(/'/g, '');
	disclamers_text = disclamers_text.replace(/\//g, "");
	}
	
	var miscellaneous = tgpd_item.miscellaneous;
	if(!!miscellaneous){
	miscellaneous = miscellaneous.replace(/\\/g, "");
	miscellaneous = miscellaneous.replace(/_/g, "");
	miscellaneous = miscellaneous.replace(/'/g, '');
	miscellaneous = miscellaneous.replace(/\//g, "");
	}
	
	var daylight_width = tgpd_item.daylight_width;
/*	if(!!daylight_width){
	daylight_width = daylight_width.replace(/\\/g, "");
	daylight_width = daylight_width.replace(/_/g, "");
	daylight_width = daylight_width.replace(/'/g, '');
	daylight_width = daylight_width.replace(/\//g, "");
	}*/
	
	var daylight_height = tgpd_item.daylight_height;
/*	if(!!daylight_height){
	daylight_height = daylight_height.replace(/\\/g, "");
	daylight_height = daylight_height.replace(/_/g, "");
	daylight_height = daylight_height.replace(/'/g, '');
	daylight_height = daylight_height.replace(/\//g, "");
	}*/
	
	var go_width = tgpd_item.go_width;
/*	if(!!go_width){
	go_width = go_width.replace(/\\/g, "");
	go_width = go_width.replace(/_/g, "");
	go_width = go_width.replace(/'/g, '');
	go_width = go_width.replace(/\//g, "");
	}*/
	
	var go_height = tgpd_item.go_height;
/*	if(!!go_height){
	go_height = go_height.replace(/\\/g, "");
	go_height = go_height.replace(/_/g, "");
	go_height = go_height.replace(/'/g, '');
	go_height = go_height.replace(/\//g, "");
	}*/
	
	var glass_lift = tgpd_item.glass_lift;
	if(!!glass_lift){
	glass_lift = glass_lift.replace(/\\/g, "");
	glass_lift = glass_lift.replace(/_/g, "");
	glass_lift = glass_lift.replace(/'/g, '');
	glass_lift = glass_lift.replace(/\//g, "");
	}
	
	var glass_location = tgpd_item.glass_location;
	if(!!glass_location){
	glass_location = glass_location.replace(/\\/g, "");
	glass_location = glass_location.replace(/_/g, "");
	glass_location = glass_location.replace(/'/g, '');
	glass_location = glass_location.replace(/\//g, "");
	}
   jQuery('#select_tgpd_glass_type').val([tgpd_item.glass_type]).trigger("change");
	tgpd_set_glass_type_state(tgpd_item.glass_type, false);
	
	jQuery('#text_tgpd_glass_location').val(glass_location);
	jQuery('#text_tgpd_coating_exterior_type').val(tgpd_item.coating_exterior_type);
	jQuery('#text_tgpd_coating_interior_type').val(tgpd_item.coating_interior_type);

   //alert("here");
   // jQuery('#select_tgpd_exterior_coating_family_glass_type').attr('value',tgpd_item.tgpd_coating_family_exterior_type);
   jQuery('#select_tgpd_exterior_coating_family_glass_type').addClass('hello44');
  /***sag 124**/
  console.log(typeof tgpd_item.tgpd_coating_family_color_exterior_type, tgpd_item.tgpd_coating_family_color_exterior_type)
   jQuery('#select_tgpd_exterior_coating_family_glass_type').val([tgpd_item.tgpd_coating_family_exterior_type]);
   jQuery('#select_tgpd_interior_coating_family_glass_type').val([tgpd_item.tgpd_coating_family_interior_type]);
   jQuery('#select_tgpd_exterior_coating_family_color_glass_type').val(tgpd_item.tgpd_coating_family_color_exterior_type);
   jQuery('#select_tgpd_interior_oating_family_color_glass_type').val(tgpd_item.tgpd_coating_family_color_interior_type);
   /***sag 124**/
	jQuery('#select_tgpd_exterior_glass_type').val([tgpd_item.exterior_glass_type]);
	jQuery('#select_tgpd_interior_glass_type').val([tgpd_item.interior_glass_type]);
	jQuery('#select_tgpd_interior_glass_type_non_igu').val([tgpd_item.interior_glass_type_non_igu]);
	jQuery('#select_tgpd_treatment_exterior_glass').val([tgpd_item.treatment_exterior_glass]);
	jQuery('#select_tgpd_treatment_interior_glass').val([tgpd_item.treatment_interior_glass]);
	jQuery('#select_tgpd_exterior_color_glass_type').val([tgpd_item.exterior_color_glass_type]);
	jQuery('#select_tgpd_interior_color_glass_type').val([tgpd_item.interior_color_glass_type]);
	jQuery('#text_tgpd_glass_quantity').val(tgpd_item.glass_quantity);
	jQuery('#select_tgpd_servicetype').val([tgpd_item.glass_servicetype]);
	jQuery('#select_tgpd_glass_shape').val([tgpd_item.glass_shape]).trigger("change");
  if ( (jQuery('#select_tgpd_glass_shape').val() != 'not_applicable' ) && (jQuery('#select_tgpd_glass_shape').val() != 'Rectangular/Square' ) ) {
    jQuery('#text_tagd_shape_left_leg_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_left_leg);
    jQuery('#text_tagd_shape_right_leg_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_right_leg);
    jQuery('#text_tagd_shape_width_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_width);
    jQuery('#text_tagd_shape_base_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_base);
    jQuery('#text_tagd_shape_leg_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_leg);
    jQuery('#text_tagd_shape_side_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_side);
    jQuery('#text_tagd_shape_diameter_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_diameter);
    jQuery('#text_tagd_shape_radius_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_radius);
    jQuery('#text_tagd_shape_radius_one_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_radius_one);
    jQuery('#text_tagd_shape_radius_two_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_radius_two);
    jQuery('#text_tagd_shape_length_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_length);
    jQuery('#text_tagd_shape_left_side_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_left_side);
    jQuery('#text_tagd_shape_right_side_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_right_side);
    jQuery('#text_tagd_shape_height_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_height);
    jQuery('#text_tagd_shape_left_offset_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_left_offset);
    jQuery('#text_tagd_shape_top_offset_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_top_offset);
    jQuery('#text_tagd_shape_right_offset_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_right_offset);
    jQuery('#text_tagd_shape_top_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_top);
  }

	
	tgpd_db_val=true;
	console.log("tgpd_item.glass_type test aap " + tgpd_item.glass_type);
	
	
	tgpd_db_val=false;
	
	jQuery('#select_tgpd_servicetype').val([tgpd_item.glass_servicetype]).trigger("change");
	
	
	
	
	
	//tgpd_temp_lami_state(jQuery('#select_tgpd_glass_type').val(), false);
	jQuery('#exterior_light_thickness').val(tgpd_item.exterior_light_thickness);
	jQuery('#interior_light_thickness').val(tgpd_item.interior_light_thickness);
	jQuery('#select_tgpd_innner_layer_thickness').val([tgpd_item.select_tgpd_innner_layer_thickness]);
	jQuery('#custom_layer_thickness').val(tgpd_item.custom_layer_thickness);
   jQuery('#select_tgpd_lite_thickness').val([tgpd_item.lite_thickness]);
	jQuery('#select_tgpd_spacer_color').val([tgpd_item.spacer_color]);
	jQuery('#select_tgpd_glass_treatment').val([tgpd_item.glass_treatment]);
	jQuery('#select_tgpd_glass_treatment').prop('disabled', false);
	jQuery('#select_tgpd_glass_color').val([tgpd_item.glass_color]);


	jQuery('#text_tgpd_glass_color_note').val(tgpd_item.glass_color_note);
	jQuery('#text_tgpd_glass_lift').val(glass_lift);
	jQuery('#text_tgpd_glass_inside_lift_with_glass_type').val(liftinside);
	jQuery('#text_tgpd_glass_outside_lift_with_glass_type').val(lift_outside);
	jQuery('#select_tgpd_glass_set').val([tgpd_item.glass_set]);
   if (!tgpd_copying || (tgpd_copying && tgpd_copy_operation == 0)) {
      jQuery('#text_tgpd_daylight_width').val(daylight_width);
      jQuery('#text_tgpd_daylight_height').val(daylight_height);
      jQuery('#text_tgpd_go_width').val(go_width);
      jQuery('#text_tgpd_go_height').val(go_height);
      jQuery('#select_tgpd_size_verification_status').val([tgpd_item.size_verification_status]);
      tgpd_set_size_verification_status_state(tgpd_item.size_verification_status);
   }
	jQuery('#select_tgpd_overall_thickness').val([tgpd_item.overall_thickness]);
	jQuery('#select_tgpd_interior_glass_type').val([tgpd_item.interior_glass_type]);
	jQuery('#select_tgpd_interior_glass_type_non_igu').val([tgpd_item.interior_glass_type_non_igu]);
	jQuery('#verify_tgpd_overall_thickness').prop('checked', tgpd_item.verify_tgpd_overall_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_exterior_glass_type').prop('checked', tgpd_item.verify_tgpd_exterior_glass_type == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_interior_glass_type').prop('checked', tgpd_item.verify_tgpd_interior_glass_type == 1 ? true : false).addClass('verify-active');
   /***sag 124 **/
   jQuery('#verify_tgpd_interior_coating_family_glass_type').prop('checked', tgpd_item.verify_tgpd_interior_coating_family_glass_type == 1 ? true : false).addClass('verify-active');
   jQuery('#verify_tgpd_exterior_coating_family_glass_type').prop('checked', tgpd_item.verify_tgpd_exterior_coating_family_glass_type == 1 ? true : false).addClass('verify-active');

   jQuery('#verify_tgpd_interior_coating_family_color_glass_type').prop('checked', tgpd_item.verify_tgpd_interior_coating_family_color_glass_type == 1 ? true : false).addClass('verify-active');
   jQuery('#verify_tgpd_exterior_coating_family_color_glass_type').prop('checked', tgpd_item.verify_tgpd_exterior_coating_family_color_glass_type == 1 ? true : false).addClass('verify-active');
   /****124**/
	jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked', tgpd_item.verify_tgpd_treatment_exterior_glass == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_treatment_interior_glass').prop('checked', tgpd_item.verify_tgpd_treatment_interior_glass == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_exterior_color_glass_type').prop('checked', tgpd_item.verify_tgpd_exterior_color_glass_type == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_interior_color_glass_type').prop('checked', tgpd_item.verify_tgpd_interior_color_glass_type == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_coating_exterior_type').prop('checked', tgpd_item.verify_tgpd_coating_exterior_type == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_coating_interior_type').prop('checked', tgpd_item.verify_tgpd_coating_interior_type == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked', tgpd_item.verify_tgpd_exterior_lite_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_interior_lite_thickness').prop('checked', tgpd_item.verify_tgpd_interior_lite_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_inner_layer_thickness').prop('checked', tgpd_item.verify_tgpd_inner_layer_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_lite_thickness').prop('checked', tgpd_item.verify_tgpd_lite_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_set').prop('checked', tgpd_item.verify_tgpd_glass_set == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_treatment').prop('checked', tgpd_item.verify_tgpd_glass_treatment == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_color').prop('checked', tgpd_item.verify_tgpd_glass_color == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_color_note').prop('checked', tgpd_item.verify_tgpd_glass_color_note == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_lift').prop('checked', tgpd_item.verify_tgpd_glass_lift == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').prop('checked', tgpd_item.verify_tgpd_glass_inside_lift_with_glass_type == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').prop('checked', tgpd_item.verify_tgpd_glass_outside_lift_with_glass_type == 1 ? true : false).addClass('verify-active');
	jQuery('#text_lift_inside_position').prop('checked', tgpd_item.text_tgpd_text_lift_inside_position == 1 ? true : false).addClass('verify-active');
	jQuery('#text_lift_outside_position').prop('checked', tgpd_item.text_tgpd_text_lift_outside_position == 1 ? true : false).addClass('verify-active');
	jQuery('#text_tgpd_fabrication_polished_edges_hide').prop('checked', tgpd_item.text_tgpd_fabrication_polished_edges_hide == 1 ? true : false).addClass('verify-active');
	jQuery('#text_tgpd_fabrication_pattern_hide').prop('checked', tgpd_item.text_tgpd_fabrication_pattern_hide == 1 ? true : false).addClass('verify-active');
	jQuery('#text_tgpd_fabrication_holes_hide_new').prop('checked', tgpd_item.text_tgpd_fabrication_holes_hide_new == 1 ? true : false).addClass('verify-active');
	jQuery('#text_tgpd_fabrication_holes_hide_old').prop('checked', tgpd_item.text_tgpd_fabrication_holes_hide_old == 1 ? true : false).addClass('verify-active');
	
	var text_lift_inside_position = tgpd_item.text_tgpd_text_lift_inside_position;
	if(text_lift_inside_position == '1'){
		jQuery("#text_lift_inside_position").attr('checked');
		jQuery('#lift_inside_hide_on_glass_type').hide();
	}
	//jQuery('#text_lift_outside_position').prop('checked', tgpd_item.text_tgpd_text_lift_outside_position == 1 ? true : false).addClass('');
	
	var text_lift_outside_position = tgpd_item.text_tgpd_text_lift_outside_position;
	if(text_lift_outside_position == '1'){
		jQuery("#text_lift_outside_position").attr('checked');
		jQuery('#lift_outside_hide_on_glass_type').hide();		
	}	
	
	var text_tgpd_fabrication_polished_edges_hide = tgpd_item.text_tgpd_fabrication_polished_edges_hide;
	if(text_tgpd_fabrication_polished_edges_hide == '1'){
		jQuery("#text_tgpd_fabrication_polished_edges_hide").attr('checked');
		jQuery('#text_tgpd_fabrication_polished_edges').hide();		
	}	
	
	var text_tgpd_fabrication_pattern_hide = tgpd_item.text_tgpd_fabrication_pattern_hide;
	if(text_tgpd_fabrication_pattern_hide == '1'){
		jQuery("#text_tgpd_fabrication_pattern_hide").attr('checked');
		jQuery('#text_tgpd_fabrication_pattern').hide();		
	}
   var text_tgpd_fabrication_gride_hide = tgpd_item.text_tgpd_fabrication_gride_hide;



   if(text_tgpd_fabrication_gride_hide == '1'){
      jQuery("#text_tgpd_fabrication_gride_hide").attr('checked');
      jQuery('#text_tgpd_fabrication_gride_hide').hide();      
   }
	
	var text_tgpd_fabrication_holes_hide_new = tgpd_item.text_tgpd_fabrication_holes_hide_new;
	if(text_tgpd_fabrication_holes_hide_new == '1'){
		jQuery("#text_tgpd_fabrication_holes_hide_new").attr('checked');
		jQuery('#glass_piece_holes_div').hide();		
	}
	
	var text_tgpd_fabrication_holes_hide_old = tgpd_item.text_tgpd_fabrication_holes_hide_old;
	if(text_tgpd_fabrication_holes_hide_old == '1'){
		jQuery("#text_tgpd_fabrication_holes_hide_old").attr('checked');
		jQuery('#text_tgpd_fabrication_holes').hide();		
	}	
	
	jQuery('#verify_tgpd_spacer_color').prop('checked', tgpd_item.verify_tgpd_spacer_color == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_daylight_height').prop('checked', tgpd_item.verify_tgpd_daylight_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_daylight_height').prop('checked', tgpd_item.verify_tgpd_daylight_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_daylight_width').prop('checked', tgpd_item.verify_tgpd_daylight_width == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_set_height').prop('checked', tgpd_item.verify_set_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_set_width').prop('checked', tgpd_item.verify_set_width == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_go_size_height').prop('checked', tgpd_item.verify_go_size_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_go_size_width').prop('checked', tgpd_item.verify_go_size_width == 1 ? true : false).addClass('verify-active');
	jQuery('#text_tgpd_extra_height').val(tgpd_item.text_tgpd_extra_height);
	jQuery('#text_tgpd_extra_width').val(tgpd_item.text_tgpd_extra_width);
   /*verify data from db to frontend code end at here date @02-11-17*/


   var tgpd_sag_or_quote = tgpd_item.sag_or_quote == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked', tgpd_sag_or_quote);


   tgpd_set_glass_pricing_sag_or_quote_state(tgpd_sag_or_quote, false);
   jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer').val(manufacturer);
   var tgpd_solar_film = tgpd_item.solar_film == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked', tgpd_solar_film);
   tgpd_set_glass_reminders_solar_film1_state(tgpd_solar_film, false);
   jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]').val([tgpd_item.solar_film_responsibility]);
   tgpd_set_glass_reminders_solar_film2_state(tgpd_item.solar_film_responsibility, false);
   jQuery('#text_tgpd_glass_reminders_solar_film_type').val(solar_film_type);
   jQuery('#text_tgpd_glass_reminders_solar_film_source').val(solar_film_source);
   var tgpd_wet_seal = tgpd_item.wet_seal == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked', tgpd_wet_seal);
   tgpd_set_glass_reminders_wet_seal_state(tgpd_wet_seal, false);
   jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]').val([tgpd_item.wet_seal_responsibility]);
   var tgpd_furniture_to_move = tgpd_item.furniture_to_move == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked', tgpd_furniture_to_move);
   tgpd_set_glass_reminders_furniture_to_move_state(tgpd_furniture_to_move, false);
  
   var tgpd_lift_inside_glass_section_reminder = tgpd_item.text_tgpd_text_lift_inside_position == 1 ? true : false;
   jQuery('#text_lift_inside_position').prop('checked', tgpd_lift_inside_glass_section_reminder);
   tgpd_set_lift_inside_glass_reminders(tgpd_lift_inside_glass_section_reminder, false); 
   
    var tgpd_lift_outside_glass_section_reminder = tgpd_item.text_tgpd_text_lift_outside_position == 1 ? true : false;
   jQuery('#text_lift_outside_position').prop('checked', tgpd_lift_outside_glass_section_reminder);
   tgpd_set_lift_outside_glass_reminders(tgpd_lift_outside_glass_section_reminder, false);
   var tgpd_disclamers = tgpd_item.disclamers == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_add_disclamers').prop('checked', tgpd_disclamers);
    tgpd_set_glass_reminders_disclamers_to_move_state(tgpd_disclamers, false);
	
	var tgpd_damage_waiver = tgpd_item.damage_viewer == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').prop('checked', tgpd_damage_waiver);
    tgpd_set_glass_reminders_damage_waiver_to_move_state(tgpd_damage_waiver, false);
	
	
	
	
   
   var tgpd_damage_waiver_move_to = tgpd_item.damage_viewer == 1 ? true : false ;
   jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').prop('checked',tgpd_damage_waiver_move_to);
   tgpd_set_glass_reminders_damage_waiver_state(tgpd_damage_waiver_move_to, false);
   jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment').val(furniture_to_move_comment);
   jQuery('#select_tgpd_glass_damage_waiver').val(tgpd_item.damage_waiver_select);
   jQuery('#select_tgpd_glass_disclamers').val(tgpd_item.disclamers_select);
   
   
   var select_damage = jQuery('#select_tgpd_glass_damage_waiver').val();
	if(select_damage=='Removal/Reinstall of Glass Currently Installed' || select_damage=='Handling of Customers Materials' || select_damage=='Adjacent Glass'){
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').hide();
	}else{
			jQuery('#text_tgpd_glass_damage_waiver_reminder_section').val(damage_waiver_text);
	}
   
   var select_disclaimers = jQuery('#select_tgpd_glass_disclamers').val();
   if(select_disclaimers == 'Wood Bead' || select_disclaimers == 'Painted Frames (AFTERMARKET)' || select_disclaimers == 'TBD'){
		jQuery('#fieldset_tgpd_glass_disclamers_reminder_section').hide();
   }else{
		jQuery('#text_tgpd_glass_disclamers_reminder_section').val(disclamers_text);
   }
   
   
 
 //  jQuery('#text_tgpd_glass_damage_waiver_reminder_section').val(tgpd_item.damage_waiver_text);
   //jQuery('#text_tgpd_glass_disclamers_reminder_section').val(tgpd_item.disclamers_text);
  
  
   
   var tgpd_damage_waiver_to_move = tgpd_item.damage_viewer == 1 ? true :false;
	jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').prop('checked' ,tgpd_damage_waiver_to_move );
	
	
	
   var tgpd_walls_or_ceilings_to_cut = tgpd_item.walls_or_ceilings_to_cut == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked', tgpd_walls_or_ceilings_to_cut);
   tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(tgpd_walls_or_ceilings_to_cut, false);
   jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]').val([tgpd_item.walls_or_ceilings_to_cut_responsibility]);
   tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_item.walls_or_ceilings_to_cut_responsibility, false);
   jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment').val(walls_or_ceilings_to_cut_comment);
   jQuery('#checkbox_tgpd_glass_reminders_blind_needs_removing').prop('checked', tgpd_item.blind_needs_removing == 1 ? true : false);
   jQuery('#checkbox_tgpd_glass_reminders_glass_fits_elevator').prop('checked', tgpd_item.glass_fits_elevator == 1 ? true : false);
   if (!tgpd_copying) {
	  
      if (tgpd_item.pictures_download_url.length > 0) tgpd_pictures_download_url = tgpd_item.pictures_download_url.split(',');
      tgpd_print_pictures_to_form();
      if (tgpd_item.sketches_download_url.length > 0) tgpd_sketches_download_url = tgpd_item.sketches_download_url.split(',');
      tgpd_print_sketches_to_form();
   }


   jQuery('#textarea_tgpd_instructions').val(instructions);
  
  		var tgpcf=	tgpd_current_item;
		
		
		/*holes new functionality start from here*/
		
		
		
	//	addAndUpdateHoles
		
		// $('.glass_piece_holes_div').html('');
		//$('#glass_piece1_holes_'+tgpd_current_item).remove();
		/*$('.chaulksection').prepend('<div class="form-group addqty"  id="add_quntity_'+tgpd_current_item+'"><div class="add_quntity"></div><div class="add_tap_scaff"></div><div class="add_tap_amount"></div><div class="add_tap_channel"></div></div>');*/
		
	/*	
if(tgpd_item.caulk_amount.length < tgpd_item.caulk_type.length){
	
	maxVal=tgpd_item.caulk_type.length;
}


for(var i=0;i<maxVal;i++){

		if((!!!tgpd_item.caulk_amount[i]) &&  tgpd_item.caulk_type[i]!='not_applicable'){
			
			addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
			currentPosition++;
		}
}
		*/		
		jQuery('#glass_piece_holes_div').html('');
		glassHolesCounter=0;
		var maxHolesLength=0;
		if(tgpd_item.glass_holes_type && !!tgpd_item.glass_holes_type.length){
		maxHolesLength =tgpd_item.glass_holes_type.length;
		}
		//alert(tgpd_item.glass_holes_type.length);
		if(tgpd_item.glass_holes_type && tgpd_item.glass_holes_type.length<tgpd_item.glass_holes_size.length){		
			maxHolesLength =tgpd_item.glass_holes_size.length;
		}
		
		if(tgpd_item.holes_x_location && maxHolesLength<tgpd_item.holes_x_location.length){maxHolesLength =tgpd_item.holes_x_location.length;		}
		
		if(tgpd_item.holes_y_location && maxHolesLength<tgpd_item.holes_y_location.length){maxHolesLength =tgpd_item.holes_y_location.length;		}
		
		if(tgpd_item.holes_x_location_cl && maxHolesLength<tgpd_item.holes_x_location_cl.length){maxHolesLength =tgpd_item.holes_x_location_cl.length;	}
		
		if(tgpd_item.holes_y_location_cl && maxHolesLength<tgpd_item.holes_y_location_cl.length){maxHolesLength =tgpd_item.holes_y_location_cl.length;	}
		
		if(tgpd_item.holes_notes && maxHolesLength<tgpd_item.holes_notes.length){			maxHolesLength =tgpd_item.holes_notes.length;		}
		
		
		
		
		//alert(maxHolesLength);
	var blankHole =0;
	for(var i=0;i<maxHolesLength;i++){
		
		
		
		addAndUpdateHoles(tgpd_item.glass_holes_type[i],tgpd_item.glass_holes_size[i],tgpd_item.holes_x_location[i],tgpd_item.holes_y_location[i],tgpd_item.holes_x_location_cl[i],tgpd_item.holes_y_location_cl[i],tgpd_item.holes_notes[i],i,true);
   
	}
	
	if(blankHole==i){
        
			addAndUpdateHoles('','','','','','','',0,true);
		
	}
		/*holes new functionality start from here*/
		
		var data_off = ''+tgpd_item.glass_offset ; 


      if(data_off != ''){

    off_data   = data_off.split(',');

    if(off_data.length > 0 ){

      $('#glass_piece_offset_div').html('');

       arr_length = (off_data.length/5);

       //alert(off_data.length);

      var i;
    for (i = 0; i < arr_length; i++) { 
      var mult = arr_length;
      var fir = i;
     var  sec = i+mult;
      var thr = i+mult+mult;
      var fr = i+mult+mult+mult;
      var fiv = i+mult+mult+mult+mult;
      addAndUpdateOffset(off_data[fir],off_data[sec],off_data[thr],off_data[fr],off_data[fiv],i);



    }


       $('#text_tgpd_fabrication_polished_edges_hide').prop('checked', true);
   $('#offset_data_frm').hide();
   $('#text_tgpd_fabrication_pattern_hide').prop('checked', true);
   $('#text_tgpd_fabrication_holes_hide_new').prop('checked', true);
   $('#text_tgpd_fabrication_offset_hide_new').prop('checked', true);
   $('#text_tgpd_fabrication_pattern_hide').prop('checked', true);
     $('#text_tgpd_fabrication_gride_hide').prop('checked', true);
   $('#text_tgpd_fabrication_polished_edges').hide();
   $('#text_tgpd_fabrication_polished_edges').hide();
   $('#text_tgpd_fabrication_pattern').hide();
   $('#glass_piece_holes_div').hide();
    
   } else {


       addAndUpdateOffset('','','','','',0);


   }
		}
		
		 $('.chaulksection').find('.addqty').remove('.addqty');
		$('#add_quntity_'+tgpd_current_item).remove();
		$('.chaulksection').prepend('<div class="form-group addqty"  id="add_quntity_'+tgpd_current_item+'"><div class="add_quntity"></div><div class="add_tap_amount"></div><div class="add_tap_scaff"></div><div class="add_tap_channel"></div></div>');

//if(culkAdded==false){
 /*if( typeof tgpd_item.caulk_amount != "undefined"
  && tgpd_item.caulk_amount != null
  && tgpd_item.caulk_amount.length != null
  && tgpd_item.caulk_amount.length > 0){*/
	var blank=0; var currentPosition=0;
	var maxVal =tgpd_item.caulk_amount.length;
	
	if(tgpd_item.caulk_amount.length < tgpd_item.caulk_type.length){
		
		maxVal=tgpd_item.caulk_type.length;
	}
	//alert(" maxVal "+maxVal);
	var i=0;
	for(i=0;i<maxVal;i++){
	
			if((!!!tgpd_item.caulk_amount[i]) &&  tgpd_item.caulk_type[i]!='not_applicable'){
				
				addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
				currentPosition++;
			
			}else if(!!tgpd_item.caulk_amount[i] &&  tgpd_item.caulk_type[i]=='not_applicable')
			{
				addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
				currentPosition++;
				
			}
			else if(!!tgpd_item.caulk_amount[i] &&  tgpd_item.caulk_type[i]!='not_applicable')
			{
				addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
				currentPosition++;
				
			}
			else{
				
				blank++;
		
			}
		
	}
	if(blank==i){

		addAndUpdateCulk( '0',  '', '',0,true );
		
	}
  
  
	
	
/*}else if((typeof tgpd_item.caulk_amount == "undefined"
  && tgpd_item.caulk_amount == null
  && tgpd_item.caulk_amount.length == null )
  || (tgpd_item.caulk_amount.length == 0)){
	 
	 
		addAndUpdateCulk( '',  '', '',0,true );
 }*/
 
 


var blank=0; var currentPosition=0;
	var maxVal =tgpd_item.scaffolding_type.length;
	
	if(tgpd_item.scaffolding_type.length < tgpd_item.quantity_type.length){
		
		maxVal=tgpd_item.quantity_type.length;
	}
	//alert(" maxVal "+maxVal);
	var i=0;
	for(i=0;i<maxVal;i++){
	
			if((!!!tgpd_item.quantity_type[i]) &&  tgpd_item.scaffolding_type[i]!='not_applicable'){
				
				//addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
				addAndUpdateScaffolding(tgpd_item.scaffolding_type[i],tgpd_item.quantity_type[i],currentPosition,true);
				currentPosition++;
			
			}else if(!!tgpd_item.quantity_type[i] &&  tgpd_item.scaffolding_type[i]=='not_applicable')
			{
				//addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
				addAndUpdateScaffolding(tgpd_item.scaffolding_type[i],tgpd_item.quantity_type[i],currentPosition,true);
				currentPosition++;
				
			}
			else if(!!tgpd_item.quantity_type[i] &&  tgpd_item.scaffolding_type[i]!='not_applicable')
			{
				//addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
				addAndUpdateScaffolding(tgpd_item.scaffolding_type[i],tgpd_item.quantity_type[i],currentPosition,true);
				currentPosition++;
				
			}
			else{
				
				blank++;
		
			}
		
	}
	if(blank==i){

		addAndUpdateScaffolding( '0',  '0', 0,true );
		
	}
	
	
	

 	//addAndUpdateChannel();addAndUpdateScaffolding();
 
//culkAdded=true;
//}
	//jQuery('#text_tgpd_glass_materials_caulk_amount').val(tgpd_item.caulk_amount); 
  // jQuery('#select_tgpd_glass_materials_caulk_type').val([tgpd_item.caulk_type]);
  // jQuery('#select_tgpd_glass_materials_scaffolding_type').val([tgpd_item.scaffolding_type]);
  
  // if(tapeAdded==false){
 /*if(typeof tgpd_item.tape_amount != "undefined"
  && tgpd_item.tape_amount != null
  && tgpd_item.tape_amount.length != null
  && tgpd_item.tape_amount.length > 0){*/
	  var blank=0; var currentPosition=0;
	  var maxVal = tgpd_item.tape_amount.length;
	  if(tgpd_item.tape_amount.length < tgpd_item.tape_type.length){
		
		maxVal=tgpd_item.caulk_type.length;
	}
	
	 for(var i=0;i<maxVal;i++){
			
		//	if((!!tgpd_item.tape_amount[i]) || tgpd_item.tape_type[i] ){
			
			if((!!!tgpd_item.tape_amount[i]) &&  tgpd_item.tape_type[i]!='not_applicable'){
				
				addAndUpdateTape(tgpd_item.tape_amount[i],tgpd_item.tape_type[i],tgpd_item.channel[i],currentPosition,true);
				currentPosition++;
			
			}else if(!!tgpd_item.tape_amount[i] &&  tgpd_item.tape_type[i]=='not_applicable')
			{
				addAndUpdateTape(tgpd_item.tape_amount[i],tgpd_item.tape_type[i],tgpd_item.channel[i],currentPosition,true);
				currentPosition++;
				
			}
			else if(!!tgpd_item.tape_amount[i] &&  tgpd_item.tape_type[i]!='not_applicable')
			{
				addAndUpdateTape(tgpd_item.tape_amount[i],tgpd_item.tape_type[i],tgpd_item.channel[i],currentPosition,true);
				currentPosition++;
				
			}
			else{
				blank++;
				
			}
			 
	
	}
	
	 
	if(blank==i){
		addAndUpdateTape('0', '', '',0,true);
		
	}
 /*}else if((typeof tgpd_item.tape_amount == "undefined"
  && tgpd_item.tape_amount == null
  && tgpd_item.tape_amount.length == null)
  || ( tgpd_item.tape_amount.length == 0)){
	 
	 addAndUpdateTape('', '', '',0,true);
	
 }else{
	 
	 	addAndUpdateTape('', '', '',0,true);
 }*/
 
  /*********old_code_cannle******/
  /*
  if( typeof tgpd_item.channel != "undefined"
  && tgpd_item.channel != null ){
	  var blank=0;var currentPosition=0;
	for(var i=0;i<tgpd_item.channel.length;i++){
			
			if(!!tgpd_item.channel[i]){
				var channel_type = tgpd_item.channel[i];
				channel_type = channel_type.replace(/\\/g, "");
				channel_type = channel_type.replace(/_/g, "");
				channel_type = channel_type.replace(/'/g, '');
				channel_type = channel_type.replace(/\//g, "");
					addAndUpdateChannel(channel_type,currentPosition,true);
					currentPosition++;
			}else{
				blank++;
				
			}
		
		
	}
	
	if(blank==i){
      addAndUpdateChannel( '',0,true );
		
	}
}else if(typeof tgpd_item.channel == "undefined"
  && tgpd_item.channel == null){
	 
	 
      addAndUpdateChannel( '',0,true );
 }
*/
  /*********old_code_cannle******/




var blank=0; var currentPosition=0;
   var maxVal =tgpd_item.channel.length;


   //alert(" maxVal_0 "+maxVal);
   
   if(tgpd_item.channel.length < tgpd_item.quantity_channel.length){
      
      maxVal=tgpd_item.quantity_channel.length;
   }
   //alert(" maxVal "+maxVal);
   var i=0;
   for(i=0;i<maxVal;i++){
   
         if((!!!tgpd_item.quantity_channel[i]) &&  tgpd_item.channel[i]!='not_applicable'){
            
            //addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
            addAndUpdateChannel(tgpd_item.channel[i],tgpd_item.quantity_channel[i],currentPosition,true);
            currentPosition++;
         
         }else if(!!tgpd_item.quantity_channel[i] &&  tgpd_item.channel[i]=='not_applicable')
         {

            
            //addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
            addAndUpdateChannel(tgpd_item.channel[i],tgpd_item.quantity_channel[i],currentPosition,true);
            currentPosition++;
            
         }
         else if(!!tgpd_item.quantity_channel[i] &&  tgpd_item.channel[i]!='not_applicable')
         {
            
             
            //addAndUpdateCulk(tgpd_item.caulk_amount[i],tgpd_item.caulk_type[i],'',currentPosition,true);
            addAndUpdateChannel(tgpd_item.channel[i],tgpd_item.quantity_channel[i],currentPosition,true);
            currentPosition++;
            
         }
         else{
            
            blank++;
      
         }
      
   }
   if(blank==i){

      
			addAndUpdateChannel( '0',  '0', 0,true );
      
 }
 






 
 //tapeAdded=true;
  // }
 // jQuery('#text_tgpd_glass_materials_tape_amount').val(tgpd_item.tape_amount);   
//   jQuery('#select_tgpd_glass_materials_tape_type').val([tgpd_item.tape_type]);
 //  jQuery('#text_tgpd_glass_materials_channel').val(tgpd_item.channel);
 
 
   jQuery('#text_tgpd_miscellaneous').val(miscellaneous);
   var tgpd_glass_grids = tgpd_item.glass_grids == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_grids').prop('checked', tgpd_glass_grids);
   tgpd_set_glass_grids_state(tgpd_glass_grids, false);
   jQuery('#number_tgpd_pattern_horizontal_grids').val(tgpd_item.pattern_horizontal_grids);
   jQuery('#number_tgpd_pattern_vertical_grids').val(tgpd_item.pattern_vertical_grids);
   jQuery('#select_tgpd_grids_thickness').val([tgpd_item.grids_thickness]);
   jQuery('#text_tgpd_grids_color').val(grids_color);
   /*sag-158*/
   if(grids_color_custom == 'custom'){
      jQuery('#text_tgpd_grids_color_custom').show();
      jQuery('.text_tgpd_grids_color_custom_label').show();
   jQuery('#text_tgpd_grids_color_custom').val(grids_color_custom);

   } 
   /*sag-158*/
   var tgpd_glass_fabrication = tgpd_item.glass_fabrication == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_fabrication').prop('checked', tgpd_glass_fabrication);
   tgpd_set_glass_fabrication_state(tgpd_glass_fabrication, false);
   jQuery('#text_tgpd_fabrication_polished_edges').val(fabrication_polished_edges);
   jQuery('#text_tgpd_fabrication_holes').val(fabrication_holes);
   jQuery('#text_tgpd_fabrication_pattern').val(fabrication_pattern);
 
      var daylight_enable  = tgpd_item.daylight_enable  == 1 ? true : false;
      var coating_position_enable  = tgpd_item.coating_position_enable  == 1 ? true : false;
 jQuery('#text_tgpd_daylight').trigger("change").attr("checked",daylight_enable);
   if(tgpd_item.daylight_enable  == 1)
   {
		//$("#daylightsetglasspiece").hide();
   }
   else
   {
	//	$("#daylightsetglasspiece").show();
   }
   
   jQuery('#text_tgpd_coating_first_position').trigger("change").attr("checked",coating_position_enable);
   if(tgpd_item.coating_position_enable == 1)
   {
		$("#text_tgpd_coating_exterior_type").show();
   }
   else
   {
		$("#text_tgpd_coating_exterior_type").show();
   }
   
   jQuery('#text_tgpd_coating_second_position').trigger("change").attr("checked",coating_position_enable1);
   if(tgpd_item.coating_position_enable1 == 1)
   {
		$("#text_tgpd_coating_interior_type").show();
   }
   else
   {
		$("#text_tgpd_coating_interior_type").show();
   }
   
   
   

    var gosize_enable   = tgpd_item.gosize_enable   == 1 ? true : false;
	 jQuery('#text_tgpd_gosize').trigger("change").attr("checked",gosize_enable);

    if(tgpd_item.gosize_enable  == 1)
   {
		// $("#div_tgpd_go_size").css("display","none");
   }
   else
   {
	//	$("#div_tgpd_go_size").css("display","block");
   }

 var daylight_enable  = tgpd_item.daylight_enable  == 1 ? true : false;
	$('#text_tgpd_daylight').trigger("change").attr("checked",daylight_enable);
   $('#text_tgpd_daylight').trigger("change");
   
  var coating_position_enable  = tgpd_item.coating_position_enable  == 1 ? true : false;
 $('#text_tgpd_coating_first_position').trigger("change").attr("checked",coating_position_enable);
   $('#text_tgpd_coating_first_position').trigger("change"); 
   
   var coating_position_enable1  = tgpd_item.coating_position_enable1  == 1 ? true : false;
	$('#text_tgpd_coating_second_position').trigger("change").attr("checked",coating_position_enable1);
   $('#text_tgpd_coating_second_position').trigger("change"); 
   
    var gosize_enable   = tgpd_item.gosize_enable   == 1 ? true : false;
	$('#text_tgpd_gosize').trigger("change").attr("checked",gosize_enable);
   $('#text_tgpd_gosize').trigger("change");
    var color_viewer   = tgpd_item.color_viewer   == "add_color_waiver" ? true : false;
 jQuery('#checkbox_tgpd_glass_reminders_add_color_waiver').attr("checked",color_viewer);

    var damage_viewer   = tgpd_item.damage_viewer   == "damage_waiver" ? true : false;
	jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').attr("checked",damage_viewer);
	if(tgpd_item.damage_viewer=='damage_waiver'){
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section_for_select').show();
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').show();
	}
	
	if(back_date_entry ==true){ 
		jQuery("#extra_type_coating_treatment").hide();
		jQuery("#extra_type_coating_treatment_second").attr("display","none !important");

	}
	
	if($('#select_tgpd_glass_damage_waiver').val() == 'select_tgpd_glass_damage_waiver_for_text'){
		$('#text_tgpd_glass_damage_waiver_reminder_section').show();
	}
	
	if($('#select_tgpd_glass_disclamers').val() == 'select_tgpd_glass_disclamers_for_text'){
		$('#text_tgpd_glass_disclamers_reminder_section').show();
	}
	
}

function tgpd_print_pictures_to_form() {
   if (tgpd_pictures_download_url.length == 0) return;
   var $tgpd_picture_filelist = jQuery('#div_tgpd_picture_filelist');
   var $tgpd_picture_filelist_controls = jQuery("#div_tgpd_picture_filelist_controls");
   var $picture_list = jQuery(tgpd_picture_list);
   $tgpd_picture_filelist.append($picture_list);
   tgpd_pictures_download_url.forEach(function(element, index, array) {
      var $picture_list_item = jQuery(tgpd_picture_list_item);
      var $image = jQuery('img', $picture_list_item);
      var $info = jQuery('.info', $picture_list_item);
      var $progress_bar = jQuery('.progressBar', $picture_list_item);
      $image.attr('src', element);
      var start = element.lastIndexOf('/');
      var file_name = element.substr((start + 1));
      $info.html(file_name);
      $progress_bar.closest('.progressHolder').hide();
      $picture_list.append($picture_list_item);
   });
   $tgpd_picture_filelist_controls.show();
   $tgpd_picture_filelist.click(function(event) {
      tgpd_toggle_picture_filelist_controls(event, this);
   });
}


function tgpd_print_sketches_to_form() {


   if (tgpd_sketches_download_url.length == 0) return;
   var $tgpd_sketch_filelist = jQuery('#div_tgpd_sketch_filelist');
   var $tgpd_sketch_filelist_controls = jQuery("#div_tgpd_sketch_filelist_controls");
   var $sketch_list = jQuery(tgpd_sketch_list);
   $tgpd_sketch_filelist.append($sketch_list);
   tgpd_sketches_download_url.forEach(function(element, index, array) {
      var $sketch_list_item = jQuery(tgpd_sketch_list_item);
      var $image = jQuery('img', $sketch_list_item);
      var $info = jQuery('.info', $sketch_list_item);
      var $progress_bar = jQuery('.progressBar', $sketch_list_item);
      $image.attr('src', element);
      var start = element.lastIndexOf('/');
      var file_name = element.substr((start + 1));
      $info.html(file_name);
      $progress_bar.closest('.progressHolder').hide();
      $sketch_list.append($sketch_list_item);
   });
   $tgpd_sketch_filelist_controls.show();
   $tgpd_sketch_filelist.click(function(event) {
      tgpd_toggle_sketch_filelist_controls(event, this);
   });
}

function tgpd_print_item_to_console(debug_tag, tgpd_item) {console.log(tgpd_item);
   var log_tag = (debug_tag.length == 0 ? 'tgpd_print_item_to_console: ' : ('tgpd_print_item_to_console:' + debug_tag + ': '));
   console.log(log_tag + 'tgpd_id => ' + tgpd_item.id);
   console.log(log_tag + 'tgpd_job_id => ' + tgpd_item.job_id);
   console.log(log_tag + 'tgpd_glass_location => ' + tgpd_item.glass_location);
   console.log(log_tag + 'tgpd_coating_exterior_type => ' + tgpd_item.coating_exterior_type);
   console.log(log_tag + 'tgpd_coating_interior_type => ' + tgpd_item.coating_interior_type);
   console.log(log_tag + 'tgpd_glass_type => ' + tgpd_item.glass_type);
   console.log(log_tag + 'tgpd_lite_thickness => ' + tgpd_item.lite_thickness);
   console.log(log_tag + 'tgpd_spacer_color => ' + tgpd_item.spacer_color);
   console.log(log_tag + 'tgpd_glass_treatment => ' + tgpd_item.glass_treatment);
   console.log(log_tag + 'tgpd_glass_color => ' + tgpd_item.glass_color);
   console.log(log_tag + 'tgpd_glass_color_note => ' + tgpd_item.glass_color_note);
   console.log(log_tag + 'tgpd_glass_lift => ' + tgpd_item.glass_lift);
   console.log(log_tag + 'tgpd_glass_set => ' + tgpd_item.glass_set);
   console.log(log_tag + 'tgpd_daylight_width => ' + tgpd_item.daylight_width);
   console.log(log_tag + 'tgpd_daylight_height => ' + tgpd_item.daylight_height);
   console.log(log_tag + 'tgpd_go_width => ' + tgpd_item.go_width);
   console.log(log_tag + 'tgpd_go_height => ' + tgpd_item.go_height);
   console.log(log_tag + 'tgpd_size_verification_status => ' + tgpd_item.size_verification_status);
   console.log(log_tag + 'tgpd_overall_thickness => ' + tgpd_item.overall_thickness);
   console.log(log_tag + 'tgpd_sag_or_quote => ' + tgpd_item.sag_or_quote);
   console.log(log_tag + 'tgpd_manufacturer => ' + tgpd_item.manufacturer);
   console.log(log_tag + 'tgpd_solar_film => ' + tgpd_item.solar_film);
   console.log(log_tag + 'tgpd_solar_film_responsibility => ' + tgpd_item.solar_film_responsibility);
   console.log(log_tag + 'tgpd_solar_film_type => ' + tgpd_item.solar_film_type);
   console.log(log_tag + 'tgpd_solar_film_source => ' + tgpd_item.solar_film_source);
   console.log(log_tag + 'tgpd_wet_seal => ' + tgpd_item.wet_seal);
   console.log(log_tag + 'tgpd_wet_seal_responsibility => ' + tgpd_item.wet_seal_responsibility);
   console.log(log_tag + 'tgpd_furniture_to_move => ' + tgpd_item.furniture_to_move);
   console.log(log_tag + 'tgpd_furniture_to_move_comment => ' + tgpd_item.furniture_to_move_comment);
   console.log(log_tag + 'tgpd_walls_or_ceilings_to_cut => ' + tgpd_item.walls_or_ceilings_to_cut);
   console.log(log_tag + 'tgpd_walls_or_ceilings_to_cut_responsibility => ' + tgpd_item.walls_or_ceilings_to_cut_responsibility);
   console.log(log_tag + 'tgpd_walls_or_ceilings_to_cut_comment => ' + tgpd_item.walls_or_ceilings_to_cut_comment);
   console.log(log_tag + 'tgpd_blind_needs_removing => ' + tgpd_item.blind_needs_removing);
   console.log(log_tag + 'tgpd_glass_fits_elevator => ' + tgpd_item.glass_fits_elevator);
   console.log(log_tag + 'tgpd_pictures_download_url => ' + tgpd_item.pictures_download_url);
   console.log(log_tag + 'tgpd_sketches_download_url => ' + tgpd_item.sketches_download_url);
   console.log(log_tag + 'tgpd_instructions => ' + tgpd_item.instructions);
   console.log(log_tag + 'tgpd_caulk_amount => ' + tgpd_item.caulk_amount);
   console.log(log_tag + 'tgpd_caulk_type => ' + tgpd_item.caulk_type);
   console.log(log_tag + 'tgpd_scaffolding_type => ' + tgpd_item.scaffolding_type);
   console.log(log_tag + 'tgpd_quantity_type => ' + tgpd_item.quantity_type);
   console.log(log_tag + 'tgpd_tape_amount => ' + tgpd_item.tape_amount);
   console.log(log_tag + 'tgpd_tape_type => ' + tgpd_item.tape_type);
   console.log(log_tag + 'tgpd_channel => ' + tgpd_item.channel);
   console.log(log_tag + 'tgpd_miscellaneous => ' + tgpd_item.miscellaneous);
   console.log(log_tag + 'tgpd_glass_grids => ' + tgpd_item.glass_grids);
   console.log(log_tag + 'tgpd_pattern_horizontal_grids => ' + tgpd_item.pattern_horizontal_grids);
   console.log(log_tag + 'tgpd_pattern_vertical_grids => ' + tgpd_item.pattern_vertical_grids);
   console.log(log_tag + 'tgpd_grids_thickness => ' + tgpd_item.grids_thickness);
   console.log(log_tag + 'tgpd_grids_color => ' + tgpd_item.grids_color);
  /*sag-158*/
   console.log(log_tag + 'tgpd_grids_color_custom => ' + tgpd_item.grids_color_custom);
   /*sag-158*/
   console.log(log_tag + 'tgpd_glass_fabrication => ' + tgpd_item.glass_fabrication);
   console.log(log_tag + 'tgpd_fabrication_polished_edges => ' + tgpd_item.fabrication_polished_edges);
   console.log(log_tag + 'tgpd_fabrication_holes => ' + tgpd_item.fabrication_holes);
   console.log(log_tag + 'tgpd_fabrication_pattern => ' + tgpd_item.fabrication_pattern);
   console.log(log_tag + 'tgpd_exterior_glass_type => ' + tgpd_item.exterior_glass_type);
   console.log(log_tag + 'tgpd_interior_glass_type => ' + tgpd_item.interior_glass_type);
   console.log(log_tag + 'tgpd_treatment_exterior_glass => ' + tgpd_item.treatment_exterior_glass);
   console.log(log_tag + 'tgpd_treatment_interior_glass => ' + tgpd_item.treatment_interior_glass);
   console.log(log_tag + 'tgpd_exterior_color_glass_type => ' + tgpd_item.exterior_color_glass_type);
   console.log(log_tag + 'tgpd_interior_color_glass_type => ' + tgpd_item.interior_color_glass_type);
   // console.log(log_tag + 'tgpd_item => ' + JSON.stringify(tgpd_item));
}

function tgpd_init(tgpd_seed, tgpd_seed_items) {
	jQuery('.shape-container').hide();
   tgpd_current_item = -1;
   tgpd_items = [];
   tgpd_pictures_download_url = [];
   tgpd_sketches_download_url = [];
   tgpd_init_form_state();
   if (tgpd_seed && tgpd_seed_items.length > 0) {
      tgpd_current_item = 0;
      tgpd_items = tgpd_seed_items;
      tgpd_init_toolbar_navigation_buttons_state();
      tgpd_init_toolbar_editing_buttons_state();
      tgpd_show_current_item();
	  tgpd_editingsave=true;
   }
   tgpd_set_toolbar_navigation_buttons_state();
   tgpd_editing = false;
   tgpd_copying = false;
   tgpd_copy_operation = -1;
   tgpd_clipboard = {};
   tgpd_set_toolbar_editing_buttons_state();
}

function tgpd_s3_upload_progress(event, $progress_bar) {
   var percent_complete_int = parseInt((event.loaded * 100) / event.total);
   var percent_complete_str = percent_complete_int + '%';
   if ($progress_bar) {
      $progress_bar.css('width', percent_complete_str);
   }
}

function tgpd_s3_upload_object(file,imagecontent, $progress_bar, callback) {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
	  var salesmanid = jQuery('#currentuserid').val();
	  var quoteNumber = jQuery('#text_quote_number').val();
	  var unix = Math.round(+new Date()/1000);
	  var fileArray = file.name.split('.');
	  var updatedfilename = fileArray[0]+'_'+salesmanid+'_'+unix+'_'+quoteNumber+'.'+fileArray[1];
      var params = {Key: updatedfilename, ContentType: file.type, Body: imagecontent, ACL: "public-read"};
      var upload = s3.upload(params);
      upload.on('httpUploadProgress', function(event) {
         tgpd_s3_upload_progress(event, $progress_bar);
      });
      upload.send(function (err, data) {
         if (err) {
            console.log('tgpd_s3_upload_object: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            console.log('tgpd_s3_upload_object: data.Location -> ' + data.Location + ', data.Bucket -> ' + data.Bucket + ', data.Key -> ' + data.Key);
         }
         callback(err, data);
      });
   });
}
function tgpd_s3_upload_object_sketch(file,$progress_bar, callback) {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
	  var salesmanid = jQuery('#currentuserid').val();
	  var quoteNumber = jQuery('#text_quote_number').val();
	  var unix = Math.round(+new Date()/1000);
	  var fileArray = file.name.split('.');
	  var updatedfilename = fileArray[0]+'_'+salesmanid+'_'+unix+'_'+quoteNumber+'.'+fileArray[1];
      var params = {Key: updatedfilename, ContentType: file.type, Body: file, ACL: "public-read"};
      var upload = s3.upload(params);
      upload.on('httpUploadProgress', function(event) {
         tgpd_s3_upload_progress(event, $progress_bar);
      });
      upload.send(function (err, data) {
         if (err) {
            console.log('tgpd_s3_upload_object: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            console.log('tgpd_s3_upload_object: data.Location -> ' + data.Location + ', data.Bucket -> ' + data.Bucket + ', data.Key -> ' + data.Key);
         }
         callback(err, data);
      });
   });
}

function tgpd_s3_delete_object(object_key) {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
      var params = {Bucket: aws_s3_bucket, Key: object_key};
      s3.deleteObject(params, function(err, data) {
         if (err) {
            console.log('tgpd_s3_delete_object: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            console.log('tgpd_s3_delete_object: data.DeleteMarker -> ' + data.DeleteMarker + ', data.VersionId -> ' + data.VersionId + ', data.RequestCharged -> ' + data.RequestCharged);
         }
      });
   });
}

function tgpd_s3_get_bucket_cors() {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
      var params = {Bucket: aws_s3_bucket};
      s3.getBucketCors(params, function(err, data) {
         if (err) {
            console.log('tgpd_s3_get_bucket_cors: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            var allowedHeaders = data.CORSRules[0]['AllowedHeaders'];
            console.log('tgpd_s3_get_bucket_cors: allowedHeaders');
            allowedHeaders.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var allowedMethods = data.CORSRules[0]['AllowedMethods'];
            console.log('tgpd_s3_get_bucket_cors: allowedMethods');
            allowedMethods.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var allowedOrigins = data.CORSRules[0]['AllowedOrigins'];
            console.log('tgpd_s3_get_bucket_cors: allowedOrigins');
            allowedOrigins.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var exposeHeaders = data.CORSRules[0]['ExposeHeaders'];
            console.log('tgpd_s3_get_bucket_cors: exposeHeaders');
            exposeHeaders.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var maxAgeSeconds = data.CORSRules[0]['MaxAgeSeconds'];
            console.log('tgpd_s3_get_bucket_cors: maxAgeSeconds -> ' + maxAgeSeconds);
         }
      });
   });
}

function tgpd_s3_get_bucket_acl() {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
      var params = {Bucket: aws_s3_bucket};
      s3.getBucketAcl(params, function(err, data) {
         if (err) {
            console.log('tgpd_s3_get_bucket_acl: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            var displayName = data.Owner['DisplayName'];
            console.log('tgpd_s3_get_bucket_acl: Owner.displayName -> ' + displayName);

            var iD = data.Owner['ID'];
            console.log('tgpd_s3_get_bucket_acl: Owner.ID -> ' + iD);

            data.Grants.forEach(function (grant, index, array) {
               var grantee = grant['Grantee'];
               var grantee_display_name = grantee['DisplayName'];
               var grantee_email_address = grantee['EmailAddress'];
               var grantee_id = grantee['ID'];
               var grantee_type = grantee['Type'];
               var grantee_uri = grantee['URI'];
               var grantee_permission = grant['Permission'];
               console.log('tgpd_s3_get_bucket_acl: data.Grants[' + index + ']: grantee_display_name -> ' + grantee_display_name + ', grantee_email_address -> ' + grantee_email_address + ', grantee_id -> ' + grantee_id + ', grantee_type -> ' + grantee_type + ', grantee_uri -> ' + grantee_uri + ', grantee_permission -> ' + grantee_permission);
            });
         }
      });
   });
}

function tgpd_toggle_canvas_controls(event, target) {
   if (event.target == target) {
      jQuery('#div_tgpd_canvas_controls').toggle();
   }
}

function tgpd_toggle_canvas_holder(event, target) {
   jQuery('#div_tgpd_canvas_holder').slideToggle(300, 'easeInOutSine', function() {
      if (jQuery(target).is(':hidden')) {
         jQuery('#div_tgpd_canvas_controls').hide();
      } else {
         jQuery('#div_tgpd_canvas_controls').show();
      }
   });
   jQuery(target).toggleClass('canvas-close');
}


//krishan notes jQuery start//

function tgpd_toggle_canvas_holder_notes(event, target) {
   jQuery('#div_usg_canvas_holder_notes').slideToggle(300, 'easeInOutSine', function() {
      if (jQuery(target).is(':hidden')) {
         jQuery('#div_usg_canvas_holder_notes').hide();
      } else {
         jQuery('#div_usg_canvas_controls_notes').show();
      }
   });
   jQuery(target).toggleClass('canvas-close');
}

//krishan notes jQuery end//

function tgpd_show_canvas_holder(event) {
	var $tgpd_canvas_holder = jQuery('#div_tgpd_canvas_holder');

   if (!$tgpd_canvas_holder.is(':hidden')) {
      $tgpd_canvas_holder.slideDown(300, 'easeInOutSine', function() {
         jQuery('#div_tgpd_canvas_controls').show();
      });
      jQuery('#canvas-wrap h5').addClass('canvas-close');
   }
}


function tgpd_hide_canvas_holder(event) {
   var $tgpd_canvas_holder = jQuery('#div_usg_canvas_holder_notes');
   if (!$tgpd_canvas_holder.is(':hidden')) {
      $tgpd_canvas_holder.slideUp(300, 'easeInOutSine', function() {
         jQuery('#div_usg_canvas_controls_notes').hide();
      });
      jQuery('#canvas-wrap h5').removeClass('canvas-close');
   }
}



function tgpd_toggle_picture_filelist_controls(event, target) {
   if (event.target == target) {
      jQuery('#div_tgpd_picture_filelist_controls').toggle();
   }
}

function tgpd_toggle_picture_filelist_controls_notes(event, target) {
	   if (event.target == target) {
      jQuery('#div_upg_picture_filelist_controls_notes').toggle();
   }
}

function tgpd_toggle_sketch_filelist_controls(event, target) {
   if (event.target == target) {
      jQuery('#div_tgpd_sketch_filelist_controls').toggle();
	  jQuery('#btn_tgpd_upload_sketch_filelist').removeAttr('disabled');
	jQuery('#btn_tgpd_upload_sketch_filelist').removeClass('cust-disable');
   }
}

function tgpd_clear_glass_picture_filelist(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var $tgpd_picture_filelist = jQuery('#div_tgpd_picture_filelist');
   var $tgpd_picture_filelist_controls = jQuery("#div_tgpd_picture_filelist_controls");
   $tgpd_picture_filelist_controls.hide();
   $tgpd_picture_filelist.html('');
   $tgpd_picture_filelist.off('click');
   if (tgpd_pictures_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_pictures_download_url.forEach(function(element, index, array) {
         console.log('tgpd_clear_glass_picture_filelist: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   tgpd_pictures_download_url = [];
}

function tgpd_clear_glass_sketch_filelist(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var tgpd_option_clear_canvas = typeof tgpd_options.clear_canvas === 'undefined' ? false : tgpd_options.clear_canvas;
   var tgpd_option_hide_canvas = typeof tgpd_options.hide_canvas === 'undefined' ? false : tgpd_options.hide_canvas;
   var $tgpd_sketch_filelist = jQuery('#div_tgpd_sketch_filelist');
   var $tgpd_sketch_filelist_controls = jQuery("#div_tgpd_sketch_filelist_controls");
   $tgpd_sketch_filelist_controls.hide();
   $tgpd_sketch_filelist.html('');
   $tgpd_sketch_filelist.off('click');
   if (tgpd_sketches_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_sketches_download_url.forEach(function(element, index, array) {
         console.log('tgpd_clear_glass_sketch_filelist: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   tgpd_sketches_download_url = [];
   if (tgpd_option_clear_canvas) tgpd_clear_canvas();
   if (tgpd_option_hide_canvas) tgpd_hide_canvas_holder();
}
//krishan clear list start//

function tgpd_clear_glass_sketch_filelist_notes(tgpd_options) {

   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var tgpd_option_clear_canvas = typeof tgpd_options.clear_canvas === 'undefined' ? false : tgpd_options.clear_canvas;
   var tgpd_option_hide_canvas = typeof tgpd_options.hide_canvas === 'undefined' ? false : tgpd_options.hide_canvas;
   var $tgpd_sketch_filelist = jQuery('#div_usg_sketch_filelist_notes');
   var $tgpd_sketch_filelist_controls = jQuery("#div_usg_sketch_filelist_controls_notes");
   $tgpd_sketch_filelist_controls.hide();
   $tgpd_sketch_filelist.html('');
   $tgpd_sketch_filelist.off('click');
   if (tgpd_sketches_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_sketches_download_url.forEach(function(element, index, array) {
         console.log('div_usg_sketch_filelist_notes: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   tgpd_sketches_download_url = [];
   if (tgpd_option_clear_canvas) tgpd_clear_canvas();
   if (tgpd_option_hide_canvas) tgpd_hide_canvas_holder();
}
//krishan clear list end



function tgpd_upload_glass_picture_filelist() {

   var $imgs = jQuery('.picture_obj');
   var imgCOunter = tgpd_pictures_download_url.length+1;
   $imgs.each(function(index) {

      var self = this;
      var file = this.file;
      var $progress_bar = this.progress;
      var $status = this.status;

      $progress_bar.css('width', '0%');
      $status.removeClass('success failure');
      console.log('tgpd_upload_glass_picture_filelist: file.name => ' + file.name + ', file.size => ' + file.size + ', file.type => ' + file.type + ', file.lastModifiedDate => ' + file.lastModifiedDate);
	  var resize = new window.resize();
	  resize.init();
	  resize.photo(file, 1200, 'file', function (resizedFile) {
				tgpd_s3_upload_object(file,resizedFile, $progress_bar, function(err, data) {
				 if (!err) {
					$status.addClass('success');
					$progress_bar.css('width', '100%');
					jQuery(self).removeClass('picture_obj');
					
					//tgpd_pictures_download_url_object[index] = data.Location;
					if(imgCOunter== 1){
						tgpd_pictures_download_url[index] = data.Location;
					}else{
						tgpd_pictures_download_url[imgCOunter+index] = data.Location;
						}
					console.log('tgpd_upload_glass_picture_filelist: data.Location => ' + data.Location + ', tgpd_pictures_download_url => ' + tgpd_pictures_download_url.toString());
				 } else {
					$status.addClass('failure');
					$progress_bar.css('width', '0%');
				 }
			});

		});
   });

}

function tgpd_upload_glass_sketch_filelist() {
   var $imgs = jQuery('.sketch_obj');
   var imgCOunter = tgpd_sketches_download_url.length+1;
   $imgs.each(function(index) {

      var self = this;
      var file = this.file;
      var $progress_bar = this.progress;
      var $status = this.status;
      $progress_bar.css('width', '0%');
      $status.removeClass('success failure');
      console.log('tgpd_upload_glass_sketch_filelist: file.name => ' + file.name + ', file.size => ' + file.size + ', file.type => ' + file.type + ', file.lastModifiedDate => ' + file.lastModifiedDate);
      tgpd_s3_upload_object_sketch(file, $progress_bar, function(err, data) {
         if (!err) {
            $status.addClass('success');
            $progress_bar.css('width', '100%');
            jQuery(self).removeClass('sketch_obj');
			if(imgCOunter== 1){
            	tgpd_sketches_download_url[index] = data.Location;
			}else{
				tgpd_sketches_download_url[imgCOunter+index] = data.Location;
				}
            console.log('tgpd_upload_glass_sketch_filelist: data.Location => ' + data.Location + ', tgpd_sketches_download_url => ' + tgpd_sketches_download_url.toString());
         } else {
            $status.addClass('failure');
            $progress_bar.css('width', '0%');
         }
      });
   });


}

function S4() {
   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}

function tgpd_clear_canvas(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_hide_toolbar = typeof tgpd_options.hide_toolbar === 'undefined' ? false : tgpd_options.hide_toolbar;
   var $tgpd_glass_sketch = jQuery("#canvas_tgpd_glass_sketch");
   var canvas = $tgpd_glass_sketch[0];
   var context = canvas.getContext("2d");
   $tgpd_glass_sketch.sketch('stopPainting');
   $tgpd_glass_sketch.sketch('actions', []);
   context.clearRect(0, 0, canvas.width, canvas.height);
   context.fillStyle = '#FFFFFF';
   context.fillRect(0, 0, canvas.width, canvas.height);
   if (tgpd_hide_toolbar) jQuery("#div_tgpd_canvas_controls").hide();
}

// krishan clear_canvas_notes start//
function tgpd_clear_canvas_notes(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_hide_toolbar = typeof tgpd_options.hide_toolbar === 'undefined' ? false : tgpd_options.hide_toolbar;
   var $tgpd_glass_sketch = jQuery("#canvas_usg_glass_sketch_notes");
   var canvas = $tgpd_glass_sketch[0];
   var context = canvas.getContext("2d");
   $tgpd_glass_sketch.sketch('stopPainting');
   $tgpd_glass_sketch.sketch('actions', []);
   context.clearRect(0, 0, canvas.width, canvas.height);
   context.fillStyle = '#FFFFFF';
   context.fillRect(0, 0, canvas.width, canvas.height);
   if (tgpd_hide_toolbar) jQuery("#div_usg_canvas_controls_notes").hide();
}



function tgpd_canvas_to_sketch_filelist_notes() {

   var tgpd_glass_sketch = jQuery("#canvas_usg_glass_sketch_notes")[0];
   tgpd_glass_sketch.toBlob(function(blob) {
      console.log('tgpd_canvas_to_sketch_filelist_notes:canvas.toBlob: blob.type => ' + blob.type + ', blob.size => ' + blob.size);
      var fileName = S4() + S4() + '-' + (new Date()).getTime() + '.jpg';

      var newFile = new File([blob], fileName, {type:blob.type})
      console.log('tgpd_canvas_to_sketch_filelist_notes:canvas.toBlob: newFile.name => ' + newFile.name + ', newFile.lastModified => ' + newFile.lastModified + ', newFile.type => ' + newFile.type + ', newFile.size => ' + newFile.size);
      tgpd_handle_sketch_file_notes(newFile);
   }, 'image/jpg');
}
// krishan clear_canvas_notes end//

function tgpd_canvas_to_sketch_filelist() {
   var tgpd_glass_sketch = jQuery("#canvas_tgpd_glass_sketch")[0];
   tgpd_glass_sketch.toBlob(function(blob) {
      console.log('tgpd_canvas_to_sketch_filelist:canvas.toBlob: blob.type => ' + blob.type + ', blob.size => ' + blob.size);
      var fileName = S4() + S4() + '-' + (new Date()).getTime() + '.jpg';

      var newFile = new File([blob], fileName, {type:blob.type})
      console.log('tgpd_canvas_to_sketch_filelist:canvas.toBlob: newFile.name => ' + newFile.name + ', newFile.lastModified => ' + newFile.lastModified + ', newFile.type => ' + newFile.type + ', newFile.size => ' + newFile.size);
      tgpd_handle_sketch_file(newFile);
   }, 'image/jpg');
}
/*
function validateFormGPD() {
  return true;
}*/

function validateFormGPD() {
   var text_glass_location = jQuery('#text_tgpd_glass_location')[0];
   //var text_coating_exterior_type = jQuery('#text_tgpd_coating_exterior_type')[0];
   //var text_coating_interior_type = jQuery('#text_tgpd_coating_interior_type')[0];
   if (text_glass_location.value.trim().length == 0) {
      text_glass_location.focus();
      return false;
   }
   if (!text_glass_location.checkValidity()) {
      text_glass_location.focus();
      return false;
   }
  /*  if (text_coating_exterior_type.value.trim().length == 0) {
      text_coating_exterior_type.focus();
      return false;
   }
   if (!text_coating_exterior_type.checkValidity()) {
      text_coating_exterior_type.focus();
      return false;
   } */
   /*Shape Validation*/
   var glass_shape = jQuery('#select_tgpd_glass_shape').val();
   if (typeof glass_shape === 'undefined' || glass_shape == null) glass_shape = 'not_applicable';
   if (glass_shape.localeCompare('not_applicable') != 0) {
	   if(jQuery('#text_tagd_shape_left_leg_'+glass_shape).is(":visible")){
		   var text_glass_shape_left_leg = jQuery('#text_tagd_shape_left_leg_'+glass_shape)[0];

		   if (text_glass_shape_left_leg.value.trim().length == 0) {
			  text_glass_shape_left_leg.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_right_leg_'+glass_shape).is(":visible")){
		   var text_glass_shape_right_leg = jQuery('#text_tagd_shape_right_leg_'+glass_shape)[0];
		   if (text_glass_shape_right_leg.value.trim().length == 0) {
			  text_glass_shape_right_leg.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_width_'+glass_shape).is(":visible")){
		   var text_glass_shape_width = jQuery('#text_tagd_shape_width_'+glass_shape)[0];
		   if (text_glass_shape_width.value.trim().length == 0) {
			  text_glass_shape_width.focus();
			  return false;
		   }
   		}
		 if(jQuery('#text_tagd_shape_height_'+glass_shape).is(":visible")){
		   var text_glass_shape_height = jQuery('#text_tagd_shape_height_'+glass_shape)[0];
		   if (text_glass_shape_height.value.trim().length == 0) {
			  text_glass_shape_height.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_base_'+glass_shape).is(":visible")){
		   var text_glass_shape_base = jQuery('#text_tagd_shape_base_'+glass_shape)[0];
		   if (text_glass_shape_base.value.trim().length == 0) {
			  text_glass_shape_base.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_left_side_'+glass_shape).is(":visible")){
		   var text_glass_shape_left_side = jQuery('#text_tagd_shape_left_side_'+glass_shape)[0];
		   if (text_glass_shape_left_side.value.trim().length == 0) {
			  	text_glass_shape_left_side.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_right_side_'+glass_shape).is(":visible")){
		   var text_glass_shape_right_side = jQuery('#text_tagd_shape_right_side_'+glass_shape)[0];
		   if (text_glass_shape_right_side.value.trim().length == 0) {
			  	text_glass_shape_right_side.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_diameter_'+glass_shape).is(":visible")){
		   var text_glass_shape_diameter = jQuery('#text_tagd_shape_diameter_'+glass_shape)[0];
		   if (text_glass_shape_diameter.value.trim().length == 0) {
			  	text_glass_shape_diameter.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_radius_'+glass_shape).is(":visible")){
		   var text_glass_shape_radius = jQuery('#text_tagd_shape_radius_'+glass_shape)[0];
		   if (text_glass_shape_radius.value.trim().length == 0) {
			  	text_glass_shape_radius.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_radius_one_'+glass_shape).is(":visible")){
		   var text_glass_shape_radius_one = jQuery('#text_tagd_shape_radius_one_'+glass_shape)[0];
		   if (text_glass_shape_radius_one.value.trim().length == 0) {
			  	text_glass_shape_radius_one.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_radius_two_'+glass_shape).is(":visible")){
		   var text_glass_shape_radius_two = jQuery('#text_tagd_shape_radius_two_'+glass_shape)[0];
		   if (text_glass_shape_radius_two.value.trim().length == 0) {
			  	text_glass_shape_radius_two.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_length_'+glass_shape).is(":visible")){
		   var text_glass_shape_length = jQuery('#text_tagd_shape_length_'+glass_shape)[0];
		   if (text_glass_shape_length.value.trim().length == 0) {
			  	text_glass_shape_length.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_leg_'+glass_shape).is(":visible")){
		   var text_glass_shape_leg = jQuery('#text_tagd_shape_leg_'+glass_shape)[0];
		   if (text_glass_shape_leg.value.trim().length == 0) {
			  	text_glass_shape_leg.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_side_'+glass_shape).is(":visible")){
		   var text_glass_shape_side = jQuery('#text_tagd_shape_side_'+glass_shape)[0];
		   if (text_glass_shape_side.value.trim().length == 0) {
			  	text_glass_shape_side.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_left_offset_'+glass_shape).is(":visible")){
		   var text_glass_shape_left_offset = jQuery('#text_tagd_shape_left_offset_'+glass_shape)[0];
		   if (text_glass_shape_left_offset.value.trim().length == 0) {
			  text_glass_shape_left_offset.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_top_offset_'+glass_shape).is(":visible")){
		   var text_glass_shape_top_offset = jQuery('#text_tagd_shape_top_offset_'+glass_shape)[0];
		   if (text_glass_shape_top_offset.value.trim().length == 0) {
			  text_glass_shape_top_offset.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_right_offset_'+glass_shape).is(":visible")){
		   var text_glass_shape_right_offset = jQuery('#text_tagd_shape_right_offset_'+glass_shape)[0];
		   if (text_glass_shape_right_offset.value.trim().length == 0) {
			  text_glass_shape_right_offset.focus();
			  return false;
		   }
	   }

	    if(jQuery('#text_tagd_shape_top_'+glass_shape).is(":visible")){
		   var text_glass_shape_top = jQuery('#text_tagd_shape_top_'+glass_shape)[0];
		   if (text_glass_shape_top.value.trim().length == 0) {
			  text_glass_shape_top.focus();
			  return false;
		   }
	   }
   }



   var glass_type = jQuery('#select_tgpd_glass_type').val();
   if (typeof glass_type === 'undefined' || glass_type == null) glass_type = 'not_applicable';
   if (glass_type.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_type')[0].focus();
      return false;
   }
   if (glass_type.indexOf('igu') > -1) {
	  /*  if(glass_type!="igu_lami_temp_lami" && glass_type!="igu_sky_lite_temp_or_lami")
	   {
			  var lite_thickness = jQuery('#select_tgpd_lite_thickness').val();
			  if (typeof lite_thickness === 'undefined' || lite_thickness == null) lite_thickness = 'not_applicable';
			  if (lite_thickness.localeCompare('not_applicable') == 0) {
				 jQuery('#select_tgpd_lite_thickness')[0].focus();
				 return false;
			  }
	   } */
      var spacer_color = jQuery('#select_tgpd_spacer_color').val();
      if (typeof spacer_color === 'undefined' || spacer_color == null) spacer_color = 'not_applicable';
      if (spacer_color.localeCompare('not_applicable') == 0) {
         jQuery('#select_tgpd_spacer_color')[0].focus();
         return false;
      }
   }
   
   $('#select_tgpd_glass_type').change(function(){	
	var glass_type_change = $('#select_tgpd_glass_type').val();   
	if(glass_type_change !='igu_window' || glass_type_change !='igu_door_lite' ||  glass_type_change !='igu_spandrel' || glass_type_change !='igu_patio_door_or_panel' || glass_type_change !='bullet_proof_igu' || glass_type_change !='curved_igu' || glass_type_change != 'IGU (VIRACON)'){
     	 var glass_treatment = jQuery('#select_tgpd_glass_treatment').val();
	     if (typeof glass_treatment === 'undefined' || glass_treatment == null) glass_treatment = 'not_applicable';
			   if (glass_treatment.localeCompare('not_applicable') == 0) {
				  jQuery('#select_tgpd_glass_treatment')[0].focus();
				  return false;
			}
		var text_glass_lift = jQuery('#text_tgpd_glass_lift')[0];
			if (text_glass_lift.value.trim().length == 0) {
				text_glass_lift.focus();
				return false;
			}
		var text_glass_inside_lift_with_glass_type = jQuery('#text_tgpd_glass_inside_lift_with_glass_type')[0];
			if (text_glass_inside_lift_with_glass_type.value.trim().length == 0) {
				text_glass_inside_lift_with_glass_type.focus();
				return false;
			}	
			
		if (!text_glass_lift.checkValidity()) {
				text_glass_lift.focus();
				return false;
			}
		var glass_color = jQuery('#select_tgpd_glass_color').val();
		   if (typeof glass_color === 'undefined' || glass_color == null) glass_color = 'not_applicable';
			   if (glass_color.localeCompare('not_applicable') == 0) {
				  jQuery('#select_tgpd_glass_color')[0].focus();
				  return false;
			   }
		   if (glass_color.localeCompare('custom') == 0) {
			  var text_glass_color_note = jQuery('#text_tgpd_glass_color_note')[0];
			  if (text_glass_color_note.value.trim().length == 0) {
				 text_glass_color_note.focus();
				 return false;
			  }
			  if (!text_glass_color_note.checkValidity()) {
				 text_glass_color_note.focus();
				 return false;
			  }
		   }			
		}
   });
   


   
   var glass_set = jQuery('#select_tgpd_glass_set').val();
   if (typeof glass_set === 'undefined' || glass_set == null) glass_set = 'not_applicable';
   if (glass_set.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_set')[0].focus();
      return false;
   }
   /*****************************************************************/
   
     if(jQuery('#text_tgpd_coating_first_position').is(":visible")){
		   var text_tgpd_coating_position = jQuery('#text_tgpd_coating_first_position').prop('checked') ? 1 : 0;

		 if(text_tgpd_coating_position==0)
		 {
		   var text_coating_position = jQuery('#text_tgpd_coating_first_position')[0];
		   if (text_coating_position.value.trim().length == 0) {
			  text_coating_position.focus();
			  return false;
		   }
		   if (!text_coating_position.checkValidity()) {
			  text_coating_position.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_coating_position.value)) {
			  text_coating_position.focus();
			  return false;
		   }
		 }
	 }
	 
	 if(jQuery('#text_tgpd_coating_second_position').is(":visible")){
		   var text_tgpd_coating_position1 = jQuery('#text_tgpd_coating_second_position').prop('checked') ? 1 : 0;

		 if(text_tgpd_coating_position1==0)
		 {
		   var text_coating_position1 = jQuery('#text_tgpd_coating_second_position')[0];
		   if (text_coating_position1.value.trim().length == 0) {
			  text_coating_position1.focus();
			  return false;
		   }
		   if (!text_coating_position1.checkValidity()) {
			  text_coating_position1.focus();
			  return false;
		   }
		   if (!is_valid_fraction1(text_coating_position1.value)) {
			  text_coating_position1.focus();
			  return false;
		   }
		 }
	 }
   
   /******************************************************************/
   
   
  if(jQuery('#text_tgpd_daylight').is(":visible")){
		   var text_tgpd_daylight = jQuery('#text_tgpd_daylight').prop('checked') ? 1 : 0;

		 if(text_tgpd_daylight==0)
		 {
		   var text_daylight_width = jQuery('#text_tgpd_daylight_width')[0];
		   if (text_daylight_width.value.trim().length == 0) {
			  text_daylight_width.focus();
			  return false;
		   }
		   if (!text_daylight_width.checkValidity()) {
			  text_daylight_width.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_daylight_width.value)) {
			  text_daylight_width.focus();
			  return false;
		   }
		   var text_daylight_height = jQuery('#text_tgpd_daylight_height')[0];
		   if (text_daylight_height.value.trim().length == 0) {
			  text_daylight_height.focus();
			  return false;
		   }
		   if (!text_daylight_height.checkValidity()) {
			  text_daylight_height.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_daylight_height.value)) {
			  text_daylight_height.focus();
			  return false;
		   }
		 }


		   var text_tgpd_gosize = jQuery('#text_tgpd_gosize').prop('checked') ? 1 : 0;

		 if(text_tgpd_gosize==0)
		 {


		   var text_go_width = jQuery('#text_tgpd_go_width')[0];
		   if (text_go_width.value.trim().length == 0) {
			  text_go_width.focus();
			  return false;
		   }
		   if (!text_go_width.checkValidity()) {
			  text_go_width.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_go_width.value)) {
			  text_go_width.focus();
			  return false;
		   }
		   var text_go_height = jQuery('#text_tgpd_go_height')[0];
		   if (text_go_height.value.trim().length == 0) {
			  text_go_height.focus();
			  return false;
		   }
		   if (!text_go_height.checkValidity()) {
			  text_go_height.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_go_height.value)) {
			  text_go_height.focus();
			  return false;
		   }
		 }
  }
  
 

   /*Removed on date 2nd Nov 2017*/
   
   
   
   
   var sag_or_quote = jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked') ? 1 : 0;
   if (sag_or_quote == 1) {
      var text_manufacturer = jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer')[0];
      if (text_manufacturer.value.trim().length == 0) {
         text_manufacturer.focus();
         return false;
      }
      if (!text_manufacturer.checkValidity()) {
         text_manufacturer.focus();
         return false;
      }
   }
   var solar_film = jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked') ? 1 : 0;
   if (solar_film == 1) {
      var solar_film_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]:checked').val();
      if (typeof solar_film_responsibility === 'undefined' || solar_film_responsibility == null) {
         jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]').focus();
         return false;
      }
      if (solar_film_responsibility.localeCompare('sag') == 0) {
         var text_solar_film_type = jQuery('#text_tgpd_glass_reminders_solar_film_type')[0];
         if (text_solar_film_type.value.trim().length == 0) {
            text_solar_film_type.focus();
            return false;
         }
         if (!text_solar_film_type.checkValidity()) {
            text_solar_film_type.focus();
            return false;
         }
         var text_solar_film_source = jQuery('#text_tgpd_glass_reminders_solar_film_source')[0];
         if (text_solar_film_source.value.trim().length == 0) {
            text_solar_film_source.focus();
            return false;
         }
         if (!text_solar_film_source.checkValidity()) {
            text_solar_film_source.focus();
            return false;
         }
      }
   }
   var wet_seal = jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked') ? 1 : 0;
   if (wet_seal == 1) {
      var wet_seal_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]:checked').val();
      if (typeof wet_seal_responsibility === 'undefined' || wet_seal_responsibility == null) {
         jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]').focus();
         return false;
      }
   }
   var furniture_to_move = jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked') ? 1 : 0;
   if (furniture_to_move == 1) {
      var text_furniture_to_move_comment = jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment')[0];
      if (text_furniture_to_move_comment.value.trim().length == 0) {
         text_furniture_to_move_comment.focus();
         return false;
      }
      if (!text_furniture_to_move_comment.checkValidity()) {
         text_furniture_to_move_comment.focus();
         return false;
      }
   }
   var walls_or_ceilings_to_cut = jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked') ? 1 : 0;
   if (walls_or_ceilings_to_cut == 1) {
      var walls_or_ceilings_to_cut_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]:checked').val();
      if (typeof walls_or_ceilings_to_cut_responsibility === 'undefined' || walls_or_ceilings_to_cut_responsibility == null) {
         jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]').focus();
         return false;
      }
      if (walls_or_ceilings_to_cut_responsibility.localeCompare('sag') == 0 || walls_or_ceilings_to_cut_responsibility.localeCompare('customer') == 0) {
         var text_walls_or_ceilings_to_cut_comment = jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment')[0];
         if (text_walls_or_ceilings_to_cut_comment.value.trim().length == 0) {
            text_walls_or_ceilings_to_cut_comment.focus();
            return false;
         }
         if (!text_walls_or_ceilings_to_cut_comment.checkValidity()) {
            text_walls_or_ceilings_to_cut_comment.focus();
            return false;
         }
      }
   }
   var textarea_instructions = jQuery('#textarea_tgpd_instructions')[0];
   if (!textarea_instructions.checkValidity()) {
      textarea_instructions.focus();
      return false;
   }
  
  
   if (glass_type.indexOf('igu') > -1) {
      var glass_grids = jQuery('#checkbox_tgpd_glass_grids').prop('checked') ? 1 : 0;
      console.log('validateFormGPD:glass_grids -> ' + glass_grids + ', glass_grids => ' + glass_grids == 1 ? true : false);
      if (glass_grids == 1) {
         var number_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids')[0];
         if (number_pattern_horizontal_grids.value.trim().length == 0) {
            number_pattern_horizontal_grids.focus();
            return false;
         }
         if (!number_pattern_horizontal_grids.checkValidity()) {
            number_pattern_horizontal_grids.focus();
            return false;
         }
         var number_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids')[0];
         if (number_pattern_vertical_grids.value.trim().length == 0) {
            number_pattern_vertical_grids.focus();
            return false;
         }
         if (!number_pattern_vertical_grids.checkValidity()) {
            number_pattern_vertical_grids.focus();
            return false;
         }
         var grids_thickness = jQuery('#select_tgpd_grids_thickness').val();
         if (typeof grids_thickness === 'undefined' || grids_thickness == null) grids_thickness = 'not_applicable';
         if (grids_thickness.localeCompare('not_applicable') == 0) {
            jQuery('#select_tgpd_grids_thickness')[0].focus();
            return false;
         }
         var text_grids_color = jQuery('#text_tgpd_grids_color')[0];
         if (text_grids_color.value.trim().length == 0) {
            text_grids_color.focus();
            return false;
         }
         if (!text_grids_color.checkValidity()) {
            text_grids_color.focus();
            return false;
         }
      }
   }
   var glass_fabrication = jQuery('#checkbox_tgpd_glass_fabrication').prop('checked') ? 1 : 0;
   console.log('validateFormGPD:glass_fabrication -> ' + glass_fabrication + ', glass_fabrication => ' + glass_fabrication == 1 ? true : false);
   if (glass_fabrication == 1) {
      var text_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges')[0];
      var glass_fabrication_edges = jQuery('#text_tgpd_fabrication_polished_edges_hide').prop('checked') ? 1 : 0;
	   var glass_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern_hide').prop('checked') ? 1 : 0;
	   
	   if(glass_fabrication_edges == '1'){
		    console.log('glass_fabrication_edges'+ glass_fabrication_edges+ '><>>>>>>>>>>>>>>>' );
	   }else{
      if (text_fabrication_polished_edges.value.trim().length == 0) {
         text_fabrication_polished_edges.focus();
         return false;
      }
	}
      if (!text_fabrication_polished_edges.checkValidity()) {
         text_fabrication_polished_edges.focus();
         return false;
      }
       var text_fabrication_holes = jQuery('#text_tgpd_fabrication_holes')[0];
      
      
    if(  text_fabrication_holes){
      if (!text_fabrication_holes.checkValidity()) {
         text_fabrication_holes.focus();
         return false;
      }
    }
      var text_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern')[0];
      
      if(glass_fabrication_pattern == '1'){
		  
		  console.log('glass_fabrication_pattern'+ glass_fabrication_pattern+ '><>>>>>>>>>>>>>>>' );
	  }else{
      if (text_fabrication_pattern.value.trim().length == 0) {
         text_fabrication_pattern.focus();
         return false;
      }
  }
	  
      if (!text_fabrication_pattern.checkValidity()) {
         text_fabrication_pattern.focus();
         return false;
      }
   }
   return true;
}

function tgpd_handle_picture_files(files) {

   var $tgpd_picture_filelist = jQuery('#div_tgpd_picture_filelist');
   var $tgpd_picture_filelist_controls = jQuery('#div_tgpd_picture_filelist_notes');
   if (!files.length) {
   } else {
      var $list = jQuery('#div_tgpd_picture_filelist ul');
      if ($list.length == 0) {

         $list = jQuery(tgpd_picture_list);

		 $tgpd_picture_filelist.append($list);
		}

      for (var i = 0; i < files.length; i++) {
         var $list_item = jQuery(tgpd_picture_list_item);
         var $image = jQuery('img', $list_item);
         var $status = jQuery('.status', $list_item);
         var $info = jQuery('.info', $list_item);
         var $progress_bar = jQuery('.progressBar', $list_item);
         $image[0].file = files[i];
         $image[0].status = $status;
         $image[0].progress = $progress_bar;
         $image.addClass('picture_obj');
         $image.attr('src', window.URL.createObjectURL(files[i]));
         $image.on('load', function(event) {
            window.URL.revokeObjectURL(this.src);
         });
         $info.html(files[i].name + ': ' + files[i].size + ' bytes');
         $list.append($list_item);
      }
      if ($tgpd_picture_filelist_controls.is(':hidden')) $tgpd_picture_filelist_controls.show();
      var ev = jQuery._data($tgpd_picture_filelist[0], 'events');

      if(!(ev && ev.click)) {
         $tgpd_picture_filelist.click(function(event) {
            tgpd_toggle_picture_filelist_controls(event, this);

         });
	 }

	 jQuery("#div_tgpd_picture_filelist_controls").show();
		jQuery('#btn_tgpd_upload_picture_filelist').removeAttr('disabled');
		jQuery('#btn_tgpd_upload_picture_filelist').removeClass('cust-disable');

   }
}

/* <!-- krishan Notes tabs start function --> */

function tgpd_handle_picture_files_notes(files) {
   var $tgpd_picture_filelist = jQuery('#div_upg_picture_filelist_notes');
   var $tgpd_picture_filelist_controls = jQuery('#div_upg_picture_filelist_controls_notes');

   if (!files.length) {
   } else {
      var $list = jQuery('#div_upg_picture_filelist_notes ul');
      if ($list.length == 0) {
         $list = jQuery(tgpd_picture_list);
         $tgpd_picture_filelist.append($list);
		}

      for (var i = 0; i < files.length; i++) {
         var $list_item = jQuery(tgpd_picture_list_item);
         var $image = jQuery('img', $list_item);
         var $status = jQuery('.status', $list_item);
         var $info = jQuery('.info', $list_item);
         var $progress_bar = jQuery('.progressBar', $list_item);
         $image[0].file = files[i];
         $image[0].status = $status;
         $image[0].progress = $progress_bar;
         $image.addClass('picture_obj');
         $image.attr('src', window.URL.createObjectURL(files[i]));
         $image.on('load', function(event) {
            window.URL.revokeObjectURL(this.src);
         });
         $info.html(files[i].name + ': ' + files[i].size + ' bytes');
         $list.append($list_item);
      }

      if ($tgpd_picture_filelist_controls.is(':hidden')) $tgpd_picture_filelist_controls.show();
      var ev = jQuery._data($tgpd_picture_filelist[0], 'events');

	  if(!(ev && ev.click)) {
		$tgpd_picture_filelist.click(function(event) {
            tgpd_toggle_picture_filelist_controls_notes(event, this);

         });
	 }


   }
}



function tgpd_handle_sketch_file_notes(file) {

   var $tgpd_sketch_filelist = jQuery('#div_usg_sketch_filelist_notes');
   var $tgpd_sketch_filelist_controls = jQuery("#div_usg_sketch_filelist_controls_notes");
   var $list = jQuery('#div_usg_sketch_filelist_notes ul');
   if ($list.length == 0) {
      $list = jQuery(tgpd_sketch_list);
      $tgpd_sketch_filelist.append($list);
   }
   var $list_item = jQuery(tgpd_sketch_list_item);
   var $image = jQuery('img', $list_item);
   var $status = jQuery('.status', $list_item);
   var $info = jQuery('.info', $list_item);
   var $progress_bar = jQuery('.progressBar', $list_item);
   $image[0].file = file;
   $image[0].status = $status;
   $image[0].progress = $progress_bar;
   $image.addClass('sketch_obj');
   $image.attr('src', window.URL.createObjectURL(file));
   $image.on('load', function(event) {
      window.URL.revokeObjectURL(this.src);
   });
   $info.html(file.name + ': ' + file.size + ' bytes');
   $list.append($list_item);
   if ($tgpd_sketch_filelist_controls.is(':hidden')) $tgpd_sketch_filelist_controls.show();
   var ev = jQuery._data($tgpd_sketch_filelist[0], 'events');
   if(!(ev && ev.click)) {
      $tgpd_sketch_filelist.click(function(event) {
         tgpd_toggle_sketch_filelist_controls(event, this);
      });
   }
}

<!-- krishan notes tab end function-->


function tgpd_handle_sketch_file(file) {
   var $tgpd_sketch_filelist = jQuery('#div_tgpd_sketch_filelist');
   var $tgpd_sketch_filelist_controls = jQuery("#div_tgpd_sketch_filelist_controls");
   var $list = jQuery('#div_tgpd_sketch_filelist ul');
   if ($list.length == 0) {
      $list = jQuery(tgpd_sketch_list);
      $tgpd_sketch_filelist.append($list);
   }
   var $list_item = jQuery(tgpd_sketch_list_item);
   var $image = jQuery('img', $list_item);
   var $status = jQuery('.status', $list_item);
   var $info = jQuery('.info', $list_item);
   var $progress_bar = jQuery('.progressBar', $list_item);
   $image[0].file = file;
   $image[0].status = $status;
   $image[0].progress = $progress_bar;
   $image.addClass('sketch_obj');
   $image.attr('src', window.URL.createObjectURL(file));
   $image.on('load', function(event) {
      window.URL.revokeObjectURL(this.src);
   });
   $info.html(file.name + ': ' + file.size + ' bytes');
   $list.append($list_item);
   if ($tgpd_sketch_filelist_controls.is(':hidden')) $tgpd_sketch_filelist_controls.show();
   var ev = jQuery._data($tgpd_sketch_filelist[0], 'events');
   if(!(ev && ev.click)) {
      $tgpd_sketch_filelist.click(function(event) {
         tgpd_toggle_sketch_filelist_controls(event, this);
      });
   }
}

jQuery(document).ready(function($) {
	$('#select_tgpd_servicetype').change(function(){
			if($(this).val() == 'Removal/Reinstall_different_day'){
				$('#secondryquote').show();
				}else{
					$('#secondryquote').hide();
					}
												  });

	$('#text_tgpd_daylight').change(function(){

			  var tgpd_checked = $(this).prop('checked')?1:0;

			  if(tgpd_checked==1)
			  {
			//	  $("#daylightsetglasspiece").hide();
				//$("#text_tgpd_daylight_width").attr("disabled",true);
				//$("#text_tgpd_daylight_height").attr("disabled",true);
			  }
			  else
			  {
			//	  	 $("#daylightsetglasspiece").show();
				  //$("#text_tgpd_daylight_width").attr("disabled",false);
					//$("#text_tgpd_daylight_height").attr("disabled",false);
			  }
		/*	$('#text_tgpd_gosize').change(function(){

			  var tgpd_checked = $(this).prop('checked')?1:0;
			  if(tgpd_checked==1)
			  {
				  	 $("#div_tgpd_go_size").css("display","none");
					 
			  }
			  else
			  {
				  	$("#div_tgpd_go_size").css("display","block");
			  }

				});*/

			});

			$('#text_tgpd_gosize').change(function(){
		var tgpd_checked = $(this).prop('checked')?1:0;
		if(tgpd_checked==1)
			  {
				$("#hide_height_width").hide();
					$("#verify_go_size_width_go_width").css("visibility", "hidden");
				$("#ttgh").css("visibility", "hidden");
//				$("#verify_go_size_width_go_width").hide();
				
				/* $("#text_tgpd_go_height1").hide();
				$('label[for="verify_go_size_widths"]').hide();
				$('label[for="verify_go_size_heights"]').hide();
				$("#verify_go_size_heights").hide(); */
				// $("#verify_go_size_width_go_width").hide();
				// $("#text_tgpd_go_height").hide();
			  }
			  else
			  {
				$("#hide_height_width").show();
				
				$("#verify_go_size_width_go_width").css("visibility", "visible");
				$("#ttgh").css("visibility", "visible");
			//	$("#verify_go_size_width_go_width").hide();
				/* $("#text_tgpd_go_height1").show();
				$('label[for="verify_go_size_widths"]').show();
				$('label[for="verify_go_size_heights"]').show(); */
				// $("#verify_go_size_width_go_width").show();
				// $("#text_tgpd_go_height").show();
			  }

	});
			$("#interior_ttdw").hide();
			$("#exterior_ttdh").hide();
			$('#text_tgpd_coating').change(function(){
			var tgpd_checked = $(this).prop('checked')?1:0;
			if(tgpd_checked == 1)
			  {
				$("#hide_coating_exterior").show();
			/* 	$("#interior_ttdw").show();
				$("#exterior_ttdh").show(); */
				$("#hide_coating_exterior").css("visibility", "visible");
				$("#exterior_ttdh").css("visibility", "visible");
				$("#interior_ttdw").css("visibility", "visible");
			  }
			  else
			  {
				$("#hide_coating_exterior").hide();
				/* $("#interior_ttdw").hide();
				$("#exterior_ttdh").hide(); */
				$("#hide_coating_exterior").css("visibility", "hidden");
				$("#interior_ttdw").css("visibility", "hidden");
				$("#exterior_ttdh").css("visibility", "hidden");
				
			  }
 
			});

			/* $('#text_tgpd_coating_position').change(function(){
			var tgpd_checked = $(this).prop('checked')?1:0;
			if(tgpd_checked==0)
			  {
				$("#text_tgpd_coating_exterior_type").hide(); 
			  }
			  else
			  {
			  $("#text_tgpd_coating_exterior_type").show();
			  }
			  }); */
			
			  

			$('#text_tgpd_daylight').change(function(){
			var tgpd_checked = $(this).prop('checked')?1:0;
			if(tgpd_checked==1)
			  {
				$("#hide_width_height").hide();
				$("#div_tgpd_day_size_plus").hide();
				$("#extra_width_extra_hieght").hide();
//				$("#ttdw").addAttr();
				$("#ttdw").css("visibility", "hidden");
				$("#ttdh").css("visibility", "hidden");
	//			$("#ttdh").hide();
				/* $("#text_tgpd_daylight_height").hide();
				$('label[for="verify_tgpd_daylight_width"]').hide();
				$('label[for="verify_tgpd_daylight_height"]').hide();
				$("#verify_go_size_heights").hide(); */
				// $("#verify_go_size_width_go_width").hide();
				// $("#text_tgpd_go_height").hide();
			  }
			  else
			  {
				$("#hide_width_height").show();$("#div_tgpd_day_size_plus").show();
				
				$("#ttdw").css("visibility", "visible");
				$("#ttdh").css("visibility", "visible");
				$("#extra_width_extra_hieght").show();
		//		$("#ttdw").show();
		//		$("#ttdh").show();
				/* $("#text_tgpd_daylight_height").show();
				$('label[for="verify_tgpd_daylight_width"]').show();
				$('label[for="verify_tgpd_daylight_height"]').show(); */
				// $("#verify_go_size_width_go_width").show();
				// $("#text_tgpd_go_height").show();
			  }

			});
			

   $('#select_tgpd_glass_type').change(function(event) {
      //alert("here too")
      var tgpd_glass_type = $(this).val();
      console.log('select_tgpd_glass_type.change: tgpd_glass_type -> ' + tgpd_glass_type);
      tgpd_set_glass_type_state(tgpd_glass_type, false);
   });
   $('#select_tgpd_glass_color').change(function(event) {
      var tgpd_glass_color = $(this).val();
      console.log('select_tgpd_glass_color.change: tgpd_glass_color -> ' + tgpd_glass_color);
      tgpd_set_glass_color_state(tgpd_glass_color, true);
   });
   $('input:checkbox[name=checkbox_tgpd_glass_reminders]').change(function(event) {
			//alert('here to');															  
      var tgpd_glass_reminders = $(this).val();
      var tgpd_checked = $(this).prop('checked')?1:0;

     // alert('tgpd_glass_reminders',tgpd_glass_reminders);
     // alert('tgpd_checked',tgpd_checked);
      if(tgpd_glass_reminders.localeCompare('lift_outside') == 0){
       tgpd_set_lift_outside_glass_reminders(tgpd_checked, false) 
     }
     
     if(tgpd_glass_reminders.localeCompare('lift_inside') == 0){
      tgpd_set_lift_inside_glass_reminders(tgpd_checked, false) 
     }
	
      if (tgpd_glass_reminders.localeCompare('solar_film') == 0) {
        tgpd_set_glass_reminders_solar_film1_state(tgpd_checked, false);
      }
      if (tgpd_glass_reminders.localeCompare('wet_seal') == 0) {
         tgpd_set_glass_reminders_wet_seal_state(tgpd_checked, false);
      }
      if (tgpd_glass_reminders.localeCompare('furniture_to_move') == 0) {
         tgpd_set_glass_reminders_furniture_to_move_state(tgpd_checked, true);
      }
	  
	  if (tgpd_glass_reminders.localeCompare('disclamers') == 0) {
         tgpd_set_glass_reminders_disclamers_to_move_state(tgpd_checked, true);
      }
	  
	  if (tgpd_glass_reminders.localeCompare('damage_viewer') == 0) {
         tgpd_set_glass_reminders_damage_waiver_to_move_state(tgpd_checked, true);
      }
	 
	  
	  
      if (tgpd_glass_reminders.localeCompare('walls_or_ceilings_to_cut') == 0) {
         tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(tgpd_checked, false);
      }
	  
	  /*if(tgpd_damage_waiver_to_move.localeCompare('damage_viewer')==0){
		  tgpd_set_glass_reminders_damage_waiver_state(tgpd_checked,false);
	  }*/
	  
   });
   $('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]').change(function(event) {
      var tgpd_solar_film_responsibility = $(this).val();
      console.log('radio_tgpd_glass_reminders_solar_film_responsibility.change: tgpd_solar_film_responsibility -> ' + tgpd_solar_film_responsibility);
      tgpd_set_glass_reminders_solar_film2_state(tgpd_solar_film_responsibility, true);
   });
   $('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]').change(function(event) {
      var tgpd_walls_or_ceilings_to_cut_responsibility = $(this).val();
      console.log('radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.change: tgpd_walls_or_ceilings_to_cut_responsibility -> ' + tgpd_walls_or_ceilings_to_cut_responsibility);
      tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_walls_or_ceilings_to_cut_responsibility, true);
   });
   $('#select_tgpd_glass_set').change(function(event) {
      set_text_tgpd_go_width(true);
      set_text_tgpd_go_height(true);
       $("#verify_set_width").prop("checked",true);
	   $("#verify_set_height").prop("checked",true);
   });
   $('#text_tgpd_daylight_width').change(function(event) {
      set_text_tgpd_go_width(true);
   });
   $('#text_tgpd_daylight_height').change(function(event) {
      
      set_text_tgpd_go_height(true);
   });
   $('#text_tgpd_extra_width').change(function(event) {
       $("#verify_set_width").prop('checked',true);
      set_text_tgpd_go_width(false);
   });
   $('#text_tgpd_extra_height').change(function(event) {
        $("#verify_set_height").prop('checked',true);
      set_text_tgpd_go_height(false);
   });
   $('#select_tgpd_size_verification_status').change(function(event) {
      var tgpd_size_verification_status = $(this).val();
      tgpd_set_size_verification_status_state(tgpd_size_verification_status);
   });
   $('input:checkbox[name=checkbox_tgpd_glass_pricing_sag_or_quote]').change(function(event) {
      var tgpd_glass_pricing = $(this).val();
      var tgpd_checked = $(this).prop('checked');

      if (tgpd_glass_pricing.localeCompare('sag_or_quote') == 0) {
         tgpd_set_glass_pricing_sag_or_quote_state(tgpd_checked, true);
      }
   });
   $('#checkbox_tgpd_glass_grids').change(function(event) {
      var tgpd_checked = $(this).prop('checked');
      tgpd_set_glass_grids_state(tgpd_checked, true);
   });
   $('#checkbox_tgpd_glass_fabrication').change(function(event) {
      var tgpd_checked = $(this).prop('checked');
      tgpd_set_glass_fabrication_state(tgpd_checked, true);
   });
   $('#file_tgpd_glass_picture').change(function(event) {
      tgpd_handle_picture_files(this.files);
   });

   $('#file_tgpd_notes_picture').change(function(event) {
      tgpd_handle_picture_files_notes(this.files);
	  jQuery('#btn_upg_upload_picture_filelist_notes').removeAttr('disabled');
	jQuery('#btn_upg_upload_picture_filelist_notes').removeClass('cust-disable');
   });


   $('#canvas_tgpd_glass_sketch').dblclick(function(event) {
      tgpd_toggle_canvas_controls(event, this);
   });
   $('#btn_tgpd_clear_canvas').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_canvas();
   });
   // krishan notes clear_canvas start//
   $('#btn_usg_clear_canvas_notes').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_canvas_notes();
   });

   $('#btn_usg_canvas_to_sketch_filelist_notes').click(function(event) {
		jQuery('#btn_usg_upload_sketch_filelist_notes').removeAttr('disabled');
		jQuery('#btn_usg_upload_sketch_filelist_notes').removeClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      tgpd_canvas_to_sketch_filelist_notes();
      $('#btn_usg_clear_canvas_notes').click();
   });


   ////notes clear_canvas end//
   $('#btn_tgpd_canvas_to_sketch_filelist').click(function(event) {

														 
      event.stopPropagation();
      tgpd_canvas_to_sketch_filelist();
	  jQuery('#btn_tgpd_upload_sketch_filelist').removeAttr('disabled');
	jQuery('#btn_tgpd_upload_sketch_filelist').removeClass('cust-disable');
      $('#btn_tgpd_clear_canvas').click();
   });
   $('#btn_tgpd_clear_picture_filelist').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_glass_picture_filelist();
   });
   $('#btn_tgpd_upload_picture_filelist').click(function(event) {
	jQuery(this).attr('disabled','disabled');
	jQuery(this).addClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      tgpd_upload_glass_picture_filelist();
	  tgpd_editingsave=true;
   });
   $('#canvas-wrap h5').click(function(event) {
      event.preventDefault();
      tgpd_toggle_canvas_holder(event, this);
   });

 //krishan start click function //
 $('#usg-canvas-wrap > #h5notes').click(function(event) {
	  event.preventDefault();
      tgpd_toggle_canvas_holder_notes(event, this);
   });
  /*$('#btn_usg_clear_sketch_filelist_notes').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_glass_sketch_filelist_notes();
   });*/
 // krishan end click function end//

   $('#btn_tgpd_clear_sketch_filelist').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_glass_sketch_filelist();
   });
   $('#btn_tgpd_upload_sketch_filelist').click(function(event) {
		jQuery('#btn_tgpd_upload_sketch_filelist').attr('disabled','disabled');
	jQuery('#btn_tgpd_upload_sketch_filelist').addClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      tgpd_upload_glass_sketch_filelist();
	  tgpd_editingsave=true;
   });
   $("#list_tgpd_canvas_marker_size a").click(function(event) {
      event.preventDefault();
	      $("#btn_tgpd_canvas_marker_size").dropdown("toggle");
   });


   $("#list_tgpd_canvas_marker_color a").click(function(event) {
      event.preventDefault();
      $("#btn_tgpd_canvas_marker_color").dropdown("toggle");
   });

   $("#list_usg_canvas_marker_size_notes a").click(function(event) {
	 event.preventDefault();
	$("#btn_usg_canvas_marker_size_notes").dropdown("toggle");
  });


   $('#btn_tgpd_add').click(function(event) {
	
		event.preventDefault();
		event.stopPropagation();
		tgpd_editing = true;
		tgpd_clear_form();
		glass_piece_enabled();
		maxChaulk=0;
		maxTape=0;
		maxChannel=0;
		maxScaffolding=0;
		maxHole=0;
	//alert("tgpd_current_item before >> " + tgpd_current_item);
	//alert('maxclucks array before '+maxChaulks[tgpd_current_item]);
		tgpd_current_item++;
		//alert("tgpd_current_item >> " + tgpd_current_item);
		maxChaulks[tgpd_current_item]=0;
		maxTapes[tgpd_current_item]=0;
		maxChannels[tgpd_current_item]=0;
		maxScaffoldings[tgpd_current_item]=0;
		maxHoles[tgpd_current_item]=0;
		//alert('maxclucks array after '+maxChaulks[tgpd_current_item]);
		if(back_date_entry ==true){ 
			$("#litethick_see").show();
			$("#lift_inside_hide_on_glass_type").hide();
			$("#text_tgpd_in_lift_position").hide();
			$("#lift_inside").hide();
			$("#lift_outside_hide_on_glass_type").hide();
			$("#text_tgpd_out_lift_position").hide();
			$("#lift_outside").hide();
			jQuery('#verify_tgpd_lite_thickness').prop('checked', false).removeClass('verify-active');;
			jQuery('#verify_tgpd_glass_treatment').prop('checked', false).removeClass('verify-active');;
			jQuery('#verify_tgpd_glass_color').prop('checked', false).removeClass('verify-active');;
			jQuery('#verify_tgpd_glass_lift').prop('checked', false).removeClass('verify-active');;
			jQuery('#verify_tgpd_spacer_color').prop('checked', false).removeClass('verify-active');;
			jQuery('#select_tgpd_spacer_color').val(['not_applicable']);
			jQuery('#verify_tgpd_lite_thickness').prop('checked', false).removeClass('verify-active');;
			jQuery('#select_tgpd_lite_thickness').val(['not_applicable']);
			
		}
		tgpd_set_toolbar_navigation_buttons_state();
		tgpd_set_toolbar_editing_buttons_state();
		$('#extra_type_coating_treatment').hide();
		$('#extra_type_coating_treatment_second').hide();
		$("#text_tgpd_coating_first_position").prop( "checked", true );
		$("#text_tgpd_coating_second_position").prop( "checked", true );
		$('#text_tgpd_coating_exterior_type').val('N/A');
		$('#text_tgpd_coating_interior_type').val('N/A');
		$('.chaulksection .addqty').hide();
		//jQuery('#verify_tgpd_glass_set').prop('checked',true).addClass('verify-active');;
		
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').hide();
		jQuery('#fieldset_tgpd_glass_disclamers_reminder_section').hide();
		jQuery('#text_tgpd_glass_damage_waiver_reminder_section').hide();
		jQuery('#text_tgpd_glass_disclamers_reminder_section').hide();
		
		jQuery('#number_tgpd_pattern_vertical_grids').val('0');
		jQuery('#number_tgpd_pattern_horizontal_grids').val('0');
		jQuery('#text_tgpd_fabrication_polished_edges').hide();
      jQuery('#text_tgpd_fabrication_pattern').hide();
      if ( $( "#glass_piece_offset_div" ).length ) {
       $('#glass_piece_offset_div').html('');

       addAndUpdateOffset('','','','','',0);

    }

       $('#text_tgpd_fabrication_offset_hide_new').prop('checked', false);
		glassHolesCounter=0;
	$('.chaulksection').prepend('<div class="form-group addqty"  id="add_quntity_'+tgpd_current_item+'"><div class="add_quntity"></div><div class="add_tap_amount"></div><div class="add_tap_scaff"></div><div class="add_tap_channel"></div></div>');
		addAndUpdateTape('0', '', '',0,true);
		addAndUpdateCulk( '0',  '', '',0,true );
	 	addAndUpdateChannel('',0,0,true);
    addAndUpdateScaffolding('',0,0,true);
		
		$('#glass_piece_holes_div').html('');
		addAndUpdateHoles('','','','','','','',0,true);
		
      $('#glass_piece_holes_div').hide();
      $('#offset_data_frm').hide();

      $('#text_tgpd_fabrication_polished_edges_hide').prop('checked',true);
      $('#text_tgpd_fabrication_pattern_hide').prop('checked',true);
      
      $('#text_tgpd_fabrication_holes_hide_new').prop('checked',true);
      
      $('#text_tgpd_fabrication_offset_hide_new').prop('checked',true);
      $('#select_tgpd_servicetype').val('replace')
		
   });

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

   $('#btn_tgpd_save').click(function(event) {
		/*if(getParameterByName('job_id')!=''){
			tgpd_editing=true;
		}	*/

	 event.preventDefault();
      event.stopPropagation();
      if (!validateFormGPD()) {
         console.log('btn_tgpd_save.click: validateFormGPD() -> false');
         return false;
      }
      console.log('btn_tgpd_save.click: validateFormGPD() -> true');
      var tgpd_item = tgpd_get_item_from_form();
	
	 if (tgpd_editing) {
         tgpd_editing = false;
         tgpd_items.push(tgpd_item);
         if (tgpd_current_item == -1 || tgpd_items.length == 1) {
            tgpd_current_item = 0;
         }
         tgpd_show_current_item();
         tgpd_set_toolbar_navigation_buttons_state();
         tgpd_set_toolbar_editing_buttons_state();
      } else {
         tgpd_items[tgpd_current_item] = tgpd_item;
      }
        ///alert(JSON.stringify(tgpd_items));
     
   });
   $('#btn_tgpd_delete').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
	//  glass_piece_disabled();
		maxChaulks[tgpd_current_item]=0;
		maxTapes[tgpd_current_item]=0;maxScaffoldings[tgpd_current_item]=0;maxChannels[tgpd_current_item]=0;
		maxHoles[tgpd_current_item]=0;
		/*
	maxChannels=[];var maxScaffoldings=[];var maxScaffolding;	var maxChannel;		
	*/
		maxChaulk=0;
		maxTape=0;
		maxChannel=0;
		maxScaffolding=0;
		maxHole=0;glassHolesCounter=0;
		$('#add_quntity_'+(tgpd_current_item+1)).remove();
		$('#add_quntity_'+tgpd_current_item).show();
		
		$('#glass_piece1_holes_'+(tgpd_current_item+1)).remove();
		$('#glass_piece1_holes_'+tgpd_current_item).show();
		
		//$('.chaulksection').prepend('<div class="form-group addqty"  id="add_quntity_'+(tgpd_current_item)+'"><div class="add_quntity"></div><div class="add_tap_amount"></div></div> ');
		console.log('line no 3850 >> tgpd_items.length ' + tgpd_items.length);
      if (tgpd_editing) {console.log('line no 3851');
         tgpd_editing = false;
         if (!(tgpd_current_item == -1) && !(tgpd_items.length == 0)) {
			 console.log('line no 3853');
            tgpd_show_current_item();
			 console.log('line no 3856');
         }
         if(tgpd_set_toolbar_navigation_buttons_state()){
			console.log('line no 3859');
			 
		 }else {
			 
			 console.log('line no 3863');
		 }
        if(tgpd_set_toolbar_editing_buttons_state()){
			 
			console.log('line no 3867');
		}
      } else {
		  console.log('line no 3869');
		  glass_piece_enabled();
		  
        if (!confirm('Delete this record?')){ return;
		 }else{
         tgpd_items.splice(tgpd_current_item, 1);
				 if(tgpd_current_item>0){
				tgpd_current_item--;
			}  
		 }		
		 
		console.log('line no 3876');
		
         if (tgpd_items.length == 0) {console.log('line no 3878');
            tgpd_current_item = -1;
            hideDamageAndDisclaimersDelete();
         } else {
            if (tgpd_current_item == tgpd_items.length) {
               tgpd_current_item = tgpd_items.length - 1;
            }
         }
		
         if (!(tgpd_current_item == -1) && !(tgpd_items.length == 0)) {
            tgpd_show_current_item();
             damageAnddisclaimers();
         } else {
            tgpd_clear_form();
			
			
         }
         if(tgpd_set_toolbar_navigation_buttons_state()){		 }
         if(tgpd_set_toolbar_editing_buttons_state()){			 }
		 
      }
	  if(tgpd_current_item<=0 && tgpd_items.length<=0){
		   
		  // tgpd_current_item=0;
		   jQuery('#select_tgpd_glass_treatment').prop('disabled','disabled');

		   jQuery('.chaulksection').find('.addqty').remove('.addqty');
		   	jQuery('.chaulksection').prepend('<div class="form-group addqty"  id="add_quntity_'+tgpd_current_item+'"><div class="add_quntity"></div><div class="add_tap_amount"></div><div class="add_tap_scaff"></div><div class="add_tap_channel"></div></div>');
			addAndUpdateTape('0', '', '',0,true);
			addAndUpdateCulk( '0',  '', '',0,true );
			addAndUpdateChannel('',0,0,true);
			addAndUpdateScaffolding('',0,0,true);
			
			
			jQuery('#glass_piece_holes_div').html('');
			addAndUpdateHoles('','','','','','','',0,true);
			
			glass_piece_disabled(); 
			jQuery(".chaulksection :input").prop('disabled','disabled');
			
			
			
		   
	  }
	 
	  
   console.log('line no 3922 >> tgpd_items.length ' + tgpd_items.length + " >> tgpd_current_item "+ tgpd_current_item);
	  
      if(tgpd_current_item==-1){
		 
    jQuery('#select_tgpd_glass_treatment').prop('disabled','disabled'); 
    }
	  //$('#extra_type_coating_treatment').hide();
	  //$('#extra_type_coating_treatment_second').hide();
//	if(tgpd_current_item>0){
//		tgpd_current_item--;
//	}  
      // set focus to first textbox
   });
   $('#btn_tgpd_refresh').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_show_current_item();
   });
   $('#btn_tgpd_copy_specs_plus_size').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_copying = true;
      tgpd_copy_operation = 0;
      tgpd_clipboard = tgpd_get_item_from_form();
      tgpd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tgpd_copy_specs').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_copying = true;
      tgpd_copy_operation = 1;
      tgpd_clipboard = tgpd_get_item_from_form();
      tgpd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tgpd_paste').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_print_item_to_form(tgpd_clipboard);
      tgpd_clipboard = {};
      tgpd_copying = false;
      tgpd_copy_operation = -1;
      tgpd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tgpd_first').click(function(event) {

			/*	if(saveclickserachtech() !=true)
				{
					return false;
				}*/

      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item = 0; //
      tgpd_show_current_item();
      tgpd_set_toolbar_navigation_buttons_state();showHideChulkAndTape();
       glass_piece_disabled();
	 
   });
   $('#btn_tgpd_previous').click(function(event) {

			/*	if(saveclickserachtech() !=true)
				{
					return false;
				}
				*/
      event.preventDefault();
      event.stopPropagation();
     	if(tgpd_current_item>0){
		tgpd_current_item--;
	} 
      tgpd_show_current_item();//
      tgpd_set_toolbar_navigation_buttons_state();
	  showHideChulkAndTape();
	  damageAnddisclaimers();
      glass_piece_disabled();
	 
   });
   
   $('#btn_tgpd_next').click(function(event) {

	    

		/*	if(saveclickserachtech() !=true)
			{
				return false;
			}
			*/



      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item++;//
      tgpd_show_current_item();
      tgpd_set_toolbar_navigation_buttons_state();
	  showHideChulkAndTape();
	  damageAnddisclaimers();
     glass_piece_disabled();
	  
   });
   $('#btn_tgpd_last').click(function(event) {

	/*if(saveclickserachtech() !=true)
			{
				return false;
			}*/
      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item = tgpd_items.length - 1;
	    //
      tgpd_show_current_item();
      showHideChulkAndTape();
      glass_piece_disabled();
	
   });

    $('#select_tgpd_glass_type').change(function(){
			var val = $(this).val();
			
     if($(this).val()=='igu_window' || $(this).val()=='igu_door_lite' || $(this).val()=='IGU (VIRACON)' || $(this).val()=='igu_spandrel' || $(this).val()=='igu_patio_door_or_panel' || $(this).val()=='bullet_proof_igu' || $(this).val()=='curved_igu' || $(this).val()=='igu_lami_temp_lami' || $(this).val()=='igu_sky_lite_temp_or_lami') {
		 $('#lamidrophide').hide();
		$('#treatment_without_glass_type').hide();  
		 	
			 if(back_date_entry ==true){  
				$('#litethick_see').show(); 
				/*1202018*/
				$('#treatment_without_glass_type').show();
								
			 } 
			 if( back_date_entry ==true && (val =='igu_sky_lite_temp_or_lami' || val =='igu_lami_temp_lami')){
					$('#lamidrophide').show();
					$('#litethick_see').hide();
					/*1202018*/
					$('#overall_thickness_without_glass_type').show();
				
					$('#overall_thickness_without_glass_type').insertBefore($('#treatment_without_glass_type'));
					
			}
			  
			 
		 
		}else{
		 $('#lamidrophide').hide();
		 $('#litethick_see').hide();
		
		  
	 }
   });
	 jQuery('#select_tgpd_glass_shape').change(function(){
			var tgpd_glass_shape = jQuery('#select_tgpd_glass_shape').val();

			if (typeof tgpd_glass_shape === 'undefined' || tgpd_glass_shape == null || tgpd_glass_shape == 'Rectangular/Square') tgpd_glass_shape = 'not_applicable';
				   if (tgpd_glass_shape.localeCompare('not_applicable') != 0) {
							jQuery('.shape-container').hide();
							jQuery('.shape-container-day').hide();
							jQuery('.shape-container-go').hide();
							jQuery('#tgpd-type_'+jQuery(this).val()+'-container').show();
				   }else{
					   		jQuery('.shape-container').hide();
							jQuery('.shape-container-day').show();
							jQuery('.shape-container-go').show();
					   }
   });
   		$('#handlea_label').hide();
				$('#handleb_label').hide();
				$('#handlec_label').hide();
				$('#handled_label').hide();
				$('#handlee_label').hide();

				$('.handle_bp_a').hide();
				$('.handle_bp_b').hide();
				$('.handle_bp_c').hide();
				$('.handle_bp_d').hide();
				$('.hendle_bp_e').hide();

				$('.handle_f_a').hide();
				$('.handle_f_b').hide();
				$('.handle_f_c').hide();
				$('.handle_f_d').hide();
				$('.hendle_f_e').hide();

				$('.handle_p_a').hide();
				$('.handle_p_b').hide();
				$('.handle_p_c').hide();
				$('.handle_p_d').hide();
				$('.handle_p_e').hide();

				$('.handle_wp_a').hide();
				$('.handle_wp_b').hide();
				$('.handle_wp_c').hide();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').hide()

   $('#all_replacement_heandle_type_wp_single_door').change(function(){

		var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
		var id= $(this).find('option:selected').val();
		$('.type_wp_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');



		var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){
				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show()
				$('.handle_wp_c').show()
				$('.handle_wp_d').hide()
				$('.handle_wp_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').hide();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').show();
				$('.handle_wp_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').hide();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').hide();

			}

  	})

    $('#all_replacement_heandle_type_p_single_door').change(function(){

		var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
		var id= $(this).find('option:selected').attr('id');
		$('.type_p_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');

		var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').hide();
				$('.handle_p_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){
				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').hide();
				$('.handle_p_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show()
				$('.handle_p_c').show()
				$('.handle_p_d').hide()
				$('.handle_p_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').hide();
				$('.handle_p_d').hide();

				$('.handle_p_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').show();
				$('.handle_p_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').hide();
				$('.handle_p_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').hide();
				$('.handle_p_d').hide();
				$('.handle_p_e').hide();

			}

  	})

	$('#all_replacement_heandle_type_bp_single_door').change(function(){

			var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
			var id= $(this).find('option:selected').attr('id');
			$('.type_bp_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');
			var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){
				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show()
				$('.handle_bp_c').show()
				$('.handle_bp_d').hide()
				$('.handle_bp_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').hide();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').show();
				$('.handle_bp_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').hide();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').hide();

			}

	})

	$('#all_replacement_heandle_type_f_single_door').change(function(){

			var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
			var id= $(this).find('option:selected').attr('id');
			$('.type_f_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');


			var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').hide();
				$('.hendle_f_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').hide();
				$('.hendle_f_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show()
				$('.handle_f_c').show()
				$('.handle_f_d').hide()
				$('.hendle_f_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').hide();
				$('.handle_f_d').hide();
				$('.hendle_f_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').show();
				$('.hendle_f_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').hide();
				$('.hendle_f_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').hide();
				$('.handle_f_d').hide();
				$('.hendle_f_e').hide();

			}
	})

	$('#all_replacement_heandle_type_a_single_door').change(function(){

      

			var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
			var id= $(this).find('option:selected').attr('id');
			$('.type_a_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');

			var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').hide();
				$('#handlee').show();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').hide();
				$('#handlee').show();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').show();

			}


			if(values=="pull_handle_2_holes"){


				$('#handlea').show();
				$('#handleb').show()
				$('#handlec').show()
				$('#handled').hide()
				$('#handlee').hide()

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').hide();

			}

			if(values=="pull_bar_2_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').hide();
				$('#handled').hide();
				$('#handlee').show();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').hide();
				$('#handled_label').hide();
				$('#handlee_label').show();

			}


			if(values=="ladder_pull_3_holes"){
				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').show();
				$('#handlee').hide();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').show();
				$('#handlee_label').hide();


			}

			if(values=="ladder_pull_2_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').hide();
				$('#handlee').hide();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').hide();

			}

			if(values=="single_pull_1_hole"){

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').hide();
				$('#handled_label').hide();
				$('#handlee_label').hide();

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').hide();
				$('#handled').hide();
				$('#handlee').hide();
			}

		});
			
			//$('#select_tgpd_glass_damage_waiver').hide();
			$('#fieldset_tgpd_glass_damage_waiver_reminder_section_for_select').hide();
			$('#fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers').hide();
        $('#fieldset_tgpd_glass_lift_inside_section1').hide();
         $('#fieldset_tgpd_glass_lift_outside').hide(); 
			$('#text_tgpd_glass_disclamers_reminder_section').hide();
			$('#text_tgpd_glass_damage_waiver_reminder_section').hide();
			$('.custom_inner_leval_thinkness').hide();
			$('.select_tgpd_lite_thickness_lebel').hide();
			$('#select_tgpd_lite_thickness').show();
			$('#verify_tgpd_lite_thickness').show(); //date @02-11-2017
			$('#vtlt').show();

				 $('#select_tgpd_innner_layer_thickness').change(function(){
					   if($(this).val()=='inner_thickness_custom'){
						$('.custom_inner_leval_thinkness').show();
					  }else{
						$('.custom_inner_leval_thinkness').hide();
					  }
				 });
				
			
				
				$('#select_tgpd_glass_disclamers').change(function(){
					if($(this).val()=='select_tgpd_glass_disclamers_for_text'){
						$('#text_tgpd_glass_disclamers_reminder_section').show();
						$('#fieldset_tgpd_glass_disclamers_reminder_section').show();
					}else{
						$('#text_tgpd_glass_disclamers_reminder_section').hide();
						$('#fieldset_tgpd_glass_disclamers_reminder_section').hide();
					}
					
				});
				
				$('#select_tgpd_glass_damage_waiver').change(function(){
					if($(this).val()=='select_tgpd_glass_damage_waiver_for_text'){
						jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').show();
						jQuery('#text_tgpd_glass_damage_waiver_reminder_section').show();
						jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').parent().addClass('damage_comment');
					}else{
						jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').hide();
						jQuery('#text_tgpd_glass_damage_waiver_reminder_section').hide();
						jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').parent().removeClass('damage_comment');
					}
				});
				

			 $('#select_tgpd_glass_type').change(function(){

				if($(this).val()=='igu_window' || $(this).val()=='IGU (VIRACON)' || $(this).val()=='igu_door_lite' ||  $(this).val()=='igu_sky_lite_temp_or_lami' || $(this).val()=='igu_lami_temp_lami' || $(this).val()=='igu_spandrel' || $(this).val()=='igu_patio_door_or_panel' ||  $(this).val()=='bullet_proof_igu' || $(this).val()=='curved_igu' ){

					$('#verify_tgpd_lite_thickness').show();////date @02-11-2017
					$('.select_tgpd_lite_thickness_lebel').show();
					$('#select_tgpd_lite_thickness').show();$('#vtlt').show();

				}else{

				//	$('.select_tgpd_lite_thickness_lebel').hide();
				//	$('#select_tgpd_lite_thickness').hide();
					$('#verify_tgpd_lite_thickness').hide();////date @02-11-2017
				//	$('#vtlt').hide();
				}
				
				
			});
	var ajaxurl_image = jQuery('#ajax_url_image_url').val();
    jQuery( "#text_tgpd_glass_secondryquote" ).autocomplete({
      source: function( request, response ) {
        jQuery.ajax( {
         url: ajaxurl_image,
         dataType: "json",
          data: {
			'action':'get_quotenumbers',
            'term': request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 4,
      select: function( event, ui ) {
        //ui.item.id
		jQuery('#childjobid').val(ui.item.id);
      }
    } );
});
function saveclickserachtech()
{

			if (!validateFormGPD()) {
			console.log('btn_tgpd_save.click: validateFormGPD() -> false');
			return false;
			}
			console.log('btn_tgpd_save.click: validateFormGPD() -> true');
			var tgpd_item = tgpd_get_item_from_form();
			if (tgpd_editing) {
			tgpd_editing = false;
			tgpd_items.push(tgpd_item);
			if (tgpd_current_item == -1 || tgpd_items.length == 1) {
			tgpd_current_item = 0;
			}
			tgpd_show_current_item();
			tgpd_set_toolbar_navigation_buttons_state();
			tgpd_set_toolbar_editing_buttons_state();
			} else {
			tgpd_items[tgpd_current_item] = tgpd_item;
			}

			return true;
			// set focus to first textbox
}


 //value without decimal
 	function isNumberkey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;

    return true;

	}
	//value with decimal

	function myNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

	// Take number two digit after decimal

 	function myfunction(){
 	jQuery('.text_tagd_shape_left_leg, .text_tagd_shape_width,.text_tagd_shape_right_leg,.text_tagd_shape_top,.text_tagd_shape_base,.text-num').keypress(function(event) {
    var $this = jQuery(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
       ((event.which < 48 || event.which > 57) &&
       (event.which != 0 && event.which != 8))) {
           event.preventDefault();
    }

    var text = jQuery(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (event.which != 0 && event.which != 8) &&
        (jQuery(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
    }
});

jQuery('.number').bind("paste", function(e) {
var text = e.originalEvent.clipboardData.getData('Text');
if (jQuery.isNumeric(text)) {
    if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
        e.preventDefault();
       jQuery(this).val(text.substring(0, text.indexOf('.') + 3));
   }
}
else {
        e.preventDefault();
     }
});

	}
jQuery('.verify-hidden-checkbox').change(function(){
  $( this ).addClass('verify-active');
})

jQuery('#exterior_light_thickness').change(function() {
		jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked',true).addClass('verify-active');
	});

	jQuery('#interior_light_thickness').change(function() {  jQuery('#verify_tgpd_interior_lite_thickness').prop('checked',true).addClass('verify-active');;	});

jQuery('#verify_go_size_width_text').change(function() {  jQuery('#verify_go_size_widths').prop('checked',true).addClass('verify-active');;	});

jQuery('#text_tgpd_go_height1').change(function() {  jQuery('#verify_go_size_heights').prop('checked',true).addClass('verify-active');;	});


	jQuery('#select_tgpd_innner_layer_thickness').change(function() {  jQuery('#verify_tgpd_inner_layer_thickness').prop('checked',true).addClass('verify-active');;});

	jQuery('#select_tgpd_overall_thickness').change(function(){
		jQuery('#verify_tgpd_overall_thickness').prop('checked',true).addClass('verify-active');;
	});
	
	jQuery('#select_tgpd_exterior_glass_type').change(function(){
		jQuery('#verify_tgpd_exterior_glass_type').prop('checked',true).addClass('verify-active');;
		jQuery('#verify_tgpd_interior_glass_type').prop('checked',true).addClass('verify-active');;
		jQuery('#verify_tgpd_interior_glass_type').prop('checked',true).addClass('verify-active');;
	});
	
  jQuery('#select_tgpd_treatment_exterior_glass').change(function(){
      jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked',true).addClass('verify-active');;
      jQuery('#verify_tgpd_treatment_interior_glass').prop('checked',true).addClass('verify-active');;
   });
	jQuery('#select_tgpd_interior_glass_type').change(function(){
		jQuery('#verify_tgpd_interior_glass_type').prop('checked',true).addClass('verify-active');;
	});
	jQuery('#select_tgpd_interior_glass_type_non_igu').change(function(){
		jQuery('#verify_tgpd_interior_glass_type').prop('checked',true).addClass('verify-active');;
	});
   jQuery('#select_tgpd_exterior_coating_family_glass_type').change(function(){
      
      jQuery('#verify_tgpd_exterior_coating_family_glass_type ').prop('checked',true).addClass('verify-active');
      
      
      var cal_val1  = $(this).val();
      //jQuery('#select_tgpd_interior_coating_family_glass_type').val(cal_val1);
      
       jQuery('#verify_tgpd_interior_coating_family_glass_type ').prop('checked',true).addClass('verify-active');
      
   
   });

    jQuery('#select_tgpd_exterior_coating_family_color_glass_type').change(function(){
        
      
      jQuery('#verify_tgpd_exterior_coating_family_color_glass_type').prop('checked',true).addClass('verify-active');
      var cal_val  = $(this).val();
      jQuery('#select_tgpd_interior_oating_family_color_glass_type').val('1-CLEAR');
      
       jQuery('#verify_tgpd_interior_coating_family_color_glass_type ').prop('checked',true).addClass('verify-active');
   
   });

     jQuery('#select_tgpd_interior_oating_family_color_glass_type').change(function(){
      
      jQuery('#verify_tgpd_interior_coating_family_color_glass_type ').prop('checked',true).addClass('verify-active');
     
     var  int_colr = $(this).val();
     if(int_colr == '1-CLEAR'){
      jQuery('#select_tgpd_interior_coating_family_glass_type ').val('-');
      jQuery('#text_tgpd_coating_interior_type ').val('N/A');


      }
   
   });

    jQuery('#select_tgpd_interior_coating_family_glass_type').change(function(){
      

      jQuery('#verify_tgpd_interior_coating_family_glass_type').prop('checked',true).addClass('verify-active');
      
      var  int_cot_f = $(this).val();
     if(int_cot_f == '-'){
          jQuery('#select_tgpd_interior_oating_family_color_glass_type').val('1-CLEAR');
          jQuery('#text_tgpd_coating_interior_type ').val('N/A');
      } 


   });
	
	jQuery('#select_tgpd_treatment_exterior_glass').change(function(){
		jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked',true).addClass('verify-active');;
		jQuery('#verify_tgpd_treatment_interior_glass').prop('checked',true).addClass('verify-active');;
	});
	
	jQuery('#select_tgpd_treatment_interior_glass').change(function(){
		jQuery('#verify_tgpd_treatment_interior_glass').prop('checked',true).addClass('verify-active');;
	});
	
	
	jQuery('#verify_tgpd_interior_color_glass_type').prop('checked',true).addClass('verify-active');;
	jQuery('#select_tgpd_interior_color_glass_type').change(function(){
		jQuery('#verify_tgpd_interior_color_glass_type').prop('checked',true).addClass('verify-active');;
	});
	
	jQuery('#text_tgpd_coating_interior_type').change(function(){
		jQuery('#verify_tgpd_coating_interior_type').prop('checked',true).addClass('verify-active');;
   var coating_val = $(this).val();
      if(coating_val == 'N/A'){
         
         jQuery('#select_tgpd_interior_oating_family_color_glass_type').val('1-CLEAR');
         jQuery('#select_tgpd_interior_coating_family_glass_type').val('-');
      
      }

	});
	
	jQuery('#text_tgpd_coating_exterior_type').change(function(){
		jQuery('#verify_tgpd_coating_exterior_type').prop('checked',true).addClass('verify-active');;
		//jQuery('#verify_tgpd_coating_interior_type').prop('checked',true).addClass('verify-active');;
	});

	jQuery('#select_tgpd_lite_thickness').change(function() {  		jQuery('#verify_tgpd_lite_thickness').prop('checked',true).addClass('verify-active');;		});

	jQuery('#select_tgpd_glass_color').change(function() {  	jQuery('#verify_tgpd_glass_color').prop('checked',true).addClass('verify-active');;	});

	jQuery('#text_tgpd_glass_color_note').change(function() {  	jQuery('#verify_tgpd_glass_color_note').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_glass_set').change(function() {  	jQuery('#verify_tgpd_glass_set').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_glass_treatment').change(function() {  	jQuery('#verify_tgpd_glass_treatment').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_spacer_color').change(function() {  	jQuery('#verify_tgpd_spacer_color').prop('checked',true).addClass('verify-active');;	});




	jQuery('#text_tgpd_glass_lift').change(function() {
    jQuery('#verify_tgpd_glass_lift').addClass('verify-active');
		if(jQuery('#text_tgpd_glass_lift').val().length>0){
			jQuery('#verify_tgpd_glass_lift').prop('checked',true);
		}else if(jQuery('#text_tgpd_glass_lift').val().length==0){
			jQuery('#verify_tgpd_glass_lift').prop('checked',false);
		}
	});

	jQuery('#text_tgpd_glass_inside_lift_with_glass_type').change(function() {
    jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').addClass('verify-active');
		if(jQuery('#text_tgpd_glass_inside_lift_with_glass_type').val().length>0){
			jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').prop('checked',true);
		}else if(jQuery('#text_tgpd_glass_inside_lift_with_glass_type').val().length==0){
			jQuery('#verify_tgpd_glass_inside_lift_with_glass_type').prop('checked',false);
		}
	});
	jQuery('#text_tgpd_glass_outside_lift_with_glass_type').change(function() {
    jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').addClass('verify-active');
		if(jQuery('#text_tgpd_glass_outside_lift_with_glass_type').val().length>0){
			jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').prop('checked',true);
		}else if(jQuery('#text_tgpd_glass_outside_lift_with_glass_type').val().length==0){
			jQuery('#verify_tgpd_glass_outside_lift_with_glass_type').prop('checked',false);
		}
	});


	jQuery('#text_tgpd_daylight_width').change(function() {
    jQuery('#text_tgpd_daylight_width').addClass('verify-active');
		if(jQuery('#text_tgpd_daylight_width').val().length>0){
			jQuery('#verify_tgpd_daylight_width').prop('checked',true);
     
		}else if(jQuery('#text_tgpd_daylight_width').val().length==0){
			jQuery('#verify_tgpd_daylight_width').prop('checked',false);
      
		}
	});


	//jQuery('#text_tgpd_daylight_height').change(function() {  	jQuery('#verify_tgpd_daylight_height').prop('checked',true);	});

	jQuery('#text_tgpd_daylight_height').change(function() {
    jQuery('#text_tgpd_daylight_height').addClass('verify-active');
		if(jQuery('#text_tgpd_daylight_height').val().length>0){
			jQuery('#verify_tgpd_daylight_height').prop('checked',true);
     
		}else if(jQuery('#text_tgpd_daylight_height').val().length==0){
			jQuery('#verify_tgpd_daylight_height').prop('checked',false);
      
			}

	});


	jQuery('#text_tgpd_go_width').change(function() {
    jQuery('#text_tgpd_go_width').addClass('verify-active');
		if(jQuery('#text_tgpd_go_width').val().length>0){
			jQuery('#verify_go_size_width').prop('checked',true);
		}else if(jQuery('#text_tgpd_go_width').val().length==0){
			jQuery('#verify_go_size_width').prop('checked',false);
		}

	});
	
	jQuery('#text_tgpd_go_height').change(function() {
		jQuery('#text_tgpd_go_height').addClass('verify-active');
		if(jQuery('#text_tgpd_go_height').val().length>0){
			jQuery('#verify_go_size_height').prop('checked',true);
		}else if(jQuery('#text_tgpd_go_height').val().length==0){
			jQuery('#verify_go_size_height').prop('checked',false);
		}
	});
/*********************************************************************************************************************************/
function tgpd_init_form_state_for_back_date_entry(){
	jQuery('#fieldset_tgpd_glass_type').show();
	jQuery('#litethick_see').show();
	jQuery('.select_tgpd_lite_thickness_lebel').show();
	jQuery('#select_tgpd_lite_thickness').show();
	jQuery('#lift_inside_with_glass_type').hide();
	jQuery('#lift_outside_with_glass_type').hide();
	jQuery('#grids_and_fabrication').hide();
	//jQuery('.custom_addon.input-group-addon').show();
	jQuery('.custom_addon>.input-group-addon').show();
	//servic selection
	//jQuery('#select_tgpd_servicetype option[value="all_new"],#select_tgpd_servicetype option[value="Deliver_Only"]').remove();
	//jQuery('#select_tgpd_glass_type option[value="PLATE (Spandrel)"]').remove();
		
}
  $('#select_tgpd_exterior_glass_type').change(function(){	
	$('#select_tgpd_interior_glass_type').val($("#select_tgpd_exterior_glass_type").val());
}); 

$('#select_tgpd_treatment_exterior_glass').change(function(){	
	$('#select_tgpd_treatment_interior_glass').val($("#select_tgpd_treatment_exterior_glass").val());
});

$('#select_tgpd_exterior_color_glass_type').change(function(){	
	$('#select_tgpd_interior_color_glass_type').val('Clear');
	$('#verify_tgpd_exterior_color_glass_type').prop('checked',true).addClass('verify-active');;
	$('#verify_tgpd_interior_color_glass_type').prop('checked',true).addClass('verify-active');;
});

$('#text_tgpd_coating_exterior_type').change(function(){	
	//$('#text_tgpd_coating_interior_type').val($("#text_tgpd_coating_exterior_type").val());
	
});
 

$('.inside_lift_with_glass_type').css('visibility','hidden');
$("#text_tgpd_coating_first_position").prop( "checked", true );
$("#text_tgpd_coating_second_position").prop( "checked", true );
$('#text_tgpd_coating_first_position').change(function(){
		var coating_first = $(this).prop('checked')?1:0;
			if(coating_first == 1){
				$('#text_tgpd_coating_exterior_type').show();
				$('#coating_first_hide_glass_type').show();	
				$('.verify_tgpd_coating_exterior_type').css('visibility', 'visible');	
			}
			else{
				$('#text_tgpd_coating_exterior_type').show();
				$('#coating_first_hide_glass_type').show();
				$('.verify_tgpd_coating_exterior_type').css('visibility', 'visible');
			}	
	}); 
	$('#text_tgpd_coating_second_position').change(function(){
		var coating_second = $(this).prop('checked')?1:0;
			if(coating_second == 0){
				$('.verify_tgpd_coating_interior_type').css('visibility','visible');
				$('#text_tgpd_coating_interior_type').show();
				$('#coating_second_hide_glass_type').show();
			}
			else{
				$('.verify_tgpd_coating_interior_type').css('visibility','visible');
				$('#text_tgpd_coating_interior_type').show();
				$('#coating_second_hide_glass_type').show();
			}
		
	})
	
	$('#text_lift_inside_position').change(function(){
		var lift_inside = $(this).prop('checked')?1:0;
			if(lift_inside == 1){
				$('#lift_inside_hide_on_glass_type').hide();		
			}
			else{
				$('#lift_inside_hide_on_glass_type').show();			
			}
	});
	$('#text_lift_outside_position').change(function(){
		var lift_outside = $(this).prop('checked')?1:0;
			if(lift_outside == 1){
				$('#lift_outside_hide_on_glass_type').hide();
			}else{
				$('#lift_outside_hide_on_glass_type').show();
			}
	});

	$('#text_tgpd_fabrication_polished_edges_hide').change(function(){
		var lift_fabrication = $(this).prop('checked')?1:0;
			if(lift_fabrication == 1){
				$('#text_tgpd_fabrication_polished_edges').hide();
			}else{
				$('#text_tgpd_fabrication_polished_edges').show();
			}
	});

	$('#text_tgpd_fabrication_pattern_hide').change(function(){
		var lift_pattern = $(this).prop('checked')?1:0;
			if(lift_pattern == 1){
				$('#text_tgpd_fabrication_pattern').hide();
			}else{
				$('#text_tgpd_fabrication_pattern').show();
			}
	});
  $('#text_tgpd_fabrication_gride_hide').change(function(){
       
       
      var lift_pattern = $(this).prop('checked')?1:0;
         if(lift_pattern == 1){
            $('#grids_and_fabrication').hide();
         }else{
            $('#grids_and_fabrication').show();
         }
   });
	
	$('#text_tgpd_fabrication_holes_hide_new').change(function(){
		var fabrication_holes = $(this).prop('checked')?1:0;
			if(fabrication_holes == 1){
				$('#glass_piece_holes_div').hide();
			}else{
				$('#glass_piece_holes_div').show();
			}
	});
	
	$('#text_tgpd_fabrication_holes_hide_old').change(function(){
		var fabrication_holes_hide = $(this).prop('checked')?1:0;
			if(fabrication_holes_hide == 1){
				$('#text_tgpd_fabrication_holes').hide();
			}else{
				$('#text_tgpd_fabrication_holes').show();
			}
	});

	function reset_all_igu_non_fields(){
		$('#select_tgpd_exterior_glass_type').val('not_applicable');
		$('#select_tgpd_interior_glass_type_non_igu').val('not_applicable');
		$('#select_tgpd_interior_glass_type').val('not_applicable');
		$('#verify_tgpd_exterior_glass_type').prop('checked', false).removeClass('verify-active');
		jQuery('#select_tgpd_exterior_glass_type').val('not_applicable');
		jQuery('#select_tgpd_interior_glass_type_non_igu').val('not_applicable');
		jQuery('#select_tgpd_treatment_exterior_glass').val('not_applicable');
		jQuery('#select_tgpd_exterior_color_glass_type').val('not_applicable');
		jQuery('#text_tgpd_coating_exterior_type').val('not_applicable');
		jQuery('#select_tgpd_interior_glass_type').val('not_applicable');
		jQuery('#select_tgpd_treatment_interior_glass').val('not_applicable');
		jQuery('#select_tgpd_interior_color_glass_type').val('not_applicable');
		jQuery('#text_tgpd_coating_interior_type').val('not_applicable');
		jQuery('#select_tgpd_overall_thickness').val('not_applicable');
		jQuery('#verify_tgpd_overall_thickness').prop('checked',false).removeClass('verify-active');;
		jQuery('#verify_tgpd_exterior_glass_type').prop('checked',false).removeClass('verify-active');;
		jQuery('#verify_tgpd_treatment_exterior_glass').prop('checked',false).removeClass('verify-active');;
		jQuery('#verify_tgpd_exterior_color_glass_type').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_coating_exterior_type').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_interior_glass_type').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_treatment_interior_glass').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_interior_color_glass_type').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_coating_interior_type').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_lite_thickness').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_glass_color').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_glass_set').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_glass_lift').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_spacer_color').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_interior_lite_thickness').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_inner_layer_thickness').prop('checked', false).removeClass('verify-active');;
		jQuery('#verify_tgpd_glass_treatment').prop('checked', false).removeClass('verify-active');;
		jQuery('#select_tgpd_spacer_color').val(['not_applicable']);
		jQuery('#text_tgpd_glass_lift').val(['not_applicable']);
		jQuery('#text_tgpd_glass_lift').val('');
		jQuery('#select_tgpd_glass_set').val(['not_applicable']);
		jQuery('#select_tgpd_glass_color').val(['not_applicable']);
		jQuery('#verify_tgpd_lite_thickness').prop('checked', false).removeClass('verify-active');;
		jQuery('#select_tgpd_lite_thickness').val(['not_applicable']);
		jQuery('#exterior_light_thickness').val('');
		jQuery('#interior_light_thickness').val('');
		jQuery('#select_tgpd_innner_layer_thickness').val(['not_applicable']);
		
	}	
	
	
$('#select_tgpd_interior_color_glass_type').val('Clear');
$('#text_tgpd_coating_exterior_type').val('N/A');
$('#text_tgpd_coating_interior_type').val('N/A');
//$('#select_tgpd_spacer_color').val('Clear');
/* jQuery('#select_tgpd_glass_set').val('Snap Beads, Aluminum, INTERIOR (+3/4)');
jQuery('#verify_tgpd_glass_set').prop('checked',true).addClass('verify-active');; */
$('#clear_both_forms_values').click(function(){
	$('#text_tgpd_coating_exterior_type').val('');
	$('#text_tgpd_coating_interior_type').val('');
	$('#select_tgpd_exterior_color_glass_type').val('Please select glass color');
	$('#select_tgpd_interior_color_glass_type').val('Please select glass color');
	//$('#select_tgpd_treatment_exterior_glass').val('Please select glass treatment');
	$('#select_tgpd_treatment_interior_glass').val('Please select glass treatment');
	$('#select_tgpd_exterior_glass_type').val('Please select overall thickness');
	$('#select_tgpd_interior_glass_type').val('Please select overall thickness');
	$('#select_tgpd_interior_glass_type_non_igu').val('Please select overall thickness');
}); 

jQuery('#select_tgpd_glass_type').trigger("change");
$("#extra_type_coating_treatment").hide();
//$("#daylightsetglasspiece").hide();
$("#extra_type_coating_treatment_second").hide();
$('#lift_inside_with_glass_type').hide();
$('#lift_outside_with_glass_type').hide();


$('#select_tgpd_glass_type').change(function(){ 

//	tgpd_db_val=true;
	var glass_type_change = $('#select_tgpd_glass_type').val();  
	if(glass_type_change =='igu_window' || glass_type_change =='igu_door_lite' ||  glass_type_change =='igu_spandrel' || glass_type_change =='igu_patio_door_or_panel' || glass_type_change =='bullet_proof_igu' || glass_type_change =='curved_igu'|| glass_type_change =='igu_lami_temp_lami'|| glass_type_change =='igu_sky_lite_temp_or_lami' || glass_type_change =='IGU (VIRACON)'){
		$('.customck-thickness_int_non_igu').hide();
		if(tgpd_db_val==false){
			reset_all_igu_non_fields();
		}
	
		$('.customck-thickness_int').show();
		$("#hide_for_non_igu").show();		
		$("#sapacer_color_glass_type").show();
		$("#daylightsetglasspiece").show();
		$('#overall_thickness_without_glass_type').insertBefore($('#extra_type_coating_treatment'));
		 /*sag-153*/
      $('#glass_pricing_with_glass_type_select_glass').insertBefore($('.check_price_insert'));     

	
		
			//check back date entry
			if(back_date_entry ==true){
					if(glass_type_change =='igu_sky_lite_temp_or_lami' || glass_type_change =='igu_lami_temp_lami'){
						$('#overall_thickness_without_glass_type').hide();
					}
					else{
						$('#overall_thickness_without_glass_type').show();
						
						
					}			
				$('#litethick_see').show();
				$('#select_tgpd_glass_treatment').show();
		//		$('#litethick_see').insertAfter($('#overall_thickness_without_glass_type'));
				$("#sapacer_color_glass_type").show();
				$("#fieldset_tgpd_glass_type").show();
				$('#lift_without_glass_type').show();
				$("#extra_type_coating_treatment_second").hide();
				$("#lift_inside_with_glass_type").hide();
				$("#lift_outside_with_glass_type").hide();
				$("#glass_color_with_glass_type").show();
				$('#treatment_without_glass_type').show();
			//	$('#grids_and_fabrication').insertBefore($('#glass_pricing_with_glass_type_select_glass'));			
			}else{
					$('.litethick_see').hide();
					$("#extra_type_coating_treatment").show();
					$("#extra_type_coating_treatment_second").show();					
					$('#glass_color_with_glass_type').hide();
					$('#lift_without_glass_type').hide();
					$('#spacer_color_without_glass_type').insertBefore($('#extra_type_coating_treatment_second'));
					$('#lift_without_glass_type').hide();
					$('#coating_without_glass_type').hide();
					$('#lift_inside_with_glass_type').show();
					$('#lift_outside_with_glass_type').show();
					$('#text_tgpd_coating_exterior_type').show();
					$('#verify_tgpd_coating_interior_type').show();
					$('#text_tgpd_coating_interior_type').show();	
					$('#coating_first_hide_glass_type').show();	
					$('#coating_second_hide_glass_type').show();
					$('.verify_tgpd_coating_exterior_type').css('visibility', 'visible');
					$('#overall_thickness_without_glass_type').show();
					$('#text_tgpd_coating_second_position').hide();
					//$('#select_tgpd_spacer_color').val('Clear');
					
			}
		
				
	}
	else{
		if(tgpd_db_val==false){
			reset_all_igu_non_fields();
		}
		$("#extra_type_coating_treatment").hide();
		$("#extra_type_coating_treatment_second").show();
		$("#extra_type_coating_treatment_second-non-igu").show();	
		$("#sapacer_color_glass_type").hide();
	//	$('#spacer_color_without_glass_type').insertAfter($('#lift_without_glass_type'));
		//$('#overall_thickness_without_glass_type').insertBefore($('#extra_type_coating_treatment'));
		//$('#treatment_without_glass_type').insertAfter($('#litethick_see'));
	//	$('#glass_set_without_glass_select').insertAfter($('#extra_type_coating_treatment_second'));
		$('#treatment_without_glass_type').hide();
		$('#glass_color_with_glass_type').hide();
		$('#lift_without_glass_type').hide();			
		$('#coating_without_glass_type').hide();			
		$('#glass_pricing_with_glass_type').show();			
		$('#lift_inside_with_glass_type').show();			
		$('#lift_outside_with_glass_type').show();
		$('#coating_first_hide_glass_type').show();	
		$('#coating_second_hide_glass_type').show();		
		//
		$('#grids_and_fabrication').show();
		$('#extra_type_coating_treatment_second-non-igu').hide();
		$('.customck-thickness_int_non_igu').show();
		$('.customck-thickness_int').hide();
		/* jQuery('#select_tgpd_glass_set').val('Snap Beads, Aluminum, INTERIOR (+3/4)');
		jQuery('#verify_tgpd_glass_set').prop('checked',true).addClass('verify-active');; */
		//for back date
		
		if(back_date_entry ==true){
			$('#overall_thickness_without_glass_type').show();
			$('#treatment_without_glass_type').show();
			$('#glass_color_with_glass_type').show();
			$('#glass_color_with_glass_type').show();
			$('#lift_without_glass_type').show();
			$('#glass_set_without_glass_select').insertBefore($('#lift_without_glass_type'));
			$("#extra_type_coating_treatment_second").hide();
			$('#lift_inside_with_glass_type').hide();			
			$('#lift_outside_with_glass_type').hide();
			$('.litethick_see').show();
			//$('#grids_and_fabrication').hide();		
		}
		else{
		$('#overall_thickness_without_glass_type').hide();			
		$("#hide_for_non_igu").hide();
		$('.litethick_see').hide();
				
		}
	}
	
	
});

/**************************************************************************************************************/
	jQuery('#select_tgpd_glass_set').change(function() {

    fillSetFormula();

	})
	


	function glass_piece_disabled(){

      console.log('glass_pice_disabled'+ tgpd_items.length); 
    if(tgpd_items.length < 1 ){
     $('#glass-piece-details').find('input, textarea, select').prop('disabled','disabled');
  }
	
	/*jQuery('#select_tgpd_servicetype').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_location').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_quantity').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_shape').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_type').prop('disabled','disabled');	
	jQuery('#select_tgpd_overall_thickness').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_set').prop('disabled','disabled');	
	jQuery('#text_tgpd_daylight_width').prop('disabled','disabled');	
	jQuery('#text_tgpd_daylight_height').prop('disabled','disabled');	
	jQuery('#text_tgpd_extra_width').prop('disabled','disabled');	
	jQuery('#text_tgpd_extra_height').prop('disabled','disabled');	
	jQuery('#text_tgpd_go_width').prop('disabled','disabled');//select_tgpd_glass_treatment
	jQuery('#text_tgpd_go_height').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_inside_lift_with_glass_type').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_outside_lift_with_glass_type').prop('disabled','disabled');	
	jQuery('#textarea_tgpd_instructions').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_materials_caulk_amount').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_materials_caulk_type').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_materials_scaffolding_type').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_materials_tape_amount').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_materials_tape_type').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_materials_channel').prop('disabled','disabled');	
	jQuery('#text_tgpd_miscellaneous').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_treatment').prop('disabled','disabled');	
	jQuery('#select_tgpd_glass_color').prop('disabled','disabled');	
	jQuery('#text_tgpd_glass_lift').prop('disabled','disabled');	
	jQuery('#select_tgpd_spacer_color').prop('disabled','disabled');	
	jQuery('#select_tgpd_exterior_glass_type').prop('disabled','disabled');	
	jQuery('#select_tgpd_treatment_exterior_glass').prop('disabled','disabled');	
	jQuery('#select_tgpd_exterior_color_glass_type').prop('disabled','disabled');	
	jQuery('#text_tgpd_coating_exterior_type').prop('disabled','disabled');	
	jQuery('#select_tgpd_interior_glass_type').prop('disabled','disabled');	
	jQuery('#select_tgpd_treatment_interior_glass').prop('disabled','disabled');	
	jQuery('#select_tgpd_interior_color_glass_type').prop('disabled','disabled');	
	jQuery('#text_tgpd_coating_interior_type').prop('disabled','disabled');
	jQuery('#select_tgpd_lite_thickness').prop('disabled','disabled'); 
	jQuery('#text_tgpd_glass_reminders_solar_film_type').prop('disabled','disabled'); 
	jQuery('#text_tgpd_glass_reminders_solar_film_source').prop('disabled','disabled'); 
	jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment').prop('disabled','disabled'); 
	jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment').prop('disabled','disabled'); 
	jQuery('#text_tgpd_fabrication_polished_edges').prop('disabled','disabled'); 
	jQuery('#text_tgpd_fabrication_holes').prop('disabled','disabled'); 
	jQuery('#text_tgpd_fabrication_pattern').prop('disabled','disabled'); 
	jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer').prop('disabled','disabled'); 
	jQuery('#number_tgpd_pattern_horizontal_grids').prop('disabled','disabled'); 
	jQuery('#number_tgpd_pattern_vertical_grids').prop('disabled','disabled'); 
	jQuery('#select_tgpd_grids_thickness').prop('disabled','disabled'); 
	jQuery('#text_tgpd_grids_color').prop('disabled','disabled'); */
  /*sag-158*/
   //jQuery('#text_tgpd_grids_color_custom').prop('disabled','disabled');
   /*sag-158*/
	/*jQuery('#select_tgpd_interior_glass_type_non_igu').prop('disabled','disabled'); 
	jQuery('.verify-hidden-checkbox').prop('disabled','disabled'); 
	//jQuery('.btn_pluse').prop('disabled','disabled'); 
	//jQuery('.btn_pluse').removeAttr('href');
//jQuery('.btn_pluse').attr('href','javascript:void(0)');	
	//jQuery('.btn_pluse').click('disabled','disabled'); 
	jQuery(".btn_pluse").addClass("disable_a_href"); 
	//jQuery('.removeItem1').attr('disabled','disabled'); 
	
	//jQuery('#btn11').prop('disabled','disabled'); 
	//jQuery('#btn11').attr("disabled","disabled"); 
	//jQuery('#btn11').prop("disabled","disabled"); 
	
	jQuery('#text_tgpd_glass_materials_caulk_amount_0_0').prop('disabled','disabled');
	jQuery('#select_tgpd_glass_materials_caulk_type_0_0').prop('disabled','disabled');

	jQuery('#select_tgpd_glass_materials_scaffolding_type_0_0').prop('disabled','disabled');
	jQuery('#select_tgpd_glass_materials_quantity_0_0').prop('disabled','disabled');

	jQuery('#text_tgpd_glass_materials_tape_amount_0_0').prop('disabled','disabled');
	jQuery('#select_tgpd_glass_materials_tape_type_0_0').prop('disabled','disabled');
	jQuery('#text_tgpd_glass_materials_channel_0_0').prop('disabled','disabled');

	jQuery('#exterior_light_thickness').prop('disabled','disabled');
	jQuery('#interior_light_thickness').prop('disabled','disabled');
	jQuery('#select_tgpd_innner_layer_thickness').prop('disabled','disabled');
	
	jQuery('#select_tgpd_glass_damage_waiver').prop('disabled','disabled');
	jQuery('#text_tgpd_glass_damage_waiver_reminder_section').prop('disabled','disabled');
	jQuery('#select_tgpd_glass_disclamers').prop('disabled','disabled');
	jQuery('#text_tgpd_glass_disclamers_reminder_section').prop('disabled','disabled');
   */
		
	}

	
	function glass_piece_enabled(){

      console.log('enabled glass');
   $('#glass-piece-details').find('input, textarea, select').removeAttr('disabled');		
   /* jQuery('#exterior_light_thickness').removeAttr('disabled');
   jQuery('#interior_light_thickness').removeAttr('disabled');
   jQuery('#select_tgpd_innner_layer_thickness').removeAttr('disabled');		   
	jQuery('#select_tgpd_servicetype').removeAttr('disabled');	
	jQuery('#select_tgpd_servicetype').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_location').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_quantity').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_shape').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_type').removeAttr('disabled');	
	jQuery('#select_tgpd_overall_thickness').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_set').removeAttr('disabled');	
	jQuery('#text_tgpd_daylight_width').removeAttr('disabled');	
	jQuery('#text_tgpd_daylight_height').removeAttr('disabled');	
	jQuery('#text_tgpd_extra_width').removeAttr('disabled');	
	jQuery('#text_tgpd_extra_height').removeAttr('disabled');	
	jQuery('#text_tgpd_go_width').removeAttr('disabled');	
	jQuery('#text_tgpd_go_height').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_inside_lift_with_glass_type').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_outside_lift_with_glass_type').removeAttr('disabled');	
	jQuery('#textarea_tgpd_instructions').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_materials_caulk_amount').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_materials_caulk_type').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_materials_scaffolding_type').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_materials_tape_amount').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_materials_tape_type').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_materials_channel').removeAttr('disabled');	
	jQuery('#text_tgpd_miscellaneous').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_treatment').removeAttr('disabled');	
	jQuery('#select_tgpd_glass_color').removeAttr('disabled');	
	jQuery('#text_tgpd_glass_lift').removeAttr('disabled');
	jQuery('#select_tgpd_spacer_color').removeAttr('disabled');
	jQuery('#select_tgpd_exterior_glass_type').removeAttr('disabled');
	jQuery('#select_tgpd_treatment_exterior_glass').removeAttr('disabled');
	jQuery('#select_tgpd_exterior_color_glass_type').removeAttr('disabled');
	jQuery('#text_tgpd_coating_exterior_type').removeAttr('disabled');
	jQuery('#select_tgpd_interior_glass_type').removeAttr('disabled');
	jQuery('#select_tgpd_treatment_interior_glass').removeAttr('disabled');
	jQuery('#select_tgpd_interior_color_glass_type').removeAttr('disabled');
	jQuery('#text_tgpd_coating_interior_type').removeAttr('disabled');
	jQuery('#select_tgpd_lite_thickness').removeAttr('disabled');
	jQuery('#text_tgpd_glass_reminders_solar_film_type').removeAttr('disabled'); 
	jQuery('#text_tgpd_glass_reminders_solar_film_source').removeAttr('disabled');
	jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment').removeAttr('disabled');
	jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment').removeAttr('disabled'); 
	jQuery('#text_tgpd_fabrication_polished_edges').removeAttr('disabled');
	jQuery('#text_tgpd_fabrication_holes').removeAttr('disabled');
	jQuery('#text_tgpd_fabrication_pattern').removeAttr('disabled');
	jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer').removeAttr('disabled');
	jQuery('#number_tgpd_pattern_horizontal_grids').removeAttr('disabled'); 
	jQuery('#number_tgpd_pattern_vertical_grids').removeAttr('disabled'); 
	jQuery('#select_tgpd_grids_thickness').removeAttr('disabled');
	jQuery('#text_tgpd_grids_color').removeAttr('disabled');*/
  /*sag-158*/
   //jQuery('#text_tgpd_grids_color_custom').removeAttr('disabled');
   /*sag-158*/
	//jQuery('#select_tgpd_interior_glass_type_non_igu').removeAttr('disabled');
	//jQuery('.verify-hidden-checkbox').removeAttr('disabled'); 
	
	//jQuery(".btn_pluse").removeClass("disable_a_href"); 
	
	//jQuery('#select_tgpd_glass_damage_waiver').removeAttr('disabled');
	//jQuery('#text_tgpd_glass_damage_waiver_reminder_section').removeAttr('disabled');
	//jQuery('#select_tgpd_glass_disclamers').removeAttr('disabled');
	//jQuery('#text_tgpd_glass_disclamers_reminder_section').removeAttr('disabled');


	}
	
	
	
	
			
	
	 
	 
	 
	 function addAndUpdateCulk( caulk_amount='0',  caulk_type='', scaffolding_type='',curretPosition='',old=false ){
       
       var tgpcf= tgpd_current_item; //+tgpcf+'_'
       console.log(maxChaulk +" >> tgpcf");

       console.log(tgpd_current_item +" >> tgpd_current_item");
       //alert( 'maxChaulks array ' + maxChaulks[tgpcf] +' >> >> >> tgpcf >> ' + tgpcf);
      
      var addOrMinusBtn;      var lbl='';
      if(curretPosition==0 && old==true){
        // lbl ='<label class="leftalign add">Caulk</label>';
         addOrMinusBtn =    ' class="btn_pluse" id="btn12">+';
      }else{
         addOrMinusBtn =    ' class="removeItem btn_pluse amtremove  margintop">-';
      }
      
      //<option value="999 Clear Silicone (TUBE)">999 Clear Silicone (TUBE)</option> <option value="795 White (TUBE)">795 White (TUBE)</option> <option value="795 Black (TUBE)">795 Black (TUBE)</option> <option value="795 Dark Bronze(TUBE)">795 Dark Bronze(TUBE)</option> <option value="795 Anodized Aluminum (TUBE)">795 Anodized Aluminum (TUBE)</option> <option value="Dymonic White (TUBE)">Dymonic White (TUBE)</option> <option value="Dymonic Black (TUBE)">Dymonic Black (TUBE)</option> <option value="Dymonic Dark Bronze (TUBE)">Dymonic Dark Bronze (TUBE)</option> <option value="Dymonic Anodized Aluminum (TUBE)">Dymonic Anodized Aluminum (TUBE)</option> <option value="999 Clear Silicone (SAUSAGE)">999 Clear Silicone (SAUSAGE)</option> <option value="795 White (SAUSAGE)">795 White (SAUSAGE)</option> <option value="795 Black (SAUSAGE)">795 Black (SAUSAGE)</option> <option value="795 Dark Bronze(SAUSAGE)">795 Dark Bronze(SAUSAGE)</option> <option value="795 Anodized Aluminum (SAUSAGE)">795 Anodized Aluminum (SAUSAGE)</option> <option value="Dymonic White (SAUSAGE)">Dymonic White (SAUSAGE)</option> <option value="Dymonic Black (SAUSAGE)">Dymonic Black (SAUSAGE)</option> <option value="Dymonic Dark Bronze (SAUSAGE)">Dymonic Dark Bronze (SAUSAGE)</option> <option value="Dymonic Anodized Aluminum (SAUSAGE)">Dymonic Anodized Aluminum (SAUSAGE)</option>

   
      // text_tgpd_glass_materials_caulk_amount++;
        jQuery(".add_quntity").append('<div class="myCustom" id="add_id_'+tgpcf+'_'+maxChaulk+'"><div class="col-xs-12 add"><div class="row add plus-minus-btn-row"><div class="col-xs-12 add"><fieldset id="fieldset_tgpd_glass_materials_caulk"><div class="row add"><div class="col-xs-3 col-md-4 col-lg-4 gls_caulk"><label class="leftalign add" for="text_tgpd_glass_materials_caulk_amount">Amount</label><div class="input-group"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+maxChaulk+'"><span class="glyphicon glyphicon-minus"></span></button></span><input type="text" id="text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+maxChaulk+'"       value="'+caulk_amount+'" name="text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+maxChaulk+'"' + 'class="form-control input-number-plus-minus qty-align-new" min="0" max="100"><span class="input-group-btn"><button data-field="text_tgpd_glass_materials_caulk_amount_'+tgpcf+'_'+maxChaulk+'" type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"><span class="glyphicon glyphicon-plus"></span></button></span></div></div><div class="col-xs-8 col-md-6 col-lg-6 add"><label class="leftalign add" for="select_tgpd_glass_materials_caulk_type">CAULK</label><select id="select_tgpd_glass_materials_caulk_type_'+tgpcf+'_'+maxChaulk+'" class="form-control add"><option value="not_applicable">Caulk type</option><option value="799-Clear (T)">799-Clear (T)</option><option value="999-A-Clear (T)">999-A-Clear (T)</option><option value="790-Black (S)">790-Black (S)</option><option value="790-Black (T)">790-Black (T)</option><option value="795-Black (S)">795-Black (S)</option><option value="795-Black (T)">795-Black (T)</option><option value="995-Black (S)">995-Black (S)</option><option value="999-A-Black (T)">999-A-Black (T)</option><option value="Dymonic-Black (S)">Dymonic-Black (S)</option><option value="Dymonic-Black (T)">Dymonic-Black (T)</option><option value="790-Bronze (S)">790-Bronze (S)</option><option value="790-Bronze (T)">790-Bronze (T)</option><option value="795-Bronze (S)">795-Bronze (S)</option><option value="795-Bronze (T)">795-Bronze (T)</option><option value="999-A-Bronze (T)">999-A-Bronze (T)</option><option value="Dymonic-Bronze (S)">Dymonic-Bronze (S)</option><option value="Dymonic-Bronze (T)">Dymonic-Bronze (T)</option><option value="795-Anodized Aluminum (S)">795-Anodized Aluminum (S)</option><option value="795-Anodized Aluminum (T)">795-Anodized Aluminum (T)</option><option value="999-A-Aluminum (T)">999-A-Aluminum (T)</option><option value="Dymonic-Anodized Aluminum (S)">Dymonic-Anodized Aluminum (S)</option><option value="Dymonic-Anodized Aluminum (T)">Dymonic-Anodized Aluminum (T)</option><option value="790-White (S)">790-White (S)</option><option value="790-White (T)">790-White (T)</option><option value="795-White (S)">795-White (S)</option><option value="795-White (T)">795-White (T)</option><option value="995-White (S)">995-White (S)</option><option value="999-A-White (T)">999-A-White (T)</option><option value="Dymonic-White (S)">Dymonic-White (S)</option><option value="Dymonic-White (T)">Dymonic-White (T)</option><option value="Rockite (Bucket)">Rockite (Bucket)</option><option value="Rockite (Gallon)">Rockite (Gallon)</option><option value="Epoxy (Gallon)">Epoxy (Gallon)</option><option value="Putty White (Pint) - Wood">Putty White (Pint) - Wood</option><option value="Putty White (Gallon) - Wood">Putty White (Gallon) - Wood</option><option value="Putty Grey (Pint) - Metal">Putty Grey (Pint) - Metal</option><option value="Putty Grey (Gallon) - Metal">Putty Grey (Gallon) - Metal</option><option value="790-Adobe Tan (S)">790-Adobe Tan (S)</option><option value="790-Adobe Tan (T)">790-Adobe Tan (T)</option><option value="790-Blue Spruce (S)">790-Blue Spruce (S)</option><option value="790-Blue Spruce (T)">790-Blue Spruce (T)</option><option value="790-Charcoal (S)">790-Charcoal (S)</option><option value="790-Charcoal (T)">790-Charcoal (T)</option><option value="790-Dusty Rose (S)">790-Dusty Rose (S)</option><option value="790-Dusty Rose (T)">790-Dusty Rose (T)</option><option value="790-Gray (S)">790-Gray (S)</option><option value="790-Gray (T)">790-Gray (T)</option><option value="790-Limestone (S)">790-Limestone (S)</option><option value="790-Limestone (T)">790-Limestone (T)</option><option value="790-Natural Stone (S)">790-Natural Stone (S)</option><option value="790-Natural Stone (T)">790-Natural Stone (T)</option><option value="790-Precast White (S)">790-Precast White (S)</option><option value="790-Precast White (T)">790-Precast White (T)</option><option value="790-Rustic Brick (S)">790-Rustic Brick (S)</option><option value="790-Rustic Brick (T)">790-Rustic Brick (T)</option><option value="790-Sandstone (S)">790-Sandstone (S)</option><option value="790-Sandstone (T)">790-Sandstone (T)</option><option value="795-Adobe Tan (S)">795-Adobe Tan (S)</option><option value="795-Adobe Tan (T)">795-Adobe Tan (T)</option><option value="795-Blue Spruce (S)">795-Blue Spruce (S)</option><option value="795-Blue Spruce (T)">795-Blue Spruce (T)</option><option value="795-Champagne (S)">795-Champagne (S)</option><option value="795-Champagne (T)">795-Champagne (T)</option><option value="795-Charcoal (S)">795-Charcoal (S)</option><option value="795-Charcoal (T)">795-Charcoal (T)</option><option value="795-Dusty Rose (S)">795-Dusty Rose (S)</option><option value="795-Dusty Rose (T)">795-Dusty Rose (T)</option><option value="795-Gray (S)">795-Gray (S)</option><option value="795-Gray (T)">795-Gray (T)</option><option value="795-Limestone (S)">795-Limestone (S)</option><option value="795-Limestone (T)">795-Limestone (T)</option><option value="795-Natural Stone (S)">795-Natural Stone (S)</option><option value="795-Natural Stone (T)">795-Natural Stone (T)</option><option value="795-Rustic Brick (S)">795-Rustic Brick (S)</option><option value="795-Rustic Brick (T)">795-Rustic Brick (T)</option><option value="795-Sandstone (S)">795-Sandstone (S)</option><option value="795-Sandstone (T)">795-Sandstone (T)</option><option value="995-Gray (S)">995-Gray (S)</option><option value="999-A-Light Bronze (T)">999-A-Light Bronze (T)</option><option value="Dymonic-Almond (S)">Dymonic-Almond (S)</option><option value="Dymonic-Almond (T)">Dymonic-Almond (T)</option><option value="Dymonic-Aluminum Stone (S)">Dymonic-Aluminum Stone (S)</option><option value="Dymonic-Aluminum Stone (T)">Dymonic-Aluminum Stone (T)</option><option value="Dymonic-Beige (S)">Dymonic-Beige (S)</option><option value="Dymonic-Beige (T)">Dymonic-Beige (T)</option><option value="Dymonic-Buff (S)">Dymonic-Buff (S)</option><option value="Dymonic-Buff (T)">Dymonic-Buff (T)</option><option value="Dymonic-Dark Bronze (S)">Dymonic-Dark Bronze (S)</option><option value="Dymonic-Dark Bronze (T)">Dymonic-Dark Bronze (T)</option><option value="Dymonic-Gray (S)">Dymonic-Gray (S)</option><option value="Dymonic-Gray (T)">Dymonic-Gray (T)</option><option value="Dymonic-Gray Stone (S)">Dymonic-Gray Stone (S)</option><option value="Dymonic-Gray Stone (T)">Dymonic-Gray Stone (T)</option><option value="Dymonic-Hartford Green (S)">Dymonic-Hartford Green (S)</option><option value="Dymonic-Hartford Green (T)">Dymonic-Hartford Green (T)</option><option value="Dymonic-Ivory (S)">Dymonic-Ivory (S)</option><option value="Dymonic-Ivory (T)">Dymonic-Ivory (T)</option><option value="Dymonic-Light Bronze (S)">Dymonic-Light Bronze (S)</option><option value="Dymonic-Light Bronze (T)">Dymonic-Light Bronze (T)</option><option value="Dymonic-Limestone (S)">Dymonic-Limestone (S)</option><option value="Dymonic-Limestone (T)">Dymonic-Limestone (T)</option><option value="Dymonic-Natural Clay (S)">Dymonic-Natural Clay (S)</option><option value="Dymonic-Natural Clay (T)">Dymonic-Natural Clay (T)</option><option value="Dymonic-Off White (S)">Dymonic-Off White (S)</option><option value="Dymonic-Off White (T)">Dymonic-Off White (T)</option><option value="Dymonic-Precast White (S)">Dymonic-Precast White (S)</option><option value="Dymonic-Precast White (T)">Dymonic-Precast White (T)</option><option value="Dymonic-Redwood Tan (S)">Dymonic-Redwood Tan (S)</option><option value="Dymonic-Redwood Tan (T)">Dymonic-Redwood Tan (T)</option><option value="Dymonic-Sandalwood (S)">Dymonic-Sandalwood (S)</option><option value="Dymonic-Sandalwood (T)">Dymonic-Sandalwood (T)</option><option value="Dymonic-Stone (S)">Dymonic-Stone (S)</option><option value="Dymonic-Stone (T)">Dymonic-Stone (T)</option></select></div><div class="col-xs-1"><a href="javascript:void(0);" '+addOrMinusBtn+'</a></div></div></fieldset></div></div></div></div>');
      
      
      //<select id="select_tgpd_glass_materials_caulk_type_'+tgpcf+'_'+maxChaulk+'" class="form-control add"><option value="not_applicable">Caulk type</option> <option value="799-Clear (T)">799-Clear (T)</option>   <option value="999-A-Clear (T)">999-A-Clear (T)</option>   <option value="790-Black (S)">790-Black (S)</option>   <option value="790-Black (T)">790-Black (T)</option>   <option value="795-Black (S)">795-Black (S)</option>   <option value="795-Black (T)">795-Black (T)</option>   <option value="995-Black (S)">995-Black (S)</option>   <option value="999-A-Black (T)">999-A-Black (T)</option>   <option value="Dymonic-Black (S)">Dymonic-Black (S)</option>   <option value="Dymonic-Black (T)">Dymonic-Black (T)</option>   <option value="790-Bronze (S)">790-Bronze (S)</option>   <option value="790-Bronze (T)">790-Bronze (T)</option>   <option value="795-Bronze (S)">795-Bronze (S)</option>   <option value="795-Bronze (T)">795-Bronze (T)</option>   <option value="999-A-Bronze (T)">999-A-Bronze (T)</option>   <option value="Dymonic-Bronze (S)">Dymonic-Bronze (S)</option>   <option value="Dymonic-Bronze (T)">Dymonic-Bronze (T)</option>   <option value="795-Anodized Aluminum (S)">795-Anodized Aluminum (S)</option>   <option value="795-Anodized Aluminum (T)">795-Anodized Aluminum (T)</option>   <option value="999-A-Aluminum (T)">999-A-Aluminum (T)</option>   <option value="Dymonic-Anodized Aluminum (S)">Dymonic-Anodized Aluminum (S)</option>   <option value="Dymonic-Anodized Aluminum (T)">Dymonic-Anodized Aluminum (T)</option>   <option value="790-White (S)">790-White (S)</option>   <option value="790-White (T)">790-White (T)</option>   <option value="795-White (S)">795-White (S)</option>   <option value="795-White (T)">795-White (T)</option>   <option value="995-White (S)">995-White (S)</option>   <option value="999-A-White (T)">999-A-White (T)</option>   <option value="Dymonic-White (S)">Dymonic-White (S)</option>   <option value="Dymonic-White (T)">Dymonic-White (T)</option>  <option value="White Latex (T)">White Latex (T)</option> <option value="Beige Latex (T)">Beige Latex (T)</option>  <option value="Rockite (Bucket)">Rockite (Bucket)</option>   <option value="Rockite (Gallon)">Rockite (Gallon)</option>   <option value="Epoxy (Gallon)">Epoxy (Gallon)</option>   <option value="Putty White (Pint) - Wood">Putty White (Pint) - Wood</option>   <option value="Putty White (Gallon) - Wood">Putty White (Gallon) - Wood</option>   <option value="Putty Grey (Pint) - Metal">Putty Grey (Pint) - Metal</option>   <option value="Putty Grey (Gallon) - Metal">Putty Grey (Gallon) - Metal</option>   <option value="790-Adobe Tan (S)">790-Adobe Tan (S)</option>   <option value="790-Adobe Tan (T)">790-Adobe Tan (T)</option>   <option value="790-Blue Spruce (S)">790-Blue Spruce (S)</option>   <option value="790-Blue Spruce (T)">790-Blue Spruce (T)</option>   <option value="790-Charcoal (S)">790-Charcoal (S)</option>  <option value="790-Charcoal (T)">790-Charcoal (T)</option>   <option value="790-Dusty Rose (S)">790-Dusty Rose (S)</option>   <option value="790-Dusty Rose (T)">790-Dusty Rose (T)</option>   <option value="790-Gray (S)">790-Gray (S)</option>   <option value="790-Gray (T)">790-Gray (T)</option>   <option value="790-Limestone (S)">790-Limestone (S)</option>   <option value="790-Limestone (T)">790-Limestone (T)</option>   <option value="790-Natural Stone (S)">790-Natural Stone (S)</option>   <option value="790-Natural Stone (T)">790-Natural Stone (T)</option>   <option value="790-Precast White (S)">790-Precast White (S)</option>   <option value="790-Precast White (T)">790-Precast White (T)</option>   <option value="790-Rustic Brick (S)">790-Rustic Brick (S)</option>   <option value="790-Rustic Brick (T)">790-Rustic Brick (T)</option>   <option value="790-Sandstone (S)">790-Sandstone (S)</option>   <option value="790-Sandstone (T)">790-Sandstone (T)</option>   <option value="795-Adobe Tan (S)">795-Adobe Tan (S)</option>   <option value="795-Adobe Tan (T)">795-Adobe Tan (T)</option>   <option value="795-Blue Spruce (S)">795-Blue Spruce (S)</option>   <option value="795-Blue Spruce (T)">795-Blue Spruce (T)</option>   <option value="795-Champagne (S)">795-Champagne (S)</option>   <option value="795-Champagne (T)">795-Champagne (T)</option>   <option value="795-Charcoal (S)">795-Charcoal (S)</option>   <option value="795-Charcoal (T)">795-Charcoal (T)</option>   <option value="795-Dusty Rose (S)">795-Dusty Rose (S)</option>   <option value="795-Dusty Rose (T)">795-Dusty Rose (T)</option>   <option value="795-Gray (S)">795-Gray (S)</option>   <option value="795-Gray (T)">795-Gray (T)</option>   <option value="795-Limestone (S)">795-Limestone (S)</option>   <option value="795-Limestone (T)">795-Limestone (T)</option>   <option value="795-Natural Stone (S)">795-Natural Stone (S)</option>   <option value="795-Natural Stone (T)">795-Natural Stone (T)</option>   <option value="795-Rustic Brick (S)">795-Rustic Brick (S)</option>   <option value="795-Rustic Brick (T)">795-Rustic Brick (T)</option>   <option value="795-Sandstone (S)">795-Sandstone (S)</option>   <option value="795-Sandstone (T)">795-Sandstone (T)</option>   <option value="995-Gray (S)">995-Gray (S)</option>   <option value="999-A-Light Bronze (T)">999-A-Light Bronze (T)</option>   <option value="Dymonic-Almond (S)">Dymonic-Almond (S)</option>   <option value="Dymonic-Almond (T)">Dymonic-Almond (T)</option>   <option value="Dymonic-Aluminum Stone (S)">Dymonic-Aluminum Stone (S)</option>   <option value="Dymonic-Aluminum Stone (T)">Dymonic-Aluminum Stone (T)</option>   <option value="Dymonic-Beige (S)">Dymonic-Beige (S)</option>   <option value="Dymonic-Beige (T)">Dymonic-Beige (T)</option>   <option value="Dymonic-Buff (S)">Dymonic-Buff (S)</option>   <option value="Dymonic-Buff (T)">Dymonic-Buff (T)</option>   <option value="Dymonic-Dark Bronze (S)">Dymonic-Dark Bronze (S)</option>   <option value="Dymonic-Dark Bronze (T)">Dymonic-Dark Bronze (T)</option>   <option value="Dymonic-Gray (S)">Dymonic-Gray (S)</option>   <option value="Dymonic-Gray (T)">Dymonic-Gray (T)</option>   <option value="Dymonic-Gray Stone (S)">Dymonic-Gray Stone (S)</option>   <option value="Dymonic-Gray Stone (T)">Dymonic-Gray Stone (T)</option>   <option value="Dymonic-Hartford Green (S)">Dymonic-Hartford Green (S)</option>   <option value="Dymonic-Hartford Green (T)">Dymonic-Hartford Green (T)</option>   <option value="Dymonic-Ivory (S)">Dymonic-Ivory (S)</option>   <option value="Dymonic-Ivory (T)">Dymonic-Ivory (T)</option>   <option value="Dymonic-Light Bronze (S)">Dymonic-Light Bronze (S)</option>   <option value="Dymonic-Light Bronze (T)">Dymonic-Light Bronze (T)</option>   <option value="Dymonic-Limestone (S)">Dymonic-Limestone (S)</option>   <option value="Dymonic-Limestone (T)">Dymonic-Limestone (T)</option>   <option value="Dymonic-Natural Clay (S)">Dymonic-Natural Clay (S)</option>   <option value="Dymonic-Natural Clay (T)">Dymonic-Natural Clay (T)</option>   <option value="Dymonic-Off White (S)">Dymonic-Off White (S)</option>   <option value="Dymonic-Off White (T)">Dymonic-Off White (T)</option>   <option value="Dymonic-Precast White (S)">Dymonic-Precast White (S)</option>   <option value="Dymonic-Precast White (T)">Dymonic-Precast White (T)</option>   <option value="Dymonic-Redwood Tan (S)">Dymonic-Redwood Tan (S)</option>   <option value="Dymonic-Redwood Tan (T)">Dymonic-Redwood Tan (T)</option>   <option value="Dymonic-Sandalwood (S)">Dymonic-Sandalwood (S)</option>   <option value="Dymonic-Sandalwood (T)">Dymonic-Sandalwood (T)</option>   <option value="Dymonic-Stone (S)">Dymonic-Stone (S)</option>   <option value="Dymonic-Stone (T)">Dymonic-Stone (T)</option></select>
          
       if(caulk_type!=''){
            jQuery('#select_tgpd_glass_materials_caulk_type_'+tgpcf+'_'+maxChaulk).val(caulk_type);
         }
         
   // if(scaffolding_type!=''){
   //    jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+maxChaulk).val(scaffolding_type);
   // }
         
//       maxChaulk[tgpcf]=maxChaulks[tgpcf]++;
      if(curretPosition==0){
         jQuery("#btn12").click(function(){
            // text_tgpd_glass_materials_caulk_amount++;
            addAndUpdateCulk('0','','',1,true);
         }); 
      }
         maxChaulk++;
         maxChaulks[tgpcf]=maxChaulk;
    }
	 
	/***********************************************************************************************/

		//add_tap_channel
		//add_tap_scaff
	function addAndUpdateScaffolding(scaffolding='',qtyType=0,curretPosition,old=false){
      
      var tgpcf=  tgpd_current_item; //+tgpcf+'_'
      
      var lbl='';
      var addOrMinusBtn;      
      if(curretPosition==0 && old==true){
      addOrMinusBtn =    ' class="btn_pluse" id="btn14">+';
      //lbl = '<label class="leftalign add ">Equipment </label>';
      }else{
      addOrMinusBtn =    ' class="removeItem1 btn_pluse margintop">-';
      lbl = '';
      }//number_tgpd_pattern_horizontal_grids
      jQuery(".add_tap_scaff").append('<div class="myCustom1" id="add_id_scaff_'+maxScaffolding+'"><div class="col-xs-12 add">'+lbl+'<div class="row add plus-minus-btn-row"><div class="col-xs-12 add"><fieldset id="fieldset_tgpd_glass_materials_scaffolding"><div class="row add"><div class="col-xs-3 col-md-4  gls_caulk"> <label class="leftalign smaller" for="select_tgpd_glass_materials_quantity">Quantity</label>  <div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="select_tgpd_glass_materials_quantity_'+tgpcf+'_'+maxScaffolding+'"><span class="glyphicon glyphicon-minus"></span></button></span> <input id="select_tgpd_glass_materials_quantity_'+tgpcf+'_'+maxScaffolding+'" name="select_tgpd_glass_materials_quantity_'+tgpcf+'_'+maxScaffolding+'" type="text" class="form-control text-num input-number-plus-minus qty-align-new" autocomplete="on"  min="0" max="100" value="'+qtyType+'" ><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="select_tgpd_glass_materials_quantity_'+tgpcf+'_'+maxScaffolding+'"><span class="glyphicon glyphicon-plus"></span></button></span></div> </div><div class="col-xs-8 col-md-6 add"><label class="leftalign add"for="select_tgpd_glass_materials_scaffolding_type">EQUIPMENT</label><select id="select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+maxScaffolding+'" class="form-control add"><option value="not_applicable">Equipment type</option><option value="Swing Stage (SAG)">Swing Stage (SAG)</option><option value="Swing Stage (SUB)">Swing Stage (SUB)</option><option value="Scaffolding (SAG)">Scaffolding (SAG)</option><option value="Scaffolding-HALF (SAG)">Scaffolding-HALF (SAG)</option><option value="Scaffolding (SUB, RENT)">Scaffolding (SUB, RENT)</option><option value="Scaffolding (SUB, SETUP)">Scaffolding (SUB, SETUP)</option><option value="Scaffolding-BAKER (SUB.RENT)">Scaffolding-BAKER (SUB.RENT)</option><option value="Scaffolding-BAKER (SUB.SETUP)">Scaffolding-BAKER (SUB.SETUP)</option><option value="40FT Articulating Lift">40FT Articulating Lift</option><option value="60FT Articulating Lift">60FT Articulating Lift</option><option value="80FT Articulating Lift">80FT Articulating Lift</option><option value="120FT Articulating Lift">120FT Articulating Lift</option><option value="135FT Articulating Lift">135FT Articulating Lift</option><option value="40FT STICK BOOM">40FT STICK BOOM</option><option value="60FT STICK BOOM">60FT STICK BOOM</option><option value="80FT STICK BOOM">80FT STICK BOOM</option><option value="120FT STICK BOOM">120FT STICK BOOM</option><option value="135FT STICK BOOM">135FT STICK BOOM</option><option value="19FT Scissor Lift">19FT Scissor Lift</option><option value="24-26FT Scissor Lift">24-26FT Scissor Lift</option><option value="30-35FT Scissor Lift">30-35FT Scissor Lift</option><option value="39-40FT Scissor Lift">39-40FT Scissor Lift</option><option value="25-27T 4WD Scissor Lift">25-27T 4WD Scissor Lift</option><option value="36-49FT 4WD Scissor Lift">36-49FT 4WD Scissor Lift</option><option value="Towable Boom">Towable Boom</option><option value="Generator">Generator</option><option value="Power Cup">Power Cup</option><option value="Crane">Crane</option><option value="Lift My Glass">Lift My Glass</option><option value="LULL LIFT">LULL LIFT</option><option value="Fork Boom">Fork Boom</option><option value="Chain Fall">Chain Fall</option><option value="Traffic Control">Traffic Control</option><option value="Street Closure Permit">Street Closure Permit</option><option value="New Part- TEXT BOX">New Part- TEXT BOX</option></select></div><div class="col-xs-1"><a href="javascript:void(0);" '+addOrMinusBtn+'</a></div>          </div></fieldset></div></div></div></div></div>');
      
      //<select id="select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+maxScaffolding+'" class="form-control add"><option value="not_applicable">Equipment type</option><option value="40ft_articulating_lift">40FT Articulating Lift</option><option value="60ft_articulating_lift">60FT Articulating Lift</option><option value="80ft_articulating_lift">80FT Articulating Lift</option><option value="120ft_articulating_lift">120FT Articulating Lift</option><option value="135ft_articulating_lift">135FT Articulating Lift</option><option value="40ft_stick_boom">40FT Stick Boom</option><option value="60ft_stick_boom">60FT Stick Boom</option><option value="80ft_stick_boom">80FT Stick Boom</option><option value="120ft_stick_boom">120FT Stick Boom</option><option value="135ft_stick_boom">135FT Stick Boom</option><option value="19ft_scissor_lift">19FT Scissor Lift</option><option value="24_26ft_scissor_lift">24-26FT Scissor Lift</option><option value="30_35ft_scissor_lift">30-35FT Scissor Lift</option><option value="39_40ft_scissor_lift">39-40FT Scissor Lift</option><option value="25_27ft_4wd_scissor_lift">25-27FT 4WD Scissor Lift</option><option value="36_49ft_4wd_scissor_lift">36-49FT 4WD Scissor Lift</option><option value="swing_stage_sag">Swing Stage (SAG)</option><option value="swing_stage_sub">Swing Stage (SUB)</option><option value="1_scaffolding">1 - Scaffolding</option><option value="1_1/2_scaffolding">1 1/2 -Scaffolding</option><option value="2_scaffolding">2 - Scaffolding</option><option value="2_1/2_scaffolding">2 1/2 - Scaffolding</option><option value="3_scaffolding">3 - Scaffolding</option><option value="3_1/2_scaffolding">3 1/2 - Scaffolding</option><option value="4_scaffolding">4 - Scaffolding</option><option value="4_1/2_scaffolding">4 1/2 - Scaffolding</option><option value="5_scaffolding">5 - Scaffolding</option><option value="5_1/2_scaffolding">5 1/2 - Scaffolding</option><option value="6_scaffolding">6 - Scaffolding</option><option value="6_1/2_scaffolding">6 1/2 - Scaffolding</option><option value="7_scaffolding">7 - Scaffolding</option><option value="7_1/2_scaffolding">7 1/2 - Scaffolding</option><option value="8_scaffolding">8 - Scaffolding</option><option value="Swing Stage(SAG)">Swing Stage (SAG)</option><option value="Swing Stage (SUB)">Swing Stage (SUB)</option><option value="Scaffolding (SAG)">Scaffolding (SAG)</option><option value="Scaffolding-HALF (SAG)">Scaffolding-HALF (SAG)</option><option value="Scaffolding (SUB, RENT)">Scaffolding (SUB, RENT)</option><option value="Scaffolding (SUB, SETUP)">Scaffolding (SUB, SETUP)</option><option value="Scaffolding-BAKER (SUB.RENT)">Scaffolding-BAKER (SUB.RENT)</option><option value="Scaffolding-BAKER (SUB.SETUP)">Scaffolding-BAKER (SUB.SETUP)</option><option value="40FT Articulating Lift">40FT Articulating Lift</option><option value="60FT Articulating Lift">60FT Articulating Lift</option><option value="80FT Articulating Lift">80FT Articulating Lift</option><option value="120FT Articulating Lift">120FT Articulating Lift</option><option value="135FT Articulating Lift">135FT Articulating Lift</option><option value="40FT STICK BOOM">40FT STICK BOOM</option><option value="60FT STICK BOOM">60FT STICK BOOM</option><option value="80FT STICK BOOM">80FT STICK BOOM</option><option value="120FT STICK BOOM">120FT STICK BOOM</option><option value="135FT STICK BOOM">135FT STICK BOOM</option><option value="19FT Scissor Lift">19FT Scissor Lift</option><option value="24-26FT Scissor Lift">24-26FT Scissor Lift</option><option value="30-35FT Scissor Lift">30-35FT Scissor Lift</option><option value="39-40FT Scissor Lift">39-40FT Scissor Lift</option><option value="25-27T 4WD Scissor Lift">25-27T 4WD Scissor Lift</option><option value="36-49FT 4WD Scissor Lift">36-49FT 4WD Scissor Lift</option><option value="Towable Boom">Towable Boom</option><option value="Generator">Generator</option><option value="Power Cup">Power Cup</option><option value="Crane">Crane</option><option value="Lift My Glass ">Lift My Glass</option><option value="LULL LIFT">LULL LIFT</option><option value="Fork Boom">Fork Boom</option><option value="Chain Fall">Chain Fall</option><option value="Traffic Control">Traffic Control</option><option value="Street Closure Permit">Street Closure Permit</option><option value="New Part- TEXT BOXLift">New Part- TEXT BOX</option></select>
      
      //maxTapes[tgpcf]=maxTape[tgpcf]++;
      
      if(!!scaffolding){
            
            jQuery('#select_tgpd_glass_materials_scaffolding_type_'+tgpcf+'_'+maxScaffolding).val(scaffolding);
         }
      if(qtyType>0){
            
            jQuery('#select_tgpd_glass_materials_quantity_'+tgpcf+'_'+maxScaffolding).val(qtyType);
         }
      if(curretPosition==0){
            jQuery("#btn14").click(function(){
               addAndUpdateScaffolding('',0,1,false);
            }); 
         }
         
      maxScaffolding++;
      maxScaffoldings[tgpcf]=maxScaffolding;
      
   }
	
	
     /******sag 129*****/
   function addAndUpdateChannel(channel='',qty_channel=0,curretPosition,old=false){

      
      
      var tgpcf=  tgpd_current_item; //+tgpcf+'_'
      
      //alert('curretPosition addAndUpdateChannel >> '+curretPosition);
      var addOrMinusBtn;      
      var lbl='' ;
      if(curretPosition==0 && old==true){
      addOrMinusBtn =    ' class="btn_pluse" id="btn13">+';
      
      }else{
      addOrMinusBtn =    ' class="removeItem1 btn_pluse margintop">-';
      
      }
      if(curretPosition==0){
         lbl = '<label class="leftalign" style="">CHANNEL</label>';
         
      }else{
         lbl = '<label class="leftalign" style="">CHANNEL</label>';
         
      }

    
      
      jQuery(".add_tap_channel").append('<div class="myCustom1"><div class="col-xs-12"><div class="row add plus-minus-btn-row"><div class="col-xs-12 add"><fieldset id="fieldset_tgpd_glass_materials_channel"><div class="row add"><div class="col-xs-3 col-md-4  gls_caulk" style="margin-top:9px;"><label class="leftalign smaller" for="select_tgpd_glass_materials_channel_quantity">Quantity</label><div class="input-group col-xs-12"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus bt_for_minus"  data-type="minus" data-field="select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+maxChannel+'"><span class="glyphicon glyphicon-minus"></span></button></span><input id="select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+maxChannel+'" name="select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+maxChannel+'" type="text" class="form-control text-num input-number-plus-minusqty-align-new text-center user-success" autocomplete="on" min="0" max="100" value="0"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus" data-field="select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+maxChannel+'"><span class="glyphicon glyphicon-plus"></span></button></span></div> </div><div class=" col-xs-8 col-md-6 add" id="add_id_channel_'+maxChannel+'">'+lbl+'<select id="text_tgpd_glass_materials_channel_'+tgpcf+'_'+maxChannel+'" class="form-control add" ><option value="not_applicable">Channel Type</option>   <option value="J-Channel 5/8 Chrome">J-Channel 5/8 Chrome</option><option value="J-Channel 5/8 Gold">J-Channel 5/8 Gold</option><option value="J-Channel 5/8 Brushed Nickel">J-Channel 5/8 Brushed Nickel</option><option value="J-Channel 5/8 Oil Rubbed Bronze">J-Channel 5/8 Oil Rubbed Bronze</option><option value="J-Channel 3/8 Chrome">J-Channel 3/8 Chrome</option><option value="J-Channel 3/8 Gold">J-Channel 3/8 Gold</option><option value="J-Channel 3/8 Brushed Nickel">J-Channel 3/8 Brushed Nickel</option><option value="J-Channel 3/8 Oil Rubbed Bronze">J-Channel 3/8 Oil Rubbed Bronze</option><option value="L-Channel Chrome">L-Channel Chrome</option><option value="L-Channel Gold">L-Channel Gold</option><option value="L-Channel Brushed Nickel">L-Channel Brushed Nickel</option><option value="L-Channel Oil Rubbed Bronze">L-Channel Oil Rubbed Bronze</option><option value="U-Channel 3/4 x 3/4 Clear Anodized/ Satin">U-Channel 3/4 x 3/4 Clear Anodized/ Satin</option><option value="U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin">U-Channel 3/4 x 1-1/2 Clear Anodized/ Satin</option><option value="U-Channel 1 x 1 Clear Anodized/ Satin">U-Channel 1 x 1 Clear Anodized/ Satin</option><option value="U-Channel 1 x 1 Polished Brite Silver">U-Channel 1 x 1 Polished Brite Silver</option><option value="U-Channel 1 x 1 Brushed Stainless">U-Channel 1 x 1 Brushed Stainless</option><option value="U-Channel 1 x 2 Clear Anodized/ Satin">U-Channel 1 x 2 Clear Anodized/ Satin</option><option value="U-Channel 1 x 2 Polished Brite Silver">U-Channel 1 x 2 Polished Brite Silver</option><option value="U-Channel 1 x 2 Brushed Stainless">U-Channel 1 x 2 Brushed Stainless</option><option value="Tube - Chrome">Tube - Chrome</option><option value="Tube - Brushed Nickel">Tube - Brushed Nickel</option><option value="Tube - Oil Rubbed Bronze">Tube - Oil Rubbed Bronze</option><option value="Glazing Rubber for U Channel, 1/4 Glass - Black">Glazing Rubber for U Channel, 1/4 Glass - Black</option><option value="Glazing Rubber for U Channel, 3/8 Glass - Black">Glazing Rubber for U Channel, 3/8 Glass - Black</option><option value="Glazing Rubber for U Channel, 1/2 Glass - Black">Glazing Rubber for U Channel, 1/2 Glass - Black</option></select></div><div class=" col-xs-1"><a href="javascript:void(0);" '+addOrMinusBtn+'</a></div></div></fieldset></div></div></div>');
   
      if(channel!=''){
            
            jQuery('#text_tgpd_glass_materials_channel_'+tgpcf+'_'+maxChannel).val(channel);
         }

         if(qty_channel!=''){
            
            jQuery('#select_tgpd_glass_materials_channel_quantity_'+tgpcf+'_'+maxChannel).val(qty_channel);
         }




      
      
      //maxTapes[tgpcf]=maxTape[tgpcf]++;
      
      if(curretPosition==0){
            jQuery("#btn13").click(function(){
               addAndUpdateChannel('',0,1,false,0);
            }); 
         }
         
      maxChannel++;
      maxChannels[tgpcf]=maxChannel;
      
   }
    /******sag 129*****/
	
	function addAndUpdateTape( tape_amount='0', tape_type='', channel='',curretPosition,old=false){
      console.log("---inside addAndUpdateTape----")
      var tgpcf=  tgpd_current_item; //+tgpcf+'_'
      
      
      var addOrMinusBtn;   var lbl ='';   
      if(curretPosition==0 && old==true){
      addOrMinusBtn =    ' class="btn_pluse" id="btn11">+';
      
      }else{
      addOrMinusBtn =    ' class="removeItem1 btn_pluse margintop">-';
      }
   // if(old==false && curretPosition!=0){   }
      
      if(curretPosition==0){
         
        // lbl ='<label class="leftalign ">Tape</label> ';
      }
      
       jQuery(".add_tap_amount").append('<div class="myCustom1" id="add_id_tap_'+maxTape+'"><div class="col-xs-12">'+lbl+'<div class="row plus-minus-btn-row"><div class="col-xs-12"><fieldset id="fieldset_tgpd_glass_materials_tape"><div class="row"><div class="col-xs-3 col-md-4 gls_caulk"><label class="leftalign">Amount</label><div class="input-group"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" disabled="disabled" data-type="minus" data-field="text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+maxTape+'"><span class="glyphicon glyphicon-minus"></span></button></span><input type="text" id="text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+maxTape+'" value="'+tape_amount+'" name="text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+maxTape+'" class="form-control input-number-plus-minus qty-align-new" min="0" max="100"><span class="input-group-btn"><button type="button" class="btn btn-default btn-number-plus-minus" data-type="plus"  data-field="text_tgpd_glass_materials_tape_amount_'+tgpcf+'_'+maxTape+'"><span class="glyphicon glyphicon-plus"></span></button></span></div></div><div class="col-xs-8 col-md-6 add"><label class="leftalign">TAPE</label><select id="select_tgpd_glass_materials_tape_type_'+tgpcf+'_'+maxTape+'" class="form-control">  <option value="not_applicable">Tape type</option><option value="1_over_8_440">1/8 440 tape</option><option value="1_over_4_440">1/4 440 tape</option><option value="1_over_2_440">1/2 440 tape</option><option value="3_over_4_440">3/4 440 tape</option><option value="foam">Foam tape</option><option value="double_faced">Double faced tape</option><option value="cladding">Cladding tape</option></select></div> <div class="col-xs-1"><a href="javascript:void(0);" '+addOrMinusBtn+'</a></div> </div> </fieldset></div></div></div></div>');
         if(tape_type!=''){
            
            jQuery('#select_tgpd_glass_materials_tape_type_'+tgpcf+'_'+maxTape).val(tape_type);
         }
         //maxTapes[tgpcf]=maxTape[tgpcf]++; 
         if(curretPosition==0){
               jQuery("#btn11").click(function(){
                  addAndUpdateTape('0','','',1,false);
               }); 
            }
            
         maxTape++;
         maxTapes[tgpcf]=maxTape;
      
   }
   
  $('#btn_pluse.glass_holes_remove').click(function(event){
    glassHolesCounter--;
    event.preventDefault();
    event.stopPropagation();
  })
	function addAndUpdateHoles(glass_holes_type='',glass_holes_size='',holes_x_location='',holes_y_location='',holes_x_location_cl='',holes_y_location_cl='',holes_notes='',curretPosition,old=false){
		glassHolesCounter++;
		
		var tgpcf=	tgpd_current_item; //+tgpcf+'_'
		//<a href="javascript:void(0)" id="holes_btn_a" class="btn_pluse glass_holes_remove">-</a>
		
		var addOrMinusBtn;	var lbl ='';	
		if(curretPosition==0 && old==true){
		addOrMinusBtn = 	 '  id="holes_btn_a"  class="btn_pluse">+';
		
		}else{
		addOrMinusBtn =	 ' class="btn_pluse glass_holes_remove">-';
		}
	
		
		if(curretPosition==0){
			
			lbl ='<label class="leftalign ">Tape</label> ';
		}
		
		
		
		jQuery("#glass_piece_holes_div").append('<div class="glass_holes" id="glass_piece1_holes_'+maxHole+'"><div class="form-group custom_holes_glass"><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left holes-margin-left"><label class="leftalign hole_label" style="color:#337ab7;" for="text_tgpd_fabrication_holes">Hole '+glassHolesCounter+'</label></div><div class="col-xs-4 holes_btn"><a style="float:right;" href="javascript:void(0)" '+addOrMinusBtn+'</a></div></div><div class="col-xs-1 custom_holes_glass holes_close" style="visibility:hidden">X</div><div class=" col-xs-7 no-padding-left hole-type-margin"><label class="leftalign" for="glass_piece_holes_type_text">Type</label><select onchange="show_size(this)" class="form-control" id="glass_piece_holes_type_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_type_text"><option value="not_applicable">Hole Type</option><option value="Rectangle Square">Rectangle/Square</option><option value="Rectangle Square (Rounded Corners)">Rectangle/Square (Rounded Corners)</option><option value="Circle">Circle</option><option value="Counter Sunk (Circle)">Counter Sunk (Circle)</option></select></div><div class="col-xs-4 holes_btn hole-size"><label class="leftalign" for="glass_piece_holes_size_text">Size</label><input id="glass_piece_holes_size_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_size_text" type="text" class="form-control"  placeholder="Size"></div><div class="col-xs-4 holes_btn_2 location-row glass_pice_type_vir" style="display: none;"><div class="row"><div class="col-xs-6"><label style="font-size:9px;" class="leftalign" for="glass_piece_holes_exterior_size_text">EXTERIOR Size</label><input id="glass_piece_holes_exterior_size_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_exterior_size_text" type="text" class="form-control user-success" placeholder="Size"></div><div class="col-xs-6"><label style="font-size:10px;" class="leftalign" for="glass_piece_holes_interior_size_text">INTERIOR Size</label><input id="glass_piece_holes_interior_size_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_interior_size_text" type="text" class="form-control user-success" placeholder="Size"></div></div></div><div class="form-group"><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="col-xs-3" for="" style="padding-left: 0px;">Location C/L</label><div class="col-xs-4 col-xs-offset-1 col-sm-offset-1 no-padding-left start-hole"><label class="leftalign">Starting</label> </div><div class="col-xs-3 location-left" ><label class="leftalign">Location</label></div></div> </div><div class="col-xs-1 custom_holes_glass holes_close" style="color:#337ab7;text-align:center;">X</div><div class="col-xs-7 no-padding-left custom_holes_glass"><select class="form-control x-location2" name="glass_piece_holes_x_location_starting_point_text" id="glass_piece_holes_x_location_starting_point_text_'+tgpcf+'_'+maxHole+'" ><option value="not_applicable">X Location Starting Point</option><option value="Top">Top</option><option value="Bottom">Bottom</option><option value="Left">Left</option><option value="Right">Right</option></select></div><div class="col-xs-4 custom_holes_glass location-row"><input id="glass_piece_holes_x_location_cl_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_x_location_cl_text" type="text" class="form-control"  placeholder="X Location"></div><div class="col-xs-1 holes_close" style="color:#337ab7; text-align:center;">Y</div><div class="col-xs-7 no-padding-left custom_holes_glass"><select class="form-control x-location2" name="glass_piece_holes_y_location_starting_point_text" id="glass_piece_holes_y_location_starting_point_text_'+tgpcf+'_'+maxHole+'" ><option value="not_applicable">Y Location Starting Point</option><option value="Top">Top</option><option value="Bottom">Bottom</option><option value="Left">Left</option><option value="Right">Right</option></select></div><div class="col-xs-4 custom_holes_glass location-row"><input id="glass_piece_holes_y_location_cl_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_y_location_cl_text" type="text" class="form-control"  placeholder="Y Location"></div><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_holes_notes_text">Notes</label></div><div class="col-xs-1 holes_close" style="color:#337ab7; text-align:center; visibility:hidden">Y</div><div class="col-xs-11 custom_holes_glass mobile-responsive"><input id="glass_piece_holes_notes_text_'+tgpcf+'_'+maxHole+'" name="glass_piece_holes_notes_text" type="text" class="form-control" placeholder="Notes"></div></div>');
		

		//glass_holes_type='',glass_holes_size='',holes_x_location='',holes_y_location='',holes_x_location_cl='',holes_y_location_cl='',holes_notes=''
		
		if(!!glass_holes_type){								
				jQuery('#glass_piece_holes_type_text_'+tgpcf+'_'+maxHole).val(glass_holes_type);
			}
			
         if(glass_holes_type == 'Counter Sunk (Circle)'){

            var glass_holes_size_val = glass_holes_size.toString();



            var glass_holes_size_circle_array = glass_holes_size_val.split(',');

            //console.log('check_size_arr',glass_holes_size_circle_array);

            jQuery('#glass_piece_holes_exterior_size_text_'+tgpcf+'_'+maxHole).val(glass_holes_size_circle_array[0]);
            
            jQuery('#glass_piece_holes_interior_size_text_'+tgpcf+'_'+maxHole).val(glass_holes_size_circle_array[1]);

            $('#glass_piece_holes_interior_size_text_'+tgpcf+'_'+maxHole).parent().parent().parent().parent().children('.col-xs-4.holes_btn').css('display','none');
        $('#glass_piece_holes_interior_size_text_'+tgpcf+'_'+maxHole).parent().parent().parent().parent().children('.holes_btn_2').css('display','block');



         } else {
		
			if(!!glass_holes_size){								
				jQuery('#glass_piece_holes_size_text_'+tgpcf+'_'+maxHole).val(glass_holes_size);
			}

}
			if(!!holes_x_location){								
				jQuery('#glass_piece_holes_x_location_starting_point_text_'+tgpcf+'_'+maxHole).val(holes_x_location);
			}
	
if(!!holes_y_location){							
				jQuery('#glass_piece_holes_y_location_starting_point_text_'+tgpcf+'_'+maxHole).val(holes_y_location);
			}

if(!!holes_x_location_cl){							
				jQuery('#glass_piece_holes_x_location_cl_text_'+tgpcf+'_'+maxHole).val(holes_x_location_cl);
			}
			
			if(!!holes_y_location_cl){		
			
				jQuery('#glass_piece_holes_y_location_cl_text_'+tgpcf+'_'+maxHole).val(holes_y_location_cl);
			}
				
			if(!!holes_notes){							
				jQuery('#glass_piece_holes_notes_text_'+tgpcf+'_'+maxHole).val(holes_notes);
			}
		
			//maxTapes[tgpcf]=maxTape[tgpcf]++;
			
			
			
			if(curretPosition==0){
					jQuery("#holes_btn_a").click(function(){
						
						addAndUpdateHoles('','','','','','','',1,false)
						
					}); 
				}
				
			maxHole++;
		//	alert(maxHole);
			maxHoles[tgpcf]=maxHole;
		
	//	maxHoles++;
		
	}
    function addAndUpdateOffset(lite='',fill='',side='',offset='',note='',el=0)
   {



      //alert('here');
      elem = el+1;

      var len_off = $('#glass_piece_offset_div').find('form[id="offset_data_frm"]').length;



      if(el == 0 && len_off < 1){
         jQuery("#glass_piece_offset_div").append('<form title="'+elem+'" name="offset_data" id="offset_data_frm"><div class="glass_offset" id="glass_piece1_offset'+el+'"><div class="form-group custom_offset_glass"><div class="col-xs-6 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign hole_label" style="color:#337ab7;" for="text_tgpd_fabrication_offset">Offset '+elem+' </label></div><div class="col-xs-5 offset_btn"><a style="float:right; margin:0" href="javascript:void(0)" id="new_offset" onclick="add_new_offset()" class="btn_pluse">+</a></div></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_lite_text">Lite</label><select class="form-control" id="glass_piece_offset_lite_text" name="glass_piece_offset_lite_text[]"><option value="not_applicable">Select Lite</option><option value="Exterior (interior lite smaller)">Exterior (interior lite smaller)</option><option value="Interior (exterior lite smaller)">Interior (exterior lite smaller)</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_fill_text">Fill</label><select class="form-control" id="glass_piece_offset_Fill_text" name="glass_piece_offset_Fill_text[]"><option value="not_applicable">Select Fill</option><option selected value="90" >90</option><option value="Normal ">Normal </option><option value="Buttered ">Buttered </option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_side_text">Side</label><select class="form-control" id="glass_piece_offset_side_text" name="glass_piece_offset_side_text[]"><option value="not_applicable">Select Side</option><option value="Top" >Top</option><option value="Bottom">Bottom</option><option value="Left ">Left</option><option value="Right ">Right</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_offset_text">Offset</label><select class="form-control" id="glass_piece_offset_offset_text" name="glass_piece_offset_offset_text[]"><option value="not_applicable">Select offset</option><option value="1/6" >1/6</option><option value="1/8">1/8</option><option value="3/16">3/16</option><option value="1/4">1/4</option><option value="5/16">5/16</option><option value="3/8">3/8</option><option value="7/16">7/16</option><option value="1/2">1/2</option><option value="9/16">9/16</option><option value="5/8">5/8</option><option value="11/16">11/16</option><option value="3/4">3/4</option><option value="13/16">13/16</option><option value="7/8">7/8</option><option value="15/16">15/16</option><option value="1">1</option><option value="1 1/6">1 1/6</option><option value="1 1/8">1 1/8</option><option value="1 3/16">1 3/16</option><option value="1 1/4">1 1/4</option><option value="1 5/16">1 5/16</option><option value="1 3/8">1 3/8</option><option value="1 7/16">1 7/16</option><option value="1 1/2">1 1/2</option><option value="1 9/16">1 9/16</option><option value="1 5/8">1 5/8</option><option value="1 11/16">1 11/16</option><option value="1 3/4">1 3/4</option><option value="1 13/16">1 13/16</option><option value="1 7/8">1 7/8</option><option value="1 15/16">1 15/16</option><option value="2">2</option><option value="2 1/6">2 1/6</option><option value="2 1/8">2 1/8</option><option value="2 3/16">2 3/16</option><option value="2 1/4">2 1/4</option><option value="2 5/16">2 5/16</option><option value="2 3/8">2 3/8</option><option value="2 7/16">2 7/16</option><option value="2 1/2">2 1/2</option><option value="2 9/16">2 9/16</option><option value="2 5/8">2 5/8</option><option value="2 11/16">2 11/16</option><option value="2 3/4">2 3/4</option><option value="2 13/16">2 13/16</option><option value="2 7/8">2 7/8</option><option value="2 15/16">2 15/16</option><option value="3">3</option></select></div><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_notes_text">Notes</label></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left custom_offset_glass "><input id="glass_piece_offset_notes_text" name="glass_piece_offset_notes_text[]" type="text" class="form-control" placeholder="Notes"></div></div></form>');


      } else {
         //alert('okk');

         $('#offset_data_frm').attr('title',elem);

         jQuery("#offset_data_frm").append('<div class="glass_offset" id="glass_piece1_offset'+el+'"><div class="form-group custom_offset_glass"><div class="col-xs-6 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign hole_label" style="color:#337ab7;" for="text_tgpd_fabrication_offset">Offset '+elem+' </label></div><div class="col-xs-5 offset_btn"><a style="float:right;margin: 0 0 10 0;" href="javascript:void(0)" id="new_offset" onclick="remove_offset(this)" class="btn_pluse remove_offset">-</a></div></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_lite_text">Lite</label><select class="form-control" id="glass_piece_offset_lite_text" name="glass_piece_offset_lite_text[]"><option value="not_applicable">Select Lite</option><option value="Exterior (interior lite smaller)">Exterior (interior lite smaller)</option><option value="Interior (exterior lite smaller)">Interior (exterior lite smaller)</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_fill_text">Fill</label><select class="form-control" id="glass_piece_offset_Fill_text" name="glass_piece_offset_Fill_text[]"><option value="not_applicable">Select Fill</option><option value="90" >90</option><option value="Normal ">Normal </option><option value="Buttered ">Buttered </option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_side_text">Side</label><select class="form-control" id="glass_piece_offset_side_text" name="glass_piece_offset_side_text[]"><option value="not_applicable">Select Side</option><option value="Top" >Top</option><option value="Bottom">Bottom</option><option value="Left ">Left</option><option value="Right ">Right</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_offset_text">Offset</label><select class="form-control" id="glass_piece_offset_offset_text" name="glass_piece_offset_offset_text[]"><option value="not_applicable">Select offset</option><option value="1/6" >1/6</option><option value="1/8">1/8</option><option value="3/16">3/16</option><option value="1/4">1/4</option><option value="5/16">5/16</option><option value="3/8">3/8</option><option value="7/16">7/16</option><option value="1/2">1/2</option><option value="9/16">9/16</option><option value="5/8">5/8</option><option value="11/16">11/16</option><option value="3/4">3/4</option><option value="13/16">13/16</option><option value="7/8">7/8</option><option value="15/16">15/16</option><option value="1">1</option><option value="1 1/6">1 1/6</option><option value="1 1/8">1 1/8</option><option value="1 3/16">1 3/16</option><option value="1 1/4">1 1/4</option><option value="1 5/16">1 5/16</option><option value="1 3/8">1 3/8</option><option value="1 7/16">1 7/16</option><option value="1 1/2">1 1/2</option><option value="1 9/16">1 9/16</option><option value="1 5/8">1 5/8</option><option value="1 11/16">1 11/16</option><option value="1 3/4">1 3/4</option><option value="1 13/16">1 13/16</option><option value="1 7/8">1 7/8</option><option value="1 15/16">1 15/16</option><option value="2">2</option><option value="2 1/6">2 1/6</option><option value="2 1/8">2 1/8</option><option value="2 3/16">2 3/16</option><option value="2 1/4">2 1/4</option><option value="2 5/16">2 5/16</option><option value="2 3/8">2 3/8</option><option value="2 7/16">2 7/16</option><option value="2 1/2">2 1/2</option><option value="2 9/16">2 9/16</option><option value="2 5/8">2 5/8</option><option value="2 11/16">2 11/16</option><option value="2 3/4">2 3/4</option><option value="2 13/16">2 13/16</option><option value="2 7/8">2 7/8</option><option value="2 15/16">2 15/16</option><option value="3">3</option></select></div><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_notes_text">Notes</label></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left custom_offset_glass "><input id="glass_piece_offset_notes_text" name="glass_piece_offset_notes_text[]" type="text" class="form-control" placeholder="Notes"></div></div>');


      }

      

      
   

      if(lite != ''){

         $('#glass_piece1_offset'+el).find('#glass_piece_offset_lite_text').val(lite);

      }

      if(fill != ''){

         $('#glass_piece1_offset'+el).find('#glass_piece_offset_Fill_text').val(fill);

      }

      if(side != ''){

         $('#glass_piece1_offset'+el).find('#glass_piece_offset_side_text').val(side);

      }

      if(offset != ''){

         $('#glass_piece1_offset'+el).find('#glass_piece_offset_offset_text').val(offset);

      }

      if(note != ''){

         $('#glass_piece1_offset'+el).find('#glass_piece_offset_notes_text').val(note);

      }



   }

	
	jQuery(document).ready(function(){
		
		jQuery("#holes_btn_a").click(function(){
			// text_tgpd_glass_materials_caulk_amount++;
			addAndUpdateHoles();
		}); 
		
		
		jQuery("#btn12").click(function(){
			// text_tgpd_glass_materials_caulk_amount++;
			addAndUpdateCulk();
		}); 
		 jQuery('body').delegate('.removeItem', 'click', function () {
			

        var current_tem_ct =  jQuery(this).closest('.myCustom').parent().attr('id');

        if(current_tem_ct != ''){

         current_tem_ct = parseInt(current_tem_ct) - 1;

         jQuery(this).closest('.myCustom').parent().attr('id',current_tem_ct);

        }

         jQuery(this).closest('.myCustom').remove();
			//maxChaulks[tgpcf]--;
		 });
	 
		jQuery("#btn11").click(function(){
			addAndUpdateTape();
      
		});
	jQuery("#btn13").click(function(){
					addAndUpdateChannel();
		

		});
	jQuery("#btn14").click(function(){
			addAndUpdateScaffolding();
		});
		jQuery('body').delegate('.removeItem1', 'click', function () {
			//maxTapes[tgpcf]--;

         var current_tem_ct =  jQuery(this).closest('.myCustom1').parent().attr('id');

        if(current_tem_ct != ''){

         current_tem_ct = parseInt(current_tem_ct) - 1;

         jQuery(this).closest('.myCustom1').parent().attr('id',current_tem_ct);

        }
			jQuery(this).closest('.myCustom1').remove();
		});
		
		jQuery('body').delegate('.glass_holes_remove', 'click', function () {
			//alert(tgpd_current_item);
			maxHole--;
			glassHolesCounter--;
			//tgpcf--;
			jQuery(this).closest('.glass_holes').remove();
			div_cnt = 0;
			jQuery("#glass_piece_holes_div").find("div.glass_holes").each(function(j, e) {
				//alert(maxHoles[tgpcf]);
				cnt = "_"+tgpd_current_item+"_"+div_cnt
                jQuery(this).attr("id","glass_piece1_holes_"+div_cnt);
				jQuery(this).find("label.hole_label").text('Hole '+(div_cnt+1));
				
				jQuery(this).find("input[name=glass_piece_holes_type_text]").attr("id","glass_piece_holes_type_text"+cnt);
				jQuery(this).find("input[name=glass_piece_holes_size_text]").attr("id","glass_piece_holes_size_text"+cnt);

				jQuery(this).find("input[name=glass_piece_holes_x_location_starting_point_text]").attr("id","glass_piece_holes_x_location_starting_point_text"+cnt);
				jQuery(this).find("input[name=glass_piece_holes_x_location_cl_text]").attr("id","glass_piece_holes_x_location_cl_text"+cnt);

				jQuery(this).find("input[name=glass_piece_holes_y_location_starting_point_text]").attr("id","glass_piece_holes_y_location_starting_point_text"+cnt);
				jQuery(this).find("input[name=glass_piece_holes_y_location_cl_text]").attr("id","glass_piece_holes_y_location_cl_text"+cnt);


				jQuery(this).find("input[name=glass_piece_holes_notes_text]").attr("id","glass_piece_holes_notes_text"+cnt);
				div_cnt++;
            });
		});
		
			glass_piece_disabled();
			
		if(back_date_entry ==true){
			$('#litethick_see').show();
		}else{
			$('#litethick_see').hide();
		}
		
		 glass_piece_disabled();
	});
	
	//var fix_date =  new Date("2018-01-06 18:00:18");//YYYY-DD-MM H:i:s 2017-12-11
	
	//var created_date =  new Date(dateNew[0]);

	
	
function showHideChulkAndTape(){
//	alert('showHideChulkAndTape');
	jQuery('.chaulksection .addqty').hide();
//alert("showHideChulkAndTape tgpd_current_item current >> "+ tgpd_current_item)
jQuery(".chaulksection #add_quntity_"+tgpd_current_item).show();
}	

function initializePlusMinusButtons(){
  $(document.body).on('click','.btn-number-plus-minus',function(e){
  e.preventDefault();
  
  fieldName = $(this).attr('data-field');
  type      = $(this).attr('data-type');
  var input = $("input[name='"+fieldName+"']");
  var currentVal = parseInt(input.val());
  if (!isNaN(currentVal)) {
      if(type == 'minus') {
          console.log("it's a minus")
          if(currentVal > input.attr('min')) {
              input.val(currentVal - 1).change();
          } 
          if(parseInt(input.val()) == input.attr('min')) {
              $(this).attr('disabled', true);
          }

      } else if(type == 'plus') {
        console.log("it's a plus")

          if(currentVal < input.attr('max')) {
              input.val(currentVal + 1).change();
          }
          if(parseInt(input.val()) == input.attr('max')) {
              $(this).attr('disabled', true);
          }

      }
  } else {
      input.val(0);
  }
});
$(document.body).on('focusin','.input-number-plus-minus',function(){
 $(this).data('oldValue', $(this).val());
});
$(document.body).on('change', '.input-number-plus-minus', function(){
  console.log("inside .input-number-plus-minus change");
  minValue =  parseInt($(this).attr('min'));
  maxValue =  parseInt($(this).attr('max'));
  valueCurrent = parseInt($(this).val());
  
  name = $(this).attr('name');
  if(valueCurrent >= minValue) {
      $(".btn-number-plus-minus[data-type='minus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
      alert('Sorry, the minimum value was reached');
      $(this).val($(this).data('oldValue'));
  }
  if(valueCurrent <= maxValue) {
      $(".btn-number-plus-minus[data-type='plus'][data-field='"+name+"']").removeAttr('disabled')
  } else {
      alert('Sorry, the maximum value was reached');
      $(this).val($(this).data('oldValue'));
  }
  
  
});
$(".input-number-plus-minus").keydown(function (e) {
      // Allow: backspace, delete, tab, escape, enter and .
      if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||
           // Allow: Ctrl+A
          (e.keyCode == 65 && e.ctrlKey === true) || 
           // Allow: home, end, left, right
          (e.keyCode >= 35 && e.keyCode <= 39)) {
               // let it happen, don't do anything
               return;
      }
      // Ensure that it is a number and stop the keypress
      if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
          e.preventDefault();
      }
  });
}
	
	
	
	
	jQuery(document).ready(function(){
				
			initializePlusMinusButtons();
		/*	jQuery('body').delegate('.glass_holes_remove', 'click', function () {
			//	alert('asdf');
			//maxTapes[tgpcf]--;
				glassHolesCounter--;
			jQuery(this).closest('.glass_holes').remove();
		});
    */
		
		
		
	});
	//fieldset_tgpd_glass_damage_waiver_reminder_section_for_select
	//fieldset_tgpd_glass_damage_waiver_reminder_section
	
	
	jQuery('#checkbox_tgpd_glass_reminders_add_disclamers').click(function(){
		$('#fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers').toggle();
		$('#fieldset_tgpd_glass_disclamers_reminder_section').toggle();
		//$('#fieldset_tgpd_glass_disclamers_reminder_section_select_glass_disclamers').sh();
		
		
	});
	
	jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').click(function(){
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section_for_select').toggle();
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').toggle();
      jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').parent().toggleClass('damage_comment');
	});

    function damageAnddisclaimers(){
		var hidedamage = jQuery('#select_tgpd_glass_damage_waiver').val();
		var hidisclaimers = jQuery('#select_tgpd_glass_disclamers').val();
		
		if(hidedamage == 'Removal/Reinstall of Glass Currently Installed' || hidedamage == 'Handling of Customers Materials' || hidedamage == 'Adjacent Glass'){
			jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').hide();
		}else{
			jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').show();
		}
		
		if(hidisclaimers == 'Wood Bead' || hidisclaimers == 'Painted Frames (AFTERMARKET)' || hidisclaimers == 'TBD'){
			jQuery('#fieldset_tgpd_glass_disclamers_reminder_section').hide();
		}else{
			jQuery('#fieldset_tgpd_glass_disclamers_reminder_section').show();
		}
	}


function hideDamageAndDisclaimersDelete(){
		
		jQuery('#fieldset_tgpd_glass_damage_waiver_reminder_section').hide();
		jQuery('#fieldset_tgpd_glass_disclamers_reminder_section').hide();
		jQuery('#text_tgpd_glass_damage_waiver_reminder_section').hide();
		jQuery('#text_tgpd_glass_disclamers_reminder_section').hide(); 
		
	}
	  
// written on 13/08/18 for dessble and enable repair doors and new doors
  function repair_doors_disable(){
        console.log('repair_doors_disable'+ tagd_items.length); 
        if(tagd_items.length < 1){
         $('#door-repairs').find('input, textarea, select').prop('disabled','disabled');   
        }
        // jQuery('#select-repair-door-category').prop('disabled','disabled');   
        // jQuery('#select-repair-door-type').prop('disabled','disabled'); 
        // jQuery('#select_tagd_gdr_a_single_location').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_handles_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_handles').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_finish').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_panic_device_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_panic_device').prop('disabled','disabled');  
        // jQuery('#select_tagd_gdr_a_single_door_closer_status').prop('disabled','disabled');  
        // jQuery('#select_tagd_gdr_a_single_bottom_insert_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_top_pivot').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_bottom_insert').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_patch_fittings_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_bottom_pivot_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_patch_fittings').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_bottom_pivot').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_top_inserts_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_floor_door_closer_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_top_inserts').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_floor_door_closer').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_header_door_stop_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_threshold_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_header_door_stop').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_threshold').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_type_of_glass_status').prop('disabled','disabled');
        // jQuery('#select_tagd_gdr_a_single_type_of_glass').prop('disabled','disabled');
        // jQuery('#textarea_tagd_gdr_a_single_notes').prop('disabled','disabled');
   
    }
   
    function repair_doors_enabled(){
         $('#door-repairs').find('input, textarea, select').removeAttr('disabled');
         
        // jQuery('#select-repair-door-category').removeAttr('disabled');  
        // jQuery('#select-repair-door-type').removeAttr('disabled');   
        // jQuery('#select_tagd_gdr_a_single_location').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_handles_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_handles').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_finish').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_panic_device_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_panic_device').removeAttr('disabled'); 
        // jQuery('#select_tagd_gdr_a_single_door_closer_status').removeAttr('disabled'); 
        // jQuery('#select_tagd_gdr_a_single_bottom_insert_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_top_pivot').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_bottom_insert').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_patch_fittings_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_bottom_pivot_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_patch_fittings').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_bottom_pivot').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_top_inserts_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_floor_door_closer_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_top_inserts').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_floor_door_closer').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_header_door_stop_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_threshold_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_header_door_stop').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_threshold').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_type_of_glass_status').removeAttr('disabled');
        // jQuery('#select_tagd_gdr_a_single_type_of_glass').removeAttr('disabled');
        // jQuery('#textarea_tagd_gdr_a_single_notes').removeAttr('disabled');
    }
    
    function new_doors_disable(){
         console.log('new_doors_disable'+ newdoor_items.length); 
        if(newdoor_items.length < 1){
            $('#door-new').find('input, textarea, select').attr('disabled','disabled');
         }   
        // jQuery('#select-new-door-category').prop('disabled','disabled');   
        // jQuery('.door_hide_function').prop('disabled','disabled');
        // jQuery('#div_upload_picture_generic input').prop('disabled','disabled');
        // jQuery('#select_Agndsing_a_single_door_style').prop('disabled','disabled');
        // jQuery('#select_Agndsing_j_closers').prop('disabled','disabled');
        // jQuery('#text_Agndsing_b_single_rough_opening_width').prop('disabled','disabled');
        // jQuery('#text_Agndsing_b_single_rough_opening_height').prop('disabled','disabled');
        // jQuery('#select_Agndsing_k_looks').prop('disabled','disabled');
        // jQuery('#text_Agndsing_c_single_door_opening_width').prop('disabled','disabled');
        // jQuery('#text_Agndsing_c_single_door_opening_height').prop('disabled','disabled');
        // jQuery('#select_Agndsing_l_pull_hardware').prop('disabled','disabled');
        // jQuery('#text_Agndsing_d_door_size_width').prop('disabled','disabled');
        // jQuery('#text_Agndsing_d_door_size_height').prop('disabled','disabled');
        // jQuery('#select_Agndsing_m_header').prop('disabled','disabled');
        // jQuery('#text_Agndsing_e_door_hand_right').prop('disabled','disabled');
        // jQuery('#select_Agndsing_n_threshold').prop('disabled','disabled');
        // jQuery('#select_Agndsing_f_glass_type').prop('disabled','disabled');
        // jQuery('#select_Agndsing_o_sidelight_head_condition').prop('disabled','disabled');
        // jQuery('#text_Agndsing_g_top_rail_width').prop('disabled','disabled');
        // jQuery('#select_Agndsing_p_sidelight_sill_condition').prop('disabled','disabled');
        // jQuery('#text_Agndsing_h_bottom_rail_width').prop('disabled','disabled');
        // jQuery('#select_Agndsing_q_sidelites_configuration').prop('disabled','disabled');
        // jQuery('#select_Agndsing_i_metal_finish').prop('disabled','disabled');
        // jQuery('#all_replacement_heandle_newdoor').prop('disabled','disabled');
        // jQuery('#textarea_newdoor_instructions').prop('disabled','disabled');
        
        
        
    }
    
    function new_doors_enabled(){
        
         $('#door-new').find('input, textarea, select').removeAttr('disabled');  
        // jQuery('#select-new-door-category').removeAttr('disabled');  
        // jQuery('.door_hide_function').removeAttr('disabled');
        // jQuery('#div_upload_picture_generic input').removeAttr('disabled');
        // jQuery('#select_Agndsing_a_single_door_style').removeAttr('disabled');
        // jQuery('#select_Agndsing_j_closers').removeAttr('disabled');
        // jQuery('#text_Agndsing_b_single_rough_opening_width').removeAttr('disabled');
        // jQuery('#text_Agndsing_b_single_rough_opening_height').removeAttr('disabled');
        // jQuery('#select_Agndsing_k_looks').removeAttr('disabled');
        // jQuery('#text_Agndsing_c_single_door_opening_width').removeAttr('disabled');
        // jQuery('#text_Agndsing_c_single_door_opening_height').removeAttr('disabled');
        // jQuery('#select_Agndsing_l_pull_hardware').removeAttr('disabled');
        // jQuery('#text_Agndsing_d_door_size_width').removeAttr('disabled');
        // jQuery('#text_Agndsing_d_door_size_height').removeAttr('disabled');
        // jQuery('#select_Agndsing_m_header').removeAttr('disabled');
        // jQuery('#text_Agndsing_e_door_hand_right').removeAttr('disabled');
        // jQuery('#select_Agndsing_n_threshold').removeAttr('disabled');
        // jQuery('#select_Agndsing_f_glass_type').removeAttr('disabled');
        // jQuery('#select_Agndsing_o_sidelight_head_condition').removeAttr('disabled');
        // jQuery('#text_Agndsing_g_top_rail_width').removeAttr('disabled');
        // jQuery('#select_Agndsing_p_sidelight_sill_condition').removeAttr('disabled');
        // jQuery('#text_Agndsing_h_bottom_rail_width').removeAttr('disabled');
        // jQuery('#select_Agndsing_q_sidelites_configuration').removeAttr('disabled');
        // jQuery('#select_Agndsing_i_metal_finish').removeAttr('disabled');
        //   jQuery('#all_replacement_heandle_newdoor').removeAttr('disabled');
        // jQuery('#textarea_newdoor_instructions').removeAttr('disabled');
        
    }
    jQuery(document).ready(function(){
        $('.offset_btn').on('click', function(){
         
         jQuery("#glass_piece_offset_div").append('<div class="glass_offset" id="glass_piece1_offset"><div class="form-group custom_offset_glass"><div class="col-xs-6 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign hole_label" style="color:#337ab7;" for="text_tgpd_fabrication_offset"> </label></div><div class="col-xs-4 offset_btn"><a style="float:right; margin:0" href="javascript:void(0)" id="new_offset" class="btn_pluse remove_offset">-</a></div></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_lite_text">Lite</label><select class="form-control" id="glass_piece_offset_lite_text" name="glass_piece_offset_lite_text[]"><option value="not_applicable">Select Lite</option><option value="Exterior (interior lite smaller)">Exterior (interior lite smaller)</option><option value="Interior (exterior lite smaller)">Interior (exterior lite smaller)</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_fill_text">Fill</label><select class="form-control" id="glass_piece_offset_Fill_text" name="glass_piece_offset_Fill_text[]"><option value="not_applicable">Fill Type</option><option value="90" >90</option><option value="Normal ">Normal </option><option value="Buttered ">Buttered </option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_side_text">Side</label><select class="form-control" id="glass_piece_offset_side_text" name="glass_piece_offset_side_text[]"><option value="not_applicable">Select Side</option><option value="Top" >Top</option><option value="Bottom">Bottom</option><option value="Left ">Left</option><option value="Right ">Right</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_offset_text">Offset</label><select class="form-control" id="glass_piece_offset_offset_text" name="glass_piece_offset_offset_text[]"><option value="not_applicable">Select Offset</option><option value="1/6" >1/6</option><option value="1/8">1/8</option><option value="3/16">3/16</option><option value="1/4">1/4</option><option value="5/16">5/16</option><option value="3/8">3/8</option><option value="7/16">7/16</option><option value="1/2">1/2</option><option value="9/16">9/16</option><option value="5/8">5/8</option><option value="11/16">11/16</option><option value="3/4">3/4</option><option value="13/16">13/16</option><option value="7/8">7/8</option><option value="15/16">15/16</option><option value="1">1</option><option value="1/6">1 1/6</option><option value="1/8">1 1/8</option><option value="3/16">1 3/16</option><option value="1/4">1 1/4</option><option value="5/16">1 5/16</option><option value="3/8">1 3/8</option><option value="7/16">1 7/16</option><option value="1/2">1 1/2</option><option value="9/16">1 9/16</option><option value="5/8">1 5/8</option><option value="11/16">1 11/16</option><option value="3/4">1 3/4</option><option value="13/16">1 13/16</option><option value="7/8">1 7/8</option><option value="15/16">1 15/16</option><option value="2">2</option><option value="1/6">2 1/6</option><option value="1/8">2 1/8</option><option value="3/16">2 3/16</option><option value="1/4">2 1/4</option><option value="5/16">2 5/16</option><option value="3/8">2 3/8</option><option value="7/16">2 7/16</option><option value="1/2">2 1/2</option><option value="9/16">2 9/16</option><option value="5/8">2 5/8</option><option value="11/16">2 11/16</option><option value="3/4">2 3/4</option><option value="13/16">2 13/16</option><option value="7/8">2 7/8</option><option value="15/16">2 15/16</option><option value="3">3</option></select></div><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_notes_text">Notes</label></div><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left custom_offset_glass"><input id="glass_piece_offset_notes_text" name="glass_piece_offset_notes_text[]" type="text" class="form-control" placeholder="Notes"></div></div>');
      });
   
        $('#text_tgpd_fabrication_polished_edges_hide').prop('checked', true);
   $('#offset_data_frm').hide();
   $('#text_tgpd_fabrication_pattern_hide').prop('checked', true);
   $('#text_tgpd_fabrication_holes_hide_new').prop('checked', true);
   $('#text_tgpd_fabrication_offset_hide_new').prop('checked', true);
$('#text_tgpd_fabrication_gride_hide').prop('checked', true);

   $('#text_tgpd_fabrication_polished_edges').hide();
   $('#text_tgpd_fabrication_pattern').hide();
   $('#glass_piece_holes_div').hide();

   /*sag-153*/
  $('#text_tgpd_fabrication_gride_hide').addClass('hello123');
   $('#grids_and_fabrication').hide();
   /*sag-153*/


   });
   function show_size(value){
      $(value).parent().parent().addClass("tttttt");
      
      var text = $(value).attr('value');
      console.log(value);
      console.log(text);
      if( text == 'Counter Sunk (Circle)'){
         
         //$(value).parent().addClass('top_m');
         $(value).parent().parent().children('.col-xs-4.holes_btn').css('display','none');
         $(value).parent().parent().find('.holes_btn_2').css('display','block');
      }else{
         $(value).parent().removeClass('top_m');
         $(value).parent().parent().children('.col-xs-4.holes_btn').css('display','block');
         $(value).parent().parent().find('.holes_btn_2').css('display','none');
      }
   }

   function add_new_offset(){

     var glassOffsetCounter1 =  jQuery('#offset_data_frm').attr('title');

     glassOffsetCounter1++;

      //glassOffsetCounter++;
      jQuery("#offset_data_frm").append('<div class="glass_offset" style="margin-top: 20px;" ><div class="form-group custom_offset_glass"><div class="col-xs-6 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign hole_label" style="color:#337ab7;" for="text_tgpd_fabrication_offset">Offset '+glassOffsetCounter1+'</label></div><div class="col-xs-5 offset_btn"><a style="float:right; margin:0 0 10 0 " href="javascript:void(0)" id="new_offset" onclick="remove_offset(this)" class="btn_pluse remove_offset">-</a></div></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_lite_text">Lite</label><select class="form-control" id="glass_piece_offset_lite_text" name="glass_piece_offset_lite_text[]"><option value="not_applicable">Select Lite</option><option value="Exterior (interior lite smaller)">Exterior (interior lite smaller)</option><option value="Interior (exterior lite smaller)">Interior (exterior lite smaller)</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_fill_text">Fill</label><select class="form-control" id="glass_piece_offset_Fill_text" name="glass_piece_offset_Fill_text[]"><option value="not_applicable">Selcet Fill</option><option  selected value="90" >90</option><option value="Normal ">Normal </option><option value="Buttered ">Buttered </option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_side_text">Side</label><select class="form-control" id="glass_piece_offset_side_text" name="glass_piece_offset_side_text[]"><option value="not_applicable">Select Side</option><option value="Top" >Top</option><option value="Bottom">Bottom</option><option value="Left ">Left</option><option value="Right ">Right</option></select></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_offset_text">Offset</label><select class="form-control" id="glass_piece_offset_offset_text" name="glass_piece_offset_offset_text[]"><option value="not_applicable"> Select offset</option><option value="1/6" >1/6</option><option value="1/8">1/8</option><option value="3/16">3/16</option><option value="1/4">1/4</option><option value="5/16">5/16</option><option value="3/8">3/8</option><option value="7/16">7/16</option><option value="1/2">1/2</option><option value="9/16">9/16</option><option value="5/8">5/8</option><option value="11/16">11/16</option><option value="3/4">3/4</option><option value="13/16">13/16</option><option value="7/8">7/8</option><option value="15/16">15/16</option><option value="1">1</option><option value="1 1/6">1 1/6</option><option value="1 1/8">1 1/8</option><option value="1 3/16">1 3/16</option><option value="1 1/4">1 1/4</option><option value="1 5/16">1 5/16</option><option value="1 3/8">1 3/8</option><option value="1 7/16">1 7/16</option><option value="1 1/2">1 1/2</option><option value="1 9/16">1 9/16</option><option value="1 5/8">1 5/8</option><option value="1 11/16">1 11/16</option><option value="1 3/4">1 3/4</option><option value="1 13/16">1 13/16</option><option value="1 7/8">1 7/8</option><option value="1 15/16">1 15/16</option><option value="2">2</option><option value="2 1/6">2 1/6</option><option value="2 1/8">2 1/8</option><option value="2 3/16">2 3/16</option><option value="2 1/4">2 1/4</option><option value="2 5/16">2 5/16</option><option value="2 3/8">2 3/8</option><option value="2 7/16">2 7/16</option><option value="2 1/2">2 1/2</option><option value="2 9/16">2 9/16</option><option value="2 5/8">2 5/8</option><option value="2 11/16">2 11/16</option><option value="2 3/4">2 3/4</option><option value="2 13/16">2 13/16</option><option value="2 7/8">2 7/8</option><option value="2 15/16">2 15/16</option><option value="3">3</option></select></div><div class="col-xs-11 col-xs-offset-1 col-sm-offset-1 no-padding-left"><label class="leftalign" for="glass_piece_offset_notes_text">Notes</label></div><div class="col-xs-7 col-xs-offset-1 col-sm-offset-1 no-padding-left custom_offset_glass"><input id="glass_piece_offset_notes_text" name="glass_piece_offset_notes_text[]" type="text" class="form-control" placeholder="Notes"></div></div>');
      
      $('#offset_data_frm').attr('title',glassOffsetCounter1);
   }
   function remove_offset(value){

       var glassOffsetCounter1 =  jQuery('#offset_data_frm').attr('title');

     
      glassOffsetCounter1--;
      $(value).parent().parent().parent().remove();
      $('#offset_data_frm').attr('title',glassOffsetCounter1);
   }

   $('#text_tgpd_fabrication_offset_hide_new').click(function(){
         var off_che_att =   $(this).attr('checked');

            if(off_che_att == 'checked'){

               $('#offset_data_frm').hide();

            } else {

               $('#offset_data_frm').show();

            }
   })

/****sag-158***/   
$('#text_tgpd_grids_color').change(function(){ 
   var col_val = $(this).val();

   if(col_val == 'custom'){

      $('#text_tgpd_grids_color_custom').show();
      $('.text_tgpd_grids_color_custom_label').show();

   } else {

      $('#text_tgpd_grids_color_custom').hide();
      $('.text_tgpd_grids_color_custom_label').hide();
   }

});

/****sag-158***/  


/****sag-163***/   
$('#text_project_parking').change(function(){ 
   var par_val = $(this).val();

   if(par_val == 'Other'){

       $('#text_project_postal_code_label').show();
      $('#text_project_parking_custom').show();
      $('#text_project_parking_custom').val('');

   } else {

      $('#text_project_postal_code_label').hide();
      $('#text_project_parking_custom').hide();
      $('#text_project_parking_custom').val('');
   }

});

/****sag-163***/
	
		