
  //get pictures and sketches and add to the pdf
  //---------------------    GET ALL IMAGES   ---------------------
  //--------------------- VERY IMPORTANT LINK ---------------------
  //https://siongui.github.io/2012/09/29/javascript-single-callback-for-multiple-asynchronous-xhr-requests/

 getParameterByName = function(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, "\\$&");
   var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, " "));
}
//-------------native PDF Functions-----------
addLines = function(numLines, doc){
  for (var i =0; i<numLines; i++){
    doc.moveDown()
  }
}

titleCase = function(str) {
   var splitStr = str.toLowerCase().split(' ');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
   }
   // Directly return the joined string
   return splitStr.join(' ');
}

boolToHuman = function(val){
  if(val == 0 ){
    return "NO"
  }
  return "YES"
}

humanize = function(str) {
  //remove _ and capitalize words
  var frags = str.split('_');
  for (i=0; i<frags.length; i++) {
    frags[i] = frags[i].charAt(0).toUpperCase() + frags[i].slice(1);
  }
  return frags.join(' ');
}

handleIfEmpty = function(str){
  if(str.length == 0){
    doc.moveDown() //skip line
  } else{
    doc.text(str);
  }
}
shouldDisplay = function(fieldVal){
  return fieldVal!= null && fieldVal != 'null' && fieldVal != "not_applicable" && fieldVal != 0 &&  fieldVal != " " && fieldVal != 0.00
}

printStatusAndField = function(statusVal, fieldVal, pdfDoc){

  if (statusVal == "adjust_align"){
    pdfDoc.fillColor(adjustColor);
    pdfDoc.text('A',  350, pdfDoc.y, {
      lineBreak : false,
      lineGap   : 4,
    }).fillColor(bodyColor).fontSize(smallFontSize).text(' - ' + fieldVal, {lineBreak : true, lineGap : 4});
  }
  if(statusVal == 'info_only'){
    pdfDoc.fillColor(infoColor);
    pdfDoc.text('I ', 350, pdfDoc.y, {
      //here it is,
      lineBreak : false,
      lineGap   : 4,
    }).fillColor(bodyColor).fontSize(smallFontSize).text(' - ' + fieldVal, {lineBreak : true, lineGap : 4});
  }
  if(statusVal == 'replace'){
    pdfDoc.fillColor(replaceColor);
    pdfDoc.text('R', 350, pdfDoc.y, {
      //here it is,
      lineBreak : false,
      lineGap   : 4,
    }).fillColor(bodyColor).fontSize(smallFontSize).text(' - ' + fieldVal, {lineBreak : true, lineGap : 4});
  }
  if(statusVal == "not_applicable"){
    pdfDoc.fillColor(naColor);
    pdfDoc.text('NA', 350, pdfDoc.y, {
      //here it is,
      lineBreak : false,
      lineGap   : 4,
    }).fillColor(bodyColor).fontSize(smallFontSize).text(' - ' + fieldVal, {lineBreak : true, lineGap : 4});
  }
}

fieldAndStatus = function(item, fieldName, curDoc){
  var fieldNameStatus = fieldName + "_status";
  if( shouldDisplay(item[fieldName])){
    var lineText = item[fieldName].replace(/-/g, " ");
    lineText = lineText.replace(/_/g, " ");
    lineText = lineText.replace(/\\/g, "");
    printStatusAndField(item[fieldNameStatus], lineText, curDoc );
  }
}

glassPricingText = function(bool){
  if(bool == 0 ){
    return "SAG"
  } else{
    return "Quote"
  }
}

setFont = function(color, size, doc){
  doc.fontSize(size);
  doc.fillColor(color);
}
//http://stackoverflow.com/questions/9267899/arraybuffer-to-base64-encoded-string
_arrayBufferToBase64 = function( buffer ) {
  var binary = '';
  var bytes = new Uint8Array( buffer );
  var len = bytes.byteLength;
  for (var i = 0; i < len; i++) {
      binary += String.fromCharCode( bytes[ i ] );
  }
  return window.btoa( binary );
}

/**
 * Cross-Browser AJAX request (XMLHttpRequest)
 *
 * @param {string} url The url of HTTP GET (AJAX) request.
 * @param {function} callback The callback function if the request succeeds.
 * @param {function} failCallback The callback function if the request fails.
 */


var ix =1;
imageAjaxRequest = function(photoObject, callback, failCallback, pdfDocument, picWidth, newPage, curItem, x , y) {
    
  console.log("inside -------------imageAjaxRequest------------");
  console.log("newPage is " + newPage);
  var xmlhttp;
  var imageX;
  var imageY;
  if(x && y){ //position not defined
    console.log("IF X && Y");
    imageX = x;
    imageY = y;
  } else{
    console.log(" NOT IF X && Y");
    imageX = pdfDocument.x ;
    imageY = pdfDocument.y ;
  }

  if (window.XMLHttpRequest){
    xmlhttp=new XMLHttpRequest();
    console.log("xmlhttp: " + xmlhttp);
  }
  else{
    xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
//	console.log(" photo url "+ photoObject.url);
  //xmlhttp.open("GET", photoObject.url, true);
  

  xmlhttp.open("POST", ajax_object.ajax_url+'?action=browse_s3&imgUrl='+photoObject.url, true);
  xmlhttp.responseType = "arraybuffer";
  //console.log('photoObject_length'+photoObject.length);
  var arr;
	  xmlhttp.onload = function(event){
    var b64 = _arrayBufferToBase64(this.response);
    var dataURL="data:image/jpeg;base64," + b64;
    if(dataURL){
      if(newPage){
        pdfDocument.addPage();
		pdfDocument.y+=40;
        pdfDocument.text(photoObject.header);

           

              pdfDocument.image(dataURL, pdfDocument.x, pdfDocument.y, {width:  picWidth });
              
             // sleep(1000);
            
            //console.log('check_data_url_new' + photoObject.url);
            //setTimeout(function() {  pdfDocument.image(dataURL, pdfDocument.x, pdfDocument.y, {width:  picWidth }); }, 1000);
             } else{

       
        pdfDocument.image(dataURL, imageX, imageY, {width:  picWidth }); // add image to the page  at saved point
        
      }
      arr = null
      callback(photoObject.url + curItem);
    } else{
      failCallback(photoObject.url + curItem);
    }
  }
  xmlhttp.send(null);
};

//urls is an array of objects that prints to the end of the pdf
//urls = [
  // {
  //   url    : "photo_url",
  //   header : "Some header"
  // }
//]

imagesAjaxRequests = function(photoObjects, callbackMulti, failCallbackMulti, pdfDocument, picWidth, newPage, x, y) {
	console.log(photoObjects);
  console.log("inside AjaxRequestsMulti");
  var isAllCallsCompleted = false;
  var isCallFailed = false;
  var data = {};
  console.log("size of photoObjects Coming is " + photoObjects.length)

    var callback = function(url) {
      console.log("inside callback from writing one photo to pd")
      if (isCallFailed) return;
      data[url] = true;
      // get size of data
      var size = 0;
      for (var index in data) {
        console.log("inside index in data loop")
        console.log("index is : " + index)
        if (data.hasOwnProperty(index))
          console.log("size is " + size)
          size ++;
          console.log("size after size ++ is " + size)
      }

      if (size == photoObjects.length){
        console.log("----------size == photoObjects.length----------")
        callbackMulti(data);
      }
      
	  if(photoObjects.length >= ix){

        //console.log('check_iX'+ix);

         setTimeout(function() {  imageAjaxRequest(photoObjects[ix],callback, failCallback,pdfDocument, picWidth, newPage, ix, x, y );ix++; }, 1000);
        
    	
		//true b/c is arrayBuffer
		}
    };
    var failCallback = function(url) {

      isCallFailed = true;
      failCallbackMulti(url);
    };
    imageAjaxRequest(photoObjects[0], callback, failCallback, pdfDocument, picWidth, newPage, 0, x, y ); //true b/c is arrayBuffer

};
//Show only GlasType IGU case Not lami
shouldDisplayIguGlassType = function(fieldVal){
	return fieldVal =='igu_door_lite' ||  fieldVal =='igu_spandrel' || fieldVal =='igu_patio_door_or_panel' || fieldVal =='bullet_proof_igu' || fieldVal =='curved_igu';
}
//Show only GlasType IGU case lami
shouldDisplayIguLamiGlassType = function(fieldVal){
	return fieldVal =='igu_lami_temp_lami' || fieldVal =='igu_sky_lite_temp_or_lami';
}
