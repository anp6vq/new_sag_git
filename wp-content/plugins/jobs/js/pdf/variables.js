//---------------------------------------------------------------------
//----------------------PRINT PDF VARIABLES----------------------------
//---------------------------------------------------------------------

var MARGIN_TOP_BOTTOM  = 25;
var PAGE_COUNTER  = 2;
var MARGIN_LEFT_RIGHT  = 15;
var JOB_INFO_MARGIN    = 30;
var col2Q            = (650 - (MARGIN_LEFT_RIGHT * 2) ) / 2; // half pdf size
var col1Q            = col2Q / 2;
var col3Q            = col1Q + col2Q;
var horSectSpacing   = 10;
var vertSectSpacing  = 20 ;
var bodyFontSize     = 11;
var smallFontSize    = 9;
var smallerFontSize  = 7;
var h1FontSize       = 13;
var h2FontSize       = 12;
var primaryColor     = "#f7281b";
var lightGrayColor     = "#bebebe";
var bodyColor        = "#000000";
var secondaryColor   = "#07547E";
//----Status Colors -----
var infoColor        = '#E72E02';
var adjustColor      = '#E77B02';
var replaceColor     = '#02A644';
var naColor          = '#C6C6C6';
var verifiedColor 	 = '#00FF00';
var verifyonorderColor 	 = '#FFA500';
var blocksizeColor 	 = '#0000FF';
var JOB_PDF_IMAGES = []; // array to store object of all images and sketches for a project.
var pdfmarkerLocation= [];
var doc = new PDFDocument({
		autoFirstPage:false,
		bufferPages:true,
		margins: {
		top    : MARGIN_TOP_BOTTOM,
		bottom : MARGIN_TOP_BOTTOM,
		left   : MARGIN_LEFT_RIGHT,
		right  : MARGIN_LEFT_RIGHT
  }
});
doc.lineGap(1);
var pageNumber = 0;
var loci = 0;
    doc.on('pageAdded', () => {
        pageNumber++;
        var bottom = doc.page.margins.bottom;
        doc.page.margins.bottom = 0;
		doc.fontSize(smallFontSize);
        doc.text('Page: '+pageNumber,
            15,
            doc.page.height - 20,
            {
                
                align: 'left',
                lineBreak: false,
            });
		pdfmarkerLocation[loci++] = { 
  'xpos': doc.x,
  'ypos': doc.y };
		
        // Reset text writer position
        doc.text('', MARGIN_LEFT_RIGHT, MARGIN_LEFT_RIGHT);
        doc.page.margins.bottom = bottom;
    });
doc.addPage();
// var oReq = new XMLHttpRequest();
// oReq.open("GET", "fonts/OpenSans-Regular.ttf", true);
// oReq.responseType = "arraybuffer";
//
// oReq.onload = function(oEvent) {
//     var arrayBuffer = oReq.response; // Note: not oReq.responseText
//
//     if (arrayBuffer) {
//         PdfExporter.doc.registerFont('OpenSans', arrayBuffer)
//     }
// };
//
// var oReq = new XMLHttpRequest();
// oReq.open("GET", "fonts/OpenSans-Semibold.ttf", true);
// oReq.responseType = "arraybuffer";
//
// oReq.onload = function(oEvent) {
//     var arrayBuffer = oReq.response; // Note: not oReq.responseText
//
//     if (arrayBuffer) {
//         PdfExporter.doc.registerFont('OpenSansSemibold', arrayBuffer)
//     }
// };
//
// oReq.send(null);
//
// doc.font('OpenSans');

//doc.font('MainFont'); //set font to Open Sans for all pdf documents;
var CURRENT_PDF_STREAM = doc.pipe(blobStream());

var PDF_FRAME_CONTENT = jQuery('#frame_content')[0];


//photos are stored on amazon s3 to use in PDF otherwise photos don't work in PDF
var DOOR_REPAIR_PHOTOS = {
  gdr : {
    a : {
      single : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_a_single.jpg",
      pair   : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_a_pair.jpg",
      single_right: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/A-Stile-Single-Door-Opp.jpg"
    },
    bp : {
      single : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_bp_single.jpg",
      pair   : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_bp_pair.jpg",
      single_right: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/BP-Stile-Single-Door-Opp.jpg"
    },
    f : {
      single : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_f_single.jpg",
      pair   : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_f_pair.jpg",
      single_right: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/F-Stile-Single-Door-Opp.jpg"

    },
    p : {
      single : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_p_single.jpg",
      pair   : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_p_pair.jpg",
      single_right: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/P-Stile-Single-Door-Opp.jpg"

    },
    wp : {
      single : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_wp_single.jpg",
      pair   : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/type_wp_pair.jpg",
      single_right: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass/P-Stile-Single-Door-Opp.jpg"
    }
  },

  gdn: {
    a : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass_replacement/type_a_single.jpg",
    f: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass_replacement/type_f_single.jpg",
    bp: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass_replacement/type_bp_single.jpg",
    p : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass_replacement/type_p_single.jpg",
    wp: "https://s3.amazonaws.com/sag-jobs-photos/door_repair/glass_replacement/type_wp_single.jpg"
  },
  sfd : {
    center_hung      : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/type_center_hung_single.jpg",
    center_hung_opp  : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/SFDoor---Center-Hung-opp.jpg", 
    center_hung_pair : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/SfDoor---Center-Hung-Pair.jpg", 

    offset_pivots    : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/type_offset_pivots_single.jpg",
    offset_pivots_opp    : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/SFDoor---Offset-Pivot-opp.jpg",
    offset_pivots_pair    : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/SFDoor---Offset-Pivot-Pair.jpg",

    continuous_hinge : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/type_continuous_hinge_single.jpg",
    continuous_hinge_opp : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/SFDoor---Continuous-Hinge-opp.jpg",
    continuous_hinge_pair : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/aluminum_storefront/SFDoor---Continuous-Hinge-Pair.jpg"
  },
  wmd : {
    continuous_hinge : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/hollow_metal/type_continuous_hinges_single.jpg",
    continuous_hinge_opp : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/hollow_metal/HMD.Door---Panic-opp-(Cont.jpg",
    continuous_hinge_pair : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/hollow_metal/HMD.Door---Panic-Pair-(cont.-Hinge).jpg",

    butt_hinge       : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/hollow_metal/type_butt_hinges_single.jpg",
    butt_hinge_opp       : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/hollow_metal/HMD.Door---Panic-opp-(Offet-Pivot).jpg",
    butt_hinge_pair       : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/hollow_metal/HMD.Door---Panic-Pair-(Offet-Pivot).jpg"
},

sdbg : {

    left_2_hing : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/shower/Shower-Door--(left)---2-Hinges.jpg",
    right_2_hing : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/shower/Shower-Door-(Right)---2-Hinges.jpg",
    left_3_hing : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/shower/Shewer-Door-(Left)--3-Hinges.jpg",
    right_3_hing : "https://s3.amazonaws.com/sag-jobs-photos/door_repair/shower/Shower-Door-(Right)---3-Hinges.jpg"
    
    }



}

var DOOR_HANDLE_PHOTOS = {
  pull_handle_up_push_bar_3_holes   : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-1.jpg",
  pull_handle_down_push_bar_3_holes : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-2.jpg",
  pull_handle_2_holes               : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-3.jpg",
  pull_bar_2_holes                  : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-4.jpg",
  ladder_pull_3_holes               : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-5.jpg",
  ladder_pull_2_holes               : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-6.jpg",
  single_pull_1_hole                : "https://s3.amazonaws.com/sag-jobs-photos/handles/Handle-7.jpg",
   pull_handle_up_push_bar_3_holes_opp   : "https://s3.amazonaws.com/sag-website-assets/Handle-1-opp.jpg",
  pull_handle_down_push_bar_3_holes_opp : "https://s3.amazonaws.com/sag-website-assets/Handle-2-opp.jpg",
  pull_handle_2_holes_opp               : "https://s3.amazonaws.com/sag-website-assets/Handle-3-opp.jpg",
  pull_bar_2_holes_opp                  : "https://s3.amazonaws.com/sag-website-assets/Handle-4.jpg",
  ladder_pull_3_holes_opp               : "https://s3.amazonaws.com/sag-website-assets/Handle-5-opp.jpg",
  ladder_pull_2_holes_opp               : "https://s3.amazonaws.com/sag-website-assets/Handle-6-opp.jpg",
  single_pull_1_hole_opp                : "https://s3.amazonaws.com/sag-website-assets/Handle-7-opp.jpg"
}


var DOOR_NEW_PHOTOS = {
 "Gdoor" : {
    "Agndsing"                             : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/S-1.jpg",
    "Single Door w/ Sidelite Left"         : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/S-2.jpg",
    "Single Door w/ Sidelite Right"        : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/S-3.jpg",
    "Single Door w/ Sidelite Left & Right" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/S-4.jpg",
    "Agndpar"                              : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/DD-1.jpg",
    "Pair Doors w/ Sidelite Left"          : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/DD-2.jpg",
    "Pair Doors w/ Sidelite Right"         : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/DD-3.jpg",
    "Pair Doors w/ Sidelite Left & Right"  : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/DD-4.jpg"
  },
  "Aludoor" : {
    "Aludnfsig" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/aluminum/aluminumDoorFrameTransomSingle.jpg",
    "Aludnfpar" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/aluminum/aluminumDoorFrameTransomPair.jpg",
    "Alufsig"   : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/aluminum/aluminumDoorFrameSingle.jpg",
    "Alufpar"   : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/aluminum/aluminumDoorFramePair.jpg",
    "Aluosig"   : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/aluminum/aluminumDoorSingleReplacement.jpg",
    "Aluopar"   : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/aluminum/aluminumDoorPairReplacement.jpg"
  },
  "hmdoor" : {
    "Hmdndsig" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/hollow-metal/hollowMetalDoorSingle.jpg",
    "hmdndpar" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/hollow-metal/hollowMetalDoorPair.jpg",
    "hmdnfsig" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/hollow-metal/hollowMetalFrameDoorSingle.jpg",
    "hmdnfpar" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/hollow-metal/hollowMetalFrameDoorPair.jpg"
  },
  "patio" : {
    "ox" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/OX-Slider.png",
    "xo" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/XO-Slider.jpg",
    "oxxo" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/OXXO-Slider.jpg",
    "oxo_sr" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/OXO-Right-Slide.jpg",
    "oxo_sl" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/oxo_left_slide.jpg",
    "xoo" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/XOO-Slider.jpg",
    "oox" : "https://s3.amazonaws.com/sag-jobs-photos/new-doors/patio/OOX-Slider.jpg"
  }
}

var DOOR_NEW_LABELS = {
  "Gdoor" : {
     "Agndsing"                             : "Single Door",
     "Single Door w/ Sidelite Left"         : "Single Door Sidelite Left",
     "Single Door w/ Sidelite Right"        : "Single Door Sidelite Right",
     "Single Door w/ Sidelite Left & Right" : "Single Door Sidelite Left & Right",
     "Agndpar"                              : "Pair Doors",
     "Pair Doors w/ Sidelite Left"          : "Pair Doors Sidelite Left",
     "Pair Doors w/ Sidelite Right"         : "Pair Doors Sidelite Right",
     "Pair Doors w/ Sidelite Left & Right"  : "Pair Doors Sidelite Left & Right"
   },
   "Aludoor" : {
     "Aludnfsig" : "Doors & Frame Transom Single",
     "Aludnfpar" : "Doors & Frame Transom Pair",
     "Alufsig"   : "Doors & Frame Single",
     "Alufpar"   : "Doors & Frame Pair",
     "Aluosig"   : "Door Only Single ",
     "Aluopar"   : "Door Only Pair"
   },
   "hmdoor" : {
     "Hmdndsig" : "Door Only Single",
     "hmdndpar" : "Door Only Pair",
     "hmdnfsig" : "Door Only Frame Single",
     "hmdnfpar" : "Door Only Frame Pair"
   },

   "patio" : {
     "ox" : "OX",
     "xo" : "XO",
     "oxo_sr" : "OXO (SLIDE RIGHT)",
     "oxo_sl" : "OXO (SLIDE LEFT)",
     "xoo" : "XOO",
     "oox" : "OOX",
     "oxxo" : "OXXO"
   }
}