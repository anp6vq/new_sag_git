
var nwaluof_item;

addnwaluofItemsToPdf = function(callback){

  console.log("inside addnwaluofToPdf");

  console.log('job_item["nwdr_items"]["nwaluof"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["nwaluof"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.y+=20;
  doc.text('Aluminum Door Only - New Door', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
 /* doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var nwaluofLength = job_item["nwdr_items"]["nwaluof"].length; // must be > 0
  var nwaluofIndex  = 0;
  nwaluof_item = job_item["nwdr_items"]["nwaluof"][nwaluofIndex];
  console.log("--------------------------------------------");
  console.log("nwaluof ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwaluof"][nwaluofIndex]));
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(nwaluof_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside nwaluof.length loop and ' + " i is " + nwaluofIndex);
  var curPiece = nwaluofIndex + 1 ;
  doc.fillColor(primaryColor);
  doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwaluof_item.category ][ nwaluof_item.type ] + " ",doc.x,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

 nwaluofPhotoFailCallback = function(){
    console.log("failed to grab nwdr Door Diagram");
  }

  nwaluofPhotoCallback = function(){
	   nwaluofIndex += 1;
	    doc.fontSize(smallFontSize);
     doc.text(' ', 200,120, {align : 'left'});
     if(shouldDisplay(nwaluof_item.opening_width)){
       doc.text('A - Door Opening Width');
     }
     if(shouldDisplay(nwaluof_item.opening_height)){
       doc.text('B - Door Opening Height');
     }
     if(shouldDisplay(nwaluof_item.door_hand)){
       doc.text('E - Door Hand');
     }
	 
	 if(shouldDisplay(nwaluof_item.offset_arm)){
       doc.text('E1 - Offset Arm');
     }
	 
     if(shouldDisplay(nwaluof_item.door_width)){
       doc.text('F - Door Width');
     }
     if(shouldDisplay(nwaluof_item.door_height)){
       doc.text('G - Door Height');
     }
     if(shouldDisplay(nwaluof_item.door_style)){
       doc.text('H - Door Style');
     }
     if(shouldDisplay(nwaluof_item.type_of_glass)){
       doc.text('I - Type of Glass');
     }
     if(shouldDisplay(nwaluof_item.freme_finish)){
       doc.text('K - Finish');
     }
     if(shouldDisplay(nwaluof_item.hanging_hardware)){
       doc.text('L - Hanging Hardware');
     }
     if(shouldDisplay(nwaluof_item.center_intermediate)){
       doc.text('M - TOD to CL of intermediate');
     }
     if(shouldDisplay(nwaluof_item.door_closer)){
       doc.text('O - Door Closer');
     }
     if(shouldDisplay(nwaluof_item.locks)){
       doc.text('P - Lock');
     }
     if(shouldDisplay(nwaluof_item.panic_device)){
       doc.text('Q - Panic Device');
     }
     if(shouldDisplay(nwaluof_item.center_lock)){
       doc.text('R - TOD to CL of lock/panic');
     }
     if(shouldDisplay(nwaluof_item.push_pull)){
       doc.text('S - Push Pull');
     }
     if(shouldDisplay(nwaluof_item.center_pull)){
       doc.text('T - TOD to CL of top pull');
     }
     if(shouldDisplay(nwaluof_item.mid_panel)){
       doc.text('U - Mid Panel');
     }
     if(shouldDisplay(nwaluof_item.center_panel)){
       doc.text('V - TOD to CL of mid panel');
     }
     if(shouldDisplay(nwaluof_item.bottom_rail)){
       doc.text('W - Bottom Rail');
     }
      if(shouldDisplay(nwaluof_item.door_sweep)){
       doc.text('X - Door Sweeps');
     }
     if(shouldDisplay(nwaluof_item.notes)){
       addLines(2, doc);
       doc.text('Notes');
     }
     doc.text(' ', 350,140, {align : 'left'});
     if(shouldDisplay(nwaluof_item.opening_width)){
       nwaluof_item.opening_width = nwaluof_item.opening_width.replace(/-/g, " ");
      var txt = nwaluof_item.opening_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluof_item.opening_height)){
       nwaluof_item.opening_height = nwaluof_item.opening_height.replace(/-/g, " ");
       var txt = nwaluof_item.opening_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
      if(shouldDisplay(nwaluof_item.door_hand)){
        nwaluof_item.door_hand = nwaluof_item.door_hand.replace(/-/g, " ");
        doc.text(nwaluof_item.door_hand);
      }
	  
	 if(shouldDisplay(nwaluof_item.offset_arm)){
        nwaluof_item.offset_arm = nwaluof_item.offset_arm.replace(/-/g, " ");
        doc.text(nwaluof_item.offset_arm);
     }

	  
     if(shouldDisplay(nwaluof_item.door_width)){
       nwaluof_item.door_width = nwaluof_item.door_width.replace(/-/g, " ");
       var txt = nwaluof_item.door_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluof_item.door_height)){
       nwaluof_item.door_height = nwaluof_item.door_height.replace(/-/g, " ");
       var txt = nwaluof_item.door_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluof_item.door_style)){
       nwaluof_item.door_style = nwaluof_item.door_style.replace(/-/g, " ");
       doc.text(nwaluof_item.door_style);
     }
     if(shouldDisplay(nwaluof_item.type_of_glass)){
       nwaluof_item.type_of_glass = nwaluof_item.type_of_glass.replace(/-/g, " ");
       doc.text(nwaluof_item.type_of_glass);
     }

     if(shouldDisplay(nwaluof_item.freme_finish)){
       nwaluof_item.freme_finish = nwaluof_item.freme_finish.replace(/-/g, " ");
       doc.text(nwaluof_item.freme_finish);
     }
     if(shouldDisplay(nwaluof_item.hanging_hardware)){
       nwaluof_item.hanging_hardware = nwaluof_item.hanging_hardware.replace(/-/g, " ");
       doc.text(nwaluof_item.hanging_hardware);
     }

      if(shouldDisplay(nwaluof_item.center_intermediate)){
        nwaluof_item.center_intermediate = nwaluof_item.center_intermediate.replace(/-/g, " ");
         var txt =  nwaluof_item.center_intermediate ;
      var x ='"';
      x = txt+x;
      doc.text(x);
      }
     if(shouldDisplay(nwaluof_item.door_closer)){
       nwaluof_item.door_closer = nwaluof_item.door_closer.replace(/-/g, " ");
       doc.text(nwaluof_item.door_closer);
     }
     if(shouldDisplay(nwaluof_item.locks)){
       nwaluof_item.locks = nwaluof_item.locks.replace(/-/g, " ");
       doc.text(nwaluof_item.locks);
     }



          //   `center_panel` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
          //   `bottom_rail` text COLLATE utf8mb4_unicode_ci,
          //   `notes` text COLLATE utf8mb4_unicode_ci,
          //   `pictures_download_url` text COLLATE utf8mb4_unicode_ci,
          //   `sketches_download_url` text COLLATE utf8mb4_unicode_ci
     if(shouldDisplay(nwaluof_item.panic_device)){
       nwaluof_item.panic_device = nwaluof_item.panic_device.replace(/-/g, " ");
       doc.text(nwaluof_item.panic_device);
     }
     if(shouldDisplay(nwaluof_item.center_lock)){
       nwaluof_item.center_lock = nwaluof_item.center_lock.replace(/-/g, " ");
      var txt = nwaluof_item.center_lock;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluof_item.push_pull)){
       nwaluof_item.push_pull = nwaluof_item.push_pull.replace(/-/g, " ");
       doc.text(nwaluof_item.push_pull);
     }
     if(shouldDisplay(nwaluof_item.center_pull)){
       nwaluof_item.center_pull = nwaluof_item.center_pull.replace(/-/g, " ");
        var txt = nwaluof_item.center_pull;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluof_item.mid_panel)){
       nwaluof_item.mid_panel = nwaluof_item.mid_panel.replace(/-/g, " ");
       doc.text(nwaluof_item.mid_panel);
     }
     if(shouldDisplay(nwaluof_item.center_panel)){
       nwaluof_item.center_panel = nwaluof_item.center_panel.replace(/-/g, " ");
       var txt = nwaluof_item.center_panel;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluof_item.bottom_rail)){
       nwaluof_item.bottom_rail = nwaluof_item.bottom_rail.replace(/-/g, " ");
       doc.text(nwaluof_item.bottom_rail);
     }

     if(shouldDisplay(nwaluof_item.door_sweep)){
       nwaluof_item.door_sweep = nwaluof_item.door_sweep.replace(/-/g, " ");
       doc.text(nwaluof_item.door_sweep);
     }

     if(shouldDisplay(nwaluof_item.notes)){
       addLines(2, doc);
       nwaluof_item.notes = nwaluof_item.notes.replace(/-/g, " ");
       doc.text(nwaluof_item.notes);
     }

     materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              //console.log('materialsXStart',materialsXStart);
              //console.log('materialsYStart',materialsYStart);

              doc.text('GLASS REMINDERS:',materialsXStart-145 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
             
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('nwaluof_item',nwaluof_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(nwaluof_item.solar_film)){
               doc.y+=12;
             

              console.log('doc.x+=195; ',doc.x+=195 );
              doc.text('Solar Film:',);
              doc.y-=12;
              doc.x+=155; 
              doc.text(titleCase(boolToHuman(nwaluof_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(nwaluof_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              nwaluof_item.solar_film_responsibility=nwaluof_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(nwaluof_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(nwaluof_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              nwaluof_item.solar_film_type=nwaluof_item.solar_film_type.replace(/\\/g, '');
              nwaluof_item.solar_film_type=nwaluof_item.solar_film_type.replace(/'/g, '');
              nwaluof_item.solar_film_type=nwaluof_item.solar_film_type.replace(/\//g, "");
              nwaluof_item.solar_film_type=nwaluof_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(nwaluof_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(nwaluof_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              nwaluof_item.solar_film_source=nwaluof_item.solar_film_source.replace(/\\/g, '');
              nwaluof_item.solar_film_source=nwaluof_item.solar_film_source.replace(/'/g, '');
              nwaluof_item.solar_film_source=nwaluof_item.solar_film_source.replace(/\//g, "");
              nwaluof_item.solar_film_source=nwaluof_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(nwaluof_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(nwaluof_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              nwaluof_item.wet_seal=nwaluof_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(nwaluof_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(nwaluof_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              nwaluof_item.wet_seal_responsibility=nwaluof_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(nwaluof_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(nwaluof_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluof_item.furniture_to_move=nwaluof_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(nwaluof_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(nwaluof_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluof_item.furniture_to_move_comment=nwaluof_item.furniture_to_move_comment.replace(/_/g, " ");
                  nwaluof_item.furniture_to_move_comment=nwaluof_item.furniture_to_move_comment.replace(/\\/g, '');
                  nwaluof_item.furniture_to_move_comment=nwaluof_item.furniture_to_move_comment.replace(/'/g, '');
                  nwaluof_item.furniture_to_move_comment=nwaluof_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(nwaluof_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(nwaluof_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluof_item.walls_or_ceilings_to_cut=nwaluof_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  nwaluof_item.walls_or_ceilings_to_cut=nwaluof_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  nwaluof_item.walls_or_ceilings_to_cut=nwaluof_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  nwaluof_item.walls_or_ceilings_to_cut=nwaluof_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(nwaluof_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(nwaluof_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluof_item.walls_or_ceilings_to_cut_responsibility=nwaluof_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(nwaluof_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(nwaluof_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  nwaluof_item.walls_or_ceilings_to_cut_comment=nwaluof_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  nwaluof_item.walls_or_ceilings_to_cut_comment=nwaluof_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  nwaluof_item.walls_or_ceilings_to_cut_comment=nwaluof_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  nwaluof_item.walls_or_ceilings_to_cut_comment=nwaluof_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(nwaluof_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(nwaluof_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  nwaluof_item.blind_needs_removing=nwaluof_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(nwaluof_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(nwaluof_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  nwaluof_item.glass_fits_elevator=nwaluof_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(nwaluof_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(nwaluof_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwaluof_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(nwaluof_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwaluof_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(nwaluof_item.damage_waiver_text) ||shouldDisplay(nwaluof_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  nwaluof_item.damage_waiver_select = nwaluof_item.damage_waiver_select.replace(/_/g, " ");
                  if(nwaluof_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || nwaluof_item.damage_waiver_select =='Handling of Customers Materials' ||nwaluof_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(nwaluof_item.damage_waiver_select);
                  }else{
                  
                  if(nwaluof_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(nwaluof_item.damage_waiver_select);
                  }
                  doc.text(nwaluof_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(nwaluof_item.disclamers=='1'){
                if(shouldDisplay(nwaluof_item.disclamers)){
                    if(nwaluof_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || nwaluof_item.damage_waiver_select =='Handling of Customers Materials' ||nwaluof_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwaluof_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(nwaluof_item.disclamers_text) || shouldDisplay(nwaluof_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  nwaluof_item.disclamers_select = nwaluof_item.disclamers_select.replace(/_/g, " ");
                  if(nwaluof_item.disclamers_select == 'Wood Bead' || nwaluof_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || nwaluof_item.disclamers_select == 'TBD'){
                      doc.text(nwaluof_item.disclamers_select);
                  }else{
                  if(nwaluof_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(nwaluof_item.disclamers_select);
                  }
                  doc.text(nwaluof_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(nwaluof_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(nwaluof_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(nwaluof_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(nwaluof_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(nwaluof_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(nwaluof_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(nwaluof_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(nwaluof_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(nwaluof_item.tgpd_tgpd_caulk_amount) || shouldDisplay(nwaluof_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(nwaluof_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(nwaluof_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(nwaluof_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(nwaluof_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(nwaluof_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(nwaluof_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(nwaluof_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(nwaluof_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

 if(nwaluofIndex < nwaluofLength){ //there are more nwdr items
      if(nwaluofIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();

      }
      nwaluof_item = job_item["nwdr_items"]["nwaluof"][nwaluofIndex];
      console.log("--------------------------------------------");
      console.log("nwaluof ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwaluof"][nwaluofIndex]) );
      console.log("--------------------------------------------");
      addPhotosFromProjectItem(nwaluof_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = nwaluofIndex + 1 ;
      doc.fillColor(primaryColor);
      doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwaluof_item.category ][ nwaluof_item.type ] + " ", 20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwaluof_item.category][nwaluof_item.type]}],nwaluofPhotoCallback, nwaluofPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }

	  }
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwaluof_item.category][nwaluof_item.type]}],nwaluofPhotoCallback, nwaluofPhotoFailCallback, doc, 150, false); //add logo to header


}

// `id` bigint(20) unsigned NOT NULL,
//   `job_id` int(10) DEFAULT NULL,
//   `category` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `type` varchar(22) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `opening_width` varchar(30) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `opening_height` varchar(70) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `door_hand` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `door_width` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `door_height` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `door_style` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `type_of_glass` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `freme_finish` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `hanging_hardware` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `center_intermediate` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `door_closer` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `locks` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `panic_device` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `center_lock` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `push_pull` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `center_pull` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `mid_panel` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `center_panel` varchar(222) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
//   `bottom_rail` text COLLATE utf8mb4_unicode_ci,
//   `notes` text COLLATE utf8mb4_unicode_ci,
//   `pictures_download_url` text COLLATE utf8mb4_unicode_ci,
//   `sketches_download_url` text COLLATE utf8mb4_unicode_ci