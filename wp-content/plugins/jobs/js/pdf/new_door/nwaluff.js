
var nwaluff_item;

addNwaluffItemsToPdf = function(callback){

  console.log("inside addNwaluffToPdf");

  console.log('job_item["nwdr_items"]["nwaluff"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["nwaluff"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.y+=20;
  doc.text('Aluminum Doors and Frame -  New Door', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
  /*doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var nwaluffLength = job_item["nwdr_items"]["nwaluff"].length; // must be > 0
  var nwaluffIndex  = 0;
  nwaluff_item = job_item["nwdr_items"]["nwaluff"][nwaluffIndex];
  console.log("--------------------------------------------");
  console.log("Nwaluff ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwaluff"][nwaluffIndex]));
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(nwaluff_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside nwaluff.length loop and ' + " i is " + nwaluffIndex);
  var curPiece = nwaluffIndex + 1 ;
  doc.fillColor(primaryColor);
  doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwaluff_item.category ][ nwaluff_item.type ] + " ",doc.x,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

 nwaluffPhotoFailCallback = function(){
    console.log("failed to grab nwdr Door Diagram");
  }

  nwaluffPhotoCallback = function(){
	   nwaluffIndex += 1;
	    doc.fontSize(smallFontSize);
     doc.text(' ', 200,120, {align : 'left'});
     if(shouldDisplay(nwaluff_item.opening_width)){
       doc.text('A - Door Opening Width');
     }
     if(shouldDisplay(nwaluff_item.opening_height)){
       doc.text('B - Door Opening Height');
     }
     if(shouldDisplay(nwaluff_item.door_width)){
       doc.text('C - Rough Opening Width');
     }
     if(shouldDisplay(nwaluff_item.door_height)){
       doc.text('D - Rough Opening Height');
     }
	  if(shouldDisplay(nwaluff_item.door_hand)){
       doc.text('E -  Door Hand');
     }
	 /*11-28-17*/
	  if(shouldDisplay(nwaluff_item.offset_arm)){
       doc.text('E1 -  Offset Arm');
     }
	 /*11-28-17*/
	 
     if(shouldDisplay(nwaluff_item.door_style)){
       doc.text('H - Door Style');
     }
     if(shouldDisplay(nwaluff_item.type_of_glass)){
       doc.text('I - Line');
     }
     if(shouldDisplay(nwaluff_item.frame_system)){
       doc.text('J - Frame System');
     }
     if(shouldDisplay(nwaluff_item.freme_finish)){
       doc.text('K - Finish');
     }
     if(shouldDisplay(nwaluff_item.hanging_hardware)){
       doc.text('L - Hanging Hardware');
     }
     if(shouldDisplay(nwaluff_item.threshould)){
       doc.text('N - Threshold');
     }
     if(shouldDisplay(nwaluff_item.door_closer)){
       doc.text('O - Door Closer');
     }
     if(shouldDisplay(nwaluff_item.lock)){
       doc.text('P - Lock');
     }
     if(shouldDisplay(nwaluff_item.panic_device)){
       doc.text('Q - Panic Device');
     }
     if(shouldDisplay(nwaluff_item.push_pull)){
       doc.text('S - Push Pull');
     }
     if(shouldDisplay(nwaluff_item.mid_panel)){
       doc.text('U - Mid Panel');
     }
     if(shouldDisplay(nwaluff_item.bottom_rail)){
       doc.text('W - Bottom Rail');
     }

     if(shouldDisplay(nwaluff_item.door_sweep)){
       doc.text('X - Door Sweeps');
     }
     if(shouldDisplay(nwaluff_item.notes)){
       addLines(2, doc);
       doc.text('Notes');
     }
     doc.text(' ', 350,120, {align : 'left'});
     if(shouldDisplay(nwaluff_item.opening_width)){
       nwaluff_item.opening_width = nwaluff_item.opening_width.replace(/-/g, " ");
      var txt = nwaluff_item.opening_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluff_item.opening_height)){
       nwaluff_item.opening_height = nwaluff_item.opening_height.replace(/-/g, " ");
        var txt = nwaluff_item.opening_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluff_item.door_width)){
       nwaluff_item.door_width = nwaluff_item.door_width.replace(/-/g, " ");
       var txt = nwaluff_item.door_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluff_item.door_height)){
       nwaluff_item.door_height = nwaluff_item.door_height.replace(/-/g, " ");
        var txt = nwaluff_item.door_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
	 
	 if(shouldDisplay(nwaluff_item.door_hand)){
       nwaluff_item.door_hand = nwaluff_item.door_hand.replace(/-/g, " ");
       doc.text(nwaluff_item.door_hand);
     }
	 
	  /*11-28-17*/
	  if(shouldDisplay(nwaluff_item.offset_arm)){
         nwaluff_item.offset_arm = nwaluff_item.offset_arm.replace(/-/g, " ");
       doc.text(nwaluff_item.offset_arm);
     }
	 /*11-28-17*/
	 
	 
     if(shouldDisplay(nwaluff_item.door_style)){
       nwaluff_item.door_style = nwaluff_item.door_style.replace(/-/g, " ");
       doc.text(nwaluff_item.door_style);
     }
     if(shouldDisplay(nwaluff_item.type_of_glass)){
       nwaluff_item.type_of_glass = nwaluff_item.type_of_glass.replace(/-/g, " ");
       doc.text(nwaluff_item.type_of_glass);
     }
     if(shouldDisplay(nwaluff_item.frame_system)){
       nwaluff_item.frame_system = nwaluff_item.frame_system.replace(/-/g, " ");
       doc.text(nwaluff_item.frame_system);
     }
     if(shouldDisplay(nwaluff_item.freme_finish)){
       nwaluff_item.freme_finish = nwaluff_item.freme_finish.replace(/-/g, " ");
       doc.text(nwaluff_item.freme_finish);
     }
     if(shouldDisplay(nwaluff_item.hanging_hardware)){
       nwaluff_item.hanging_hardware = nwaluff_item.hanging_hardware.replace(/-/g, " ");
       doc.text(nwaluff_item.hanging_hardware);
     }
     if(shouldDisplay(nwaluff_item.threshould)){
       nwaluff_item.threshould = nwaluff_item.threshould.replace(/-/g, " ");
       doc.text(nwaluff_item.threshould);
     }
     if(shouldDisplay(nwaluff_item.door_closer)){
       nwaluff_item.door_closer = nwaluff_item.door_closer.replace(/-/g, " ");
       doc.text(nwaluff_item.door_closer);
     }
     if(shouldDisplay(nwaluff_item.lock)){
       nwaluff_item.lock = nwaluff_item.lock.replace(/-/g, " ");
       doc.text(nwaluff_item.lock);
     }
     if(shouldDisplay(nwaluff_item.panic_device)){
       nwaluff_item.panic_device = nwaluff_item.panic_device.replace(/-/g, " ");
       doc.text(nwaluff_item.panic_device);
     }
     if(shouldDisplay(nwaluff_item.push_pull)){
       nwaluff_item.push_pull = nwaluff_item.push_pull.replace(/-/g, " ");
       doc.text(nwaluff_item.push_pull);
     }
     if(shouldDisplay(nwaluff_item.mid_panel)){
       nwaluff_item.mid_panel = nwaluff_item.mid_panel.replace(/-/g, " ");
       doc.text(nwaluff_item.mid_panel);
     }
     if(shouldDisplay(nwaluff_item.bottom_rail)){
       nwaluff_item.bottom_rail = nwaluff_item.bottom_rail.replace(/-/g, " ");
       doc.text(nwaluff_item.bottom_rail);
     }

     if(shouldDisplay(nwaluff_item.door_sweep)){
       nwaluff_item.door_sweep = nwaluff_item.door_sweep.replace(/-/g, " ");
       doc.text(nwaluff_item.door_sweep);
     }

     if(shouldDisplay(nwaluff_item.notes)){
       addLines(2, doc);
       nwaluff_item.notes = nwaluff_item.notes.replace(/-/g, " ");
       doc.text(nwaluff_item.notes);
     }
     materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              

              doc.text('GLASS REMINDERS:',materialsXStart-145 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
             
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('hmnf_item',hmnf_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(hmnf_item.solar_film)){
               doc.y-=12;
              doc.x+=195; 
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=155; 
              doc.text(titleCase(boolToHuman(hmnf_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(hmnf_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              hmnf_item.solar_film_responsibility=hmnf_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(hmnf_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/\\/g, '');
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/'/g, '');
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/\//g, "");
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(hmnf_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/\\/g, '');
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/'/g, '');
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/\//g, "");
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(hmnf_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              hmnf_item.wet_seal=hmnf_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(hmnf_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              hmnf_item.wet_seal_responsibility=hmnf_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(hmnf_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(hmnf_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.furniture_to_move=hmnf_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnf_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(hmnf_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/_/g, " ");
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/\\/g, '');
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/'/g, '');
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(hmnf_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(hmnf_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(hmnf_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.walls_or_ceilings_to_cut_responsibility=hmnf_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(hmnf_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(hmnf_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.blind_needs_removing=hmnf_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnf_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.glass_fits_elevator=hmnf_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnf_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnf_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(hmnf_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnf_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(hmnf_item.damage_waiver_text) ||shouldDisplay(hmnf_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  hmnf_item.damage_waiver_select = hmnf_item.damage_waiver_select.replace(/_/g, " ");
                  if(hmnf_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || hmnf_item.damage_waiver_select =='Handling of Customers Materials' ||hmnf_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(hmnf_item.damage_waiver_select);
                  }else{
                  
                  if(hmnf_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(hmnf_item.damage_waiver_select);
                  }
                  doc.text(hmnf_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(hmnf_item.disclamers=='1'){
                if(shouldDisplay(hmnf_item.disclamers)){
                    if(hmnf_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || hmnf_item.damage_waiver_select =='Handling of Customers Materials' ||hmnf_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnf_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(hmnf_item.disclamers_text) || shouldDisplay(hmnf_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.disclamers_select = hmnf_item.disclamers_select.replace(/_/g, " ");
                  if(hmnf_item.disclamers_select == 'Wood Bead' || hmnf_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || hmnf_item.disclamers_select == 'TBD'){
                      doc.text(hmnf_item.disclamers_select);
                  }else{
                  if(hmnf_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(hmnf_item.disclamers_select);
                  }
                  doc.text(hmnf_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(hmnf_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x-=155;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(hmnf_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(hmnf_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(hmnf_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(hmnf_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(hmnf_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(hmnf_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(hmnf_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(hmnf_item.tgpd_tgpd_caulk_amount) || shouldDisplay(hmnf_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(hmnf_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(hmnf_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(hmnf_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(hmnf_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(hmnf_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(hmnf_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(hmnf_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(hmnf_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }



 if(nwaluffIndex < nwaluffLength){ //there are more nwdr items
      if(nwaluffIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();

      }
      nwaluff_item = job_item["nwdr_items"]["nwaluff"][nwaluffIndex];
      console.log("--------------------------------------------");
      console.log("Nwaluff ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwaluff"][nwaluffIndex]) );
      console.log("--------------------------------------------");
      addPhotosFromProjectItem(nwaluff_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = nwaluffIndex + 1 ;
      doc.fillColor(primaryColor);
      doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwaluff_item.category ][ nwaluff_item.type ] + " ",20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwaluff_item.category][nwaluff_item.type]}],nwaluffPhotoCallback, nwaluffPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }

	  }
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwaluff_item.category][nwaluff_item.type]}],nwaluffPhotoCallback, nwaluffPhotoFailCallback, doc, 150, false); //add logo to header


}

// 'id' int(11) NOT NULL,
// 'job_id' int(11) DEFAULT NULL,
// 'category' varchar(22) DEFAULT NULL,
// 'type' varchar(22) DEFAULT NULL,
// 'opening_width' int(11) DEFAULT NULL,
// 'opening_height' int(11) DEFAULT NULL,
// 'door_hand' int(11) DEFAULT NULL,
// 'door_width' int(11) DEFAULT NULL,
// 'door_height' int(11) DEFAULT NULL,
// 'door_style' varchar(222) DEFAULT NULL,
// 'type_of_glass' varchar(222) DEFAULT NULL,
// 'freme_finish' varchar(222) DEFAULT NULL,
// 'hanging_hardware' varchar(222) DEFAULT NULL,
// 'door_closer' varchar(222) DEFAULT NULL,
// 'lock' varchar(222) DEFAULT NULL,
// 'panic_device' varchar(222) DEFAULT NULL,
// 'push_pull' varchar(222) DEFAULT NULL,
// 'mid_panel' varchar(222) DEFAULT NULL,
// 'bottom_rail' varchar(222) DEFAULT NULL,
// 'notes' text,
// 'pictures_download_url' text,
// 'sketches_download_url' text
// frame_system
// threshould