
var nwdr_item;
  var nwdrIndex  = 0;
addNwdrItemsToPdf = function(callback){


  console.log('job_item["nwdr_items"]["nwdr"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["nwdr"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.moveDown();
  //doc.moveDown();
  
	  var data = {
          'action': 'update_log_for_pdf',
          'msg': 'New Glass Doors section started',
          'quote_number':job_item.quote_number
         };
           
                console.log("send pdf imageajsx started");
        jQuery.post(ajax_object.ajax_url, data, function (response)
                {
                
        }); 
  doc.text('New Glass Doors ', doc.x, doc.y + vertSectSpacing + 5 , col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
 /* doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var nwdrLength = job_item["nwdr_items"]["nwdr"].length; // must be > 0

  nwdr_item = job_item["nwdr_items"]["nwdr"][nwdrIndex];
  console.log("--------------------------------------------");
  console.log("Nwdr ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwdr"][nwdrIndex]));
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(nwdr_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside nwdr.length loop and ' + " i is " + nwdrIndex);
  var curPiece = nwdrIndex + 1 ;
  doc.fillColor(primaryColor);
  //doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwdr_item.category ][ nwdr_item.type ] + " " ,20,120, {underline: true});
	if([ nwdr_item.type ] == 'Single Door w/ Sidelite Left' ||[ nwdr_item.type ] == 'Single Door w/ Sidelite Right'){
		doc.text("Door " + curPiece + " - " + [ nwdr_item.type ] + " " ,20,100, {underline: true});
	}else{
		doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwdr_item.category ][ nwdr_item.type ] + " " ,20,100, {underline: true});
	}
   doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

 nwdrPhotoFailCallback = function(){
    console.log("failed to grab nwdr Door Diagram");
  }

  nwdrPhotoCallback = function(){
	   nwdrIndex += 1;
     doc.fontSize(smallFontSize);
	 doc.text(' ', 200,100, {align : 'left'});
     if(shouldDisplay(nwdr_item.door_style)){
       doc.text('A - Door Style');
     }
     if(nwdr_item.door_style == 'Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - PAIR' || nwdr_item.door_style =='Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE'){
		addLines(1, doc);	 
	 }
     if(shouldDisplay(nwdr_item.rought_opening_width)){
       doc.text('B - Rough Opening Width');
     }
     if(shouldDisplay(nwdr_item.rought_opening_height)){
       doc.text('B - Rough Opening Height');
     }
     if(shouldDisplay(nwdr_item.door_opening_width)){
       doc.text('C - Door Opening Width');
     }
     if(shouldDisplay(nwdr_item.door_opening_height)){
       doc.text('C - Door Opening Height');
     }

     if(shouldDisplay(nwdr_item.door_size_width)){
       doc.text('D - Door Size Width');
     }
     if(shouldDisplay(nwdr_item.door_size_height)){
       doc.text('D - Door Size Height');
     }
     if(shouldDisplay(nwdr_item.door_hand_right)){
       doc.text('E - Door Hand');
     }
     if(shouldDisplay(nwdr_item.glass_type)){
       doc.text('F - Glass Type');
     }
	

      if(shouldDisplay(nwdr_item.door_style)){
        // ---------- Type A or Type F ---------
        //if(nwdr_item.door_style.indexOf("Type F") || nwdr_item.door_style.indexOf("Type A") ){		
			
        if(nwdr_item.door_style == "Type A (TOP & BOTTOM PATCH) - SINGLE" || nwdr_item.door_style == "Type A (TOP & BOTTOM PATCH) - PAIR" || nwdr_item.door_style == "Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE" || nwdr_item.door_style == "Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - PAIR"  ){
			
          if(shouldDisplay(nwdr_item.top_rail_width)){
            doc.text('G - Top Patch');
          }
          if(shouldDisplay(nwdr_item.bottom_rail_width)){
            doc.text('H - Bottom Patch');
          }
        }
		if(nwdr_item.door_style == "Type BP (BOTTOM RAIL & TOP PATCH) - SINGLE" || nwdr_item.door_style == "Type BP (BOTTOM RAIL & TOP PATCH - PAIR" || nwdr_item.door_style == "Type BP (BOTTOM RAIL & TOP PATCH) - SINGLE" || nwdr_item.door_style == "Type P (TOP & BOTTOM RAILS) -  PAIR" || nwdr_item.door_style == "Type P (TOP & BOTTOM RAILS) - SINGLE"){	
			// --------- TYPE BP or TYPE P ---------
			
			 if(shouldDisplay(nwdr_item.top_rail_width)){
            doc.text('G - Top Rail Size');
          }
		 if(shouldDisplay(nwdr_item.top_rail_type)){
            doc.text('G - Top Rail Type');
          }
         
          if(shouldDisplay(nwdr_item.bottom_rail_width)){
            doc.text('H - Bottom Rail Size');
          }
          if(shouldDisplay(nwdr_item.bottom_rail_type)){
            doc.text('H - Bottom Rail Type');
          }
        }
      }

     if(shouldDisplay(nwdr_item.metal_finsh)){
       doc.text('I - Metal Finish');
     }
     if(shouldDisplay(nwdr_item.closers)){
       doc.text('J - Closers');
     }
     if(shouldDisplay(nwdr_item.looks)){
       doc.text('K - Locks');
     }
     if(shouldDisplay(nwdr_item.pull_hardware)){
       doc.text('L - HANDLES');
     }
     if(shouldDisplay(nwdr_item.header)){
       doc.text('M - Header');
     }
     if(shouldDisplay(nwdr_item.newdoor_threshold)){
       doc.text('N - Threshold');
     }
     if(shouldDisplay(nwdr_item.sidelight_henad_condition )){
       doc.text('O - Sidelight Head Condition');
     }
     if(shouldDisplay(nwdr_item.sidelight_sill_condition )){
       doc.text('P - Sidelight Sill Condition');
     }
     if(shouldDisplay(nwdr_item.sidelites_configruaton)){
       doc.text('Q - Sidelites Configuration');
     }
     if(shouldDisplay(nwdr_item.newdoor_instructions)){
       addLines(2, doc);
       doc.text('Notes');
     }



var handletype;
     if(shouldDisplay(nwdr_item.handle_newdoor) ){
       addLines(6, doc);
	   
       doc.text('Handle Type', 200, 400);
       if(shouldDisplay(nwdr_item.handle_a)){
         doc.text('A - Handle A');
       }
       if(shouldDisplay(nwdr_item.handle_b)){
         doc.text('B - Handle B');
       }
       if(shouldDisplay(nwdr_item.handle_c)){
         doc.text('C - Handle C');
       }
       if(shouldDisplay(nwdr_item.handle_d)){
         doc.text('D - Handle D');
       }
       if(shouldDisplay(nwdr_item.handle_e)){
         doc.text('E - Handle E');
       }
     }
     doc.fontSize(smallFontSize);
     // 'door_style // 'rought_opening_width // 'rought_opening_height
     // 'door_opening_width // 'door_opening_height // 'door_size_width
     // 'door_size_height // 'door_hand_right // 'door_hand_left
     // 'glass_type // 'top_rail_width // 'top_rail_rapered
     // 'top_rail_square // 'bottom_rail_width // 'bottom_rail_height
     // 'bottom_rail_square
     // 'metal_finsh
     // 'closers
     // 'looks
     // 'pull_hardware
     // 'header
     // 'newdoor_threshold
     // 'sidelight_henad_condition
     // 'sidelight_sill_condition
     // 'sidelites_configruaton
     // 'newdoor_instructions text,
     // 'handle_newdoor
     // 'handle_a   // 'handle_b // 'handle_c // 'handle_d // 'handle_e
	 if(nwdr_item.door_style == "Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK)   PAIR" || nwdr_item.door_style == "Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE"){
			doc.text(' ', 350,85	, {align : 'left'});
	 }
	 else{
			doc.text(' ', 350,100, {align : 'left'});
	 }
	 if(nwdr_item.door_style =='Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE'){
		addLines(1, doc);
		doc.y+=3;
	 }
     if(shouldDisplay(nwdr_item.door_style)){
       nwdr_item.door_style = nwdr_item.door_style.replace(/-/g, " ");
       doc.text(nwdr_item.door_style);
     }
	 
     if(shouldDisplay(nwdr_item.rought_opening_width)){
       nwdr_item.rought_opening_width = nwdr_item.rought_opening_width.replace(/-/g, " ");
       //var x = nwdr_item.rought_opening_width;
       var txt = nwdr_item.rought_opening_width;
       var x ='"';
       x = txt+x;
       doc.text(x);

 		
 	}
     if(shouldDisplay(nwdr_item.rought_opening_height)){
       nwdr_item.rought_opening_height = nwdr_item.rought_opening_height.replace(/-/g, " ");
       var txt = nwdr_item.rought_opening_height;
       var x ='"';
       x = txt+x;
       doc.text(x);

     }

     if(shouldDisplay(nwdr_item.door_opening_width)){
       nwdr_item.door_opening_width = nwdr_item.door_opening_width.replace(/-/g, " ");
       var txt = nwdr_item.door_opening_width;
       var x ='"';
       x = txt+x;
       doc.text(x);
     }

     if(shouldDisplay(nwdr_item.door_opening_height)){
       nwdr_item.door_opening_height = nwdr_item.door_opening_height.replace(/-/g, " ");
       var txt = nwdr_item.door_opening_height;
       var x ='"';
       x = txt+x;
       doc.text(x);
     }

     if(shouldDisplay(nwdr_item.door_size_width)){
       nwdr_item.door_size_width = nwdr_item.door_size_width.replace(/-/g, " ");
       var txt = nwdr_item.door_size_width;
       var x ='"';
       x = txt+x;
       doc.text(x);
     }

     if(shouldDisplay(nwdr_item.door_size_height)){
       nwdr_item.door_size_height = nwdr_item.door_size_height.replace(/-/g, " ");
       var txt = nwdr_item.door_size_height;
       var x ='"';
       x = txt+x;
       doc.text(x);
     }

     if(shouldDisplay(nwdr_item.door_hand_right)){
       nwdr_item.door_hand_right = nwdr_item.door_hand_right.replace(/-/g, " ");
       var txt = nwdr_item.door_hand_right;
       var x ='"';
       x = txt+x;
       doc.text(x);
       //doc.text(nwdr_item.door_hand_right);
     }

     if(shouldDisplay(nwdr_item.glass_type)){
       nwdr_item.glass_type = nwdr_item.glass_type.replace(/-/g, " ");
       doc.text(nwdr_item.glass_type);
     }
     if(shouldDisplay(nwdr_item.door_style)){
       // ---------- Type A or Type F ---------
       //if(nwdr_item.door_style.indexOf("Type F")>=0 ||  nwdr_item.door_style.indexOf("Type A") )
	   if(nwdr_item.door_style == "Type A (TOP & BOTTOM PATCH)   PAIR" || nwdr_item.door_style == "Type A (TOP & BOTTOM PATCH)   SINGLE" || nwdr_item.door_style == "Type A (TOP & BOTTOM PATCH) - PAIR" || nwdr_item.door_style == "Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE" || nwdr_item.door_style == "Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - PAIR" || nwdr_item.door_style == "Type F  (TOP/BOTTOM PATCH& Bottom PATCH LOCK)   PAIR" || nwdr_item.door_style == "Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK) - SINGLE" || nwdr_item.door_style == "Type F (TOP/BOTTOM PATCH& Bottom PATCH LOCK)   SINGLE"){
			if(shouldDisplay(nwdr_item.top_rail_width)){
				nwdr_item.top_rail_width = nwdr_item.top_rail_width.replace(/-/g, " ");
				var txt = nwdr_item.top_rail_width;
				var x ='"';
				x = txt+x;
				doc.text(x);
				//doc.text(nwdr_item.top_rail_width);
			}
			if(shouldDisplay(nwdr_item.bottom_rail_width)){
				nwdr_item.bottom_rail_width = nwdr_item.bottom_rail_width.replace(/-/g, " ");
				var txt = nwdr_item.bottom_rail_width;
				var x ='"';
				x = txt+x;
				doc.text(x);
				//doc.text(nwdr_item.bottom_rail_width);
			}
			if(shouldDisplay(nwdr_item.top_rail_type)){
				nwdr_item.top_rail_type = nwdr_item.top_rail_type.replace(/-/g, " ");
				doc.text(nwdr_item.top_rail_type);
			}
       } 

		   		
		 
	   if(nwdr_item.door_style == "Type BP (BOTTOM RAIL & TOP PATCH)   SINGLE" || nwdr_item.door_style == "Type BP (BOTTOM RAIL & TOP PATCH   PAIR" || nwdr_item.door_style == "Type BP (BOTTOM RAIL & TOP PATCH) - SINGLE" || nwdr_item.door_style == "Type P (TOP & BOTTOM RAILS) -  PAIR" || nwdr_item.door_style == "Type P (TOP & BOTTOM RAILS) SINGLE" || nwdr_item.door_style == "Type P (TOP & BOTTOM RAILS) - SINGLE" || nwdr_item.door_style == "Type P (TOP & BOTTOM RAILS)    PAIR"  || nwdr_item.door_style =="Type P (TOP & BOTTOM RAILS)   SINGLE"){
		   // --------- TYPE BP or TYPE P ---------

		  
		if(shouldDisplay(nwdr_item.top_rail_width)){
           nwdr_item.top_rail_width = nwdr_item.top_rail_width.replace(/-/g, " ");
			var txt = nwdr_item.top_rail_width;
			var x ='"';
			x = txt+x;
			doc.text(x);
           //doc.text(nwdr_item.top_rail_width);
         }
		
         if(shouldDisplay(nwdr_item.top_rail_type)){
           nwdr_item.top_rail_type = nwdr_item.top_rail_type.replace(/-/g, " ");
           doc.text(nwdr_item.top_rail_type);
         }
        
         if(shouldDisplay(nwdr_item.bottom_rail_width)){
           nwdr_item.bottom_rail_width = nwdr_item.bottom_rail_width.replace(/-/g, " ");
           var txt = nwdr_item.bottom_rail_width;
			var x ='"';
			x = txt+x;
			doc.text(x);
           //doc.text(nwdr_item.bottom_rail_width);
         }
		 
         if(shouldDisplay(nwdr_item.bottom_rail_type)){
           nwdr_item.bottom_rail_type = nwdr_item.bottom_rail_type.replace(/-/g, " ");
           doc.text(nwdr_item.bottom_rail_type);
         }
       }
     }

     if(shouldDisplay(nwdr_item.metal_finsh)){
       nwdr_item.metal_finsh = nwdr_item.metal_finsh.replace(/-/g, " ");
       doc.text(nwdr_item.metal_finsh);
     }

     if(shouldDisplay(nwdr_item.closers)){
       nwdr_item.closers = nwdr_item.closers.replace(/-/g, " ");
       doc.text(nwdr_item.closers);
     }

     if(shouldDisplay(nwdr_item.looks)){
       nwdr_item.looks = nwdr_item.looks.replace(/-/g, " ");
       doc.text(nwdr_item.looks);
     }

     if(shouldDisplay(nwdr_item.pull_hardware)){
       nwdr_item.pull_hardware = nwdr_item.pull_hardware.replace(/-/g, " ");
       doc.text(nwdr_item.pull_hardware);
     }

     if(shouldDisplay(nwdr_item.header)){
       nwdr_item.header = nwdr_item.header.replace(/-/g, " ");
       doc.text(nwdr_item.header);
     }

     if(shouldDisplay(nwdr_item.newdoor_threshold)){
       nwdr_item.newdoor_threshold = nwdr_item.newdoor_threshold.replace(/-/g, " ");
       doc.text(nwdr_item.newdoor_threshold);
     }

     if(shouldDisplay(nwdr_item.sidelight_henad_condition)){
       nwdr_item.sidelight_henad_condition = nwdr_item.sidelight_henad_condition.replace(/-/g, " ");
       doc.text(nwdr_item.sidelight_henad_condition);
     }
	
     if(shouldDisplay(nwdr_item.sidelight_sill_condition)){
       nwdr_item.sidelight_sill_condition = nwdr_item.sidelight_sill_condition.replace(/-/g, " ");
       doc.text(nwdr_item.sidelight_sill_condition);
     }
	 
     if(shouldDisplay(nwdr_item.sidelites_configruaton)){
       nwdr_item.sidelites_configruaton = nwdr_item.sidelites_configruaton.replace(/-/g, " ");
       doc.text(nwdr_item.sidelites_configruaton);
     }

     if(shouldDisplay(nwdr_item.newdoor_instructions)){
       addLines(2, doc);
       nwdr_item.newdoor_instructions = nwdr_item.newdoor_instructions.replace(/-/g, " ");
       doc.text(nwdr_item.newdoor_instructions);
     }
     continueAfterHandleImage = function(){
       if(shouldDisplay(nwdr_item.handle_newdoor)){
         doc.text(humanize(nwdr_item.handle_newdoor),350,400);
         if(shouldDisplay(nwdr_item.handle_a)){
           nwdr_item.handle_a=nwdr_item.handle_a.replace(/-/g, " ");
			var txt = nwdr_item.handle_a;
			var x ='"';
			x = txt+x;
			doc.text(x);
         }
         if(shouldDisplay(nwdr_item.handle_b)){
           nwdr_item.handle_b=nwdr_item.handle_b.replace(/-/g, " ");
           var txt = nwdr_item.handle_b;
			var x ='"';
			x = txt+x;
			doc.text(x);
         }
         if(shouldDisplay(nwdr_item.handle_c)){
           nwdr_item.handle_c=nwdr_item.handle_c.replace(/-/g, " ");
           var txt = nwdr_item.handle_c;
			var x ='"';
			x = txt+x;
			doc.text(x);
         }
         if(shouldDisplay(nwdr_item.handle_d)){
           nwdr_item.handle_d=nwdr_item.handle_d.replace(/-/g, " ");
            var txt = nwdr_item.handle_d;
			var x ='"';
			x = txt+x;
			doc.text(x);
         }
         if(shouldDisplay(nwdr_item.handle_e)){
           nwdr_item.handle_e=nwdr_item.handle_e.replace(/-/g, " ");
            var txt = nwdr_item.handle_e;
			var x ='"';
			x = txt+x;
			doc.text(x);
         }
       }

       materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              //console.log('materialsXStart',materialsXStart);
              //console.log('materialsYStart',materialsYStart);

              doc.text('GLASS REMINDERS:',materialsXStart-145 , materialsYStart+50, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
             
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('nwdr_item',nwdr_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(nwdr_item.solar_film)){
               doc.y+=12;
              doc.x+=5; 
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=155; 
              doc.text(titleCase(boolToHuman(nwdr_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(nwdr_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              nwdr_item.solar_film_responsibility=nwdr_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(nwdr_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(nwdr_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              nwdr_item.solar_film_type=nwdr_item.solar_film_type.replace(/\\/g, '');
              nwdr_item.solar_film_type=nwdr_item.solar_film_type.replace(/'/g, '');
              nwdr_item.solar_film_type=nwdr_item.solar_film_type.replace(/\//g, "");
              nwdr_item.solar_film_type=nwdr_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(nwdr_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(nwdr_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              nwdr_item.solar_film_source=nwdr_item.solar_film_source.replace(/\\/g, '');
              nwdr_item.solar_film_source=nwdr_item.solar_film_source.replace(/'/g, '');
              nwdr_item.solar_film_source=nwdr_item.solar_film_source.replace(/\//g, "");
              nwdr_item.solar_film_source=nwdr_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(nwdr_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(nwdr_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              nwdr_item.wet_seal=nwdr_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(nwdr_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(nwdr_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              nwdr_item.wet_seal_responsibility=nwdr_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(nwdr_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(nwdr_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwdr_item.furniture_to_move=nwdr_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(nwdr_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(nwdr_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwdr_item.furniture_to_move_comment=nwdr_item.furniture_to_move_comment.replace(/_/g, " ");
                  nwdr_item.furniture_to_move_comment=nwdr_item.furniture_to_move_comment.replace(/\\/g, '');
                  nwdr_item.furniture_to_move_comment=nwdr_item.furniture_to_move_comment.replace(/'/g, '');
                  nwdr_item.furniture_to_move_comment=nwdr_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(nwdr_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(nwdr_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwdr_item.walls_or_ceilings_to_cut=nwdr_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  nwdr_item.walls_or_ceilings_to_cut=nwdr_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  nwdr_item.walls_or_ceilings_to_cut=nwdr_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  nwdr_item.walls_or_ceilings_to_cut=nwdr_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(nwdr_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(nwdr_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwdr_item.walls_or_ceilings_to_cut_responsibility=nwdr_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(nwdr_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(nwdr_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  nwdr_item.walls_or_ceilings_to_cut_comment=nwdr_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  nwdr_item.walls_or_ceilings_to_cut_comment=nwdr_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  nwdr_item.walls_or_ceilings_to_cut_comment=nwdr_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  nwdr_item.walls_or_ceilings_to_cut_comment=nwdr_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(nwdr_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(nwdr_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  nwdr_item.blind_needs_removing=nwdr_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(nwdr_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(nwdr_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  nwdr_item.glass_fits_elevator=nwdr_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(nwdr_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(nwdr_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwdr_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(nwdr_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwdr_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(nwdr_item.damage_waiver_text) ||shouldDisplay(nwdr_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  nwdr_item.damage_waiver_select = nwdr_item.damage_waiver_select.replace(/_/g, " ");
                  if(nwdr_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || nwdr_item.damage_waiver_select =='Handling of Customers Materials' ||nwdr_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(nwdr_item.damage_waiver_select);
                  }else{
                  
                  if(nwdr_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(nwdr_item.damage_waiver_select);
                  }
                  doc.text(nwdr_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(nwdr_item.disclamers=='1'){
                if(shouldDisplay(nwdr_item.disclamers)){
                    if(nwdr_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || nwdr_item.damage_waiver_select =='Handling of Customers Materials' ||nwdr_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwdr_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(nwdr_item.disclamers_text) || shouldDisplay(nwdr_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  nwdr_item.disclamers_select = nwdr_item.disclamers_select.replace(/_/g, " ");
                  if(nwdr_item.disclamers_select == 'Wood Bead' || nwdr_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || nwdr_item.disclamers_select == 'TBD'){
                      doc.text(nwdr_item.disclamers_select);
                  }else{
                  if(nwdr_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(nwdr_item.disclamers_select);
                  }
                  doc.text(nwdr_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(nwdr_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(nwdr_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(nwdr_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(nwdr_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(nwdr_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(nwdr_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(nwdr_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(nwdr_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(nwdr_item.tgpd_tgpd_caulk_amount) || shouldDisplay(nwdr_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(nwdr_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(nwdr_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(nwdr_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(nwdr_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(nwdr_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(nwdr_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(nwdr_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(nwdr_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
       if(nwdrIndex < nwdrLength){ //there are more nwdr items
        if(nwdrIndex != 0 ){ // not last item & not first item - already has new page
          doc.addPage();

        }
        nwdr_item = job_item["nwdr_items"]["nwdr"][nwdrIndex];
        console.log("--------------------------------------------");
        console.log("Nwdr ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwdr"][nwdrIndex]) );
        console.log("--------------------------------------------");
		
        addPhotosFromProjectItem(nwdr_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
		
        var curPiece = nwdrIndex + 1 ;
        doc.fillColor(primaryColor);
        doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwdr_item.category ][ nwdr_item.type ] + " " ,20,80, {underline: true});
        doc.moveDown();
        doc.fillColor(bodyColor);
        imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwdr_item.category][nwdr_item.type]}],nwdrPhotoCallback, nwdrPhotoFailCallback, doc, 150, false); //add logo to header
      } else{
        callback();
      }
     }
     if(shouldDisplay(nwdr_item.handle_newdoor)){
       imagesAjaxRequests([{url: DOOR_HANDLE_PHOTOS[nwdr_item.handle_newdoor]}],continueAfterHandleImage, nwdrPhotoFailCallback, doc, 150, false, MARGIN_LEFT_RIGHT  , 390 ); //add logo to header
     } else{
       continueAfterHandleImage();
     }


	  }
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwdr_item.category][nwdr_item.type]}],nwdrPhotoCallback, nwdrPhotoFailCallback, doc, 150, false); //add logo to header

}
