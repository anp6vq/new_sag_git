
var patio_item;

addnwapatioToPdf = function(callback){

  console.log("inside addpatioToPdf");

  console.log('job_item["nwdr_items"]["patio"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["patio"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.y+=20;
  doc.text('Patio Door Only - New Door', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
var patioLength = job_item["nwdr_items"]["patio"].length; // must be > 0
  var patiofIndex  = 0;
  console.log('patio data',job_item["nwdr_items"]["patio"]);
  patio_item = job_item["nwdr_items"]["patio"][patiofIndex];
  console.log("--------------------------------------------");
  //alert(patiofIndex);
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(patio_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside patio.length loop and ' + " i is " + patio_item);
  var curPiece = patiofIndex + 1 ;
  doc.fillColor(primaryColor);
  doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ patio_item.category ] [ patio_item.type ]+ " ",doc.x,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);
 nwpatiofPhotoFailCallback = function(){
    console.log("failed to grab nwdr patio Door Diagram");
  }
patioPhotoCallback = function(){
  patiofIndex += 1;
    doc.fontSize(smallFontSize);
   doc.text(' ', 200,40, {align : 'left'});
     if(shouldDisplay(patio_item.text_new_patio_measurment_witdh)){
       doc.text('A - MEASUREMENTS : ');
       ////addLines(1, doc);
     }

      if(shouldDisplay(patio_item.text_new_patio_measurment_height)){
       doc.text('B - MEASUREMENTS : ');
       ////addLines(1, doc);
     }

      if(shouldDisplay(patio_item.text_new_patio_other_width)){
       doc.text('DOOR WIDTH (if different from standard) : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_frame_type)){
       doc.text('FRAME TYPE : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_frame_depth)){
       doc.text('FRAMING DEPTH : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_thermally_broken)){
       doc.text('THERMALLY BROKEN : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_thermally_attachement)){
       doc.text('THERMALLY ATTACHEMENT : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_receptor)){
       doc.text('FRAME RECEPTOR : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_assembly)){
       doc.text('ASSEMBLY : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_finish_a)){
       doc.text('FINISH A : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_finish_b)){
       doc.text('FINISH B (if different from A) : ');
      // addLines(1, doc);
     }

    if(shouldDisplay(patio_item.select_new_glass_thickness)){
       doc.text('GLASS THICKNESS : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_glass_color)){
       doc.text('GLASS COLOR : ');
       //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_grid)){
       doc.text('GRIDS : ');
       //addLines(1, doc);
     }

      if(shouldDisplay(patio_item.textarea_hmdnfsig_a_pair_notes)){
       doc.text('Instructions : ');
       //addLines(1, doc);
     }

    if(shouldDisplay(patio_item.select_new_pactio_hardware_color)){
       doc.text('HARDWARE COLOR : ');
       //addLines(2, doc);
     }

      
       //console.log(res);
       
       myString1 = patio_item.new_door_patio_standerd_harware.replace('[','');
       myString2= myString1.replace(']','');
       var res = myString2.split(',');
       //alert(patio_item.new_door_patio_standerd_harware.length);
     if(shouldDisplay(patio_item.new_door_patio_standerd_harware) && res.length > 1 ){ 
       doc.text('STANDARD HARDWARE : ');
       //addLines(1, doc);
     if(patio_item.new_door_patio_standerd_harware.length > 124){
        addLines(5, doc);

       } else {
        addLines(3, doc);

       }
       
     }

   
       doc.text('MATERIALS: ');
       doc.moveDown();
     
    if(shouldDisplay(patio_item.solar_film )){ 
       doc.text('Solar film  : ');
       //doc.moveDown();
     }

     if(shouldDisplay(patio_item.solar_film )){ 
       doc.text('Solar film Responsibility : ');
       //doc.moveDown();
     }

     if(shouldDisplay(patio_item.solar_film_source )){ 
       doc.text('Solar film Source : ');
       //doc.moveDown();
     }

     if(shouldDisplay(patio_item.solar_film_type )){ 
       doc.text('Solar film Type : ');
       //doc.moveDown();
     }  
     // alert(patio_item.wet_seal);
     // alert(shouldDisplay(patio_item.wet_seal ));
      if(shouldDisplay(patio_item.wet_seal )){ 
       doc.text('Wet seal : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.wet_seal_responsibility )){ 
       doc.text('Wet seal Responsibility : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.furniture_to_move )){ 
       doc.text('Furniture to Move : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.furniture_to_move_comment )){ 
       doc.text('Furniture to Move Comment: ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.walls_or_ceilings_to_cut )){ 
       doc.text('Walls/ceilings cut : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.walls_or_ceilings_to_cut_responsibility )){ 
       doc.text('Walls/ceilings cut Responsibility : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.walls_or_ceilings_to_cut_comment )){ 
       doc.text('Walls/ceilings cut Comment : ');
       ////doc.moveDown();
     }

      if(shouldDisplay(patio_item.blind_needs_removing )){ 
       doc.text('Blind needs Removing : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.glass_fits_elevator )){ 
       doc.text('Glass Fits Elevator : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.color_viewer )){ 
       doc.text('GColor Waiver : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.damage_viewer )){ 
       doc.text('Damage Waiver : ');
       ////doc.moveDown();
     }


     if(shouldDisplay(patio_item.damage_waiver_select )){ 
       doc.text('Damage Waiver Type : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.damage_waiver_text )){ 
       doc.text('Damage Waiver Other : ');
       ////doc.moveDown();
     }

    if(shouldDisplay(patio_item.disclamers )){ 
       doc.text('Disclaimers : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.disclamers_select )){ 
       doc.text('Disclaimers Type : ');
       ////doc.moveDown();
     }

     if(shouldDisplay(patio_item.disclamers_text )){ 
       doc.text('Disclaimers Type other : ');
       ////doc.moveDown();
     }

    if(shouldDisplay(patio_item.text_tgpd_text_lift_inside_position )){ 
       doc.text('Lift Inside : ');
       ////doc.moveDown();
     }

    if(shouldDisplay(patio_item.text_tgpd_text_lift_outside_position )){ 
       doc.text('Lift Outside : ');
       ////doc.moveDown();
    }
    myString1_channel = patio_item.tgpd_channel.replace('[','');
    myString2_channel= myString1_channel.replace(']','');
    var res_channel = myString2_channel.split(',');

    myString1_channel_qantity = patio_item.tgpd_quantity_channel.replace('[','');
    myString2_channel_qantity= myString1_channel_qantity.replace(']','');
    var res_channel_qantity = myString2_channel_qantity.split(',');
      

    myString1_caulk = patio_item.tgpd_caulk_type.replace('[','');
    myString2_caulk= myString1_caulk.replace(']','');
    var res_caulk = myString2_caulk.split(',');

    myString1_caulk_qantity = patio_item.tgpd_caulk_amount.replace('[','');
    myString2_caulk_qantity= myString1_caulk_qantity.replace(']','');
    var res_caulk_qantity = myString2_caulk_qantity.split(',');

    myString1_tape = patio_item.tgpd_tape_type.replace('[','');
    myString2_tape= myString1_tape.replace(']','');
    var res_tape = myString2_tape.split(',');

    myString1_tape_amount = patio_item.tgpd_tape_amount.replace('[','');
    myString2_tape_amount= myString1_tape_amount.replace(']','');
    var res_tape_amount = myString2_tape_amount.split(',');

    myString1_scaffolding_type = patio_item.tgpd_scaffolding_type.replace('[','');
    myString2_scaffolding_type= myString1_scaffolding_type.replace(']','');
    var res_scaffolding_type = myString2_scaffolding_type.split(',');

    myString1_quantity_type = patio_item.tgpd_quantity_type.replace('[','');
    myString2_quantity_type= myString1_quantity_type.replace(']','');
    var res_quantity_type = myString2_quantity_type.split(',');


      if((res_channel.length > 0 && res_channel[0] != '' && res_channel[0] != '"not_applicable"' )|| (res_caulk.length > 0 && res_caulk[0] != '' && res_caulk[0] != '"not_applicable"') || (res_tape.length > 0 && res_tape[0] != '' && res_tape[0] != '"not_applicable"') || (res_scaffolding_type.length > 0 && res_scaffolding_type[0] != '' && res_scaffolding_type[0] != '"not_applicable"')) {
      doc.moveDown();

      doc.text('GLASS MATERIALS: ');
       doc.moveDown();
     }
       console.log('res_channel',res_channel);
      // alert('res_channel'+res_channel.length);
      if(res_channel.length > 0 && res_channel[0] != '' && res_channel[0] != '"not_applicable"') {
    if(shouldDisplay(patio_item.tgpd_channel )){ 
       doc.text('Channel : ');
       for(var j=0;j<res_channel.length;j++){
        addLines(1, doc);

       }

       ////doc.moveDown();
    }
  }
           
      // alert('res_caulk'+res_caulk.length);

       console.log('res_caulk',res_caulk);
       if(res_caulk.length > 0 && res_caulk[0] != '""' && res_caulk[0] != '"not_applicable"' ) {
    if(shouldDisplay(patio_item.tgpd_caulk_type )){ 
       doc.text('CAULK : ');
      for(var k=0;k<res_caulk.length;k++){
        addLines(1, doc);
      }
       ////doc.moveDown();
    }
}
           //alert("res_tape "+res_tape.length);
       console.log('res_tape',res_tape);
       if(res_tape.length > 0 && res_tape[0] != '""' && res_tape[0] != '"not_applicable"') {
    if(shouldDisplay(patio_item.tgpd_tape_type )){ 
       doc.text('TAPE : ');
       for(var j=0;j<res_tape.length;j++){
        addLines(1, doc);

       }

       ////doc.moveDown();
    }
       }
    
 //alert("res_scaffolding_type "+res_scaffolding_type.length);
  console.log('res_scaffolding_type',res_scaffolding_type);
 if(res_scaffolding_type.length > 0 && res_scaffolding_type[0] != '""' && res_scaffolding_type[0] != '"not_applicable"') {
    if(shouldDisplay(patio_item.tgpd_scaffolding_type )){ 
       doc.text('EQUIPMENT : ');
       for(var j=0;j<res_scaffolding_type.length;j++){
        addLines(1, doc);
       }
      
    }
}


       //alert('tgpd_channel_length',res_channel);
    //alert(jQuery.type( patio_item.tgpd_quantity_channel ));

     doc.text(' ', 390,40, {align : 'left'});
     if(shouldDisplay(patio_item.text_new_patio_measurment_witdh)){       
      doc.text(patio_item.text_new_patio_measurment_witdh);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.text_new_patio_measurment_height)){       
      doc.text(patio_item.text_new_patio_measurment_height);
      //addLines(1, doc);
     }

      if(shouldDisplay(patio_item.text_new_patio_other_width)){       
      doc.text(patio_item.text_new_patio_other_width);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_frame_type)){       
      doc.text(patio_item.select_new_pactio_frame_type);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_frame_depth)){       
      doc.text(patio_item.select_new_pactio_frame_depth);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_thermally_broken)){       
      doc.text(patio_item.select_new_pactio_thermally_broken);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_thermally_attachement)){       
      doc.text(patio_item.select_new_pactio_thermally_attachement);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_receptor)){       
      doc.text(patio_item.select_new_pactio_receptor);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_assembly)){       
      doc.text(patio_item.select_new_pactio_assembly);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_finish_a)){       
      doc.text(patio_item.select_new_pactio_finish_a);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_finish_b)){       
      doc.text(patio_item.select_new_pactio_finish_b);
      //addLines(1, doc);
     }
       if(shouldDisplay(patio_item.select_new_glass_thickness)){       
      doc.text(patio_item.select_new_glass_thickness);
      //addLines(1, doc);
     }
     if(shouldDisplay(patio_item.select_new_glass_color)){       
      doc.text(patio_item.select_new_glass_color);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_grid)){       
      doc.text(patio_item.select_new_pactio_grid);
      //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.textarea_hmdnfsig_a_pair_notes)){       
      doc.text(patio_item.textarea_hmdnfsig_a_pair_notes);
     //addLines(1, doc);
     }

     if(shouldDisplay(patio_item.select_new_pactio_hardware_color)){       
      doc.text(patio_item.select_new_pactio_hardware_color);
      //addLines(2, doc);
     }
     //console.log('arra_type123',jQuery.type( patio_item.new_door_patio_standerd_harware ));
    //if($.isArray(patio_item.new_door_patio_standerd_harware[0])){
       //console.log('hello123',patio_item.new_door_patio_standerd_harware[0]);
       
       //var res = patio_item.new_door_patio_standerd_harware.split(',');
       //console.log(res);
       
       //myString1 = patio_item.new_door_patio_standerd_harware.replace('[','');
       //myString2= myString1.replace(']','');
       //var res = myString2.split(',');

       console.log('hello123',res.length);

       if(shouldDisplay(patio_item.new_door_patio_standerd_harware) && res.length > 1 ){ 
           doc.text(patio_item.new_door_patio_standerd_harware);

           if(patio_item.new_door_patio_standerd_harware.length > 124){
            addLines(4, doc);

           } else {

            addLines(3, doc);
           }
            
        } else {

          addLines(2, doc);
        }
    //}  
      if(shouldDisplay(patio_item.solar_film )){ 

        if(patio_item.solar_film == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes   ');

         }
           //doc.moveDown();
         }


         if(shouldDisplay(patio_item.solar_film_responsibility )){ 

        
           doc.text(patio_item.solar_film_responsibility);
        
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.solar_film_source )){ 

        
           doc.text(patio_item.solar_film_source);
        
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.solar_film_type )){ 

        
           doc.text(patio_item.solar_film_type);
        
           //doc.moveDown();
         }
     if(shouldDisplay(patio_item.wet_seal )){ 

        if(patio_item.wet_seal == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }
         if(shouldDisplay(patio_item.wet_seal_responsibility )){ 

        
           doc.text(patio_item.wet_seal_responsibility);
        
           //doc.moveDown();
         }

      if(shouldDisplay(patio_item.furniture_to_move )){ 

        if(patio_item.furniture_to_move == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.furniture_to_move_comment )){ 

        
           doc.text(patio_item.furniture_to_move_comment);
        
           //doc.moveDown();
         }

          if(shouldDisplay(patio_item.walls_or_ceilings_to_cut )){ 

        if(patio_item.walls_or_ceilings_to_cut == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.walls_or_ceilings_to_cut_responsibility )){ 

        
           doc.text(patio_item.walls_or_ceilings_to_cut_responsibility);
        
           //doc.moveDown();
         }

          if(shouldDisplay(patio_item.walls_or_ceilings_to_cut_comment )){ 

        
           doc.text(patio_item.walls_or_ceilings_to_cut_comment);
        
           //doc.moveDown();
         }


          if(shouldDisplay(patio_item.blind_needs_removing )){ 

        if(patio_item.blind_needs_removing == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.glass_fits_elevator )){ 

        if(patio_item.glass_fits_elevator == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.color_viewer )){ 

        if(patio_item.color_viewer == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.damage_viewer )){ 

        if(patio_item.color_viewer == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

     if(shouldDisplay(patio_item.damage_waiver_select )){ 

        
           doc.text(patio_item.damage_waiver_select);
        
           //doc.moveDown();
         }    
       if(shouldDisplay(patio_item.damage_waiver_text )){ 

        
           doc.text(patio_item.damage_waiver_text);
        
           //doc.moveDown();
         }     

         if(shouldDisplay(patio_item.disclamers )){ 

        if(patio_item.disclamers == 0){
           doc.text('No   ');
         } else{
             doc.text('Yes  ');

         }
           //doc.moveDown();
         }

         if(shouldDisplay(patio_item.disclamers_select )){ 

        
           //doc.text(patio_item.disclamers_select);

           if(patio_item.disclamers_select == 'select_tgpd_glass_disclamers_for_text'){

             doc.text('other');

           } else {

            doc.text(patio_item.disclamers_select);

           }
        
           //doc.moveDown();
         } 

         if(shouldDisplay(patio_item.disclamers_text )){ 

        
           doc.text(patio_item.disclamers_text);
        
           //doc.moveDown();
         } 

        if(shouldDisplay(patio_item.text_tgpd_text_lift_inside_position )){ 

          if(patio_item.disclamers == 0){
             doc.text('No   ');
           } else{
               doc.text('Yes  ,'+patio_item.tgpd_glass_inside_lift_with_glass_type);

           }
           //doc.moveDown();
         } 

         if(shouldDisplay(patio_item.text_tgpd_text_lift_outside_position )){ 

          if(patio_item.disclamers == 0){
             doc.text('No   ');
           } else {
               doc.text('Yes  ,'+patio_item.tgpd_glass_outside_lift_with_glass_type);

           }
           doc.moveDown();
         } 


      
          //doc.moveDown();
         addLines(2, doc);
       for(i=0;i<res_channel.length;i++){

          if(res_channel[i] != '"not_applicable"'){
             doc.text(res_channel[i].replace(/"/g,'')+'      '+res_channel_qantity[i].replace(/"/g,''));
           }
          //doc.moveDown();
       }
       doc.moveDown();

       myString1_caulk = patio_item.tgpd_caulk_type.replace('[','');
       myString2_caulk= myString1_caulk.replace(']','');
       var res_caulk = myString2_caulk.split(',');

       myString1_caulk_qantity = patio_item.tgpd_caulk_amount.replace('[','');
       myString2_caulk_qantity= myString1_caulk_qantity.replace(']','');
       var res_caulk_qantity = myString2_caulk_qantity.split(',');

      

       for(i=0;i<res_caulk.length;i++){

          if(res_caulk[i] != '"not_applicable"'){
             doc.text(res_caulk[i].replace(/"/g,'')+'      '+res_caulk_qantity[i].replace(/"/g,''));
           }
          //doc.moveDown();
       }
        doc.moveDown();
       for(i=0;i<res_tape.length;i++){

          if(res_tape[i] != '"not_applicable"'){
             doc.text(res_tape[i].replace(/"/g,'')+'      '+res_tape_amount[i].replace(/"/g,''));
           }
          //doc.moveDown();
       }
        doc.moveDown();
        //console.log('ooo',res_quantity_type);
        for(i=0;i<res_scaffolding_type.length;i++){

          if(res_tape[i] != '"not_applicable"'){
             doc.text(res_scaffolding_type[i].replace(/"/g,'')+'      '+res_quantity_type[i].replace(/"/g,''));
           }
          //doc.moveDown();
       }


  if(patiofIndex < patioLength){ //there are more nwdr items
      if(patiofIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();
    }
      patio_item = job_item["nwdr_items"]["patio"][patiofIndex];
      console.log("--------------------------------------------");
      console.log("nwaluof ITEM: " + JSON.stringify(job_item["nwdr_items"]["patio"][patiofIndex]) );
      console.log("--------------------------------------------");
      addPhotosFromProjectItem(patio_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = patiofIndex + 1 ;
      doc.fillColor(primaryColor);
      doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ patio_item.category ][ patio_item.type ] + " ", 20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[patio_item.category][patio_item.type]}],patioPhotoCallback, nwpatiofPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }
    }
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[patio_item.category][patio_item.type]}],patioPhotoCallback, nwpatiofPhotoFailCallback, doc, 150, false); //add logo to header
}


