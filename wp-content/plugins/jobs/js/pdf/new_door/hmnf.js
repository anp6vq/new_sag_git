
var hmnf_item;
addHmnfItemsToPdf = function(callback){
  console.log("inside addHmnfItemsToPdf");
  console.log('job_item["nwdr_items"]["hmnf"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["hmnf"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
    doc.y+=20;
  doc.text('Hollow Metal Door and Frame -  New Door', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
/*  doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var hmnfLength = job_item["nwdr_items"]["hmnf"].length; // must be > 0
  var hmnfIndex  = 0;
  console.log('we are on the ' + hmnfIndex  +  ' job_item["nwdr_items"]["hmnf"]  right now');
  hmnf_item = job_item["nwdr_items"]["hmnf"][hmnfIndex];
  console.log("--------------------------------------------");
  console.log("Hmnf ITEM: " + JSON.stringify(job_item["nwdr_items"]["hmnf"][hmnfIndex]));
  console.log("--------------------------------------------");




 addPhotosFromProjectItem(hmnf_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  var curPiece = hmnfIndex + 1 ;
  doc.fillColor(primaryColor);
  doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ hmnf_item.category ][ hmnf_item.type ] + " ",doc.x,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

 hmnfPhotoFailCallback = function(){
    console.log("failed to grab hmnf Door Diagram");
  }

  hmnfPhotoCallback = function(){
	   hmnfIndex += 1;
	   doc.fontSize(smallFontSize);
     doc.text(' ', 200,120, {align : 'left'});
     if(shouldDisplay(hmnf_item.wall_type)){
       doc.text('A - Wall Type');
     }
     if(shouldDisplay(hmnf_item.opening_width_bootms)){
       doc.text('B - Rough Opening Width - Bottom');
     }
     if(shouldDisplay(hmnf_item.opening_width_middel)){
       doc.text('C - Rough Opening Width - Middle');
     }
     if(shouldDisplay(hmnf_item.opening_width_top)){
       doc.text('D - Rough Opening Width - Top');
     }
     if(shouldDisplay(hmnf_item.opening_height_left)){
       doc.text('E - Rough Opening Height - Left');
     }
     if(shouldDisplay(hmnf_item.opening_height_right)){
       doc.text('F - Rough Opening Height - Right');
     }
     if(shouldDisplay(hmnf_item.depth_or_thickness)){
       doc.text('G - Wall Depth or Thickness');
     }
     if(shouldDisplay(hmnf_item.freme_width) ){
       doc.text('H - Frame Width');
     }
     if(shouldDisplay(hmnf_item.lock_backset) ){
       doc.text('I - Lock Backset');
     }
     if(shouldDisplay(hmnf_item.glass_height) ){
       doc.text('J - Hinge Height');
     }

     if(shouldDisplay(hmnf_item.thickness) ){
       doc.text('K - Door Thickness');
     }
     if(shouldDisplay(hmnf_item.continuous_hinge) ){
       doc.text('L - Continuous Hinge');
     }
     if(shouldDisplay(hmnf_item.door_hand)){
         doc.text('M - Door Hand');
     }
     if(shouldDisplay(hmnf_item.fir_rating)){
         doc.text('N - Fire Rating');
     }
     if(shouldDisplay(hmnf_item.vision_kit)){
         doc.text('O - Vision Kit');
     }

     if(shouldDisplay(hmnf_item.lock_back)){
         doc.text('P - Lockset');
     }

     if(shouldDisplay(hmnf_item.dead_bolt)){
         doc.text('Q - Deadbolt');
     }

     if(shouldDisplay(hmnf_item.panic_divice)){
         doc.text('R - Panic Device');
     }

     if(shouldDisplay(hmnf_item.door_closer)){
         doc.text('S - Door Closer');
     }

     if(shouldDisplay(hmnf_item.pull_set)){
         doc.text('T - Pull Set');
     }
     if(shouldDisplay(hmnf_item.latch_guard)){
         doc.text('U - Latch Guard');
     }
     if(shouldDisplay(hmnf_item.kick_plate)){
         doc.text('V - Kick Plate');
     }

     if(shouldDisplay(hmnf_item.door_sweep)){
         doc.text('W - Door Sweep');
     }

     if(shouldDisplay(hmnf_item.drip_cap)){
         doc.text('X - Drip Cap');
     }

     if(shouldDisplay(hmnf_item.door_viewer)){
         doc.text('Y - Door Viewer');
     }
     if(shouldDisplay(hmnf_item.frame_depth)){
         doc.text('Z - Frame Depth');
     }

     if(shouldDisplay(hmnf_item.notes)){
       addLines(2, doc);
       doc.text('Notes');
     }


    // INSERTTTTT PDF VALUESSSSS!!!!!!
      doc.text(' ', 370,120, {align : 'left'});
     if(shouldDisplay(hmnf_item.wall_type)){
       hmnf_item.wall_type = hmnf_item.wall_type.replace(/-/g, " ");
        var txt =  hmnf_item.wall_type;
     // var x ='"';
      x = txt;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.opening_width_bootms)){
       hmnf_item.opening_widtht_bootms = hmnf_item.opening_width_bootms.replace(/-/g, " ");
       var txt =  hmnf_item.opening_widtht_bootms;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.opening_width_middel)){
       hmnf_item.opening_width_middel = hmnf_item.opening_width_middel.replace(/-/g, " ");
        var txt =  hmnf_item.opening_width_middel;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.opening_width_top)){
       hmnf_item.opening_width_top = hmnf_item.opening_width_top.replace(/-/g, " ");
       var txt =  hmnf_item.opening_width_top;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.opening_height_left)){
       hmnf_item.opening_height_left = hmnf_item.opening_height_left.replace(/-/g, " ");
      var txt = hmnf_item.opening_height_left;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.opening_height_right)){
       hmnf_item.opening_height_right = hmnf_item.opening_height_right.replace(/-/g, " ");
       var txt = hmnf_item.opening_height_right;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.depth_or_thickness)){
       hmnf_item.depth_or_thickness = hmnf_item.depth_or_thickness.replace(/-/g, " ");
      var txt = hmnf_item.depth_or_thickness;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.freme_width)){
       hmnf_item.freme_width = hmnf_item.freme_width.replace(/-/g, " ");
        var txt = hmnf_item.freme_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.lock_backset)){
       hmnf_item.lock_backset = hmnf_item.lock_backset.replace(/-/g, " ");
       var txt = hmnf_item.lock_backset;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }

     if(shouldDisplay(hmnf_item.glass_height)){
       hmnf_item.glass_height = hmnf_item.glass_height.replace(/-/g, " ");
       doc.text(hmnf_item.glass_height);
     }
     if(shouldDisplay(hmnf_item.thickness)){
       hmnf_item.thickness = hmnf_item.thickness.replace(/-/g, " ");
       doc.text(hmnf_item.thickness);
     }
     if(shouldDisplay(hmnf_item.continuous_hinge)){
       hmnf_item.continuous_hinge = hmnf_item.continuous_hinge.replace(/-/g, " ");
       doc.text(hmnf_item.continuous_hinge);
     }
     if(shouldDisplay(hmnf_item.door_hand)){
       hmnf_item.door_hand = hmnf_item.door_hand.replace(/-/g, " ");
       doc.text(hmnf_item.door_hand);
     }
     if(shouldDisplay(hmnf_item.fir_rating)){
       hmnf_item.fir_rating = hmnf_item.fir_rating.replace(/-/g, " ");
       doc.text(hmnf_item.fir_rating);
     }
     if(shouldDisplay(hmnf_item.vision_kit)){
       hmnf_item.vision_kit = hmnf_item.vision_kit.replace(/-/g, " ");
       doc.text(hmnf_item.vision_kit);
     }
     if(shouldDisplay(hmnf_item.lock_back)){
       hmnf_item.lock_back = hmnf_item.lock_back.replace(/-/g, " ");
       doc.text(hmnf_item.lock_back);
     }
     if(shouldDisplay(hmnf_item.dead_bolt)){
       hmnf_item.dead_bolt = hmnf_item.dead_bolt.replace(/-/g, " ");
       doc.text(hmnf_item.dead_bolt);
     }
     if(shouldDisplay(hmnf_item.panic_divice)){
       hmnf_item.panic_divice = hmnf_item.panic_divice.replace(/-/g, " ");
       doc.text(hmnf_item.panic_divice);
     }
     if(shouldDisplay(hmnf_item.door_closer)){
       hmnf_item.door_closer = hmnf_item.door_closer.replace(/-/g, " ");
       doc.text(hmnf_item.door_closer);
     }
     if(shouldDisplay(hmnf_item.pull_set)){
       hmnf_item.pull_set = hmnf_item.pull_set.replace(/-/g, " ");
       doc.text(hmnf_item.pull_set);
     }
     if(shouldDisplay(hmnf_item.latch_guard)){
       hmnf_item.latch_guard = hmnf_item.latch_guard.replace(/-/g, " ");
       doc.text(hmnf_item.latch_guard);
     }
     if(shouldDisplay(hmnf_item.kick_plate)){
       hmnf_item.kick_plate = hmnf_item.kick_plate.replace(/-/g, " ");
       doc.text(hmnf_item.kick_plate);
     }
     if(shouldDisplay(hmnf_item.door_sweep)){
       hmnf_item.door_sweep = hmnf_item.door_sweep.replace(/-/g, " ");
       doc.text(hmnf_item.door_sweep);
     }

     if(shouldDisplay(hmnf_item.drip_cap)){
       hmnf_item.drip_cap = hmnf_item.drip_cap.replace(/-/g, " ");
       doc.text(hmnf_item.drip_cap);
     }

     if(shouldDisplay(hmnf_item.door_viewer)){
       hmnf_item.door_viewer = hmnf_item.door_viewer.replace(/-/g, " ");
       doc.text(hmnf_item.door_viewer);
     }

     if(shouldDisplay(hmnf_item.frame_depth)){
       hmnf_item.frame_depth = hmnf_item.frame_depth.replace(/-/g, " ");
       doc.text(hmnf_item.frame_depth);
     }

     if(shouldDisplay(hmnf_item.notes)){
       addLines(2, doc);
       hmnf_item.notes=hmnf_item.notes.replace(/-/g, " ");
       doc.text(hmnf_item.notes);
     }

      materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              

              doc.text('GLASS REMINDERS:',materialsXStart-125 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
             
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('hmnf_item',hmnf_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(hmnf_item.solar_film)){
               doc.y-=12;
              doc.x+=195; 
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=155; 
              doc.text(titleCase(boolToHuman(hmnf_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(hmnf_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              hmnf_item.solar_film_responsibility=hmnf_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(hmnf_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/\\/g, '');
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/'/g, '');
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/\//g, "");
              hmnf_item.solar_film_type=hmnf_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(hmnf_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/\\/g, '');
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/'/g, '');
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/\//g, "");
              hmnf_item.solar_film_source=hmnf_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(hmnf_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              hmnf_item.wet_seal=hmnf_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(hmnf_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(hmnf_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              hmnf_item.wet_seal_responsibility=hmnf_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(hmnf_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(hmnf_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.furniture_to_move=hmnf_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnf_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(hmnf_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/_/g, " ");
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/\\/g, '');
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/'/g, '');
                  hmnf_item.furniture_to_move_comment=hmnf_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(hmnf_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(hmnf_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  hmnf_item.walls_or_ceilings_to_cut=hmnf_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(hmnf_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnf_item.walls_or_ceilings_to_cut_responsibility=hmnf_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(hmnf_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  hmnf_item.walls_or_ceilings_to_cut_comment=hmnf_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(hmnf_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.blind_needs_removing=hmnf_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnf_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.glass_fits_elevator=hmnf_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnf_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(hmnf_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnf_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(hmnf_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnf_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(hmnf_item.damage_waiver_text) ||shouldDisplay(hmnf_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  hmnf_item.damage_waiver_select = hmnf_item.damage_waiver_select.replace(/_/g, " ");
                  if(hmnf_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || hmnf_item.damage_waiver_select =='Handling of Customers Materials' ||hmnf_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(hmnf_item.damage_waiver_select);
                  }else{
                  
                  if(hmnf_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(hmnf_item.damage_waiver_select);
                  }
                  doc.text(hmnf_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(hmnf_item.disclamers=='1'){
                if(shouldDisplay(hmnf_item.disclamers)){
                    if(hmnf_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || hmnf_item.damage_waiver_select =='Handling of Customers Materials' ||hmnf_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnf_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(hmnf_item.disclamers_text) || shouldDisplay(hmnf_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  hmnf_item.disclamers_select = hmnf_item.disclamers_select.replace(/_/g, " ");
                  if(hmnf_item.disclamers_select == 'Wood Bead' || hmnf_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || hmnf_item.disclamers_select == 'TBD'){
                      doc.text(hmnf_item.disclamers_select);
                  }else{
                  if(hmnf_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(hmnf_item.disclamers_select);
                  }
                  doc.text(hmnf_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(hmnf_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(hmnf_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(hmnf_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(hmnf_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(hmnf_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(hmnf_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(hmnf_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(hmnf_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(hmnf_item.tgpd_tgpd_caulk_amount) || shouldDisplay(hmnf_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(hmnf_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(hmnf_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(hmnf_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(hmnf_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(hmnf_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(hmnf_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(hmnf_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(hmnf_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


 if(hmnfIndex < hmnfLength){ //there are more nwdr items
      if(hmnfIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();

      }
      hmnf_item = job_item["nwdr_items"]["hmnf"][hmnfIndex];
      console.log("--------------------------------------------");
      console.log("HmnfIndex ITEM: " + JSON.stringify(job_item["nwdr_items"]["hmnf"][hmnfIndex]) );
      console.log("--------------------------------------------");
      addPhotosFromProjectItem(hmnf_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = hmnfIndex + 1 ;
      doc.fillColor(primaryColor);
      doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ hmnf_item.category ][ hmnf_item.type ] + " " ,20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[hmnf_item.category][hmnf_item.type]}],hmnfPhotoCallback, hmnfPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }

	  }

  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[hmnf_item.category][hmnf_item.type]}],hmnfPhotoCallback, hmnfPhotoFailCallback, doc, 150, false); //add logo to header





  // 'id' int(11) NOT NULL,
  // 'job_id' int(11) DEFAULT NULL,
  // 'category' varchar(222) DEFAULT NULL,
  // 'type' varchar(222) DEFAULT NULL,
  // 'continuous_hinge' varchar(222) DEFAULT NULL,
  // 'depth_or_thickness' int(11) DEFAULT NULL,
  // 'door_closer' varchar(222) DEFAULT NULL,
  // 'door_hand' varchar(222) DEFAULT NULL,
  // 'fir_rating' varchar(222) DEFAULT NULL,
  // 'freme_width' int(11) DEFAULT NULL,
  // 'glass_height' int(11) DEFAULT NULL,
  // 'kick_plate' int(11) DEFAULT NULL,
  // 'lock_back' varchar(222) DEFAULT NULL,
  // 'lock_backset' varchar(222) DEFAULT NULL,
  // 'opening_height_left' int(11) DEFAULT NULL,
  // 'opening_height_right' int(11) DEFAULT NULL,
  // 'opening_width_bootms' int(11) DEFAULT NULL,
  // 'opening_width_middel' int(11) DEFAULT NULL,
  // 'opening_width_top' int(11) DEFAULT NULL,
  // 'panic_divice' varchar(222) DEFAULT NULL,
  // 'pull_set' varchar(222) DEFAULT NULL,
  // 'thickness' varchar(222) DEFAULT NULL,
  // 'vision_kit' varchar(222) DEFAULT NULL,
  // 'notes' text,
  // 'pictures_download_url' text,
  // 'sketches_download_url' text
  //dead_bolt
  //latch_guard
  //door_sweep
  //drip_cap
  //door_viewer
  //frame_depth
  //wall_type

}