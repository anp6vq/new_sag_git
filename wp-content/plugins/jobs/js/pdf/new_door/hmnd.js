var hmnd_item;

addHmndItemsToPdf = function(callback){
  console.log('job_item["nwdr_items"]["hmnd"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["hmnd"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
    doc.y+=20;
  doc.text('Hollow Metal Doors -  New Door', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
  
 /* doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var hmndLength = job_item["nwdr_items"]["hmnd"].length; // must be > 0
  var hmndIndex  = 0;
  hmnd_item = job_item["nwdr_items"]["hmnd"][hmndIndex];
  console.log("--------------------------------------------");
  console.log("Hmnd_items ITEM: " + JSON.stringify(job_item["nwdr_items"]["hmnd"][hmndIndex]));
  console.log("--------------------------------------------");

 addPhotosFromProjectItem(hmnd_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  var curPiece = hmndIndex + 1 ;
  doc.fillColor(primaryColor);
  doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ hmnd_item.category ][ hmnd_item.type ] + " " ,20,120, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

 hmndPhotoFailCallback = function(){
    console.log("failed to grab hmnd Door Diagram");
  }

  hmndPhotoCallback = function(){
	   hmndIndex += 1;
     doc.fontSize(smallFontSize);
     doc.text(' ', 200,120, {align : 'left'});
    //----------labels---------
    if(shouldDisplay( hmnd_item.width_out_side )) {
      doc.text('A - Door Width (outside side)');
    }
    if(shouldDisplay(hmnd_item.door_height )){
      doc.text('B - Door Height');
    }
    if(shouldDisplay(hmnd_item.the_top_hinge1)){
      doc.text('C - Top door to top hinge #1');
    }
    if(shouldDisplay(hmnd_item.the_top_hinge2)){
      doc.text('D - Top door to top hinge #2');
    }
    if(shouldDisplay(hmnd_item.the_top_hinge3)){
      doc.text('E - TOD to top hinge #3');
    }
    if(shouldDisplay(hmnd_item.the_top_hinge4)){
      doc.text('F - TOD to top hinge #4');
    }
    if(shouldDisplay(hmnd_item.center_deadbolt)){
      doc.text('G - TOD to center line dead bolt');
    }
    if(shouldDisplay(hmnd_item.line_of_lock)){
      doc.text('H - TOD to center line lock');
    }
    if(shouldDisplay(hmnd_item.lock_backset)){
      doc.text('I - Lock Backset');
    }
    if(shouldDisplay(hmnd_item.hinge_height)){
      doc.text('J - Hinge Height');
    }
    if(shouldDisplay(hmnd_item.thinkness)){
      doc.text('K - Door Thickness');
    }
    if(shouldDisplay(hmnd_item.continuous_hinge)){
      doc.text('L - Continuous Hinge');
    }
    if(shouldDisplay(hmnd_item.door_hand)){
      doc.text('M - Door Hand');
    }
    if(shouldDisplay(hmnd_item.fire_rating)){
      doc.text('N - Fire Rating');
    }
    if(shouldDisplay(hmnd_item.vision_kit)){
      doc.text('O - Vision Kit');
    }
    if(shouldDisplay(hmnd_item.single_lockset)){
      doc.text('P - Lockset');
    }
    if(shouldDisplay(hmnd_item.dead_bolt)){
      doc.text('Q - Dead Bolt');
    }

    if(shouldDisplay(hmnd_item.panic_divice)){
      doc.text('R - Panic Device');
    }

    if(shouldDisplay(hmnd_item.door_closer)){
      doc.text('S - Door Closer');
    }
    if(shouldDisplay(hmnd_item.pull_set)){
      doc.text('T - Pull Set');
    }

    if(shouldDisplay(hmnd_item.latch_guard)){
      doc.text('U - Latch Guard');
    }

    if(shouldDisplay(hmnd_item.kick_plate)){
      doc.text('V - Kick Plate');
    }

    if(shouldDisplay(hmnd_item.door_sweep)){
      doc.text('W - Door Sweep');
    }

    if(shouldDisplay(hmnd_item.drip_cap)){
      doc.text('X - Drip Cap');
    }

    if(shouldDisplay(hmnd_item.door_viewer)){
      doc.text('Y - Door Viewer');
    }

    if(shouldDisplay(hmnd_item.notes)){
      addLines(2, doc);
      doc.text('Notes');
    }

    //*******VALUES********

    doc.text(' ', 370,120, {align : 'left'});
    if(shouldDisplay( hmnd_item.width_out_side )) {
      hmnd_item.width_out_side = hmnd_item.width_out_side.replace(/-/g, " ");
      var txt =  hmnd_item.width_out_side;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }
    if(shouldDisplay( hmnd_item.door_height )) {
      hmnd_item.door_height = hmnd_item.door_height.replace(/-/g, " ");
      var txt =  hmnd_item.door_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }
    if(shouldDisplay( hmnd_item.the_top_hinge1 )) {
      hmnd_item.the_top_hinge1 = hmnd_item.the_top_hinge1.replace(/-/g, " ");
      var txt =  hmnd_item.the_top_hinge1;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }
    if(shouldDisplay( hmnd_item.the_top_hinge2 )) {
      hmnd_item.the_top_hinge2 = hmnd_item.the_top_hinge2.replace(/-/g, " ");
      var txt =  hmnd_item.the_top_hinge2;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }
    if(shouldDisplay( hmnd_item.the_top_hinge3 )) {
      hmnd_item.the_top_hinge3 = hmnd_item.the_top_hinge3.replace(/-/g, " ");
      var txt =  hmnd_item.the_top_hinge3 ;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }

    if(shouldDisplay( hmnd_item.the_top_hinge4 )) {
      hmnd_item.the_top_hinge4 = hmnd_item.the_top_hinge4.replace(/-/g, " ");
      var txt =  hmnd_item.the_top_hinge4 ;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }

    if(shouldDisplay( hmnd_item.center_deadbolt )) {
      hmnd_item.center_deadbolt = hmnd_item.center_deadbolt.replace(/-/g, " ");
      doc.text(hmnd_item.center_deadbolt);
    }
    if(shouldDisplay( hmnd_item.line_of_lock )) {
      hmnd_item.line_of_lock = hmnd_item.line_of_lock.replace(/-/g, " ");
      var txt =  hmnd_item.line_of_lock ;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }

    if(shouldDisplay( hmnd_item.lock_backset )) {
      hmnd_item.lock_backset = hmnd_item.lock_backset.replace(/-/g, " ");
      var txt =  hmnd_item.lock_backset ;
      var x ='"';
      x = txt+x;
      doc.text(x);
    }

    if(shouldDisplay( hmnd_item.hinge_height )) {
      hmnd_item.hinge_height = hmnd_item.hinge_height.replace(/-/g, " ");
      doc.text(hmnd_item.hinge_height);
    }

    if(shouldDisplay( hmnd_item.thinkness )) {
      hmnd_item.thinkness = hmnd_item.thinkness.replace(/-/g, " ");
      doc.text(hmnd_item.thinkness);
    }

    if(shouldDisplay( hmnd_item.continuous_hinge )) {
      hmnd_item.continuous_hinge = hmnd_item.continuous_hinge.replace(/-/g, " ");
      doc.text(hmnd_item.continuous_hinge);
    }
    if(shouldDisplay( hmnd_item.door_hand )) {
      hmnd_item.door_hand = hmnd_item.door_hand.replace(/-/g, " ");
      doc.text(hmnd_item.door_hand);
    }
    if(shouldDisplay( hmnd_item.fire_rating )) {
      hmnd_item.fire_rating = hmnd_item.fire_rating.replace(/-/g, " ");
      doc.text(hmnd_item.fire_rating);
    }
    if(shouldDisplay( hmnd_item.vision_kit )) {
      hmnd_item.vision_kit = hmnd_item.vision_kit.replace(/-/g, " ");
      doc.text(hmnd_item.vision_kit);
    }

    if(shouldDisplay( hmnd_item.single_lockset )) {
      hmnd_item.single_lockset = hmnd_item.single_lockset.replace(/-/g, " ");
      doc.text(hmnd_item.single_lockset);
    }

    if(shouldDisplay( hmnd_item.dead_bolt )) {
      hmnd_item.dead_bolt = hmnd_item.dead_bolt.replace(/-/g, " ");
      doc.text(hmnd_item.dead_bolt);
    }

    if(shouldDisplay( hmnd_item.panic_divice )) {
      hmnd_item.panic_divice = hmnd_item.panic_divice.replace(/-/g, " ");
      doc.text(hmnd_item.panic_divice);
    }

    if(shouldDisplay( hmnd_item.door_closer )) {
      hmnd_item.door_closer = hmnd_item.door_closer.replace(/-/g, " ");
      doc.text(hmnd_item.door_closer);
    }
    if(shouldDisplay( hmnd_item.pull_set )) {
      hmnd_item.pull_set = hmnd_item.pull_set.replace(/-/g, " ");
      doc.text(hmnd_item.pull_set);
    }
    if(shouldDisplay( hmnd_item.latch_guard )) {
      hmnd_item.latch_guard = hmnd_item.latch_guard.replace(/-/g, " ");
      doc.text(hmnd_item.latch_guard);
    }
    if(shouldDisplay( hmnd_item.kick_plate )) {
      hmnd_item.kick_plate = hmnd_item.kick_plate.replace(/-/g, " ");
      doc.text(hmnd_item.kick_plate);
    }
    if(shouldDisplay( hmnd_item.door_sweep )) {
      hmnd_item.door_sweep = hmnd_item.door_sweep.replace(/-/g, " ");
      doc.text(hmnd_item.door_sweep);
    }
    if(shouldDisplay( hmnd_item.drip_cap )) {
      hmnd_item.drip_cap = hmnd_item.drip_cap.replace(/-/g, " ");
      doc.text(hmnd_item.drip_cap);
    }

    if(shouldDisplay( hmnd_item.door_viewer )) {
      hmnd_item.door_viewer = hmnd_item.door_viewer.replace(/-/g, " ");
      doc.text(hmnd_item.door_viewer);
    }
    if(shouldDisplay(hmnd_item.notes)){
      addLines(2, doc);
      hmnd_item.notes=hmnd_item.notes.replace(/-/g, " ");
      doc.text(hmnd_item.notes);
    }

     materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              

              doc.text('GLASS REMINDERS:',materialsXStart-145 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
            
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('hmnd_item',hmnd_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(hmnd_item.solar_film)){
              doc.text('Solar Film:');
              doc.y+=12;
              doc.x+=195;
              doc.y-=12;
              doc.x+=150; 
              doc.text(titleCase(boolToHuman(hmnd_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(hmnd_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              hmnd_item.solar_film_responsibility=hmnd_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(hmnd_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(hmnd_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              hmnd_item.solar_film_type=hmnd_item.solar_film_type.replace(/\\/g, '');
              hmnd_item.solar_film_type=hmnd_item.solar_film_type.replace(/'/g, '');
              hmnd_item.solar_film_type=hmnd_item.solar_film_type.replace(/\//g, "");
              hmnd_item.solar_film_type=hmnd_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(hmnd_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(hmnd_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              hmnd_item.solar_film_source=hmnd_item.solar_film_source.replace(/\\/g, '');
              hmnd_item.solar_film_source=hmnd_item.solar_film_source.replace(/'/g, '');
              hmnd_item.solar_film_source=hmnd_item.solar_film_source.replace(/\//g, "");
              hmnd_item.solar_film_source=hmnd_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(hmnd_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(hmnd_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              hmnd_item.wet_seal=hmnd_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(hmnd_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(hmnd_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              hmnd_item.wet_seal_responsibility=hmnd_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(hmnd_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(hmnd_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnd_item.furniture_to_move=hmnd_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnd_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(hmnd_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnd_item.furniture_to_move_comment=hmnd_item.furniture_to_move_comment.replace(/_/g, " ");
                  hmnd_item.furniture_to_move_comment=hmnd_item.furniture_to_move_comment.replace(/\\/g, '');
                  hmnd_item.furniture_to_move_comment=hmnd_item.furniture_to_move_comment.replace(/'/g, '');
                  hmnd_item.furniture_to_move_comment=hmnd_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(hmnd_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(hmnd_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnd_item.walls_or_ceilings_to_cut=hmnd_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  hmnd_item.walls_or_ceilings_to_cut=hmnd_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  hmnd_item.walls_or_ceilings_to_cut=hmnd_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  hmnd_item.walls_or_ceilings_to_cut=hmnd_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(hmnd_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(hmnd_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  hmnd_item.walls_or_ceilings_to_cut_responsibility=hmnd_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(hmnd_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(hmnd_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  hmnd_item.walls_or_ceilings_to_cut_comment=hmnd_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  hmnd_item.walls_or_ceilings_to_cut_comment=hmnd_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  hmnd_item.walls_or_ceilings_to_cut_comment=hmnd_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  hmnd_item.walls_or_ceilings_to_cut_comment=hmnd_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(hmnd_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(hmnd_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  hmnd_item.blind_needs_removing=hmnd_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnd_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(hmnd_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  hmnd_item.glass_fits_elevator=hmnd_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(hmnd_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(hmnd_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnd_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(hmnd_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnd_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(hmnd_item.damage_waiver_text) ||shouldDisplay(hmnd_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  hmnd_item.damage_waiver_select = hmnd_item.damage_waiver_select.replace(/_/g, " ");
                  if(hmnd_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || hmnd_item.damage_waiver_select =='Handling of Customers Materials' ||hmnd_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(hmnd_item.damage_waiver_select);
                  }else{
                  
                  if(hmnd_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(hmnd_item.damage_waiver_select);
                  }
                  doc.text(hmnd_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(hmnd_item.disclamers=='1'){
                if(shouldDisplay(hmnd_item.disclamers)){
                    if(hmnd_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || hmnd_item.damage_waiver_select =='Handling of Customers Materials' ||hmnd_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(hmnd_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(hmnd_item.disclamers_text) || shouldDisplay(hmnd_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  hmnd_item.disclamers_select = hmnd_item.disclamers_select.replace(/_/g, " ");
                  if(hmnd_item.disclamers_select == 'Wood Bead' || hmnd_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || hmnd_item.disclamers_select == 'TBD'){
                      doc.text(hmnd_item.disclamers_select);
                  }else{
                  if(hmnd_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(hmnd_item.disclamers_select);
                  }
                  doc.text(hmnd_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(hmnd_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
               
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(hmnd_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(hmnd_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(hmnd_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(hmnd_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(hmnd_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(hmnd_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(hmnd_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(hmnd_item.tgpd_tgpd_caulk_amount) || shouldDisplay(hmnd_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(hmnd_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(hmnd_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(hmnd_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(hmnd_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(hmnd_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(hmnd_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(hmnd_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(hmnd_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


 if(hmndIndex < hmndLength){ //there are more nwdr items
      if(hmndIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();

      }
      hmnd_item = job_item["nwdr_items"]["hmnd"][hmndIndex];
      addPhotosFromProjectItem(hmnd_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = hmndIndex + 1 ;
      doc.fillColor(primaryColor);
      doc.fontSize(bodyFontSize);
      doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ hmnd_item.category ][ hmnd_item.type ] + " " ,20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[hmnd_item.category][hmnd_item.type]}],hmndPhotoCallback, hmndPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }

	  }

  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[hmnd_item.category][hmnd_item.type]}],hmndPhotoCallback, hmndPhotoFailCallback, doc, 150, false); //add logo to header


  // 'id' int(11)
  // 'job_id'
  // 'category'
  // 'type'
  // 'width_out_side'
  // 'door_height'
  // 'the_top_hinge1'
  // 'the_top_hinge2'
  // 'the_top_hinge3'
  // 'the_top_hinge4'
  // 'line_of_lock'
  // 'lock_backset'
  // 'hinge_height'
  // 'thinkness'
  // 'continuous_hinge'
  // 'door_hand'
  // 'fire_rating'
  // 'vision_kit'
  // 'single_lockset'
  // 'panic_divice'
  // 'door_closer'
  // 'pull_set'
  // 'kick_plate'
  // 'notes' text,
  // 'pictures_download_url' text,
  // 'sketches_download_url' text
  //center_deadbolt
  //dead_bolt
  //latch_guard
  //door_sweep
  //drip_cap
  //door_viewer
}