
var nwaluft_item;

addNwaluftItemsToPdf = function(callback){


  console.log('job_item["nwdr_items"]["nwaluft"]   ' + " is  ------    " + JSON.stringify(job_item["nwdr_items"]["nwaluft"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.y+=20;
  doc.moveDown();
  doc.text('Aluminum Doors & Frame Transom ', doc.x, doc.y + vertSectSpacing + 25, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
  /*doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var nwaluftLength = job_item["nwdr_items"]["nwaluft"].length; // must be > 0
  var nwaluftIndex  = 0;
  nwaluft_item = job_item["nwdr_items"]["nwaluft"][nwaluftIndex];
  console.log("--------------------------------------------");
  console.log("Nwaluft ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwaluft"][nwaluftIndex]));
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(nwaluft_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside nwaluft.length loop and ' + " i is " + nwaluftIndex);
  var curPiece = nwaluftIndex + 1 ;
  doc.fillColor(primaryColor);
  doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwaluft_item.category ][ nwaluft_item.type ] + " " ,doc.x,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

 nwaluftPhotoFailCallback = function(){
    console.log("failed to grab nwdr Door Diagram");
  }

  nwaluftPhotoCallback = function(){
	   nwaluftIndex += 1;
	   doc.fontSize(smallFontSize);
     doc.text(' ', 200,140, {align : 'left'});
     if(shouldDisplay(nwaluft_item.opening_width)){
       doc.text('A - Door Opening Width');
     }
     if(shouldDisplay(nwaluft_item.opening_height)){
       doc.text('B - Door Opening Height');
     }
     if(shouldDisplay(nwaluft_item.rought_opening_width)){
       doc.text('C - Rough Opening Width');
     }
     if(shouldDisplay(nwaluft_item.rought_opening_height)){
       doc.text('D - Rough Opening Height');
     }
     if(shouldDisplay(nwaluft_item.single_door_hand)){
       doc.text('E - Door Hand');
     }
	 
	  if(shouldDisplay(nwaluft_item.offset_arm)){
       doc.text('E1 - Offset Arm');
     }
	 
     if(shouldDisplay(nwaluft_item.door_style)){
       doc.text('H - Door Style');
     }
     if(shouldDisplay(nwaluft_item.type_of_glass)){
       doc.text('I - Type of Glass');
     }
     if(shouldDisplay(nwaluft_item.frame_system)){
       doc.text('J - Frame System');
     }
     if(shouldDisplay(nwaluft_item.frame_finish)){
       doc.text('K - Finish');
     }
     if(shouldDisplay(nwaluft_item.hardware)){
       doc.text('L - Hanging Hardware');
     }
     if(shouldDisplay(nwaluft_item.threshold)){
       doc.text('N - Threshold');
     }
     if(shouldDisplay(nwaluft_item.door_closer)){
       doc.text('O - Door Closer');
     }
     if(shouldDisplay(nwaluft_item.look)){
       doc.text('P - Lock');
     }
     if(shouldDisplay(nwaluft_item.panic_device)){
       doc.text('Q - Panic Device');
     }
     if(shouldDisplay(nwaluft_item.pull)){
       doc.text('S - Push Pull');
     }
     if(shouldDisplay(nwaluft_item.mid_panel)){
       doc.text('U - Mid Panel');
     }
     if(shouldDisplay(nwaluft_item.bottom_rail)){
       doc.text('W - Bottom Rail');
     }

      if(shouldDisplay(nwaluft_item.door_sweep)){
       doc.text('X - Door Sweeps');
     }
     if(shouldDisplay(nwaluft_item.notes)){
       addLines(2, doc);
       doc.text('Notes');
     }

     doc.text(' ', 350,125, {align : 'left'});
     if(shouldDisplay(nwaluft_item.opening_width)){
       nwaluft_item.opening_width = nwaluft_item.opening_width.replace(/-/g, " ");
       var txt = nwaluft_item.opening_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluft_item.opening_height)){
       nwaluft_item.opening_height = nwaluft_item.opening_height.replace(/-/g, " ");
       var txt = nwaluft_item.opening_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluft_item.rought_opening_width)){
       nwaluft_item.rought_opening_width = nwaluft_item.rought_opening_width.replace(/-/g, " ");
       var txt = nwaluft_item.rought_opening_width;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluft_item.rought_opening_height)){
       nwaluft_item.rought_opening_height = nwaluft_item.rought_opening_height.replace(/-/g, " ");
       var txt = nwaluft_item.rought_opening_height;
      var x ='"';
      x = txt+x;
      doc.text(x);
     }
     if(shouldDisplay(nwaluft_item.single_door_hand)){
       nwaluft_item.single_door_hand = nwaluft_item.single_door_hand.replace(/-/g, " ");
       doc.text(nwaluft_item.single_door_hand);
     }
	 
	  if(shouldDisplay(nwaluft_item.offset_arm)){
        nwaluft_item.offset_arm = nwaluft_item.offset_arm.replace(/-/g, " ");
        doc.text(nwaluft_item.offset_arm);
     }
	 
	 
     if(shouldDisplay(nwaluft_item.door_style)){
       nwaluft_item.door_style = nwaluft_item.door_style.replace(/-/g, " ");
       doc.text(nwaluft_item.door_style);
     }
     if(shouldDisplay(nwaluft_item.type_of_glass)){
       nwaluft_item.type_of_glass = nwaluft_item.type_of_glass.replace(/-/g, " ");
       doc.text(nwaluft_item.type_of_glass);
     }
     if(shouldDisplay(nwaluft_item.frame_system)){
       nwaluft_item.frame_system = nwaluft_item.frame_system.replace(/-/g, " ");
       doc.text(nwaluft_item.frame_system);
     }
     if(shouldDisplay(nwaluft_item.frame_finish)){
       nwaluft_item.frame_finish = nwaluft_item.frame_finish.replace(/-/g, " ");
       doc.text(nwaluft_item.frame_finish);
     }
     if(shouldDisplay(nwaluft_item.hardware)){
       nwaluft_item.hardware = nwaluft_item.hardware.replace(/-/g, " ");
       doc.text(nwaluft_item.hardware);
     }
     if(shouldDisplay(nwaluft_item.threshold)){
       nwaluft_item.threshold = nwaluft_item.threshold.replace(/-/g, " ");
       doc.text(nwaluft_item.threshold);
     }
     if(shouldDisplay(nwaluft_item.door_closer)){
       nwaluft_item.door_closer = nwaluft_item.door_closer.replace(/-/g, " ");
       doc.text(nwaluft_item.door_closer);
     }
     if(shouldDisplay(nwaluft_item.look)){
       nwaluft_item.look = nwaluft_item.look.replace(/-/g, " ");
       doc.text(nwaluft_item.look);
     }
     if(shouldDisplay(nwaluft_item.panic_device)){
       nwaluft_item.panic_device = nwaluft_item.panic_device.replace(/-/g, " ");
       doc.text(nwaluft_item.panic_device);
     }
     if(shouldDisplay(nwaluft_item.pull)){
       nwaluft_item.pull = nwaluft_item.pull.replace(/-/g, " ");
       doc.text(nwaluft_item.pull);
     }
     if(shouldDisplay(nwaluft_item.mid_panel)){
       nwaluft_item.mid_panel = nwaluft_item.mid_panel.replace(/-/g, " ");
       doc.text(nwaluft_item.mid_panel);
     }
     if(shouldDisplay(nwaluft_item.bottom_rail)){
       nwaluft_item.bottom_rail = nwaluft_item.bottom_rail.replace(/-/g, " ");
       doc.text(nwaluft_item.bottom_rail);
     }

     if(shouldDisplay(nwaluft_item.door_sweep)){
       nwaluft_item.door_sweep = nwaluft_item.door_sweep.replace(/-/g, " ");
       doc.text(nwaluft_item.door_sweep);
     }

     if(shouldDisplay(nwaluft_item.notes)){
       addLines(2, doc);
       nwaluft_item.notes = nwaluft_item.notes.replace(/-/g, " ");
       doc.text(nwaluft_item.notes);
     }

     materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              //console.log('materialsXStart',materialsXStart);
              //console.log('materialsYStart',materialsYStart);

              doc.text('GLASS REMINDERS:',materialsXStart-145 , materialsYStart+50, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
             
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('nwaluft_item',nwaluft_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(nwaluft_item.solar_film)){
               doc.y+=12;
              doc.x+=155; 
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=155; 
              doc.text(titleCase(boolToHuman(nwaluft_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(nwaluft_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              nwaluft_item.solar_film_responsibility=nwaluft_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(nwaluft_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(nwaluft_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              nwaluft_item.solar_film_type=nwaluft_item.solar_film_type.replace(/\\/g, '');
              nwaluft_item.solar_film_type=nwaluft_item.solar_film_type.replace(/'/g, '');
              nwaluft_item.solar_film_type=nwaluft_item.solar_film_type.replace(/\//g, "");
              nwaluft_item.solar_film_type=nwaluft_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(nwaluft_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(nwaluft_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              nwaluft_item.solar_film_source=nwaluft_item.solar_film_source.replace(/\\/g, '');
              nwaluft_item.solar_film_source=nwaluft_item.solar_film_source.replace(/'/g, '');
              nwaluft_item.solar_film_source=nwaluft_item.solar_film_source.replace(/\//g, "");
              nwaluft_item.solar_film_source=nwaluft_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(nwaluft_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(nwaluft_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              nwaluft_item.wet_seal=nwaluft_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(nwaluft_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(nwaluft_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              nwaluft_item.wet_seal_responsibility=nwaluft_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(nwaluft_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(nwaluft_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluft_item.furniture_to_move=nwaluft_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(nwaluft_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(nwaluft_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluft_item.furniture_to_move_comment=nwaluft_item.furniture_to_move_comment.replace(/_/g, " ");
                  nwaluft_item.furniture_to_move_comment=nwaluft_item.furniture_to_move_comment.replace(/\\/g, '');
                  nwaluft_item.furniture_to_move_comment=nwaluft_item.furniture_to_move_comment.replace(/'/g, '');
                  nwaluft_item.furniture_to_move_comment=nwaluft_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(nwaluft_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(nwaluft_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluft_item.walls_or_ceilings_to_cut=nwaluft_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  nwaluft_item.walls_or_ceilings_to_cut=nwaluft_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  nwaluft_item.walls_or_ceilings_to_cut=nwaluft_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  nwaluft_item.walls_or_ceilings_to_cut=nwaluft_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(nwaluft_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(nwaluft_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  nwaluft_item.walls_or_ceilings_to_cut_responsibility=nwaluft_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(nwaluft_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(nwaluft_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  nwaluft_item.walls_or_ceilings_to_cut_comment=nwaluft_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  nwaluft_item.walls_or_ceilings_to_cut_comment=nwaluft_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  nwaluft_item.walls_or_ceilings_to_cut_comment=nwaluft_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  nwaluft_item.walls_or_ceilings_to_cut_comment=nwaluft_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(nwaluft_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(nwaluft_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  nwaluft_item.blind_needs_removing=nwaluft_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(nwaluft_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(nwaluft_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  nwaluft_item.glass_fits_elevator=nwaluft_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(nwaluft_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(nwaluft_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwaluft_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(nwaluft_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwaluft_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(nwaluft_item.damage_waiver_text) ||shouldDisplay(nwaluft_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  nwaluft_item.damage_waiver_select = nwaluft_item.damage_waiver_select.replace(/_/g, " ");
                  if(nwaluft_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || nwaluft_item.damage_waiver_select =='Handling of Customers Materials' ||nwaluft_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(nwaluft_item.damage_waiver_select);
                  }else{
                  
                  if(nwaluft_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(nwaluft_item.damage_waiver_select);
                  }
                  doc.text(nwaluft_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(nwaluft_item.disclamers=='1'){
                if(shouldDisplay(nwaluft_item.disclamers)){
                    if(nwaluft_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || nwaluft_item.damage_waiver_select =='Handling of Customers Materials' ||nwaluft_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(nwaluft_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(nwaluft_item.disclamers_text) || shouldDisplay(nwaluft_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  nwaluft_item.disclamers_select = nwaluft_item.disclamers_select.replace(/_/g, " ");
                  if(nwaluft_item.disclamers_select == 'Wood Bead' || nwaluft_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || nwaluft_item.disclamers_select == 'TBD'){
                      doc.text(nwaluft_item.disclamers_select);
                  }else{
                  if(nwaluft_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(nwaluft_item.disclamers_select);
                  }
                  doc.text(nwaluft_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(nwaluft_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(nwaluft_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(nwaluft_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(nwaluft_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(nwaluft_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(nwaluft_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(nwaluft_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(nwaluft_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(nwaluft_item.tgpd_tgpd_caulk_amount) || shouldDisplay(nwaluft_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(nwaluft_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(nwaluft_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(nwaluft_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(nwaluft_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(nwaluft_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(nwaluft_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(nwaluft_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(nwaluft_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

 if(nwaluftIndex < nwaluftLength){ //there are more nwdr items
      if(nwaluftIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();

      }
    //  nwaluft_item = job_item["agd_items"]["nwaluft"][nwaluftIndex];
	  nwaluft_item = job_item["nwdr_items"]["nwaluft"][nwaluftIndex];
      console.log("--------------------------------------------");
    //  console.log("Nwaluft ITEM: " + JSON.stringify(job_item["nwdr_items"]["nwaluft"][nwaluftIndex]) )
      console.log("--------------------------------------------");
      addPhotosFromProjectItem(nwaluft_item, ["category", "type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = nwaluftIndex + 1 ;
      doc.fillColor(primaryColor);
      doc.text("Door " + curPiece + " - " +  DOOR_NEW_LABELS[ nwaluft_item.category ][ nwaluft_item.type ] + " " ,20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwaluft_item.category][nwaluft_item.type]}],nwaluftPhotoCallback, nwaluftPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }

	  }
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url:DOOR_NEW_PHOTOS[nwaluft_item.category][nwaluft_item.type]}],nwaluftPhotoCallback, nwaluftPhotoFailCallback, doc, 150, false); //add logo to header

}

// 'id'(11) NOT NULL,
// 'job_id'(11) DEFAULT NULL,
// 'category'(222) DEFAULT NULL,
// 'type'(222) DEFAULT NULL,
// 'opening_width'(11) DEFAULT NULL,
// 'opening_height'(11) DEFAULT NULL,
// 'rought_opening_width'(11) DEFAULT NULL,
// 'rought_opening_height'(11) DEFAULT NULL,
// 'single_door_hand'(222) DEFAULT NULL,
// 'door_style'(222) DEFAULT NULL,
// 'type_of_glass'(222) DEFAULT NULL,
// 'frame_system'(222) DEFAULT NULL,
// 'frame_finish'(222) DEFAULT NULL,
// 'hardware'(222) DEFAULT NULL,
// 'threshold'(222) DEFAULT NULL,
// 'door_closer'(222) DEFAULT NULL,
// 'look'(222) DEFAULT NULL,
// 'panic_device'(222) DEFAULT NULL,
// 'pull'(222) DEFAULT NULL,
// 'mid_panel'(222) DEFAULT NULL,
// 'bottom_rail'(222) DEFAULT NULL,
// 'notes' text,
// 'pictures_download_url' text,
// 'sketches_download_url' text