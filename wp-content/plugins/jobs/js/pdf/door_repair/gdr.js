var gdr_item;
addGdrItemsToPdf = function(callback){
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  //doc.y+=10;
  doc.moveDown();
  doc.moveDown();
  doc.text('All Glass Door -  Parts Repair', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
 /* doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/
  


  
 
  

  var gdrLength = job_item["agd_items"]["gdr"].length; // must be > 0
  var gdrIndex  = 0;
  gdr_item = job_item["agd_items"]["gdr"][gdrIndex];
  addPhotosFromProjectItem(gdr_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  var curPiece = gdrIndex + 1 ;
  doc.fillColor(primaryColor);

  type_update_label = gdr_item.type+'-'+gdr_item.subtype;

console.log('gdr_item.type',gdr_item.type);

if(type_update_label == 'a-single'){

  var rep_dor_type = 'A Single, Left - (TOP & BOTTOM PATCH)';

} else if(type_update_label == 'a-single_right'){

  var rep_dor_type = 'A Single, Right - (TOP & BOTTOM PATCH)';

}else if(type_update_label == 'a-pair'){

  var rep_dor_type = 'A Pair - (TOP & BOTTOM PATCH)';

} else if(type_update_label == 'f-single'){

  var rep_dor_type = 'F Single, Left - (TOP/BOTTOM PATCH with BOTTOM PATCH LOCK)';

} else if(type_update_label == 'f-single_right'){

  var rep_dor_type = 'F Single, Right - (TOP/BOTTOM PATCH with BOTTOM PATCH LOCK)';

} else if(type_update_label == 'f-pair'){

  var rep_dor_type = 'F Pair â€“ (TOP/BOTTOM PATCH with BOTTOM PATCH LOCK)';

} else if(type_update_label == 'bp-single'){

  var rep_dor_type = 'BP Single, Left - (BOTTOM RAIL & TOP PATCH)';

} else if(type_update_label == 'bp-single_right'){

  var rep_dor_type = 'BP Single, Right - (BOTTOM RAIL & TOP PATCH)';

} else if(type_update_label == 'bp-pair'){

  var rep_dor_type = 'BP Pair - (BOTTOM RAIL & TOP PATCH)';

} else if(type_update_label == 'p-single'){

  var rep_dor_type = 'P Single, Left - (TOP & BOTTOM RAILS)';

} else if(type_update_label == 'p-single_right'){

  var rep_dor_type = 'P Single, Right - (TOP & BOTTOM RAILS)';

} else if(type_update_label == 'p-pair'){

  var rep_dor_type = 'P Pair - (TOP & BOTTOM RAILS)';

} else if(type_update_label == 'wp-single'){

  var rep_dor_type = 'WP Single, Left â€“ (TOP & BOTTOM RAILS with Side Rails)';

} else if(type_update_label == 'wp-single_right'){

  var rep_dor_type = 'WP Single, Right - (TOP & BOTTOM RAILS with Side Rails) ';

} else if(type_update_label == 'wp-pair'){

  var rep_dor_type = 'WP Pair - (TOP & BOTTOM RAILS with Side Rails)';

}else {
  var rep_dor_type = gdr_item.type+' '+gdr_item.subtype

}
  doc.y+=50;
  doc.text("Door " +  curPiece + " - " +   rep_dor_type ,20,120,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

  //pull door hardcoded photo from amazon s3 using DOOR_REPAIR_PHOTOS object inside pdf/variables.js
  gdrPhotoFailCallback = function(){
    console.log("failed to grab GDR Door Diagram");
  }


  gdrPhotoCallback = function(){
    gdrIndex += 1;
    //----------labels---------
    doc.fontSize(smallFontSize);
    doc.text(' ', 200,120,itemYStart, {align : 'left'});
  if( shouldDisplay(gdr_item.location) ){
  doc.text('Location');
 }
  if( shouldDisplay(gdr_item.finish) ){
    doc.text('A - Finish');
  }
  if( shouldDisplay(gdr_item.door_closer) ){
    doc.text('B - Door Closer', {lineGap:4});
  }
  if( shouldDisplay(gdr_item.top_pivot) ){
    if(gdr_item.type == "p" || gdr_item.type == "wp" ) {
      doc.text('C - Top Pivot/Arm', {lineGap:4});
   } else{
     doc.text('C - Top Pivot', {lineGap:4});
   }
  }
  if( shouldDisplay(gdr_item.patch_fittings) ){
    doc.text('D - Patch Fittings', {lineGap:4});
  }

  if( shouldDisplay(gdr_item.top_inserts) ){
    doc.text('E - Top Insert', {lineGap:4});
  }

  if( shouldDisplay(gdr_item.header_door_stop)  ){
    doc.text('F - Header Door Stop', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.type_of_glass) ){
    doc.text('G - Type of Glass', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.handles ) ) {
    doc.text('H - Handles', {lineGap:4});
  }

  if( shouldDisplay(gdr_item.panic_device) ){
    doc.text('I  - Panic Device', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.edge_seal_weatherstrip ) ){
    doc.text('J  - Edge Seal Weatherstrip', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.bottom_insert ) ){
    doc.text('K - Bottom Insert', {lineGap:4});
  }
  if(shouldDisplay(gdr_item.bottom_pivot_arm ) ){
    doc.text('K - Bottom Pivot/Arm', {lineGap:4});
  }

  if( shouldDisplay(gdr_item.bottom_pivot ) ){
    doc.text('L  - Bottom Pivot', {lineGap:4});
  }

  if( shouldDisplay(gdr_item.floor_door_closer ) ){
    doc.text('M - Floor Door Closer', {lineGap:4});
  }

  if( shouldDisplay(gdr_item.threshold) ){
    doc.text('N  - Threshold', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.bottom_rail)){
    if(! gdr_item.type == "p" && ! gdr_item.type == "wp" ){
      doc.text('O - Bottom Rail', {lineGap:4});
    }
  }

  if(shouldDisplay(gdr_item.top_rail)){
    doc.text('O - Top Rail', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.patch_lock)){
    if(gdr_item.type == "f" ) {
      doc.text('P - Patch Lock', {lineGap:4});
    }
    if(gdr_item.type == "bp" || gdr_item.type == "p" || gdr_item.type == "wp" ) {
      doc.text('P - Lock', {lineGap:4});
    }
  }

  if(shouldDisplay(gdr_item.bottom_arm)){
    doc.text('Q - Bottom Arm', {lineGap:4});
  }

  if(shouldDisplay(gdr_item.bottom_rail)){
    if(gdr_item.type == "p" || gdr_item.type == "wp"){
      doc.text('R - Bottom Rail', {lineGap:4});
    }
  }

  if(shouldDisplay(gdr_item.side_rails)){
    doc.text('S - Side Rails', {lineGap:4});
  }


  if( shouldDisplay(gdr_item.notes) ){
    addLines(2, doc);
    doc.text('Notes');
  }

  //--------------------------------------
  //-----------DISPLAY VALUES-------------
  //--------------------------------------

  doc.fontSize(smallFontSize);
  doc.text(' ', 350,120,itemYStart, {align : 'left'});
  if( shouldDisplay( gdr_item.location ) ){
    doc.text(gdr_item.location);
  }
  if( shouldDisplay( gdr_item.finish ) ){
    doc.text(gdr_item.finish);
  }
  fieldAndStatus(gdr_item, 'door_closer', doc);
  fieldAndStatus(gdr_item, 'top_pivot', doc);
  fieldAndStatus(gdr_item, 'patch_fittings', doc);
  fieldAndStatus(gdr_item, 'top_inserts', doc);
  fieldAndStatus(gdr_item, 'header_door_stop', doc);
  fieldAndStatus(gdr_item, 'type_of_glass', doc);
  fieldAndStatus(gdr_item, 'handles', doc);
  fieldAndStatus(gdr_item, 'panic_device', doc);
  fieldAndStatus(gdr_item, 'edge_seal_weatherstrip', doc);
  fieldAndStatus(gdr_item, 'bottom_insert', doc);
  fieldAndStatus(gdr_item, 'bottom_pivot_arm', doc);
  fieldAndStatus(gdr_item, 'bottom_pivot', doc);
  fieldAndStatus(gdr_item, 'floor_door_closer', doc);
  fieldAndStatus(gdr_item, 'threshold', doc);
  if(! gdr_item.type == "p" && ! gdr_item.type == "wp" ){
    fieldAndStatus(gdr_item, 'bottom_rail', doc);
  }
  fieldAndStatus(gdr_item, 'top_rail', doc);
  fieldAndStatus(gdr_item, 'patch_lock', doc);
  fieldAndStatus(gdr_item, 'bottom_arm', doc);
  if(gdr_item.type == "p" || gdr_item.type == "wp"){
    fieldAndStatus(gdr_item, 'bottom_rail', doc);
  }
  fieldAndStatus(gdr_item, 'side_rails', doc);

  if( shouldDisplay(gdr_item.notes) ){
    addLines(2, doc);
    doc.text(gdr_item.notes);
  }


  materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              

              doc.text('GLASS REMINDERS:',materialsXStart-160 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
            
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('gdr_item',gdr_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(gdr_item.solar_film)){
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=155; 
              doc.text(titleCase(boolToHuman(gdr_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(gdr_item.solar_film_responsibility)){

              doc.y+=6;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              gdr_item.solar_film_responsibility=gdr_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(gdr_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(gdr_item.solar_film_type)){
              doc.y+=6;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              gdr_item.solar_film_type=gdr_item.solar_film_type.replace(/\\/g, '');
              gdr_item.solar_film_type=gdr_item.solar_film_type.replace(/'/g, '');
              gdr_item.solar_film_type=gdr_item.solar_film_type.replace(/\//g, "");
              gdr_item.solar_film_type=gdr_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(gdr_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(gdr_item.solar_film_source)){
              doc.y+=6;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              gdr_item.solar_film_source=gdr_item.solar_film_source.replace(/\\/g, '');
              gdr_item.solar_film_source=gdr_item.solar_film_source.replace(/'/g, '');
              gdr_item.solar_film_source=gdr_item.solar_film_source.replace(/\//g, "");
              gdr_item.solar_film_source=gdr_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(gdr_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(gdr_item.wet_seal)){
              doc.y+=6;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              gdr_item.wet_seal=gdr_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(gdr_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(gdr_item.wet_seal_responsibility)){
                doc.y+=6;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              gdr_item.wet_seal_responsibility=gdr_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(gdr_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(gdr_item.furniture_to_move)){

                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdr_item.furniture_to_move=gdr_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(gdr_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(gdr_item.furniture_to_move_comment)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdr_item.furniture_to_move_comment=gdr_item.furniture_to_move_comment.replace(/_/g, " ");
                  gdr_item.furniture_to_move_comment=gdr_item.furniture_to_move_comment.replace(/\\/g, '');
                  gdr_item.furniture_to_move_comment=gdr_item.furniture_to_move_comment.replace(/'/g, '');
                  gdr_item.furniture_to_move_comment=gdr_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(gdr_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(gdr_item.walls_or_ceilings_to_cut)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdr_item.walls_or_ceilings_to_cut=gdr_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  gdr_item.walls_or_ceilings_to_cut=gdr_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  gdr_item.walls_or_ceilings_to_cut=gdr_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  gdr_item.walls_or_ceilings_to_cut=gdr_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(gdr_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(gdr_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdr_item.walls_or_ceilings_to_cut_responsibility=gdr_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(gdr_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(gdr_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  gdr_item.walls_or_ceilings_to_cut_comment=gdr_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  gdr_item.walls_or_ceilings_to_cut_comment=gdr_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  gdr_item.walls_or_ceilings_to_cut_comment=gdr_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  gdr_item.walls_or_ceilings_to_cut_comment=gdr_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(gdr_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(gdr_item.blind_needs_removing)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  gdr_item.blind_needs_removing=gdr_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(gdr_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(gdr_item.glass_fits_elevator)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  gdr_item.glass_fits_elevator=gdr_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(gdr_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(gdr_item.color_viewer)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(gdr_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(gdr_item.damage_viewer)){

                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(gdr_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(gdr_item.damage_waiver_text) ||shouldDisplay(gdr_item.damage_waiver_select)){
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  gdr_item.damage_waiver_select = gdr_item.damage_waiver_select.replace(/_/g, " ");
                  if(gdr_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || gdr_item.damage_waiver_select =='Handling of Customers Materials' ||gdr_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(gdr_item.damage_waiver_select);
                  }else{
                  
                  if(gdr_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(gdr_item.damage_waiver_select);
                  }
                  doc.text(gdr_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(gdr_item.disclamers=='1'){
                if(shouldDisplay(gdr_item.disclamers)){
                    doc.y+=6;
                     doc.x+=195;
                      
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(gdr_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(gdr_item.disclamers_text) || shouldDisplay(gdr_item.disclamers_select)){
                  
                  doc.y+=6;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  gdr_item.disclamers_select = gdr_item.disclamers_select.replace(/_/g, " ");
                  if(gdr_item.disclamers_select == 'Wood Bead' || gdr_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || gdr_item.disclamers_select == 'TBD'){
                      doc.text(gdr_item.disclamers_select);
                  }else{
                  if(gdr_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(gdr_item.disclamers_select);
                  }
                  doc.text(gdr_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(gdr_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=6;
                doc.x-=155;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(gdr_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(gdr_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=6;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(gdr_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(gdr_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=6;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=6;
                doc.x+=155;
                doc.text(boolToHuman(gdr_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(gdr_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=6;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(gdr_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(gdr_item.tgpd_tgpd_caulk_amount) || shouldDisplay(gdr_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(gdr_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(gdr_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(gdr_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(gdr_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(gdr_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(gdr_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(gdr_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(gdr_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
           
          
           
  if(gdrIndex < gdrLength){ //there are more gdr items
    if(gdrIndex != 0 ){ // not last item & not first item - already has new page
      doc.addPage();
    }
    gdr_item = job_item["agd_items"]["gdr"][gdrIndex];
    addPhotosFromProjectItem(gdr_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
    var curPiece = gdrIndex + 1 ;
    doc.fillColor(primaryColor);
    doc.fontSize(bodyFontSize);

    type_update_label = gdr_item.type+'-'+gdr_item.subtype;

console.log('gdr_item.type',gdr_item.type);

if(type_update_label == 'a-single'){

  var rep_dor_type = 'A Single, Left - (TOP & BOTTOM PATCH)';

} else if(type_update_label == 'a-single_right'){

  var rep_dor_type = 'A Single, Right - (TOP & BOTTOM PATCH)';

}else if(type_update_label == 'a-pair'){

  var rep_dor_type = 'A Pair - (TOP & BOTTOM PATCH)';

} else if(type_update_label == 'f-single'){

  var rep_dor_type = 'F Single, Left - (TOP/BOTTOM PATCH with BOTTOM PATCH LOCK)';

} else if(type_update_label == 'f-single_right'){

  var rep_dor_type = 'F Single, Right - (TOP/BOTTOM PATCH with BOTTOM PATCH LOCK)';

} else if(type_update_label == 'f-pair'){

  var rep_dor_type = 'F Pair â€“ (TOP/BOTTOM PATCH with BOTTOM PATCH LOCK)';

} else if(type_update_label == 'bp-single'){

  var rep_dor_type = 'BP Single, Left - (BOTTOM RAIL & TOP PATCH)';

} else if(type_update_label == 'bp-single_right'){

  var rep_dor_type = 'BP Single, Right - (BOTTOM RAIL & TOP PATCH)';

} else if(type_update_label == 'bp-pair'){

  var rep_dor_type = 'BP Pair - (BOTTOM RAIL & TOP PATCH)';

} else if(type_update_label == 'p-single'){

  var rep_dor_type = 'P Single, Left - (TOP & BOTTOM RAILS)';

} else if(type_update_label == 'p-single_right'){

  var rep_dor_type = 'P Single, Right - (TOP & BOTTOM RAILS)';

} else if(type_update_label == 'p-pair'){

  var rep_dor_type = 'P Pair - (TOP & BOTTOM RAILS)';

} else if(type_update_label == 'wp-single'){

  var rep_dor_type = 'WP Single, Left â€“ (TOP & BOTTOM RAILS with Side Rails)';

} else if(type_update_label == 'wp-single_right'){

  var rep_dor_type = 'WP Single, Right - (TOP & BOTTOM RAILS with Side Rails) ';

} else if(type_update_label == 'wp-pair'){

  var rep_dor_type = 'WP Pair - (TOP & BOTTOM RAILS with Side Rails)';

}else {
  var rep_dor_type = gdr_item.type+' '+gdr_item.subtype

}





    doc.text("Door " +  curPiece + " - " +   rep_dor_type ,20,80, {underline: true});
    doc.moveDown();
    var itemXStart = doc.x;
    var itemYStart = doc.y;
    doc.fillColor(bodyColor);
    imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["gdr"][gdr_item.type][gdr_item.subtype]}],gdrPhotoCallback, gdrPhotoFailCallback, doc, 150, false);

  }
  else{
    callback();
  }
}
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["gdr"][gdr_item.type][gdr_item.subtype]}],gdrPhotoCallback, gdrPhotoFailCallback, doc, 150, false);
}