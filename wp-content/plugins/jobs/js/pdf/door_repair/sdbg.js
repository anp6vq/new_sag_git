

var sdbg_item;
addSdbgItemsToPdf = function(callback){

  doc.addPage();

  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  //doc.y+=10;
  doc.moveDown();
  doc.moveDown();
  doc.text('Showr Door Broken Glass', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);

  var sdbgLength = job_item["agd_items"]["sdbg"].length; // must be > 0
  var sdbgIndex  = 0;
  sdbg_item = job_item["agd_items"]["sdbg"][sdbgIndex];
  addPhotosFromProjectItem(sdbg_item, ["location","type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  var curPiece = sdbgIndex + 1 ;
  doc.fillColor(primaryColor);

  var type = sdbg_item.type;

  if(type == 'left_2_hing'){

      var print_type = "LEFT (2 Hinge)";

  } else if(type == 'left_3_hing'){
    var print_type = "LEFT (3 Hinge)";

  } else if(type == 'right_2_hing'){
    var print_type = "RIGHT (2 Hinge)";
    
  } else if(type == 'right_3_hing'){
    var print_type = "RIGHT (3 Hinge)";
    
  } 


  //var glass_type = type.replace('_',' ');
  doc.y+=50;
  doc.text("Glass " +  curPiece + " - " +   print_type ,20,120,doc.y, {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);






   sdbgPhotoFailCallback = function(){
    console.log("failed to grab SDBG Door Diagram");
  } 

sdbgPhotoCallback = function(){
    sdbgIndex += 1;


    doc.fontSize(smallFontSize);
    doc.text(' ', 200,120,itemYStart, {align : 'left'});
  if( shouldDisplay(sdbg_item.text_tagd_gdn_a_single_glass_location) ){
  doc.text('Location', {lineGap:4});
 }

 if( shouldDisplay(sdbg_item.text_tagd_gdn_a_single_glass_thickness) ){
    doc.text('Glass Color/Thickness', {lineGap:4});
  }

  if( shouldDisplay(sdbg_item.polished_edge) ){
    doc.text('POLISHED EDGES', {lineGap:4});
  }


  if( shouldDisplay(sdbg_item.text_sdbg_a_height_left) ){
    doc.text('A - HEIGHT LEFT ', {lineGap:4});
  }

  if( shouldDisplay(sdbg_item.text_sdbg_b_widght_left) ){
    doc.text('B - WIDTH ', {lineGap:4});
  }

  if( shouldDisplay(sdbg_item.text_sdbg_c_height_left) ){
    doc.text('C - HEIGHT RIGHT (if different from A) ', {lineGap:4});
  }

  if( shouldDisplay(sdbg_item.text_sdbg_d_top_door_hing) ){
    doc.text('D - TOP of Door to CL of Hinge ', {lineGap:4});
  }

  if( shouldDisplay(sdbg_item.text_sdbg_e_bottom_door_hing) ){
    doc.text('E - BOTTOM of Door to CL of Hinge ', {lineGap:4});
  }


  if( shouldDisplay(sdbg_item.text_tagd_gdn_a_single_hole_size) ){
    doc.text('G - GHole Size',{lineGap:4});
  }

   if( shouldDisplay(sdbg_item.textarea_tagd_gdn_a_single_notes) ){
    doc.text('Instructions',{lineGap:4});
  }


  var height_y = 120;

   doc.fontSize(smallFontSize);
  doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
  if( shouldDisplay( sdbg_item.text_tagd_gdn_a_single_glass_location ) ){
    doc.text(sdbg_item.text_tagd_gdn_a_single_glass_location);
  }

 
    
  if( shouldDisplay( sdbg_item.text_tagd_gdn_a_single_glass_thickness ) ){

    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_tagd_gdn_a_single_glass_thickness);
  }

  if( shouldDisplay( sdbg_item.polished_edge ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.polished_edge);
  }

  if( shouldDisplay( sdbg_item.text_sdbg_a_height_left ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_sdbg_a_height_left);
  }

  if( shouldDisplay( sdbg_item.text_sdbg_b_widght_left ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_sdbg_b_widght_left);
  }

  if( shouldDisplay( sdbg_item.text_sdbg_c_height_left ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_sdbg_c_height_left);
  }

  if( shouldDisplay( sdbg_item.text_sdbg_d_top_door_hing ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_sdbg_d_top_door_hing);
  }

  if( shouldDisplay( sdbg_item.text_sdbg_e_bottom_door_hing ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_sdbg_e_bottom_door_hing);
  }

  if( shouldDisplay( sdbg_item.text_tagd_gdn_a_single_hole_size ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.text_tagd_gdn_a_single_hole_size);
  }

  


   if( shouldDisplay( sdbg_item.textarea_tagd_gdn_a_single_notes ) ){
    height_y = height_y + 14;
    doc.text(' ', 380,height_y,itemYStart, {align : 'left'});
    doc.text(sdbg_item.textarea_tagd_gdn_a_single_notes);
  }



   // console.log('sdbgIndex',sdbgIndex);

   // console.log('sdbgLength',sdbgLength);

    if(sdbgIndex < sdbgLength){
    if(sdbgIndex != 0 ){ // not last item & not first item - already has new page
      doc.addPage();
    }
    sdbg_item = job_item["agd_items"]["sdbg"][sdbgIndex];
    addPhotosFromProjectItem(sdbg_item, ["location","type"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
    var curPiece = sdbgIndex + 1 ;
    doc.fillColor(primaryColor);
    doc.fontSize(bodyFontSize);  

  doc.y+=50;
 
    doc.text(' ', 200,120,itemYStart, {align : 'left'});
    doc.text("Door " +  curPiece + " - " +   print_type ,20,80, {underline: true});
    doc.moveDown();
    var itemXStart = doc.x;
    var itemYStart = doc.y;
    doc.fillColor(bodyColor);
    imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["sdbg"][sdbg_item.type]}],sdbgPhotoCallback, sdbgPhotoFailCallback, doc, 150, false);

    if(shouldDisplay(sdbg_item.all_replacement_heandle_type_a_single_door)){
      imagesAjaxRequests([{url: DOOR_HANDLE_PHOTOS[sdbg_item.all_replacement_heandle_type_a_single_door]}],continueAfterHandleImage, sdbgPhotoFailCallback, doc, 150, false, MARGIN_LEFT_RIGHT  , 440 ); //add logo to header
    }  


  } else{
    callback();
  }


}

continueAfterHandleImage = function(){

    if( shouldDisplay( sdbg_item.all_replacement_heandle_type_a_single_door ) ){
    doc.text(' ', 240,440,itemYStart, {align : 'left'});
    doc.text('Handle : '+sdbg_item.all_replacement_heandle_type_a_single_door);
 
    doc.y+=12;
    doc.text('GLASS REMINDERS :');

    setFont(bodyColor,smallFontSize, doc);
            
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('sdbg_item',gdn_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(sdbg_item.solar_film)){
              doc.text('Solar Film :');
              doc.y-=12;
              doc.x+=150; 
              doc.text(titleCase(boolToHuman(sdbg_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(sdbg_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=190;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              sdbg_item.solar_film_responsibility=sdbg_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(sdbg_item.solar_film_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(sdbg_item.solar_film_type)){
              doc.y+=12;
              doc.x+=190;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              sdbg_item.solar_film_type=sdbg_item.solar_film_type.replace(/\\/g, '');
              sdbg_item.solar_film_type=sdbg_item.solar_film_type.replace(/'/g, '');
              sdbg_item.solar_film_type=sdbg_item.solar_film_type.replace(/\//g, "");
              sdbg_item.solar_film_type=sdbg_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(sdbg_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(sdbg_item.solar_film_source)){
              doc.y+=12;
              doc.x+=190;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              sdbg_item.solar_film_source=sdbg_item.solar_film_source.replace(/\\/g, '');
              sdbg_item.solar_film_source=sdbg_item.solar_film_source.replace(/'/g, '');
              sdbg_item.solar_film_source=sdbg_item.solar_film_source.replace(/\//g, "");
              sdbg_item.solar_film_source=sdbg_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(sdbg_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(sdbg_item.wet_seal)){
              doc.y+=12;
              doc.x+=190; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              sdbg_item.wet_seal=sdbg_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(sdbg_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(sdbg_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=190; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              sdbg_item.wet_seal_responsibility=sdbg_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(sdbg_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(sdbg_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  sdbg_item.furniture_to_move=sdbg_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(sdbg_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(sdbg_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  sdbg_item.furniture_to_move_comment=sdbg_item.furniture_to_move_comment.replace(/_/g, " ");
                  sdbg_item.furniture_to_move_comment=sdbg_item.furniture_to_move_comment.replace(/\\/g, '');
                  sdbg_item.furniture_to_move_comment=sdbg_item.furniture_to_move_comment.replace(/'/g, '');
                  sdbg_item.furniture_to_move_comment=sdbg_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(sdbg_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(sdbg_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  sdbg_item.walls_or_ceilings_to_cut=sdbg_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  sdbg_item.walls_or_ceilings_to_cut=sdbg_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  sdbg_item.walls_or_ceilings_to_cut=sdbg_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  sdbg_item.walls_or_ceilings_to_cut=sdbg_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(sdbg_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(sdbg_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  sdbg_item.walls_or_ceilings_to_cut_responsibility=sdbg_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(sdbg_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(sdbg_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  sdbg_item.walls_or_ceilings_to_cut_comment=sdbg_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  sdbg_item.walls_or_ceilings_to_cut_comment=sdbg_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  sdbg_item.walls_or_ceilings_to_cut_comment=sdbg_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  sdbg_item.walls_or_ceilings_to_cut_comment=sdbg_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(sdbg_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(sdbg_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  sdbg_item.blind_needs_removing=sdbg_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(sdbg_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(sdbg_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  sdbg_item.glass_fits_elevator=sdbg_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(sdbg_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(sdbg_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(sdbg_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(sdbg_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(sdbg_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(sdbg_item.damage_waiver_text) ||shouldDisplay(sdbg_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  sdbg_item.damage_waiver_select = sdbg_item.damage_waiver_select.replace(/_/g, " ");
                  if(sdbg_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || sdbg_item.damage_waiver_select =='Handling of Customers Materials' ||sdbg_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(sdbg_item.damage_waiver_select);
                  }else{
                  
                  if(sdbg_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(sdbg_item.damage_waiver_select);
                  }
                  doc.text(sdbg_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                 if(sdbg_item.disclamers=='1'){
                if(shouldDisplay(sdbg_item.disclamers)){
                    if(sdbg_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || sdbg_item.damage_waiver_select =='Handling of Customers Materials' ||sdbg_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    //alert(doc.x);
                      doc.x+=190;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(sdbg_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(sdbg_item.disclamers_text) || shouldDisplay(sdbg_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=190;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  sdbg_item.disclamers_select = sdbg_item.disclamers_select.replace(/_/g, " ");
                  if(sdbg_item.disclamers_select == 'Wood Bead' || sdbg_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || sdbg_item.disclamers_select == 'TBD'){
                      doc.text(sdbg_item.disclamers_select);
                  }else{
                  if(sdbg_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(sdbg_item.disclamers_select);
                  }
                  doc.text(sdbg_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(sdbg_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=190;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(sdbg_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(sdbg_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=190;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(sdbg_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(sdbg_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=190;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(sdbg_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(sdbg_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=190;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(sdbg_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 
       /*
          var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(sdbg_item.tgpd_tgpd_caulk_amount) || shouldDisplay(sdbg_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(sdbg_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(sdbg_item.tgpd_caulk_type);
              doc.x=30;


             doc.y+=12;
                doc.x+=170;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }*/
          /*
              var tapeAmt = JSON.parse(sdbg_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(sdbg_item.tgpd_tape_type);
              doc.x=30;

                   
              doc.moveDown();
               doc.y+=12;
                doc.x+=170;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }*/
          
  }

  }


  
var itemXStart = doc.x;
    var itemYStart = doc.y;
if(shouldDisplay(sdbg_item.all_replacement_heandle_type_a_single_door)){
      imagesAjaxRequests([{url: DOOR_HANDLE_PHOTOS[sdbg_item.all_replacement_heandle_type_a_single_door]}],continueAfterHandleImage, sdbgPhotoFailCallback, doc, 150, false, MARGIN_LEFT_RIGHT  , 440 ); //add logo to header
    


    } 

   

           
              

//console.log('DOOR_REPAIR_PHOTOS',DOOR_REPAIR_PHOTOS);

//  alert(DOOR_REPAIR_PHOTOS["sdbg"]);
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["sdbg"][sdbg_item.type]}],sdbgPhotoCallback, sdbgPhotoFailCallback, doc, 150, false);
  /*
     var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(sdbg_item.tgpd_tgpd_caulk_amount) || shouldDisplay(sdbg_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(sdbg_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(sdbg_item.tgpd_caulk_type);
              doc.x=30;


doc.y+=12;
                doc.x+=170;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(sdbg_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(sdbg_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=170;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(sdbg_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(sdbg_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=170;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(sdbg_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(sdbg_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=170;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
        */




}
  //--------pull hardcoded door diagram from amazon s3------
 // imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["sdbg"][sdbg_item.type]}],sdbgPhotoCallback, sdbgPhotoFailCallback, doc, 150, false);
