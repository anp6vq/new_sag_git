//---------------HOLLOW METAL------------------
//---------------wmd_item---------------
var wmd_item;

addWmdItemsToPdf = function(callback){
  console.log("inside addWmdItemsToPdf")
  //callback();




  console.log('job_item["agd_items"]["wmd"]   ' + " is  ------    " + JSON.stringify(job_item["agd_items"]["wmd"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.moveDown();
  doc.moveDown();
  doc.text('Hollow Metal -  Parts Repair', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
/*  doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/
  var wmdLength = job_item["agd_items"]["wmd"].length; // must be > 0
  var wmdIndex  = 0;
  wmd_item = job_item["agd_items"]["wmd"][wmdIndex];
  console.log("--------------------------------------------");
  console.log("WMD ITEM: " + JSON.stringify(job_item["agd_items"]["wmd"][wmdIndex]));
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(wmd_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside wmd.length loop and ' + " i is " + wmdIndex);
  var curPiece = wmdIndex + 1 ;
  doc.fillColor(primaryColor);
  wmd_item.type=wmd_item.type.replace(/-/g, " ");
  wmd_item.type=wmd_item.type.replace(/_/g, " ");
  // wmd_item.type=wmd_item.subtype.replace(/-/g, " ");
  // wmd_item.type=wmd_item.subtype.replace(/_/g, " ");

  if(wmd_item.type == 'continuous hinge'){

          var rep_dor_type = 'Continuous Hinge Single Left';

        } else if(wmd_item.type == 'continuous hinge opp'){

          var rep_dor_type = 'Continuous Hinge Single Right';

        }else if(wmd_item.type == 'continuous hinge pair'){

          var rep_dor_type = 'Continuous Hinge Pair';

        } else if(wmd_item.type == 'butt hinge'){

          var rep_dor_type = 'Butt Hinge Single Left';

        } else if(wmd_item.type == 'butt hinge opp'){

          var rep_dor_type = 'Butt Hinge Single Right';

        } else if(wmd_item.type == 'butt hinge pair'){

          var rep_dor_type = 'Butt Hinge Pair';

        } else {
          var rep_dor_type = wmd_item.type;

        }


  doc.text("Door " +  curPiece + " - " +   rep_dor_type ,doc.x,doc.y, {underline: true});
  doc.moveDown();
  doc.fillColor(bodyColor);

  //pull door hardcoded photo from amazon s3 using DOOR_REPAIR_PHOTOS object inside pdf/variables.js
  console.log( 'URL TO GRAB PHOTO IS  ' + DOOR_REPAIR_PHOTOS["wmd"][wmd_item.type]);
  wmdPhotoFailCallback = function(){
    console.log("failed to grab WMD Door Diagram");
  }

  wmdPhotoCallback = function(){
    doc.text('',200,100,{align : 'left'})
    wmdIndex += 1;
    doc.fontSize(smallFontSize);
    //doc.y+=30;
	if( shouldDisplay(wmd_item.location )){
      doc.text('Location', {lineGap:4});
    }
	
	
	if( shouldDisplay(wmd_item.width )){
      doc.text('Width', {lineGap:4});
    }
	
	
	if( shouldDisplay(wmd_item.height )){
      doc.text('Height', {lineGap:4});
    }
	
	
    if( shouldDisplay(wmd_item.brand )){
      doc.text('A - Brand', {lineGap:4});
    }
    if( shouldDisplay(wmd_item.coc_closer )){
      doc.text('B - COC Closer', {lineGap:4});
    }
    if( shouldDisplay(wmd_item.surface_mounted_closer )){
      doc.text('D - Surface Mounted Closer', {lineGap:4});
    }
    if( shouldDisplay(wmd_item.offset_arm )){
      doc.text('E - Offset Arm', {lineGap:4});
    }
    if( shouldDisplay(wmd_item.panic_device_rim )){
      doc.text('I - Panic Device Rim', {lineGap:4});
    }
    if( shouldDisplay(wmd_item.panic_device_vertical_rods)){
      doc.text('I1 - Panic Device - Verical Rods', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.weather_stripping)){
      doc.text('J - Weather Stripping', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.bottom_arm)){
      doc.text('L - Bottom Arm', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.floor_closer)){
      doc.text('M - Floor Closer', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.threshold)){
      doc.text('N - Threshold', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.lock_type)){
      doc.text('W - Lock', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.knob_lock)){
      doc.text('W1 - Knob Lock', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.lever_lock)){
      doc.text('W2 - Lever Lock', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.top_flush_bolt)){
      doc.text('Y1 - Top Flush Bolt', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.bottom_flush_bolt)){
      doc.text('Y2 - Bottom Flush Bolt', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.continuous_hinge)){
      doc.text('Z - Continuous Hinge', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.top_hinge)){
      doc.text('Z1 - Top Hinge', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.intermediate_hinge)){
      doc.text('Z2 - Intermediate Hinge', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.bottom_hinge)){
      doc.text('Z3 - Bottom Hinge', {lineGap:4})
    }
    if( shouldDisplay(wmd_item.notes)){
      addLines(2, doc);
      doc.text('Notes', {lineGap:4})
    }

	doc.text('',350,100,{align : 'left'});
	if( shouldDisplay( wmd_item.location ) ){
    doc.text(wmd_item.location ,350,doc.y,{align : 'left', lineGap : 4});
  }
  
  if( shouldDisplay( wmd_item.width ) ){
      var txt = wmd_item.width;
      var x = '"';
      txt = txt+x;
      doc.text(txt ,350,doc.y,{align : 'left', lineGap : 4});
  }
  if( shouldDisplay( wmd_item.height ) ){
      var txt = wmd_item.height;
      var x = '"';
      txt = txt+x;
    doc.text(txt ,350,doc.y,{align : 'left', lineGap : 4});
  }
  
  
  if( shouldDisplay( wmd_item.brand ) ){
    doc.text(wmd_item.brand ,350,doc.y,{align : 'left', lineGap : 4});
  }
  fieldAndStatus(wmd_item, 'coc_closer', doc );
  fieldAndStatus(wmd_item, 'surface_mounted_closer', doc );
 // fieldAndStatus(wmd_item, 'surface_mounted_closer', doc );
  fieldAndStatus(wmd_item, 'offset_arm', doc);
  fieldAndStatus(wmd_item, 'panic_device_rim', doc);
  fieldAndStatus(wmd_item, 'panic_device_vertical_rods', doc);
  fieldAndStatus(wmd_item, 'weather_stripping', doc);
  fieldAndStatus(wmd_item, 'bottom_arm', doc);
  fieldAndStatus(wmd_item, 'floor_closer', doc);
  fieldAndStatus(wmd_item, 'threshold', doc);
  fieldAndStatus(wmd_item, 'lock_type', doc);
  fieldAndStatus(wmd_item, 'knob_lock', doc);
  fieldAndStatus(wmd_item, 'lever_lock', doc);
  fieldAndStatus(wmd_item, 'top_flush_bolt', doc);
  fieldAndStatus(wmd_item, 'bottom_flush_bolt', doc);
  fieldAndStatus(wmd_item, 'continuous_hinge', doc);
  fieldAndStatus(wmd_item, 'top_hinge', doc);
  fieldAndStatus(wmd_item, 'intermediate_hinge', doc);
  fieldAndStatus(wmd_item, 'bottom_hinge', doc);
  if( shouldDisplay(wmd_item.notes)){
    addLines(2, doc);
    doc.text(wmd_item.notes)
  }

  var itemXStart = doc.x;
              var itemYStart = doc.y;



              materialsXStart = doc.x;
              materialsYStart = doc.y;

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
   
              doc.text('GLASS REMINDERS:',materialsXStart-160 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
            
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('wmd_item',gdn_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(wmd_item.solar_film)){
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=150; 
              doc.text(titleCase(boolToHuman(wmd_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(wmd_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              wmd_item.solar_film_responsibility=wmd_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(wmd_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(wmd_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              wmd_item.solar_film_type=wmd_item.solar_film_type.replace(/\\/g, '');
              wmd_item.solar_film_type=wmd_item.solar_film_type.replace(/'/g, '');
              wmd_item.solar_film_type=wmd_item.solar_film_type.replace(/\//g, "");
              wmd_item.solar_film_type=wmd_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(wmd_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(wmd_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              wmd_item.solar_film_source=wmd_item.solar_film_source.replace(/\\/g, '');
              wmd_item.solar_film_source=wmd_item.solar_film_source.replace(/'/g, '');
              wmd_item.solar_film_source=wmd_item.solar_film_source.replace(/\//g, "");
              wmd_item.solar_film_source=wmd_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(wmd_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(wmd_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              wmd_item.wet_seal=wmd_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(wmd_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(wmd_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              wmd_item.wet_seal_responsibility=wmd_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(wmd_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(wmd_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  wmd_item.furniture_to_move=wmd_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(wmd_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(wmd_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  wmd_item.furniture_to_move_comment=wmd_item.furniture_to_move_comment.replace(/_/g, " ");
                  wmd_item.furniture_to_move_comment=wmd_item.furniture_to_move_comment.replace(/\\/g, '');
                  wmd_item.furniture_to_move_comment=wmd_item.furniture_to_move_comment.replace(/'/g, '');
                  wmd_item.furniture_to_move_comment=wmd_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(wmd_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(wmd_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  wmd_item.walls_or_ceilings_to_cut=wmd_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  wmd_item.walls_or_ceilings_to_cut=wmd_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  wmd_item.walls_or_ceilings_to_cut=wmd_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  wmd_item.walls_or_ceilings_to_cut=wmd_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(wmd_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(wmd_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  wmd_item.walls_or_ceilings_to_cut_responsibility=wmd_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(wmd_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(wmd_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  wmd_item.walls_or_ceilings_to_cut_comment=wmd_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  wmd_item.walls_or_ceilings_to_cut_comment=wmd_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  wmd_item.walls_or_ceilings_to_cut_comment=wmd_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  wmd_item.walls_or_ceilings_to_cut_comment=wmd_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(wmd_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(wmd_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  wmd_item.blind_needs_removing=wmd_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(wmd_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(wmd_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  wmd_item.glass_fits_elevator=wmd_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(wmd_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(wmd_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(wmd_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(wmd_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(wmd_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(wmd_item.damage_waiver_text) ||shouldDisplay(wmd_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  wmd_item.damage_waiver_select = wmd_item.damage_waiver_select.replace(/_/g, " ");
                  if(wmd_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || wmd_item.damage_waiver_select =='Handling of Customers Materials' ||wmd_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(wmd_item.damage_waiver_select);
                  }else{
                  
                  if(wmd_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(wmd_item.damage_waiver_select);
                  }
                  doc.text(wmd_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(wmd_item.disclamers=='1'){
                if(shouldDisplay(wmd_item.disclamers)){
                    if(wmd_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || wmd_item.damage_waiver_select =='Handling of Customers Materials' ||wmd_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    alert(doc.x);
                      doc.x+=195;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(wmd_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(wmd_item.disclamers_text) || shouldDisplay(wmd_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  wmd_item.disclamers_select = wmd_item.disclamers_select.replace(/_/g, " ");
                  if(wmd_item.disclamers_select == 'Wood Bead' || wmd_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || wmd_item.disclamers_select == 'TBD'){
                      doc.text(wmd_item.disclamers_select);
                  }else{
                  if(wmd_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(wmd_item.disclamers_select);
                  }
                  doc.text(wmd_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(wmd_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(wmd_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(wmd_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(wmd_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(wmd_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(wmd_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(wmd_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(wmd_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(wmd_item.tgpd_tgpd_caulk_amount) || shouldDisplay(wmd_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(wmd_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(wmd_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(wmd_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(wmd_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(wmd_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(wmd_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(wmd_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(wmd_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }




    if(wmdIndex < wmdLength){ //there are more wmd items
      if(wmdIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();
      }
      wmd_item = job_item["agd_items"]["wmd"][wmdIndex];
      console.log("--------------------------------------------")
      console.log("WMD ITEM: " + JSON.stringify(job_item["agd_items"]["wmd"][wmdIndex]) )
      console.log("--------------------------------------------")
      addPhotosFromProjectItem(wmd_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      var curPiece = wmdIndex + 1 ;
      doc.fillColor(primaryColor);

        if(wmd_item.type == 'continuous_hinge'){

          var rep_dor_type = 'Continuous Hinge-Single Left';

        } else if(wmd_item.type == 'continuous_hinge-opp'){

          var rep_dor_type = 'Continuous Hinge-Single Right';

        }else if(wmd_item.type == 'continuous_hinge-pair'){

          var rep_dor_type = 'Continuous Hinge-Pair';

        } else if(wmd_item.type == 'butt_hinge'){

          var rep_dor_type = 'Butt Hinge-Single Left';

        } else if(wmd_item.type == 'butt_hinge-opp'){

          var rep_dor_type = 'Butt Hinge-Single Right';

        } else if(wmd_item.type == 'butt_hinge-pair'){

          var rep_dor_type = 'Butt Hinge-Pair';

        } else {
          var rep_dor_type = wmd_item.type;

        }



      doc.text("Door " +  curPiece + " - " +   rep_dor_type ,20,80, {underline: true});
      doc.moveDown();
      doc.fillColor(bodyColor);
      var itemTypeUnderscores = wmd_item.type.replace(/ /g, "_");
      itemTypeUnderscores = itemTypeUnderscores.replace(/-/g, '_');
      imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["wmd"][itemTypeUnderscores]}],wmdPhotoCallback, wmdPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }
  }

  //--------pull hardcoded door diagram from amazon s3------


  var itemTypeUnderscores = wmd_item.type.replace(/ /g, "_");
  var temTypeUnderscores =  itemTypeUnderscores.replace(/-/g, "_");
  imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["wmd"][itemTypeUnderscores]}],wmdPhotoCallback, wmdPhotoFailCallback, doc, 150, false); //add logo to header


   // `id`
  // `job_id`
  // `category`
  // `type`
  // `subtype`
  // `notes`
  // `brand`
  // `coc_closer_status`
  // `coc_closer`
  // `surface_mounted_closer_status`
  // `surface_mounted_closer`
  // `offset_arm_status`
  // `offset_arm`
  // `drop_plate_status`
  // `drop_plate`
  // `type_of_glass_status`
  // `type_of_glass`
  // `handles_status`
  // `handles`
  // `panic_device_rim_status`
  // `panic_device_rim`
  // `panic_device_vertical_rods_status`
  // `panic_device_vertical_rods`
  // `weather_stripping_status`
  // `weather_stripping`
  // `bottom_arm_status`
  // `bottom_arm`
  // `floor_closer_status`
  // `floor_closer`
  // `threshold_status`
  // `threshold`
  // `type_of_beads_status`
  // `type_of_beads`
  // `thumbturn_status`
  // `thumbturn`
  // `key_cylinder_status`
  // `key_cylinder`
  // `lock_type_status`
  // `lock_type`
  // `knob_lock_status`
  // `knob_lock`
  // `lever_lock_status`
  // `lever_lock`
  // `top_flush_bolt_status`
  // `top_flush_bolt`
  // `bottom_flush_bolt_status`
  // `bottom_flush_bolt`
  // `continuous_hinge_status`
  // `continuous_hinge`
  // `top_hinge_status`
  // `top_hinge`
  // `intermediate_hinge_status`
  // `intermediate_hinge`
  // `bottom_hinge_status`
  // `bottom_hinge`
  // `pictures_download_url`
  // `sketches_download_url`
}