//-----------ALUMINUM STOREFRONT------------

//sfd_item
var sfd_item;

addSfdItemsToPdf = function(callback){
  /*console.log("inside addSfdItemsToPdf")
  callback();*/



  console.log('job_item["agd_items"]["sfd"]   ' + " is  ------    " + JSON.stringify(job_item["agd_items"]["sfd"]));
  doc.addPage();
  //add gpd photos to large image array
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.moveDown();
  doc.moveDown();
  doc.text('Aluminum Storefront -  Parts Repair', doc.x, doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
 /* doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/

  var sfdLength = job_item["agd_items"]["sfd"].length; // must be > 0
  var sfdIndex  = 0;
  sfd_item = job_item["agd_items"]["sfd"][sfdIndex];
  addPhotosFromProjectItem(sfd_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  var curPiece = sfdIndex + 1 ;
  doc.fillColor(primaryColor);
  sfd_item.type=sfd_item.type.replace(/-/g, " ");
  sfd_item.type=sfd_item.type.replace(/_/g, " ");
  // sfd_item.type=sfd_item.subtype.replace(/-/g, " ");
  // sfd_item.type=sfd_item.subtype.replace(/_/g, " ");

console.log('sfd_item.type',sfd_item.type);

if(sfd_item.type == 'offset pivots'){

  var rep_dor_type = 'Offset Pivot/Butt Hinges Doors';

} else if(sfd_item.type == 'center hung opp'){

  var rep_dor_type = 'Center Hung-Single Right';

}else if(sfd_item.type == 'center hung'){

  var rep_dor_type = 'Center Hung Single Left';

} else if(sfd_item.type == 'center hung pair'){

  var rep_dor_type = 'Center Hung Pair';

} else if(sfd_item.type == 'offset pivots opp'){

  var rep_dor_type = 'Offset Pivots/Butt Hinge-Single Right';

} else if(sfd_item.type == 'offset pivots pair'){

  var rep_dor_type = 'Offset Pivots/Butt Hinge-Pair';

} else if(sfd_item.type == 'continuous hinge'){

  var rep_dor_type = 'Continuous Hinge-Single Left';

} else if(sfd_item.type == 'continuous hinge opp'){

  var rep_dor_type = 'Continuous Hinge-Single Right';

} else if(sfd_item.type == 'continuous hinge pair'){

  var rep_dor_type = 'Continuous pair';

}else {
  var rep_dor_type = sfd_item.type;

}





  doc.text("Door " +  curPiece + " - " +   rep_dor_type ,doc.x,doc.y, {underline: true});
  doc.moveDown();
  doc.fillColor(bodyColor);

  //pull door hardcoded photo from amazon s3 using DOOR_REPAIR_PHOTOS object inside pdf/variables.js
  sfdPhotoFailCallback = function(){
    console.log("failed to grab SFD Door Diagram");
  }

  sfdPhotoCallback = function(){
    doc.text('',200,100,{align : 'left'});
    sfdIndex += 1;
    doc.fontSize(smallFontSize);
	doc.y+=20;
	 if( shouldDisplay(sfd_item.location )){
    doc.text('Location', {lineGap:4});
  }
  
  
   if( shouldDisplay(sfd_item.width )){
    doc.text('Width', {lineGap:4});
  }

   if( shouldDisplay(sfd_item.height )){
    doc.text('Height', {lineGap:4});
  }
  

  if( shouldDisplay(sfd_item.brand )){
    doc.text('A - Brand', {lineGap:4});
  }

  if( shouldDisplay(sfd_item.coc_closer )){
    doc.text('B - COC Closer', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.top_pivot_frame )){
    doc.text('C1 - Top Pivot Frame', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.top_pivot_door) ){
    doc.text('C2 - Top Pivot Door', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.surface_mounted_closer ) ){
    doc.text('D - Surface Mounted Closer', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.offset_arm )){
    doc.text('E - Offset Arm', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.drop_plate )){
    doc.text('F - Drop Plate', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.type_of_glass )){
  	doc.text('G - Type Of Glass', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.handles )){
    doc.text('H - Handles', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.panic_device)){
  	doc.text('I - Panic Device', {lineGap:4});
  }

  if(shouldDisplay(sfd_item.weather_stripping)){
  	doc.text('J - Weather Stripping', {lineGap:4});
  }

if(shouldDisplay(sfd_item.bottom_pivot)){
	doc.text('K - Bottom Pivot', {lineGap:4});
}
if(shouldDisplay(sfd_item.spindle )){
	doc.text('L - Spindle' , {lineGap:4});
}

if(shouldDisplay(sfd_item.floor_closer)){
	doc.text('M - Floor Closer', {lineGap:4});
}
if(shouldDisplay(sfd_item.threshold )){
	doc.text('N - Threshold', {lineGap:4});
}

if(shouldDisplay(sfd_item.type_of_beads )){
	doc.text('T - Type Of Beads', {lineGap:4});
}
if(shouldDisplay(sfd_item.thumbturn )){
	doc.text('U - Thumbturn', {lineGap:4});
}

if(shouldDisplay(sfd_item.key_cylinder )){
	doc.text('V - Key Cylinder', {lineGap:4});
}
if(shouldDisplay(sfd_item.lock_type )){
	doc.text('W - Lock', {lineGap:4});
}

if(shouldDisplay(sfd_item.path_paddle )){
	doc.text('X - Push Paddle', {lineGap:4}); //db name is path_paddle but display name is push_paddle
}

if(shouldDisplay(sfd_item.top_flush_bolt )){
	doc.text('Y1 - Top Flush Bolt', {lineGap:4});
}
if(shouldDisplay(sfd_item.bottom_flush_bolt)){
	doc.text('Y2 - Bottom Flush Bolt', {lineGap:4});
}
if(shouldDisplay(sfd_item.continuous_hinge )){
	doc.text('Z - Continuous Hinge', {lineGap:4});
}

if(shouldDisplay(sfd_item.top_pivot_offset )){
	doc.text('Z1 - Top Pivot Offset', {lineGap:4});
}

if(shouldDisplay(sfd_item.intermediate_pivot )){
	doc.text('Z2 - Intermediate Pivot', {lineGap:4});
}

if(shouldDisplay(sfd_item.bottom_pivot_offset )){
	doc.text('Z3 - Bottom Pivot Offset', {lineGap:4});
}
if(shouldDisplay(sfd_item.notes )){
  addLines(2, doc);
	doc.text('Notes ', {lineGap:4});
}




doc.text('',350,100,{align : 'left'});
doc.y+=20;
if( shouldDisplay( sfd_item.location ) ){
  doc.text(sfd_item.location , 350,doc.y,{align : 'left', lineGap : 4});
}


if( shouldDisplay( sfd_item.width ) ){
	//doc.y+=7;
	var txt = sfd_item.width;
	var x = '"';
	txt = txt+x;
  doc.text(txt ,350,doc.y,{align : 'left', lineGap : 4});
}

//doc.text('',350,100,{align : 'left'});
if( shouldDisplay( sfd_item.height ) ){
///	doc.y+=7;
    var txt = sfd_item.height;
    var x = '"';
    txt = txt+x;
  doc.text(txt ,350,doc.y,{align : 'left', lineGap : 4});
}


if( shouldDisplay( sfd_item.brand ) ){
	//doc.y+=7;
  doc.text(sfd_item.brand ,350,doc.y,{align : 'left', lineGap : 4});
}
fieldAndStatus(sfd_item, 'coc_closer', doc );
fieldAndStatus(sfd_item, 'pivot_frame', doc );
fieldAndStatus(sfd_item, 'top_pivot_frame', doc );
fieldAndStatus(sfd_item, 'top_pivot_door', doc );
fieldAndStatus(sfd_item, 'surface_mounted_closer', doc );
fieldAndStatus(sfd_item, 'offset_arm', doc );
fieldAndStatus(sfd_item, 'drop_plate', doc );
fieldAndStatus(sfd_item, 'type_of_glass', doc );
fieldAndStatus(sfd_item, 'handles', doc );
fieldAndStatus(sfd_item, 'panic_device', doc );
fieldAndStatus(sfd_item, 'weather_stripping', doc );
fieldAndStatus(sfd_item, 'bottom_pivot', doc );
fieldAndStatus(sfd_item, 'spindle', doc );
fieldAndStatus(sfd_item, 'floor_closer', doc );
fieldAndStatus(sfd_item, 'threshold', doc );
fieldAndStatus(sfd_item, 'type_of_beads', doc );
fieldAndStatus(sfd_item, 'thumbturn', doc );
fieldAndStatus(sfd_item, 'key_cylinder', doc );
fieldAndStatus(sfd_item, 'lock_type', doc );
fieldAndStatus(sfd_item, 'path_paddle', doc );
fieldAndStatus(sfd_item, 'top_flush_bolt', doc );
fieldAndStatus(sfd_item, 'bottom_flush_bolt', doc );
fieldAndStatus(sfd_item, 'continuous_hinge', doc );
fieldAndStatus(sfd_item, 'top_pivot_offset', doc );
fieldAndStatus(sfd_item, 'intermediate_pivot', doc );
fieldAndStatus(sfd_item, 'bottom_pivot_offset', doc );

if(shouldDisplay(sfd_item.notes )){
  addLines(2, doc);
	doc.text(sfd_item.notes);
}
              var itemXStart = doc.x;
              var itemYStart = doc.y;



              materialsXStart = doc.x;
              materialsYStart = doc.y;

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
   
              doc.text('GLASS REMINDERS:',materialsXStart-160 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
            
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('sfd_item',gdn_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(sfd_item.solar_film)){
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=150; 
              doc.text(titleCase(boolToHuman(sfd_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(sfd_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              sfd_item.solar_film_responsibility=sfd_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(sfd_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(sfd_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              sfd_item.solar_film_type=sfd_item.solar_film_type.replace(/\\/g, '');
              sfd_item.solar_film_type=sfd_item.solar_film_type.replace(/'/g, '');
              sfd_item.solar_film_type=sfd_item.solar_film_type.replace(/\//g, "");
              sfd_item.solar_film_type=sfd_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(sfd_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(sfd_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              sfd_item.solar_film_source=sfd_item.solar_film_source.replace(/\\/g, '');
              sfd_item.solar_film_source=sfd_item.solar_film_source.replace(/'/g, '');
              sfd_item.solar_film_source=sfd_item.solar_film_source.replace(/\//g, "");
              sfd_item.solar_film_source=sfd_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(sfd_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(sfd_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              sfd_item.wet_seal=sfd_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(sfd_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(sfd_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              sfd_item.wet_seal_responsibility=sfd_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(sfd_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(sfd_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  sfd_item.furniture_to_move=sfd_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(sfd_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(sfd_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  sfd_item.furniture_to_move_comment=sfd_item.furniture_to_move_comment.replace(/_/g, " ");
                  sfd_item.furniture_to_move_comment=sfd_item.furniture_to_move_comment.replace(/\\/g, '');
                  sfd_item.furniture_to_move_comment=sfd_item.furniture_to_move_comment.replace(/'/g, '');
                  sfd_item.furniture_to_move_comment=sfd_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(sfd_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(sfd_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  sfd_item.walls_or_ceilings_to_cut=sfd_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  sfd_item.walls_or_ceilings_to_cut=sfd_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  sfd_item.walls_or_ceilings_to_cut=sfd_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  sfd_item.walls_or_ceilings_to_cut=sfd_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(sfd_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(sfd_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  sfd_item.walls_or_ceilings_to_cut_responsibility=sfd_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(sfd_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(sfd_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  sfd_item.walls_or_ceilings_to_cut_comment=sfd_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  sfd_item.walls_or_ceilings_to_cut_comment=sfd_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  sfd_item.walls_or_ceilings_to_cut_comment=sfd_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  sfd_item.walls_or_ceilings_to_cut_comment=sfd_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(sfd_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(sfd_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  sfd_item.blind_needs_removing=sfd_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(sfd_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(sfd_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  sfd_item.glass_fits_elevator=sfd_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(sfd_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(sfd_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(sfd_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(sfd_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(sfd_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(sfd_item.damage_waiver_text) ||shouldDisplay(sfd_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  sfd_item.damage_waiver_select = sfd_item.damage_waiver_select.replace(/_/g, " ");
                  if(sfd_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || sfd_item.damage_waiver_select =='Handling of Customers Materials' ||sfd_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(sfd_item.damage_waiver_select);
                  }else{
                  
                  if(sfd_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(sfd_item.damage_waiver_select);
                  }
                  doc.text(sfd_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(sfd_item.disclamers=='1'){
                if(shouldDisplay(sfd_item.disclamers)){
                    if(sfd_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || sfd_item.damage_waiver_select =='Handling of Customers Materials' ||sfd_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    alert(doc.x);
                      doc.x+=195;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(sfd_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(sfd_item.disclamers_text) || shouldDisplay(sfd_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  sfd_item.disclamers_select = sfd_item.disclamers_select.replace(/_/g, " ");
                  if(sfd_item.disclamers_select == 'Wood Bead' || sfd_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || sfd_item.disclamers_select == 'TBD'){
                      doc.text(sfd_item.disclamers_select);
                  }else{
                  if(sfd_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(sfd_item.disclamers_select);
                  }
                  doc.text(sfd_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(sfd_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(sfd_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(sfd_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(sfd_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(sfd_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(sfd_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(sfd_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(sfd_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(sfd_item.tgpd_tgpd_caulk_amount) || shouldDisplay(sfd_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(sfd_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(sfd_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(sfd_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(sfd_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(sfd_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(sfd_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(sfd_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(sfd_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }



    if(sfdIndex < sfdLength){ //there are more sfd items
      if(sfdIndex != 0 ){ // not last item & not first item - already has new page
        doc.addPage();
      }
      console.log('we are on the ' +  sfdIndex  +  ' job_item["agd_items"]["sfd"]  right now');
      sfd_item = job_item["agd_items"]["sfd"][sfdIndex];
      console.log("--------------------------------------------")
      console.log("SFD ITEM: " + JSON.stringify(job_item["agd_items"]["sfd"][sfdIndex]) )
      console.log("-------------------------------------------- >> tester nee " +  sfd_item.type)
      addPhotosFromProjectItem(sfd_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
      console.log('inside sfd.length loop and ' + " sfdIndex is " + sfdIndex);
      var curPiece = sfdIndex + 1 ;
      doc.fillColor(primaryColor);

      console.log('sfd_item.itemTypeUnderscores_1 ',sfd_item.type );

      if(sfd_item.type == 'offset_pivots'){

          var rep_dor_type = 'Offset Pivots/Butt Hinge-Single Left';

        } else if(sfd_item.type == 'center_hung-opp'){

          var rep_dor_type = 'Center Hung-Single Right';

        }else if(sfd_item.type == 'center_hung'){

          var rep_dor_type = 'Center Hung-Single Left';

        } else if(sfd_item.type == 'center_hung-pair'){

          var rep_dor_type = 'Center Hung Pair';

        } else if(sfd_item.type == 'offset_pivots-opp'){

          var rep_dor_type = 'Offset Pivots/Butt Hinge-Single Right';

        } else if(sfd_item.type == 'offset_pivots-pair'){

          var rep_dor_type = 'Offset Pivots/Butt Hinge-Pair';

        } else if(sfd_item.type == 'continuous_hinge'){

          var rep_dor_type = 'Continuous Hinge-Single Left';

        } else if(sfd_item.type == 'continuous_hinge-opp'){

          var rep_dor_type = 'Continuous Hinge-Single Right';

        } else if(sfd_item.type == 'continuous_hinge-pair'){

          var rep_dor_type = 'Continuous pair';

        }else {
          var rep_dor_type = sfd_item.type;

        }

      if(sfd_item.subtype!=null){
      doc.text("Door " +  curPiece + " - " +   rep_dor_type  ,20,80, {underline: true});
      }else{
      doc.text("Door " +  curPiece + " - " +   rep_dor_type  ,20,80, {underline: true});
      }
      doc.moveDown();
      doc.fillColor(bodyColor);

      var itemTypeUnderscores = sfd_item.type.replace(/ /g, "_");

      console.log('itemTypeUnderscores_0',itemTypeUnderscores);

      itemTypeUnderscores = itemTypeUnderscores.replace(/-/g, '_');



      console.log('itemTypeUnderscores_1',itemTypeUnderscores);
	  
      imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["sfd"][itemTypeUnderscores]}],sfdPhotoCallback, sfdPhotoFailCallback, doc, 150, false); //add logo to header
    } else{
      callback();
    }
  }
  //--------pull hardcoded door diagram from amazon s3------

   
  var itemTypeUnderscores = sfd_item.type.replace(/ /g, "_");
   // itemTypeUnderscores.replace(' ', "_");
   
 var temTypeUnderscores =  itemTypeUnderscores.replace(/-/g, "_");

  console.log('itemTypeUnderscores2',itemTypeUnderscores);
  imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["sfd"][itemTypeUnderscores]}],sfdPhotoCallback, sfdPhotoFailCallback, doc, 150, false); //add logo to header


   // `id`
  // `job_id`
  // `category`
  // `type`
  // `subtype`
  // `notes`
  // `brand`
  // `coc_closer_status`
  // `coc_closer`
  // `top_pivot_frame_status`
  // `top_pivot_frame`
  // `top_pivot_door_status`
  // `top_pivot_door`
  // `surface_mounted_closer_status`
  // `surface_mounted_closer`
  // `offset_arm_status`
  // `offset_arm`
  // `drop_plate_status`
  // `drop_plate`
  // `type_of_glass_status`
  // `type_of_glass`
  // `handles_status`
  // `handles`
  // `panic_device_status`
  // `panic_device`
  // `weather_stripping_status`
  // `weather_stripping`
  // `bottom_pivot_status`
  // `bottom_pivot`
  // `spindle_status`
  // `spindle`
  // `floor_closer_status`
  // `floor_closer`
  // `threshold_status`
  // `threshold`
  // `type_of_beads_status`
  // `type_of_beads`
  // `thumbturn_status`
  // `thumbturn`
  // `key_cylinder_status`
  // `key_cylinder`
  // `lock_type_status`
  // `lock_type`
  // `path_paddle_status`
  // `path_paddle`
  // `top_flush_bolt_status`
  // `top_flush_bolt`
  // `bottom_flush_bolt_status`
  // `bottom_flush_bolt`
  // `top_pivot_offset_status`
  // `top_pivot_offset`
  // `intermediate_pivot_status`
  // `intermediate_pivot`
  // `bottom_pivot_offset_status`
  // `bottom_pivot_offset`
  // `continuous_hinge_status`
  // `continuous_hinge`
  // `pictures_download_url`
  // `sketches_download_url`
}