//---------------GLASS DOOR REPLACEMENT------------------
  //gdn_item - glass door replacement
  var  gdn_item;


  addGdnItemsToPdf = function(callback){
    //console.log("inside addGdnItemsToPdf")
   // callback();
   	  var data = {
      'action': 'update_log_for_pdf',
      'msg': 'Pdf All Glass Door Replacement -  Parts Repair Started ',
      'quote_number':job_item.quote_number
     };
       
    jQuery.post(ajax_object.ajax_url, data, function (response)
            {
    }); 


  console.log('job_item["agd_items"]["gdn"]   ' + " is  ------    " + JSON.stringify(job_item["agd_items"]["gdn"]));
  doc.addPage();
  //add gpd photos to large image array
    doc.y+=10;
  doc.fontSize(h2FontSize);
  doc.fillColor(primaryColor); // red color
  doc.moveDown();
  doc.text('All Glass Door Replacement -  Parts Repair', doc.x , doc.y + vertSectSpacing, col2Q, 14,  {align : 'center'});
  doc.fillColor(bodyColor);
  doc.fontSize(bodyFontSize);
 doc.moveDown();
 /* doc.text(job_item.job_name, doc.x, doc.y, {
    align: 'right',
  });*/
  

  console.log('GDN ITEMS SIZE is All Glass Door Replacement' + job_item["agd_items"]["gdn"].length);

  var gdnLength = job_item["agd_items"]["gdn"].length; // must be > 0
  var gdnIndex  = 0;
  console.log('we are on the ' + gdnIndex  +  ' job_item["agd_items"]["gdn"]  right now');
  gdn_item = job_item["agd_items"]["gdn"][gdnIndex];
  console.log("--------------------------------------------");
  console.log("GDN ITEM: " + JSON.stringify(job_item["agd_items"]["gdn"][gdnIndex]));
  console.log("--------------------------------------------");
 addPhotosFromProjectItem(gdn_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
  console.log('inside gdn.length loop and ' + " i is " + gdnIndex);
  var curPiece = gdnIndex + 1 ;

 var data = {
      'action': 'update_log_for_pdf',
      'msg': 'Pdf All Glass Door Replacement -  Parts Repair door '+   curPiece + ' - ' +   gdn_item.type +' Started ',
      'quote_number':job_item.quote_number
     };
       
    jQuery.post(ajax_object.ajax_url, data, function (response)
            {
    }); 


  
  doc.fillColor(primaryColor);
  doc.text("Door " +  curPiece + " - " +   gdn_item.type + " " + gdn_item.subtype ,doc.x , doc.y , {underline: true});
  doc.moveDown();
  var itemXStart = doc.x;
  var itemYStart = doc.y;
  doc.fillColor(bodyColor);

  //pull door hardcoded photo from amazon s3 using DOOR_REPAIR_PHOTOS object inside pdf/variables.js
  console.log(JSON.stringify(DOOR_REPAIR_PHOTOS["gdn"]));
   console.log("type"+gdn_item.type);
    console.log("type"+gdn_item.subtype);
  console.log( 'URL TO GRAB PHOTO IS  ' + DOOR_REPAIR_PHOTOS["gdn"][gdn_item.type]);
  gdnPhotoFailCallback = function(){
    console.log("failed to grab GDN Door Diagram");
  }

  gdnPhotoCallback = function(){
    gdnIndex += 1;
    doc.fontSize(smallFontSize);
    doc.text(' ', 200,itemYStart, {align : 'left'});
	if(shouldDisplay(gdn_item.location )) {
      doc.text('Location');
    }
    if(shouldDisplay(gdn_item.glass_width )) {
      doc.text('A - Glass Width');
    }
    if(shouldDisplay(gdn_item.glass_height)){
        doc.text('B - Glass Height');
    }
    if(shouldDisplay(gdn_item.glass_thickness)){
      doc.text('C/D Glass Color/Thickness');
    }
   /* if(shouldDisplay(gdn_item.glass_color)){
      doc.text('D - Glass Color');
    }*/
    if(shouldDisplay(gdn_item.overall_door_height)){
    	doc.text('E - Total Glass Door Height');
    }
	
	
	 if(shouldDisplay(gdn_item.top_inserts)){
    	doc.text('E1 - Top Inserts');
    }
	
    if(shouldDisplay(gdn_item.daylite) ){
      doc.text('F - Daylite');
    }
    if(shouldDisplay(gdn_item.top_shoe_size)){
      doc.text('G - Top Shoe Size');
    }
    if(shouldDisplay(gdn_item.top_shoe_pocket)){
      doc.text('H - Top Shoe Pocket');
    }
    if(shouldDisplay(gdn_item.bottom_shoe_size)){
      doc.text('I - Bottom Shoe Size');
    }
    if(shouldDisplay(gdn_item.bottom_shoe_pocket)){
      doc.text('J - Bottom Shoe Pocket');
    }
    if(shouldDisplay(gdn_item.door_opening)){
      doc.text('K - Door Opening');
    }
    if(shouldDisplay(gdn_item.type_of_set)){
      doc.text('L - Type of Set');
    }
    if(shouldDisplay(gdn_item.type_of_handle)){
      doc.text('M - Type of Handle');
    }
    if(shouldDisplay(gdn_item.hole_location)){
      doc.text('N - Hole Location');
    }
    if(shouldDisplay(gdn_item.hole_size)){
      doc.text('O - Hole Size');
    }
    if(shouldDisplay(gdn_item.patch_fitting_manufacturer)){
      doc.text('P - Patch Fitting Manufacturer');
    }
    if(shouldDisplay(gdn_item.bottom_patch_lock_manufacturer)){
      doc.text('Q  - Bottom Patch Lock Manufacturer');
    }

    if(shouldDisplay(gdn_item.lock_notch_height)){
      doc.text(' R - Lock Notch Height');
    }
    if(shouldDisplay(gdn_item.lock_notch_width)){
      doc.text('S - Lock Notch Width');
    }
    if(shouldDisplay(gdn_item.side_rail_size) ) {
      doc.text('T - Side Rail Size');
    }
    if(shouldDisplay(gdn_item.side_rail_pocket)){
      doc.text('U - Side Rail Pocket');
    }
    if(shouldDisplay(gdn_item.notes)){
      addLines(2, doc);
      doc.text('Notes');
    }
    // HANDLE
	
	var handletype;
    if(shouldDisplay(gdn_item.handle_type) ){
      addLines(6, doc);
	   handletype=doc.y;
      doc.text('Handle Type', 205, doc.y);
      if(shouldDisplay(gdn_item.handle_a)){
        doc.text('A - Handle A');
      }
      if(shouldDisplay(gdn_item.handle_b)){
        doc.text('B - Handle B');
      }
      if(shouldDisplay(gdn_item.handle_c)){
        doc.text('C - Handle C');
      }
      if(shouldDisplay(gdn_item.handle_d)){
        doc.text('D - Handle D');
      }
      if(shouldDisplay(gdn_item.handle_e)){
        doc.text('E - Handle E');
      }
    }
    doc.text(' ', 350,itemYStart, {align : 'left'});
    doc.fontSize(smallFontSize);
	
	if(shouldDisplay (gdn_item.location)){
      gdn_item.location = gdn_item.location.replace(/-/g, " ");
      doc.text(gdn_item.location);
    }
    if(shouldDisplay (gdn_item.glass_width)){
      gdn_item.glass_width = gdn_item.glass_width.replace(/-/g, " ");
      var txt = gdn_item.glass_width;
      var x = '"';
      txt = txt+x;
      doc.text(txt);
    }
    if(shouldDisplay (gdn_item.glass_height)){
      gdn_item.glass_height =gdn_item.glass_height.replace(/-/g, " ");
      var txt = gdn_item.glass_height;
      var x = '"';
      txt = txt+x;
      doc.text(txt);
    }
    if(shouldDisplay (gdn_item.glass_thickness)){
      gdn_item.glass_thickness =gdn_item.glass_thickness.replace(/-/g, " ");
      doc.text(gdn_item.glass_thickness);
    }
    if(shouldDisplay (gdn_item.glass_color)){
      gdn_item.glass_color =gdn_item.glass_color.replace(/-/g, " ");
      doc.text(gdn_item.glass_color);
    }
    if(shouldDisplay (gdn_item.overall_door_height)){
      gdn_item.overall_door_height =gdn_item.overall_door_height.replace(/-/g, " ");
      var txt = gdn_item.overall_door_height;
      var x = '"';
      txt = txt+x;
      doc.text(txt);
    }
	
	if(shouldDisplay (gdn_item.top_inserts)){
      gdn_item.top_inserts =gdn_item.top_inserts.replace(/-/g, " ");
      doc.text(gdn_item.top_inserts);
    }
	
    if(shouldDisplay (gdn_item.daylite)){
      gdn_item.daylite =gdn_item.daylite.replace(/-/g, " ");
      doc.text(gdn_item.daylite);
    }
    if(shouldDisplay (gdn_item.top_shoe_size)){
      gdn_item.top_shoe_size =gdn_item.top_shoe_size.replace(/-/g, " ");
      doc.text(gdn_item.top_shoe_size);
    }
    if(shouldDisplay (gdn_item.top_shoe_pocket)){
      gdn_item.top_shoe_pocket =gdn_item.top_shoe_pocket.replace(/-/g, " ");
      doc.text(gdn_item.top_shoe_pocket);
    }
    if(shouldDisplay (gdn_item.bottom_shoe_size)){
      gdn_item.bottom_shoe_size =gdn_item.bottom_shoe_size.replace(/-/g, " ");
      doc.text(gdn_item.bottom_shoe_size);
    }
    if(shouldDisplay (gdn_item.bottom_shoe_pocket)){
      gdn_item.bottom_shoe_pocket =gdn_item.bottom_shoe_pocket.replace(/-/g, " ");
      doc.text(gdn_item.bottom_shoe_pocket);
    }
    if(shouldDisplay (gdn_item.door_opening)){
      gdn_item.door_opening=gdn_item.door_opening.replace(/-/g, " ");
    	doc.text(gdn_item.door_opening);
    }
    if(shouldDisplay (gdn_item.type_of_set)){
    	gdn_item.type_of_set=gdn_item.type_of_set.replace(/-/g, " ");
      doc.text(gdn_item.type_of_set);
    }
    if(shouldDisplay (gdn_item.type_of_handle)){
    	gdn_item.type_of_handle=gdn_item.type_of_handle.replace(/-/g, " ");
      doc.text(gdn_item.type_of_handle);
    }
    if(shouldDisplay (gdn_item.hole_location)){
    	gdn_item.hole_location=gdn_item.hole_location.replace(/-/g, " ");
      doc.text(gdn_item.hole_location);
    }
    if(shouldDisplay (gdn_item.hole_size)){
    	gdn_item.hole_size=gdn_item.hole_size.replace(/-/g, " ");
      doc.text(gdn_item.hole_size);
    }
    if(shouldDisplay (gdn_item.patch_fitting_manufacturer)){
    	gdn_item.patch_fitting_manufacturer=gdn_item.patch_fitting_manufacturer.replace(/-/g, " ");
      doc.text(gdn_item.patch_fitting_manufacturer);
    }
    if(shouldDisplay (gdn_item.bottom_patch_lock_manufacturer)){
    	gdn_item.bottom_patch_lock_manufacturer=gdn_item.bottom_patch_lock_manufacturer.replace(/-/g, " ");
    	doc.text(gdn_item.bottom_patch_lock_manufacturer);
    }
    if(shouldDisplay (gdn_item.lock_notch_height)){
    	gdn_item.lock_notch_height=gdn_item.lock_notch_height.replace(/-/g, " ");
    	doc.text(gdn_item.lock_notch_height);
    }
    if(shouldDisplay (gdn_item.lock_notch_width)){
    	gdn_item.lock_notch_width=gdn_item.lock_notch_width.replace(/-/g, " ");
      doc.text(gdn_item.lock_notch_width);
    }
    if(shouldDisplay (gdn_item.side_rail_size)){
    	gdn_item.side_rail_size=gdn_item.side_rail_size.replace(/-/g, " ");
      doc.text(gdn_item.side_rail_size);
    }
    if( shouldDisplay(gdn_item.side_rail_pocket )){
    	gdn_item.side_rail_pocket=gdn_item.side_rail_pocket.replace(/-/g, " ");
      doc.text(gdn_item.side_rail_pocket);
    }
    if(shouldDisplay(gdn_item.notes)){
      addLines(2, doc);
      gdn_item.notes=gdn_item.notes.replace(/-/g, " ");
      doc.text(gdn_item.notes);
    }
    continueAfterHandleImage = function(){
      if(shouldDisplay(gdn_item.handle_type)){
        doc.text(humanize(gdn_item.handle_type),350,handletype);
        if(shouldDisplay(gdn_item.handle_a)){
          gdn_item.handle_a=gdn_item.handle_a.replace(/-/g, " ");
          doc.text(gdn_item.handle_a);
        }
        if(shouldDisplay(gdn_item.handle_b)){
          gdn_item.handle_b=gdn_item.handle_b.replace(/-/g, " ");
          doc.text(gdn_item.handle_b);
        }
        if(shouldDisplay(gdn_item.handle_c)){
          gdn_item.handle_c=gdn_item.handle_c.replace(/-/g, " ");
          doc.text(gdn_item.handle_c);
        }
        if(shouldDisplay(gdn_item.handle_d)){
          gdn_item.handle_d=gdn_item.handle_d.replace(/-/g, " ");
          doc.text(gdn_item.handle_d);
        }
        if(shouldDisplay(gdn_item.handle_e)){
          gdn_item.handle_e=gdn_item.handle_e.replace(/-/g, " ");
          doc.text(gdn_item.handle_e);
        }
      }
materialsXStart = doc.x;
              materialsYStart = doc.y;

              //console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);

              doc.text(' ',doc.x ,doc.y,itemYStart, {align : 'left'});
              
              

              doc.text('GLASS REMINDERS:',materialsXStart-145 , materialsYStart, {align : 'left'}); 

                      
            setFont(bodyColor,smallFontSize, doc);
            
            doc.fontSize(2);
            doc.text(' ', {align : 'left'});
            doc.fontSize(smallFontSize)
            
            //console.log('gdn_item',gdn_item);
            //console.log('doc.y',doc.y);

            if(shouldDisplay(gdn_item.solar_film)){
              doc.text('Solar Film:');
              doc.y-=12;
              doc.x+=150; 
              doc.text(titleCase(boolToHuman(gdn_item.solar_film) ));
              doc.x=10;
            }

          
           if(shouldDisplay(gdn_item.solar_film_responsibility)){

              doc.y+=12;
              doc.x+=195;

              doc.text('Solar Film Responsibility:');
              doc.y-=12;
              doc.x+=155;
             // console.log('doc.x',doc.x);
           // console.log('doc.y',doc.y);
              gdn_item.solar_film_responsibility=gdn_item.solar_film_responsibility.replace(/_/g, " ");   
              doc.text(titleCase(gdn_item.solar_film_responsibility) );
              doc.x=10;
            }
            if(shouldDisplay(gdn_item.solar_film_type)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Type:');
              doc.y-=12;
              doc.x+=155;
              gdn_item.solar_film_type=gdn_item.solar_film_type.replace(/\\/g, '');
              gdn_item.solar_film_type=gdn_item.solar_film_type.replace(/'/g, '');
              gdn_item.solar_film_type=gdn_item.solar_film_type.replace(/\//g, "");
              gdn_item.solar_film_type=gdn_item.solar_film_type.replace(/_/g, " ");
              doc.text(titleCase(gdn_item.solar_film_type));
              doc.x=10;
            }
            if(shouldDisplay(gdn_item.solar_film_source)){
              doc.y+=12;
              doc.x+=195;
              doc.text('Solar Film Source:');
              doc.y-=12;
              doc.x+=155;
              gdn_item.solar_film_source=gdn_item.solar_film_source.replace(/\\/g, '');
              gdn_item.solar_film_source=gdn_item.solar_film_source.replace(/'/g, '');
              gdn_item.solar_film_source=gdn_item.solar_film_source.replace(/\//g, "");
              gdn_item.solar_film_source=gdn_item.solar_film_source.replace(/_/g, " ");             
              doc.text( titleCase(gdn_item.solar_film_source) );
              doc.x=10;
            }
            if(shouldDisplay(gdn_item.wet_seal)){
              doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal:');
              doc.y-=12;
              doc.x+=155; 
              gdn_item.wet_seal=gdn_item.wet_seal.replace(/_/g, " ");
              doc.text( titleCase( boolToHuman(gdn_item.wet_seal) ) );
              doc.x=10;
            }
            if(shouldDisplay(gdn_item.wet_seal_responsibility)){
                doc.y+=12;
              doc.x+=195; 
              doc.text('Wet Seal Responsibility:');
              doc.y-=12;
              doc.x+=155; 
              gdn_item.wet_seal_responsibility=gdn_item.wet_seal_responsibility.replace(/_/g, " ");
              doc.text( titleCase(gdn_item.wet_seal_responsibility) );
              doc.x=10;
            }

            if(shouldDisplay(gdn_item.furniture_to_move)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Furniture to Move:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdn_item.furniture_to_move=gdn_item.furniture_to_move.replace(/_/g, " ");
                  doc.text(boolToHuman(gdn_item.furniture_to_move));
                  doc.x=10;
                }
                 if(shouldDisplay(gdn_item.furniture_to_move_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdn_item.furniture_to_move_comment=gdn_item.furniture_to_move_comment.replace(/_/g, " ");
                  gdn_item.furniture_to_move_comment=gdn_item.furniture_to_move_comment.replace(/\\/g, '');
                  gdn_item.furniture_to_move_comment=gdn_item.furniture_to_move_comment.replace(/'/g, '');
                  gdn_item.furniture_to_move_comment=gdn_item.furniture_to_move_comment.replace(/\//g, "");
                  doc.text(gdn_item.furniture_to_move_comment);
                  doc.x=10;
                }

                if(shouldDisplay(gdn_item.walls_or_ceilings_to_cut)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Walls/Ceilings Cut:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdn_item.walls_or_ceilings_to_cut=gdn_item.walls_or_ceilings_to_cut.replace(/_/g, " ");
                  gdn_item.walls_or_ceilings_to_cut=gdn_item.walls_or_ceilings_to_cut.replace(/\\/g, '');
                  gdn_item.walls_or_ceilings_to_cut=gdn_item.walls_or_ceilings_to_cut.replace(/'/g, '');
                  gdn_item.walls_or_ceilings_to_cut=gdn_item.walls_or_ceilings_to_cut.replace(/\//g, "");
                  doc.text( boolToHuman(gdn_item.walls_or_ceilings_to_cut));
                  doc.x=10;
                }
                if(shouldDisplay(gdn_item.walls_or_ceilings_to_cut_responsibility)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Responsibility:');
                  doc.y-=12;
                  doc.x+=155; 
                  gdn_item.walls_or_ceilings_to_cut_responsibility=gdn_item.walls_or_ceilings_to_cut_responsibility.replace(/_/g, " ");
                  doc.text(gdn_item.walls_or_ceilings_to_cut_responsibility);
                  doc.x=10;
                }
                if(shouldDisplay(gdn_item.walls_or_ceilings_to_cut_comment)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text("Walls/Ceiling Comment");
                  doc.y-=12;
                  doc.x+=155;
                  gdn_item.walls_or_ceilings_to_cut_comment=gdn_item.walls_or_ceilings_to_cut_comment.replace(/_/g, " ");
                  gdn_item.walls_or_ceilings_to_cut_comment=gdn_item.walls_or_ceilings_to_cut_comment.replace(/\\/g, '');
                  gdn_item.walls_or_ceilings_to_cut_comment=gdn_item.walls_or_ceilings_to_cut_comment.replace(/'/g, '');
                  gdn_item.walls_or_ceilings_to_cut_comment=gdn_item.walls_or_ceilings_to_cut_comment.replace(/\//g, "");
                  doc.text(gdn_item.walls_or_ceilings_to_cut_comment);
                  doc.x=10;
                }
                if(shouldDisplay(gdn_item.blind_needs_removing)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Blinds Need Removing:' );
                  doc.y-=12;
                  doc.x+=155;
                  gdn_item.blind_needs_removing=gdn_item.blind_needs_removing.replace(/_/g, " ");
                  doc.text(boolToHuman(gdn_item.blind_needs_removing));
                  doc.x=10;
                }
                if(shouldDisplay(gdn_item.glass_fits_elevator)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Glass Fits Elevator:');
                  doc.y-=12;
                  doc.x+=155;
                  gdn_item.glass_fits_elevator=gdn_item.glass_fits_elevator.replace(/_/g, " ");
                  doc.text(boolToHuman(gdn_item.glass_fits_elevator));
                  doc.x=10;
                }
                if(shouldDisplay(gdn_item.color_viewer)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Color Waiver:');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(gdn_item.color_viewer));
                  doc.x=10;
                }

                if(shouldDisplay(gdn_item.damage_viewer)){

                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Damage Waiver :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(gdn_item.damage_viewer));
                  doc.x=10;
                } 

                if(shouldDisplay(gdn_item.damage_waiver_text) ||shouldDisplay(gdn_item.damage_waiver_select)){
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=14;
                  doc.x+=155;
                  gdn_item.damage_waiver_select = gdn_item.damage_waiver_select.replace(/_/g, " ");
                  if(gdn_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || gdn_item.damage_waiver_select =='Handling of Customers Materials' ||gdn_item.damage_waiver_select =='Adjacent Glass'){
                      doc.text(gdn_item.damage_waiver_select);
                  }else{
                  
                  if(gdn_item.damage_waiver_select =='select tgpd glass damage waiver for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(gdn_item.damage_waiver_select);
                  }
                  doc.text(gdn_item.damage_waiver_text);
                  doc.x=10;
                    } 
                  }

                  if(gdn_item.disclamers=='1'){
                if(shouldDisplay(gdn_item.disclamers)){
                    if(gdn_item.damage_waiver_select =='Removal/Reinstall of Glass Currently Installed' || gdn_item.damage_waiver_select =='Handling of Customers Materials' ||gdn_item.damage_waiver_select =='Adjacent Glass'){
                        //doc.x-=155;
                    }

                    alert(doc.x);
                      doc.x-=155;
                      doc.y+=12;
                  doc.text('Disclamers :');
                  doc.y-=12;
                  doc.x+=155;
                  doc.text(boolToHuman(gdn_item.disclamers));
                  doc.x=10;
                } 
                
              
                if(shouldDisplay(gdn_item.disclamers_text) || shouldDisplay(gdn_item.disclamers_select)){
                  
                  doc.y+=12;
                  doc.x+=195;
                  doc.text('Comment :');
                  doc.y-=12;
                  doc.x+=155;
                  gdn_item.disclamers_select = gdn_item.disclamers_select.replace(/_/g, " ");
                  if(gdn_item.disclamers_select == 'Wood Bead' || gdn_item.disclamers_select == 'Painted Frames (AFTERMARKET)' || gdn_item.disclamers_select == 'TBD'){
                      doc.text(gdn_item.disclamers_select);
                  }else{
                  if(gdn_item.disclamers_select == 'select tgpd glass disclamers for text'){
                      doc.text(' ');
                      doc.y-=8;
                  }else{
                  doc.text(gdn_item.disclamers_select);
                  }
                  doc.text(gdn_item.disclamers_text);
                  doc.x=10;
                } 
              }
               }


              if(gdn_item.text_tgpd_text_lift_inside_position == '1'){
                doc.y+=12;
                doc.x-=155;
                doc.text('Lift Inside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(gdn_item.text_tgpd_text_lift_inside_position));
                doc.x=10;
              } 

              if(shouldDisplay(gdn_item.tgpd_glass_inside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(gdn_item.tgpd_glass_inside_lift_with_glass_type);
                doc.x=10;
              } 


              if(gdn_item.text_tgpd_text_lift_outside_position == '1'){
                doc.y+=12;
                doc.x+=195;
                doc.text('Lift Outside :');
                doc.y-=12;
                doc.x+=155;
                doc.text(boolToHuman(gdn_item.text_tgpd_text_lift_outside_position));
                doc.x=10;
              } 

              if(shouldDisplay(gdn_item.tgpd_glass_outside_lift_with_glass_type)){
                doc.y+=12;
                doc.x+=195;
                doc.text('Comment :');
                doc.y-=12;
                doc.x+=155;
                doc.text(gdn_item.tgpd_glass_outside_lift_with_glass_type);
                doc.x=10;
              } 


              var flag_a=false;var flag_b=false; var flag_c=false; 
          if(shouldDisplay(gdn_item.tgpd_tgpd_caulk_amount) || shouldDisplay(gdn_item.tgpd_caulk_type)){
              doc.moveDown();
              flag_a=true;

              var caulkAmt = JSON.parse(gdn_item.tgpd_caulk_amount);        
              var caulkTyp = JSON.parse(gdn_item.tgpd_caulk_type);
              doc.x=30;

              doc.y+=12;
                doc.x+=175;
              doc.text('GLASS Materials:');
              
              doc.moveDown();
              doc.text('Caulk :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<caulkAmt.length; i++){
                if(shouldDisplay(caulkAmt[i])){
                  doc.text(caulkAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(caulkTyp[i])){
                doc.text(caulkTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }
            }

            var tapeAmt = JSON.parse(gdn_item.tgpd_tape_amount);        
              var tapekTyp = JSON.parse(gdn_item.tgpd_tape_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('TAPE :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<tapeAmt.length; i++){
                if(shouldDisplay(tapeAmt[i])){
                  doc.text(tapeAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(tapekTyp[i])){
                doc.text(tapekTyp[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }


              var channelAmt = JSON.parse(gdn_item.tgpd_quantity_channel);        
              var channeltype = JSON.parse(gdn_item.tgpd_channel);
              doc.x=30;

                        
              doc.moveDown();
              doc.y+=12;
                doc.x+=175;
              doc.text('CHANNEL :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<channelAmt.length; i++){
                if(shouldDisplay(channelAmt[i])){
                  doc.text(channelAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(channeltype[i])){
                doc.text(channeltype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }

              var eqAmt = JSON.parse(gdn_item.tgpd_quantity_type);        
              var eqtype = JSON.parse(gdn_item.tgpd_scaffolding_type);
              doc.x=30;

                        
              doc.moveDown();
               doc.y+=12;
                doc.x+=175;
              doc.text('EQUIPMENT :');
              doc.x+=155;
              doc.y-=12;
              for(var i=0; i<eqAmt.length; i++){
                if(shouldDisplay(eqAmt[i])){
                  doc.text(eqAmt[i].replace(/\\/g,''));
                }else{
                doc.text('-');
                }
                  doc.x+=30;
                  doc.y-=12;
                if(shouldDisplay(eqtype[i])){
                doc.text(eqtype[i].replace(/_/g,' '));
                }else{
                doc.text('-');
                }
                  doc.x-=30;
                
              }




      if(gdnIndex < gdnLength){ //there are more gdn items
        if(gdnIndex != 0 ){ // not last item & not first item - already has new page
          doc.addPage();
        }
        console.log('we are on the ' +  gdnIndex  +  ' job_item["agd_items"]["gdn"]  right now');
        gdn_item = job_item["agd_items"]["gdn"][gdnIndex];
        console.log("--------------------------------------------")
        console.log("GDN ITEM: " + JSON.stringify(job_item["agd_items"]["gdn"][gdnIndex]) )
        console.log("--------------------------------------------")
        addPhotosFromProjectItem(gdn_item, ["location","type", "subtype"]); // addPhotosFromProjectItems defined inside pdf/general/methods.js
        console.log('inside gdn.length loop and ' + " gdnIndex is " + gdnIndex);
        var curPiece = gdnIndex + 1 ;
        doc.fillColor(primaryColor);
        doc.text("Door " + curPiece+ "-" + gdn_item.type + " " + gdn_item.subtype ,20,110, {underline: true});
        doc.moveDown();
        var itemXStart = doc.x;
        var itemYStart = doc.y;
        doc.fillColor(bodyColor);
        imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["gdn"][gdn_item.type]}],gdnPhotoCallback, gdnPhotoFailCallback, doc, 150, false);
      } else{
        callback();
      }
    }
    //add handle image
    if(shouldDisplay(gdn_item.handle_type)){
      imagesAjaxRequests([{url: DOOR_HANDLE_PHOTOS[gdn_item.handle_type]}],continueAfterHandleImage, gdnPhotoFailCallback, doc, 150, false, MARGIN_LEFT_RIGHT  , 390 ); //add logo to header
    } else{
      continueAfterHandleImage();
    }
  }
  //--------pull hardcoded door diagram from amazon s3------
  imagesAjaxRequests([{url: DOOR_REPAIR_PHOTOS["gdn"][gdn_item.type]}],gdnPhotoCallback, gdnPhotoFailCallback, doc, 150, false);

  }
    // `id`
    // `job_id`
    // `category`
    // `type`
    // `subtype`
    // `notes`
    // `glass_width`
    // `glass_height`
    // `glass_thickness`
    // `glass_color`
    // `overall_door_height`
    // `daylite`
    // `top_shoe_size`
    // `top_shoe_pocket`
    // `bottom_shoe_size`
    // `bottom_shoe_pocket`
    // `door_opening`
    // `type_of_set`
    // `type_of_handle`
    // `hole_location`
    // `hole_size`
    // `patch_fitting_manufacturer`
    // `bottom_patch_lock_manufacturer`
    // `lock_notch_height`
    // `lock_notch_width`
    // `side_rail_size`
    // `side_rail_pocket`
    // `pictures_download_url`
    // `sketches_download_url`
    // `handle_a`
    // `handle_b`
    // `handle_c`
    // `handle_d`
    // `handle_e`

