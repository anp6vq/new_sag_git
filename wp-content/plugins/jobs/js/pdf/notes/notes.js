var notes_items;
addNotesItemsToPdf = function(callback){
  var notes_items = job_item.notes_items;
  checkNotesUrl = function(pictureItem, stringKey){
    return pictureItem && pictureItem[stringKey] &&  pictureItem[stringKey].length > 1 && pictureItem[stringKey] !=null && pictureItem[stringKey] != 'null' && pictureItem[stringKey] != 'NULL'
  }
  if(job_item["notes_items"].length > 0){

  if(checkNotesUrl(notes_items[0], "notes_upload_pic") ){
    var notesUrlsArr =  notes_items[0].notes_upload_pic.split(',');
    console.log("notesUrlArr.length is " + notesUrlsArr.length)
    for(var j=0; j< notesUrlsArr.length; j++){
      var notesPhotoObject = {
        url    : notesUrlsArr[j],
        header : "Notes"
      }
	  if(notesUrlsArr[j]!=''){
      JOB_PDF_IMAGES.push(notesPhotoObject); //JOB_PDF_IMAGES is variable defined in pdf/variables.js
	  }
    }
  }
  if(checkNotesUrl(notes_items[0], "notes_sketch_diagram") ){
    var notesUrlsArrNew =  notes_items[0].notes_sketch_diagram.split(',');
    console.log("notesUrlsArrNew.length is " + notesUrlsArrNew.length)
    for(var j=0; j< notesUrlsArrNew.length; j++){
      var notesSketchObject = {
        url    : notesUrlsArrNew[j],
        header : "Notes"
      }
	  if(notesUrlsArrNew[j]!=''){
      	JOB_PDF_IMAGES.push(notesSketchObject); //JOB_PDF_IMAGES is variable defined in pdf/variables.js
	  }
    }
  }
  callback(); //add all the photos and callback
  } else{
    //there are no notes photos or sketches at all yo!
    callback(); //move onto next round.
  }

}