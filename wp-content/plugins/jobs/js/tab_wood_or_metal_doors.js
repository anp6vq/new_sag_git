var twmd_items = [];
var twmd_current_item = -1;
var twmd_editing = false;
var twmd_copying = false;
var twmd_clipboard = {};
// perhaps also map value to text - for saving to db
var twmd_map_door_hand_value_to_index = {
   lh : 0,
   lhr : 1,
   rh : 2,
   rhr : 3
};

function twmd_set_toolbar_navigation_item_count() {
   if (twmd_editing) {
      jQuery('#text_twmd_item_count').val('Add Item');
   } else {
      jQuery('#text_twmd_item_count').val((twmd_current_item + 1) + '/' + twmd_items.length);
   }
}

function twmd_init_form_state() {
   jQuery('#text_twmd_door_type_veneer').attr('disabled','disabled');
   jQuery('#text_twmd_door_type_other').attr('disabled','disabled');
   jQuery('#text_twmd_lite_kit_other').attr('disabled','disabled');
   jQuery('#text_twmd_louver_size_other').attr('disabled','disabled');
   jQuery('#text_twmd_hardware_hinges_type').attr('disabled','disabled');
   jQuery('#text_twmd_hardware_lock_set_function').attr('disabled','disabled');
   jQuery('#text_twmd_hardware_panic_bar_trim').attr('disabled','disabled');
   jQuery('#text_twmd_hardware_finish_other').attr('disabled','disabled');
}

function twmd_init_toolbar_navigation_buttons_state() {
   jQuery('#btn_twmd_first').attr('disabled','disabled');
   jQuery('#btn_twmd_previous').attr('disabled','disabled');
   jQuery('#text_twmd_item_count').attr('readonly','readonly');
   jQuery('#btn_twmd_next').attr('disabled','disabled');
   jQuery('#btn_twmd_last').attr('disabled','disabled');
   twmd_set_toolbar_navigation_item_count();
}

function twmd_init_toolbar_editing_buttons_state() {
   jQuery('#btn_twmd_add').removeAttr('disabled');
   jQuery('#btn_twmd_save').attr('disabled','disabled');
   jQuery('#btn_twmd_delete').attr('disabled','disabled');
   jQuery('#btn_twmd_refresh').attr('disabled','disabled');
   twmd_init_toolbar_copy_paste_buttons_state();
}

function twmd_init_toolbar_copy_paste_buttons_state() {
   jQuery('#btn_twmd_copy').attr('disabled','disabled');
   jQuery('#btn_twmd_paste').attr('disabled','disabled');
}

function twmd_set_toolbar_navigation_buttons_state() {
   if (twmd_current_item == -1 || twmd_items.length == 0 || twmd_editing) {
      twmd_init_toolbar_navigation_buttons_state();
      return;
   }
   if (twmd_items.length == 1) {
      jQuery('#btn_twmd_first').attr('disabled','disabled');
      jQuery('#btn_twmd_previous').attr('disabled','disabled');
      jQuery('#btn_twmd_next').attr('disabled','disabled');
      jQuery('#btn_twmd_last').attr('disabled','disabled');
   } else {
      if (twmd_current_item == 0) {
         jQuery('#btn_twmd_first').attr('disabled','disabled');
         jQuery('#btn_twmd_previous').attr('disabled','disabled');
         jQuery('#btn_twmd_next').removeAttr('disabled');
         jQuery('#btn_twmd_last').removeAttr('disabled');
      } else if (twmd_current_item == (twmd_items.length - 1)) {
         jQuery('#btn_twmd_first').removeAttr('disabled');
         jQuery('#btn_twmd_previous').removeAttr('disabled');
         jQuery('#btn_twmd_next').attr('disabled','disabled');
         jQuery('#btn_twmd_last').attr('disabled','disabled');
      } else {
         jQuery('#btn_twmd_first').removeAttr('disabled');
         jQuery('#btn_twmd_previous').removeAttr('disabled');
         jQuery('#btn_twmd_next').removeAttr('disabled');
         jQuery('#btn_twmd_last').removeAttr('disabled');
      }
   }
   twmd_set_toolbar_navigation_item_count();
}

function twmd_set_toolbar_editing_buttons_state() {
   if ((!twmd_editing) && (!twmd_copying) && (!(twmd_current_item >= 0 && twmd_current_item < twmd_items.length))) {
      twmd_init_toolbar_editing_buttons_state();
      jQuery('#btn_submit_new_job').removeAttr('disabled');
      return;
   }
   if (twmd_editing) {
      jQuery('#btn_twmd_add').attr('disabled','disabled');
      jQuery('#btn_twmd_save').removeAttr('disabled');
      jQuery('#btn_twmd_delete').removeAttr('disabled');
      jQuery('#btn_twmd_refresh').attr('disabled','disabled');
      jQuery('#btn_submit_new_job').attr('disabled','disabled');
   } else {
      jQuery('#btn_twmd_add').removeAttr('disabled');
      if (twmd_current_item >= 0 && twmd_current_item < twmd_items.length) {
         jQuery('#btn_twmd_save').removeAttr('disabled');
         jQuery('#btn_twmd_delete').removeAttr('disabled');
         jQuery('#btn_twmd_refresh').removeAttr('disabled');
      }
      jQuery('#btn_submit_new_job').removeAttr('disabled');
   }
   twmd_set_toolbar_copy_paste_buttons_state();
}

function twmd_set_toolbar_copy_paste_buttons_state() {
   if (twmd_editing) {
      jQuery('#btn_twmd_copy').attr('disabled','disabled');
      if (twmd_copying) {
         jQuery('#btn_twmd_paste').removeAttr('disabled');
      } else {
         jQuery('#btn_twmd_paste').attr('disabled','disabled');
      }
   } else {
      if (twmd_current_item >= 0 && twmd_current_item < twmd_items.length) {
         jQuery('#btn_twmd_copy').removeAttr('disabled');
         if (twmd_copying) {
            jQuery('#btn_twmd_paste').removeAttr('disabled');
         } else {
            jQuery('#btn_twmd_paste').attr('disabled','disabled');
         }
      }
   }
}

function twmd_clear_form() {
   jQuery('input:radio[name=radio_twmd_door_type]').val(['not_a_value']);
   jQuery('#text_twmd_door_type_veneer').val('');
   twmd_set_door_type_veneer_state_by_door_type('not_a_value', false);
   jQuery('#text_twmd_door_type_other').val('');
   twmd_set_door_type_other_state_by_door_type('not_a_value', false);
   jQuery('#text_twmd_dimension_of_framewidth').val('');
   jQuery('#text_twmd_actual_door_height').val('');
   jQuery('#text_twmd_head_to_hinge_top_1').val('');
   jQuery('#text_twmd_head_to_hinge_top_2').val('');
   jQuery('#text_twmd_head_to_hinge_top_3').val('');
   jQuery('#text_twmd_head_to_frame_lock').val('');
   jQuery('#text_twmd_head_to_dead_bolt').val('');
   jQuery('#text_twmd_lock_backset').val('');
   jQuery('#text_twmd_door_thickness').val('');
   jQuery('#text_twmd_hinge_height').val('');
   jQuery('#radio_group_twmd_door_hand > .btn').removeClass('active');
   jQuery('#radio_group_twmd_door_hand > .btn').eq(0).addClass('active');
   jQuery('#select_twmd_fire_rating').val(['not_a_value']);
   jQuery('#radio_twmd_lite_kit').val(['not_a_value']);
   jQuery('#text_twmd_lite_kit_other').val('');
   twmd_set_lite_kit_other_state_by_lite_kit('not_a_value', false);
   jQuery('input:radio[name=radio_twmd_louver_size]').val(['not_a_value']);
   jQuery('#text_twmd_louver_size_other').val('');
   twmd_set_louver_size_other_state_by_louver_size('not_a_value', false);
   jQuery('input:checkbox[name=checkbox_twmd_hardware]').val([]);
   jQuery('#text_twmd_hardware_hinges_type').val('');
   twmd_set_hardware_hinges_type_state(false, false);
   jQuery('#text_twmd_hardware_lock_set_function').val('');
   twmd_set_hardware_lock_set_function_state(false, false);
   jQuery('#text_twmd_hardware_panic_bar_trim').val('');
   twmd_set_hardware_panic_bar_trim_state(false, false);
   jQuery('input:radio[name=radio_twmd_hardware_finish]').val(['not_a_value']);
   jQuery('#text_twmd_hardware_finish_other').val('');
   twmd_set_hardware_finish_other_state_by_hardware_finish('not_a_value', false);
   jQuery('#textarea_twmd_additional_info').val('');
}

function twmd_set_door_type_veneer_state_by_door_type(twmd_door_type, twmd_focus) {
   var twmd_door_type_veneer = jQuery('#text_twmd_door_type_veneer');
   if (twmd_door_type.localeCompare('solid_core_wood') == 0) {
      twmd_door_type_veneer.removeAttr('disabled');
      if (twmd_focus) {
         twmd_door_type_veneer[0].focus();
      }
   } else {
      twmd_door_type_veneer.val('');
      twmd_door_type_veneer.attr('disabled','disabled');
   }
}

function twmd_set_door_type_other_state_by_door_type(twmd_door_type, twmd_focus) {
   var twmd_door_type_other = jQuery('#text_twmd_door_type_other');
   if (twmd_door_type.localeCompare('other') == 0) {
      twmd_door_type_other.removeAttr('disabled');
      if (twmd_focus) {
         twmd_door_type_other[0].focus();
      }
   } else {
      twmd_door_type_other.val('');
      twmd_door_type_other.attr('disabled','disabled');
   }
}

function twmd_set_lite_kit_other_state_by_lite_kit(twmd_lite_kit, twmd_focus) {
   var twmd_lite_kit_other = jQuery('#text_twmd_lite_kit_other');
   if (twmd_lite_kit.localeCompare('other') == 0) {
      twmd_lite_kit_other.removeAttr('disabled');
      if (twmd_focus) {
         twmd_lite_kit_other[0].focus();
      }
   } else {
      twmd_lite_kit_other.val('');
      twmd_lite_kit_other.attr('disabled','disabled');
   }
}

function twmd_set_louver_size_other_state_by_louver_size(twmd_louver_size, twmd_focus) {
   var twmd_louver_size_other = jQuery('#text_twmd_louver_size_other');
   if (twmd_louver_size.localeCompare('other') == 0) {
      twmd_louver_size_other.removeAttr('disabled');
      if (twmd_focus) {
         twmd_louver_size_other[0].focus();
      }
   } else {
      twmd_louver_size_other.val('');
      twmd_louver_size_other.attr('disabled','disabled');
   }
}

function twmd_set_hardware_hinges_type_state(twmd_checked, twmd_focus) {
   var twmd_hardware_hinges_type = jQuery('#text_twmd_hardware_hinges_type');
   if (twmd_checked) {
      twmd_hardware_hinges_type.removeAttr('disabled');
      if (twmd_focus) {
         twmd_hardware_hinges_type[0].focus();
      }
   } else {
      twmd_hardware_hinges_type.val('');
      twmd_hardware_hinges_type.attr('disabled','disabled');
   }
}

function twmd_set_hardware_lock_set_function_state(twmd_checked, twmd_focus) {
   var twmd_hardware_lock_set_function = jQuery('#text_twmd_hardware_lock_set_function');
   if (twmd_checked) {
      twmd_hardware_lock_set_function.removeAttr('disabled');
      if (twmd_focus) {
         twmd_hardware_lock_set_function[0].focus();
      }
   } else {
      twmd_hardware_lock_set_function.val('');
      twmd_hardware_lock_set_function.attr('disabled','disabled');
   }
}

function twmd_set_hardware_panic_bar_trim_state(twmd_checked, twmd_focus) {
   var twmd_hardware_panic_bar_trim = jQuery('#text_twmd_hardware_panic_bar_trim');
   if (twmd_checked) {
      twmd_hardware_panic_bar_trim.removeAttr('disabled');
      if (twmd_focus) {
         twmd_hardware_panic_bar_trim[0].focus();
      }
   } else {
      twmd_hardware_panic_bar_trim.val('');
      twmd_hardware_panic_bar_trim.attr('disabled','disabled');
   }
}

function twmd_set_hardware_finish_other_state_by_hardware_finish(twmd_hardware_finish, twmd_focus) {
   var twmd_hardware_finish_other = jQuery('#text_twmd_hardware_finish_other');
   if (twmd_hardware_finish.localeCompare('other') == 0) {
      twmd_hardware_finish_other.removeAttr('disabled');
      if (twmd_focus) {
         twmd_hardware_finish_other[0].focus();
      }
   } else {
      twmd_hardware_finish_other.val('');
      twmd_hardware_finish_other.attr('disabled','disabled');
   }
}

function twmd_show_current_item() {
   twmd_clear_form();
   var twmd_item = twmd_items[twmd_current_item];
   twmd_print_item_to_form(twmd_item);
}

function twmd_get_item_from_form() {
   var twmd_door_type = jQuery('input:radio[name=radio_twmd_door_type]:checked').val();
   var twmd_door_type_veneer = '';
   if (twmd_door_type.localeCompare('solid_core_wood') == 0) {
      twmd_door_type_veneer = jQuery('#text_twmd_door_type_veneer').val();
   }
   var twmd_door_type_other = '';
   if (twmd_door_type.localeCompare('other') == 0) {
      twmd_door_type_other = jQuery('#text_twmd_door_type_other').val();
   }
   var twmd_dimension_of_framewidth = jQuery('#text_twmd_dimension_of_framewidth').val();
   var twmd_actual_door_height = jQuery('#text_twmd_actual_door_height').val();
   var twmd_head_to_hinge_top_1 = jQuery('#text_twmd_head_to_hinge_top_1').val();
   var twmd_head_to_hinge_top_2 = jQuery('#text_twmd_head_to_hinge_top_2').val();
   var twmd_head_to_hinge_top_3 = jQuery('#text_twmd_head_to_hinge_top_3').val();
   var twmd_head_to_frame_lock = jQuery('#text_twmd_head_to_frame_lock').val();
   var twmd_head_to_dead_bolt = jQuery('#text_twmd_head_to_dead_bolt').val();
   var twmd_lock_backset = jQuery('#text_twmd_lock_backset').val();
   var twmd_door_thickness = jQuery('#text_twmd_door_thickness').val();
   var twmd_hinge_height = jQuery('#text_twmd_hinge_height').val();
   var twmd_door_hand = jQuery('#radio_group_twmd_door_hand > .btn.active > input[type=radio]').val();
   var twmd_fire_rating = jQuery('#select_twmd_fire_rating').val();
   var twmd_lite_kit = jQuery('#radio_twmd_lite_kit:checked').val();
   var twmd_lite_kit_other = '';
   if (twmd_lite_kit.localeCompare('other') == 0) {
      twmd_lite_kit_other = jQuery('#text_twmd_lite_kit_other').val();
   }
   var twmd_louver_size = jQuery('input:radio[name=radio_twmd_louver_size]:checked').val();
   var twmd_louver_size_other = '';
   if (twmd_louver_size.localeCompare('other') == 0) {
      twmd_louver_size_other = jQuery('#text_twmd_louver_size_other').val();
   }
   var twmd_hardware = '';
   var twmd_hardware_objects = jQuery('input:checkbox[name=checkbox_twmd_hardware]:checked');
   twmd_hardware_objects.each(function(index) {
      twmd_hardware += jQuery(this).val();
      if (index < (twmd_hardware_objects.length - 1)) {
         twmd_hardware += '|';
      }
   });
   var twmd_hardware_hinges_type = '';
   if (!(twmd_hardware.indexOf('hinges') == -1)) {
      twmd_hardware_hinges_type = jQuery('#text_twmd_hardware_hinges_type').val();
   }
   var twmd_hardware_lock_set_function = '';
   if (!(twmd_hardware.indexOf('lock_set') == -1)) {
      twmd_hardware_lock_set_function = jQuery('#text_twmd_hardware_lock_set_function').val();
   }
   var twmd_hardware_panic_bar_trim = '';
   if (!(twmd_hardware.indexOf('panic_bar') == -1)) {
      twmd_hardware_panic_bar_trim = jQuery('#text_twmd_hardware_panic_bar_trim').val();
   }
   var twmd_hardware_finish = jQuery('input:radio[name=radio_twmd_hardware_finish]:checked').val();
   var twmd_hardware_finish_other = '';
   if (twmd_hardware_finish.localeCompare('other') == 0) {
      twmd_hardware_finish_other = jQuery('#text_twmd_hardware_finish_other').val();
   }
   var twmd_additional_info = jQuery('#textarea_twmd_additional_info').val();

   var twmd_item = {};
   twmd_item.id = 0;
   twmd_item.job_id = job_id;
   twmd_item.door_type = twmd_door_type;
   twmd_item.door_type_veneer = twmd_door_type_veneer;
   twmd_item.door_type_other = twmd_door_type_other;
   twmd_item.dimension_of_framewidth = twmd_dimension_of_framewidth;
   twmd_item.actual_door_height = twmd_actual_door_height;
   twmd_item.head_to_hinge_top_1 = twmd_head_to_hinge_top_1;
   twmd_item.head_to_hinge_top_2 = twmd_head_to_hinge_top_2;
   twmd_item.head_to_hinge_top_3 = twmd_head_to_hinge_top_3;
   twmd_item.head_to_frame_lock = twmd_head_to_frame_lock;
   twmd_item.head_to_dead_bolt = twmd_head_to_dead_bolt;
   twmd_item.lock_backset = twmd_lock_backset;
   twmd_item.door_thickness = twmd_door_thickness;
   twmd_item.hinge_height = twmd_hinge_height;
   twmd_item.door_hand = twmd_door_hand;
   twmd_item.fire_rating = twmd_fire_rating;
   twmd_item.lite_kit = twmd_lite_kit;
   twmd_item.lite_kit_other = twmd_lite_kit_other;
   twmd_item.louver_size = twmd_louver_size;
   twmd_item.louver_size_other = twmd_louver_size_other;
   twmd_item.hardware = twmd_hardware;
   twmd_item.hardware_hinges_type = twmd_hardware_hinges_type;
   twmd_item.hardware_lock_set_function = twmd_hardware_lock_set_function;
   twmd_item.hardware_panic_bar_trim = twmd_hardware_panic_bar_trim;
   twmd_item.hardware_finish = twmd_hardware_finish;
   twmd_item.hardware_finish_other = twmd_hardware_finish_other;
   twmd_item.additional_info = twmd_additional_info;

   twmd_print_item_to_console('twmd_get_item_from_form', twmd_item);

   return twmd_item;
}

function twmd_print_item_to_form(twmd_item) {
   jQuery('input:radio[name=radio_twmd_door_type]').val([twmd_item.door_type]);
   if (twmd_item.door_type.localeCompare('solid_core_wood') == 0) {
      jQuery('#text_twmd_door_type_veneer').val(twmd_item.door_type_veneer);
   }
   twmd_set_door_type_veneer_state_by_door_type(twmd_item.door_type, false);
   if (twmd_item.door_type.localeCompare('other') == 0) {
      jQuery('#text_twmd_door_type_other').val(twmd_item.door_type_other);
   }
   twmd_set_door_type_other_state_by_door_type(twmd_item.door_type, false);
   jQuery('#text_twmd_dimension_of_framewidth').val(twmd_item.dimension_of_framewidth);
   jQuery('#text_twmd_actual_door_height').val(twmd_item.actual_door_height);
   jQuery('#text_twmd_head_to_hinge_top_1').val(twmd_item.head_to_hinge_top_1);
   jQuery('#text_twmd_head_to_hinge_top_2').val(twmd_item.head_to_hinge_top_2);
   jQuery('#text_twmd_head_to_hinge_top_3').val(twmd_item.head_to_hinge_top_3);
   jQuery('#text_twmd_head_to_frame_lock').val(twmd_item.head_to_frame_lock);
   jQuery('#text_twmd_head_to_dead_bolt').val(twmd_item.head_to_dead_bolt);
   jQuery('#text_twmd_lock_backset').val(twmd_item.lock_backset);
   jQuery('#text_twmd_door_thickness').val(twmd_item.door_thickness);
   jQuery('#text_twmd_hinge_height').val(twmd_item.hinge_height);
   jQuery('#radio_group_twmd_door_hand > .btn').removeClass('active');
   jQuery('#radio_group_twmd_door_hand > .btn').eq(twmd_map_door_hand_value_to_index[twmd_item.door_hand]).addClass('active');
   jQuery('#select_twmd_fire_rating').val([twmd_item.fire_rating]);
   jQuery('#radio_twmd_lite_kit').val([twmd_item.lite_kit]);
   if (twmd_item.lite_kit.localeCompare('other') == 0) {
      jQuery('#text_twmd_lite_kit_other').val(twmd_item.lite_kit_other);
   }
   twmd_set_lite_kit_other_state_by_lite_kit(twmd_item.lite_kit, false);
   jQuery('input:radio[name=radio_twmd_louver_size]').val([twmd_item.louver_size]);
   if (twmd_item.louver_size.localeCompare('other') == 0) {
      jQuery('#text_twmd_louver_size_other').val(twmd_item.louver_size_other);
   }
   twmd_set_louver_size_other_state_by_louver_size(twmd_item.louver_size, false);
   jQuery('input:checkbox[name=checkbox_twmd_hardware]').val(twmd_item.hardware.indexOf("|") == -1 ? [twmd_item.hardware] : twmd_item.hardware.split("|"));
   if (!(twmd_item.hardware.indexOf('hinges') == -1)) {
      jQuery('#text_twmd_hardware_hinges_type').val(twmd_item.hardware_hinges_type);
      twmd_set_hardware_hinges_type_state(true, false);
   }
   if (!(twmd_item.hardware.indexOf('lock_set') == -1)) {
      jQuery('#text_twmd_hardware_lock_set_function').val(twmd_item.hardware_lock_set_function);
      twmd_set_hardware_lock_set_function_state(true, false);
   }
   if (!(twmd_item.hardware.indexOf('panic_bar') == -1)) {
      jQuery('#text_twmd_hardware_panic_bar_trim').val(twmd_item.hardware_panic_bar_trim);
      twmd_set_hardware_panic_bar_trim_state(true, false);
   }
   jQuery('input:radio[name=radio_twmd_hardware_finish]').val([twmd_item.hardware_finish]);
   if (twmd_item.hardware_finish.localeCompare('other') == 0) {
      jQuery('#text_twmd_hardware_finish_other').val(twmd_item.hardware_finish_other);
   }
   twmd_set_hardware_finish_other_state_by_hardware_finish(twmd_item.hardware_finish, false);
   jQuery('#textarea_twmd_additional_info').val(twmd_item.additional_info);
}

function twmd_print_item_to_console(debug_tag, twmd_item) {
   var log_tag = (debug_tag.length == 0 ? '' : (debug_tag + ': '));
   console.log(log_tag + 'twmd_id => ' + twmd_item.id);
   console.log(log_tag + 'twmd_job_id => ' + twmd_item.job_id);
   console.log(log_tag + 'twmd_door_type => ' + twmd_item.door_type);
   console.log(log_tag + 'twmd_door_type_veneer => ' + twmd_item.door_type_veneer);
   console.log(log_tag + 'twmd_door_type_other => ' + twmd_item.door_type_other);
   console.log(log_tag + 'twmd_dimension_of_framewidth => ' + twmd_item.dimension_of_framewidth);
   console.log(log_tag + 'twmd_actual_door_height => ' + twmd_item.actual_door_height);
   console.log(log_tag + 'twmd_head_to_hinge_top_1 => ' + twmd_item.head_to_hinge_top_1);
   console.log(log_tag + 'twmd_head_to_hinge_top_2 => ' + twmd_item.head_to_hinge_top_2);
   console.log(log_tag + 'twmd_head_to_hinge_top_3 => ' + twmd_item.head_to_hinge_top_3);
   console.log(log_tag + 'twmd_head_to_frame_lock => ' + twmd_item.head_to_frame_lock);
   console.log(log_tag + 'twmd_head_to_dead_bolt => ' + twmd_item.head_to_dead_bolt);
   console.log(log_tag + 'twmd_lock_backset => ' + twmd_item.lock_backset);
   console.log(log_tag + 'twmd_door_thickness => ' + twmd_item.door_thickness);
   console.log(log_tag + 'twmd_hinge_height => ' + twmd_item.hinge_height);
   console.log(log_tag + 'twmd_door_hand => ' + twmd_item.door_hand);
   console.log(log_tag + 'twmd_fire_rating => ' + twmd_item.fire_rating);
   console.log(log_tag + 'twmd_lite_kit => ' + twmd_item.lite_kit);
   console.log(log_tag + 'twmd_lite_kit_other => ' + twmd_item.lite_kit_other);
   console.log(log_tag + 'twmd_louver_size => ' + twmd_item.louver_size);
   console.log(log_tag + 'twmd_louver_size_other => ' + twmd_item.louver_size_other);
   console.log(log_tag + 'twmd_hardware => ' + twmd_item.hardware);
   console.log(log_tag + 'twmd_hardware_hinges_type => ' + twmd_item.hardware_hinges_type);
   console.log(log_tag + 'twmd_hardware_lock_set_function => ' + twmd_item.hardware_lock_set_function);
   console.log(log_tag + 'twmd_hardware_panic_bar_trim => ' + twmd_item.hardware_panic_bar_trim);
   console.log(log_tag + 'twmd_hardware_finish => ' + twmd_item.hardware_finish);
   console.log(log_tag + 'twmd_hardware_finish_other => ' + twmd_item.hardware_finish_other);
   console.log(log_tag + 'twmd_additional_info => ' + twmd_item.additional_info);
   // console.log(log_tag + 'twmd_item => ' + JSON.stringify(twmd_item));
}

function twmd_load_sample_data() {
   var twmd_items = [];

   for (var i=0; i<3; i++) {
      var twmd_item = {};
      twmd_item.id = 0;
      twmd_item.job_id = job_id;
      twmd_item.door_type = 'hollow_metal_18ga';
      twmd_item.door_type_veneer = '';
      twmd_item.door_type_other = '';
      twmd_item.dimension_of_framewidth = '30';
      twmd_item.actual_door_height = '83.12';
      twmd_item.head_to_hinge_top_1 = '5';
      twmd_item.head_to_hinge_top_2 = '7';
      twmd_item.head_to_hinge_top_3 = '6';
      twmd_item.head_to_frame_lock = '11.33';
      twmd_item.head_to_dead_bolt = '2.0755';
      twmd_item.lock_backset = '2.75';
      twmd_item.door_thickness = '1.75';
      twmd_item.hinge_height = '4.50';
      twmd_item.door_hand = 'lhr';
      twmd_item.fire_rating = '45_min';
      twmd_item.lite_kit = '10x10';
      twmd_item.lite_kit_other = '';
      twmd_item.louver_size = '24x24';
      twmd_item.louver_size_other = '';
      twmd_item.hardware = 'lock_set|dead_bolt|panic_bar|door_closer|weather_strip|drip_cap|wall_floor_stop';
      twmd_item.hardware_hinges_type = '';
      twmd_item.hardware_lock_set_function = 'lock_set_function';
      twmd_item.hardware_panic_bar_trim = 'panic_bar_trim';
      twmd_item.hardware_finish = 'us26d';
      twmd_item.hardware_finish_other = '';
      twmd_item.additional_info = 'Sample Job Item ' + (i + 1);
      twmd_items.push(twmd_item);
   }
   // console.log('twmd_load_sample_data:twmd_items => ' + JSON.stringify(twmd_items));
   return twmd_items;
}

function twmd_init(twmd_seed, twmd_seed_items) {
   twmd_current_item = -1;
   twmd_items = [];
   twmd_init_form_state();
   if (twmd_seed && twmd_seed_items.length > 0) {
      twmd_current_item = 0;
      twmd_items = twmd_seed_items;
      twmd_init_toolbar_navigation_buttons_state();
      twmd_init_toolbar_editing_buttons_state();
      twmd_show_current_item();
   }
   twmd_set_toolbar_navigation_buttons_state();
   twmd_editing = false;
   twmd_copying = false;
   twmd_clipboard = {};
   twmd_set_toolbar_editing_buttons_state();
}

jQuery(document).ready(function($) {
   $('input:radio[name=radio_twmd_door_type]').change(function(event) {
      var twmd_door_type = $(this).val();
      twmd_set_door_type_veneer_state_by_door_type(twmd_door_type, true);
      twmd_set_door_type_other_state_by_door_type(twmd_door_type, true);
   });
   $('#radio_twmd_lite_kit').change(function(event) {
      var twmd_lite_kit = $(this).val();
      twmd_set_lite_kit_other_state_by_lite_kit(twmd_lite_kit, true);
   });
   $('input:radio[name=radio_twmd_louver_size]').change(function(event) {
      var twmd_louver_size = $(this).val();
      twmd_set_louver_size_other_state_by_louver_size(twmd_louver_size, true);
   });
   $('input:checkbox[name=checkbox_twmd_hardware]').change(function(event) {
      var twmd_hardware = $(this).val();
      var twmd_checked = $(this).attr('checked');
      if (twmd_hardware.localeCompare('hinges') == 0) {
         twmd_set_hardware_hinges_type_state(twmd_checked, true);
      }
      if (twmd_hardware.localeCompare('lock_set') == 0) {
         twmd_set_hardware_lock_set_function_state(twmd_checked, true);
      }
      if (twmd_hardware.localeCompare('panic_bar') == 0) {
         twmd_set_hardware_panic_bar_trim_state(twmd_checked, true);
      }
   });
   $('input:radio[name=radio_twmd_hardware_finish]').change(function(event) {
      var twmd_hardware_finish = $(this).val();
      twmd_set_hardware_finish_other_state_by_hardware_finish(twmd_hardware_finish, true);
   });
   $('#btn_twmd_add').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_editing = true;
      twmd_clear_form();
      twmd_set_toolbar_navigation_buttons_state();
      twmd_set_toolbar_editing_buttons_state();
      // set focus to first textbox
   });
   $('#btn_twmd_save').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      // validation happens here...
      var twmd_item = twmd_get_item_from_form();
      if (twmd_editing) {
         twmd_editing = false;
         twmd_items.push(twmd_item);
         if (twmd_current_item == -1 || twmd_items.length == 1) {
            twmd_current_item = 0;
         }
         twmd_show_current_item();
         twmd_set_toolbar_navigation_buttons_state();
         twmd_set_toolbar_editing_buttons_state();
      } else {
         twmd_items[twmd_current_item] = twmd_item;
      }
      // set focus to first textbox
   });
   $('#btn_twmd_delete').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      if (twmd_editing) {
         twmd_editing = false;
         if (!(twmd_current_item == -1) && !(twmd_items.length == 0)) {
            twmd_show_current_item();
         }
         twmd_set_toolbar_navigation_buttons_state();
         twmd_set_toolbar_editing_buttons_state();
      } else {
         if (!confirm('Delete this record?')) return;
         twmd_items.splice(twmd_current_item, 1);
         if (twmd_items.length == 0) {
            twmd_current_item = -1;
         } else {
            if (twmd_current_item == twmd_items.length) {
               twmd_current_item = twmd_items.length - 1;
            }
         }
         if (!(twmd_current_item == -1) && !(twmd_items.length == 0)) {
            twmd_show_current_item();
         } else {
            twmd_clear_form();
         }
         twmd_set_toolbar_navigation_buttons_state();
         twmd_set_toolbar_editing_buttons_state();
      }
      // set focus to first textbox
   });
   $('#btn_twmd_refresh').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_show_current_item();
   });
   $('#btn_twmd_copy').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_copying = true;
      twmd_clipboard = twmd_get_item_from_form();
      twmd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_twmd_paste').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_print_item_to_form(twmd_clipboard);
      twmd_clipboard = {};
      twmd_copying = false;
      twmd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_twmd_first').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_current_item = 0;
      twmd_show_current_item();
      twmd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_twmd_previous').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_current_item--;
      twmd_show_current_item();
      twmd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_twmd_next').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_current_item++;
      twmd_show_current_item();
      twmd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_twmd_last').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twmd_current_item = twmd_items.length - 1;
      twmd_show_current_item();
      twmd_set_toolbar_navigation_buttons_state();
   });
});