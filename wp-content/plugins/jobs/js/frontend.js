jQuery(document).ready(function($) {
  $( "#same-addresses-btn" ).click(function(event) {
    event.preventDefault();

    $('#billing-address-line1').val($('#project-address-line1').val());
    $('#billing-address-line2').val($('#project-address-line2').val());
    $('#billing-city').val($('#project-city').val());
    $('#billing-state').val($('#project-state').val());
    $('#billing-postal-code').val($('#project-postal-code').val());
  });
  // $("#addsagjob").validate();

  $('[data-toggle="tooltip"]').tooltip();
  $('.dropdown-toggle').dropdown();

});
