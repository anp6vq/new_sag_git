var job_id;
var created_date;
var job_item = {};
var aws_s3_region = 'us-east-1';
var aws_s3_identitypoolid = 'us-east-1:5c951299-a194-4f30-8f24-5e82398c8105';
var aws_s3_bucket = 'sag-jobs-photos';

webshim.polyfill('forms');

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, "\\$&");
   var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function clear_form() {
   jQuery('#text_salesman_name').val('');
   jQuery('#text_quote_number').val('');
   jQuery('#text_job_name').val('');
   jQuery('#text_onsite_contact').val('');
   jQuery('#text_contact_number').val('');
   jQuery('#text_contact_email').val('');
   jQuery('#text_person_seen').val('');
   jQuery('#text_csr').val('');
   jQuery('#text_project_address_line1').val('');
   jQuery('#text_project_address_line2').val('');
   jQuery('#text_project_email').val('');
   jQuery('#text_project_city').val('');
   jQuery('#select_project_state').val(['not_applicable']);
   jQuery('#text_project_postal_code').val('');
   jQuery('#text_billing_address_line1').val('');
   jQuery('#text_billing_address_line2').val('');
   jQuery('#text_billing_email').val('');
   jQuery('#text_billing_city').val('');
   jQuery('#select_billing_state').val(['not_applicable']);
   jQuery('#text_billing_postal_code').val('');
   jQuery('#textarea_project_description').val('');
   jQuery('#select_project_status').val(['not_applicable']);
   jQuery('#select_labor_hours_type').val(['not_applicable']);
   jQuery('#text_labor_number_of_hours').val('');
   jQuery('#text_labor_number_of_men').val('');
}

function print_item_to_form(job_item) {
   jQuery('#text_salesman_name').val(job_item.salesman_name);
   jQuery('#text_quote_number').val(job_item.quote_number);
   jQuery('#text_job_name').val(job_item.job_name);
   jQuery('#text_onsite_contact').val(job_item.onsite_contact);
   jQuery('#text_contact_number').val(job_item.contact_number);
   jQuery('#text_contact_email').val(job_item.contact_email);
   jQuery('#text_person_seen').val(job_item.person_seen);
   jQuery('#text_csr').val(job_item.csr);
   jQuery('#text_project_address_line1').val(job_item.project_address_line1);
   jQuery('#text_project_address_line2').val(job_item.project_address_line2);
   jQuery('#text_project_email').val(job_item.project_email);
   jQuery('#text_project_city').val(job_item.project_city);
   jQuery('#select_project_state').val([job_item.project_state]);
   jQuery('#text_project_postal_code').val(job_item.project_postal_code);
   jQuery('#text_billing_address_line1').val(job_item.billing_address_line1);
   jQuery('#text_billing_address_line2').val(job_item.billing_address_line2);
   jQuery('#text_billing_email').val(job_item.billing_email);
   jQuery('#text_billing_city').val(job_item.billing_city);
   jQuery('#select_billing_state').val([job_item.billing_state]);
   jQuery('#text_billing_postal_code').val(job_item.billing_postal_code);
   jQuery('#textarea_project_description').val(job_item.project_description);
   jQuery('#select_project_status').val([job_item.project_status]);
   jQuery('#select_labor_hours_type').val([job_item.labor_hours_type]);
   jQuery('#text_labor_number_of_hours').val(job_item.labor_number_of_hours);
   jQuery('#text_labor_number_of_men').val(job_item.labor_number_of_men);
}

function get_item_from_form() {
   var salesman_name = jQuery('#text_salesman_name').val();
   var quote_number = jQuery('#text_quote_number').val();
   var job_name = jQuery('#text_job_name').val();
   var onsite_contact = jQuery('#text_onsite_contact').val();
   var contact_number = jQuery('#text_contact_number').val();
   var contact_email = jQuery('#text_contact_email').val();
   var person_seen = jQuery('#text_person_seen').val();
   var csr = jQuery('#text_csr').val();
   var project_address_line1 = jQuery('#text_project_address_line1').val();
   var project_address_line2 = jQuery('#text_project_address_line2').val();
   var project_email = jQuery('#text_project_email').val();
   var project_city = jQuery('#text_project_city').val();
   var project_state = jQuery('#select_project_state').val();
   var project_postal_code = jQuery('#text_project_postal_code').val();


   var billing_address_line1 = jQuery('#text_billing_address_line1').val();
   var billing_address_line2 = jQuery('#text_billing_address_line2').val();
   var billing_email = jQuery('#text_billing_email').val();
   var billing_city = jQuery('#text_billing_city').val();
   var billing_state = jQuery('#select_billing_state').val();
   var billing_postal_code = jQuery('#text_billing_postal_code').val();
   var project_description = jQuery('#textarea_project_description').val();
   var project_status = jQuery('#select_project_status').val();
   var labor_hours_type = jQuery('#select_labor_hours_type').val();
   var labor_number_of_hours = jQuery('#text_labor_number_of_hours').val();
   var labor_number_of_men = jQuery('#text_labor_number_of_men').val();

   var textarea_tgpd_notes = jQuery('#textarea_tgpd_notes').val();

   var job_item = {};
   job_item.id = job_id;
   job_item.salesman_name = salesman_name;
   job_item.quote_number = quote_number;
   job_item.job_name = job_name;
   job_item.onsite_contact = onsite_contact;
   job_item.contact_number = contact_number;
   job_item.contact_email = contact_email;
   job_item.person_seen = person_seen;
   job_item.csr = csr;
   job_item.project_address_line1 = project_address_line1;
   job_item.project_address_line2 = project_address_line2;
   job_item.project_email = project_email;
   job_item.project_city = project_city;
   job_item.project_state = project_state;
   job_item.project_postal_code = project_postal_code;
   job_item.billing_address_line1 = billing_address_line1;
   job_item.billing_address_line2 = billing_address_line2;
   job_item.billing_email = billing_email;
   job_item.billing_city = billing_city;
   job_item.billing_state = billing_state;
   job_item.billing_postal_code = billing_postal_code;
   job_item.project_description = project_description;
   job_item.project_status = project_status;
   job_item.labor_hours_type = labor_hours_type;
   job_item.labor_number_of_hours = labor_number_of_hours;
   job_item.labor_number_of_men = labor_number_of_men;
   job_item.textarea_tgpd_notes = textarea_tgpd_notes;
   job_item.created_date = created_date;
  // print_item_to_console('get_item_from_form', job_item);
   return job_item;
}

function print_item_to_console(debug_tag, job_item) {
   var log_tag = (debug_tag.length == 0 ? '' : (debug_tag + ': '));
   console.log(log_tag + 'job_id => ' + job_item.job_id);
   console.log(log_tag + 'salesman_name => ' + job_item.salesman_name);
   console.log(log_tag + 'quote_number => ' + job_item.quote_number);
   console.log(log_tag + 'job_name => ' + job_item.job_name);
   console.log(log_tag + 'onsite_contact => ' + job_item.onsite_contact);
   console.log(log_tag + 'contact_number => ' + job_item.contact_number);
   console.log(log_tag + 'contact_email => ' + job_item.contact_email);
   console.log(log_tag + 'person_seen => ' + job_item.person_seen);
   console.log(log_tag + 'csr => ' + job_item.csr);
   console.log(log_tag + 'project_address_line1 => ' + job_item.project_address_line1);
   console.log(log_tag + 'project_address_line2 => ' + job_item.project_address_line2);
   console.log(log_tag + 'project_email => ' + job_item.project_email);
   console.log(log_tag + 'project_city => ' + job_item.project_city);
   console.log(log_tag + 'project_state => ' + job_item.project_state);
   console.log(log_tag + 'project_postal_code => ' + job_item.project_postal_code);
   console.log(log_tag + 'billing_address_line1 => ' + job_item.billing_address_line1);
   console.log(log_tag + 'billing_address_line2 => ' + job_item.billing_address_line2);
   console.log(log_tag + 'billing_email => ' + job_item.billing_email);
   console.log(log_tag + 'billing_city => ' + job_item.billing_city);
   console.log(log_tag + 'billing_state => ' + job_item.billing_state);
   console.log(log_tag + 'billing_postal_code => ' + job_item.billing_postal_code);
   console.log(log_tag + 'project_description => ' + job_item.project_description);
   console.log(log_tag + 'project_status => ' + job_item.project_status);
   console.log(log_tag + 'labor_hours_type => ' + job_item.labor_hours_type);
   console.log(log_tag + 'labor_number_of_hours => ' + job_item.labor_number_of_hours);
   console.log(log_tag + 'labor_number_of_men => ' + job_item.labor_number_of_men);
   console.log(log_tag + 'created_date => ' + job_item.created_date);
   // console.log(log_tag + 'job_item => ' + JSON.stringify(job_item));
}

function validateForm() {
  console.log(log_tag + 'job_id => ' + job_item.job_id);
  console.log(log_tag + 'salesman_name => ' + job_item.salesman_name);
  console.log(log_tag + 'quote_number => ' + job_item.quote_number);
  console.log(log_tag + 'job_name => ' + job_item.job_name);
  console.log(log_tag + 'onsite_contact => ' + job_item.onsite_contact);
  console.log(log_tag + 'contact_number => ' + job_item.contact_number);
  console.log(log_tag + 'contact_email => ' + job_item.contact_email);
  console.log(log_tag + 'person_seen => ' + job_item.person_seen);
  console.log(log_tag + 'csr => ' + job_item.csr);
  console.log(log_tag + 'project_address_line1 => ' + job_item.project_address_line1);
  console.log(log_tag + 'project_address_line2 => ' + job_item.project_address_line2);
  console.log(log_tag + 'project_email => ' + job_item.project_email);
  console.log(log_tag + 'project_city => ' + job_item.project_city);
  console.log(log_tag + 'project_state => ' + job_item.project_state);
  console.log(log_tag + 'project_postal_code => ' + job_item.project_postal_code);
  console.log(log_tag + 'billing_address_line1 => ' + job_item.billing_address_line1);
  console.log(log_tag + 'billing_address_line2 => ' + job_item.billing_address_line2);
  console.log(log_tag + 'billing_email => ' + job_item.billing_email);
  console.log(log_tag + 'billing_city => ' + job_item.billing_city);
  console.log(log_tag + 'billing_state => ' + job_item.billing_state);
  console.log(log_tag + 'billing_postal_code => ' + job_item.billing_postal_code);
  console.log(log_tag + 'project_description => ' + job_item.project_description);
  console.log(log_tag + 'project_status => ' + job_item.project_status);
  console.log(log_tag + 'labor_hours_type => ' + job_item.labor_hours_type);
  console.log(log_tag + 'labor_number_of_hours => ' + job_item.labor_number_of_hours);
  console.log(log_tag + 'labor_number_of_men => ' + job_item.labor_number_of_men);
  console.log(log_tag + 'created_date => ' + job_item.created_date);
   var text_salesman_name = jQuery('#text_salesman_name')[0];
   if (!text_salesman_name.checkValidity()) {
      text_salesman_name.setCustomValidity('Please enter sales man name.');
      return false;
   } else {
      text_salesman_name.setCustomValidity('');
   }
   var text_quote_number = jQuery('#text_quote_number')[0];
   if (!text_quote_number.checkValidity()) {
      text_quote_number.setCustomValidity('Please enter quote number.');
      return false;
   } else {
      text_quote_number.setCustomValidity('');
   }
   var text_job_name = jQuery('#text_job_name')[0];
   if (!text_job_name.checkValidity()) {
      text_job_name.setCustomValidity('Please enter job name.');
      return false;
   } else {
      text_job_name.setCustomValidity('');
   }
   var text_onsite_contact = jQuery('#text_onsite_contact')[0];
   if (!text_onsite_contact.checkValidity()) {
      text_onsite_contact.setCustomValidity('Please enter on site contact.');
      return false;
   } else {
      text_onsite_contact.setCustomValidity('');
   }
   var text_contact_number = jQuery('#text_contact_number')[0];
   if (!text_contact_number.checkValidity()) {
      text_contact_number.setCustomValidity('Please enter contact number.');
      return false;
   } else {
      text_contact_number.setCustomValidity('');
   }
   var text_contact_email = jQuery('#text_contact_email')[0];
   if (!text_contact_email.checkValidity()) {
      text_contact_email.setCustomValidity('Please enter contact email.');
      return false;
   } else {
      text_contact_email.setCustomValidity('');
   }
   var text_person_seen = jQuery('#text_person_seen')[0];
   if (!text_person_seen.checkValidity()) {
      text_person_seen.setCustomValidity('Please enter person seen.');
      return false;
   } else {
      text_person_seen.setCustomValidity('');
   }
   var text_csr = jQuery('#text_csr')[0];
   if (!text_csr.checkValidity()) {
      text_csr.setCustomValidity('Please enter csr.');
      return false;
   } else {
      text_csr.setCustomValidity('');
   }
   var text_project_address_line1 = jQuery('#text_project_address_line1')[0];
   if (!text_project_address_line1.checkValidity()) {
      text_project_address_line1.setCustomValidity('Please enter project address.');
      return false;
   } else {
      text_project_address_line1.setCustomValidity('');
   }
   var text_project_email = jQuery('#text_project_email')[0];
   if (!text_project_email.checkValidity()) {
      text_project_email.setCustomValidity('Please enter project email.');
      return false;
   } else {
      text_project_email.setCustomValidity('');
   }
   var text_project_city = jQuery('#text_project_city')[0];
   if (!text_project_city.checkValidity()) {
      text_project_city.setCustomValidity('Please enter project city.');
      return false;
   } else {
      text_project_city.setCustomValidity('');
   }
   var project_state = jQuery('#select_project_state').val();
   if (typeof project_state === 'undefined' || project_state == null) project_state = 'not_applicable';
   if (project_state.localeCompare('not_applicable') == 0) {
      jQuery('#select_project_state')[0].focus();
      return false;
   }
   var text_project_postal_code = jQuery('#text_project_postal_code')[0];
   if (!text_project_postal_code.checkValidity()) {
      text_project_postal_code.setCustomValidity('Please enter project zip code.');
      return false;
   } else {
      text_project_postal_code.setCustomValidity('');
   }
   var text_billing_address_line1 = jQuery('#text_billing_address_line1')[0];
   if (!text_billing_address_line1.checkValidity()) {
      text_billing_address_line1.setCustomValidity('Please enter billing address.');
      return false;
   } else {
      text_billing_address_line1.setCustomValidity('');
   }
   var text_billing_email = jQuery('#text_billing_email')[0];
   if (!text_billing_email.checkValidity()) {
      text_billing_email.setCustomValidity('Please enter billing email.');
      return false;
   } else {
      text_billing_email.setCustomValidity('');
   }
   var text_billing_city = jQuery('#text_billing_city')[0];
   if (!text_billing_city.checkValidity()) {
      text_billing_city.setCustomValidity('Please enter billing city.');
      return false;
   } else {
      text_billing_city.setCustomValidity('');
   }
   var billing_state = jQuery('#select_billing_state').val();
   if (typeof billing_state === 'undefined' || billing_state == null) billing_state = 'not_applicable';
   if (billing_state.localeCompare('not_applicable') == 0) {
      jQuery('#select_billing_state')[0].focus();
      return false;
   }
   var text_billing_postal_code = jQuery('#text_billing_postal_code')[0];
   if (!text_billing_postal_code.checkValidity()) {
      text_billing_postal_code.setCustomValidity('Please enter billing zip code.');
      return false;
   } else {
      text_billing_postal_code.setCustomValidity('');
   }
   var project_status = jQuery('#select_project_status').val();
   if (typeof project_status === 'undefined' || project_status == null) project_status = 'not_applicable';
   if (project_status.localeCompare('not_applicable') == 0) {
      jQuery('#select_project_status')[0].focus();
      return false;
   }
   return true;
}

function do_same_addresses() {
   jQuery('#text_billing_address_line1').val(jQuery('#text_project_address_line1').val());
   jQuery('#text_billing_address_line2').val(jQuery('#text_project_address_line2').val());
   jQuery('#text_billing_email').val(jQuery('#text_project_email').val());
   jQuery('#text_billing_city').val(jQuery('#text_project_city').val());
   jQuery('#select_billing_state').val([jQuery('#select_project_state').val()]);
   jQuery('#text_billing_postal_code').val(jQuery('#text_project_postal_code').val());
}

function notes_tab_get_deta(){



}
function save_job() {
   if (!validateForm()) {
      return false;
   }
   var subButton = jQuery('#button_submit_new_job');
   subButton.prop('disabled', true);
   subButton.val('Saving New Job...');
   job_item = get_item_from_form();
   job_item.gpd_items = tgpd_items;
   job_item.agd_items = tagd_items;

   console.log(job_item);
   //return false;

   var data = {
      'action': 'add_job_item',
      'data': job_item
   };
   //console.log("save_job: data => " + JSON.stringify(data));
   var jqXHR = jQuery.post(ajax_object.ajax_url, data);
   jqXHR.done(function(data, textStatus, jqXHR) {
      console.log("save_job:jqXHR.done: data => " + data + ", textStatus => " + textStatus + ", responseText => " + jqXHR.responseText + ", responseXML => " + jqXHR.responseXML + ", status => " + jqXHR.status + ", statusText => " + jqXHR.statusText + ", statusCode() => " + jqXHR.statusCode());
      if (job_id > 0) {
         var data_js = JSON.parse(data);
         document.location.replace(data_js.redirect_url);
      } else {
         clear_form();
         job_item = {};
         tgpd_clear_form();
         tgpd_init(false);
         tagd_clear_form();
         tagd_init(false);
         subButton.val('Submit New Job');
         subButton.prop('disabled', false);
         jQuery('#text_salesman_name')[0].focus();
      }
   });
   jqXHR.fail(function(jqXHR, textStatus, errorThrown) {
      console.log("save_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      alert("save_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      subButton.val('Submit New Job');
      subButton.prop('disabled', false);
   });
}

function fetch_job(job_id) {
   var subButton = jQuery('#button_submit_new_job');
   subButton.prop('disabled', true);
   subButton.val('Fetching Job...');
   var data = {
      'action': 'get_job_item',
      'job_id': job_id
   };
   var jqXHR = jQuery.post(ajax_object.ajax_url, data);
   jqXHR.done(function(data, textStatus, jqXHR) {
      console.log("fetch_job: data => " + data + ", textStatus => " + textStatus + ", responseText => " + jqXHR.responseText + ", responseXML => " + jqXHR.responseXML + ", status => " + jqXHR.status + ", statusText => " + jqXHR.statusText + ", statusCode() => " + jqXHR.statusCode());
      var job_items = JSON.parse(data);
      job_item = job_items[0];
      print_item_to_form(job_item);
      tgpd_init(true, job_item["gpd_items"]);
      tagd_init(true, job_item["agd_items"]);
      created_date = job_item["created_date"];
      subButton.val('Submit New Job');
      subButton.prop('disabled', false);
      jQuery('#text_salesman_name')[0].focus();
   });
   jqXHR.fail(function(jqXHR, textStatus, errorThrown) {
      console.log("fetch_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      alert("fetch_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      subButton.val('Submit New Job');
      subButton.prop('disabled', false);
   });
}

jQuery(document).ready(function($) {
   var $loading = $('<div id="loading"></div>').insertBefore('#addsagjob');
   $(document).ajaxStart(function() {
      $loading.show();
   }).ajaxStop(function() {
      $loading.hide();
   });
   job_id = getParameterByName('job_id');
   created_date = new Date().toISOString();
   console.log('jQuery.document.ready: job_id -> ' + job_id + ', created_date => ' + created_date);
   if (typeof job_id === 'undefined' || job_id == null) job_id = 0;
   console.log('jQuery.document.ready: job_id -> ' + job_id + ', created_date => ' + created_date);
   if (job_id > 0) {
      fetch_job(job_id);
   } else {
      tgpd_init(false);
      tagd_init(false);
   }
   $('#button_same_addresses').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      do_same_addresses();
   });
   $('#addsagjob').submit(function(event) {
      event.preventDefault();
      event.stopPropagation();
      save_job();
   });
});