function DisplayDateTime(){
    var DateTime = new Date();
    var strYear= DateTime.getFullYear();
    var strMonth= DateTime.getMonth() +1;
    strMonth= (strMonth<10?'0':'') + strMonth;
    var strDay = DateTime.getDate();
    var strHours = DateTime.getHours();
    var strMinutes = (DateTime.getMinutes()<10?'0':'') + DateTime.getMinutes();
    var strSeconds = (DateTime.getSeconds()<10?'0':'') + DateTime.getSeconds();
   // var tagDiv=document.getElementById("DivTag");
    var cdate = strYear + "-" + strMonth + "-" + strDay + " " + strHours + ":" + strMinutes + ":" + strSeconds;
    document.cookie = "cdate = " + cdate;
}
function delete_cookie(name) {
    document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:01 GMT;';
};
//delete_cookie('cdate');
DisplayDateTime();