$('#btn_upg_clear_picture_filelist_note').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      btn_upg_clear_picture_filelist_notes();
   });
  $('#btn_usg_clear_sketch_filelist_notes').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_sketch_filelist_notes();
	  tgpd_clear_sketch_filelist_notes({ clear_canvas:true, hide_canvas:true });
   });
var imageOrder = 1;
var sketchOrder = 1;
var tgpd_pictures_download_url_notes = [];
var tgpd_sketches_download_url_notes = [];
function tgpd_clear_sketch_filelist_notes(tgpd_options) {

   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var tgpd_option_clear_canvas = typeof tgpd_options.clear_canvas === 'undefined' ? false : tgpd_options.clear_canvas;
   var tgpd_option_hide_canvas = typeof tgpd_options.hide_canvas === 'undefined' ? false : tgpd_options.hide_canvas;
   var $tgpd_sketch_filelist = jQuery('#div_usg_sketch_filelist_notes');
   var $tgpd_sketch_filelist_controls = jQuery("#div_usg_sketch_filelist_controls_notes");
   $tgpd_sketch_filelist_controls.hide();
   $tgpd_sketch_filelist.html('');
   $tgpd_sketch_filelist.off('click');
   if (tgpd_sketches_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_sketches_download_url.forEach(function(element, index, array) {
         console.log('div_usg_sketch_filelist_notes: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
  
   tgpd_sketches_download_url_notes = [];
   if (tgpd_option_clear_canvas) tgpd_clear_canvas();
   if (tgpd_option_hide_canvas) tgpd_hide_canvas_holder();
}
function btn_upg_clear_picture_filelist_notes(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var $tgpd_picture_filelist = jQuery('#div_upg_picture_filelist_notes');
   var $tgpd_picture_filelist_controls = jQuery("#div_upg_picture_filelist_controls_notes");
   $tgpd_picture_filelist_controls.hide();
   $tgpd_picture_filelist.html('');
   $tgpd_picture_filelist.off('click');
   if (tgpd_pictures_download_url_notes.length > 0 && tgpd_option_s3_delete) {
      tgpd_pictures_download_url_notes.forEach(function(element, index, array) {
         console.log('btn_upg_clear_picture_filelist_notes: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   
   tgpd_pictures_download_url_notes = [];
}


$('#btn_upg_upload_picture_filelist_notes').click(function(event) {
		jQuery('#btn_upg_upload_picture_filelist_notes').attr('disabled','disabled');
	jQuery('#btn_upg_upload_picture_filelist_notes').addClass('cust-disable');												   	
      event.preventDefault();
      event.stopPropagation();
      btn_upg_upload_picture_filelist_notes();
   });

function btn_upg_upload_picture_filelist_notes() {
   var $imgs = jQuery('.picture_obj');
    var imgCOunter = tgpd_pictures_download_url_notes.length+1;
	console.log('preimgCOunter'+imgCOunter);
     $imgs.each(function(index) {
						 
		console.log('index is:'+index);
			
				
      var self = this;
      var file = this.file;
      var $progress_bar = this.progress;
      var $status = this.status;
      $progress_bar.css('width', '0%');
      $status.removeClass('success failure');
      console.log('btn_upg_upload_picture_filelist_notes: file.name => ' + file.name + ', file.size => ' + file.size + ', file.type => ' + file.type + ', file.lastModifiedDate => ' + file.lastModifiedDate);
	  var resize = new window.resize();
	  resize.init();
	  resize.photo(file, 1200, 'file', function (resizedFile) {
											 
      		tgpd_s3_upload_object(file, resizedFile,$progress_bar, function(err, data) {
																			
				 if (!err) {
					$status.addClass('success');
					$progress_bar.css('width', '100%');
					jQuery(self).removeClass('picture_obj');
					console.log('aaimgCOunter'+imgCOunter);
					if(imgCOunter== 1){
						tgpd_pictures_download_url_notes[index]= data.Location;
					}else{
						tgpd_pictures_download_url_notes[imgCOunter+index]= data.Location;	
						}
					console.log('btn_upg_upload_picture_filelist_notes: data.Location => ' + data.Location + ', tgpd_pictures_download_url_notes => ' + tgpd_pictures_download_url_notes.toString());
		
					var ajaxurl_image = jQuery('#ajax_url_image_url').val();
					console.log(ajaxurl_image);
		
					jQuery.ajax({
						 url: ajaxurl_image,
						 data: {
							 'action':'temp_uploade',
							 'image_url' : tgpd_pictures_download_url_notes.toString(),
							 'type':'image',
						 },
						 success:function(data) {
							 // This outputs the result of the ajax request
							 //console.log(data);
						 }
		
					});
		
				 } else {
					$status.addClass('failure');
					$progress_bar.css('width', '0%');
				 }
			  });
			
		});
   });
}


$('#btn_usg_upload_sketch_filelist_notes').click(function(event) {
														  jQuery('#btn_usg_upload_sketch_filelist_notes').attr('disabled','disabled');
		jQuery('#btn_usg_upload_sketch_filelist_notes').addClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      btn_usg_upload_sketch_filelist_notes();
   });


function btn_usg_upload_sketch_filelist_notes() {
   var $imgs = jQuery('.sketch_obj');
   var imgCOunter = tgpd_sketches_download_url_notes.length+1;
   $imgs.each(function(index) {
					   
      var self = this;
      var file = this.file;
      var $progress_bar = this.progress;
      var $status = this.status;
      $progress_bar.css('width', '0%');
      $status.removeClass('success failure');
      console.log('btn_usg_upload_sketch_filelist_notes: file.name => ' + file.name + ', file.size => ' + file.size + ', file.type => ' + file.type + ', file.lastModifiedDate => ' + file.lastModifiedDate);
      tgpd_s3_upload_object_sketch(file, $progress_bar, function(err, data) {
         if (!err) {
            $status.addClass('success');
            $progress_bar.css('width', '100%');
            jQuery(self).removeClass('sketch_obj');
			if(imgCOunter== 1){
           	 tgpd_sketches_download_url_notes[index]=data.Location;
			}else{
				tgpd_sketches_download_url_notes[imgCOunter+index]=data.Location;
				}
            console.log('btn_usg_upload_sketch_filelist_notes: data.Location => ' + data.Location + ', tgpd_sketches_download_url_notes => ' + tgpd_sketches_download_url_notes.toString());

            var ajaxurl_image = jQuery('#ajax_url_image_url').val();
            console.log(ajaxurl_image);

            jQuery.ajax({
                 url: ajaxurl_image,
                 data: {
                     'action':'temp_uploade',
                     'image_url' : tgpd_sketches_download_url_notes.toString(),
                     'type':'sketch',
                 },
                 success:function(data) {
                     // This outputs the result of the ajax request
                     //console.log(data);
                 }

            });


         } else {
            $status.addClass('failure');
            $progress_bar.css('width', '0%');
         }
      });
   });
}

function notes_init(notes_data)
{
	jQuery('#div_upload_sketch_generic_03').show();
	var ajaxurl_image = jQuery('#ajax_url_image_url').val();
  if (typeof(notes_data) == "undefined") { return false; }
  //console.log(notes_data);
  $('#textarea_tgpd_notes').val(notes_data.note_text);
  if(notes_data.notes_upload_pic && notes_data.notes_upload_pic.length > 1 ){
    imageUrl = notes_data.notes_upload_pic.split(',');
    formatedStringImage = '';
    for (var i=0; i<imageUrl.length; i++) {
      var formatedStringImage = formatedStringImage+'<img src="'+imageUrl[i]+'" alt="" height="150" width="150" style=" float:left; display:inline; margin-right:10px;" >';
	  tgpd_pictures_download_url_notes.push(imageUrl[i]);
	  
    }
    $('#div_upg_picture_filelist_notes').append(formatedStringImage);
  }

  if(notes_data.notes_sketch_diagram && notes_data.notes_sketch_diagram.length > 1  ){
    sketchUrl = notes_data.notes_sketch_diagram.split(',');
    formatedStringSketch = '';
    for (var i=0; i<sketchUrl.length; i++) {
		
      var formatedStringSketch = formatedStringSketch+'<img src="'+sketchUrl[i]+'" alt="" height="150" width="150" style=" float:left; display:inline" >';
	   tgpd_sketches_download_url_notes.push(sketchUrl[i]);
    }
    $('#div_usg_sketch_filelist_notes').append(formatedStringSketch);
  }
}
