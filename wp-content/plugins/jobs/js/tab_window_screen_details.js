var twsd_items = [];
var twsd_current_item = -1;
var twsd_editing = false;
var twsd_copying = false;
var twsd_clipboard = {};

function twsd_set_toolbar_navigation_item_count() {
   if (twsd_editing) {
      jQuery('#text_twsd_item_count').val('Add Item');
   } else {
      jQuery('#text_twsd_item_count').val((twsd_current_item + 1) + '/' + twsd_items.length);
   }
}

function twsd_init_form_state() {
}

function twsd_init_toolbar_navigation_buttons_state() {
   jQuery('#btn_twsd_first').attr('disabled','disabled');
   jQuery('#btn_twsd_previous').attr('disabled','disabled');
   jQuery('#text_twsd_item_count').attr('readonly','readonly');
   jQuery('#btn_twsd_next').attr('disabled','disabled');
   jQuery('#btn_twsd_last').attr('disabled','disabled');
   twsd_set_toolbar_navigation_item_count();
}

function twsd_init_toolbar_editing_buttons_state() {
   jQuery('#btn_twsd_add').removeAttr('disabled');
   jQuery('#btn_twsd_save').attr('disabled','disabled');
   jQuery('#btn_twsd_delete').attr('disabled','disabled');
   jQuery('#btn_twsd_refresh').attr('disabled','disabled');
   twsd_init_toolbar_copy_paste_buttons_state();
}

function twsd_init_toolbar_copy_paste_buttons_state() {
   jQuery('#btn_twsd_copy').attr('disabled','disabled');
   jQuery('#btn_twsd_paste').attr('disabled','disabled');
}

function twsd_set_toolbar_navigation_buttons_state() {
   if (twsd_current_item == -1 || twsd_items.length == 0 || twsd_editing) {
      twsd_init_toolbar_navigation_buttons_state();
      return;
   }
   if (twsd_items.length == 1) {
      jQuery('#btn_twsd_first').attr('disabled','disabled');
      jQuery('#btn_twsd_previous').attr('disabled','disabled');
      jQuery('#btn_twsd_next').attr('disabled','disabled');
      jQuery('#btn_twsd_last').attr('disabled','disabled');
   } else {
      if (twsd_current_item == 0) {
         jQuery('#btn_twsd_first').attr('disabled','disabled');
         jQuery('#btn_twsd_previous').attr('disabled','disabled');
         jQuery('#btn_twsd_next').removeAttr('disabled');
         jQuery('#btn_twsd_last').removeAttr('disabled');
      } else if (twsd_current_item == (twsd_items.length - 1)) {
         jQuery('#btn_twsd_first').removeAttr('disabled');
         jQuery('#btn_twsd_previous').removeAttr('disabled');
         jQuery('#btn_twsd_next').attr('disabled','disabled');
         jQuery('#btn_twsd_last').attr('disabled','disabled');
      } else {
         jQuery('#btn_twsd_first').removeAttr('disabled');
         jQuery('#btn_twsd_previous').removeAttr('disabled');
         jQuery('#btn_twsd_next').removeAttr('disabled');
         jQuery('#btn_twsd_last').removeAttr('disabled');
      }
   }
   twsd_set_toolbar_navigation_item_count();
}

function twsd_set_toolbar_editing_buttons_state() {
   if ((!twsd_editing) && (!twsd_copying) && (!(twsd_current_item >= 0 && twsd_current_item < twsd_items.length))) {
      twsd_init_toolbar_editing_buttons_state();
      jQuery('#btn_submit_new_job').removeAttr('disabled');
      return;
   }
   if (twsd_editing) {
      jQuery('#btn_twsd_add').attr('disabled','disabled');
      jQuery('#btn_twsd_save').removeAttr('disabled');
      jQuery('#btn_twsd_delete').removeAttr('disabled');
      jQuery('#btn_twsd_refresh').attr('disabled','disabled');
      jQuery('#btn_submit_new_job').attr('disabled','disabled');
   } else {
      jQuery('#btn_twsd_add').removeAttr('disabled');
      if (twsd_current_item >= 0 && twsd_current_item < twsd_items.length) {
         jQuery('#btn_twsd_save').removeAttr('disabled');
         jQuery('#btn_twsd_delete').removeAttr('disabled');
         jQuery('#btn_twsd_refresh').removeAttr('disabled');
      }
      jQuery('#btn_submit_new_job').removeAttr('disabled');
   }
   twsd_set_toolbar_copy_paste_buttons_state();
}

function twsd_set_toolbar_copy_paste_buttons_state() {
   if (twsd_editing) {
      jQuery('#btn_twsd_copy').attr('disabled','disabled');
      if (twsd_copying) {
         jQuery('#btn_twsd_paste').removeAttr('disabled');
      } else {
         jQuery('#btn_twsd_paste').attr('disabled','disabled');
      }
   } else {
      if (twsd_current_item >= 0 && twsd_current_item < twsd_items.length) {
         jQuery('#btn_twsd_copy').removeAttr('disabled');
         if (twsd_copying) {
            jQuery('#btn_twsd_paste').removeAttr('disabled');
         } else {
            jQuery('#btn_twsd_paste').attr('disabled','disabled');
         }
      }
   }
}

function twsd_clear_form() {
   jQuery('input:radio[name=radio_twsd_screen_type]').val(['not_a_value']);
   jQuery('#text_twsd_exact_width').val('');
   jQuery('#text_twsd_exact_height').val('');
   jQuery('#select_twsd_frame_thickness').val(['not_a_value']);
   jQuery('#select_twsd_frame_finish').val(['not_a_value']);
   jQuery('#select_twsd_type_of_mesh').val(['not_a_value']);
   jQuery('#select_twsd_color_of_mesh').val(['not_a_value']);
   jQuery('#text_twsd_hardware_type_line1').val('');
   jQuery('#text_twsd_quantity_or_side_line1').val('');
   jQuery('#text_twsd_number_of_sides_line1').val('');
   jQuery('#text_twsd_frame_side_line1').val('');
   jQuery('#text_twsd_distance_from_end_line1').val('');
   jQuery('#text_twsd_hardware_type_line2').val('');
   jQuery('#text_twsd_quantity_or_side_line2').val('');
   jQuery('#text_twsd_number_of_sides_line2').val('');
   jQuery('#text_twsd_frame_side_line2').val('');
   jQuery('#text_twsd_distance_from_end_line2').val('');
   jQuery('#textarea_twsd_comments').val('');
}

function twsd_show_current_item() {
   twsd_clear_form();
   var twsd_item = twsd_items[twsd_current_item];
   twsd_print_item_to_form(twsd_item);
}

function twsd_get_item_from_form() {
   var twsd_screen_type = jQuery('input:radio[name=radio_twsd_screen_type]:checked').val();
   var twsd_exact_width = jQuery('#text_twsd_exact_width').val();
   var twsd_exact_height = jQuery('#text_twsd_exact_height').val();
   var twsd_frame_thickness = jQuery('#select_twsd_frame_thickness').val();
   var twsd_frame_finish = jQuery('#select_twsd_frame_finish').val();
   var twsd_type_of_mesh = jQuery('#select_twsd_type_of_mesh').val();
   var twsd_color_of_mesh = jQuery('#select_twsd_color_of_mesh').val();
   var twsd_hardware_type_line1 = jQuery('#text_twsd_hardware_type_line1').val();
   var twsd_quantity_or_side_line1 = jQuery('#text_twsd_quantity_or_side_line1').val();
   var twsd_number_of_sides_line1 = jQuery('#text_twsd_number_of_sides_line1').val();
   var twsd_frame_side_line1 = jQuery('#text_twsd_frame_side_line1').val();
   var twsd_distance_from_end_line1 = jQuery('#text_twsd_distance_from_end_line1').val();
   var twsd_hardware_type_line2 = jQuery('#text_twsd_hardware_type_line2').val();
   var twsd_quantity_or_side_line2 = jQuery('#text_twsd_quantity_or_side_line2').val();
   var twsd_number_of_sides_line2 = jQuery('#text_twsd_number_of_sides_line2').val();
   var twsd_frame_side_line2 = jQuery('#text_twsd_frame_side_line2').val();
   var twsd_distance_from_end_line2 = jQuery('#text_twsd_distance_from_end_line2').val();
   var twsd_comments = jQuery('#textarea_twsd_comments').val();

   var twsd_item = {};
   twsd_item.id = 0;
   twsd_item.job_id = job_id;
   twsd_item.screen_type = twsd_screen_type;
   twsd_item.exact_width = twsd_exact_width;
   twsd_item.exact_height = twsd_exact_height;
   twsd_item.frame_thickness = twsd_frame_thickness;
   twsd_item.frame_finish = twsd_frame_finish;
   twsd_item.type_of_mesh = twsd_type_of_mesh;
   twsd_item.color_of_mesh = twsd_color_of_mesh;
   twsd_item.hardware_type_line1 = twsd_hardware_type_line1;
   twsd_item.quantity_or_side_line1 = twsd_quantity_or_side_line1;
   twsd_item.number_of_sides_line1 = twsd_number_of_sides_line1;
   twsd_item.frame_side_line1 = twsd_frame_side_line1;
   twsd_item.distance_from_end_line1 = twsd_distance_from_end_line1;
   twsd_item.hardware_type_line2 = twsd_hardware_type_line2;
   twsd_item.quantity_or_side_line2 = twsd_quantity_or_side_line2;
   twsd_item.number_of_sides_line2 = twsd_number_of_sides_line2;
   twsd_item.frame_side_line2 = twsd_frame_side_line2;
   twsd_item.distance_from_end_line2 = twsd_distance_from_end_line2;
   twsd_item.comments = twsd_comments;

   twsd_print_item_to_console('twsd_get_item_from_form', twsd_item);

   return twsd_item;
}

function twsd_print_item_to_form(twsd_item) {
   jQuery('input:radio[name=radio_twsd_screen_type]').val([twsd_item.screen_type]);
   jQuery('#text_twsd_exact_width').val(twsd_item.exact_width);
   jQuery('#text_twsd_exact_height').val(twsd_item.exact_height);
   jQuery('#select_twsd_frame_thickness').val([twsd_item.frame_thickness]);
   jQuery('#select_twsd_frame_finish').val([twsd_item.frame_finish]);
   jQuery('#select_twsd_type_of_mesh').val([twsd_item.type_of_mesh]);
   jQuery('#select_twsd_color_of_mesh').val([twsd_item.color_of_mesh]);
   jQuery('#text_twsd_hardware_type_line1').val(twsd_item.hardware_type_line1);
   jQuery('#text_twsd_quantity_or_side_line1').val(twsd_item.quantity_or_side_line1);
   jQuery('#text_twsd_number_of_sides_line1').val(twsd_item.number_of_sides_line1);
   jQuery('#text_twsd_frame_side_line1').val(twsd_item.frame_side_line1);
   jQuery('#text_twsd_distance_from_end_line1').val(twsd_item.distance_from_end_line1);
   jQuery('#text_twsd_hardware_type_line2').val(twsd_item.hardware_type_line2);
   jQuery('#text_twsd_quantity_or_side_line2').val(twsd_item.quantity_or_side_line2);
   jQuery('#text_twsd_number_of_sides_line2').val(twsd_item.number_of_sides_line2);
   jQuery('#text_twsd_frame_side_line2').val(twsd_item.frame_side_line2);
   jQuery('#text_twsd_distance_from_end_line2').val(twsd_item.distance_from_end_line2);
   jQuery('#textarea_twsd_comments').val(twsd_item.comments);
}

function twsd_print_item_to_console(debug_tag, twsd_item) {
   var log_tag = (debug_tag.length == 0 ? '' : (debug_tag + ': '));
   console.log(log_tag + 'twsd_id => ' + twsd_item.id);
   console.log(log_tag + 'twsd_job_id => ' + twsd_item.job_id);
   console.log(log_tag + 'twsd_screen_type => ' + twsd_item.screen_type);
   console.log(log_tag + 'twsd_exact_width => ' + twsd_item.exact_width);
   console.log(log_tag + 'twsd_exact_height => ' + twsd_item.exact_height);
   console.log(log_tag + 'twsd_frame_thickness => ' + twsd_item.frame_thickness);
   console.log(log_tag + 'twsd_frame_finish => ' + twsd_item.frame_finish);
   console.log(log_tag + 'twsd_type_of_mesh => ' + twsd_item.type_of_mesh);
   console.log(log_tag + 'twsd_color_of_mesh => ' + twsd_item.color_of_mesh);
   console.log(log_tag + 'twsd_hardware_type_line1 => ' + twsd_item.hardware_type_line1);
   console.log(log_tag + 'twsd_quantity_or_side_line1 => ' + twsd_item.quantity_or_side_line1);
   console.log(log_tag + 'twsd_number_of_sides_line1 => ' + twsd_item.number_of_sides_line1);
   console.log(log_tag + 'twsd_frame_side_line1 => ' + twsd_item.frame_side_line1);
   console.log(log_tag + 'twsd_distance_from_end_line1 => ' + twsd_item.distance_from_end_line1);
   console.log(log_tag + 'twsd_hardware_type_line2 => ' + twsd_item.hardware_type_line2);
   console.log(log_tag + 'twsd_quantity_or_side_line2 => ' + twsd_item.quantity_or_side_line2);
   console.log(log_tag + 'twsd_number_of_sides_line2 => ' + twsd_item.number_of_sides_line2);
   console.log(log_tag + 'twsd_frame_side_line2 => ' + twsd_item.frame_side_line2);
   console.log(log_tag + 'twsd_distance_from_end_line2 => ' + twsd_item.distance_from_end_line2);
   console.log(log_tag + 'twsd_comments => ' + twsd_item.comments);
   // console.log(log_tag + 'twsd_item => ' + JSON.stringify(twsd_item));
}

function twsd_load_sample_data() {
   var twsd_items = [];

   for (var i=0; i<3; i++) {
      var twsd_item = {};
      twsd_item.id = 0;
      twsd_item.job_id = job_id;
      twsd_item.screen_type = 'casement_screen';
      
      twsd_item.exact_width = '27.75';

      twsd_item.exact_height = '34.75';

      twsd_item.frame_thickness = '3_over_8';

      twsd_item.frame_finish = 'bronze';

      twsd_item.type_of_mesh = 'fiberglass';
      twsd_item.color_of_mesh = 'charcoal';
      twsd_item.hardware_type_line1 = 'VL';
      twsd_item.quantity_or_side_line1 = '';
      twsd_item.number_of_sides_line1 = '';
      twsd_item.frame_side_line1 = '';
      twsd_item.distance_from_end_line1 = '';
      twsd_item.hardware_type_line2 = 'PL';
      twsd_item.quantity_or_side_line2 = '';
      twsd_item.number_of_sides_line2 = '';
      twsd_item.frame_side_line2 = '';
      twsd_item.distance_from_end_line2 = '';
      twsd_item.comments = 'Screen details comment #: ' + (i + 1) + ' => Vaughan place/High rise/#213 \nLiving room Window\nVL - Located top center on the inside\nPL - Located top 5 inches over from top edge on the inside';


      twsd_items.push(twsd_item);
   }
   // console.log('twsd_load_sample_data:twsd_items => ' + JSON.stringify(twsd_items));
   return twsd_items;
}

function twsd_init(twsd_seed, twsd_seed_items) {
   twsd_current_item = -1;
   twsd_items = [];
   twsd_init_form_state();
   if (twsd_seed && twsd_seed_items.length > 0) {
      twsd_current_item = 0;
      twsd_items = twsd_seed_items;
      twsd_init_toolbar_navigation_buttons_state();
      twsd_init_toolbar_editing_buttons_state();
      twsd_show_current_item();
   }
   twsd_set_toolbar_navigation_buttons_state();
   twsd_editing = false;
   twsd_copying = false;
   twsd_clipboard = {};
   twsd_set_toolbar_editing_buttons_state();
}

jQuery(document).ready(function($) {
   $('#btn_twsd_add').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_editing = true;
      twsd_clear_form();
      twsd_set_toolbar_navigation_buttons_state();
      twsd_set_toolbar_editing_buttons_state();
      // set focus to first textbox
   });
   $('#btn_twsd_save').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      // validation happens here...
      var twsd_item = twsd_get_item_from_form();
      if (twsd_editing) {
         twsd_editing = false;
         twsd_items.push(twsd_item);
         if (twsd_current_item == -1 || twsd_items.length == 1) {
            twsd_current_item = 0;
         }
         twsd_show_current_item();
         twsd_set_toolbar_navigation_buttons_state();
         twsd_set_toolbar_editing_buttons_state();
      } else {
         twsd_items[twsd_current_item] = twsd_item;
      }
      // set focus to first textbox
   });
   $('#btn_twsd_delete').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      if (twsd_editing) {
         twsd_editing = false;
         if (!(twsd_current_item == -1) && !(twsd_items.length == 0)) {
            twsd_show_current_item();
         }
         twsd_set_toolbar_navigation_buttons_state();
         twsd_set_toolbar_editing_buttons_state();
      } else {
         if (!confirm('Delete this record?')) return;
         twsd_items.splice(twsd_current_item, 1);
         if (twsd_items.length == 0) {
            twsd_current_item = -1;
         } else {
            if (twsd_current_item == twsd_items.length) {
               twsd_current_item = twsd_items.length - 1;
            }
         }
         if (!(twsd_current_item == -1) && !(twsd_items.length == 0)) {
            twsd_show_current_item();
         } else {
            twsd_clear_form();
         }
         twsd_set_toolbar_navigation_buttons_state();
         twsd_set_toolbar_editing_buttons_state();
      }
      // set focus to first textbox
   });
   $('#btn_twsd_refresh').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_show_current_item();
   });
   $('#btn_twsd_copy').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_copying = true;
      twsd_clipboard = twsd_get_item_from_form();
      twsd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_twsd_paste').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_print_item_to_form(twsd_clipboard);
      twsd_clipboard = {};
      twsd_copying = false;
      twsd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_twsd_first').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_current_item = 0;
      twsd_show_current_item();
      twsd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_twsd_previous').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_current_item--;
      twsd_show_current_item();
      twsd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_twsd_next').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_current_item++;
      twsd_show_current_item();
      twsd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_twsd_last').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      twsd_current_item = twsd_items.length - 1;
      twsd_show_current_item();
      twsd_set_toolbar_navigation_buttons_state();
   });
});