let wpid = 0;
let geocoder;
let geo_options = {
  enableHighAccuracy: true,
  maximumAge        : 30000,
  timeout           : 27000
};
let address;
let device_location;
let address_location;

function geo_success(position) {
  device_location = position;
  geocoder.geocode( { 'address': address }, geocode_callback );
}

function geo_error(error) {
  alert('checkin.geo_error: ERROR(' + error.code + '): ' + error.message);
}

function init_geocoder() {
  geocoder = new google.maps.Geocoder();
}

function geocode_callback(results, status) {

  if (status == 'OK') {
    address_location = results[0].geometry.location;
    console.log('geocode_callback: address_location.lat() => ' + address_location.lat() + ', address_location.lng() => ' + address_location.lng());
    console.log('geocode_callback: device_location.coords.latitude => ' + device_location.coords.latitude + ', device_location.coords.longitude => ' + device_location.coords.longitude);
   // alert('geocode_callback: address_location.lat() => ' + address_location.lat() + ', address_location.lng() => ' + address_location.lng());
    //alert('geocode_callback: device_location.coords.latitude => ' + device_location.coords.latitude + ', device_location.coords.longitude => ' + device_location.coords.longitude);
//    var device_location.coords.latitude = ;
//	var device_location.coords.longitude = ;
	var dist = calculateDistance(device_location.coords.latitude, device_location.coords.longitude, address_location.lat(), address_location.lng());
    console.log('geocode_callback: dist => ' + dist);
    //alert('geocode_callback: dist => ' + dist);

	var nearCheck = '0.402336';
	 //alert(nearCheck);
	 console.log("DISTANCE IS "+ dist);
	 if( dist <= nearCheck ){
		var ajaxurl = $("#ajaxurlset").val();
		// alert(ajaxurl);
		 var distance  = dist;
		// alert("true");
		// alert(distance);
		  $.ajax({
				url :ajaxurl,
				data:{distance:distance,action:'add_distance'},
				dataType:'json',
				type:'POST',
				success:function(data){
					//alert(data);
					  if(data = true){
						 alert("You successfully checked in.");
						 }
					}
				  });


		 }else{
			  alert("Your did not checkin properly.");
			}

  } /*
  else if (status == 'OK')
  {
	  let result= ((calculateDistance(device_location.coords.latitude, device_location.coords.longitude, address_location.lat(), address_location.lng()) == '10'));
	   console.log('geocode_callback: result => ' + result);
	  // alert('jkjk');
  } */
  else{
    console.log("geocode_callback: status => " + status);
    //alert('Geocode was not successful for the following reason: ' + status);
	}
}

function calculateDistance(lat1, lon1, lat2, lon2) {

  let R = 6371;
  let dLat = (lat2 - lat1).toRad();

  let dLon = (lon2-lon1).toRad();

 let a = Math.sin(dLat/2) * Math.sin(dLat/2) +
 Math.cos(lat1.toRad()) * Math.cos(lat2.toRad()) * Math.sin(dLon/2) * Math.sin(dLon/2);

 let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));

 let d = R * c;

  return d;

}


Number.prototype.toRad = function() {

  return this * Math.PI / 180;

}



function validateAddressForm() {
   let text_project_address_line1 = jQuery('#text_project_address_line1')[0];
   if (text_project_address_line1.value.trim().length == 0) {
      text_project_address_line1.focus();
      return false;
   }
   if (!text_project_address_line1.checkValidity()) {
      text_project_address_line1.focus();
      return false;
   }
   let text_project_city = jQuery('#text_project_city')[0];
   if (text_project_city.value.trim().length == 0) {
      text_project_city.focus();
      return false;
   }
   if (!text_project_city.checkValidity()) {
      text_project_city.focus();
      return false;
   }
   let project_state = jQuery('#select_project_state').val();
   if (typeof project_state === 'undefined' || project_state == null) project_state = 'not_applicable';
   if (project_state.localeCompare('not_applicable') == 0) {
      jQuery('#select_project_state')[0].focus();
      return false;
   }
   let text_project_postal_code = jQuery('#text_project_postal_code')[0];
   if (text_project_postal_code.value.trim().length == 0) {
      text_project_postal_code.focus();
      return false;
   }
   if (!text_project_postal_code.checkValidity()) {
      text_project_postal_code.focus();
      return false;
   }
   return true;
}

function setAddress() {
  let project_address_line1 = jQuery('#text_project_address_line1')[0].value.trim();
  let project_address_line2 = jQuery('#text_project_address_line2')[0].value.trim();
  let project_city = jQuery('#text_project_city')[0].value;
  let project_state = jQuery('#select_project_state').val();
  let project_postal_code = jQuery('#text_project_postal_code')[0].value.trim();
  if (project_address_line2.length == 0) {
    address = project_address_line1 + ', ' + project_city + ', ' + project_state + ', ' + project_postal_code;
  } else {
    address = project_address_line1 + ', ' + project_address_line2 + ', ' + project_city + ', ' + project_state + ', ' + project_postal_code;
  }
console.log("setAddress: address => " + address);
}

function checkin(event) {
  navigator.geolocation.getCurrentPosition(geo_success, geo_error, geo_options);
}
jQuery(document).ready(function($) {
  init_geocoder();
  $("#checkin").click(function(event) {
    event.preventDefault();
    if (!navigator.geolocation) {
      alert('Geolocation is not supported by your browser');
      return false;
    }
    if (!validateAddressForm()) {
      return false;
    }
    setAddress();
    checkin(event);
  });
});


$(function() {
		$("#checkins-list").click(function () {

	      var ajaxurl1 = $("#ajaxurlset").val();
		  
		  jQuery("#ResultShow").closest(".modal-title").html("All CheckIns List");
		  
	      $.ajax({
				url :ajaxurl1,
				data:{action:'show_list'},
				type:'POST',
				success:function(data){
					   $("#ResultShow").html(data);
					}
				});
		});
		
		
		$("[id^=salesmanlist-note]").click(function () {
												//alert("sdfsdf");

	      var ajaxurl1 = $("#ajaxurlset").val();
		  var splitset=jQuery(this).attr("id").split("note_");
		 
		  
		  // $("#ResultShow").html("sdfsdfsd");
		   //alert("sdfsdf");
	      $.ajax({
				url :ajaxurl1,
				data:{action:'salesmanlist',job_id:splitset[1]},
				type:'POST',
				success:function(data){
					   $("#ResultShow").html(data);
					    jQuery(".modal-title").html("SalesMan List");
					}
				});
		});
		

});


