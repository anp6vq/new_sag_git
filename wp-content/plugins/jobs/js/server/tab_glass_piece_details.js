var tgpd_current_item = -1;
var tgpd_items = [];
var tgpd_editing = false;
var tgpd_editingsave = false;
var tgpd_copying = false;
var tgpd_copy_operation = -1;
var tgpd_clipboard = {};
var tgpd_pictures_download_url = [];
var tgpd_pictures_download_url_object = {};
var tgpd_sketches_download_url = [];
var tgpd_map_glass_set_value_to_fraction = {
   snap_beads_aluminum_interior : '+3/4',
   screw_beads_metal : '+3/4',
   snap_beads_vinyl_interior : '+3/4',
   snap_beads_aluminum_exterior : '+3/4',
   snap_beads_vinyl_exterior : '+3/4',
   snap_beads_wood_interior : '+3/4',
   snap_beads_wood_exterior : '+3/4',
   flush_1_over_4_top_exterior : '+5/8',
   flush_1_over_4_top_interior : '+5/8',
   flush_1_over_4_bottom_exterior : '+5/8',
   flush_1_over_4_bottom_interior : '+5/8',
   flush_1_over_4_left_right_exterior : '+5/8',
   flush_1_over_4_left_right_interior : '+5/8',
   flush_igu_top_exterior : '+7/8',
   flush_igu_top_interior : '+7/8',
   flush_igu_bottom_exterior : '+7/8',
   flush_igu_bottom_interior : '+7/8',
   flush_igu_left_right_exterior : '+7/8',
   flush_igu_left_right_interior : '+7/8',
   wrap_around_vinyl : '+3/4',
   wrap_around_aluminium : '+3/4',
   wrap_around_wood : '+3/4',
   wrap_around_patio_door : '+3/4',
   curtain_wall_pressure_plate_all_sides_captured : '+1',
   curtain_wall_pressure_plate_butt_glazed_left_only : '',
   curtain_wall_pressure_plate_butt_glazed_right_only : '',
   curtain_wall_pressure_plate_butt_glazed_left_right : '',
   curtain_wall_pressure_plate_butt_glazed_top_only : '',
   curtain_wall_pressure_plate_butt_glazed_bottom_only : '',
   curtain_wall_pressure_plate_without_clips_all_sides_captured : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_left_only : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_right_only : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_left_right : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_top_only : '',
   curtain_wall_pressure_plate_without_clips_butt_glazed_bottom_only : '',
   structurally_glazed_all_sides_butt_glazed : '',
   j_channel_bottom : '',
   j_channel_top : '',
   j_channel_top_bottom : '',
   j_channel_top_bottom_left_right : '',
   l_channel_bottom : '',
   l_channel_top : '',
   l_channel_top_bottom : '',
   l_channel_top_bottom_left_right : '',
   caulk_mastic_only : '',
   hack_and_glaze_steel_interior : '',
   hack_and_glaze_steel_exterior : '',
   hack_and_glaze_wood_interior : '',
   hack_and_glaze_wood_exterior : '',
   vision_kit_all_new : '',
   vision_kit_glass_only : '',
   u_channel_top_bottom : '',
   sash_bar : '',
   division_bar : '',
   seamless_mullion_7_8 : '+7/8'
};
var tgpd_picture_list = '<ul></ul>';
var tgpd_picture_list_item = '<li>' +
                                '<img src="#" alt="Glass Piece Photo">' +
                                '<span class="status"></span>' +
                                '<span class="info"></span>' +
                                '<div class="progressHolder">' +
                                   '<div class="progressBar"></div>' +
                                '</div>' +
                             '</li>';
var tgpd_sketch_list = '<ul></ul>';
var tgpd_sketch_list_item = '<li>' +
                                '<img src="#" alt="Glass Piece Sketch">' +
                                '<span class="status"></span>' +
                                '<span class="info"></span>' +
                                '<div class="progressHolder">' +
                                   '<div class="progressBar"></div>' +
                                '</div>' +
                             '</li>';

function tgpd_set_toolbar_navigation_item_count() {

   if (tgpd_editing) {
      jQuery('#text_tgpd_item_count').val('Add Item');
   } else {
      jQuery('#text_tgpd_item_count').val((tgpd_current_item + 1) + '/' + tgpd_items.length);
   }
}

function tgpd_init_form_state() {

   jQuery('#fieldset_tgpd_glass_type').hide();
   jQuery('#div_tgpd_glass_color').hide();
   jQuery('#fieldset_tgpd_glass_pricing_sag_or_quote').hide();
   jQuery('#div_tgpd_picture_filelist_controls').hide();
   jQuery('#div_upg_picture_filelist_controls_notes').hide();
   jQuery('#canvas_tgpd_glass_sketch').sketch();
   jQuery('#canvas_usg_glass_sketch_notes').sketch();
   jQuery('#div_tgpd_canvas_holder').hide();
   jQuery('#div_usg_canvas_holder_notes').hide();

   //----temp or lami glass pieces hide-----

   jQuery('#lamidrophide').hide();

   jQuery('#div_tgpd_sketch_filelist_controls').hide();
   jQuery('#div_usg_sketch_filelist_controls_notes').hide();

   jQuery('#grids_and_fabrication').hide();
   jQuery('#fieldset_tgpd_glass_grids').hide();
   jQuery('#fieldset_tgpd_glass_fabrication').hide();
   jQuery('#fieldset_tgpd_glass_reminders_solar_film1').hide();
   jQuery('#fieldset_tgpd_glass_reminders_solar_film2').hide();
   jQuery('#fieldset_tgpd_glass_reminders_wet_seal').hide();
   jQuery('#fieldset_tgpd_glass_reminders_furniture_to_move').hide();
   jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1').hide();
   jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2').hide();


}

function tgpd_init_toolbar_navigation_buttons_state() {
   jQuery('#btn_tgpd_first').attr('disabled','disabled');
   jQuery('#btn_tgpd_previous').attr('disabled','disabled');
   jQuery('#text_tgpd_item_count').attr('readonly','readonly');
   jQuery('#btn_tgpd_next').attr('disabled','disabled');
   jQuery('#btn_tgpd_last').attr('disabled','disabled');
   tgpd_set_toolbar_navigation_item_count();
}

function tgpd_init_toolbar_editing_buttons_state() {
   jQuery('#btn_tgpd_add').removeAttr('disabled');
   jQuery('#btn_tgpd_save').attr('disabled','disabled');
   jQuery('#btn_tgpd_delete').attr('disabled','disabled');
   jQuery('#btn_tgpd_refresh').attr('disabled','disabled');
   tgpd_init_toolbar_copy_paste_buttons_state();
}

function tgpd_init_toolbar_copy_paste_buttons_state() {
   jQuery('#btn_tgpd_copy_specs_plus_size').attr('disabled','disabled');
   jQuery('#btn_tgpd_copy_specs').attr('disabled','disabled');
   jQuery('#btn_tgpd_paste').attr('disabled','disabled');
}

function tgpd_set_toolbar_navigation_buttons_state() {
   if (tgpd_current_item == -1 || tgpd_items.length == 0 || tgpd_editing) {
      tgpd_init_toolbar_navigation_buttons_state();
      return;
   }
   if (tgpd_items.length == 1) {
      jQuery('#btn_tgpd_first').attr('disabled','disabled');
      jQuery('#btn_tgpd_previous').attr('disabled','disabled');
      jQuery('#btn_tgpd_next').attr('disabled','disabled');
      jQuery('#btn_tgpd_last').attr('disabled','disabled');
   } else {
      if (tgpd_current_item == 0) {
         jQuery('#btn_tgpd_first').attr('disabled','disabled');
         jQuery('#btn_tgpd_previous').attr('disabled','disabled');
         jQuery('#btn_tgpd_next').removeAttr('disabled');
         jQuery('#btn_tgpd_last').removeAttr('disabled');
      } else if (tgpd_current_item == (tgpd_items.length - 1)) {
         jQuery('#btn_tgpd_first').removeAttr('disabled');
         jQuery('#btn_tgpd_previous').removeAttr('disabled');
         jQuery('#btn_tgpd_next').attr('disabled','disabled');
         jQuery('#btn_tgpd_last').attr('disabled','disabled');
      } else {
         jQuery('#btn_tgpd_first').removeAttr('disabled');
         jQuery('#btn_tgpd_previous').removeAttr('disabled');
         jQuery('#btn_tgpd_next').removeAttr('disabled');
         jQuery('#btn_tgpd_last').removeAttr('disabled');
      }
   }
   tgpd_set_toolbar_navigation_item_count();
}

function tgpd_set_toolbar_editing_buttons_state() {
   if ((!tgpd_editing) && (!tgpd_copying) && (!(tgpd_current_item >= 0 && tgpd_current_item < tgpd_items.length))) {
      tgpd_init_toolbar_editing_buttons_state();
      jQuery('#btn_submit_new_job').removeAttr('disabled');
      return;
   }
   if (tgpd_editing) {
     //lucky jQuery('#btn_tgpd_add').attr('disabled','disabled');
	  jQuery('#btn_tgpd_save').attr('disabled','disabled');
      jQuery('#btn_tgpd_save').removeAttr('disabled');
      jQuery('#btn_tgpd_delete').removeAttr('disabled');
      jQuery('#btn_tgpd_refresh').attr('disabled','disabled');
		  jQuery('#btn_submit_new_job').attr('disabled','disabled');
   } else {
      jQuery('#btn_tgpd_add').removeAttr('disabled');
      if (tgpd_current_item >= 0 && tgpd_current_item < tgpd_items.length) {
         jQuery('#btn_tgpd_save').removeAttr('disabled');
         jQuery('#btn_tgpd_delete').removeAttr('disabled');
         jQuery('#btn_tgpd_refresh').removeAttr('disabled');
      }
      jQuery('#btn_submit_new_job').removeAttr('disabled');
   }
   tgpd_set_toolbar_copy_paste_buttons_state();
}

function tgpd_set_toolbar_copy_paste_buttons_state() {
   if (tgpd_editing) {
      jQuery('#btn_tgpd_copy_specs_plus_size').attr('disabled','disabled');
      jQuery('#btn_tgpd_copy_specs').attr('disabled','disabled');
      if (tgpd_copying) {
         jQuery('#btn_tgpd_paste').removeAttr('disabled');
      } else {
         jQuery('#btn_tgpd_paste').attr('disabled','disabled');
      }
   } else {
      if (tgpd_current_item >= 0 && tgpd_current_item < tgpd_items.length) {
         jQuery('#btn_tgpd_copy_specs_plus_size').removeAttr('disabled');
         jQuery('#btn_tgpd_copy_specs').removeAttr('disabled');
         if (tgpd_copying) {
            jQuery('#btn_tgpd_paste').removeAttr('disabled');
         } else {
            jQuery('#btn_tgpd_paste').attr('disabled','disabled');
         }
      }
   }
}

function tgpd_clear_form() {
   jQuery('#text_tgpd_glass_location').val('');
   jQuery('#text_tgpd_glass_quantity').val('1');
   jQuery('.text_tagd_shape_left_leg').val('');
   jQuery('.text_tagd_shape_width').val('');
   jQuery('.text_tagd_shape_right_leg').val('');
   jQuery('.text_tagd_shape_base').val('');
   jQuery('.text_tagd_shape_top').val('');
   jQuery('#select_tgpd_servicetype').val('');
   jQuery('#select_tgpd_glass_type').val(['not_applicable']);
   jQuery('#select_tgpd_glass_shape').val(['Rectangular/Square']);
   jQuery('#select_tgpd_glass_shape').trigger("change");
   tgpd_set_glass_type_state('not_applicable', false);
   jQuery('#select_tgpd_glass_treatment').val(['not_applicable']);
   jQuery('#select_tgpd_glass_color').val(['not_applicable']);
   tgpd_set_glass_color_state('not_applicable', false);
   jQuery('#text_tgpd_glass_lift').val('');
   jQuery('#select_tgpd_glass_set').val(['not_applicable']);
   jQuery('#text_tgpd_daylight_width').val('');
   jQuery('#text_tgpd_daylight_height').val('');
   jQuery('#text_tgpd_go_width').val('');
   jQuery('#text_tgpd_go_height').val('');
   jQuery('#text_tgpd_extra_width').val('');
    jQuery('#text_tgpd_extra_height').val('');

   jQuery('#select_tgpd_size_verification_status').val(['not_applicable']);
   tgpd_set_size_verification_status_state('not_applicable');
   jQuery('#select_tgpd_overall_thickness').val(['not_applicable']);
   jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked', false);
   tgpd_set_glass_pricing_sag_or_quote_state(false, false);
   tgpd_clear_glass_picture_filelist();
   btn_upg_clear_picture_filelist_notes();
   tgpd_clear_glass_sketch_filelist({ clear_canvas:true, hide_canvas:true });
   tgpd_clear_glass_sketch_filelist_notes({ clear_canvas:true, hide_canvas:true });
   jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked', false);
   tgpd_set_glass_reminders_solar_film1_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked', false);
   tgpd_set_glass_reminders_wet_seal_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked', false);
   tgpd_set_glass_reminders_furniture_to_move_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked', false);
   tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(false, false);
   jQuery('#checkbox_tgpd_glass_reminders_blind_needs_removing').prop('checked', false);
   jQuery('#checkbox_tgpd_glass_reminders_glass_fits_elevator').prop('checked', true);
   jQuery('#textarea_tgpd_instructions').val('');
   jQuery('#text_tgpd_glass_materials_caulk_amount').val('');
   jQuery('#select_tgpd_glass_materials_caulk_type').val(['not_applicable']);
   jQuery('#select_tgpd_glass_materials_scaffolding_type').val(['not_applicable']);
   jQuery('#text_tgpd_glass_materials_tape_amount').val('');
   jQuery('#select_tgpd_glass_materials_tape_type').val(['not_applicable']);
   jQuery('#text_tgpd_glass_materials_channel').val('');
   jQuery('#text_tgpd_miscellaneous').val('');
   jQuery('#checkbox_tgpd_glass_fabrication').prop('checked', false);
   tgpd_set_glass_fabrication_state(false, false);

   var tgpd_glass_fabrication = jQuery('#fieldset_tgpd_glass_fabrication');
var tgpd_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges');
var tgpd_fabrication_holes = jQuery('#text_tgpd_fabrication_holes');
var tgpd_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern');

tgpd_fabrication_polished_edges.val('');
tgpd_fabrication_holes.val('');
tgpd_fabrication_pattern.val('');

var tgpd_glass_grids = jQuery('#fieldset_tgpd_glass_grids');
var tgpd_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids');
var tgpd_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids');
var tgpd_grids_thickness = jQuery('#select_tgpd_grids_thickness');
var tgpd_grids_color = jQuery('#text_tgpd_grids_color');

tgpd_pattern_horizontal_grids.val('');
tgpd_pattern_vertical_grids.val('');
tgpd_grids_thickness.val(['not_applicable']);
tgpd_grids_color.val('');




}
function tgpd_set_glass_type_state(tgpd_glass_type, tgpd_focus) {
   var tgpd_glass_type_fields = jQuery('#fieldset_tgpd_glass_type');
   var tgpd_lite_thickness = jQuery('#select_tgpd_lite_thickness');
   var tgpd_spacer_color = jQuery('#select_tgpd_spacer_color');
   var tgpd_grids_and_fabrication = jQuery('#grids_and_fabrication');
   var tgpd_glass_grids = jQuery('#checkbox_tgpd_glass_grids');
   if (tgpd_glass_type.indexOf('igu') > -1) {
      tgpd_glass_type_fields.show();
      tgpd_grids_and_fabrication.show();
      if (tgpd_focus) {
         tgpd_lite_thickness[0].focus();
      }
   } else {
      tgpd_lite_thickness.val(['not_applicable']);
      tgpd_spacer_color.val(['not_applicable']);
      tgpd_glass_type_fields.hide();
      tgpd_set_glass_grids_state(false, false);
      tgpd_glass_grids.prop('checked', false);
      tgpd_grids_and_fabrication.hide();
   }
}

function tgpd_temp_lami_state(tgpd_glass_type){
  console.log("---inside tgpd_temp_lami_state")
  if(tgpd_glass_type  == "igu_sky_lite_temp_or_lami" || tgpd_glass_type  == "igu_lami_temp_lami"){ //requires extra thickness fields
    jQuery('#lamidrophide').show();
  } else{
    console.log("not sky lite tempLami and not IGU Temp Lami")
  }
}

function tgpd_set_glass_color_state(tgpd_glass_color, tgpd_focus) {
   var tgpd_glass_color_fields = jQuery('#div_tgpd_glass_color');
   var tgpd_glass_color_note = jQuery('#text_tgpd_glass_color_note');
   if (tgpd_glass_color.localeCompare('custom') == 0) {
      tgpd_glass_color_fields.show();
      if (tgpd_focus) {
         tgpd_glass_color_note[0].focus();
      }
   } else {
      tgpd_glass_color_note.val('');
      tgpd_glass_color_fields.hide();
   }
}

function tgpd_set_glass_grids_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_grids = jQuery('#fieldset_tgpd_glass_grids');
   var tgpd_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids');
   var tgpd_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids');
   var tgpd_grids_thickness = jQuery('#select_tgpd_grids_thickness');
   var tgpd_grids_color = jQuery('#text_tgpd_grids_color');
   if (tgpd_checked) {
      tgpd_glass_grids.show();
      if (tgpd_focus) {
         tgpd_pattern_horizontal_grids[0].focus();
      }
   } else {
     // tgpd_pattern_horizontal_grids.val('');
      //tgpd_pattern_vertical_grids.val('');
     // tgpd_grids_thickness.val(['not_applicable']);
     // tgpd_grids_color.val('');
      tgpd_glass_grids.hide();
   }
}

function tgpd_set_glass_fabrication_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_fabrication = jQuery('#fieldset_tgpd_glass_fabrication');
   var tgpd_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges');
   var tgpd_fabrication_holes = jQuery('#text_tgpd_fabrication_holes');
   var tgpd_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern');
   if (tgpd_checked) {
      tgpd_glass_fabrication.show();
      if (tgpd_focus) {
         tgpd_fabrication_polished_edges[0].focus();
      }
   } else {
      //tgpd_fabrication_polished_edges.val('');
     // tgpd_fabrication_holes.val('');
     // tgpd_fabrication_pattern.val('');
      tgpd_glass_fabrication.hide();
   }
}

function is_valid_fraction(value) {
   var is_valid = true;
   try {
      var f = new Fraction(value);
   } catch (e) {
      is_valid = false;
   }
   return is_valid;
}

function set_text_tgpd_go_width(fromSetDropdown) {
   var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
   var text_tgpd_daylight_width = jQuery('#text_tgpd_daylight_width');
     var verify_go_size_width = jQuery('#verify_go_size_width');
   var tgpd_daylight_width = text_tgpd_daylight_width[0].checkValidity() ? text_tgpd_daylight_width.val() : '';
   var text_tgpd_go_width = jQuery('#text_tgpd_go_width');
   text_tgpd_go_width.val('');verify_go_size_width.prop("checked",false);
   console.log('set_text_tgpd_go_width: tgpd_glass_set => ' + tgpd_glass_set + ', tgpd_daylight_width => ' + tgpd_daylight_width + ', text_tgpd_daylight_width.checkValidity() => ' + text_tgpd_daylight_width[0].checkValidity());
   if ((!(tgpd_glass_set.localeCompare('not_applicable')==0)) && (!(tgpd_daylight_width.localeCompare('')==0)) && is_valid_fraction(tgpd_daylight_width)) {
     var tgpd_glass_set_fraction;
     if(fromSetDropdown === true){
       console.log("fromSetDropdown === true");
       tgpd_glass_set_fraction = tgpd_map_glass_set_value_to_fraction[tgpd_glass_set];
     } else{
       console.log("fromSetDropdown === true");
       tgpd_glass_set_fraction = jQuery('#text_tgpd_extra_width option:selected').val();
       //from manual set width formula
     }

      console.log('set_text_tgpd_go_width: tgpd_glass_set_fraction => ' + tgpd_glass_set_fraction + ', is_valid_fraction(tgpd_glass_set_fraction) => ' + is_valid_fraction(tgpd_glass_set_fraction));
      if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
      var f = new Fraction(tgpd_glass_set_fraction);
      console.log('set_text_tgpd_go_width: tgpd_glass_set_fraction => ' + tgpd_glass_set_fraction + ', f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      f = f.add(tgpd_daylight_width);
      console.log('set_text_tgpd_go_width: f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      text_tgpd_go_width.val(f.toFraction(true));
      
      verify_go_size_width.prop("checked",true);
   }
}

function set_text_tgpd_go_height(fromSetDropdown) {
	verify_set_width
   var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
   var text_tgpd_daylight_height = jQuery('#text_tgpd_daylight_height');
   
     var verify_go_size_height = jQuery('#verify_go_size_height');
     
   var tgpd_daylight_height = text_tgpd_daylight_height[0].checkValidity() ? text_tgpd_daylight_height.val() : '';
   var text_tgpd_go_height = jQuery('#text_tgpd_go_height');
   text_tgpd_go_height.val('');verify_go_size_height.prop("checked",false);
   console.log('set_text_tgpd_go_height: tgpd_glass_set => ' + tgpd_glass_set + ', tgpd_daylight_height => ' + tgpd_daylight_height + ', text_tgpd_daylight_height.checkValidity() => ' + text_tgpd_daylight_height[0].checkValidity());
   if ((!(tgpd_glass_set.localeCompare('not_applicable')==0)) && (!(tgpd_daylight_height.localeCompare('')==0)) && is_valid_fraction(tgpd_daylight_height)) {
     var tgpd_glass_set_fraction;
     if(fromSetDropdown === true){
       tgpd_glass_set_fraction = tgpd_map_glass_set_value_to_fraction[tgpd_glass_set];
     } else{ // from manual set height formula
       tgpd_glass_set_fraction = jQuery('#text_tgpd_extra_height option:selected').val();
     }

      if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
      var f = new Fraction(tgpd_glass_set_fraction);
      console.log('set_text_tgpd_go_height: tgpd_glass_set_fraction => ' + tgpd_glass_set_fraction + ', f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      f = f.add(tgpd_daylight_height);
      console.log('set_text_tgpd_go_height: f.toFraction() => ' + f.toFraction(true) + ', f.sign => ' + f.s + ', f.numerator => ' + f.n + ', f.denominator => ' + f.d);
      text_tgpd_go_height.val(f.toFraction(true));
      verify_go_size_height.prop("checked",true);
   }
}

function fillSetFormula(){
  var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
  if ((!(tgpd_glass_set.localeCompare('not_applicable')==0)) ) {
    var tgpd_glass_set_fraction = tgpd_map_glass_set_value_to_fraction[tgpd_glass_set];
     if (!is_valid_fraction(tgpd_glass_set_fraction)) return;
     var f = new Fraction(tgpd_glass_set_fraction);
     jQuery('#text_tgpd_extra_width').val(f.toFraction(true));
     jQuery('#text_tgpd_extra_height').val(f.toFraction(true));
  }
}

function tgpd_set_size_verification_status_state(tgpd_size_verification_status) {
   var $tgpd_go_width = jQuery('#text_tgpd_go_width');
   var $tgpd_go_height = jQuery('#text_tgpd_go_height');
   switch (tgpd_size_verification_status) {
      case "verified":
         $tgpd_go_width.css({'background-color': '#64B058', 'color': 'white'});
         $tgpd_go_height.css({'background-color': '#64B058', 'color': 'white'});
         break;
      case "verify_on_order":
        $tgpd_go_width.css({'background-color': '#F0B94B', 'color': 'white'});
        $tgpd_go_height.css({'background-color': '#F0B94B', 'color': 'white'});
        break;
      case "block_size":
         $tgpd_go_width.css({'background-color': '#87cefa', 'color': 'white'});
         $tgpd_go_height.css({'background-color': '#87cefa', 'color': 'white'});
         break;
      default:
         $tgpd_go_width.css('background-color', '#ffffff');
         $tgpd_go_height.css('background-color', '#ffffff');
   }
}

function tgpd_set_glass_pricing_sag_or_quote_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_pricing_sag_or_quote = jQuery('#fieldset_tgpd_glass_pricing_sag_or_quote');
   var tgpd_glass_pricing_sag_or_quote_manufacturer = jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer');
   if (tgpd_checked) {
      tgpd_glass_pricing_sag_or_quote.show();
      if (tgpd_focus) {
         tgpd_glass_pricing_sag_or_quote_manufacturer[0].focus();
      }
   } else {
      tgpd_glass_pricing_sag_or_quote_manufacturer.val('');
      tgpd_glass_pricing_sag_or_quote.hide();
   }
}

function tgpd_set_glass_reminders_solar_film1_state(tgpd_checked, tgpd_focus) {
	//alert(tgpd_checked);
	if(tgpd_checked === 'undefined'){
		return;
	}

	//alert("tesssss");
   var tgpd_glass_reminders_solar_film1 = jQuery('#fieldset_tgpd_glass_reminders_solar_film1');
   var tgpd_glass_reminders_solar_film_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]');
   var tgpd_glass_treatment = jQuery('#select_tgpd_glass_treatment');
   if (tgpd_checked) {
      tgpd_glass_reminders_solar_film1.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_solar_film_responsibility[0].focus();
      }
      tgpd_set_glass_reminders_solar_film2_state(tgpd_glass_reminders_solar_film_responsibility.val(), true);
      tgpd_glass_treatment.val(['tempered']);
      //tgpd_glass_treatment.prop('disabled', true);
   } else {
      // tgpd_glass_reminders_solar_film_responsibility.val(['sag']);
      tgpd_glass_reminders_solar_film_responsibility.val(['']);
      tgpd_set_glass_reminders_solar_film2_state('', false);
      tgpd_glass_reminders_solar_film1.hide();
      tgpd_glass_treatment.prop('disabled', false);
      // tgpd_glass_treatment.val(['not_applicable']);
   }
}

function tgpd_set_glass_reminders_solar_film2_state(tgpd_solar_film_responsibility, tgpd_focus) {
   var tgpd_glass_reminders_solar_film2 = jQuery('#fieldset_tgpd_glass_reminders_solar_film2');
   var tgpd_glass_reminders_solar_film_type = jQuery('#text_tgpd_glass_reminders_solar_film_type');
   var tgpd_glass_reminders_solar_film_source = jQuery('#text_tgpd_glass_reminders_solar_film_source');
   if (tgpd_solar_film_responsibility.localeCompare('sag') == 0) {
      tgpd_glass_reminders_solar_film2.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_solar_film_type[0].focus();
      }
   } else {
      tgpd_glass_reminders_solar_film_type.val('');
      tgpd_glass_reminders_solar_film_source.val('');
      tgpd_glass_reminders_solar_film2.hide();
   }
}

function tgpd_set_glass_reminders_wet_seal_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_reminders_wet_seal = jQuery('#fieldset_tgpd_glass_reminders_wet_seal');
   var tgpd_glass_reminders_wet_seal_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]');
   if (tgpd_checked) {
      tgpd_glass_reminders_wet_seal.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_wet_seal_responsibility[0].focus();
      }
   } else {
      // tgpd_glass_reminders_wet_seal_responsibility.val(['sag']);
      tgpd_glass_reminders_wet_seal_responsibility.val(['']);
      tgpd_glass_reminders_wet_seal.hide();
   }
}

function tgpd_set_glass_reminders_furniture_to_move_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_reminders_furniture_to_move = jQuery('#fieldset_tgpd_glass_reminders_furniture_to_move');
   var tgpd_glass_reminders_furniture_to_move_comment = jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment');
   if (tgpd_checked) {
      tgpd_glass_reminders_furniture_to_move.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_furniture_to_move_comment[0].focus();
      }
   } else {
      tgpd_glass_reminders_furniture_to_move_comment.val('');
      tgpd_glass_reminders_furniture_to_move.hide();
   }
}

function tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(tgpd_checked, tgpd_focus) {
   var tgpd_glass_reminders_walls_or_ceilings_to_cut1 = jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut1');
   var tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]');
   if (tgpd_checked) {
      tgpd_glass_reminders_walls_or_ceilings_to_cut1.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility[0].focus();
      }
      tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.val(), true);
   } else {
      // tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.val(['sag']);
      tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.val(['']);
      tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state('', true);
      tgpd_glass_reminders_walls_or_ceilings_to_cut1.hide();
   }
}

function tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_walls_or_ceilings_to_cut_responsibility, tgpd_focus) {
   var tgpd_glass_reminders_walls_or_ceilings_to_cut2 = jQuery('#fieldset_tgpd_glass_reminders_walls_or_ceilings_to_cut2');
   var tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_comment = jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment');
   if (tgpd_walls_or_ceilings_to_cut_responsibility.localeCompare('sag') == 0 || tgpd_walls_or_ceilings_to_cut_responsibility.localeCompare('customer') == 0) {
      tgpd_glass_reminders_walls_or_ceilings_to_cut2.show();
      if (tgpd_focus) {
         tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_comment[0].focus();
      }
   } else {
      tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility_comment.val('');
      tgpd_glass_reminders_walls_or_ceilings_to_cut2.hide();
   }
}

function tgpd_show_current_item() {
   tgpd_clear_form();

   var tgpd_item = tgpd_items[tgpd_current_item];

   tgpd_print_item_to_form(tgpd_item);

}

function tgpd_get_item_from_form() {
   	var tgpd_glass_location = jQuery('#text_tgpd_glass_location').val();
   	var tgpd_glass_quantity = jQuery('#text_tgpd_glass_quantity').val();

	/*verify code start from here data 02 Nov, 2017*/
	var verify_tgpd_overall_thickness = jQuery('#verify_tgpd_overall_thickness').prop('checked') ? 1 : 0;

					var verify_tgpd_exterior_lite_thickness   =jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked') ? 1 : 0;
				var verify_tgpd_interior_lite_thickness   = jQuery('#verify_tgpd_interior_lite_thickness').prop('checked') ? 1 : 0;
				var verify_tgpd_inner_layer_thickness   = jQuery('#verify_tgpd_inner_layer_thickness').prop('checked') ? 1 : 0;

	var verify_tgpd_lite_thickness  = jQuery('#verify_tgpd_lite_thickness').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_treatment = jQuery('#verify_tgpd_glass_treatment').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_color  = jQuery('#verify_tgpd_glass_color').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_color_note = jQuery('#verify_tgpd_glass_color_note').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_set = jQuery('#verify_tgpd_glass_set').prop('checked') ? 1 : 0;
	var verify_tgpd_glass_lift = jQuery('#verify_tgpd_glass_lift').prop('checked') ? 1 : 0;
	var verify_tgpd_spacer_color = jQuery('#verify_tgpd_spacer_color').prop('checked') ? 1 : 0;
	var verify_tgpd_daylight_height = jQuery('#verify_tgpd_daylight_height').prop('checked') ? 1 : 0;
	var verify_tgpd_daylight_width = jQuery('#verify_tgpd_daylight_width').prop('checked') ? 1 : 0;
	
	
	var verify_set_height = jQuery('#verify_set_height').prop('checked') ? 1 : 0;
	var verify_set_width = jQuery('#verify_set_width').prop('checked') ? 1 : 0;




	var verify_go_size_height = jQuery('#verify_go_size_height').prop('checked') ? 1 : 0;
	var verify_go_size_width = jQuery('#verify_go_size_width').prop('checked') ? 1 : 0;

	var text_tgpd_extra_width = jQuery('#text_tgpd_extra_width').val();
	var text_tgpd_extra_height = jQuery('#text_tgpd_extra_height').val();
	/*verify code end at here data 02 Nov, 2017*/

	var tgpd_glass_servicetype = jQuery('#select_tgpd_servicetype').val();
   	var tgpd_glass_type = jQuery('#select_tgpd_glass_type').val();
   	var tgpd_glass_shape = jQuery('#select_tgpd_glass_shape').val();

   if (typeof tgpd_glass_shape === 'undefined' || tgpd_glass_shape == null) tgpd_glass_shape = 'not_applicable';
   if (tgpd_glass_shape.localeCompare('not_applicable') != 0) {
	   if(jQuery('#text_tagd_shape_left_leg_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_left_leg = jQuery('#text_tagd_shape_left_leg_'+tgpd_glass_shape).val();
	   }
	   if(jQuery('#text_tagd_shape_right_leg_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_right_leg = jQuery('#text_tagd_shape_right_leg_'+tgpd_glass_shape).val();
	   }
	   if(jQuery('#text_tagd_shape_height_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_height = jQuery('#text_tagd_shape_height_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_base_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_base = jQuery('#text_tagd_shape_base_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_side_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_side = jQuery('#text_tagd_shape_side_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_diameter_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_diameter = jQuery('#text_tagd_shape_diameter_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_radius_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_radius = jQuery('#text_tagd_shape_radius_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_leg_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_leg = jQuery('#text_tagd_shape_leg_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_radius_one_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_radius_one = jQuery('#text_tagd_shape_radius_one_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_radius_two_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_radius_two = jQuery('#text_tagd_shape_radius_two_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_length_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_length = jQuery('#text_tagd_shape_length_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_left_side_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_left_side = jQuery('#text_tagd_shape_left_side_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_right_side_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_right_side = jQuery('#text_tagd_shape_right_side_'+tgpd_glass_shape).val();
		}

		if(jQuery('#text_tagd_shape_left_offset_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_left_offset = jQuery('#text_tagd_shape_left_offset_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_top_offset_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_top_offset = jQuery('#text_tagd_shape_top_offset_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_right_offset_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_right_offset = jQuery('#text_tagd_shape_right_offset_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_top_'+tgpd_glass_shape).is(":visible")){
			var tgpd_glass_shape_top = jQuery('#text_tagd_shape_top_'+tgpd_glass_shape).val();
		}
		if(jQuery('#text_tagd_shape_width_'+tgpd_glass_shape).is(":visible")){
		var tgpd_glass_shape_width = jQuery('#text_tagd_shape_width_'+tgpd_glass_shape).val();
		}
   }

   if (typeof tgpd_glass_type === 'undefined' || tgpd_glass_type == null) tgpd_glass_type = 'not_applicable';
   var tgpd_lite_thickness = jQuery('#select_tgpd_lite_thickness').val();
   if (typeof tgpd_lite_thickness === 'undefined' || tgpd_lite_thickness == null) tgpd_lite_thickness = 'not_applicable';
   var tgpd_spacer_color = jQuery('#select_tgpd_spacer_color').val();
   if (typeof tgpd_spacer_color === 'undefined' || tgpd_spacer_color == null) tgpd_spacer_color = 'not_applicable';
   var tgpd_glass_treatment = jQuery('#select_tgpd_glass_treatment').val();
   if (typeof tgpd_glass_treatment === 'undefined' || tgpd_glass_treatment == null) tgpd_glass_treatment = 'not_applicable';
   var tgpd_glass_color = jQuery('#select_tgpd_glass_color').val();
   if (typeof tgpd_glass_color === 'undefined' || tgpd_glass_color == null) tgpd_glass_color = 'not_applicable';
   var tgpd_glass_color_note = jQuery('#text_tgpd_glass_color_note').val();
   var tgpd_glass_lift = jQuery('#text_tgpd_glass_lift').val();
   var tgpd_glass_set = jQuery('#select_tgpd_glass_set').val();
   if (typeof tgpd_glass_set === 'undefined' || tgpd_glass_set == null) tgpd_glass_set = 'not_applicable';
   var tgpd_daylight_width = jQuery('#text_tgpd_daylight_width').val();
   var tgpd_daylight_height = jQuery('#text_tgpd_daylight_height').val();
   var tgpd_go_width = jQuery('#text_tgpd_go_width').val();
   var tgpd_go_height = jQuery('#text_tgpd_go_height').val();
   var tgpd_size_verification_status = jQuery('#select_tgpd_size_verification_status').val();
   if (typeof tgpd_size_verification_status === 'undefined' || tgpd_size_verification_status == null) tgpd_size_verification_status = 'not_applicable';
   var tgpd_overall_thickness = jQuery('#select_tgpd_overall_thickness').val();
   if (typeof tgpd_overall_thickness === 'undefined' || tgpd_overall_thickness == null) tgpd_overall_thickness = 'not_applicable';
   var tgpd_sag_or_quote = jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked') ? 1 : 0;
   var tgpd_manufacturer = jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer').val();
   var tgpd_solar_film = jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked') ? 1 : 0;
   var tgpd_solar_film_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]:checked').val();
   if (typeof tgpd_solar_film_responsibility === 'undefined' || tgpd_solar_film_responsibility == null) tgpd_solar_film_responsibility = '';
   var tgpd_solar_film_type = jQuery('#text_tgpd_glass_reminders_solar_film_type').val();
   var tgpd_solar_film_source = jQuery('#text_tgpd_glass_reminders_solar_film_source').val();
   var tgpd_wet_seal = jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked') ? 1 : 0;
   var tgpd_wet_seal_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]:checked').val();
   if (typeof tgpd_wet_seal_responsibility === 'undefined' || tgpd_wet_seal_responsibility == null) tgpd_wet_seal_responsibility = '';
   var tgpd_furniture_to_move = jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked') ? 1 : 0;
   var tgpd_furniture_to_move_comment = jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment').val();
   var tgpd_walls_or_ceilings_to_cut = jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked') ? 1 : 0;
   var tgpd_walls_or_ceilings_to_cut_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]:checked').val();
   if (typeof tgpd_walls_or_ceilings_to_cut_responsibility === 'undefined' || tgpd_walls_or_ceilings_to_cut_responsibility == null) tgpd_walls_or_ceilings_to_cut_responsibility = '';
   var tgpd_walls_or_ceilings_to_cut_comment = jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment').val();
   var tgpd_blind_needs_removing = jQuery('#checkbox_tgpd_glass_reminders_blind_needs_removing').prop('checked') ? 1 : 0;
   var tgpd_glass_fits_elevator = jQuery('#checkbox_tgpd_glass_reminders_glass_fits_elevator').prop('checked') ? 1 : 0;
   var tgpd_instructions = jQuery('#textarea_tgpd_instructions').val();
   var tgpd_caulk_amount = jQuery('#text_tgpd_glass_materials_caulk_amount').val();
   var tgpd_caulk_type = jQuery('#select_tgpd_glass_materials_caulk_type').val();
   if (typeof tgpd_caulk_type === 'undefined' || tgpd_caulk_type == null) tgpd_caulk_type = 'not_applicable';
   var tgpd_scaffolding_type = jQuery('#select_tgpd_glass_materials_scaffolding_type').val();
   if (typeof tgpd_scaffolding_type === 'undefined' || tgpd_scaffolding_type == null) tgpd_scaffolding_type = 'not_applicable';
   var tgpd_tape_amount = jQuery('#text_tgpd_glass_materials_tape_amount').val();
   var tgpd_tape_type = jQuery('#select_tgpd_glass_materials_tape_type').val();
   if (typeof tgpd_tape_type === 'undefined' || tgpd_tape_type == null) tgpd_tape_type = 'not_applicable';
   var tgpd_channel = jQuery('#text_tgpd_glass_materials_channel').val();
   var tgpd_miscellaneous = jQuery('#text_tgpd_miscellaneous').val();
   var tgpd_glass_grids = jQuery('#checkbox_tgpd_glass_grids').prop('checked') ? 1 : 0;
   var tgpd_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids').val();
   var tgpd_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids').val();
   var tgpd_grids_thickness = jQuery('#select_tgpd_grids_thickness').val();
   if (typeof tgpd_grids_thickness === 'undefined' || tgpd_grids_thickness == null) tgpd_grids_thickness = 'not_applicable';
   var tgpd_grids_color = jQuery('#text_tgpd_grids_color').val();
   var tgpd_glass_fabrication = jQuery('#checkbox_tgpd_glass_fabrication').prop('checked') ? 1 : 0;
   var tgpd_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges').val();
   var tgpd_fabrication_holes = jQuery('#text_tgpd_fabrication_holes').val();
   var tgpd_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern').val();




var exterior_light_thickness = jQuery('#exterior_light_thickness').val();
   var interior_light_thickness = jQuery('#interior_light_thickness').val();
   var select_tgpd_innner_layer_thickness = jQuery('#select_tgpd_innner_layer_thickness').val();
   var custom_layer_thickness = jQuery('#custom_layer_thickness').val();

  if(job_id>0)
  	{
		//tgpd_pictures_download_url=[];

				jQuery("#div_tgpd_picture_filelist").find("ul li").each(function(){

				var htmlset=jQuery(this).html();

				var srcdd=	jQuery(this).find("img").attr("src");
			if(srcdd.indexOf("blob:http")<0 && jQuery.inArray(srcdd, tgpd_pictures_download_url)  == -1)
				{
				tgpd_pictures_download_url.push(srcdd);
				}

				});

	}

	 if(job_id>0)
  	{
		//tgpd_sketches_download_url=[];
				jQuery("#div_tgpd_sketch_filelist").find("ul li").each(function(){

				var htmlset=jQuery(this).html();

				var srcdd=	jQuery(this).find("img").attr("src");
				//alert(srcdd);
				if(srcdd.indexOf("blob:http")<0 && jQuery.inArray(srcdd, tgpd_sketches_download_url) == -1)
				{
				tgpd_sketches_download_url.push(srcdd);
				}

				});

				//alert(tgpd_sketches_download_url.length);

	}


//alert(tgpd_pictures_download_url.length);
   var tgpd_item = {};


   	/*verify code start from here data 02 Nov, 2017*/
	tgpd_item.verify_tgpd_overall_thickness =verify_tgpd_overall_thickness ;


	tgpd_item.verify_tgpd_exterior_lite_thickness   = verify_tgpd_exterior_lite_thickness;
	tgpd_item.verify_tgpd_interior_lite_thickness   =verify_tgpd_interior_lite_thickness;
	tgpd_item.verify_tgpd_inner_layer_thickness  = verify_tgpd_inner_layer_thickness;


	tgpd_item.verify_tgpd_lite_thickness  = verify_tgpd_lite_thickness;
	tgpd_item.verify_tgpd_glass_treatment = verify_tgpd_glass_treatment;
	tgpd_item.verify_tgpd_glass_color  = verify_tgpd_glass_color;
	tgpd_item.verify_tgpd_glass_color_note = verify_tgpd_glass_color_note;
	tgpd_item.verify_tgpd_glass_set = verify_tgpd_glass_set;
	tgpd_item.verify_tgpd_glass_lift = verify_tgpd_glass_lift;
	tgpd_item.verify_tgpd_spacer_color = verify_tgpd_spacer_color;
	tgpd_item.verify_tgpd_daylight_height = verify_tgpd_daylight_height;
	tgpd_item.verify_tgpd_daylight_width = verify_tgpd_daylight_width;
	tgpd_item.verify_go_size_height = verify_go_size_height;
	tgpd_item.verify_go_size_width = verify_go_size_width;


	tgpd_item.verify_set_height = verify_set_height;
	
	tgpd_item.verify_set_width = verify_set_width;
	


	tgpd_item.text_tgpd_extra_height = text_tgpd_extra_height;
	tgpd_item.text_tgpd_extra_width = text_tgpd_extra_width;
	/*verify code end at here data 02 Nov, 2017*/


   tgpd_item.id = 0;
   tgpd_item.job_id = job_id;
   tgpd_item.glass_location = tgpd_glass_location;
   tgpd_item.glass_quantity = tgpd_glass_quantity;
   if(jQuery('#text_tagd_shape_left_leg_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_left_leg = tgpd_glass_shape_left_leg;
   }
   if(jQuery('#text_tagd_shape_right_leg_'+tgpd_glass_shape).is(":visible")){
   		tgpd_item.glass_shape_right_leg = tgpd_glass_shape_right_leg;
   }
   if(jQuery('#text_tagd_shape_width_'+tgpd_glass_shape).is(":visible")){
   		tgpd_item.glass_shape_width = tgpd_glass_shape_width;
   }
   if(jQuery('#text_tagd_shape_height_'+tgpd_glass_shape).is(":visible")){
   		tgpd_item.glass_shape_height = tgpd_glass_shape_height;
   }

   if(jQuery('#text_tagd_shape_base_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_base = tgpd_glass_shape_base;
   }
   if(jQuery('#text_tagd_shape_side_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_side = tgpd_glass_shape_side;
   }
   if(jQuery('#text_tagd_shape_diameter_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_diameter = tgpd_glass_shape_diameter;
   }
   if(jQuery('#text_tagd_shape_radius_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_radius = tgpd_glass_shape_radius;
   }
   if(jQuery('#text_tagd_shape_radius_one_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_radius_one = tgpd_glass_shape_radius_one;
   }
   if(jQuery('#text_tagd_shape_radius_two_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_radius_two = tgpd_glass_shape_radius_two;
   }
   if(jQuery('#text_tagd_shape_length_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_length = tgpd_glass_shape_length;
   }
   if(jQuery('#text_tagd_shape_leg_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_leg = tgpd_glass_shape_leg;
   }
   if(jQuery('#text_tagd_shape_left_side_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_left_side = tgpd_glass_shape_left_side;
   }
   if(jQuery('#text_tagd_shape_right_side_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_right_side = tgpd_glass_shape_right_side;
   }
   if(jQuery('#text_tagd_shape_left_offset_'+tgpd_glass_shape).is(":visible")){
   		tgpd_item.glass_shape_left_offset = tgpd_glass_shape_left_offset;
   }
   if(jQuery('#text_tagd_shape_top_offset_'+tgpd_glass_shape).is(":visible")){
   		tgpd_item.glass_shape_top_offset = tgpd_glass_shape_top_offset;
   }
   if(jQuery('#text_tagd_shape_right_offset_'+tgpd_glass_shape).is(":visible")){
   		tgpd_item.glass_shape_right_offset = tgpd_glass_shape_right_offset;
   }
   if(jQuery('#text_tagd_shape_top_'+tgpd_glass_shape).is(":visible")){
   	tgpd_item.glass_shape_top = tgpd_glass_shape_top;
   }
   tgpd_item.glass_servicetype = tgpd_glass_servicetype;
   tgpd_item.glass_type = tgpd_glass_type;
   tgpd_item.glass_shape = tgpd_glass_shape;
   tgpd_item.lite_thickness = tgpd_lite_thickness;
   tgpd_item.spacer_color = tgpd_spacer_color;
   tgpd_item.glass_treatment = tgpd_glass_treatment;
   tgpd_item.glass_color = tgpd_glass_color;
   tgpd_item.glass_color_note = tgpd_glass_color_note;
   tgpd_item.glass_lift = tgpd_glass_lift;
   tgpd_item.glass_set = tgpd_glass_set;
   tgpd_item.daylight_width = tgpd_daylight_width;
   tgpd_item.daylight_height = tgpd_daylight_height;
   tgpd_item.go_width = tgpd_go_width;
   tgpd_item.go_height = tgpd_go_height;
   tgpd_item.size_verification_status = tgpd_size_verification_status;
   tgpd_item.overall_thickness = tgpd_overall_thickness;
   tgpd_item.sag_or_quote = tgpd_sag_or_quote;
   tgpd_item.manufacturer = tgpd_manufacturer;
   tgpd_item.solar_film = tgpd_solar_film;
   tgpd_item.solar_film_responsibility = tgpd_solar_film_responsibility;
   tgpd_item.solar_film_type = tgpd_solar_film_type;
   tgpd_item.solar_film_source = tgpd_solar_film_source;
   tgpd_item.wet_seal = tgpd_wet_seal;
   tgpd_item.wet_seal_responsibility = tgpd_wet_seal_responsibility;
   tgpd_item.furniture_to_move = tgpd_furniture_to_move;
   tgpd_item.furniture_to_move_comment = tgpd_furniture_to_move_comment;
   tgpd_item.walls_or_ceilings_to_cut = tgpd_walls_or_ceilings_to_cut;
   tgpd_item.walls_or_ceilings_to_cut_responsibility = tgpd_walls_or_ceilings_to_cut_responsibility;
   tgpd_item.walls_or_ceilings_to_cut_comment = tgpd_walls_or_ceilings_to_cut_comment;
   tgpd_item.blind_needs_removing = tgpd_blind_needs_removing;
   tgpd_item.glass_fits_elevator = tgpd_glass_fits_elevator;
   tgpd_item.pictures_download_url = tgpd_pictures_download_url.length > 0 ? tgpd_pictures_download_url.join() : '';
   tgpd_item.sketches_download_url = tgpd_sketches_download_url.length > 0 ? tgpd_sketches_download_url.join() : '';
   tgpd_item.instructions = tgpd_instructions;
   tgpd_item.caulk_amount = tgpd_caulk_amount;
   tgpd_item.caulk_type = tgpd_caulk_type;
   tgpd_item.scaffolding_type = tgpd_scaffolding_type;
   tgpd_item.tape_amount = tgpd_tape_amount;
   tgpd_item.tape_type = tgpd_tape_type;
   tgpd_item.channel = tgpd_channel;
   tgpd_item.miscellaneous = tgpd_miscellaneous;
   tgpd_item.glass_grids = tgpd_glass_grids;
   tgpd_item.pattern_horizontal_grids = tgpd_pattern_horizontal_grids;
   tgpd_item.pattern_vertical_grids = tgpd_pattern_vertical_grids;
   tgpd_item.grids_thickness = tgpd_grids_thickness;
   tgpd_item.grids_color = tgpd_grids_color;
   tgpd_item.glass_fabrication = tgpd_glass_fabrication;
   tgpd_item.fabrication_polished_edges = tgpd_fabrication_polished_edges;
   tgpd_item.fabrication_holes = tgpd_fabrication_holes;
   tgpd_item.fabrication_pattern = tgpd_fabrication_pattern;



   tgpd_item.exterior_light_thickness = exterior_light_thickness;
   tgpd_item.interior_light_thickness = interior_light_thickness;
   tgpd_item.select_tgpd_innner_layer_thickness = select_tgpd_innner_layer_thickness;
   tgpd_item.custom_layer_thickness = custom_layer_thickness;
   tgpd_item.daylight_enable  = jQuery('#text_tgpd_daylight').prop('checked')?1:0;
   tgpd_item.gosize_enable   = jQuery('#text_tgpd_gosize').prop('checked')?1:0;

   tgpd_item.color_viewer   =  jQuery('#checkbox_tgpd_glass_reminders_add_color_waiver').prop('checked')?jQuery('#checkbox_tgpd_glass_reminders_add_color_waiver').val():0;

   tgpd_item.damage_viewer    =  jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').prop('checked')?jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').val():0;


   tgpd_print_item_to_console('tgpd_get_item_from_form', tgpd_item);

   return tgpd_item;
}

function tgpd_print_item_to_form(tgpd_item) {

   jQuery('#text_tgpd_glass_location').val(tgpd_item.glass_location);
   jQuery('#text_tgpd_glass_quantity').val(tgpd_item.glass_quantity);
   jQuery('#select_tgpd_servicetype').val([tgpd_item.glass_servicetype]);
  // jQuery('#select_tgpd_glass_type').val([tgpd_item.glass_type]);

  // select_tgpd_glass_type
   //alert("sdjkfhsdjkf");

  jQuery('#select_tgpd_glass_shape').val([tgpd_item.glass_shape]).trigger("change");
  jQuery('#text_tagd_shape_left_leg_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_left_leg);
  jQuery('#text_tagd_shape_right_leg_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_right_leg);
  jQuery('#text_tagd_shape_width_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_width);
  jQuery('#text_tagd_shape_base_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_base);
  jQuery('#text_tagd_shape_leg_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_leg);
  jQuery('#text_tagd_shape_side_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_side);
  jQuery('#text_tagd_shape_diameter_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_diameter);
  jQuery('#text_tagd_shape_radius_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_radius);
  jQuery('#text_tagd_shape_radius_one_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_radius_one);
  jQuery('#text_tagd_shape_radius_two_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_radius_two);
  jQuery('#text_tagd_shape_length_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_length);
  jQuery('#text_tagd_shape_left_side_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_left_side);
  jQuery('#text_tagd_shape_right_side_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_right_side);
  jQuery('#text_tagd_shape_height_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_height);
  jQuery('#text_tagd_shape_left_offset_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_left_offset);
  jQuery('#text_tagd_shape_top_offset_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_top_offset);
  jQuery('#text_tagd_shape_right_offset_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_right_offset);
  jQuery('#text_tagd_shape_top_'+[tgpd_item.glass_shape]).val(tgpd_item.glass_shape_top);


  jQuery('#select_tgpd_glass_type').val([tgpd_item.glass_type]).trigger("change");

  jQuery('#select_tgpd_servicetype').val([tgpd_item.glass_servicetype]).trigger("change");


   tgpd_set_glass_type_state(tgpd_item.glass_type, false);
   jQuery('#select_tgpd_lite_thickness').val([tgpd_item.lite_thickness]);
   tgpd_temp_lami_state(jQuery('#select_tgpd_glass_type').val(), false);
   jQuery('#exterior_light_thickness').val(tgpd_item.exterior_light_thickness);
   jQuery('#interior_light_thickness').val(tgpd_item.interior_light_thickness);
   jQuery('#select_tgpd_innner_layer_thickness').val([tgpd_item.select_tgpd_innner_layer_thickness]);
   jQuery('#custom_layer_thickness').val(tgpd_item.custom_layer_thickness);

   jQuery('#select_tgpd_spacer_color').val([tgpd_item.spacer_color]);
   jQuery('#select_tgpd_glass_treatment').val([tgpd_item.glass_treatment]);
   jQuery('#select_tgpd_glass_treatment').prop('disabled', false);
   jQuery('#select_tgpd_glass_color').val([tgpd_item.glass_color]);
   jQuery('#text_tgpd_glass_color_note').val(tgpd_item.glass_color_note);
   jQuery('#text_tgpd_glass_lift').val(tgpd_item.glass_lift);
   jQuery('#select_tgpd_glass_set').val([tgpd_item.glass_set]);
   if (!tgpd_copying || (tgpd_copying && tgpd_copy_operation == 0)) {
      jQuery('#text_tgpd_daylight_width').val(tgpd_item.daylight_width);
      jQuery('#text_tgpd_daylight_height').val(tgpd_item.daylight_height);
      jQuery('#text_tgpd_go_width').val(tgpd_item.go_width);
      jQuery('#text_tgpd_go_height').val(tgpd_item.go_height);
      jQuery('#select_tgpd_size_verification_status').val([tgpd_item.size_verification_status]);
      tgpd_set_size_verification_status_state(tgpd_item.size_verification_status);
   }
   jQuery('#select_tgpd_overall_thickness').val([tgpd_item.overall_thickness]);

   /*verify data from db to frontend start here date @02-11-17*/

	jQuery('#verify_tgpd_overall_thickness').prop('checked', tgpd_item.verify_tgpd_overall_thickness == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked', tgpd_item.verify_tgpd_exterior_lite_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_interior_lite_thickness').prop('checked', tgpd_item.verify_tgpd_interior_lite_thickness == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_inner_layer_thickness').prop('checked', tgpd_item.verify_tgpd_inner_layer_thickness == 1 ? true : false).addClass('verify-active');


	jQuery('#verify_tgpd_lite_thickness').prop('checked', tgpd_item.verify_tgpd_lite_thickness == 1 ? true : false).addClass('verify-active');

  jQuery('#verify_tgpd_glass_set').prop('checked', tgpd_item.verify_tgpd_glass_set == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_glass_treatment').prop('checked', tgpd_item.verify_tgpd_glass_treatment == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_glass_color').prop('checked', tgpd_item.verify_tgpd_glass_color == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_glass_color_note').prop('checked', tgpd_item.verify_tgpd_glass_color_note == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_glass_lift').prop('checked', tgpd_item.verify_tgpd_glass_lift == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_spacer_color').prop('checked', tgpd_item.verify_tgpd_spacer_color == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_daylight_height').prop('checked', tgpd_item.verify_tgpd_daylight_height == 1 ? true : false).addClass('verify-active');

	jQuery('#verify_tgpd_daylight_height').prop('checked', tgpd_item.verify_tgpd_daylight_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_tgpd_daylight_width').prop('checked', tgpd_item.verify_tgpd_daylight_width == 1 ? true : false).addClass('verify-active');
	
	
	
	jQuery('#verify_set_height').prop('checked', tgpd_item.verify_set_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_set_width').prop('checked', tgpd_item.verify_set_width == 1 ? true : false).addClass('verify-active');
	





	jQuery('#verify_go_size_height').prop('checked', tgpd_item.verify_go_size_height == 1 ? true : false).addClass('verify-active');
	jQuery('#verify_go_size_width').prop('checked', tgpd_item.verify_go_size_width == 1 ? true : false).addClass('verify-active');

	 jQuery('#text_tgpd_extra_height').val(tgpd_item.text_tgpd_extra_height);
	 jQuery('#text_tgpd_extra_width').val(tgpd_item.text_tgpd_extra_width);
   /*verify data from db to frontend code end at here date @02-11-17*/


   var tgpd_sag_or_quote = tgpd_item.sag_or_quote == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked', tgpd_sag_or_quote);


   tgpd_set_glass_pricing_sag_or_quote_state(tgpd_sag_or_quote, false);
   jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer').val(tgpd_item.manufacturer);
   var tgpd_solar_film = tgpd_item.solar_film == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked', tgpd_solar_film);
   tgpd_set_glass_reminders_solar_film1_state(tgpd_solar_film, false);
   jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]').val([tgpd_item.solar_film_responsibility]);
   tgpd_set_glass_reminders_solar_film2_state(tgpd_item.solar_film_responsibility, false);
   jQuery('#text_tgpd_glass_reminders_solar_film_type').val(tgpd_item.solar_film_type);
   jQuery('#text_tgpd_glass_reminders_solar_film_source').val(tgpd_item.solar_film_source);
   var tgpd_wet_seal = tgpd_item.wet_seal == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked', tgpd_wet_seal);
   tgpd_set_glass_reminders_wet_seal_state(tgpd_wet_seal, false);
   jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]').val([tgpd_item.wet_seal_responsibility]);
   var tgpd_furniture_to_move = tgpd_item.furniture_to_move == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked', tgpd_furniture_to_move);
   tgpd_set_glass_reminders_furniture_to_move_state(tgpd_furniture_to_move, false);
   jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment').val(tgpd_item.furniture_to_move_comment);
   var tgpd_walls_or_ceilings_to_cut = tgpd_item.walls_or_ceilings_to_cut == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked', tgpd_walls_or_ceilings_to_cut);
   tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(tgpd_walls_or_ceilings_to_cut, false);
   jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]').val([tgpd_item.walls_or_ceilings_to_cut_responsibility]);
   tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_item.walls_or_ceilings_to_cut_responsibility, false);
   jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment').val(tgpd_item.walls_or_ceilings_to_cut_comment);
   jQuery('#checkbox_tgpd_glass_reminders_blind_needs_removing').prop('checked', tgpd_item.blind_needs_removing == 1 ? true : false);
   jQuery('#checkbox_tgpd_glass_reminders_glass_fits_elevator').prop('checked', tgpd_item.glass_fits_elevator == 1 ? true : false);
   if (!tgpd_copying) {
	  // alert("kljk"+tgpd_item.pictures_download_url.length);
      if (tgpd_item.pictures_download_url.length > 0) tgpd_pictures_download_url = tgpd_item.pictures_download_url.split(',');
      tgpd_print_pictures_to_form();
      if (tgpd_item.sketches_download_url.length > 0) tgpd_sketches_download_url = tgpd_item.sketches_download_url.split(',');
      tgpd_print_sketches_to_form();
   }


   jQuery('#textarea_tgpd_instructions').val(tgpd_item.instructions);
   jQuery('#text_tgpd_glass_materials_caulk_amount').val(tgpd_item.caulk_amount);
   jQuery('#select_tgpd_glass_materials_caulk_type').val([tgpd_item.caulk_type]);
   jQuery('#select_tgpd_glass_materials_scaffolding_type').val([tgpd_item.scaffolding_type]);
   jQuery('#text_tgpd_glass_materials_tape_amount').val(tgpd_item.tape_amount);
   jQuery('#select_tgpd_glass_materials_tape_type').val([tgpd_item.tape_type]);
   jQuery('#text_tgpd_glass_materials_channel').val(tgpd_item.channel);
   jQuery('#text_tgpd_miscellaneous').val(tgpd_item.miscellaneous);
   var tgpd_glass_grids = tgpd_item.glass_grids == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_grids').prop('checked', tgpd_glass_grids);
   tgpd_set_glass_grids_state(tgpd_glass_grids, false);
   jQuery('#number_tgpd_pattern_horizontal_grids').val(tgpd_item.pattern_horizontal_grids);
   jQuery('#number_tgpd_pattern_vertical_grids').val(tgpd_item.pattern_vertical_grids);
   jQuery('#select_tgpd_grids_thickness').val([tgpd_item.grids_thickness]);
   jQuery('#text_tgpd_grids_color').val(tgpd_item.grids_color);
   var tgpd_glass_fabrication = tgpd_item.glass_fabrication == 1 ? true : false;
   jQuery('#checkbox_tgpd_glass_fabrication').prop('checked', tgpd_glass_fabrication);
   tgpd_set_glass_fabrication_state(tgpd_glass_fabrication, false);
   jQuery('#text_tgpd_fabrication_polished_edges').val(tgpd_item.fabrication_polished_edges);
   jQuery('#text_tgpd_fabrication_holes').val(tgpd_item.fabrication_holes);
   jQuery('#text_tgpd_fabrication_pattern').val(tgpd_item.fabrication_pattern);

      var daylight_enable  = tgpd_item.daylight_enable  == 1 ? true : false;
	//  alert(daylight_enable);
  /* jQuery('#text_tgpd_daylight').trigger("change").attr("checked",daylight_enable);
   if(tgpd_item.daylight_enable  == 1)
   {
		$("#daylightsetglasspiece").hide();
   }
   else
   {
		$("#daylightsetglasspiece").show();
   }


    var gosize_enable   = tgpd_item.gosize_enable   == 1 ? true : false;
	//alert(gosize_enable);
   jQuery('#text_tgpd_gosize').trigger("change").attr("checked",gosize_enable);

    if(tgpd_item.gosize_enable  == 1)
   {
		 $("#div_tgpd_go_size").css("display","none");
   }
   else
   {
		$("#div_tgpd_go_size").css("display","block");
   }
*/
 var daylight_enable  = tgpd_item.daylight_enable  == 1 ? true : false;
	//  alert(daylight_enable);
   $('#text_tgpd_daylight').trigger("change").attr("checked",daylight_enable);
   $('#text_tgpd_daylight').trigger("change");
   
    var gosize_enable   = tgpd_item.gosize_enable   == 1 ? true : false;
	//alert(gosize_enable);
   $('#text_tgpd_gosize').trigger("change").attr("checked",gosize_enable);
   $('#text_tgpd_gosize').trigger("change");
    var color_viewer   = tgpd_item.color_viewer   == "add_color_waiver" ? true : false;
	//  alert(daylight_enable);
   jQuery('#checkbox_tgpd_glass_reminders_add_color_waiver').attr("checked",color_viewer);

    var damage_viewer   = tgpd_item.damage_viewer   == "damage_waiver" ? true : false;
	//alert(gosize_enable);
   jQuery('#checkbox_tgpd_glass_reminders_add_damage_waiver').attr("checked",damage_viewer);



 // alert(tgpd_pictures_download_url.length);

    //alert(tgpd_sketches_download_url.length);
}

function tgpd_print_pictures_to_form() {
   if (tgpd_pictures_download_url.length == 0) return;
   var $tgpd_picture_filelist = jQuery('#div_tgpd_picture_filelist');
   var $tgpd_picture_filelist_controls = jQuery("#div_tgpd_picture_filelist_controls");
   var $picture_list = jQuery(tgpd_picture_list);
   $tgpd_picture_filelist.append($picture_list);
   tgpd_pictures_download_url.forEach(function(element, index, array) {
      var $picture_list_item = jQuery(tgpd_picture_list_item);
      var $image = jQuery('img', $picture_list_item);
      var $info = jQuery('.info', $picture_list_item);
      var $progress_bar = jQuery('.progressBar', $picture_list_item);
      $image.attr('src', element);
      var start = element.lastIndexOf('/');
      var file_name = element.substr((start + 1));
      $info.html(file_name);
      $progress_bar.closest('.progressHolder').hide();
      $picture_list.append($picture_list_item);
   });
   $tgpd_picture_filelist_controls.show();
   $tgpd_picture_filelist.click(function(event) {
      tgpd_toggle_picture_filelist_controls(event, this);
   });
  // alert(tgpd_pictures_download_url.length);
}

function tgpd_print_sketches_to_form() {


   if (tgpd_sketches_download_url.length == 0) return;
   var $tgpd_sketch_filelist = jQuery('#div_tgpd_sketch_filelist');
   var $tgpd_sketch_filelist_controls = jQuery("#div_tgpd_sketch_filelist_controls");
   var $sketch_list = jQuery(tgpd_sketch_list);
   $tgpd_sketch_filelist.append($sketch_list);
   tgpd_sketches_download_url.forEach(function(element, index, array) {
      var $sketch_list_item = jQuery(tgpd_sketch_list_item);
      var $image = jQuery('img', $sketch_list_item);
      var $info = jQuery('.info', $sketch_list_item);
      var $progress_bar = jQuery('.progressBar', $sketch_list_item);
      $image.attr('src', element);
      var start = element.lastIndexOf('/');
      var file_name = element.substr((start + 1));
      $info.html(file_name);
      $progress_bar.closest('.progressHolder').hide();
      $sketch_list.append($sketch_list_item);
   });
   $tgpd_sketch_filelist_controls.show();
   $tgpd_sketch_filelist.click(function(event) {
      tgpd_toggle_sketch_filelist_controls(event, this);
   });
}

function tgpd_print_item_to_console(debug_tag, tgpd_item) {console.log(tgpd_item);
   var log_tag = (debug_tag.length == 0 ? 'tgpd_print_item_to_console: ' : ('tgpd_print_item_to_console:' + debug_tag + ': '));
   console.log(log_tag + 'tgpd_id => ' + tgpd_item.id);
   console.log(log_tag + 'tgpd_job_id => ' + tgpd_item.job_id);
   console.log(log_tag + 'tgpd_glass_location => ' + tgpd_item.glass_location);
   console.log(log_tag + 'tgpd_glass_type => ' + tgpd_item.glass_type);
   console.log(log_tag + 'tgpd_lite_thickness => ' + tgpd_item.lite_thickness);
   console.log(log_tag + 'tgpd_spacer_color => ' + tgpd_item.spacer_color);
   console.log(log_tag + 'tgpd_glass_treatment => ' + tgpd_item.glass_treatment);
   console.log(log_tag + 'tgpd_glass_color => ' + tgpd_item.glass_color);
   console.log(log_tag + 'tgpd_glass_color_note => ' + tgpd_item.glass_color_note);
   console.log(log_tag + 'tgpd_glass_lift => ' + tgpd_item.glass_lift);
   console.log(log_tag + 'tgpd_glass_set => ' + tgpd_item.glass_set);
   console.log(log_tag + 'tgpd_daylight_width => ' + tgpd_item.daylight_width);
   console.log(log_tag + 'tgpd_daylight_height => ' + tgpd_item.daylight_height);
   console.log(log_tag + 'tgpd_go_width => ' + tgpd_item.go_width);
   console.log(log_tag + 'tgpd_go_height => ' + tgpd_item.go_height);
   console.log(log_tag + 'tgpd_size_verification_status => ' + tgpd_item.size_verification_status);
   console.log(log_tag + 'tgpd_overall_thickness => ' + tgpd_item.overall_thickness);
   console.log(log_tag + 'tgpd_sag_or_quote => ' + tgpd_item.sag_or_quote);
   console.log(log_tag + 'tgpd_manufacturer => ' + tgpd_item.manufacturer);
   console.log(log_tag + 'tgpd_solar_film => ' + tgpd_item.solar_film);
   console.log(log_tag + 'tgpd_solar_film_responsibility => ' + tgpd_item.solar_film_responsibility);
   console.log(log_tag + 'tgpd_solar_film_type => ' + tgpd_item.solar_film_type);
   console.log(log_tag + 'tgpd_solar_film_source => ' + tgpd_item.solar_film_source);
   console.log(log_tag + 'tgpd_wet_seal => ' + tgpd_item.wet_seal);
   console.log(log_tag + 'tgpd_wet_seal_responsibility => ' + tgpd_item.wet_seal_responsibility);
   console.log(log_tag + 'tgpd_furniture_to_move => ' + tgpd_item.furniture_to_move);
   console.log(log_tag + 'tgpd_furniture_to_move_comment => ' + tgpd_item.furniture_to_move_comment);
   console.log(log_tag + 'tgpd_walls_or_ceilings_to_cut => ' + tgpd_item.walls_or_ceilings_to_cut);
   console.log(log_tag + 'tgpd_walls_or_ceilings_to_cut_responsibility => ' + tgpd_item.walls_or_ceilings_to_cut_responsibility);
   console.log(log_tag + 'tgpd_walls_or_ceilings_to_cut_comment => ' + tgpd_item.walls_or_ceilings_to_cut_comment);
   console.log(log_tag + 'tgpd_blind_needs_removing => ' + tgpd_item.blind_needs_removing);
   console.log(log_tag + 'tgpd_glass_fits_elevator => ' + tgpd_item.glass_fits_elevator);
   console.log(log_tag + 'tgpd_pictures_download_url => ' + tgpd_item.pictures_download_url);
   console.log(log_tag + 'tgpd_sketches_download_url => ' + tgpd_item.sketches_download_url);
   console.log(log_tag + 'tgpd_instructions => ' + tgpd_item.instructions);
   console.log(log_tag + 'tgpd_caulk_amount => ' + tgpd_item.caulk_amount);
   console.log(log_tag + 'tgpd_caulk_type => ' + tgpd_item.caulk_type);
   console.log(log_tag + 'tgpd_scaffolding_type => ' + tgpd_item.scaffolding_type);
   console.log(log_tag + 'tgpd_tape_amount => ' + tgpd_item.tape_amount);
   console.log(log_tag + 'tgpd_tape_type => ' + tgpd_item.tape_type);
   console.log(log_tag + 'tgpd_channel => ' + tgpd_item.channel);
   console.log(log_tag + 'tgpd_miscellaneous => ' + tgpd_item.miscellaneous);
   console.log(log_tag + 'tgpd_glass_grids => ' + tgpd_item.glass_grids);
   console.log(log_tag + 'tgpd_pattern_horizontal_grids => ' + tgpd_item.pattern_horizontal_grids);
   console.log(log_tag + 'tgpd_pattern_vertical_grids => ' + tgpd_item.pattern_vertical_grids);
   console.log(log_tag + 'tgpd_grids_thickness => ' + tgpd_item.grids_thickness);
   console.log(log_tag + 'tgpd_grids_color => ' + tgpd_item.grids_color);
   console.log(log_tag + 'tgpd_glass_fabrication => ' + tgpd_item.glass_fabrication);
   console.log(log_tag + 'tgpd_fabrication_polished_edges => ' + tgpd_item.fabrication_polished_edges);
   console.log(log_tag + 'tgpd_fabrication_holes => ' + tgpd_item.fabrication_holes);
   console.log(log_tag + 'tgpd_fabrication_pattern => ' + tgpd_item.fabrication_pattern);
   // console.log(log_tag + 'tgpd_item => ' + JSON.stringify(tgpd_item));
}

function tgpd_init(tgpd_seed, tgpd_seed_items) {
	//alert(JSON.stringify(tgpd_seed_items));
	jQuery('.shape-container').hide();
   tgpd_current_item = -1;
   tgpd_items = [];
   tgpd_pictures_download_url = [];
   tgpd_sketches_download_url = [];
   tgpd_init_form_state();
   if (tgpd_seed && tgpd_seed_items.length > 0) {
      tgpd_current_item = 0;
      tgpd_items = tgpd_seed_items;
      tgpd_init_toolbar_navigation_buttons_state();
      tgpd_init_toolbar_editing_buttons_state();
      tgpd_show_current_item();
	  tgpd_editingsave=true;
   }
   tgpd_set_toolbar_navigation_buttons_state();
   tgpd_editing = false;
   tgpd_copying = false;
   tgpd_copy_operation = -1;
   tgpd_clipboard = {};
   tgpd_set_toolbar_editing_buttons_state();
}

function tgpd_s3_upload_progress(event, $progress_bar) {
   var percent_complete_int = parseInt((event.loaded * 100) / event.total);
   var percent_complete_str = percent_complete_int + '%';
   if ($progress_bar) {
      $progress_bar.css('width', percent_complete_str);
   }
}

function tgpd_s3_upload_object(file,imagecontent, $progress_bar, callback) {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
	  var salesmanid = jQuery('#currentuserid').val();
	  var quoteNumber = jQuery('#text_quote_number').val();
	  var unix = Math.round(+new Date()/1000);
	  var fileArray = file.name.split('.');
	  var updatedfilename = fileArray[0]+'_'+salesmanid+'_'+unix+'_'+quoteNumber+'.'+fileArray[1];
      var params = {Key: updatedfilename, ContentType: file.type, Body: imagecontent, ACL: "public-read"};
      var upload = s3.upload(params);
      upload.on('httpUploadProgress', function(event) {
         tgpd_s3_upload_progress(event, $progress_bar);
      });
      upload.send(function (err, data) {
         if (err) {
            console.log('tgpd_s3_upload_object: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            console.log('tgpd_s3_upload_object: data.Location -> ' + data.Location + ', data.Bucket -> ' + data.Bucket + ', data.Key -> ' + data.Key);
         }
         callback(err, data);
      });
   });
}
function tgpd_s3_upload_object_sketch(file,$progress_bar, callback) {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
	  var salesmanid = jQuery('#currentuserid').val();
	  var quoteNumber = jQuery('#text_quote_number').val();
	  var unix = Math.round(+new Date()/1000);
	  var fileArray = file.name.split('.');
	  var updatedfilename = fileArray[0]+'_'+salesmanid+'_'+unix+'_'+quoteNumber+'.'+fileArray[1];
      var params = {Key: updatedfilename, ContentType: file.type, Body: file, ACL: "public-read"};
      var upload = s3.upload(params);
      upload.on('httpUploadProgress', function(event) {
         tgpd_s3_upload_progress(event, $progress_bar);
      });
      upload.send(function (err, data) {
         if (err) {
            console.log('tgpd_s3_upload_object: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            console.log('tgpd_s3_upload_object: data.Location -> ' + data.Location + ', data.Bucket -> ' + data.Bucket + ', data.Key -> ' + data.Key);
         }
         callback(err, data);
      });
   });
}

function tgpd_s3_delete_object(object_key) {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
      var params = {Bucket: aws_s3_bucket, Key: object_key};
      s3.deleteObject(params, function(err, data) {
         if (err) {
            console.log('tgpd_s3_delete_object: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            console.log('tgpd_s3_delete_object: data.DeleteMarker -> ' + data.DeleteMarker + ', data.VersionId -> ' + data.VersionId + ', data.RequestCharged -> ' + data.RequestCharged);
         }
      });
   });
}

function tgpd_s3_get_bucket_cors() {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
      var params = {Bucket: aws_s3_bucket};
      s3.getBucketCors(params, function(err, data) {
         if (err) {
            console.log('tgpd_s3_get_bucket_cors: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            var allowedHeaders = data.CORSRules[0]['AllowedHeaders'];
            console.log('tgpd_s3_get_bucket_cors: allowedHeaders');
            allowedHeaders.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var allowedMethods = data.CORSRules[0]['AllowedMethods'];
            console.log('tgpd_s3_get_bucket_cors: allowedMethods');
            allowedMethods.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var allowedOrigins = data.CORSRules[0]['AllowedOrigins'];
            console.log('tgpd_s3_get_bucket_cors: allowedOrigins');
            allowedOrigins.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var exposeHeaders = data.CORSRules[0]['ExposeHeaders'];
            console.log('tgpd_s3_get_bucket_cors: exposeHeaders');
            exposeHeaders.forEach(function (item, index, array) {
               console.log('tgpd_s3_get_bucket_cors: item -> ' + item + ', index -> ' + index);
            });
            var maxAgeSeconds = data.CORSRules[0]['MaxAgeSeconds'];
            console.log('tgpd_s3_get_bucket_cors: maxAgeSeconds -> ' + maxAgeSeconds);
         }
      });
   });
}

function tgpd_s3_get_bucket_acl() {
   AWS.config.region = aws_s3_region;
   AWS.config.credentials = new AWS.CognitoIdentityCredentials({
      IdentityPoolId: aws_s3_identitypoolid,
   });
   AWS.config.credentials.get(function() {
      var s3 = new AWS.S3({params: {Bucket: aws_s3_bucket}});
      var params = {Bucket: aws_s3_bucket};
      s3.getBucketAcl(params, function(err, data) {
         if (err) {
            console.log('tgpd_s3_get_bucket_acl: err.name -> ' + err.name + ', err.message -> ' + err.message + ', err.toString() -> ' + err.toString());
         } else {
            var displayName = data.Owner['DisplayName'];
            console.log('tgpd_s3_get_bucket_acl: Owner.displayName -> ' + displayName);

            var iD = data.Owner['ID'];
            console.log('tgpd_s3_get_bucket_acl: Owner.ID -> ' + iD);

            data.Grants.forEach(function (grant, index, array) {
               var grantee = grant['Grantee'];
               var grantee_display_name = grantee['DisplayName'];
               var grantee_email_address = grantee['EmailAddress'];
               var grantee_id = grantee['ID'];
               var grantee_type = grantee['Type'];
               var grantee_uri = grantee['URI'];
               var grantee_permission = grant['Permission'];
               console.log('tgpd_s3_get_bucket_acl: data.Grants[' + index + ']: grantee_display_name -> ' + grantee_display_name + ', grantee_email_address -> ' + grantee_email_address + ', grantee_id -> ' + grantee_id + ', grantee_type -> ' + grantee_type + ', grantee_uri -> ' + grantee_uri + ', grantee_permission -> ' + grantee_permission);
            });
         }
      });
   });
}

function tgpd_toggle_canvas_controls(event, target) {
   if (event.target == target) {
      jQuery('#div_tgpd_canvas_controls').toggle();
   }
}

function tgpd_toggle_canvas_holder(event, target) {
   jQuery('#div_tgpd_canvas_holder').slideToggle(300, 'easeInOutSine', function() {
      if (jQuery(target).is(':hidden')) {
         jQuery('#div_tgpd_canvas_controls').hide();
      } else {
         jQuery('#div_tgpd_canvas_controls').show();
      }
   });
   jQuery(target).toggleClass('canvas-close');
}


//krishan notes jQuery start//

function tgpd_toggle_canvas_holder_notes(event, target) {
   jQuery('#div_usg_canvas_holder_notes').slideToggle(300, 'easeInOutSine', function() {
      if (jQuery(target).is(':hidden')) {
         jQuery('#div_usg_canvas_holder_notes').hide();
      } else {
         jQuery('#div_usg_canvas_controls_notes').show();
      }
   });
   jQuery(target).toggleClass('canvas-close');
}

//krishan notes jQuery end//

function tgpd_show_canvas_holder(event) {
	var $tgpd_canvas_holder = jQuery('#div_tgpd_canvas_holder');

   if (!$tgpd_canvas_holder.is(':hidden')) {
      $tgpd_canvas_holder.slideDown(300, 'easeInOutSine', function() {
         jQuery('#div_tgpd_canvas_controls').show();
      });
      jQuery('#canvas-wrap h5').addClass('canvas-close');
   }
}


function tgpd_hide_canvas_holder(event) {
   var $tgpd_canvas_holder = jQuery('#div_usg_canvas_holder_notes');
   if (!$tgpd_canvas_holder.is(':hidden')) {
      $tgpd_canvas_holder.slideUp(300, 'easeInOutSine', function() {
         jQuery('#div_usg_canvas_controls_notes').hide();
      });
      jQuery('#canvas-wrap h5').removeClass('canvas-close');
   }
}



function tgpd_toggle_picture_filelist_controls(event, target) {
   if (event.target == target) {
      jQuery('#div_tgpd_picture_filelist_controls').toggle();
   }
}

function tgpd_toggle_picture_filelist_controls_notes(event, target) {
	   if (event.target == target) {
      jQuery('#div_upg_picture_filelist_controls_notes').toggle();
   }
}

function tgpd_toggle_sketch_filelist_controls(event, target) {
   if (event.target == target) {
      jQuery('#div_tgpd_sketch_filelist_controls').toggle();
	  jQuery('#btn_tgpd_upload_sketch_filelist').removeAttr('disabled');
	jQuery('#btn_tgpd_upload_sketch_filelist').removeClass('cust-disable');
   }
}

function tgpd_clear_glass_picture_filelist(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var $tgpd_picture_filelist = jQuery('#div_tgpd_picture_filelist');
   var $tgpd_picture_filelist_controls = jQuery("#div_tgpd_picture_filelist_controls");
   $tgpd_picture_filelist_controls.hide();
   $tgpd_picture_filelist.html('');
   $tgpd_picture_filelist.off('click');
   if (tgpd_pictures_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_pictures_download_url.forEach(function(element, index, array) {
         console.log('tgpd_clear_glass_picture_filelist: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   tgpd_pictures_download_url = [];
}

function tgpd_clear_glass_sketch_filelist(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var tgpd_option_clear_canvas = typeof tgpd_options.clear_canvas === 'undefined' ? false : tgpd_options.clear_canvas;
   var tgpd_option_hide_canvas = typeof tgpd_options.hide_canvas === 'undefined' ? false : tgpd_options.hide_canvas;
   var $tgpd_sketch_filelist = jQuery('#div_tgpd_sketch_filelist');
   var $tgpd_sketch_filelist_controls = jQuery("#div_tgpd_sketch_filelist_controls");
   $tgpd_sketch_filelist_controls.hide();
   $tgpd_sketch_filelist.html('');
   $tgpd_sketch_filelist.off('click');
   if (tgpd_sketches_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_sketches_download_url.forEach(function(element, index, array) {
         console.log('tgpd_clear_glass_sketch_filelist: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   tgpd_sketches_download_url = [];
   if (tgpd_option_clear_canvas) tgpd_clear_canvas();
   if (tgpd_option_hide_canvas) tgpd_hide_canvas_holder();
}
//krishan clear list start//

function tgpd_clear_glass_sketch_filelist_notes(tgpd_options) {

   var tgpd_options = tgpd_options || {};
   var tgpd_option_s3_delete = typeof tgpd_options.s3_delete === 'undefined' ? false : tgpd_options.s3_delete;
   var tgpd_option_clear_canvas = typeof tgpd_options.clear_canvas === 'undefined' ? false : tgpd_options.clear_canvas;
   var tgpd_option_hide_canvas = typeof tgpd_options.hide_canvas === 'undefined' ? false : tgpd_options.hide_canvas;
   var $tgpd_sketch_filelist = jQuery('#div_usg_sketch_filelist_notes');
   var $tgpd_sketch_filelist_controls = jQuery("#div_usg_sketch_filelist_controls_notes");
   $tgpd_sketch_filelist_controls.hide();
   $tgpd_sketch_filelist.html('');
   $tgpd_sketch_filelist.off('click');
   if (tgpd_sketches_download_url.length > 0 && tgpd_option_s3_delete) {
      tgpd_sketches_download_url.forEach(function(element, index, array) {
         console.log('div_usg_sketch_filelist_notes: element => ' + element);
         var start = element.lastIndexOf('/');
         var object_key = element.substr((start + 1));
         tgpd_s3_delete_object(object_key);
      });
   }
   tgpd_sketches_download_url = [];
   if (tgpd_option_clear_canvas) tgpd_clear_canvas();
   if (tgpd_option_hide_canvas) tgpd_hide_canvas_holder();
}
//krishan clear list end



function tgpd_upload_glass_picture_filelist() {

   var $imgs = jQuery('.picture_obj');
   var imgCOunter = tgpd_pictures_download_url.length+1;
   $imgs.each(function(index) {

      var self = this;
      var file = this.file;
      var $progress_bar = this.progress;
      var $status = this.status;

      $progress_bar.css('width', '0%');
      $status.removeClass('success failure');
      console.log('tgpd_upload_glass_picture_filelist: file.name => ' + file.name + ', file.size => ' + file.size + ', file.type => ' + file.type + ', file.lastModifiedDate => ' + file.lastModifiedDate);
	  var resize = new window.resize();
	  resize.init();
	  resize.photo(file, 1200, 'file', function (resizedFile) {
				tgpd_s3_upload_object(file,resizedFile, $progress_bar, function(err, data) {
				 if (!err) {
					$status.addClass('success');
					$progress_bar.css('width', '100%');
					jQuery(self).removeClass('picture_obj');
					//alert(data.Location);
					//tgpd_pictures_download_url_object[index] = data.Location;
					if(imgCOunter== 1){
						tgpd_pictures_download_url[index] = data.Location;
					}else{
						tgpd_pictures_download_url[imgCOunter+index] = data.Location;
						}
					console.log('tgpd_upload_glass_picture_filelist: data.Location => ' + data.Location + ', tgpd_pictures_download_url => ' + tgpd_pictures_download_url.toString());
				 } else {
					$status.addClass('failure');
					$progress_bar.css('width', '0%');
				 }
			});

		});
   });

}

function tgpd_upload_glass_sketch_filelist() {
	// alert(tgpd_sketches_download_url.length);
	//alert(tgpd_sketches_download_url.length);
   var $imgs = jQuery('.sketch_obj');
   var imgCOunter = tgpd_sketches_download_url.length+1;
   $imgs.each(function(index) {

      var self = this;
      var file = this.file;
      var $progress_bar = this.progress;
      var $status = this.status;
      $progress_bar.css('width', '0%');
      $status.removeClass('success failure');
      console.log('tgpd_upload_glass_sketch_filelist: file.name => ' + file.name + ', file.size => ' + file.size + ', file.type => ' + file.type + ', file.lastModifiedDate => ' + file.lastModifiedDate);
      tgpd_s3_upload_object_sketch(file, $progress_bar, function(err, data) {
         if (!err) {
            $status.addClass('success');
            $progress_bar.css('width', '100%');
            jQuery(self).removeClass('sketch_obj');
			if(imgCOunter== 1){
            	tgpd_sketches_download_url[index] = data.Location;
			}else{
				tgpd_sketches_download_url[imgCOunter+index] = data.Location;
				}
            console.log('tgpd_upload_glass_sketch_filelist: data.Location => ' + data.Location + ', tgpd_sketches_download_url => ' + tgpd_sketches_download_url.toString());
         } else {
            $status.addClass('failure');
            $progress_bar.css('width', '0%');
         }
      });
   });


}

function S4() {
   return (((1+Math.random())*0x10000)|0).toString(16).substring(1);
}

function tgpd_clear_canvas(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_hide_toolbar = typeof tgpd_options.hide_toolbar === 'undefined' ? false : tgpd_options.hide_toolbar;
   var $tgpd_glass_sketch = jQuery("#canvas_tgpd_glass_sketch");
   var canvas = $tgpd_glass_sketch[0];
   var context = canvas.getContext("2d");
   $tgpd_glass_sketch.sketch('stopPainting');
   $tgpd_glass_sketch.sketch('actions', []);
   context.clearRect(0, 0, canvas.width, canvas.height);
   context.fillStyle = '#FFFFFF';
   context.fillRect(0, 0, canvas.width, canvas.height);
   if (tgpd_hide_toolbar) jQuery("#div_tgpd_canvas_controls").hide();
}

// krishan clear_canvas_notes start//
function tgpd_clear_canvas_notes(tgpd_options) {
   var tgpd_options = tgpd_options || {};
   var tgpd_hide_toolbar = typeof tgpd_options.hide_toolbar === 'undefined' ? false : tgpd_options.hide_toolbar;
   var $tgpd_glass_sketch = jQuery("#canvas_usg_glass_sketch_notes");
   var canvas = $tgpd_glass_sketch[0];
   var context = canvas.getContext("2d");
   $tgpd_glass_sketch.sketch('stopPainting');
   $tgpd_glass_sketch.sketch('actions', []);
   context.clearRect(0, 0, canvas.width, canvas.height);
   context.fillStyle = '#FFFFFF';
   context.fillRect(0, 0, canvas.width, canvas.height);
   if (tgpd_hide_toolbar) jQuery("#div_usg_canvas_controls_notes").hide();
}



function tgpd_canvas_to_sketch_filelist_notes() {

   var tgpd_glass_sketch = jQuery("#canvas_usg_glass_sketch_notes")[0];
   tgpd_glass_sketch.toBlob(function(blob) {
      console.log('tgpd_canvas_to_sketch_filelist_notes:canvas.toBlob: blob.type => ' + blob.type + ', blob.size => ' + blob.size);
      var fileName = S4() + S4() + '-' + (new Date()).getTime() + '.jpg';

      var newFile = new File([blob], fileName, {type:blob.type})
      console.log('tgpd_canvas_to_sketch_filelist_notes:canvas.toBlob: newFile.name => ' + newFile.name + ', newFile.lastModified => ' + newFile.lastModified + ', newFile.type => ' + newFile.type + ', newFile.size => ' + newFile.size);
      tgpd_handle_sketch_file_notes(newFile);
   }, 'image/jpg');
}
// krishan clear_canvas_notes end//

function tgpd_canvas_to_sketch_filelist() {
   var tgpd_glass_sketch = jQuery("#canvas_tgpd_glass_sketch")[0];
   tgpd_glass_sketch.toBlob(function(blob) {
      console.log('tgpd_canvas_to_sketch_filelist:canvas.toBlob: blob.type => ' + blob.type + ', blob.size => ' + blob.size);
      var fileName = S4() + S4() + '-' + (new Date()).getTime() + '.jpg';

      var newFile = new File([blob], fileName, {type:blob.type})
      console.log('tgpd_canvas_to_sketch_filelist:canvas.toBlob: newFile.name => ' + newFile.name + ', newFile.lastModified => ' + newFile.lastModified + ', newFile.type => ' + newFile.type + ', newFile.size => ' + newFile.size);
      tgpd_handle_sketch_file(newFile);
   }, 'image/jpg');
}

function validateFormGPD() {
   var text_glass_location = jQuery('#text_tgpd_glass_location')[0];
   if (text_glass_location.value.trim().length == 0) {
      text_glass_location.focus();
      return false;
   }
   if (!text_glass_location.checkValidity()) {
      text_glass_location.focus();
      return false;
   }
   /*Shape Validation*/
   var glass_shape = jQuery('#select_tgpd_glass_shape').val();
   if (typeof glass_shape === 'undefined' || glass_shape == null) glass_shape = 'not_applicable';
   if (glass_shape.localeCompare('not_applicable') != 0) {
	   if(jQuery('#text_tagd_shape_left_leg_'+glass_shape).is(":visible")){
		   var text_glass_shape_left_leg = jQuery('#text_tagd_shape_left_leg_'+glass_shape)[0];

		   if (text_glass_shape_left_leg.value.trim().length == 0) {
			  text_glass_shape_left_leg.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_right_leg_'+glass_shape).is(":visible")){
		   var text_glass_shape_right_leg = jQuery('#text_tagd_shape_right_leg_'+glass_shape)[0];
		   if (text_glass_shape_right_leg.value.trim().length == 0) {
			  text_glass_shape_right_leg.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_width_'+glass_shape).is(":visible")){
		   var text_glass_shape_width = jQuery('#text_tagd_shape_width_'+glass_shape)[0];
		   if (text_glass_shape_width.value.trim().length == 0) {
			  text_glass_shape_width.focus();
			  return false;
		   }
   		}
		 if(jQuery('#text_tagd_shape_height_'+glass_shape).is(":visible")){
		   var text_glass_shape_height = jQuery('#text_tagd_shape_height_'+glass_shape)[0];
		   if (text_glass_shape_height.value.trim().length == 0) {
			  text_glass_shape_height.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_base_'+glass_shape).is(":visible")){
		   var text_glass_shape_base = jQuery('#text_tagd_shape_base_'+glass_shape)[0];
		   if (text_glass_shape_base.value.trim().length == 0) {
			  text_glass_shape_base.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_left_side_'+glass_shape).is(":visible")){
		   var text_glass_shape_left_side = jQuery('#text_tagd_shape_left_side_'+glass_shape)[0];
		   if (text_glass_shape_left_side.value.trim().length == 0) {
			  	text_glass_shape_left_side.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_right_side_'+glass_shape).is(":visible")){
		   var text_glass_shape_right_side = jQuery('#text_tagd_shape_right_side_'+glass_shape)[0];
		   if (text_glass_shape_right_side.value.trim().length == 0) {
			  	text_glass_shape_right_side.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_diameter_'+glass_shape).is(":visible")){
		   var text_glass_shape_diameter = jQuery('#text_tagd_shape_diameter_'+glass_shape)[0];
		   if (text_glass_shape_diameter.value.trim().length == 0) {
			  	text_glass_shape_diameter.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_radius_'+glass_shape).is(":visible")){
		   var text_glass_shape_radius = jQuery('#text_tagd_shape_radius_'+glass_shape)[0];
		   if (text_glass_shape_radius.value.trim().length == 0) {
			  	text_glass_shape_radius.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_radius_one_'+glass_shape).is(":visible")){
		   var text_glass_shape_radius_one = jQuery('#text_tagd_shape_radius_one_'+glass_shape)[0];
		   if (text_glass_shape_radius_one.value.trim().length == 0) {
			  	text_glass_shape_radius_one.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_radius_two_'+glass_shape).is(":visible")){
		   var text_glass_shape_radius_two = jQuery('#text_tagd_shape_radius_two_'+glass_shape)[0];
		   if (text_glass_shape_radius_two.value.trim().length == 0) {
			  	text_glass_shape_radius_two.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_length_'+glass_shape).is(":visible")){
		   var text_glass_shape_length = jQuery('#text_tagd_shape_length_'+glass_shape)[0];
		   if (text_glass_shape_length.value.trim().length == 0) {
			  	text_glass_shape_length.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_leg_'+glass_shape).is(":visible")){
		   var text_glass_shape_leg = jQuery('#text_tagd_shape_leg_'+glass_shape)[0];
		   if (text_glass_shape_leg.value.trim().length == 0) {
			  	text_glass_shape_leg.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_side_'+glass_shape).is(":visible")){
		   var text_glass_shape_side = jQuery('#text_tagd_shape_side_'+glass_shape)[0];
		   if (text_glass_shape_side.value.trim().length == 0) {
			  	text_glass_shape_side.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_left_offset_'+glass_shape).is(":visible")){
		   var text_glass_shape_left_offset = jQuery('#text_tagd_shape_left_offset_'+glass_shape)[0];
		   if (text_glass_shape_left_offset.value.trim().length == 0) {
			  text_glass_shape_left_offset.focus();
			  return false;
		   }
	   }
	   if(jQuery('#text_tagd_shape_top_offset_'+glass_shape).is(":visible")){
		   var text_glass_shape_top_offset = jQuery('#text_tagd_shape_top_offset_'+glass_shape)[0];
		   if (text_glass_shape_top_offset.value.trim().length == 0) {
			  text_glass_shape_top_offset.focus();
			  return false;
		   }
	   }
	    if(jQuery('#text_tagd_shape_right_offset_'+glass_shape).is(":visible")){
		   var text_glass_shape_right_offset = jQuery('#text_tagd_shape_right_offset_'+glass_shape)[0];
		   if (text_glass_shape_right_offset.value.trim().length == 0) {
			  text_glass_shape_right_offset.focus();
			  return false;
		   }
	   }

	    if(jQuery('#text_tagd_shape_top_'+glass_shape).is(":visible")){
		   var text_glass_shape_top = jQuery('#text_tagd_shape_top_'+glass_shape)[0];
		   if (text_glass_shape_top.value.trim().length == 0) {
			  text_glass_shape_top.focus();
			  return false;
		   }
	   }
   }



   var glass_type = jQuery('#select_tgpd_glass_type').val();
   if (typeof glass_type === 'undefined' || glass_type == null) glass_type = 'not_applicable';
   if (glass_type.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_type')[0].focus();
      return false;
   }
   if (glass_type.indexOf('igu') > -1) {
	   if(glass_type!="igu_lami_temp_lami" && glass_type!="igu_sky_lite_temp_or_lami")
	   {
			  var lite_thickness = jQuery('#select_tgpd_lite_thickness').val();
			  if (typeof lite_thickness === 'undefined' || lite_thickness == null) lite_thickness = 'not_applicable';
			  if (lite_thickness.localeCompare('not_applicable') == 0) {
				 jQuery('#select_tgpd_lite_thickness')[0].focus();
				 return false;
			  }
	   }
      var spacer_color = jQuery('#select_tgpd_spacer_color').val();
      if (typeof spacer_color === 'undefined' || spacer_color == null) spacer_color = 'not_applicable';
      if (spacer_color.localeCompare('not_applicable') == 0) {
         jQuery('#select_tgpd_spacer_color')[0].focus();
         return false;
      }
   }
   var glass_treatment = jQuery('#select_tgpd_glass_treatment').val();
   if (typeof glass_treatment === 'undefined' || glass_treatment == null) glass_treatment = 'not_applicable';
   if (glass_treatment.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_treatment')[0].focus();
      return false;
   }
   var glass_color = jQuery('#select_tgpd_glass_color').val();
   if (typeof glass_color === 'undefined' || glass_color == null) glass_color = 'not_applicable';
   if (glass_color.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_color')[0].focus();
      return false;
   }
   if (glass_color.localeCompare('custom') == 0) {
      var text_glass_color_note = jQuery('#text_tgpd_glass_color_note')[0];
      if (text_glass_color_note.value.trim().length == 0) {
         text_glass_color_note.focus();
         return false;
      }
      if (!text_glass_color_note.checkValidity()) {
         text_glass_color_note.focus();
         return false;
      }
   }


   var text_glass_lift = jQuery('#text_tgpd_glass_lift')[0];
   if (text_glass_lift.value.trim().length == 0) {
      text_glass_lift.focus();
      return false;
   }
   if (!text_glass_lift.checkValidity()) {
      text_glass_lift.focus();
      return false;
   }
   var glass_set = jQuery('#select_tgpd_glass_set').val();
   if (typeof glass_set === 'undefined' || glass_set == null) glass_set = 'not_applicable';
   if (glass_set.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_set')[0].focus();
      return false;
   }
  if(jQuery('#text_tgpd_daylight').is(":visible")){
		   var text_tgpd_daylight = jQuery('#text_tgpd_daylight').prop('checked') ? 1 : 0;

		 if(text_tgpd_daylight==0)
		 {
		   var text_daylight_width = jQuery('#text_tgpd_daylight_width')[0];
		   if (text_daylight_width.value.trim().length == 0) {
			  text_daylight_width.focus();
			  return false;
		   }
		   if (!text_daylight_width.checkValidity()) {
			  text_daylight_width.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_daylight_width.value)) {
			  text_daylight_width.focus();
			  return false;
		   }
		   var text_daylight_height = jQuery('#text_tgpd_daylight_height')[0];
		   if (text_daylight_height.value.trim().length == 0) {
			  text_daylight_height.focus();
			  return false;
		   }
		   if (!text_daylight_height.checkValidity()) {
			  text_daylight_height.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_daylight_height.value)) {
			  text_daylight_height.focus();
			  return false;
		   }
		 }


		   var text_tgpd_gosize = jQuery('#text_tgpd_gosize').prop('checked') ? 1 : 0;

		 if(text_tgpd_gosize==0)
		 {


		   var text_go_width = jQuery('#text_tgpd_go_width')[0];
		   if (text_go_width.value.trim().length == 0) {
			  text_go_width.focus();
			  return false;
		   }
		   if (!text_go_width.checkValidity()) {
			  text_go_width.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_go_width.value)) {
			  text_go_width.focus();
			  return false;
		   }
		   var text_go_height = jQuery('#text_tgpd_go_height')[0];
		   if (text_go_height.value.trim().length == 0) {
			  text_go_height.focus();
			  return false;
		   }
		   if (!text_go_height.checkValidity()) {
			  text_go_height.focus();
			  return false;
		   }
		   if (!is_valid_fraction(text_go_height.value)) {
			  text_go_height.focus();
			  return false;
		   }
		 }
  }

   /*Removed on date 2nd Nov 2017*/
   /*
   var size_verification_status = jQuery('#select_tgpd_size_verification_status').val();

   if (typeof size_verification_status === 'undefined' || size_verification_status == null) size_verification_status = 'not_applicable';

   if (size_verification_status.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_size_verification_status')[0].focus();
      return false;
   }
   */

   var overall_thickness = jQuery('#select_tgpd_overall_thickness').val();
   if (typeof overall_thickness === 'undefined' || overall_thickness == null) overall_thickness = 'not_applicable';
   if (overall_thickness.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_overall_thickness')[0].focus();
      return false;
   }
   var sag_or_quote = jQuery('#checkbox_tgpd_glass_pricing_sag_or_quote').prop('checked') ? 1 : 0;
   if (sag_or_quote == 1) {
      var text_manufacturer = jQuery('#text_tgpd_glass_pricing_sag_or_quote_manufacturer')[0];
      if (text_manufacturer.value.trim().length == 0) {
         text_manufacturer.focus();
         return false;
      }
      if (!text_manufacturer.checkValidity()) {
         text_manufacturer.focus();
         return false;
      }
   }
   var solar_film = jQuery('#checkbox_tgpd_glass_reminders_solar_film').prop('checked') ? 1 : 0;
   if (solar_film == 1) {
      var solar_film_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]:checked').val();
      if (typeof solar_film_responsibility === 'undefined' || solar_film_responsibility == null) {
         jQuery('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]').focus();
         return false;
      }
      if (solar_film_responsibility.localeCompare('sag') == 0) {
         var text_solar_film_type = jQuery('#text_tgpd_glass_reminders_solar_film_type')[0];
         if (text_solar_film_type.value.trim().length == 0) {
            text_solar_film_type.focus();
            return false;
         }
         if (!text_solar_film_type.checkValidity()) {
            text_solar_film_type.focus();
            return false;
         }
         var text_solar_film_source = jQuery('#text_tgpd_glass_reminders_solar_film_source')[0];
         if (text_solar_film_source.value.trim().length == 0) {
            text_solar_film_source.focus();
            return false;
         }
         if (!text_solar_film_source.checkValidity()) {
            text_solar_film_source.focus();
            return false;
         }
      }
   }
   var wet_seal = jQuery('#checkbox_tgpd_glass_reminders_wet_seal').prop('checked') ? 1 : 0;
   if (wet_seal == 1) {
      var wet_seal_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]:checked').val();
      if (typeof wet_seal_responsibility === 'undefined' || wet_seal_responsibility == null) {
         jQuery('input:radio[name=radio_tgpd_glass_reminders_wet_seal_responsibility]').focus();
         return false;
      }
   }
   var furniture_to_move = jQuery('#checkbox_tgpd_glass_reminders_furniture_to_move').prop('checked') ? 1 : 0;
   if (furniture_to_move == 1) {
      var text_furniture_to_move_comment = jQuery('#text_tgpd_glass_reminders_furniture_to_move_comment')[0];
      if (text_furniture_to_move_comment.value.trim().length == 0) {
         text_furniture_to_move_comment.focus();
         return false;
      }
      if (!text_furniture_to_move_comment.checkValidity()) {
         text_furniture_to_move_comment.focus();
         return false;
      }
   }
   var walls_or_ceilings_to_cut = jQuery('#checkbox_tgpd_glass_reminders_walls_or_ceilings_to_cut').prop('checked') ? 1 : 0;
   if (walls_or_ceilings_to_cut == 1) {
      var walls_or_ceilings_to_cut_responsibility = jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]:checked').val();
      if (typeof walls_or_ceilings_to_cut_responsibility === 'undefined' || walls_or_ceilings_to_cut_responsibility == null) {
         jQuery('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]').focus();
         return false;
      }
      if (walls_or_ceilings_to_cut_responsibility.localeCompare('sag') == 0 || walls_or_ceilings_to_cut_responsibility.localeCompare('customer') == 0) {
         var text_walls_or_ceilings_to_cut_comment = jQuery('#text_tgpd_glass_reminders_walls_or_ceilings_to_cut_comment')[0];
         if (text_walls_or_ceilings_to_cut_comment.value.trim().length == 0) {
            text_walls_or_ceilings_to_cut_comment.focus();
            return false;
         }
         if (!text_walls_or_ceilings_to_cut_comment.checkValidity()) {
            text_walls_or_ceilings_to_cut_comment.focus();
            return false;
         }
      }
   }
   var textarea_instructions = jQuery('#textarea_tgpd_instructions')[0];
   if (!textarea_instructions.checkValidity()) {
      textarea_instructions.focus();
      return false;
   }
  /* var text_caulk_amount = jQuery('#text_tgpd_glass_materials_caulk_amount')[0];
   if (text_caulk_amount.value.trim().length == 0) {
      text_caulk_amount.focus();
      return false;
   }*/
 /*  if (!text_caulk_amount.checkValidity()) {
      text_caulk_amount.focus();
      return false;
   }*/
 /*  var caulk_type = jQuery('#select_tgpd_glass_materials_caulk_type').val();
   if (typeof caulk_type === 'undefined' || caulk_type == null) caulk_type = 'not_applicable';
   if (caulk_type.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_materials_caulk_type')[0].focus();
      return false;
   }*/
 /*  var scaffolding_type = jQuery('#select_tgpd_glass_materials_scaffolding_type').val();
   if (typeof scaffolding_type === 'undefined' || scaffolding_type == null) scaffolding_type = 'not_applicable';
   if (scaffolding_type.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_materials_scaffolding_type')[0].focus();
      return false;
   }*/
  /* var text_tape_amount = jQuery('#text_tgpd_glass_materials_tape_amount')[0];
   if (text_tape_amount.value.trim().length == 0) {
      text_tape_amount.focus();
      return false;
   }
   if (!text_tape_amount.checkValidity()) {
      text_tape_amount.focus();
      return false;
   }*/
  /* var tape_type = jQuery('#select_tgpd_glass_materials_tape_type').val();
   if (typeof tape_type === 'undefined' || tape_type == null) tape_type = 'not_applicable';
   if (tape_type.localeCompare('not_applicable') == 0) {
      jQuery('#select_tgpd_glass_materials_tape_type')[0].focus();
      return false;
   }
   var text_channel = jQuery('#text_tgpd_glass_materials_channel')[0];
   if (text_channel.value.trim().length == 0) {
      text_channel.focus();
      return false;
   }
   if (!text_channel.checkValidity()) {
      text_channel.focus();
      return false;
   }
   var text_miscellaneous = jQuery('#text_tgpd_miscellaneous')[0];
   if (text_miscellaneous.value.trim().length == 0) {
      text_miscellaneous.focus();
      return false;
   }
   if (!text_miscellaneous.checkValidity()) {
      text_miscellaneous.focus();
      return false;
   }*/
   if (glass_type.indexOf('igu') > -1) {
      var glass_grids = jQuery('#checkbox_tgpd_glass_grids').prop('checked') ? 1 : 0;
      console.log('validateFormGPD:glass_grids -> ' + glass_grids + ', glass_grids => ' + glass_grids == 1 ? true : false);
      if (glass_grids == 1) {
         var number_pattern_horizontal_grids = jQuery('#number_tgpd_pattern_horizontal_grids')[0];
         if (number_pattern_horizontal_grids.value.trim().length == 0) {
            number_pattern_horizontal_grids.focus();
            return false;
         }
         if (!number_pattern_horizontal_grids.checkValidity()) {
            number_pattern_horizontal_grids.focus();
            return false;
         }
         var number_pattern_vertical_grids = jQuery('#number_tgpd_pattern_vertical_grids')[0];
         if (number_pattern_vertical_grids.value.trim().length == 0) {
            number_pattern_vertical_grids.focus();
            return false;
         }
         if (!number_pattern_vertical_grids.checkValidity()) {
            number_pattern_vertical_grids.focus();
            return false;
         }
         var grids_thickness = jQuery('#select_tgpd_grids_thickness').val();
         if (typeof grids_thickness === 'undefined' || grids_thickness == null) grids_thickness = 'not_applicable';
         if (grids_thickness.localeCompare('not_applicable') == 0) {
            jQuery('#select_tgpd_grids_thickness')[0].focus();
            return false;
         }
         var text_grids_color = jQuery('#text_tgpd_grids_color')[0];
         if (text_grids_color.value.trim().length == 0) {
            text_grids_color.focus();
            return false;
         }
         if (!text_grids_color.checkValidity()) {
            text_grids_color.focus();
            return false;
         }
      }
   }
   var glass_fabrication = jQuery('#checkbox_tgpd_glass_fabrication').prop('checked') ? 1 : 0;
   console.log('validateFormGPD:glass_fabrication -> ' + glass_fabrication + ', glass_fabrication => ' + glass_fabrication == 1 ? true : false);
   if (glass_fabrication == 1) {
      var text_fabrication_polished_edges = jQuery('#text_tgpd_fabrication_polished_edges')[0];
      if (text_fabrication_polished_edges.value.trim().length == 0) {
         text_fabrication_polished_edges.focus();
         return false;
      }
      if (!text_fabrication_polished_edges.checkValidity()) {
         text_fabrication_polished_edges.focus();
         return false;
      }
      var text_fabrication_holes = jQuery('#text_tgpd_fabrication_holes')[0];
      if (text_fabrication_holes.value.trim().length == 0) {
         text_fabrication_holes.focus();
         return false;
      }
      if (!text_fabrication_holes.checkValidity()) {
         text_fabrication_holes.focus();
         return false;
      }
      var text_fabrication_pattern = jQuery('#text_tgpd_fabrication_pattern')[0];
      if (text_fabrication_pattern.value.trim().length == 0) {
         text_fabrication_pattern.focus();
         return false;
      }
      if (!text_fabrication_pattern.checkValidity()) {
         text_fabrication_pattern.focus();
         return false;
      }
   }
   return true;
}

function tgpd_handle_picture_files(files) {

   var $tgpd_picture_filelist = jQuery('#div_tgpd_picture_filelist');
   var $tgpd_picture_filelist_controls = jQuery('#div_tgpd_picture_filelist_notes');
   if (!files.length) {
   } else {
      var $list = jQuery('#div_tgpd_picture_filelist ul');
      if ($list.length == 0) {

         $list = jQuery(tgpd_picture_list);

		 $tgpd_picture_filelist.append($list);
		}

      for (var i = 0; i < files.length; i++) {
         var $list_item = jQuery(tgpd_picture_list_item);
         var $image = jQuery('img', $list_item);
         var $status = jQuery('.status', $list_item);
         var $info = jQuery('.info', $list_item);
         var $progress_bar = jQuery('.progressBar', $list_item);
         $image[0].file = files[i];
         $image[0].status = $status;
         $image[0].progress = $progress_bar;
         $image.addClass('picture_obj');
         $image.attr('src', window.URL.createObjectURL(files[i]));
         $image.on('load', function(event) {
            window.URL.revokeObjectURL(this.src);
         });
         $info.html(files[i].name + ': ' + files[i].size + ' bytes');
         $list.append($list_item);
      }
      if ($tgpd_picture_filelist_controls.is(':hidden')) $tgpd_picture_filelist_controls.show();
      var ev = jQuery._data($tgpd_picture_filelist[0], 'events');

      if(!(ev && ev.click)) {
         $tgpd_picture_filelist.click(function(event) {
            tgpd_toggle_picture_filelist_controls(event, this);

         });
	 }

	 jQuery("#div_tgpd_picture_filelist_controls").show();
		jQuery('#btn_tgpd_upload_picture_filelist').removeAttr('disabled');
		jQuery('#btn_tgpd_upload_picture_filelist').removeClass('cust-disable');

   }
}

<!-- krishan Notes tabs start function -->

function tgpd_handle_picture_files_notes(files) {
   var $tgpd_picture_filelist = jQuery('#div_upg_picture_filelist_notes');
   var $tgpd_picture_filelist_controls = jQuery('#div_upg_picture_filelist_controls_notes');

   if (!files.length) {
   } else {
      var $list = jQuery('#div_upg_picture_filelist_notes ul');
      if ($list.length == 0) {
         $list = jQuery(tgpd_picture_list);
         $tgpd_picture_filelist.append($list);
		}

      for (var i = 0; i < files.length; i++) {
         var $list_item = jQuery(tgpd_picture_list_item);
         var $image = jQuery('img', $list_item);
         var $status = jQuery('.status', $list_item);
         var $info = jQuery('.info', $list_item);
         var $progress_bar = jQuery('.progressBar', $list_item);
         $image[0].file = files[i];
         $image[0].status = $status;
         $image[0].progress = $progress_bar;
         $image.addClass('picture_obj');
         $image.attr('src', window.URL.createObjectURL(files[i]));
         $image.on('load', function(event) {
            window.URL.revokeObjectURL(this.src);
         });
         $info.html(files[i].name + ': ' + files[i].size + ' bytes');
         $list.append($list_item);
      }

      if ($tgpd_picture_filelist_controls.is(':hidden')) $tgpd_picture_filelist_controls.show();
      var ev = jQuery._data($tgpd_picture_filelist[0], 'events');

	  if(!(ev && ev.click)) {
		$tgpd_picture_filelist.click(function(event) {
            tgpd_toggle_picture_filelist_controls_notes(event, this);

         });
	 }


   }
}



function tgpd_handle_sketch_file_notes(file) {

   var $tgpd_sketch_filelist = jQuery('#div_usg_sketch_filelist_notes');
   var $tgpd_sketch_filelist_controls = jQuery("#div_usg_sketch_filelist_controls_notes");
   var $list = jQuery('#div_usg_sketch_filelist_notes ul');
   if ($list.length == 0) {
      $list = jQuery(tgpd_sketch_list);
      $tgpd_sketch_filelist.append($list);
   }
   var $list_item = jQuery(tgpd_sketch_list_item);
   var $image = jQuery('img', $list_item);
   var $status = jQuery('.status', $list_item);
   var $info = jQuery('.info', $list_item);
   var $progress_bar = jQuery('.progressBar', $list_item);
   $image[0].file = file;
   $image[0].status = $status;
   $image[0].progress = $progress_bar;
   $image.addClass('sketch_obj');
   $image.attr('src', window.URL.createObjectURL(file));
   $image.on('load', function(event) {
      window.URL.revokeObjectURL(this.src);
   });
   $info.html(file.name + ': ' + file.size + ' bytes');
   $list.append($list_item);
   if ($tgpd_sketch_filelist_controls.is(':hidden')) $tgpd_sketch_filelist_controls.show();
   var ev = jQuery._data($tgpd_sketch_filelist[0], 'events');
   if(!(ev && ev.click)) {
      $tgpd_sketch_filelist.click(function(event) {
         tgpd_toggle_sketch_filelist_controls(event, this);
      });
   }
}

<!-- krishan notes tab end function-->


function tgpd_handle_sketch_file(file) {
   var $tgpd_sketch_filelist = jQuery('#div_tgpd_sketch_filelist');
   var $tgpd_sketch_filelist_controls = jQuery("#div_tgpd_sketch_filelist_controls");
   var $list = jQuery('#div_tgpd_sketch_filelist ul');
   if ($list.length == 0) {
      $list = jQuery(tgpd_sketch_list);
      $tgpd_sketch_filelist.append($list);
   }
   var $list_item = jQuery(tgpd_sketch_list_item);
   var $image = jQuery('img', $list_item);
   var $status = jQuery('.status', $list_item);
   var $info = jQuery('.info', $list_item);
   var $progress_bar = jQuery('.progressBar', $list_item);
   $image[0].file = file;
   $image[0].status = $status;
   $image[0].progress = $progress_bar;
   $image.addClass('sketch_obj');
   $image.attr('src', window.URL.createObjectURL(file));
   $image.on('load', function(event) {
      window.URL.revokeObjectURL(this.src);
   });
   $info.html(file.name + ': ' + file.size + ' bytes');
   $list.append($list_item);
   if ($tgpd_sketch_filelist_controls.is(':hidden')) $tgpd_sketch_filelist_controls.show();
   var ev = jQuery._data($tgpd_sketch_filelist[0], 'events');
   if(!(ev && ev.click)) {
      $tgpd_sketch_filelist.click(function(event) {
         tgpd_toggle_sketch_filelist_controls(event, this);
      });
   }
}

jQuery(document).ready(function($) {
	$('#select_tgpd_servicetype').change(function(){
			if($(this).val() == 'Removal/Reinstall_different_day'){
				$('#secondryquote').show();
				}else{
					$('#secondryquote').hide();
					}
												  });

	$('#text_tgpd_daylight').change(function(){

			  var tgpd_checked = $(this).prop('checked')?1:0;

			  if(tgpd_checked==1)
			  {
			//	  $("#daylightsetglasspiece").hide();
				//$("#text_tgpd_daylight_width").attr("disabled",true);
				//$("#text_tgpd_daylight_height").attr("disabled",true);
			  }
			  else
			  {
			//	  	 $("#daylightsetglasspiece").show();
				  //$("#text_tgpd_daylight_width").attr("disabled",false);
					//$("#text_tgpd_daylight_height").attr("disabled",false);
			  }
		/*	$('#text_tgpd_gosize').change(function(){

			  var tgpd_checked = $(this).prop('checked')?1:0;
			  if(tgpd_checked==1)
			  {
				  	 $("#div_tgpd_go_size").css("display","none");
					 
			  }
			  else
			  {
				  	$("#div_tgpd_go_size").css("display","block");
			  }

				});*/

			});

			$('#text_tgpd_gosize').change(function(){
		var tgpd_checked = $(this).prop('checked')?1:0;
		if(tgpd_checked==1)
			  {
				$("#hide_height_width").hide();
					$("#verify_go_size_width_go_width").css("visibility", "hidden");
				$("#ttgh").css("visibility", "hidden");
//				$("#verify_go_size_width_go_width").hide();
				
				/* $("#text_tgpd_go_height1").hide();
				$('label[for="verify_go_size_widths"]').hide();
				$('label[for="verify_go_size_heights"]').hide();
				$("#verify_go_size_heights").hide(); */
				// $("#verify_go_size_width_go_width").hide();
				// $("#text_tgpd_go_height").hide();
			  }
			  else
			  {
				$("#hide_height_width").show();
				
				$("#verify_go_size_width_go_width").css("visibility", "visible");
				$("#ttgh").css("visibility", "visible");
			//	$("#verify_go_size_width_go_width").hide();
				/* $("#text_tgpd_go_height1").show();
				$('label[for="verify_go_size_widths"]').show();
				$('label[for="verify_go_size_heights"]').show(); */
				// $("#verify_go_size_width_go_width").show();
				// $("#text_tgpd_go_height").show();
			  }

	});


			$('#text_tgpd_daylight').change(function(){
		var tgpd_checked = $(this).prop('checked')?1:0;
		if(tgpd_checked==1)
			  {
				$("#hide_width_height").hide();$("#div_tgpd_day_size_plus").hide();
//				$("#ttdw").addAttr();
				$("#ttdw").css("visibility", "hidden");
				$("#ttdh").css("visibility", "hidden");
	//			$("#ttdh").hide();
				/* $("#text_tgpd_daylight_height").hide();
				$('label[for="verify_tgpd_daylight_width"]').hide();
				$('label[for="verify_tgpd_daylight_height"]').hide();
				$("#verify_go_size_heights").hide(); */
				// $("#verify_go_size_width_go_width").hide();
				// $("#text_tgpd_go_height").hide();
			  }
			  else
			  {
				$("#hide_width_height").show();$("#div_tgpd_day_size_plus").show();
				
				$("#ttdw").css("visibility", "visible");
				$("#ttdh").css("visibility", "visible");
		//		$("#ttdw").show();
		//		$("#ttdh").show();
				/* $("#text_tgpd_daylight_height").show();
				$('label[for="verify_tgpd_daylight_width"]').show();
				$('label[for="verify_tgpd_daylight_height"]').show(); */
				// $("#verify_go_size_width_go_width").show();
				// $("#text_tgpd_go_height").show();
			  }

			});


   $('#select_tgpd_glass_type').change(function(event) {
      var tgpd_glass_type = $(this).val();
      console.log('select_tgpd_glass_type.change: tgpd_glass_type -> ' + tgpd_glass_type);
      tgpd_set_glass_type_state(tgpd_glass_type, false);
   });
   $('#select_tgpd_glass_color').change(function(event) {
      var tgpd_glass_color = $(this).val();
      console.log('select_tgpd_glass_color.change: tgpd_glass_color -> ' + tgpd_glass_color);
      tgpd_set_glass_color_state(tgpd_glass_color, true);
   });
   $('input:checkbox[name=checkbox_tgpd_glass_reminders]').change(function(event) {
																		  // alert("iyuiyuiy");
      var tgpd_glass_reminders = $(this).val();
      var tgpd_checked = $(this).prop('checked')?1:0;
	  //alert("opuiou"+tgpd_checked);
      if (tgpd_glass_reminders.localeCompare('solar_film') == 0) {
        tgpd_set_glass_reminders_solar_film1_state(tgpd_checked, false);
      }
      if (tgpd_glass_reminders.localeCompare('wet_seal') == 0) {
         tgpd_set_glass_reminders_wet_seal_state(tgpd_checked, false);
      }
      if (tgpd_glass_reminders.localeCompare('furniture_to_move') == 0) {
         tgpd_set_glass_reminders_furniture_to_move_state(tgpd_checked, true);
      }
      if (tgpd_glass_reminders.localeCompare('walls_or_ceilings_to_cut') == 0) {
         tgpd_set_glass_reminders_walls_or_ceilings_to_cut1_state(tgpd_checked, false);
      }
   });
   $('input:radio[name=radio_tgpd_glass_reminders_solar_film_responsibility]').change(function(event) {
      var tgpd_solar_film_responsibility = $(this).val();
      console.log('radio_tgpd_glass_reminders_solar_film_responsibility.change: tgpd_solar_film_responsibility -> ' + tgpd_solar_film_responsibility);
      tgpd_set_glass_reminders_solar_film2_state(tgpd_solar_film_responsibility, true);
   });
   $('input:radio[name=radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility]').change(function(event) {
      var tgpd_walls_or_ceilings_to_cut_responsibility = $(this).val();
      console.log('radio_tgpd_glass_reminders_walls_or_ceilings_to_cut_responsibility.change: tgpd_walls_or_ceilings_to_cut_responsibility -> ' + tgpd_walls_or_ceilings_to_cut_responsibility);
      tgpd_set_glass_reminders_walls_or_ceilings_to_cut2_state(tgpd_walls_or_ceilings_to_cut_responsibility, true);
   });
   $('#select_tgpd_glass_set').change(function(event) {
      set_text_tgpd_go_width(true);
      set_text_tgpd_go_height(true);
       $("#verify_set_width").prop("checked",true);
	   $("#verify_set_height").prop("checked",true);
   });
   $('#text_tgpd_daylight_width').change(function(event) {
      set_text_tgpd_go_width(true);
   });
   $('#text_tgpd_daylight_height').change(function(event) {
      
      set_text_tgpd_go_height(true);
   });
   $('#text_tgpd_extra_width').change(function(event) {
       $("#verify_set_width").prop('checked',true);
      set_text_tgpd_go_width(false);
   });
   $('#text_tgpd_extra_height').change(function(event) {
        $("#verify_set_height").prop('checked',true);
      set_text_tgpd_go_height(false);
   });
   $('#select_tgpd_size_verification_status').change(function(event) {
      var tgpd_size_verification_status = $(this).val();
      tgpd_set_size_verification_status_state(tgpd_size_verification_status);
   });
   $('input:checkbox[name=checkbox_tgpd_glass_pricing_sag_or_quote]').change(function(event) {
      var tgpd_glass_pricing = $(this).val();
      var tgpd_checked = $(this).prop('checked');

      if (tgpd_glass_pricing.localeCompare('sag_or_quote') == 0) {
         tgpd_set_glass_pricing_sag_or_quote_state(tgpd_checked, true);
      }
   });
   $('#checkbox_tgpd_glass_grids').change(function(event) {
      var tgpd_checked = $(this).prop('checked');
      tgpd_set_glass_grids_state(tgpd_checked, true);
   });
   $('#checkbox_tgpd_glass_fabrication').change(function(event) {
      var tgpd_checked = $(this).prop('checked');
      tgpd_set_glass_fabrication_state(tgpd_checked, true);
   });
   $('#file_tgpd_glass_picture').change(function(event) {
      tgpd_handle_picture_files(this.files);
   });

   $('#file_tgpd_notes_picture').change(function(event) {
      tgpd_handle_picture_files_notes(this.files);
	  jQuery('#btn_upg_upload_picture_filelist_notes').removeAttr('disabled');
	jQuery('#btn_upg_upload_picture_filelist_notes').removeClass('cust-disable');
   });


   $('#canvas_tgpd_glass_sketch').dblclick(function(event) {
      tgpd_toggle_canvas_controls(event, this);
   });
   $('#btn_tgpd_clear_canvas').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_canvas();
   });
   // krishan notes clear_canvas start//
   $('#btn_usg_clear_canvas_notes').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_canvas_notes();
   });

   $('#btn_usg_canvas_to_sketch_filelist_notes').click(function(event) {
		jQuery('#btn_usg_upload_sketch_filelist_notes').removeAttr('disabled');
		jQuery('#btn_usg_upload_sketch_filelist_notes').removeClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      tgpd_canvas_to_sketch_filelist_notes();
      $('#btn_usg_clear_canvas_notes').click();
   });


   ////notes clear_canvas end//
   $('#btn_tgpd_canvas_to_sketch_filelist').click(function(event) {

														 //   alert(tgpd_sketches_download_url.length);
      event.preventDefault();
      event.stopPropagation();
      tgpd_canvas_to_sketch_filelist();
	  jQuery('#btn_tgpd_upload_sketch_filelist').removeAttr('disabled');
	jQuery('#btn_tgpd_upload_sketch_filelist').removeClass('cust-disable');
      $('#btn_tgpd_clear_canvas').click();
   });
   $('#btn_tgpd_clear_picture_filelist').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_glass_picture_filelist();
   });
   $('#btn_tgpd_upload_picture_filelist').click(function(event) {
	jQuery(this).attr('disabled','disabled');
	jQuery(this).addClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      tgpd_upload_glass_picture_filelist();
	  tgpd_editingsave=true;
   });
   $('#canvas-wrap h5').click(function(event) {
      event.preventDefault();
      tgpd_toggle_canvas_holder(event, this);
   });

 //krishan start click function //
 $('#usg-canvas-wrap > #h5notes').click(function(event) {
	  event.preventDefault();
      tgpd_toggle_canvas_holder_notes(event, this);
   });
  /*$('#btn_usg_clear_sketch_filelist_notes').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_glass_sketch_filelist_notes();
   });*/
 // krishan end click function end//

   $('#btn_tgpd_clear_sketch_filelist').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_clear_glass_sketch_filelist();
   });
   $('#btn_tgpd_upload_sketch_filelist').click(function(event) {
		jQuery('#btn_tgpd_upload_sketch_filelist').attr('disabled','disabled');
	jQuery('#btn_tgpd_upload_sketch_filelist').addClass('cust-disable');
      event.preventDefault();
      event.stopPropagation();
      tgpd_upload_glass_sketch_filelist();
	  tgpd_editingsave=true;
   });
   $("#list_tgpd_canvas_marker_size a").click(function(event) {
      event.preventDefault();
	      $("#btn_tgpd_canvas_marker_size").dropdown("toggle");
   });


   $("#list_tgpd_canvas_marker_color a").click(function(event) {
      event.preventDefault();
      $("#btn_tgpd_canvas_marker_color").dropdown("toggle");
   });
   //krishan notes size start//

   $("#list_usg_canvas_marker_size_notes a").click(function(event) {
	 event.preventDefault();
	$("#btn_usg_canvas_marker_size_notes").dropdown("toggle");
  });


   //krishan notes size end //
//alert("kljkljkljklj");

   $('#btn_tgpd_add').click(function(event) {

			//if(getParameterByName('job_id')!=''){
					//tgpd_editing=true;
		//}


		/*if(tgpd_editing == true)
		{
			//alert("opiuopuiop");
			if(saveclickserachtech() !=true)
			{
				return false;
			}

		}
		if(tgpd_editingsave==true)
		{
			if(saveclickserachtech() !=true)
			{
				return false;
			}

		}
			*/

      event.preventDefault();
      event.stopPropagation();
      tgpd_editing = true;
      tgpd_clear_form();
      tgpd_set_toolbar_navigation_buttons_state();
      tgpd_set_toolbar_editing_buttons_state();
      // set focus to first textbox
   });

  function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

   $('#btn_tgpd_save').click(function(event) {
		/*if(getParameterByName('job_id')!=''){
			tgpd_editing=true;
		}	*/

	// alert(tgpd_editing);
      event.preventDefault();
      event.stopPropagation();
      if (!validateFormGPD()) {
         console.log('btn_tgpd_save.click: validateFormGPD() -> false');
         return false;
      }
      console.log('btn_tgpd_save.click: validateFormGPD() -> true');
      var tgpd_item = tgpd_get_item_from_form();
//alert(JSON.stringify(tgpd_item));
	 // alert("sdfsdfsd");
      if (tgpd_editing) {
         tgpd_editing = false;
         tgpd_items.push(tgpd_item);
         if (tgpd_current_item == -1 || tgpd_items.length == 1) {
            tgpd_current_item = 0;
         }
         tgpd_show_current_item();
         tgpd_set_toolbar_navigation_buttons_state();
         tgpd_set_toolbar_editing_buttons_state();
      } else {
         tgpd_items[tgpd_current_item] = tgpd_item;
      }
      // set focus to first textbox
   });
   $('#btn_tgpd_delete').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      if (tgpd_editing) {
         tgpd_editing = false;
         if (!(tgpd_current_item == -1) && !(tgpd_items.length == 0)) {
            tgpd_show_current_item();
         }
         tgpd_set_toolbar_navigation_buttons_state();
         tgpd_set_toolbar_editing_buttons_state();
      } else {
         if (!confirm('Delete this record?')) return;
         tgpd_items.splice(tgpd_current_item, 1);
         if (tgpd_items.length == 0) {
            tgpd_current_item = -1;
         } else {
            if (tgpd_current_item == tgpd_items.length) {
               tgpd_current_item = tgpd_items.length - 1;
            }
         }
         if (!(tgpd_current_item == -1) && !(tgpd_items.length == 0)) {
            tgpd_show_current_item();
         } else {
            tgpd_clear_form();
         }
         tgpd_set_toolbar_navigation_buttons_state();
         tgpd_set_toolbar_editing_buttons_state();
      }
      // set focus to first textbox
   });
   $('#btn_tgpd_refresh').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_show_current_item();
   });
   $('#btn_tgpd_copy_specs_plus_size').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_copying = true;
      tgpd_copy_operation = 0;
      tgpd_clipboard = tgpd_get_item_from_form();
      tgpd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tgpd_copy_specs').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_copying = true;
      tgpd_copy_operation = 1;
      tgpd_clipboard = tgpd_get_item_from_form();
      tgpd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tgpd_paste').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tgpd_print_item_to_form(tgpd_clipboard);
      tgpd_clipboard = {};
      tgpd_copying = false;
      tgpd_copy_operation = -1;
      tgpd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tgpd_first').click(function(event) {

			/*	if(saveclickserachtech() !=true)
				{
					return false;
				}*/

      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item = 0;
      tgpd_show_current_item();
      tgpd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_tgpd_previous').click(function(event) {

			/*	if(saveclickserachtech() !=true)
				{
					return false;
				}
				*/
      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item--;
      tgpd_show_current_item();
      tgpd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_tgpd_next').click(function(event) {



		/*	if(saveclickserachtech() !=true)
			{
				return false;
			}
			*/



      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item++;
      tgpd_show_current_item();
      tgpd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_tgpd_last').click(function(event) {

	/*if(saveclickserachtech() !=true)
			{
				return false;
			}*/
      event.preventDefault();
      event.stopPropagation();
      tgpd_current_item = tgpd_items.length - 1;
      tgpd_show_current_item();
      tagd_item
   });

    $('#select_tgpd_glass_type').change(function(){
     if($(this).val()=='igu_lami_temp_lami' || $(this).val()=='igu_sky_lite_temp_or_lami'){
		 $('#lamidrophide').show();
		 $('#litethick_see').hide();
		}else{
		 $('#lamidrophide').hide();
		  $('#litethick_see').show();
	 }
   });
	 jQuery('#select_tgpd_glass_shape').change(function(){
			var tgpd_glass_shape = jQuery('#select_tgpd_glass_shape').val();

			if (typeof tgpd_glass_shape === 'undefined' || tgpd_glass_shape == null || tgpd_glass_shape == 'Rectangular/Square') tgpd_glass_shape = 'not_applicable';
				   if (tgpd_glass_shape.localeCompare('not_applicable') != 0) {
							jQuery('.shape-container').hide();
							jQuery('.shape-container-day').hide();
							jQuery('.shape-container-go').hide();
							jQuery('#tgpd-type_'+jQuery(this).val()+'-container').show();
				   }else{
					   		jQuery('.shape-container').hide();
							jQuery('.shape-container-day').show();
							jQuery('.shape-container-go').show();
					   }
   });
   		$('#handlea_label').hide();
				$('#handleb_label').hide();
				$('#handlec_label').hide();
				$('#handled_label').hide();
				$('#handlee_label').hide();

				$('.handle_bp_a').hide();
				$('.handle_bp_b').hide();
				$('.handle_bp_c').hide();
				$('.handle_bp_d').hide();
				$('.hendle_bp_e').hide();

				$('.handle_f_a').hide();
				$('.handle_f_b').hide();
				$('.handle_f_c').hide();
				$('.handle_f_d').hide();
				$('.hendle_f_e').hide();

				$('.handle_p_a').hide();
				$('.handle_p_b').hide();
				$('.handle_p_c').hide();
				$('.handle_p_d').hide();
				$('.handle_p_e').hide();

				$('.handle_wp_a').hide();
				$('.handle_wp_b').hide();
				$('.handle_wp_c').hide();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').hide()

   $('#all_replacement_heandle_type_wp_single_door').change(function(){

		var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
		var id= $(this).find('option:selected').val();
		$('.type_wp_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');



		var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){
				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show()
				$('.handle_wp_c').show()
				$('.handle_wp_d').hide()
				$('.handle_wp_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').hide();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').show();
				$('.handle_wp_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').show();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_wp_a').show();
				$('.handle_wp_b').show();
				$('.handle_wp_c').hide();
				$('.handle_wp_d').hide();
				$('.handle_wp_e').hide();

			}

  	})

    $('#all_replacement_heandle_type_p_single_door').change(function(){

		var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
		var id= $(this).find('option:selected').attr('id');
		$('.type_p_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');

		var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').hide();
				$('.handle_p_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){
				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').hide();
				$('.handle_p_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show()
				$('.handle_p_c').show()
				$('.handle_p_d').hide()
				$('.handle_p_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').hide();
				$('.handle_p_d').hide();

				$('.handle_p_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').show();
				$('.handle_p_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').show();
				$('.handle_p_d').hide();
				$('.handle_p_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_p_a').show();
				$('.handle_p_b').show();
				$('.handle_p_c').hide();
				$('.handle_p_d').hide();
				$('.handle_p_e').hide();

			}

  	})

	$('#all_replacement_heandle_type_bp_single_door').change(function(){

			var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
			var id= $(this).find('option:selected').attr('id');
			$('.type_bp_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');
			var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){
				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show()
				$('.handle_bp_c').show()
				$('.handle_bp_d').hide()
				$('.handle_bp_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').hide();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').show();
				$('.handle_bp_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').show();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_bp_a').show();
				$('.handle_bp_b').show();
				$('.handle_bp_c').hide();
				$('.handle_bp_d').hide();
				$('.handle_bp_e').hide();

			}

	})

	$('#all_replacement_heandle_type_f_single_door').change(function(){

			var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
			var id= $(this).find('option:selected').attr('id');
			$('.type_f_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');


			var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').hide();
				$('.hendle_f_e').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').hide();
				$('.hendle_f_e').show();

			}


			if(values=="pull_handle_2_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show()
				$('.handle_f_c').show()
				$('.handle_f_d').hide()
				$('.hendle_f_e').hide()

			}

			if(values=="pull_bar_2_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').hide();
				$('.handle_f_d').hide();
				$('.hendle_f_e').show();

			}


			if(values=="ladder_pull_3_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').show();
				$('.hendle_f_e').hide();

			}

			if(values=="ladder_pull_2_holes"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').show();
				$('.handle_f_d').hide();
				$('.hendle_f_e').hide();

			}

			if(values=="single_pull_1_hole"){

				$('.handle_f_a').show();
				$('.handle_f_b').show();
				$('.handle_f_c').hide();
				$('.handle_f_d').hide();
				$('.hendle_f_e').hide();

			}
	})

	$('#all_replacement_heandle_type_a_single_door').change(function(){

			var imgurl_heandle= $(this).find('option:selected').attr('imgurl');
			var id= $(this).find('option:selected').attr('id');
			$(this).parent().parent().find('.type_a_sigle_image').html('<img src="'+imgurl_heandle+'" class="door-diagram-img" />');

			var values= $(this).val();
			if(values=="pull_handle_up_push_bar_3_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').hide();
				$('#handlee').show();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').show();


			}


			if(values=="pull_handle_down_push_bar_3_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').hide();
				$('#handlee').show();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').show();

			}


			if(values=="pull_handle_2_holes"){


				$('#handlea').show();
				$('#handleb').show()
				$('#handlec').show()
				$('#handled').hide()
				$('#handlee').hide()

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').hide();

			}

			if(values=="pull_bar_2_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').hide();
				$('#handled').hide();
				$('#handlee').show();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').hide();
				$('#handled_label').hide();
				$('#handlee_label').show();

			}


			if(values=="ladder_pull_3_holes"){
				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').show();
				$('#handlee').hide();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').show();
				$('#handlee_label').hide();


			}

			if(values=="ladder_pull_2_holes"){

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').show();
				$('#handled').hide();
				$('#handlee').hide();

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').show();
				$('#handled_label').hide();
				$('#handlee_label').hide();

			}

			if(values=="single_pull_1_hole"){

				$('#handlea_label').show();
				$('#handleb_label').show();
				$('#handlec_label').hide();
				$('#handled_label').hide();
				$('#handlee_label').hide();

				$('#handlea').show();
				$('#handleb').show();
				$('#handlec').hide();
				$('#handled').hide();
				$('#handlee').hide();
			}

		});
			$('.custom_inner_leval_thinkness').hide();
			$('.select_tgpd_lite_thickness_lebel').hide();
			$('#select_tgpd_lite_thickness').hide();
			$('#verify_tgpd_lite_thickness').hide(); //date @02-11-2017
			$('#vtlt').hide();

				 $('#select_tgpd_innner_layer_thickness').change(function(){
					   if($(this).val()=='inner_thickness_custom'){
						$('.custom_inner_leval_thinkness').show();
					  }else{
						$('.custom_inner_leval_thinkness').hide();
					  }
				 });


			 $('#select_tgpd_glass_type').change(function(){

				if($(this).val()=='igu_window' || $(this).val()=='igu_door_lite' ||  $(this).val()=='igu_sky_lite_temp_or_lami' || $(this).val()=='igu_lami_temp_lami' || $(this).val()=='igu_spandrel' || $(this).val()=='igu_patio_door_or_panel' ||  $(this).val()=='bullet_proof_igu' || $(this).val()=='curved_igu' ){

					$('#verify_tgpd_lite_thickness').show();////date @02-11-2017
					$('.select_tgpd_lite_thickness_lebel').show();
					$('#select_tgpd_lite_thickness').show();$('#vtlt').show();

				}else{

					$('.select_tgpd_lite_thickness_lebel').hide();
					$('#select_tgpd_lite_thickness').hide();
					$('#verify_tgpd_lite_thickness').hide();////date @02-11-2017
					$('#vtlt').hide();
				}
			});
	var ajaxurl_image = jQuery('#ajax_url_image_url').val();
    jQuery( "#text_tgpd_glass_secondryquote" ).autocomplete({
      source: function( request, response ) {
        jQuery.ajax( {
         url: ajaxurl_image,
         dataType: "json",
          data: {
			'action':'get_quotenumbers',
            'term': request.term
          },
          success: function( data ) {
            response( data );
          }
        } );
      },
      minLength: 4,
      select: function( event, ui ) {
        //ui.item.id
		jQuery('#childjobid').val(ui.item.id);
      }
    } );
});
function saveclickserachtech()
{

			if (!validateFormGPD()) {
			console.log('btn_tgpd_save.click: validateFormGPD() -> false');
			return false;
			}
			console.log('btn_tgpd_save.click: validateFormGPD() -> true');
			var tgpd_item = tgpd_get_item_from_form();
			//alert(JSON.stringify(tgpd_item));
			// alert("sdfsdfsd");
			if (tgpd_editing) {
			tgpd_editing = false;
			tgpd_items.push(tgpd_item);
			if (tgpd_current_item == -1 || tgpd_items.length == 1) {
			tgpd_current_item = 0;
			}
			tgpd_show_current_item();
			tgpd_set_toolbar_navigation_buttons_state();
			tgpd_set_toolbar_editing_buttons_state();
			} else {
			tgpd_items[tgpd_current_item] = tgpd_item;
			}

			return true;
			// set focus to first textbox
}


 //value without decimal
 	function isNumberkey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if ((charCode < 48 || charCode > 57))
        return false;

    return true;

	}
	//value with decimal

	function myNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode;
    if (charCode != 46 && charCode > 31
    && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

	// Take number two digit after decimal

 	function myfunction(){
 	jQuery('.text_tagd_shape_left_leg, .text_tagd_shape_width,.text_tagd_shape_right_leg,.text_tagd_shape_top,.text_tagd_shape_base,.text-num').keypress(function(event) {
    var $this = jQuery(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
       ((event.which < 48 || event.which > 57) &&
       (event.which != 0 && event.which != 8))) {
           event.preventDefault();
    }

    var text = jQuery(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function() {
            if ($this.val().substring($this.val().indexOf('.')).length > 3) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 3));
            }
        }, 1);
    }

    if ((text.indexOf('.') != -1) &&
        (text.substring(text.indexOf('.')).length > 2) &&
        (event.which != 0 && event.which != 8) &&
        (jQuery(this)[0].selectionStart >= text.length - 2)) {
            event.preventDefault();
    }
});

jQuery('.number').bind("paste", function(e) {
var text = e.originalEvent.clipboardData.getData('Text');
if (jQuery.isNumeric(text)) {
    if ((text.substring(text.indexOf('.')).length > 3) && (text.indexOf('.') > -1)) {
        e.preventDefault();
       jQuery(this).val(text.substring(0, text.indexOf('.') + 3));
   }
}
else {
        e.preventDefault();
     }
});

	}
jQuery('.verify-hidden-checkbox').change(function(){
  $( this ).addClass('verify-active');
})

jQuery('#exterior_light_thickness').change(function() {
		jQuery('#verify_tgpd_exterior_lite_thickness').prop('checked',true).addClass('verify-active');
	});

	jQuery('#interior_light_thickness').change(function() {  jQuery('#verify_tgpd_interior_lite_thickness').prop('checked',true).addClass('verify-active');;	});

jQuery('#verify_go_size_width_text').change(function() {  jQuery('#verify_go_size_widths').prop('checked',true).addClass('verify-active');;	});

jQuery('#text_tgpd_go_height1').change(function() {  jQuery('#verify_go_size_heights').prop('checked',true).addClass('verify-active');;	});


	jQuery('#select_tgpd_innner_layer_thickness').change(function() {  jQuery('#verify_tgpd_inner_layer_thickness').prop('checked',true).addClass('verify-active');;});

	jQuery('#select_tgpd_overall_thickness').change(function() {  jQuery('#verify_tgpd_overall_thickness').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_lite_thickness').change(function() {  		jQuery('#verify_tgpd_lite_thickness').prop('checked',true).addClass('verify-active');;		});

	jQuery('#select_tgpd_glass_color').change(function() {  	jQuery('#verify_tgpd_glass_color').prop('checked',true).addClass('verify-active');;	});

	jQuery('#text_tgpd_glass_color_note').change(function() {  	jQuery('#verify_tgpd_glass_color_note').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_glass_set').change(function() {  	jQuery('#verify_tgpd_glass_set').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_glass_treatment').change(function() {  	jQuery('#verify_tgpd_glass_treatment').prop('checked',true).addClass('verify-active');;	});

	jQuery('#select_tgpd_spacer_color').change(function() {  	jQuery('#verify_tgpd_spacer_color').prop('checked',true).addClass('verify-active');;	});




	jQuery('#text_tgpd_glass_lift').change(function() {
    jQuery('#verify_tgpd_glass_lift').addClass('verify-active');
		if(jQuery('#text_tgpd_glass_lift').val().length>0){
			jQuery('#verify_tgpd_glass_lift').prop('checked',true);
		}else if(jQuery('#text_tgpd_glass_lift').val().length==0){
			jQuery('#verify_tgpd_glass_lift').prop('checked',false);
		}
	});



	jQuery('#text_tgpd_daylight_width').change(function() {
    jQuery('#text_tgpd_daylight_width').addClass('verify-active');
		if(jQuery('#text_tgpd_daylight_width').val().length>0){
			jQuery('#verify_tgpd_daylight_width').prop('checked',true);
     
		}else if(jQuery('#text_tgpd_daylight_width').val().length==0){
			jQuery('#verify_tgpd_daylight_width').prop('checked',false);
      
		}
	});


	//jQuery('#text_tgpd_daylight_height').change(function() {  	jQuery('#verify_tgpd_daylight_height').prop('checked',true);	});

	jQuery('#text_tgpd_daylight_height').change(function() {
    jQuery('#text_tgpd_daylight_height').addClass('verify-active');
		if(jQuery('#text_tgpd_daylight_height').val().length>0){
			jQuery('#verify_tgpd_daylight_height').prop('checked',true);
     
		}else if(jQuery('#text_tgpd_daylight_height').val().length==0){
			jQuery('#verify_tgpd_daylight_height').prop('checked',false);
      
			}

	});


	jQuery('#text_tgpd_go_width').change(function() {
    jQuery('#text_tgpd_go_width').addClass('verify-active');
		if(jQuery('#text_tgpd_go_width').val().length>0){
			jQuery('#verify_go_size_width').prop('checked',true);
		}else if(jQuery('#text_tgpd_go_width').val().length==0){
			jQuery('#verify_go_size_width').prop('checked',false);
		}

	});
	//jQuery('#text_tgpd_go_width').change(function() {  	jQuery('#verify_go_size_width').prop('checked',true);	});
	jQuery('#text_tgpd_go_height').change(function() {
    jQuery('#text_tgpd_go_height').addClass('verify-active');
		if(jQuery('#text_tgpd_go_height').val().length>0){
			jQuery('#verify_go_size_height').prop('checked',true);
		}else if(jQuery('#text_tgpd_go_height').val().length==0){
			jQuery('#verify_go_size_height').prop('checked',false);
		}
	});





	jQuery('#select_tgpd_glass_set').change(function() {

    fillSetFormula();

	})
