(function($) {
  $( document ).ready(function() {
    $( ".ceiling-checkbox" ).change(function(event) { //glass type select input
      event.preventDefault();
      if($(this).is(':checked')){
        $(this).closest('.reminder-row-wrapper').append(
          '<div class="row sub-row ceiling-details">'+
            '<div class="col-md-5">'+
            '<input  type="radio" name="ceiling-source" class="visible" value="Customer" checked>  Customer'+
            '<input  type="radio" name="ceiling-source" class="visible" value="SAG">  SAG'+
            '</div>'+
          '</div>'
        );
      }
      else{
        $(this).closest('.reminder-row-wrapper').find('.ceiling-details').remove();
      }
    });
    $(document).on('change',  "input[name=ceiling-source]:radio", function() { //glass type select input
      if($(this).val() === "SAG"){
        $(this).closest('.ceiling-details').append(
          '<div class="col-md-7 ceiling-subcontractor-col">'+
            '<label>Subcontractor</label>'+
            '<input type="text" class="form-control" id="ceiling-subcontractor" placeholder="Example Company Inc.">'+
          '</div>'
        );
      }
      else{
        $(this).closest('.ceiling-details').find('.ceiling-subcontractor-col').remove();
      }
    });
    $( ".solarfilm-checkbox" ).change(function(event) { //glass type select input
      event.preventDefault();
      if($(this).is(':checked')){
        $(this).closest('.reminder-row-wrapper').append(
          '<div class="row sub-row solarfilm-details">'+
            '<div class="col-md-6">'+
              '<label>Type</label>'+
              '<input class="form-control" type="text" id="solarfilm-type" name="solarfilm-type" placeholder="Type" >'+
            '</div>'+
            '<div id="solar-contractor-col" class="col-md-6">'+
              '<label>Source</label>'+
              '<input type="text" class="form-control" id="solarfilm-source" name="solarfilm-source" placeholder="Source">'+
            '</div>'+
          '</div>'
        );
      }
      else{
        $(this).closest('.reminder-row-wrapper').find('.solarfilm-details').remove();
      }
    });
    $( ".wetseal-checkbox" ).change(function(event) { //glass type select input
      if($(this).is(':checked')){
        $(this).closest('.reminder-row-wrapper').append(
          '<div class="row sub-row wetseal-details">'+
            '<div class="col-md-5">'+
              '<input type="radio" class="visible"  name="wetseal-source" value="Customer" checked>  Customer'+
              '<input type="radio" class="visible" name="wetseal-source" value="SAG" >  SAG'+
            '</div>'+
          '</div>'
        );
      }
      else{
        $(this).closest('.reminder-row-wrapper').find('.wetseal-details').remove();
      }
    });
    $(document).on('change',  "input[name=wetseal-source]:radio", function() { //glass type select input
      if($(this).val() === "SAG"){
        $(this).closest('.wetseal-details').append(
          '<div class="col-md-7 wetseal-subcontractor-col">'+
            '<label for="job-name">Subcontractor</label>'+
            '<input type="text" class="form-control" id="wetseal-subcontractor" placeholder="Example Company Inc.">'+
          '</div>'
        );
      }
      else{
        $(this).closest('.wetseal-details').find('.wetseal-subcontractor-col').remove();
      }
    });

  });
})( jQuery );
