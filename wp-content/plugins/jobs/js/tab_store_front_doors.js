var tsfd_items = [];
var tsfd_current_item = -1;
var tsfd_editing = false;
var tsfd_copying = false;
var tsfd_clipboard = {};
// perhaps also map value to text - for saving to db
var tsfd_map_elevation_type_value_to_index = {
   a : 0,
   b : 1,
   c : 2,
   d : 3,
   e : 4,
   f : 5
};
var tsfd_map_door_opening_value_to_index = {
   lh : 0,
   lhr : 1,
   rh : 2,
   rhr : 3
};

function tsfd_set_toolbar_navigation_item_count() {
   if (tsfd_editing) {
      jQuery('#text_tsfd_item_count').val('Add Item');
   } else {
      jQuery('#text_tsfd_item_count').val((tsfd_current_item + 1) + '/' + tsfd_items.length);
   }
}

function tsfd_init_form_state() {
   jQuery('#row_tsfd_door_opening').hide();
   jQuery('#text_tsfd_finish_other').attr('disabled','disabled');
}

function tsfd_init_toolbar_navigation_buttons_state() {
   jQuery('#btn_tsfd_first').attr('disabled','disabled');
   jQuery('#btn_tsfd_previous').attr('disabled','disabled');
   jQuery('#text_tsfd_item_count').attr('readonly','readonly');
   jQuery('#btn_tsfd_next').attr('disabled','disabled');
   jQuery('#btn_tsfd_last').attr('disabled','disabled');
   tsfd_set_toolbar_navigation_item_count();
}

function tsfd_init_toolbar_editing_buttons_state() {
   jQuery('#btn_tsfd_add').removeAttr('disabled');
   jQuery('#btn_tsfd_save').attr('disabled','disabled');
   jQuery('#btn_tsfd_delete').attr('disabled','disabled');
   jQuery('#btn_tsfd_refresh').attr('disabled','disabled');
   tsfd_init_toolbar_copy_paste_buttons_state();
}

function tsfd_init_toolbar_copy_paste_buttons_state() {
   jQuery('#btn_tsfd_copy').attr('disabled','disabled');
   jQuery('#btn_tsfd_paste').attr('disabled','disabled');
}

function tsfd_set_toolbar_navigation_buttons_state() {
   if (tsfd_current_item == -1 || tsfd_items.length == 0 || tsfd_editing) {
      tsfd_init_toolbar_navigation_buttons_state();
      return;
   }
   if (tsfd_items.length == 1) {
      jQuery('#btn_tsfd_first').attr('disabled','disabled');
      jQuery('#btn_tsfd_previous').attr('disabled','disabled');
      jQuery('#btn_tsfd_next').attr('disabled','disabled');
      jQuery('#btn_tsfd_last').attr('disabled','disabled');
   } else {
      if (tsfd_current_item == 0) {
         jQuery('#btn_tsfd_first').attr('disabled','disabled');
         jQuery('#btn_tsfd_previous').attr('disabled','disabled');
         jQuery('#btn_tsfd_next').removeAttr('disabled');
         jQuery('#btn_tsfd_last').removeAttr('disabled');
      } else if (tsfd_current_item == (tsfd_items.length - 1)) {
         jQuery('#btn_tsfd_first').removeAttr('disabled');
         jQuery('#btn_tsfd_previous').removeAttr('disabled');
         jQuery('#btn_tsfd_next').attr('disabled','disabled');
         jQuery('#btn_tsfd_last').attr('disabled','disabled');
      } else {
         jQuery('#btn_tsfd_first').removeAttr('disabled');
         jQuery('#btn_tsfd_previous').removeAttr('disabled');
         jQuery('#btn_tsfd_next').removeAttr('disabled');
         jQuery('#btn_tsfd_last').removeAttr('disabled');
      }
   }
   tsfd_set_toolbar_navigation_item_count();
}

function tsfd_set_toolbar_editing_buttons_state() {
   if ((!tsfd_editing) && (!tsfd_copying) && (!(tsfd_current_item >= 0 && tsfd_current_item < tsfd_items.length))) {
      tsfd_init_toolbar_editing_buttons_state();
      jQuery('#btn_submit_new_job').removeAttr('disabled');
      return;
   }
   if (tsfd_editing) {
      jQuery('#btn_tsfd_add').attr('disabled','disabled');
      jQuery('#btn_tsfd_save').removeAttr('disabled');
      jQuery('#btn_tsfd_delete').removeAttr('disabled');
      jQuery('#btn_tsfd_refresh').attr('disabled','disabled');
      jQuery('#btn_submit_new_job').attr('disabled','disabled');
   } else {
      jQuery('#btn_tsfd_add').removeAttr('disabled');
      if (tsfd_current_item >= 0 && tsfd_current_item < tsfd_items.length) {
         jQuery('#btn_tsfd_save').removeAttr('disabled');
         jQuery('#btn_tsfd_delete').removeAttr('disabled');
         jQuery('#btn_tsfd_refresh').removeAttr('disabled');
      }
      jQuery('#btn_submit_new_job').removeAttr('disabled');
   }
   tsfd_set_toolbar_copy_paste_buttons_state();
}

function tsfd_set_toolbar_copy_paste_buttons_state() {
   if (tsfd_editing) {
      jQuery('#btn_tsfd_copy').attr('disabled','disabled');
      if (tsfd_copying) {
         jQuery('#btn_tsfd_paste').removeAttr('disabled');
      } else {
         jQuery('#btn_tsfd_paste').attr('disabled','disabled');
      }
   } else {
      if (tsfd_current_item >= 0 && tsfd_current_item < tsfd_items.length) {
         jQuery('#btn_tsfd_copy').removeAttr('disabled');
         if (tsfd_copying) {
            jQuery('#btn_tsfd_paste').removeAttr('disabled');
         } else {
            jQuery('#btn_tsfd_paste').attr('disabled','disabled');
         }
      }
   }
}

function tsfd_clear_form() {
   jQuery('#radio_group_tsfd_elevation_type > .btn').removeClass('active');
   jQuery('#radio_group_tsfd_elevation_type > .btn').eq(0).addClass('active');
   jQuery('#radio_group_tsfd_door_opening > .btn').removeClass('active');
   jQuery('#radio_group_tsfd_door_opening > .btn').eq(0).addClass('active');
   tsfd_set_door_opening_state_by_elevation_type('a');
   jQuery('#text_tsfd_quantity').val('');
   jQuery('#text_tsfd_mark_number').val('');
   jQuery('#select_tsfd_door_style').val(['not_a_value']);
   jQuery('#select_tsfd_door_glass').val(['not_a_value']);
   jQuery('#text_tsfd_frame_mrp_number').val('');
   jQuery('#select_tsfd_frame').val(['not_a_value']);
   jQuery('input:radio[name=radio_tsfd_finish]').val(['not_a_value']);
   jQuery('#text_tsfd_finish_other').val('');
   tsfd_set_finish_other_state_by_finish('not_a_value', false);
   jQuery('#select_tsfd_hanging_hardware').val(['not_a_value']);
   jQuery('#select_tsfd_threshold').val(['not_a_value']);
   jQuery('#select_tsfd_closer').val(['not_a_value']);
   jQuery('#select_tsfd_lock_panic').val(['not_a_value']);
   jQuery('#select_tsfd_push_pull').val(['not_a_value']);
   jQuery('#select_tsfd_mid_panel').val(['not_a_value']);
   jQuery('#select_tsfd_bottom_rail').val(['not_a_value']);
}

function tsfd_set_door_opening_state_by_elevation_type(tsfd_elevation_type) {
   var tsfd_elevation_type_index = tsfd_map_elevation_type_value_to_index[tsfd_elevation_type];
   if (tsfd_elevation_type_index == 2 || tsfd_elevation_type_index == 3 || tsfd_elevation_type_index == 4 || tsfd_elevation_type_index == 5) {
      jQuery('#row_tsfd_door_opening').slideDown();
   } else {
      jQuery('#row_tsfd_door_opening').slideUp();
   }
}

function tsfd_set_finish_other_state_by_finish(tsfd_finish, tsfd_focus) {
   var tsfd_finish_other = jQuery('#text_tsfd_finish_other');
   if (tsfd_finish.localeCompare('other') == 0) {
      tsfd_finish_other.removeAttr('disabled');
      if (tsfd_focus) {
         tsfd_finish_other[0].focus();
      }
   } else {
      tsfd_finish_other.val('');
      tsfd_finish_other.attr('disabled','disabled');
   }
}

function tsfd_show_current_item() {
   tsfd_clear_form();
   var tsfd_item = tsfd_items[tsfd_current_item];
   tsfd_print_item_to_form(tsfd_item);
}

function tsfd_get_item_from_form() {
   var tsfd_elevation_type = jQuery('#radio_group_tsfd_elevation_type > .btn.active > input[type=radio]').val();
   var tsfd_elevation_type_index = tsfd_map_elevation_type_value_to_index[tsfd_elevation_type];
   var tsfd_door_opening = '';
   if (tsfd_elevation_type_index == 2 || tsfd_elevation_type_index == 3 || tsfd_elevation_type_index == 4 || tsfd_elevation_type_index == 5) {
      tsfd_door_opening = jQuery('#radio_group_tsfd_door_opening > .btn.active > input[type=radio]').val();
   }
   var tsfd_quantity = jQuery('#text_tsfd_quantity').val();
   var tsfd_mark_number = jQuery('#text_tsfd_mark_number').val();
   var tsfd_door_style = jQuery('#select_tsfd_door_style').val();
   var tsfd_door_glass = jQuery('#select_tsfd_door_glass').val();
   var tsfd_frame_mrp_number = jQuery('#text_tsfd_frame_mrp_number').val();
   var tsfd_frame = jQuery('#select_tsfd_frame').val();
   var tsfd_finish = jQuery('input:radio[name=radio_tsfd_finish]:checked').val();
   var tsfd_finish_other = '';
   if (tsfd_finish.localeCompare('other') == 0) {
      tsfd_finish_other = jQuery('#text_tsfd_finish_other').val();
   }
   var tsfd_hanging_hardware = jQuery('#select_tsfd_hanging_hardware').val();
   var tsfd_threshold = jQuery('#select_tsfd_threshold').val();
   var tsfd_closer = jQuery('#select_tsfd_closer').val();
   var tsfd_lock_panic = jQuery('#select_tsfd_lock_panic').val();
   var tsfd_push_pull = jQuery('#select_tsfd_push_pull').val();
   var tsfd_mid_panel = jQuery('#select_tsfd_mid_panel').val();
   var tsfd_bottom_rail = jQuery('#select_tsfd_bottom_rail').val();

   var tsfd_item = {};
   tsfd_item.id = 0;
   tsfd_item.job_id = job_id;
   tsfd_item.elevation_type = tsfd_elevation_type;
   tsfd_item.door_opening = tsfd_door_opening;
   tsfd_item.quantity = tsfd_quantity;
   tsfd_item.mark_number = tsfd_mark_number;
   tsfd_item.door_style = tsfd_door_style;
   tsfd_item.door_glass = tsfd_door_glass;
   tsfd_item.frame_mrp_number = tsfd_frame_mrp_number;
   tsfd_item.frame = tsfd_frame;
   tsfd_item.finish = tsfd_finish;
   tsfd_item.finish_other = tsfd_finish_other;
   tsfd_item.hanging_hardware = tsfd_hanging_hardware;
   tsfd_item.threshold = tsfd_threshold;
   tsfd_item.closer = tsfd_closer;
   tsfd_item.lock_panic = tsfd_lock_panic;
   tsfd_item.push_pull = tsfd_push_pull;
   tsfd_item.mid_panel = tsfd_mid_panel;
   tsfd_item.bottom_rail = tsfd_bottom_rail;

   tsfd_print_item_to_console('tsfd_get_item_from_form', tsfd_item);

   return tsfd_item;
}

function tsfd_print_item_to_form(tsfd_item) {
   jQuery('#radio_group_tsfd_elevation_type > .btn').removeClass('active');
   jQuery('#radio_group_tsfd_elevation_type > .btn').eq(tsfd_map_elevation_type_value_to_index[tsfd_item.elevation_type]).addClass('active');
   var tsfd_elevation_type_index = tsfd_map_elevation_type_value_to_index[tsfd_item.elevation_type];
   if (tsfd_elevation_type_index == 2 || tsfd_elevation_type_index == 3 || tsfd_elevation_type_index == 4 || tsfd_elevation_type_index == 5) {
      jQuery('#radio_group_tsfd_door_opening > .btn').removeClass('active');
      jQuery('#radio_group_tsfd_door_opening > .btn').eq(tsfd_map_door_opening_value_to_index[tsfd_item.door_opening]).addClass('active');
   }
   tsfd_set_door_opening_state_by_elevation_type(tsfd_item.elevation_type);
   jQuery('#text_tsfd_quantity').val(tsfd_item.quantity);
   jQuery('#text_tsfd_mark_number').val(tsfd_item.mark_number);
   jQuery('#select_tsfd_door_style').val([tsfd_item.door_style]);
   jQuery('#select_tsfd_door_glass').val([tsfd_item.door_glass]);
   jQuery('#text_tsfd_frame_mrp_number').val(tsfd_item.frame_mrp_number);
   jQuery('#select_tsfd_frame').val([tsfd_item.frame]);
   jQuery('input:radio[name=radio_tsfd_finish]').val([tsfd_item.finish]);
   if (tsfd_item.finish.localeCompare('other') == 0) {
      jQuery('#text_tsfd_finish_other').val(tsfd_item.finish_other);
   }
   tsfd_set_finish_other_state_by_finish(tsfd_item.finish, false);
   jQuery('#select_tsfd_hanging_hardware').val([tsfd_item.hanging_hardware]);
   jQuery('#select_tsfd_threshold').val([tsfd_item.threshold]);
   jQuery('#select_tsfd_closer').val([tsfd_item.closer]);
   jQuery('#select_tsfd_lock_panic').val([tsfd_item.lock_panic]);
   jQuery('#select_tsfd_push_pull').val([tsfd_item.push_pull]);
   jQuery('#select_tsfd_mid_panel').val([tsfd_item.mid_panel]);
   jQuery('#select_tsfd_bottom_rail').val([tsfd_item.bottom_rail]);
}

function tsfd_print_item_to_console(debug_tag, tsfd_item) {
   var log_tag = (debug_tag.length == 0 ? '' : (debug_tag + ': '));
   console.log(log_tag + 'tsfd_id => ' + tsfd_item.id);
   console.log(log_tag + 'tsfd_job_id => ' + tsfd_item.job_id);
   console.log(log_tag + 'tsfd_elevation_type => ' + tsfd_item.elevation_type);
   console.log(log_tag + 'tsfd_door_opening => ' + tsfd_item.door_opening);
   console.log(log_tag + 'tsfd_quantity => ' + tsfd_item.quantity);
   console.log(log_tag + 'tsfd_mark_number => ' + tsfd_item.mark_number);
   console.log(log_tag + 'tsfd_door_style => ' + tsfd_item.door_style);
   console.log(log_tag + 'tsfd_door_glass => ' + tsfd_item.door_glass);
   console.log(log_tag + 'tsfd_frame_mrp_number => ' + tsfd_item.frame_mrp_number);
   console.log(log_tag + 'tsfd_frame => ' + tsfd_item.frame);
   console.log(log_tag + 'tsfd_finish => ' + tsfd_item.finish);
   console.log(log_tag + 'tsfd_finish_other => ' + tsfd_item.finish_other);
   console.log(log_tag + 'tsfd_hanging_hardware => ' + tsfd_item.hanging_hardware);
   console.log(log_tag + 'tsfd_threshold => ' + tsfd_item.threshold);
   console.log(log_tag + 'tsfd_closer => ' + tsfd_item.closer);
   console.log(log_tag + 'tsfd_lock_panic => ' + tsfd_item.lock_panic);
   console.log(log_tag + 'tsfd_push_pull => ' + tsfd_item.push_pull);
   console.log(log_tag + 'tsfd_mid_panel => ' + tsfd_item.mid_panel);
   console.log(log_tag + 'tsfd_bottom_rail => ' + tsfd_item.bottom_rail);
   // console.log(log_tag + 'tsfd_item => ' + JSON.stringify(tsfd_item));
}

function tsfd_load_sample_data() {
   var tsfd_items = [];

   for (var i=0; i<3; i++) {
      var tsfd_item = {};
      tsfd_item.id = 0;
      tsfd_item.job_id = job_id;
      tsfd_item.elevation_type = 'b';
      tsfd_item.door_opening = '';
      tsfd_item.quantity = '12';
      tsfd_item.mark_number = '3043432AQCX';
      tsfd_item.door_style = '375_medium';
      tsfd_item.door_glass = 'quarter_inch';
      tsfd_item.frame_mrp_number = '17089';
      tsfd_item.frame = 'none';
      tsfd_item.finish = 'dark_bronze';
      tsfd_item.finish_other = '';
      tsfd_item.hanging_hardware = 'top_bottom_interior_pivots';
      tsfd_item.threshold = '4_inches';
      tsfd_item.closer = 'std_surface';
      tsfd_item.lock_panic = 'thumb_turn';
      tsfd_item.push_pull = 'ph20_btb';
      tsfd_item.mid_panel = 'd_21';
      tsfd_item.bottom_rail = 'd_71_d_19';

      tsfd_items.push(tsfd_item);
   }
   // console.log('tsfd_load_sample_data:tsfd_items => ' + JSON.stringify(tsfd_items));
   return tsfd_items;
}

function tsfd_init(tsfd_seed, tsfd_seed_items) {
   tsfd_current_item = -1;
   tsfd_items = [];
   tsfd_init_form_state();
   if (tsfd_seed && tsfd_seed_items.length > 0) {
      tsfd_current_item = 0;
      tsfd_items = tsfd_seed_items;
      tsfd_init_toolbar_navigation_buttons_state();
      tsfd_init_toolbar_editing_buttons_state();
      tsfd_show_current_item();
   }
   tsfd_set_toolbar_navigation_buttons_state();
   tsfd_editing = false;
   tsfd_copying = false;
   tsfd_clipboard = {};
   tsfd_set_toolbar_editing_buttons_state();
}

jQuery(document).ready(function($) {
   $('input:radio[name=radio_tsfd_elevation_type]').change(function(event) {
      // event.preventDefault();
      // event.stopPropagation();
      var tsfd_elevation_type = $(this).val();
      tsfd_set_door_opening_state_by_elevation_type(tsfd_elevation_type);
   });
   $('#radio_tsfd_finish').change(function(event) {
      // event.preventDefault();
      // event.stopPropagation();
      var tsfd_finish = $(this).val();
      tsfd_set_finish_other_state_by_finish(tsfd_finish, true);
   });
   $('#btn_tsfd_add').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_editing = true;
      tsfd_clear_form();
      tsfd_set_toolbar_navigation_buttons_state();
      tsfd_set_toolbar_editing_buttons_state();
      // set focus to first textbox
   });
   $('#btn_tsfd_save').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      // validation happens here...
      var tsfd_item = tsfd_get_item_from_form();
      if (tsfd_editing) {
         tsfd_editing = false;
         tsfd_items.push(tsfd_item);
         if (tsfd_current_item == -1 || tsfd_items.length == 1) {
            tsfd_current_item = 0;
         }
         tsfd_show_current_item();
         tsfd_set_toolbar_navigation_buttons_state();
         tsfd_set_toolbar_editing_buttons_state();
      } else {
         tsfd_items[tsfd_current_item] = tsfd_item;
      }
      // set focus to first textbox
   });
   $('#btn_tsfd_delete').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      if (tsfd_editing) {
         tsfd_editing = false;
         if (!(tsfd_current_item == -1) && !(tsfd_items.length == 0)) {
            tsfd_show_current_item();
         }
         tsfd_set_toolbar_navigation_buttons_state();
         tsfd_set_toolbar_editing_buttons_state();
      } else {
         if (!confirm('Delete this record?')) return;
         tsfd_items.splice(tsfd_current_item, 1);
         if (tsfd_items.length == 0) {
            tsfd_current_item = -1;
         } else {
            if (tsfd_current_item == tsfd_items.length) {
               tsfd_current_item = tsfd_items.length - 1;
            }
         }
         if (!(tsfd_current_item == -1) && !(tsfd_items.length == 0)) {
            tsfd_show_current_item();
         } else {
            tsfd_clear_form();
         }
         tsfd_set_toolbar_navigation_buttons_state();
         tsfd_set_toolbar_editing_buttons_state();
      }
      // set focus to first textbox
   });
   $('#btn_tsfd_refresh').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_show_current_item();
   });
   $('#btn_tsfd_copy').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_copying = true;
      tsfd_clipboard = tsfd_get_item_from_form();
      tsfd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tsfd_paste').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_print_item_to_form(tsfd_clipboard);
      tsfd_clipboard = {};
      tsfd_copying = false;
      tsfd_set_toolbar_copy_paste_buttons_state();
   });
   $('#btn_tsfd_first').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_current_item = 0;
      tsfd_show_current_item();
      tsfd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_tsfd_previous').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_current_item--;
      tsfd_show_current_item();
      tsfd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_tsfd_next').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_current_item++;
      tsfd_show_current_item();
      tsfd_set_toolbar_navigation_buttons_state();
   });
   $('#btn_tsfd_last').click(function(event) {
      event.preventDefault();
      event.stopPropagation();
      tsfd_current_item = tsfd_items.length - 1;
      tsfd_show_current_item();
      tsfd_set_toolbar_navigation_buttons_state();
   });
});