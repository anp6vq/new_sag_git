var job_id;
var created_date;
var job_item = {};
var aws_s3_region = 'us-east-1';
var aws_s3_identitypoolid = 'us-east-1:5c951299-a194-4f30-8f24-5e82398c8105';
var aws_s3_bucket = 'sag-jobs-photos';
//
var fix_date_time;
var back_date_entry = false;
var dateNew ;
var fix_date ;



webshim.polyfill('forms');

function getParameterByName(name, url) {
   if (!url) url = window.location.href;
   name = name.replace(/[\[\]]/g, "\\$&");
   var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"), results = regex.exec(url);
   if (!results) return null;
   if (!results[2]) return '';
   return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function clear_form() {
   jQuery('#text_salesman_name').val('');
   jQuery('#text_owner_name').val('');
   jQuery('#text_quote_number').val('');
   jQuery('#text_tgpd_glass_secondryquote').val('');
   jQuery('#text_job_name').val('');
   jQuery('#text_csr_description').val('');
   jQuery('#text_onsite_contact').val('');
   jQuery('#text_contact_number').val('');
   jQuery('#text_contact_email').val('');
   jQuery('#text_person_seen').val('');
   jQuery('#text_csr').val('');
   jQuery('#text_project_address_line1').val('');
   jQuery('#text_project_address_line2').val('');
   jQuery('#text_project_email').val('');
   jQuery('#text_project_city').val('');
   jQuery('#select_project_state').val(['not_applicable']);
   jQuery('#text_project_postal_code').val('');
   jQuery('#text_project_parking').val('');
   /*sag-163*/
   jQuery('#text_project_parking_custom').val('');
   /*sag-163*/
   jQuery('#text_billing_address_line1').val('');
   jQuery('#text_billing_address_line2').val('');
   jQuery('#text_billing_email').val('');
   jQuery('#text_billing_city').val('');
   jQuery('#select_billing_state').val(['not_applicable']);
   jQuery('#text_billing_postal_code').val('');
   jQuery('#textarea_project_description').val('');
   jQuery('#select_project_status').val(['not_applicable']);
   jQuery('#select_labor_hours_type').val(['not_applicable']);
   jQuery('#text_labor_number_of_hours').val('');
   jQuery('#text_labor_number_of_men').val('');
   jQuery('#addresslocation').val('');
   jQuery('#billtoid').val('');
}

function print_item_to_form(job_item) {
	//alert(JSON.stringify(job_item));
	
	//change layout old job id by fixdate 
	//var fix_date =  new Date("2018-01-06 18:00:18");//YYYY-DD-MM H:i:s 2017-12-11
	//var dateNew = job_item.created_date.split(' ');
	//var created_date =  new Date(dateNew[0]);
	
	//fix_date =  new Date("2018-01-06");//YYYY-DD-MM H:i:s 2017-12-11	
	//var dateNew = job_item.created_date.split(' ');
	//var created_date =  new Date(dateNew[0]);
	console.log("fix_date >> " + fix_date);
	console.log("created_date >> " + created_date);
	


		console.log('job_item.salesman_name >> ' + job_item.salesman_name);
		/* if(job_item.salesman_name=='SLA'){
		    jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'steven@salbertglass.com']);
		 } else if(job_item.salesman_name=='CW'){

			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'cwilliams@salbertglass.com']);
		 }else if(job_item.salesman_name=='KL'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'klewis@salbertglass.com']);

		  }else if(job_item.salesman_name=='RS'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'rscott@salbertglass.com']);

		 }
		 else if(job_item.salesman_name=='GRB'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'gburkhart@salbertglass.com']);

		 }else if(job_item.salesman_name=='EA'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'edwin@salbertglass.com']);

		 }else if(job_item.salesman_name=='SD'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'steven@salbertglass.com']);

		 }else if(job_item.salesman_name=='CAB'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'cbaumes@salbertglass.com']);

		 }else if(job_item.salesman_name=='CAO'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'cosbrone@salbertglass.com']);

		 }else if(job_item.salesman_name=='MJA'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'malbert@salbertglass.com']);

		 }else if(job_item.salesman_name=='SG'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'sgreeenstreet@salbertglass.com']);

		 }else if(job_item.salesman_name=='DB'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'dblackwell@salbertglass.com']);

		 }else if(job_item.salesman_name=='KG'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'kgrant@salbertglass.com']);

		 }
		 else if(job_item.salesman_name=='ALEX'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'alex@imajine.us']);

		 }
		  else if(job_item.salesman_name=='AJ'){
			   jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'ajay@imajine.us']);

		 }
		 else{

			 jQuery('#text_salesman_name').val(['na#']);

		 }*/
		 if(job_item.salesman_name=='SLA'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'steven@salbertglass.com']);
 }else if(job_item.salesman_name=='CW'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'cwilliams@salbertglass.com']);
 }
  else if(job_item.salesman_name=='RS' || job_item.salesman_name=='rs'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'rscott@salbertglass.com']);
 }
 else if(job_item.salesman_name=='KL'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'klewis@salbertglass.com']);
 }else if(job_item.salesman_name=='GRB'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'gburkhart@salbertglass.com']);
 }else if(job_item.salesman_name=='EA'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'edwin@salbertglass.com']);
 }else if(job_item.salesman_name=='SD'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'appmanager@salbertglass.com']);
 }else if(job_item.salesman_name=='CAB'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'cbaumes@salbertglass.com']);
 }else if(job_item.salesman_name=='CAO'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'cosborne@salbertglass.com']);
 }else if(job_item.salesman_name=='MJA'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'malbert@salbertglass.com']);
 }else if(job_item.salesman_name=='SG'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'sgreenstreet@salbertglass.com']);
 }else if(job_item.salesman_name=='DB'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'dblackwell@salbertglass.com']);
 }else if(job_item.salesman_name=='KG'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'kgrant@salbertglass.com']);
 }
  else if(job_item.salesman_name=='AJ'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'ajay@imajine.us']);
 } else if(job_item.salesman_name=='ALEX'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'alex@imajine.us']);
 }else if(job_item.salesman_name=='SC'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'scoleman@salbertglass.com']);
 }
 else if(job_item.salesman_name=='EH'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'ehuber@salbertglass.com']);
 }
 else if(job_item.salesman_name=='SK'){
     jQuery('#text_salesman_name').val([job_item.salesman_name+'#'+'skurth@salbertglass.com']);
 }
 else{
 jQuery('#text_salesman_name').val(['#na']);
 }
		 /*owner*/
		 
		 
		 	console.log('job_item.owner_name >> ' + job_item.owner_name);
		  if(job_item.owner_name=='SLA' || job_item.owner_name=='test' || job_item.owner_name=='TG'){
		    jQuery('#text_owner_name').val([job_item.owner_name+'#'+'steven@salbertglass.com']);
		 }else if(job_item.owner_name=='CW'){

			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'cwilliams@salbertglass.com']);
		 }else if(job_item.owner_name=='KL'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'klewis@salbertglass.com']);

		 }else if(job_item.owner_name=='RS'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'rscott@salbertglass.com']);

		 }else if(job_item.owner_name=='GRB'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'gburkhart@salbertglass.com']);

		 }else if(job_item.owner_name=='EA'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'edwin@salbertglass.com']);

		 }else if(job_item.owner_name=='SD'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'appmanager@salbertglass.com']);

		 }else if(job_item.owner_name=='CAB'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'cbaumes@salbertglass.com']);

		 }else if(job_item.owner_name=='CAO'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'cosborne@salbertglass.com']);

		 }else if(job_item.owner_name=='MJA'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'malbert@salbertglass.com']);

		 }else if(job_item.owner_name=='SG'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'sgreeenstreet@salbertglass.com']);

		 }else if(job_item.owner_name=='DB'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'dblackwell@salbertglass.com']);

		 }else if(job_item.owner_name=='KG'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'kgrant@salbertglass.com']);

		 }
		 else if(job_item.owner_name=='AJ'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'ajay@imajine.us']);

		 } else if(job_item.owner_name=='SC'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'scoleman@salbertglass.com']);

		 } else if(job_item.owner_name=='EH'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'ehuber@salbertglass.com']);

		 }
		 else if(job_item.owner_name=='SK'){
			   jQuery('#text_owner_name').val([job_item.owner_name+'#'+'skurth@salbertglass.com']);

		 }
		 else{

			 jQuery('#text_owner_name').val([job_item.owner_name+'#alex@imajine.us']);

		 }


   jQuery('#text_quote_number').val(job_item.quote_number);
   jQuery('#text_tgpd_glass_secondryquote').val(job_item.secondary_quote);
   jQuery('#text_job_name').val(job_item.job_name);
   jQuery('#text_csr_description').val(job_item.csr_description);
   jQuery('#text_onsite_contact').val(job_item.onsite_contact);
   jQuery('#text_contact_number').val(job_item.contact_number);
   jQuery('#text_contact_email').val(job_item.contact_email);
   jQuery('#text_person_seen').val(job_item.person_seen);
   jQuery('#text_csr').val([job_item.csr]);




 sessionStorage.setItem("job_name", job_item.job_name);

   sessionStorage.setItem("CustomerId", job_item.CustomerId);

   sessionStorage.setItem("project_address_line1", job_item.project_address_line1);   

   sessionStorage.setItem("project_address_line2", job_item.project_address_line2);

   sessionStorage.setItem("project_email", job_item.project_email);

   sessionStorage.setItem("project_city", job_item.project_city);

   sessionStorage.setItem("project_state", job_item.project_state);

   sessionStorage.setItem("project_postal_code", job_item.project_postal_code);



   sessionStorage.setItem("BillToId", job_item.BillToId);	
   sessionStorage.setItem("billing_address_line1", job_item.billing_address_line1);

   sessionStorage.setItem("billing_address_line2", job_item.billing_address_line2);

   sessionStorage.setItem("billing_email", job_item.billing_email);

   sessionStorage.setItem("billing_city", job_item.billing_city);

   sessionStorage.setItem("billing_state", job_item.billing_state);
   sessionStorage.setItem("billing_postal_code", job_item.billing_postal_code);

	/*	if(job_item.csr=='cosborne'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'cosborne@salbertglass.com']);
		 }else if(job_item.csr=='dblackwell'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'dblackwell@salbertglass.com']);
		 }else if(job_item.csr=='kgrant'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'kgrant@salbertglass.com']);
		 }else if(job_item.csr=='edwin'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'edwin@salbertglass.com']);
		 }else if(job_item.csr=='gburkhart'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'gburkhart@salbertglass.com']);
		 }else if(job_item.csr=='sgreenstreet'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'sgreenstreet@salbertglass.com']);
		 }else if(job_item.csr=='steven'){

			 jQuery('#text_csr').val([job_item.csr+'#'+'steven@salbertglass.com']);
		 }
     else{
			 jQuery('#text_csr').val([job_item.csr+'#alex@imajine.us']);

		 }*/


		 //alert(sessionStorage.getItem("job_name"));
		 
		  if(job_item.csr=='cosborne'){
						 jQuery('#text_csr').val([job_item.csr+'#'+'cosborne@salbertglass.com']);
					 }else if(job_item.csr=='rs'){
						 jQuery('#text_csr').val([job_item.csr+'#'+'rscott@salbertglass.com']);
					 }else if(job_item.csr=='dblackwell'){
						 jQuery('#text_csr').val([job_item.csr+'#'+'dblackwell@salbertglass.com']);
					 }else if(job_item.csr=='kgrant'){
						 jQuery('#text_csr').val([job_item.csr+'#'+'kgrant@salbertglass.com']);
					 }else if(job_item.csr=='edwin'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'edwin@salbertglass.com']);
					 }else if(job_item.csr=='gburkhart'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'gburkhart@salbertglass.com']);
					 }else if(job_item.csr=='sgreenstreet'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'sgreenstreet@salbertglass.com']);
					 }else if((job_item.csr=='alex') || (job_item.csr=='alex@imajine.us')){
						 jQuery('#text_csr').val([job_item.csr+'#'+'alex@imajine.us']);
					}else if((job_item.csr=='ajay') || (job_item.csr=='ajay@imajine.us')){
						 jQuery('#text_csr').val([job_item.csr+'#'+'ajay@imajine.us']);
					}else if(job_item.csr=='cwilliams'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'cwilliams@salbertglass.com']);
					}else if(job_item.csr=='steven'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'steven@salbertglass.com']);
					}else if(job_item.csr=='scoleman'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'scoleman@salbertglass.com']);
					}else if(job_item.csr=='ehuber'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'ehuber@salbertglass.com']);
					} else if(job_item.csr=='skurth'){
						  jQuery('#text_csr').val([job_item.csr+'#'+'skurth@salbertglass.com']);
					} 
					else{
						 jQuery('#text_csr').val([job_item.csr+'#'+'steven@salbertglass.com']);
					 }

   jQuery('#text_project_address_line1').val(job_item.project_address_line1);
   jQuery('#text_project_address_line2').val(job_item.project_address_line2);
   jQuery('#text_project_email').val(job_item.project_email);
   jQuery('#text_project_city').val(job_item.project_city);
   jQuery('#select_project_state').val([job_item.project_state]);
   jQuery('#text_project_postal_code').val(job_item.project_postal_code);
   /*sag-163*/

    
   var avaialbel_park = ['Loading Dock','Parking Lot','Street','Reserved Space','Other'];
   
   if(jQuery.inArray(job_item.parking, avaialbel_park) != -1 && job_item.parking != '' && job_item.parking != null){
   		console.log('you are in' + jQuery.inArray(job_item.parking, avaialbel_park));
   	if(jQuery.inArray(job_item.parking, avaialbel_park) >= 0){
   		jQuery('#text_project_parking').val(job_item.parking);

   	} else {	
   		$('#text_project_postal_code_label').show();
      $('#text_project_parking_custom').show();

   	jQuery('#text_project_parking').val('Other');
   	jQuery('#text_project_parking_custom').val(job_item.parking);
   }
   } else {

   	jQuery('#text_project_parking').val(job_item.parking);
   }
     
    console.log('check_other_park'+job_item.parking_other);
    console.log('check_parking'+job_item.parking);
   if(job_item.parking_other != null && job_item.parking_other !=  ''&& job_item.parking == 'Other'){
   jQuery('#text_project_parking_custom').val(job_item.parking_other);
  	
   $('#text_project_postal_code_label').show();
      $('#text_project_parking_custom').show();
   }
   /*sag-163*/
   jQuery('#text_billing_address_line1').val(job_item.billing_address_line1);
   jQuery('#text_billing_address_line2').val(job_item.billing_address_line2);
   jQuery('#text_billing_email').val(job_item.billing_email);
   jQuery('#text_billing_city').val(job_item.billing_city);
   jQuery('#select_billing_state').val([job_item.billing_state]);
   jQuery('#text_billing_postal_code').val(job_item.billing_postal_code);
   jQuery('#textarea_project_description').val(job_item.project_description);
   jQuery('#select_project_status').val([job_item.project_status]);
    //comment to add multiple
 
   jQuery.each( job_item.labor_hours_type , function( i, val ) {
    if(i == 0){
        jQuery('#select_labor_hours_type').val(job_item.labor_hours_type[0]);
        jQuery('#text_labor_number_of_hours').val(job_item.labor_number_of_hours[0]);
        jQuery('#text_labor_number_of_men').val(job_item.labor_number_of_men[0]);
        jQuery('#text_labur_notes').val(job_item.labor_notes[0]);
    }else{

        var data = jQuery("#c_remove").html();
        jQuery("#labur_copy_row").append('<div id="add'+i+'" class=" labur_row"><div class="col-sm-1"><button id="btn_new_labor_remove" onclick="remove_labur(this)" type="button" class="btn btn-default btn_new_labor_remove" aria-label="Left Align" title="sub"><span  class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></div><div class="remove_next">'+data+'</div></div>');
        jQuery('#add'+i).find("#select_labor_hours_type").val(job_item.labor_hours_type[i]);
        jQuery('#add'+i).find("#text_labor_number_of_hours").val(job_item.labor_number_of_hours[i]);
        jQuery('#add'+i).find("#text_labor_number_of_men").val(job_item.labor_number_of_men[i]);
        jQuery('#add'+i).find("#text_labur_notes").val(job_item.labor_notes[i]);
        jQuery('#add'+i).find("#s2id_select_labor_hours_type").remove();
        jQuery('#add'+i).find("#select_labor_hours_type").removeClass('select2-offscreen');
        
    }

  });

   



   jQuery('#addresslocation_data').val(job_item.CustomerId);
   jQuery('#billtoid_data').val(job_item.BillToId);
  // alert(job_item.whyreason);
   jQuery('#addwhyreason').val(job_item.whyreason);
   jQuery('#notapplicat').val(job_item.whyreason);
}
function hide_html_element_for_old_job_view(){
	
	jQuery('.shape-container-day, .shape-container-go').addClass('old-view');
	jQuery('.shape-container-day, .shape-container-go').show();
	jQuery('.custom_checkbox_add').show();
	//hide label
	var myArray = ['label[for="verify_tgpd_exterior_glass_type"]','label[for="verify_tgpd_treatment_exterior_glass"]', 
	'label[for="verify_tgpd_exterior_color_glass_type"]', 
	'label[for="verify_tgpd_coating_exterior_type"]',
	'label[for="text_tgpd_coating_first_position"]',
	'label[for="verify_tgpd_treatment_interior_glass"]',
	'label[for="verify_tgpd_interior_glass_type"]',
	'label[for="verify_tgpd_interior_color_glass_type"]',
	'label[for="verify_tgpd_coating_interior_type"]',
	'label[for="text_tgpd_coating_second_position"]',
	'#select_tgpd_treatment_exterior_glass',
	'#grids_and_fabrication', '#lift_inside_with_glass_type','#lift_outside_with_glass_type'];
	//alert('call A');
	 jQuery(myArray).each(function(index, element) {
        jQuery(element).show();
    });
	
	
	//jQuery('.custom_addon').removeClass('input-group');
	//jQuery('.custom_checkbox_move, .leftalign').removeClass('group_option');
	jQuery('#treatment_without_glass_type').show();
	jQuery('#glass_color_with_glass_type').show();
	jQuery('#lift_without_glass_type').show();
	jQuery('#div_select_tgpd_size_verification_status').hide();
	jQuery('#litethick_see').show();
	jQuery("#fieldset_tgpd_glass_type").show();
	jQuery('#sapacer_color_glass_type').insertBefore(jQuery('#div_select_tgpd_size_verification_status'));

	
}
//function back_date_pass(back_date_entry){ return back_date_entry;}

function get_item_from_form() {
   var salesman_name = jQuery('#text_salesman_name').val();
   var owner_name = jQuery('#text_owner_name').val();
   var quote_number = jQuery('#text_quote_number').val();
   var secondary_quote = jQuery('#text_tgpd_glass_secondryquote').val();
   var job_name = jQuery('#text_job_name').val();
   var csr_description = jQuery('#text_csr_description').val();
   var onsite_contact = jQuery('#text_onsite_contact').val();
   var contact_number = jQuery('#text_contact_number').val();
   var contact_email = jQuery('#text_contact_email').val();
   var person_seen = jQuery('#text_person_seen').val();
   var csr = jQuery('#text_csr').val();
   var project_address_line1 = jQuery('#text_project_address_line1').val();
   var project_address_line2 = jQuery('#text_project_address_line2').val();
   var project_email = jQuery('#text_project_email').val();
   var project_city = jQuery('#text_project_city').val();
   var project_state = jQuery('#select_project_state').val();
   var project_postal_code = jQuery('#text_project_postal_code').val();
   var parking= jQuery('#text_project_parking').val();
   /*sag-163*/
   var parking_other = jQuery('#text_project_parking_custom').val();

  // alert(parking_other);
   /*sag-163*/
   var whyreason=jQuery('#addwhyreason').val();


   var billing_address_line1 = jQuery('#text_billing_address_line1').val();
   var billing_address_line2 = jQuery('#text_billing_address_line2').val();
   var billing_email = jQuery('#text_billing_email').val();
   var billing_city = jQuery('#text_billing_city').val();
   var billing_state = jQuery('#select_billing_state').val();
   var billing_postal_code = jQuery('#text_billing_postal_code').val();
   var project_description = jQuery('#textarea_project_description').val();
   var project_status = jQuery('#select_project_status').val();
   var labor_hours_type = jQuery('#select_labor_hours_type').val();
   var labor_number_of_hours = jQuery('#text_labor_number_of_hours').val();
   var labor_number_of_men = jQuery('#text_labor_number_of_men').val();

   var textarea_tgpd_notes = jQuery('#textarea_tgpd_notes').val();

   var  hiddensett= jQuery("#seettt").val();
   console.log("****** HIDDEN SETTTT is " + hiddensett );
   var  addresslocation = jQuery('#addresslocation_data').val();
   var  billtoid = jQuery('#billtoid_data').val();
   
   //get form date of job multiple labur
   // var labur = jQuery("#c_remove").serializeArray();
   var labur = jQuery("#labur_copy_row :input").serializeArray();
   var labor_hours_type =[];
    var labor_number_of_men = [];
    var labor_number_of_hours = [];
    var labur_notes =[];
    console.log("labur");
    console.log(labur);
    $.each( labur, function( i, l_val ) {
        console.log(l_val.value);
       
            if(i%4 == 0)
                labor_hours_type.push(l_val.value);
            else if(i%4 == 1)
                labor_number_of_men.push(l_val.value);
            else if(i%4 == 2)
                labor_number_of_hours.push(l_val.value);
            else if(i%4 == 3)
                labur_notes.push(l_val.value);
       
    });
  console.log(labor_hours_type);
  console.log(labor_number_of_men);
  console.log(labor_number_of_hours);
  console.log(labur_notes);
  //get form date of job multiple labur end

   var job_item = {};
   job_item.id = job_id;
   job_item.salesman_name = salesman_name;
   job_item.owner_name = owner_name;
   job_item.quote_number = quote_number;
   job_item.secondary_quote = secondary_quote;
   job_item.csr_description = csr_description;
   job_item.job_name = job_name;
   job_item.onsite_contact = onsite_contact;
   job_item.contact_number = contact_number;
   job_item.contact_email = contact_email;
   job_item.person_seen = person_seen;
   job_item.csr = csr;
   job_item.project_address_line1 = project_address_line1;
   job_item.project_address_line2 = project_address_line2;
   job_item.project_email = project_email;
   job_item.project_city = project_city;
   job_item.project_state = project_state;
   job_item.project_postal_code = project_postal_code;
   job_item.billing_address_line1 = billing_address_line1;
   job_item.billing_address_line2 = billing_address_line2;
   job_item.billing_email = billing_email;
   job_item.billing_city = billing_city;
   job_item.billing_state = billing_state;
   job_item.billing_postal_code = billing_postal_code;
   job_item.project_description = project_description;
   job_item.project_status = project_status;
   job_item.labor_hours_type = labor_hours_type;
   job_item.labor_number_of_hours = labor_number_of_hours;
   job_item.labor_number_of_men = labor_number_of_men;
   //add new line for notes
   job_item.labur_notes   = labur_notes;
   job_item.textarea_tgpd_notes = textarea_tgpd_notes;
   job_item.created_date = created_date;
   job_item.hiddensett=hiddensett;
   job_item.addresslocation = addresslocation;
   job_item.billtoid = billtoid;
   job_item.whyreason=whyreason;
    job_item.parking=parking;
    /*sag-163*/
    job_item.parking_other=parking_other;
    /*sag-163*/
  // alert(JSON.stringify(job_item));

  // print_item_to_console('get_item_from_form', job_item);
   return job_item
}

function print_item_to_console(debug_tag, job_item) {
	
	return false;
   var log_tag = (debug_tag.length == 0 ? '' : (debug_tag + ': '));
   console.log(log_tag + 'job_id => ' + job_item.job_id);
   console.log(log_tag + 'salesman_name => ' + job_item.salesman_name);
   console.log(log_tag + 'quote_number => ' + job_item.quote_number);
   console.log(log_tag + 'job_name => ' + job_item.job_name);
   console.log(log_tag + 'onsite_contact => ' + job_item.onsite_contact);
   console.log(log_tag + 'contact_number => ' + job_item.contact_number);
   console.log(log_tag + 'contact_email => ' + job_item.contact_email);
   console.log(log_tag + 'person_seen => ' + job_item.person_seen);
   console.log(log_tag + 'csr => ' + job_item.csr);
   console.log(log_tag + 'project_address_line1 => ' + job_item.project_address_line1);
   console.log(log_tag + 'project_address_line2 => ' + job_item.project_address_line2);
   console.log(log_tag + 'project_email => ' + job_item.project_email);
   console.log(log_tag + 'project_city => ' + job_item.project_city);
   console.log(log_tag + 'project_state => ' + job_item.project_state);
   console.log(log_tag + 'project_postal_code => ' + job_item.project_postal_code);
   console.log(log_tag + 'billing_address_line1 => ' + job_item.billing_address_line1);
   console.log(log_tag + 'billing_address_line2 => ' + job_item.billing_address_line2);
   console.log(log_tag + 'billing_email => ' + job_item.billing_email);
   console.log(log_tag + 'billing_city => ' + job_item.billing_city);
   console.log(log_tag + 'billing_state => ' + job_item.billing_state);
   console.log(log_tag + 'billing_postal_code => ' + job_item.billing_postal_code);
   console.log(log_tag + 'project_description => ' + job_item.project_description);
   console.log(log_tag + 'project_status => ' + job_item.project_status);
   console.log(log_tag + 'labor_hours_type => ' + job_item.labor_hours_type);
   console.log(log_tag + 'labor_number_of_hours => ' + job_item.labor_number_of_hours);
   console.log(log_tag + 'labor_number_of_men => ' + job_item.labor_number_of_men);
   console.log(log_tag + 'created_date => ' + job_item.created_date);
  console.log(log_tag + 'Location Name => ' + job_item.addresslocation)
  console.log(log_tag + 'Bill to ID Name => ' + job_item.billtoid)
   // console.log(log_tag + 'job_item => ' + JSON.stringify(job_item));\

}

function validateForm() {
  //  var text_salesman_name = jQuery('#text_salesman_name')[0];
  //  if (!text_salesman_name.checkValidity()) {
  //     text_salesman_name.setCustomValidity('Please enter sales man name.');
  //     return false;
  //  } else {
  //     text_salesman_name.setCustomValidity('');
  //  }
  //  var text_quote_number = jQuery('#text_quote_number')[0];
  //  if (!text_quote_number.checkValidity()) {
  //     text_quote_number.setCustomValidity('Please enter quote number.');
  //     return false;
  //  } else {
  //     text_quote_number.setCustomValidity('');
  //  }
  //  console.log("made it past quote number");
  //  var text_job_name = jQuery('#text_job_name')[0];
  //  if (!text_job_name.checkValidity()) {
  //     text_job_name.setCustomValidity('Please enter job name.');
  //     return false;
  //  } else {
  //     text_job_name.setCustomValidity('');
  //  }
  //  var text_onsite_contact = jQuery('#text_onsite_contact')[0];
  //  if (!text_onsite_contact.checkValidity()) {
  //     text_onsite_contact.setCustomValidity('Please enter on site contact.');
  //     return false;
  //  } else {
  //     text_onsite_contact.setCustomValidity('');
  //  }
  //  console.log("made it past onstie contact");
  //  var text_contact_number = jQuery('#text_contact_number')[0];
  //  if (!text_contact_number.checkValidity()) {
  //     text_contact_number.setCustomValidity('Please enter contact number.');
  //     return false;
  //  } else {
  //     text_contact_number.setCustomValidity('');
  //  }
  //  var text_contact_email = jQuery('#text_contact_email')[0];
  //  if (!text_contact_email.checkValidity()) {
  //     text_contact_email.setCustomValidity('Please enter contact email.');
  //     return false;
  //  } else {
  //     text_contact_email.setCustomValidity('');
  //  }
  //  console.log("made it past contact email");
  //  var text_person_seen = jQuery('#text_person_seen')[0];
  //  if (!text_person_seen.checkValidity()) {
  //     text_person_seen.setCustomValidity('Please enter person seen.');
  //     return false;
  //  } else {
  //     text_person_seen.setCustomValidity('');
  //  }
  //  var text_csr = jQuery('#text_csr')[0];
  //  if (!text_csr.checkValidity()) {
  //     text_csr.setCustomValidity('Please enter csr.');
  //     return false;
  //  } else {
  //     text_csr.setCustomValidity('');
  //  }
  //  console.log("made it past csr");
  //  var text_project_address_line1 = jQuery('#text_project_address_line1')[0];
  //  if (!text_project_address_line1.checkValidity()) {
  //     text_project_address_line1.setCustomValidity('Please enter project address.');
  //     return false;
  //  } else {
  //     text_project_address_line1.setCustomValidity('');
  //  }
  //  console.log("made it past project address line1");
  //  var text_project_email = jQuery('#text_project_email')[0];
  //  if (!text_project_email.checkValidity()) {
  //     text_project_email.setCustomValidity('Please enter project email.');
  //     return false;
  //  } else {
  //     text_project_email.setCustomValidity('');
  //  }
  //  var text_project_city = jQuery('#text_project_city')[0];
  //  if (!text_project_city.checkValidity()) {
  //     text_project_city.setCustomValidity('Please enter project city.');
  //     return false;
  //  } else {
  //     text_project_city.setCustomValidity('');
  //  }
   //
  //  var addresslocation_data = jQuery('#addresslocation_data')[0];
  //  if (!addresslocation_data.checkValidity()) {
  //     addresslocation_data.setCustomValidity('Please enter Location Name.');
  //     return false;
  //  } else {
  //     addresslocation_data.setCustomValidity('');
  //  }
   //
   //
  //  var project_state = jQuery('#select_project_state').val();
  //  if (typeof project_state === 'undefined' || project_state == null) project_state = 'not_applicable';
  //  if (project_state.localeCompare('not_applicable') == 0) {
  //     jQuery('#select_project_state')[0].focus();
  //     return false;
  //  }
  //  var text_project_postal_code = jQuery('#text_project_postal_code')[0];
  //  if (!text_project_postal_code.checkValidity()) {
  //     text_project_postal_code.setCustomValidity('Please enter project zip code.');
  //     return false;
  //  } else {
  //     text_project_postal_code.setCustomValidity('');
  //  }
  //  var text_billing_address_line1 = jQuery('#text_billing_address_line1')[0];
  //  if (!text_billing_address_line1.checkValidity()) {
  //     text_billing_address_line1.setCustomValidity('Please enter billing address.');
  //     return false;
  //  } else {
  //     text_billing_address_line1.setCustomValidity('');
  //  }
  //  var text_billing_email = jQuery('#text_billing_email')[0];
  //  if (!text_billing_email.checkValidity()) {
  //     text_billing_email.setCustomValidity('Please enter billing email.');
  //     return false;
  //  } else {
  //     text_billing_email.setCustomValidity('');
  //  }
  //  var text_billing_city = jQuery('#text_billing_city')[0];
  //  if (!text_billing_city.checkValidity()) {
  //     text_billing_city.setCustomValidity('Please enter billing city.');
  //     return false;
  //  } else {
  //     text_billing_city.setCustomValidity('');
  //  }
   //
  //  var billtoid_data = jQuery('#billtoid_data')[0];
  //  if (!billtoid_data.checkValidity()) {
  //     billtoid_data.setCustomValidity('Please enter Name.');
  //     return false;
  //  } else {
  //     billtoid_data.setCustomValidity('');
  //  }
   //
  //  console.log("made it past billtoId");
   //
   //
  //  var billing_state = jQuery('#select_billing_state').val();
  //  if (typeof billing_state === 'undefined' || billing_state == null) billing_state = 'not_applicable';
  //  if (billing_state.localeCompare('not_applicable') == 0) {
  //     jQuery('#select_billing_state')[0].focus();
  //     return false;
  //  }
   //
   //
  //  var text_billing_postal_code = jQuery('#text_billing_postal_code')[0];
  //  if (!text_billing_postal_code.checkValidity()) {
  //     text_billing_postal_code.setCustomValidity('Please enter billing zip code.');
  //     return false;
  //  } else {
  //     text_billing_postal_code.setCustomValidity('');
  //  }
  //  var project_status = jQuery('#select_project_status').val();
  //  if (typeof project_status === 'undefined' || project_status == null) project_status = 'not_applicable';
  //  if (project_status.localeCompare('not_applicable') == 0) {
  //     jQuery('#select_project_status')[0].focus();
  //     return false;
  //  }
   //
  //  var text_billing_city = jQuery('#text_billing_city')[0];
  //  if (!text_billing_city.checkValidity()) {
  //     text_billing_city.setCustomValidity('Please enter billing city.');
  //     return false;
  //  }



   return true;
}

function do_same_addresses() {
   jQuery('#text_project_address_line1').val(jQuery('#text_billing_address_line1').val());
   jQuery('#text_project_address_line2').val(jQuery('#text_billing_address_line2').val());
   jQuery('#text_project_email').val(jQuery('#text_billing_email').val());
   jQuery('#text_project_city').val(jQuery('#text_billing_city').val());
   jQuery('#select_project_state').val([jQuery('#select_billing_state').val()]);
   jQuery('#text_project_postal_code').val(jQuery('#text_billing_postal_code').val());
   jQuery('#addresslocation_data').val(jQuery('#billtoid_data').val())
   ;
}

function notes_tab_get_deta(){ }

function save_job() {


	console.log('save_button_clcked');
	
	console.log('tgpd_editing',tgpd_editing);

	console.log('tgpd_editingsave',tgpd_editingsave);
	
	if(tgpd_editing == true || tgpd_editingsave==true)
		{
			//alert("opiuopuiop");
			if(saveclickserachtech() !=true)
			{
				//alert("Please check glass pieces entries.");
				//return false;	
			}
			
		}
		//alert(repair_editing);
		
		if(repair_editing == true || repair_editingsave==true)
		{
			//alert("opiuopuiop");
			if(save_clickrepair() !=true)
			{
				//alert("Please check Repair Door entries.");
				//return false;	
			}
			
		}
		
	
	
	//alert(validateForm());
   if (!validateForm()) {
     console.log("not a valid form!!!");
      return false;
   }
   
 
   console.log("form was validdddd!!!");
   var subButton = jQuery('.button_submit_jobsearch');
   subButton.prop('disabled', true);
   subButton.val('Saving New Job...');
   job_item            = get_item_from_form();
   job_item.gpd_items  = tgpd_items;
   job_item.agd_items  = tagd_items;
   job_item.nwdr_items = newdoor_items;

   console.log('job_item.nwdr_items',newdoor_items);
   //job_item.sdbg_items = sdbg_item;
   job_item.hiddensett = jQuery("#seettt").val();
  
  // alert(jQuery("#seettt").val());
	//console.log('job_item',sdbg_item);
   //console.log('before-2',job_item);
   var data = {
      'action': 'add_job_item',
      'data': job_item
   };

   
   console.log("save_jobsdfdsf: data => " + JSON.stringify(data));
   var jqXHR = jQuery.post(ajax_object.ajax_url, data);
   jqXHR.done(function(data, textStatus, jqXHR) {
      console.log("save_job:jqXHR.done: data => " + data + ", textStatus => " + textStatus + ", responseText => " + jqXHR.responseText + ", responseXML => " + jqXHR.responseXML + ", status => " + jqXHR.status + ", statusText => " + jqXHR.statusText + ", statusCode() => " + jqXHR.statusCode());
      if (job_id > 0) {
        subButton.prop('disabled', false);
         var data_js = JSON.parse(data);
		 setTimeout(function(){ document.location.replace(data_js.redirect_url); }, 3000);
         	
      } else {
         clear_form();
         job_item = {};
         tgpd_clear_form();
         tgpd_init(false);
         tagd_clear_form();
         tagd_init(false);
         subButton.val('Submit New Job');
         subButton.prop('disabled', false);
       //  jQuery('#text_salesman_name')[0].focus();
      }
   });
   jqXHR.fail(function(jqXHR, textStatus, errorThrown) {
      console.log("save_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      alert("save_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      subButton.val('Submit New Job');
      subButton.prop('disabled', false);
   });
}

function fetch_job(job_id) {



   var subButton = jQuery('.button_submit_jobsearch');
   subButton.prop('disabled', true);
   subButton.val('Fetching Job...');
   var data = {
      'action': 'get_job_item',
      'job_id': job_id
   };
   var jqXHR = jQuery.post(ajax_object.ajax_url, data);
   jqXHR.done(function(data, textStatus, jqXHR) {
       
        	jQuery('#glass_piece_holes_lbl').hide();
       
       	jQuery('#text_tgpd_fabrication_polished_edges').parents('.col-xs-11').addClass('col-xs-12');
			jQuery('#text_tgpd_fabrication_polished_edges').parents('.col-xs-11').removeClass('col-xs-offset-1');
			jQuery('#text_tgpd_fabrication_polished_edges').parents('.col-xs-12').removeClass('col-xs-11');
       
       
			jQuery('#text_tgpd_fabrication_holes').parents('.col-xs-11').addClass('col-xs-12');
			jQuery('#text_tgpd_fabrication_holes').parents('.col-xs-11').removeClass('col-xs-offset-1');
			jQuery('#text_tgpd_fabrication_holes').parents('.col-xs-12').removeClass('col-xs-11');

			
			jQuery('#text_tgpd_fabrication_pattern').parents('.col-xs-11').addClass('col-xs-12');
			jQuery('#text_tgpd_fabrication_pattern').parents('.col-xs-11').removeClass('col-xs-offset-1');
			jQuery('#text_tgpd_fabrication_pattern').parents('.col-xs-12').removeClass('col-xs-11');
       
       
      console.log("fetch_job: data => " + data + ", textStatus => " + textStatus + ", responseText => " + jqXHR.responseText + ", responseXML => " + jqXHR.responseXML + ", status => " + jqXHR.status + ", statusText => " + jqXHR.statusText + ", statusCode() => " + jqXHR.statusCode());
      var job_items = JSON.parse(data);
      job_item = job_items[0];
      
      console.log(job_item);
	  
dateNew = job_item.created_date.split(' ');
created_date =  dateNew[0];

fix_date =  new Date("2018-01-06");
//var today = new Date();
var dd = fix_date.getDate();
var mm = fix_date.getMonth()+1;//January is 0!`

var yyyy = fix_date.getFullYear();
if(dd<10){dd='0'+dd}
if(mm<10){mm='0'+mm}
 fix_date_time = yyyy+"-"+mm+"-"+dd; //2017-12-19 21:00:30 
//console.log("today created_date " + created_date +" fix_date_time >> " +fix_date_time);

if(created_date < fix_date_time ){
	back_date_entry=true;

}


/**/
var todayInfo = new Date();
		var todayDate = todayInfo.getDate();
		var currentMonth = todayInfo.getMonth()+1; //January is 0!
		var currentYear = todayInfo.getFullYear();

		if(todayDate<10) {
		todayDate = '0'+todayDate;
		} 

		if(currentMonth<10) {
		currentMonth = '0'+currentMonth;
		} 

		var today = currentMonth + '-' + todayDate + '-' + currentYear;
		//document.write(today);
		var fixdateForHole =  new Date("2018-03-13");
		
	//	fix_date =  new Date("2018-01-06");
//var today = new Date();
var fixDayForHole = fixdateForHole.getDate();
var fixMonthForHole = fixdateForHole.getMonth()+1;//January is 0!`

var fixYearForHole = fixdateForHole.getFullYear();
if(fixDayForHole<10){fixDayForHole='0'+fixDayForHole}
if(fixMonthForHole<10){fixMonthForHole='0'+fixMonthForHole}
 var fix_date_time_for_hole = fixYearForHole+"-"+fixMonthForHole+"-"+fixDayForHole;
 //alert("created_date >> "+ created_date+ " << >> fix_date_time_for_hole "+fix_date_time_for_hole);
if(created_date < fix_date_time_for_hole){ //alert('tester');
	jQuery('.holesold').show();
			jQuery('#glass_piece_holes_div').remove();	
		jQuery('#glass_piece_holes_lbl').hide();
	
}else{
	jQuery('.holesold').remove();
		jQuery('#glass_piece_holes_div').show();
		jQuery('#glass_piece_holes_lbl').show();
	
}
	//	alert(' today >> '+ today +' >> fix_date '+ fix_date);
		//if(today >fix_date){
	/*	if('13032018' >'12032018'){
		jQuery('.holesold.').hide();
		jQuery('#glass_piece_holes_div').show();
		}else{
				jQuery('.holesold.').show();
			jQuery('#glass_piece_holes_div').hide();	
		}
		//var dateNew = job_item.created_date.split(' ');
			//alert(dateNew);*/
/**/

console.log("test  "+ back_date_entry);

      //return false;
		jQuery("ul").find("li").removeClass("active");
		jQuery("#glass-piece-details").removeClass("active");
		jQuery("#door-repairs").removeClass("active");
		jQuery("#door-new").removeClass("active");
		jQuery("#door-note").removeClass("active");
		
		jQuery("#door-repairsset").removeClass("active");
		jQuery("#door-newset").removeClass("active");
		jQuery("#door-noteset").removeClass("active");
		jQuery("#glass-piece-detailsset").removeClass("active");

      tgpd_init(true, job_item["gpd_items"]);
	  // tgpd_editing=true;
	   
      tagd_init(true, job_item["agd_items"]);

	  console.log(JSON.stringify(job_item["nwdr_items"]));
	 



      newdoor_init(true, job_item["nwdr_items"]);
      notes_init(job_item["notes_items"][0]);

      created_date = job_item["created_date"];
      subButton.val('Submit New Job');
      subButton.prop('disabled', false);
      //jQuery('#text_salesman_name')[0].focus();
      //add disable cide
      jQuery(".upload_picture_and_generic").show();
      
      	 if(job_item["nwdr_items"].length>0){
        console.log('sag169');
        jQuery('#door-new').find('div.fields-right-half').find('select').show();
        }else{
        console.log('sag16911');   
          //jQuery('#door-new .col-sm-6:nth-child(2) select').show();
          jQuery('#door-new').find('div.fields-right-half').find('select').show();
          $('.select-new-door-category_type1').hide()
          $('.select-new-door-category_type2').hide()
          $('.select-new-door-category_type3').hide()
          jQuery('.upload_picture_and_generic').hide();
          jQuery('#gdr-single_door-container').hide();
          
        }
    	 
        //alert(categoryval);
	   if(job_item["agd_items"].length>0)
	  {
		  	jQuery("#glass-piece-details").removeClass("active");
		jQuery("#door-repairs").removeClass("active");
		jQuery("#door-new").removeClass("active");
		jQuery("#door-note").removeClass("active");
		  
		  		jQuery("#door-repairsset").removeClass("active");
		jQuery("#door-newset").removeClass("active");
		jQuery("#door-noteset").removeClass("active");
		jQuery("#glass-piece-detailsset").removeClass("active");
		
		 // alert("case1");
		 	var categoryval=job_item["agd_items"][0]['category'];
			if(categoryval=="gdr")
		{

			if(job_item["agd_items"][0]['subtype'] == 'single_right'){

				var up_subtype = 'single-opp';
			} else {

				var up_subtype = job_item["agd_items"][0]['subtype'];
			}

			var type=job_item["agd_items"][0]['type']+"-"+up_subtype;
		}
		else
		{
			var type=job_item["agd_items"][0]['type'];
		}
			//alert('type '+type);
			jQuery("#door-repairsset").addClass("active");
			jQuery("#door-repairs").removeClass("fade");
			jQuery("#door-repairs").addClass("active");
			jQuery('#select-repair-door-category').val(categoryval).trigger('change');
			


			jQuery('#select-repair-door-type').val(type).trigger('change');;

	  }
	   else if(job_item["nwdr_items"].length>0)
	  {
		  	jQuery("#glass-piece-details").removeClass("active");
		jQuery("#door-repairs").removeClass("active");
		jQuery("#door-new").removeClass("active");
		jQuery("#door-note").removeClass("active");
		  
		  		jQuery("#door-repairsset").removeClass("active");
		jQuery("#door-newset").removeClass("active");
		jQuery("#door-noteset").removeClass("active");
		jQuery("#glass-piece-detailsset").removeClass("active");
		  	var categoryval=job_item["nwdr_items"][0]['category'];
			var typesfdsf=job_item["nwdr_items"][0]['type'];
		  //	 alert(typesfdsf);
			jQuery("#door-newset").addClass("active");
		jQuery("#door-new").removeClass("fade");
		jQuery("#door-new").addClass("active");
		//jQuery('.select-new-door-category_type1').val(typesfdsf);
			console.log("categoryval12",categoryval);
    console.log('typesfdsf', typesfdsf);
        //alert(categoryval)
		jQuery('#select-new-door-category').val(categoryval).trigger('change');
	
	//	alert(typesfdsf);
	console.log("categoryval12",categoryval);
    console.log('typesfdsf', typesfdsf);
    	if(categoryval=="Gdoor")
    	{
    	    
    		jQuery('.select-new-door-category_type1').val(typesfdsf);
    		
    		
    	}
    	else if(categoryval=="Aludoor")
    	{
    		jQuery('.select-new-door-category_type2').val(typesfdsf);
    	   
    	}
    	else if(categoryval=="hmdoor")
    	{
    		jQuery('.select-new-door-category_type3').val(typesfdsf);
    	    
    	}

    	else if(categoryval=="patio")
    	{
    		jQuery('.select-new-door-category_type4').val(typesfdsf);
    	    
    	}
		
		
			if(typesfdsf=="Agndsing")
			{
				jQuery('#gdr-single_door-container').show();
			}
			else if(typesfdsf=="Single Door w/ Sidelite Left")
			{
				jQuery('#gdr-single_door_sidelite_left-container').show();
			}
			else if(typesfdsf=="Single Door w/ Sidelite Right")
			{
				jQuery('#gdr-single_door_sidelite_right-container').show();
			}
			else if(typesfdsf=="Single Door w/ Sidelite Left & Right")
			{
				jQuery('#gdr-single_door_sidelite_left_right-container').show();
			}
			else if(typesfdsf=="Agndpar")
			{
				jQuery('#gdr-pair_doors-container').show();
			}
			else if(typesfdsf=="Pair Doors w/ Sidelite Left")
			{
				jQuery('#gdr-pair_doors_sidelite_left-container').show();
			}
			else if(typesfdsf=="Pair Doors w/ Sidelite Right")
			{
				jQuery('#gdr-pair_doors_sidelite_right-container').show();
			}
			else if(typesfdsf=="Pair Doors w/ Sidelite Left & Right")
			{
				jQuery('#gdr-pair_doors_sidelite_left_right-container').show();
			}
			
			else if(typesfdsf=="Aludnfsig")
			{
				jQuery('#gdr-Aluminum_doors_and_frame_with_transom_single-container').show();
			}
			
			else if(typesfdsf=="Aludnfpar")
			{
				jQuery('#gdr-Aluminum_doors_and_frame_with_transom_pair-container').show();
			}
			
			else if(typesfdsf=="Alufsig")
			{
					//alert("sdfsdfds");
				jQuery('#gdr-Aluminum_doors_and_frame_single-container').show();
			}
			else if(typesfdsf=="Alufpar")
			{
				jQuery('#gdr-Aluminum_doors_and_frame_pair-container').show();
			}
			else if(typesfdsf=="Aluosig")
			{
				jQuery('#gdr-Aluminum_doors_door_only_single-container').show();
			}
			else if(typesfdsf=="Aluopar")
			{
				jQuery('#gdr-Aluminum_doors_door_only_pair-container').show();
			}
			else if(typesfdsf=="Hmdndsig")
			{
				jQuery('.new_door_container_hellowmetar_dooor_single').show();
			}
			else if(typesfdsf=="hmdndpar")
			{
				jQuery('#gdr-door_only_pair-container').show();
			}
			else if(typesfdsf=="hmdnfsig")
			{
				jQuery('#gdr-door_and_frame_single-container').show();
			}
			else if(typesfdsf=="hmdnfpar")
			{
				jQuery('#gdr-door_and_frame_pair-container').show();
			}else if(typesfdsf=="hmdnfpar")
			{
				jQuery('#gdr-door_and_frame_pair-container').show();
			} else if(typesfdsf=="ox" || typesfdsf=="xo" || typesfdsf=="oxo_sr" || typesfdsf=="oxo_sl" || typesfdsf=="xoo" || typesfdsf=="oox" || typesfdsf=="oxxo" )
			{
				jQuery('#patio-'+typesfdsf+'-container').show();
			}

	  }
  else if(job_item["notes_items"].length>0)
	  {
		  jQuery("#glass-piece-details").removeClass("active");
		jQuery("#door-repairs").removeClass("active");
		jQuery("#door-new").removeClass("active");
		jQuery("#door-note").removeClass("active");
		  
		  		jQuery("#door-repairsset").removeClass("active");
		jQuery("#door-newset").removeClass("active");
		jQuery("#door-noteset").removeClass("active");
		jQuery("#glass-piece-detailsset").removeClass("active");
		  	jQuery("#door-noteset").addClass("active");
			jQuery("#door-note").removeClass("fade");
			jQuery("#door-note").addClass("active");


	  }
	  else
	  {
	      jQuery('.select-new-door-category_type2').css("display","none");
		jQuery('.select-new-door-category_type3').css("display","none");
		
		
		  jQuery("#glass-piece-details").removeClass("active");
		jQuery("#door-repairs").removeClass("active");
		jQuery("#door-new").removeClass("active");
		jQuery("#door-note").removeClass("active");
		  
		  		jQuery("#door-repairsset").removeClass("active");
		jQuery("#door-newset").removeClass("active");
		jQuery("#door-noteset").removeClass("active");
		jQuery("#glass-piece-detailsset").removeClass("active");
		
		jQuery("#glass-piece-detailsset").addClass("active");

			jQuery("#glass-piece-details").addClass("active");
			jQuery("#glass-piece-details").removeClass("fade");
			jQuery("#glass-piece-details").addClass("active");

	  }

		

	 jQuery(".upload_picture_and_generic").show();
	 
	 
	  $('#door-repairs select.form-control').each(function(){
	//alert(this.value);
	  if(this.value == 'not_applicable'){
		$(this).css('background','#ffffff');
		$(this).css('color','#555555');
	  }else{
		$(this).css('background','#5cb85c');
		$(this).css('color','#ffffff');
	  }
	});
	  
	  
	$('#door-new select.form-control').each(function(){
			  if(this.value == 'not_applicable'){
				$(this).css('background','#ffffff');
				$(this).css('color','#555555');
			  }else{
				$(this).css('background','#5cb85c');
				$(this).css('color','#ffffff');
			  }
			});
					   
			print_item_to_form(job_item);	
			console.log('Back date Job-id > > ' + created_date + " < > "+ fix_date );
	if(created_date < fix_date_time ){
		
		//console.log('date');
		console.log('Back date Job-id');
	//	back_date_entry = true';
	
		hide_html_element_for_old_job_view();
		//change elment on click new form
		jQuery('#btn_tgpd_add').click(function(){
			
			hide_html_element_for_old_job_view();
		});
		//**********
		//if(created_date !=null ){
			
	//	}
		
		jQuery('#extra_type_coating_treatment').hide();
jQuery('#extra_type_coating_treatment_second').hide();
//jQuery('#litethick_see').hide();
jQuery('#overall_thickness_without_glass_type').show();
		jQuery('#lift_without_glass_type').show();
		jQuery('#lift_inside_with_glass_type').hide();jQuery('#lift_outside_with_glass_type').hide();
			jQuery('#treatment_without_glass_type').show();		jQuery('#glass_color_with_glass_type').show();	
			
			jQuery('#litethick_see').show();
	}else{
	//	alert("form.js line 70 " + back_date_entry);
		console.log('New date Job-id');
		jQuery('#lift_without_glass_type').hide();		jQuery('#lift_inside_with_glass_type').show();
		jQuery('#lift_outside_with_glass_type').show();
		jQuery('#treatment_without_glass_type').hide();		jQuery('#glass_color_with_glass_type').hide();	
		//alert('tester hide treatment');
	
		glass_piece_disabled();
	}
	   
	 
   });
   jqXHR.fail(function(jqXHR, textStatus, errorThrown) {
      console.log("fetch_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      alert("fetch_job:jqXHR.fail: textStatus => " + textStatus + ', errorThrown.name -> ' + errorThrown.name + ', errorThrown.message -> ' + errorThrown.message + ', errorThrown.toString() -> ' + errorThrown.toString());
      subButton.val('Submit New Job');
      subButton.prop('disabled', false);
   });
}


  jQuery(document).ready(function($) {
     var $loading = $('<div id="loading"></div>').insertBefore('#addsagjob');
     $(document).ajaxStart(function() {
        $loading.show();
     }).ajaxStop(function() {
        $loading.hide();
     });
     job_id = getParameterByName('job_id');
     created_date = new Date().toISOString();
     console.log('jQuery.document.ready: job_id -> ' + job_id + ', created_date => ' + created_date);
     if (typeof job_id === 'undefined' || job_id == null) job_id = 0;
     console.log('jQuery.document.ready: job_id -> ' + job_id + ', created_date => ' + created_date);
     if (job_id > 0) {
        fetch_job(job_id);
     } else {
        tgpd_init(false);
        tagd_init(false);
        newdoor_init(false);
     }

     $('#button_same_addresses').click(function(event) {
        event.preventDefault();
        event.stopPropagation();
        do_same_addresses();
     });

     $("input,select,textarea,.btn-infos").on("focus",function(){
       $(this).css("border","#CC3300 1px solid");
     });
     $("input,select,textarea,.btn-infos").focusout(function(){
       $(this).css("border","");
     });
     $("#button_submit_new_job").click(function(event){
       $("#seettt").val("submit");
       event.preventDefault();
       event.stopPropagation();
       save_job();
     });


     $("#button_submit_save_job").click(function(event){
       console.log("inside  #button_submit_save_job .click");
       $("#seettt").val("save");
       event.preventDefault();
       event.stopPropagation();
       save_job();
      });

    $('#addsagjob').submit(function(event) {
      event.preventDefault();
   });
});