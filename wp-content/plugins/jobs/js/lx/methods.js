
function get_lx_data(job_id) {
  jQuery('#test-lx').addClass('disabled');
  jQuery('#test-lx').html('Updating From LX');

   jQuery.ajax({
    type:"POST",
    url: ajax_object.ajax_url,
    data: {
      action: 'get_lx_data'
    },
    success:function(data){
      console.log(data);
      jQuery('#test-lx').removeClass('disabled');
      jQuery('#test-lx').html('LX Data');
    },
    error: function(errorThrown){
      alert(errorThrown);
    }
  });
}

jQuery(document).ready(function($) {
  $( "#test-lx" ).click(function() {
    get_lx_data();
  });
});

