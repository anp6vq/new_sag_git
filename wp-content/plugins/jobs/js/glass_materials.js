(function($) {
  $( document ).ready(function() {
    $( ".caulk-checkbox" ).change(function(event) { //glass type select input
      event.preventDefault();
      if($(this).is(':checked')){
        $(this).closest('.caulk-col').append(
          '<div class="row sub-row caulk-details">' +
            '<div class="col-md-4 col-sm-4">' +
              '<label for="caulk-amount">Amount</label>' +
              '<input type="text" class="form-control" id="caulk-amount" placeholder="Amount">' +
            '</div>' +
            '<div class="col-md-8 col-sm-8">' +
              '<label for="caulk-type">Type</label>' +
              '<select id="caulk-type" class="form-control">' +
                '<option value="" disabled selected>Select Caulk Type</option>' +
                '<option value="wip">' +
                  'Clear Silicone' +
                '</option>' +
                '<option>' +
                '  White Latex' +
                '</option>' +
                '<option>' +
                ' Another Caulk Example' +
                '</option>' +
                '<option>' +
                '  Black Silicone' +
                '</option>' +
              '</select>' +
            '</div>' +
          '</div>'
        );
      }
      else{
        $(this).closest('.caulk-col').find('.caulk-details').remove();
      }
    });
    $( ".scaffold-checkbox" ).change(function(event) { //glass type select input
      event.preventDefault();
      if($(this).is(':checked')){
        $(this).closest('.scaffold-col').append(
          '<div class="row sub-row scaffold-details">'+
            '<div class="col-md-3 col-sm-3">'+
              '<label for="scaff">Amount</label>'+
              '<input type="text" class="form-control" id="scaff" placeholder="Amount">'+
            '</div>'+
            '<div class="col-md-9 col-sm-9">'+
              '<label for="scaff-type">Type</label>'+
              '<select id="scaff-type" class="form-control">'+
                '<option value="" disabled selected>Select Scaffold Type</option>'+
                '<option value="wip">'+
                'Small Scaffolding'+
                '</option>'+
                '<option>'+
                'Medium Scaffolding'+
                '</option>'+
                '<option>'+
                ' Large Scaffolding'+
                '</option>'+
              '</select>'+
            '</div>'+
          '</div>'
        );
      }
      else{
        $(this).closest('.scaffold-col').find('.scaffold-details').remove();
      }
    });
    $( ".tape-checkbox" ).change(function(event) { //glass type select input
      event.preventDefault();
      if($(this).is(':checked')){
        $(this).closest('.tape-col').append(
          '<div class="row sub-row tape-details">'+
            '<div class="col-md-3 col-sm-3">'+
              '<label for="tape-amount">Amount</label>'+
              '<input type="text" class="form-control" id="tape-amount" placeholder="Amount">'+
            '</div>'+
            '<div class="col-md-9 col-sm-9">'+
              '<label for="tape-type">Type</label>'+
              '<select id="tape-type" class="form-control">'+
                '<option value="" disabled selected>Select Tape Type</option>'+
                '<option value="wip">'+
                  '1/8 440 tape'+
                '</option>'+
                '<option>'+
                  '1/2 440 Tape'+
                '</option>'+
                '<option>'+
                 '3/4 440 Tape'+
                '</option>'+
                '<option>'+
                  'Masking Tape'+
                '</option>'+
              '</select>'+
            '</div>'+
          '</div>'
        );
      }
      else{
        $(this).closest('.tape-col').find('.tape-details').remove();
      }
    });

  }); //end DOM READY
})( jQuery );
