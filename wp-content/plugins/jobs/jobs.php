<?php

/*Plugin Name: Jobs
  Plugin URI: http://www.imajine.us
  Description: Plugin for SAG to manage all jobs.
  Version: 1.0
  Author: Alex Polchert and Kay Taiwo
  Author URI: http://www.imajine.us */
date_default_timezone_set('America/New_York');
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define("TWILIO_ACCOUNT_SID",'AC68a861280317881172380b2ab3de2511');
define("TWILIO_AUTH_TOKEN",'8c53e87ee6de970f9c5778d206250efc');
// Outlook API Details
define('APP_ID', '98384d63-ec69-48f5-a055-da05be051154');
define('APP_PASSWORD', 'ktkLXPKB837(%--elqfSG02');
    
//LX Integration
define("LX_TOKEN", "368DFEAD-8D7F-4F5B-A2E8-D42C34C66047");
define("LX_QUOTES_URL", "https://lx.salbertglass.com/GLASPACAPI/QuoteNumbers/Get/?since=");//XXXXX&token=YYYYY
define("LX_QUOTE_DATA_URL", "https://lx.salbertglass.com/WorkOrder/Get/"); //add token

define("EMAILFROM","noreply@salbertglass.com");

require WP_CONTENT_DIR.'/plugins/jobs/twilio-php-master/Twilio/autoload.php';
// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;
/*error_log(1);
 $sid = 'AC68a861280317881172380b2ab3de2511';
      $token = '8c53e87ee6de970f9c5778d206250efc';
      $client = new Client($sid, $token);
try{
	  $message= $client->messages->create(
          // the number you'd like to send the message to
          '+15713554340',
          array(
            // A Twilio phone number you purchased at twilio.com/console
            'from' => '+12408984517 ',

            // the body of the text message you'd like to send
            'body' => "A new job has been added " . "http://b5c.e9c.myftpupload.com/add-job/"
          )
      );



	}
	catch (Exception $e) {
            echo 'Error: ' . $e->getMessage();
        }

	  die();*/
wp_enqueue_script( 'sag-jobs-create-cookie',  plugins_url( 'js/create-cookie.js', __FILE__ ), array( 'jquery' ) );
wp_enqueue_script( 'sag-jobs-ui.min',  plugins_url( 'js/jquery-ui.min.js', __FILE__ ), array( 'jquery' ) );
wp_enqueue_script( 'sag-jobs-timepicker',  plugins_url( 'js/jquery-ui-timepicker-addon.js', __FILE__ ), array( 'jquery' ) );    
wp_enqueue_style('s-albert-jquery-ui', plugins_url('/js/jquery-ui.css', __FILE__));
wp_enqueue_script( 'sag-jobs-timepicker-addon',  plugins_url( 'js/jquery-ui-timepicker-addon-i18n.min.js', __FILE__ ), array( 'jquery' ) );
wp_enqueue_script( 'sag-jobs-sliderAccess',  plugins_url( 'js/jquery-ui-sliderAccess.js', __FILE__ ), array( 'jquery' ) );

global $sag_db_version;
$sag_db_version = '1.0';
class WP_SAG_Jobs
{
  static $instance = false;
  
	public $table_nwdr_items_flag=false;
	public 	$table_nwaluof_items_flag=false;
	public 	$table_nwaluft_items_flag=false;
	public 	$table_nwaluff_items_flag= false;
	public 	$table_hmnd_items_flag=false;
	public 	$table_hmnf_items_flag=false;
	
  private function __construct() {
     
    add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'));
    
    add_action('wp_ajax_get_participants', array($this, 'get_participants'));
    add_action('wp_ajax_nopriv_get_participants', array($this, 'get_participants'));
    
    add_action('wp_ajax_edit_event_item', array($this, 'edit_event_item'));
    add_action('wp_ajax_nopriv_edit_event_item', array($this, 'edit_event_item'));

    add_action('wp_ajax_add_job_item', array($this, 'add_job_item'));
    add_action('wp_ajax_nopriv_add_job_item', array($this, 'add_job_item'));

	 add_action('wp_ajax_chagnesales_name', array($this, 'chagnesales_name'));
    add_action('wp_ajax_nopriv_chagnesales_name', array($this, 'chagnesales_name'));

  add_action('wp_ajax_browse_s3', 'browse_s3');
    add_action('wp_ajax_nopriv_browse_s3', 'browse_s3');
    
     add_action('wp_ajax_delete_temp', 'delete_temp');
    add_action('wp_ajax_nopriv_delete_temp', 'delete_temp');
    
    add_action('wp_ajax_update_log_for_pdf', 'update_log_for_pdf');
    add_action('wp_ajax_nopriv_update_log_for_pdf', 'update_log_for_pdf');

    add_action('wp_ajax_check_session_variable', 'check_session_variable');
    add_action('wp_ajax_nopriv_check_session_variable', 'check_session_variable');
    
     add_action('wp_ajax_get_qoute_number', 'get_qoute_number');
    add_action('wp_ajax_nopriv_get_qoute_number', 'get_qoute_number');

	 add_action('wp_ajax_change_accepted', array($this, 'change_accepted'));
    add_action('wp_ajax_nopriv_change_accepted', array($this, 'change_accepted'));
    
    add_action('wp_ajax_get_notes_content', array($this, 'get_notes_content'));
	add_action('wp_ajax_nopriv_get_notes_content', array($this, 'get_notes_content'));

	add_action('wp_ajax_save_modal_on_job_list', array($this, 'save_modal_on_job_list'));
	add_action('wp_ajax_nopriv_save_modal_on_job_list', array($this, 'save_modal_on_job_list'));
	add_action('wp_ajax_open_modal_on_job_list', array($this, 'open_modal_on_job_list'));
	add_action('wp_ajax_nopriv_open_modal_on_job_list', array($this, 'open_modal_on_job_list'));

	add_action('wp_ajax_change_joblist', array($this, 'change_joblist'));
	add_action('wp_ajax_nopriv_change_joblist', array($this, 'change_joblist'));


    add_action('wp_ajax_temp_uploade', array($this, 'temp_uploade'));
    add_action('wp_ajax_nopriv_temp_uploade', array($this, 'temp_uploade'));
	add_action('wp_ajax_get_quotenumbers', array($this, 'get_quotenumbers'));
    add_action('wp_ajax_nopriv_get_quotenumbers', array($this, 'get_quotenumbers'));
    add_action('wp_ajax_get_job_item', array($this, 'get_job_item'));
    add_action('wp_ajax_nopriv_get_job_item', array($this, 'get_job_item'));
    register_activation_hook(__FILE__, array($this, 'setup_db')); //when plugin is activated
    add_action('plugins_loaded', array($this, 'update_db'));
    add_filter('query_vars', array($this, 'query_vars'));
    add_action('init', array($this, 'shortcodes_init'));

    //GLASPAC LX Integration
    //check for updates from LX 6 months ago to start
    $lxDate = date( "Y-m-d H:i", mktime(0, 0, 0, date("m") - 6, date("d"),   date("Y") ) );
    add_option('lx_last_update', $lxDate);
    add_action('wp_ajax_get_lx_data', array($this, 'get_lx_data'));
    add_action('wp_ajax_nopriv_get_lx_data', array($this, 'get_lx_data'));

	add_action('wp_ajax_addquote_api', array($this, 'addquoteset'));
	add_action('wp_ajax_nopriv_addquote_api', array($this, 'addquoteset'));

	add_action('wp_ajax_insertquote_api', array($this, 'insertquote_api'));
	add_action('wp_ajax_nopriv_insertquote_api', array($this, 'insertquote_api'));
	//set old view date for backdate entry view
	add_option('fixed_backdate_job','2018-01-06');
	register_activation_hook( __FILE__,array($this,'sag_add_roles_on_plugin_activation') );
	register_deactivation_hook( __FILE__,array($this,'sag_add_roles_on_plugin_deactivation') );
	
	add_filter( 'allowed_http_origins',array($this,  'add_allowed_origins') );
	//add_action('init', array($this, 'loadImagesNew'));

	 add_action( 'phpmailer_init', 'send_smtp_email' );
}

public function send_smtp_email( $phpmailer ) {
  //error_log('jobs.php send smtp mail', 1 , 'alex@imajine.us');
  if ( ! is_object( $phpmailer ) ) {
    $phpmailer = (object) $phpmailer;
  }

  $phpmailer->Mailer      = 'smtp';
  // $phpmailer->Username   = SMTP_USER;
  // $phpmailer->Password   = SMTP_PASS;
  $phpmailer->Host        = SMTP_HOST;
  $phpmailer->SMTPAuth    = SMTP_AUTH;
  $phpmailer->Port        = SMTP_PORT;
  $phpmailer->SMTPSecure  = SMTP_SECURE;
  $phpmailer->SMTPAutoTLS = SMTP_AUTO_TLS;
  $phpmailer->From        = SMTP_FROM;
  $phpmailer->FromName    = SMTP_NAME;
}


public function add_allowed_origins( $origins ) {
    $origins[] = 'https://sag-jobs-photos.s3.amazonaws.com';

    return $origins;
}

	public function sag_add_roles_on_plugin_activation(){
	  add_role( 'sag_owner', 'Owner',   array('read'=> true,'edit_posts'=> true,'delete_posts' => false));
	  add_role( 'sag_csr', 'CSR',   array('read'=> true,'edit_posts'=> true,'delete_posts' => false));
	  add_role( 'sag_salesman', 'Salesman',   array('read'=> true,'edit_posts'=> true,'delete_posts' => false));
	  
	  global $wp_roles;
	  foreach ( $wp_roles->roles as $key=>$value ){
		$role = get_role( $key );
			foreach($this->sag_campaignVar as $keys=>$vals){
				$role->add_cap($vals);
	   }
	  }
	 // die;
	  
	 
	 }
	 public function sag_add_roles_on_plugin_deactivation()
	 {
	   remove_role( 'sag_owner' );
	   remove_role( 'sag_csr' );
	   remove_role( 'sag_salesman' );
	 }

  public static function getInstance() {
    if ( !self::$instance ) self::$instance = new self;
    return self::$instance;
  }

	public function salesManMapData(){
			$salesmanIdMap =  array(
			"SLA" => 2,
			"CW"  => 9,
			"KL"  => 10,
			"GRB" => 8,
			"EA"  => 11,
			"SD"  => 2,
			"CAB" => 12,
			"CAO" => 13,
			"MJA" => 14,
			"SG"  => 15,
			"DB"  => 16,
			"KG"  => 17,
			"AKr"  => 87,
			"RS"   =>86,
			"SC"   => 88,
			"EH"   => 89,
			"SK"   => 90
		);
		$salesmanEmailMap = array(
		  "SLA" => "steven@salbertglass.com",
		  "CW"  => "cwilliams@salbertglass.com",
		  "KL"  => "klewis@salbertglass.com",
		  "GRB" => "gburkhart@salbertglass.com",
		  "EA"  => "edwin@salbertglass.com",
		  "SD"  => "appmanager@salbertglass.com",
		  "CAB" => "cbaumes@salbertglass.com",
		  "CAO" => "cosborne@salbertglass.com",
		  "RS" => "rscott@salbertglass.com",
		  "MJA" => "malbert@salbertglass.com",
		  "SG"  => "sgreenstreet@salbertglass.com",
		  "DB"  => "dblackwell@salbertglass.com",
		  "KG"  => "kgrant@salbertglass.com",
		  "test" => "ajay@imajine.us",
		  "ALEX" => "alex@imajine.us",
		  "AKr" => "akrakowski@salbertglass.com",
		  "AJ" => "ajay@imajine.us",
		  'SC' => "scoleman@salbertglass.com",
		  'EH' => "ehuber@salbertglass.com",
		  'SK' => "skurth@salbertglass.com"
		);
		
		
		$csrNameMap = array(
		  "SLA" => "steven",
		  "AKr" => "akrakowski",
		  "CW"  => "cwilliams",
		  "KL"  => "klewis",
		  "GRB" => "gburkhart",
		  "EA"  => "edwin",
		  "SD"  => "appmanager",
		  "CAB" => "cbaumes",
		  "CAO" => "cosborne",
		  "RS" => "rscott",
		  "MJA" => "malbert",
		  "SG"  => "sgreenstreet",
		  "DB"  => "dblackwell",
		  "KG"  => "kgrant",
		  "test" => "ajay",
		  "ALEX" => "alex",
		  "AJ" => "ajay",
		  'SC' => 'scoleman',
		  'EH' => 'ehuber',
		  'SK' => 'skurth'
		);
				
		return array('salesmanIdMap'=>$salesmanIdMap,'salesmanEmailMap'=>$salesmanEmailMap,'csrNameMap'=>$csrNameMap);
	}
	
	public  function commanMail($csrexp,$owner_nameexp ,$salesman_nameexp,$frmEmail,$blankAry){
		
		$salesmanEmailMapData = $this->salesManMapData();

		//print_r($salesmanEmailMapData);

		//die;

		$salesmanEmailMap=$salesmanEmailMapData['salesmanEmailMap'];
		/*if($owner_nameexp=='AJ'){    			$emaillist='ajay@imajine.us';		} 
			else if($owner_nameexp=='ALEX'){		     $emaillist='alex@imajine.us';		}
			else{*/
			$emaillist =array();
			if(!in_array($owner_nameexp,$blankAry ))
				{      	$emaillist[]=$salesmanEmailMap[$owner_nameexp];       }
			else{       $emaillist[]='ajay@imajine.us';		}
			//}
			
			// sales man b case


			if($salesman_nameexp != 'na'){	// 
		/*	if( $salesman_nameexp=='AJ' && $emaillist!='ajay@imajine.us'){		$emaillist.=',ajay@imajine.us';	} 
			else if( $salesman_nameexp=='ALEX' &&   $emaillist!='alex@imajine.us'){		$emaillist.=',alex@imajine.us';	}
			else{*/
				if(!in_array($salesman_nameexp,$blankAry )){		$emaillist[]=$salesmanEmailMap[$salesman_nameexp];	}
				else{		$emaillist[]='ajay@imajine.us';	}
				
				$emaillist = array_unique($emaillist);
				if(count($emaillist)>1){
				$emaillistData = $emaillist[0] ."," .$emaillist[1];
				}else{
					$emaillistData = $emaillist[0];
					}
			}else{
				$emaillistData = $emaillist[0];
				}
			//}
			

		$headers='';
			
			$headers = "From: S. Albert Glass <".EMAILFROM.">". "\r\n"  ;
			
			
			$emailcheckAry = explode(",",$emaillistData );
			$csrmailFlag=false;


	
			
			if(!in_array($csrexp,$emailcheckAry)){
				$headersCCMail []= $csrexp;$csrmailFlag=true;
				}
				$stevenMailFlag=false;
			if(in_array('steven@salbertglass.com',$emailcheckAry)){
					$stevenMailFlag=true;
				}
				
			if( $csrexp!='appmanager@salbertglass.com' && $csrmailFlag==true && $stevenMailFlag==false)
			{		$headersCCMail[]= 'appmanager@salbertglass.com';	}
			else if( $csrexp!='appmanager@salbertglass.com' && $csrmailFlag==false  && $stevenMailFlag==false){
					$headersCCMail[]= 'appmanager@salbertglass.com';
			}
			
			if(count($headersCCMail)>0){
				
		    	$headers .=   "CC: ". implode(",",array_unique($headersCCMail));
				
			}
			$headers .=	  "\r\n" ;
			
			//$explodeToList = explode(",", $emaillist);
			/*if(!in_array('ajay@imajine.us',$explodeToList) && !in_array('alex@imajine.us',$explodeToList) ){
				$headers .= "Bcc: ajay@imajine.us,alex@imajine.us". "\r\n" ;
			}
			else if(!in_array('ajay@imajine.us',$explodeToList) ){
				$headers .= "Bcc: ajay@imajine.us". "\r\n" ;
			}
			else if(!in_array('alex@imajine.us',$explodeToList) ){
				$headers .= "Bcc: alex@imajine.us". "\r\n" ;
			}*/
		
		return array('emaillist'=>$emaillistData,'header'=>$headers);
	}	

  public function enqueue_styles() {
    if ( is_admin() )
	{
    }
	else
	{
      wp_enqueue_style('s-albert', plugins_url('/stylesheets/lib/sAlbertGlass.import.css', __FILE__));
      wp_enqueue_style('toggle-rem', plugins_url('/stylesheets/lib/toggle-switch.css', __FILE__));
      wp_enqueue_style('toggle', plugins_url('/stylesheets/lib/toggle-switch-px.css', __FILE__));
      wp_enqueue_style('toggle-px', plugins_url('/stylesheets/lib/toggle-switch-rem.css', __FILE__));
      wp_enqueue_style('media', plugins_url('/stylesheets/media.import.less', __FILE__));
      wp_enqueue_style('new-doors', plugins_url('css/doors/doors_new.css', __FILE__), array());
      wp_enqueue_style('repair-doors', plugins_url('css/doors/doors_repair.css', __FILE__), array());
      wp_enqueue_style('sag-jobs-plugin-frontend', plugins_url('css/frontend.css', __FILE__), array());
      wp_enqueue_style('sag-jobs-plugin-font-awesome', plugins_url('css/font-awesome.css', __FILE__), array());
      wp_enqueue_style('sag-jobs-plugin-bootstrap', plugins_url('css/bootstrap.min.css', __FILE__), array());
    }
  }

  public function temp_uploade()
  {
    $userId = get_current_user_id();
    $imageUrl = $_GET["image_url"];
    $type = $_GET["type"];

    global $wpdb;
    $table_temp_upload = $wpdb->prefix . 'temp_upload';

    $query = "SELECT COUNT(*) FROM $table_temp_upload WHERE user_id = $userId AND type = '".$type."'";

    $count = $wpdb->get_var($wpdb->prepare($query));

      if ($count) {
        $wpdb->update($table_temp_upload, array(
          'live_url'=>$imageUrl,
        ), array(
          'user_id'=>$userId,
		  'type'=>$type
        ));
      } else {
        $wpdb->insert($table_temp_upload, array(
          'user_id'=>$userId,
          'live_url'=>$imageUrl,
          'type'=>$type,
        ));
      }
  }
  public function get_quotenumbers(){
	 global $wpdb;

	$table_sag_jobs = $wpdb->prefix . 'sag_jobs';
	$sql = "SELECT quote_number, id FROM " . $table_sag_jobs . " where quote_number LIKE '".$_GET['term']."%' ORDER BY created_date DESC";
	$quotenumbers = $wpdb->get_results($sql);

	$quotenumberArray = array();
	$counter = 0;
	foreach($quotenumbers as $quotenumber)
{

		$quotenumberArray[$counter]['id'] = $quotenumber->id;
		$quotenumberArray[$counter]['label'] = $quotenumber->quote_number;
		$quotenumberArray[$counter]['value'] = $quotenumber->quote_number;
		$counter++;
	}

	echo json_encode($quotenumberArray);
	die;
}
public function insertquote_api()
{


	if(isset($_POST['quotenumber']))
		{
			
			$frmEmail='';
			//if(get_option("select_nmode")==2)
			//{
				$frmEmail = EMAILFROM;
			//}else{
			//	$frmEmail = 'development@salbertglass.com';
			//}
			
			
	$quoteId=$_POST['quotenumber'];
	$salesManMapData = $this->salesManMapData();
	$salesmanIdMap =$salesManMapData['salesmanIdMap'];
	$salesmanEmailMap =$salesManMapData['salesmanEmailMap'];


	    $data_lx= "https://lx.salbertglass.com/GLASPACAPI/WorkOrder/Get/".$quoteId."/?token=".LX_TOKEN."";

		 $home_a = @file_get_contents($data_lx);
		// print_r($home_a);

		if($home_a==false)
		{
		//echo "case1";

		}
		else
		{

		global $wpdb;
		$table_name= $wpdb->prefix."sag_jobs";

   			 $result_lx=json_decode($home_a);


				$wpdb->insert(
  			$table_name,
  			array(
  				'salesman_name' =>  'na',
				'owner_name' =>  $result_lx->SalesRepId,
  				'quote_number' => $result_lx->QuoteNumber,
  				'salesman_id' => $salesmanIdMap[$result_lx->SalesRepId],
  				'pdf_version' => '',
  				'job_name' => $result_lx->Notes[0],
  				'onsite_contact' =>$result_lx->Contact,
  				'contact_number' =>$result_lx->PhoneNumber,
  				'contact_email' => $result_lx->Email,
  				'person_seen' => '',
  				'csr' => $result_lx->CsrId,
  				'project_address_line1' => $result_lx->InstallationAddress->Address1,
  				'project_address_line2' => $result_lx->InstallationAddress->Address2,
  				'project_email' => '',
  				'project_city' => $result_lx->InstallationAddress->City,
  				'project_state' => $result_lx->InstallationAddress->State,
  				'project_postal_code' => $result_lx->InstallationAddress->Zip,
  				'billing_address_line1' => $result_lx->CustomerAddress->Address1,
  				'billing_address_line2' => $result_lx->CustomerAddress->Address2,
  				'billing_email' => '',
  				'billing_city' => $result_lx->CustomerAddress->City ,
  				'billing_state' => $result_lx->CustomerAddress->State,
  				'billing_postal_code' => $result_lx->CustomerAddress->Zip,
  				'project_description' => '',
  				'project_status' => 'new ticket',
  				'labor_hours_type' => '',
  				'labor_number_of_hours' => '',
  				'labor_number_of_men' => '',
  				'latitude' => '',
  				'longitude' => '',
  				'created_date' => date("Y-m-d H:i:s"),
  				'modifydate' => date("Y-m-d H:i:s"),
  				'QuoteId' => '',
  				'CustomerId'=>$result_lx->CustomerId,
  				'BillToId'=>$result_lx->BillToId

  			)
  		);



		//print_r($wpdb->show_errors());
  	$jobid = $wpdb->insert_id;


	   if ($jobid > 0){
        $headers = 'From: S. Albert Glass <'.EMAILFROM.'>';
        error_log('Salesman LX Email Map is '. $salesmanEmailMap[$result_lx->SalesRepId], 1, 'alex@imajine.us' );

         $salesRepID = $result_lx->SalesRepId;

		$link=get_permalink(get_option("acceptjobpage"));


		
		

        if (trim($salesRepID) == "SD"){


			$link=get_option("siteurl")."/add-job";


          $subject = $result_lx->QuoteNumber.' A NEW SERVICE TICKET has been created and needs to be Assigned.';
          $message = $result_lx->QuoteNumber." A NEW SERVICE TICKET has been created and needs to be Assigned."."\n\n";
          $message .= $link."?job_id=".$jobid."\n\n";
          $message .= "Job Description : ".$result_lx->Notes[0]."\n\n";
          $message .= "Sales Rep A : ".$result_lx->SalesRepId."\n";
          $message .= "CSR : ".$result_lx->CsrId."\n\n";
          $message .= "Project Address :"."\n";
          $message .= $result_lx->InstallationAddress->Address1."\n";
          $message .= $result_lx->InstallationAddress->Address2."\n";
          $message .= $result_lx->InstallationAddress->City.",".  $result_lx->InstallationAddress->State." - ".$result_lx->InstallationAddress->Zip."\n\n";
          $message .=  "Project Contact :"."\n";
          $message .= $result_lx->Contact."\n";
          $message .= $result_lx->PhoneNumber."\n";
          $message .= $result_lx->Email."\n";

/*
New Ticket, Main Email is Sales A, everyone else is cc
*/
		$emailsData=array();
		$emailsData[] = 	$this->getsalesmanmail($result_lx->SalesRepId);
		

		$emailsData[]='gburkhart@salbertglass.com';
		//$emailsData[]='steven@salbertglass.com';
		$emails=  implode(",",$emailsData);
	
	
		$csrMail ='';
		if($result_lx->CsrId!=$result_lx->SalesRepId){
			$csrMail = ",". 	$this->getcsrmail($result_lx->CsrId);
		}
		$headers = 	"From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
						"CC: ajay@imajine.us,alex@imajine.us".$csrMail;
		 sendmailmode($emails,$subject,$message,$headers); //cpmment date 16-1-18
		//  sendmailmode($salesmanEmailMap[$salesRepID],$subject,$message, $headers);
		  
        //maildisable  wp_mail('alex@imajine.us',$subject, $message,'From: S. Albert Glass <noreply@salbertglass.com>');
         //maildisable wp_mail('gburkhart@salbertglass.com',$subject, $message,'From: S. Albert Glass <noreply@salbertglass.com>');
        // wp_mail('steven@salbertglass.com',$subject, $message,'From: S. Albert Glass <development@salbertglass.com>');
        } else {

		$link=get_permalink(get_option("acceptjobpage"));

          //everyone but SD, GRB, and SLA
          $subject = $result_lx->QuoteNumber . " A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted.";
          $message = $result_lx->QuoteNumber." A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted."."\n\n";
          $message .= $link."?job_id=".$jobid."\n\n";
          $message .= "Job Description : ".$result_lx->Notes[0]."\n\n";
          $message .= "Sales Rep A: ".$result_lx->SalesRepId."\n";
          $message .= "CSR : ".$result_lx->CsrId."\n\n";
          $message .= "Project Address :" ."\n";
          $message .= $result_lx->InstallationAddress->Address1 ."\n";
          $message .=$result_lx->InstallationAddress->Address2 ."\n";
          $message .= $result_lx->InstallationAddress->City.",".  $result_lx->InstallationAddress->State." - ".$result_lx->InstallationAddress->Zip."\n\n";
          $message .=  "Project Contact :"."\n";
          $message .= $result_lx->Contact."\n";
          $message .= $result_lx->PhoneNumber."\n";
          $message .= $result_lx->Email."\n";
		  
		  
			$emailsData =array();
			$emailsData[] = 	$this->getsalesmanmail($result_lx->SalesRepId);
			$emailsData[]='gburkhart@salbertglass.com';
			
			$emails=  implode(",",$emailsData);
			
			$csrMail ='';
			if($result_lx->CsrId!=$result_lx->SalesRepId){
				$csrMail = ",". 	$this->getcsrmail($result_lx->CsrId);
			}
			$headers = 	"From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
			"CC: ajay@imajine.us,alex@imajine.us".$csrMail;
		  
		//  sendmailmode('alex@imajine.us,gburkhart@salbertglass.com,steven@salbertglass.com',$subject,$message); cpmment date 16-1-18
		  sendmailmode($emails,$subject,$message, $headers);
        // wp_mail('steven@salbertglass.com',$subject, $message,'From: S. Albert Glass <development@salbertglass.com>');
          //maildisable wp_mail('alex@imajine.us',$subject, $message,'From: S. Albert Glass <noreply@salbertglass.com>');
        }

	  echo  $jobid;
	  }
	}

}

		exit();

}
public function addquoteset()
	{

		if(isset($_POST['quotenumber']))
		{
			global $wpdb;
			$jobs=$wpdb->prefix."sag_jobs";
			$quotenumber=$_POST['quotenumber'];
			$select="select * from $jobs where quote_number ='$quotenumber'";
			$results=$wpdb->get_results($select);

			//
			if(count($results)>0)
				{
					echo json_encode(array('message'=>'success','setmessage'=>'This quote already exists'));

				}
				else
				{
					$getresults=$this->insertquote($quotenumber);
					//print_r($getresults);

					if($getresults==0)
					{
						echo json_encode(array('message'=>'success','setmessage'=>"This quote number doesn't exist in LX"));
					}
					else
					{
						//$html="Salesman Name :".$getresults->SalesRepId;
						//$html.="Salesman Name :".$getresults->SalesRepId;

						echo json_encode(array('message'=>'successadd','setmessage'=>'This quote number is in LX'));
					}

					//print_r($getresults);
					//die();


				}

		}
		exit();

	}


public function insertquote($quoteId)
{
	
	    $data_lx= "https://lx.salbertglass.com/GLASPACAPI/WorkOrder/Get/".$quoteId."/?token=".LX_TOKEN."";

		 $home_a = @file_get_contents($data_lx);
		// print_r($home_a);

		if($home_a==false)
		{
		//echo "case1";
			return 0;
		}
		else
		{
			return 1;



		}





}

  public function get_lx_data(){
	  
	  
	 $frmEmail='';
//	if(get_option("select_nmode")==2)
//	{
		$frmEmail = EMAILFROM;
//	}else{
//		$frmEmail = 'development@salbertglass.com';
//	}
		
   $salesManMapData = $this->salesManMapData();
	$salesmanIdMap =$salesManMapData['salesmanIdMap'];
	$salesmanEmailMap =$salesManMapData['salesmanEmailMap'];

    //for each quote that is not in the db,
   // $lxDate = date( "Y-m-d H:i", mktime(18, 40, 0, date("m"), 3,   date("Y") ) );
    sleep(5);
    $strLxDate         = get_option( 'lx_last_update'); //this must exist
    $lxDate = date( "Y-m-d",strtotime("-4 hours"));
     update_option('lx_last_update', $lxDate);
     echo LX_QUOTES_URL . str_replace ( ' ' , '%20' , $strLxDate ) . '&token=' . LX_TOKEN ;

    $lxQuotesURL       = LX_QUOTES_URL . str_replace ( ' ' , '%20' , $strLxDate ) . '&token=' . LX_TOKEN ;
    $lxQuoteDataURL    = LX_QUOTE_DATA_URL . LX_TOKEN;
    $quotesResponse    = wp_remote_get( $lxQuotesURL );
    foreach( json_decode( $quotesResponse["body"] ) as $quoteId ){ //iterate all quotes
    $data_lx= "https://lx.salbertglass.com/GLASPACAPI/WorkOrder/Get/".$quoteId."/?token=".LX_TOKEN."";
    $home_a = file_get_contents($data_lx);

    $result_lx=json_decode($home_a);


    $table_name = $wpdb->prefix . "sag_jobs";
    global $wpdb;
    $results_quote = $wpdb->get_var( 'SELECT COUNT(quote_number) FROM wp_4twsj5a02k_sag_jobs WHERE quote_number="'.$result_lx->QuoteNumber.'"');

		if($results_quote==0){
		$wpdb->show_errors();
  		$wpdb->insert(
  			$table_name,
  			array(
  				'salesman_name' =>  'na',
				'owner_name' =>  $result_lx->SalesRepId,
  				'quote_number' => $result_lx->QuoteNumber,
  				'salesman_id' => $salesmanIdMap[$result_lx->SalesRepId],
  				'pdf_version' => '',
  				'job_name' => $result_lx->Notes[0],
  				'onsite_contact' =>$result_lx->Contact,
  				'contact_number' =>$result_lx->PhoneNumber,
  				'contact_email' => $result_lx->Email,
  				'person_seen' => '',
  				'csr' => $result_lx->CsrId,
  				'project_address_line1' => $result_lx->InstallationAddress->Address1,
  				'project_address_line2' => $result_lx->InstallationAddress->Address2,
  				'project_email' => '',
  				'project_city' => $result_lx->InstallationAddress->City,
  				'project_state' => $result_lx->InstallationAddress->State,
  				'project_postal_code' => $result_lx->InstallationAddress->Zip,
  				'billing_address_line1' => $result_lx->CustomerAddress->Address1,
  				'billing_address_line2' => $result_lx->CustomerAddress->Address2,
  				'billing_email' => '',
  				'billing_city' => $result_lx->CustomerAddress->City ,
  				'billing_state' => $result_lx->CustomerAddress->State,
  				'billing_postal_code' => $result_lx->CustomerAddress->Zip,
  				'project_description' => '',
  				'project_status' => 'new ticket',
  				'labor_hours_type' => '',
  				'labor_number_of_hours' => '',
  				'labor_number_of_men' => '',
  				'latitude' => '',
  				'longitude' => '',
  				'created_date' => date("Y-m-d H:i:s"),
  				'modifydate' => date("Y-m-d H:i:s"),
  				'QuoteId' => '',
  				'CustomerId'=>$result_lx->CustomerId,
  				'BillToId'=>$result_lx->BillToId

  			)
  		);
      $jobid = $wpdb->insert_id; // get job id of newly inserted lx ticket.
      if ($jobid > 0){
        $headers = 'From: S. Albert Glass <'.EMAILFROM.'>';
        error_log('Salesman LX Email Map is '. $salesmanEmailMap[$result_lx->SalesRepId], 1, 'alex@imajine.us' );

        $salesRepID = $result_lx->SalesRepId;

		$link=get_permalink(get_option("acceptjobpage"));
		
       if (trim($salesRepID) == "SD"){

			$link=get_option("siteurl")."/add-job";
          //$subject = 'New Ticket Needs Assignment '.$result_lx->QuoteNumber;
		  $subject = $result_lx->QuoteNumber.' A NEW SERVICE TICKET has been created and needs to be Assigned.';
          $message = $result_lx->QuoteNumber." A NEW SERVICE TICKET has been created and needs to be Assigned."."\n\n";
          $message .= $link."?job_id=".$jobid."\n\n";
          $message .= "Job Description : ".$result_lx->Notes[0]."\n\n";
          $message .= "Sales Rep A : ".$result_lx->SalesRepId."\n";
          $message .= "CSR : ".$result_lx->CsrId."\n\n";
          $message .= "Project Address :"."\n";
          $message .= $result_lx->InstallationAddress->Address1."\n";
          $message .= $result_lx->InstallationAddress->Address2."\n";
          $message .= $result_lx->InstallationAddress->City.",".  $result_lx->InstallationAddress->State." - ".$result_lx->InstallationAddress->Zip."\n\n";
          $message .=  "Project Contact :"."\n";
          $message .= $result_lx->Contact."\n";
          $message .= $result_lx->PhoneNumber."\n";
          $message .= $result_lx->Email."\n";
	
		  
		$emailsData =array();
		$emailsData[] = 	$this->getsalesmanmail($result_lx->SalesRepId);
		

		$emailsData[]='gburkhart@salbertglass.com';
		//$emailsData[]='steven@salbertglass.com';
		$emails=  implode(",",$emailsData);
	
	//	  sendmailmode('alex@imajine.us,gburkhart@salbertglass.com,steven@salbertglass.com',$subject,$message);
		$csrMail ='';
		  if($result_lx->CsrId!=$result_lx->SalesRepId){
			$csrMail = ",". 	$this->getcsrmail($result_lx->CsrId);
		  }
		  
		$headers = 	"From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
						"CC: ajay@imajine.us,alex@imajine.us".$csrMail;
						
		  sendmailmode($emails,$subject,$message,$headers);
		  
		  
/*  $headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
"CC: ajay@imajine.us,alex@imajine.us";
		//  sendmailmode('alex@imajine.us,gburkhart@salbertglass.com,steven@salbertglass.com',$subject,$message,  $headers);
		  sendmailmode('appmanager@salbertglass.com',$subject,$message,  $headers);*/
        //maildisable  wp_mail('alex@imajine.us',$subject, $message,'From: S. Albert Glass <noreply@salbertglass.com>');
         //maildisable wp_mail('gburkhart@salbertglass.com',$subject, $message,'From: S. Albert Glass <noreply@salbertglass.com>');
        // wp_mail('steven@salbertglass.com',$subject, $message,'From: S. Albert Glass <development@salbertglass.com>');
        } else {

		$link=get_permalink(get_option("acceptjobpage"));
          //everyone but SD, GRB, and SLA
          $subject = $result_lx->QuoteNumber.' A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted.';
          $message = $result_lx->QuoteNumber." A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted."."\n\n";
          $message .= $link."?job_id=".$jobid."\n\n";
          $message .= "Job Description : ".$result_lx->Notes[0]."\n\n";
          $message .= "Sales Rep A : ".$result_lx->SalesRepId."\n";
          $message .= "CSR : ".$result_lx->CsrId."\n\n";
          $message .= "Project Address :" ."\n";
          $message .= $result_lx->InstallationAddress->Address1 ."\n";
          $message .=$result_lx->InstallationAddress->Address2 ."\n";
          $message .= $result_lx->InstallationAddress->City.",".  $result_lx->InstallationAddress->State." - ".$result_lx->InstallationAddress->Zip."\n\n";
          $message .=  "Project Contact :"."\n";
          $message .= $result_lx->Contact."\n";
          $message .= $result_lx->PhoneNumber."\n";
          $message .= $result_lx->Email."\n";
		
		$emailsData =array();
		$emailsData[] = 	$this->getsalesmanmail($result_lx->SalesRepId);
		
		$emailsData[]='gburkhart@salbertglass.com';
		//$emailsData[]='steven@salbertglass.com';
		$emails=  implode(",",$emailsData);
		
		$csrMail ='';
		if($result_lx->CsrId!=$result_lx->SalesRepId){
			$csrMail = ",". 	$this->getcsrmail($result_lx->CsrId);
		}
		  
	
		  
		    $headers = 	"From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
						"CC: ajay@imajine.us,alex@imajine.us".$csrMail;
		sendmailmode($emails,$subject,$message,$headers);
			
		  
		  
		  
		/*  if( $emailid=='steven@salbertglass.com'){
			    $headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
"CC: ajay@imajine.us,alex@imajine.us";
		  }else{
		    $headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
"CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us";
		  }
		  sendmailmode($emailid,$subject,$message,  $headers);**/
        // wp_mail('steven@salbertglass.com',$subject, $message,'From: S. Albert Glass <development@salbertglass.com>');
          //maildisable wp_mail('alex@imajine.us',$subject, $message,'From: S. Albert Glass <noreply@salbertglass.com>');
        }
      }
		}
  } // end foreach
    die();
  } //end get_lx_data();
  public function sennotification_mail($status)
  {
  	global $wpdb;
	$sagjobs=$wpdb->prefix."sag_jobs";
	$fivedays_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 5, date("Y")));
	if($status=="reasearch")
	{
	 $select="SELECT * FROM $sagjobs where project_status ='research' and modifydate < '".$fivedays_ago."'";
	}
	else if($status=="newticket")
	{
		  $select="SELECT * FROM $sagjobs where project_status ='new ticket' AND created_date > '2017-5-13' and created_date < '".$fivedays_ago."'";
	}
	$results=$wpdb->get_results($select);
//echo "<pre>";print_r($results);die;
		foreach($results as $value)
			{

				//$csrmailid=$this->getcsrmail($value->csr);
			/*	$csremail=array('cosborne@salbertglass.com','dblackwell@salbertglass.com','kgrant@salbertglass.com','edwin@salbertglass.com','gburkhart@salbertglass.com','sgreenstreet@salbertglass.com','steven@salbertglass.com');

				$salesmanemail=array('steven@salbertglass.com','cwilliams@salbertglass.com','klewis@salbertglass.com','gburkhart@salbertglass.com','edwin@salbertglass.com','steven@salbertglass.com','cbaumes@salbertglass.com','cosborne@salbertglass.com','malbert@salbertglass.com','sgreenstreet@salbertglass.com','dblackwell@salbertglass.com','kgrant@salbertglass.com');
				*/
					//$mailidfinal=array("gburkhart@salbertglass.com","appmanager@salbertglass.com");
				$mailidfinal=array("appmanager@salbertglass.com");
				$csmail=$this->getcsrmail($value->csr);
				if(!in_array($csmail,$mailidfinal))
				{
					array_push($mailidfinal,$csmail);
				}

				 $salesmail=$this->getsalesmanmail($value->owner_name);

				if(!in_array($salesmail,$mailidfinal))
				{
					array_push($mailidfinal,$salesmail);
				}



				$createddate=date("m/d/Y",strtotime($value->created_date));
				$start = strtotime($value->created_date);
									$end = time();

									$days_between = ceil(abs($end - $start) / 86400);
									if($days_between<=0)
									{
										$days_between=1;
									}


				 $subject =" ";
				 $message =" ";
				 $link=" ";
			$link=get_permalink(get_option("acceptjobpage"));


				if($status=="reasearch")
					{
					 $subject = $value->quote_number . " is " . $days_between . " Days Old and has been in research since ".$createddate;
					$message = $value->quote_number. " is " . $days_between ." Days Old and has been in research since ".$createddate."."."\n\n";
					}
					else if($status=="newticket" && $value->accepted==0)
					{


							 $subject = $value->quote_number . " is ".$days_between." Days Old and has NOT been Accepted ".$createddate;
							$message = $value->quote_number." is ".$days_between." Days Old and has NOT been Accepted ".$createddate."\n\n";
					}
					else if($status=="newticket" && $value->accepted==1)
					{
$subject = $value->quote_number . " is ".$days_between." Days Old and has been accepted but has not been updated ".$createddate;
							$message = $value->quote_number." is ".$days_between." Days Old and has been accepted but has not been updated ".$createddate."\n\n";
							$link=get_option("siteurl")."/add-job";

					}



					$message .= $link."?job_id=".$value->id."\n\n";
					$message .= "Job Description : ".$value->job_name."\n\n";
					$message .= "Sales Rep A : ".$value->owner_name."\n";
					$message .= "Sales Rep B : ".$value->salesman_name."\n";
					$message .= "CSR : ".$value->csr."\n\n";
					$message .= "Project Address :" ."\n";
					$message .= $value->project_address_line1 ."\n";
					$message .=$value->project_address_line2 ."\n";
					$message .= $value->project_city.",".  $value->project_state." - ".$value->project_postal_code."\n\n";
					$message .=  "Project Contact :"."\n";
					$message .= $value->onsite_contact."\n";
					$message .= $value->contact_number."\n";
				  $message .= $value->contact_email."\n";

				// die();
					$mailidfinal=array_filter($mailidfinal);
				$mailidfinal= array_unique($mailidfinal);

				  	  $allcsr=implode(",",$mailidfinal);
/*reassigned Main email is Sales A,B everyone else is cc  */
		//		$salesman_name_a =   $this->getsalesmanmail($value->owner_name);
				//$salesman_name_b=  $this->getsalesmanmail($value->salesman_name);
				   $headers = 	"From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
						"CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us";
				  sendmailmode($allcsr,$subject,$message,$headers);


			}
			exit();

  }
  
 
   //------------------------------------------------------------------------------
   public function sendEventMail($event_id){
      	global $wpdb;
      	$current_user = wp_get_current_user();
		$cuser = $current_user->user_login;
		$sagjobs=$wpdb->prefix."sag_jobs";
    	$sagevents=$wpdb->prefix."sag_events";
    	$event=$wpdb->get_row("SELECT * FROM $sagevents where id ='".$event_id."'");
    	$value=$wpdb->get_row("SELECT * FROM $sagjobs where id ='".$event->job_id."'");
    	//echo "<pre>";echo $event_id;print_r($event);print_r($value);die;
		
		$mailidfinal=array("appmanager@salbertglass.com");
		$csmail=$this->getcsrmail($value->csr);
		if(!in_array($csmail,$mailidfinal)){
			array_push($mailidfinal,$csmail);
		}

		$salesmail=$this->getsalesmanmail($value->owner_name);
		if(!in_array($salesmail,$mailidfinal)){
			array_push($mailidfinal,$salesmail);
		}
		if($value->owner_name=='SLA' || $value->salesman_name=='SLA'){
			if (($key = array_search('appmanager@salbertglass.com', $mailidfinal)) !== false) {
				unset($mailidfinal[$key]);
			}
		}
		/*if (($key = array_search('steven@salbertglass.com', $mailidfinal)) !== false) {
			unset($mailidfinal[$key]);
		}*/
	
		#print_r($mailidfinal);
		#print_r($value);
		#print_r($event);
		
		$event_type_arr = array('Appointment','Call','Notes','Text Message','Email');
		$event_type = $event_type_arr[$event->type];

		$subject =" ";
		$message =" ";
		$link=" ";
		$link=get_permalink(get_option("acceptjobpage"));

	 
		$subject = $value->quote_number . " A New Event Type " . $event_type . " has been created";
		
		if($event->type!='0'){
    		$message .= get_option("siteurl")."/add-job/?job_id=".$value->id."\n\n";
    		$message .= $event_type." : ".$event->notes." By ".$cuser." ".date('h:i A m/d/Y', strtotime($event->start_time))."\n\n";
    		//$message .= "Date : ".date('H:i A m/d/Y', strtotime($event->start_time))."\n\n";
    		$message .= "Created By : ".$cuser."\n";
    		$message .= "Sales Rep A : ".$value->owner_name."\n";
    		$message .= "Sales Rep B : ".$value->salesman_name."\n";
    		$message .= "CSR : ".$value->csr."\n\n";
    		$message .= "Project Address :" ."\n";
    		$message .= $value->project_address_line1 ."\n";
    		$message .= $value->project_address_line2 ."\n";
    		$message .= $value->project_city.",".  $value->project_state." - ".$value->project_postal_code."\n\n";
    		$message .= "Project Contact :"."\n";
    		$message .= $value->onsite_contact."\n";
    		$message .= $value->contact_number."\n";
    		$message .= $value->contact_email."\n";
		    
		}
		/*else{
    		$message .= $event->notes;
		}*/
		// die();
		$mailidfinal=array_filter($mailidfinal);
		$mailidfinal= array_unique($mailidfinal);

		$allcsr=implode(",",$mailidfinal);
		//$allcsr="ajay@imajine.us";
	    $headers = 	"From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .
		"CC:";
	    //"CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us";
	    sendmailmode($allcsr,$subject,$message,$headers);
	    #var_dump(sendmailmode($allcsr,$subject,$message,$headers));
		//echo '<pre>';
		#echo '<hr>';
		#print_r($allcsr);
		##print_r($subject);
		#print_r($message);
		#print_r($headers);
		
		exit();
    }
    
     function getNotesContent($job_id,$notes){
		global $wpdb;
		$current_user = wp_get_current_user();
		$cuser = $current_user->user_login;
		$value=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."sag_jobs where id ='".$job_id."'");
		$content .= get_option("siteurl")."/add-job/?job_id=".$value->id."<div>&nbsp;</div>\n\n";
		$content .= "<div>Job Description : ".$value->job_name."</div><div>&nbsp;</div>\n\n";
		$content .= "<div>Quote Number : ".$value->quote_number."</div><div>&nbsp;</div>\n\n";
		$content .= "<div>Notes : ".$notes." By ".$cuser."</div><div>&nbsp;</div>\n\n";
		$content .= "<div>Created By : ".$cuser."</div>\n";
		$content .= "<div>Sales Rep A : ".$value->owner_name."</div>\n";
		$content .= "<div>Sales Rep B : ".$value->salesman_name."</div>\n";
		$content .= "<div>CSR : ".$value->csr."</div><div>&nbsp;</div>\n\n";
		$content .= "<div>Project Address :" ."</div>\n";
		$content .= "<div>".$value->project_address_line1 ."</div>\n";
		$content .= "<div>".$value->project_address_line2 ."</div>\n";
		$content .= "<div>".$value->project_city.",".  $value->project_state." - ".$value->project_postal_code."</div><div>&nbsp;</div>\n\n";
		$content .= "<div>Project Contact :"."</div>\n";
		$content .= "<div>".$value->onsite_contact."</div>\n";
		$content .= "<div>".$value->contact_number."</div>\n";
		$content .= "<div>".$value->contact_email."</div>\n";
		return $content;
	}
  //--------------------------------------------------------------------------------
 
 
  
    public function getsalesmanmail($salesman)
  {
	  
	 

  	if($salesman !=''){
		  $email='';
		 if($salesman=='CW'){
     		 $email= 'cwilliams@salbertglass.com';
		 }
		 else if($salesman=='KL'){
     		 $email= 'klewis@salbertglass.com';
		 }else if($salesman=='AKr'){
     		 $email= 'akrakowski@salbertglass.com';
		 }else if($salesman=='GRB'){
     		 $email= 'gburkhart@salbertglass.com';
		 }else if($salesman=='EA'){
     		 $email= 'edwin@salbertglass.com';
		 }else if($salesman=='SD'){
     		 $email= 'appmanager@salbertglass.com';
		 }else if($salesman=='CAB'){
     		 $email= 'cbaumes@salbertglass.com';
		 }else if($salesman=='CAO'){
     		 $email= 'cosborne@salbertglass.com';
		 }else if($salesman=='MJA'){
     		 $email= 'malbert@salbertglass.com';
		 }else if ($salesman=='RS'){
   		    $email = 'rscott@salbertglass.com';
   		 }else if($salesman=='SG'){
     		 $email= 'sgreenstreet@salbertglass.com';
		 }else if($salesman=='DB'){
     		 $email= 'dblackwell@salbertglass.com';
		 }else if($salesman=='KG'){
     		 $email= 'kgrant@salbertglass.com';
		
		 }else if($salesman=='SC'){
     		 $email= 'scoleman@salbertglass.com';
		
		 }

		 else if($salesman=='EH'){
     		 $email= 'ehuber@salbertglass.com';
		
		 }

		 else if($salesman=='SK'){
     		 $email= 'skurth@salbertglass.com';
		
		 }
		 else if($salesman=='SLA' || $salesman=='sla'){
     		 $email= 'steven@salbertglass.com';
		
		 }
		 else if($salesman=='AJ' || $salesman=='aj'){
		 
		 	$email= 'ajay@imajine.us';
		 }
		 else if($salesman=='ALEX' || $salesman=='alex'){
		 
		 	$email= 'alex@imajine.us';
		 }
		 else
		 {
		 	$email= 'steven@salbertglass.com';
		 }
	}
  return $email;
  }

  public function getcsrmail($csr)
  {

  		$emailcsr='';
		if($csr=='cosborne'){
			$emailcsr= 'cosborne@salbertglass.com';
		}else if($csr=='dblackwell'){
			$emailcsr= 'dblackwell@salbertglass.com';
		}else if($csr=='akrakowski'){
			$emailcsr= 'akrakowski@salbertglass.com';
		}else if ($csr=='RS'){
			$emailcsr = 'rscott@salbertglass.com';
		}else if($csr=='kgrant'){
			$emailcsr= 'kgrant@salbertglass.com';
		}else if($csr=='edwin'){
			$emailcsr= 'edwin@salbertglass.com';
		}else if($csr=='gburkhart'){
			$emailcsr= 'gburkhart@salbertglass.com';
		}else if($csr=='sgreenstreet'){
			$emailcsr='sgreenstreet@salbertglass.com';
		}else if($csr=='cwilliams'){
     		 $emailcsr= 'cwilliams@salbertglass.com';
		 }else if($csr=='scoleman'){
     		 $emailcsr= 'scoleman@salbertglass.com';
		 }
		 else if($csr=='ehuber'){
     		 $emailcsr= 'ehuber@salbertglass.com';
		 }
		 else if($csr=='skurth'){
     		 $emailcsr= 'skurth@salbertglass.com';
		 }
		 else if((strtolower($csr)=='alex') || (strtolower($csr)=='alex@imajine.us')){
			 $emailcsr ='alex@imajine.us';
		 }
		 else if((strtolower($csr)=='ajay') || (strtolower($csr)=='ajay@imajine.us')){
			 $emailcsr ='ajay@imajine.us';
		 }
		else{
			$emailcsr =  'steven@salbertglass.com';
		}

			return $emailcsr;


  }
 public function change_joblist()
 {
 	global $wpdb;	global $post;
	
    $table_sag_jobs = $wpdb->prefix . 'sag_jobs';
    if(isset($_POST['jobtype'])){
		$projectStatus = $_POST['jobtype'];
	}else{
		$projectStatus='new ticket';
		}
	$user_id = get_current_user_id();
	
	$current_user = wp_get_current_user();

	$is_csr = false;	$csr_name = '';		$is_salesman = false;	$salesman_name = '';
	
	$csrdataValues = $this->salesManMapData();
	
	$csrAry = array('SG','CAO','DB','KG','SC','SK');
	
	$salesManAry = array('SLA','RS','CW','KL','GRB','EA','AJ','EH');
	if (in_array($current_user->user_login,$csrAry)){
		$is_csr = true;		$csr_name	= $csrdataValues['csrNameMap'][$current_user->user_login];	
	}

	if ($is_csr){ //Greg and Steven are assigned as admins.
		$sql = "SELECT * FROM " . $table_sag_jobs . " where project_status ='$projectStatus' and csr = '".$csr_name."' 
		ORDER BY created_date DESC";
	
	}
	if (in_array($current_user->user_login,$salesManAry)){
		$is_salesman = true;
		$salesman_name = $current_user->user_login;
		$sql = "SELECT * FROM  ".$table_sag_jobs." WHERE (owner_name = '". $salesman_name . "') and 
		project_status ='$projectStatus' ORDER BY created_date DESC";
	}
	if(!$is_csr && !$is_salesman){
	
		$sql = "SELECT * FROM " . $table_sag_jobs . " where project_status ='$projectStatus' ORDER BY created_date DESC";
	}
	
	$sag_jobsjjjj = $wpdb->get_results( $sql );
	
	$counttotal=count($sag_jobsjjjj);
	
	$showlimit=50;	$start=	0;
	
	if(isset($_REQUEST['pagesno'])){		$start=$_REQUEST['pagesno']*$showlimit;	}
	
	$sql=$sql." limit ".$start." ,".$showlimit;
	
	$sag_jobs = $wpdb->get_results( $sql );
	
	$arySalesNOwner =	array($sag_job->salesman_name,$sag_job->owner_name);
	$clsDangr= 'btn btn-danger btn-sm';
	$clsMrgn='margin-top:2px;width:78px;';
		
	foreach ( $sag_jobs as $sag_job ) {
               
        $dataSend .='   <tr> <td>';
			    
		$acceptedId	=	0;
		
		if(in_array($current_user->user_login ,$arySalesNOwner) && $sag_job->accepted ==0)
		{		$acceptedId = $sag_job->id;		}
		
		$queryArgu = array('job_id'=> $sag_job->id,'accepted'=>$acceptedId);
		
		$link = esc_url(add_query_arg($queryArgu, get_permalink( get_page_by_title( 'Add Job' ) )));
		
		$dataSend .='<a  target="_blank" class="btn btn-danger btn-sm"  href="'.$link .'" role="button">'. $sag_job->quote_number;
		
		$dataSend .=($sag_job->secondary_quote !='')? ','.$sag_job->secondary_quote : '';
		
		$dataSend .='</a>';
		
		
		if($acceptedId>0)
		{
			$dataSend .='<a id="accepted_'.$sag_job->id.'" class="'.$clsDangr.'" href="" role="button" style="'.$clsMrgn.'">Accept</a>';
		}

		$dataSend .='</td>
              <td class="center">
              <span class="dashboard-edit-job glyphicon glyphicon-pencil" id="editjobset_'.$sag_job->id.'"></span>
              &nbsp;
				<a class="dashboard-event" data-jobid="'.$sag_job->id.'">
					<span class="glyphicon glyphicon-calendar"></span>
				</a>
              </td>
              <td>';
        $dataSend .='<select style="max-width:100px;" id="ownernamejob_'.$sag_job->id.'" disabled="disabled">';
				

		$myrows_owner = $wpdb->get_results( "SELECT owner_name FROM wp_4twsj5a02k_sag_jobs GROUP BY owner_name" );
		foreach($myrows_owner as $myrowss_owner){
		
			if($myrowss_owner->owner_name !=''){
				$email='';		$selectset='';
				$email	=	$this->getsalesmanmail($myrowss_owner->owner_name);
				if($sag_job->owner_name==$myrowss_owner->owner_name){	$selectset='selected=selected';	}
		
				 $dataSend .='<option value="'.$myrowss_owner->owner_name.'" '.$selectset.'>'.$myrowss_owner->owner_name.'</option>';
		 	}
		
		}
		
		$dataSend .='</select></br></br>';
		$dataSend .='<select style="max-width:100px;" id="salesnamejob_'.$sag_job->id.'" disabled="disabled">';
		
		$myrows = $wpdb->get_results( "SELECT salesman_name,contact_email FROM wp_4twsj5a02k_sag_jobs GROUP BY salesman_name" );
		foreach($myrows as $myrowss){
			$selectset	=	'';		$email	=	'';
			if($myrowss->salesman_name !=''){
				
				if($sag_job->salesman_name==$myrowss->salesman_name){		$selectset="selected=selected";		}
				
				$dataSend .='<option value="'.$myrowss->salesman_name.'" '.$selectset.'>'.$myrowss->salesman_name.'</option>';
		
			 }
		}
				  
		$dataSend .='</select></td>';
		$dataSend .='<td>';
		$dataSend .='<select style="max-width:100px;" id="csrjob_'.$sag_job->id.'" disabled="disabled" >';
		$myrowsd = $wpdb->get_results( "SELECT csr FROM wp_4twsj5a02k_sag_jobs GROUP BY csr" );
		foreach($myrowsd as $myrowsss){
			if($myrowsss->csr !=''){
				$emailcsr=''; $csrselectset='';
				$emailcsr= $this->getcsrmail($myrowsss->csr);

				if($myrowsss->csr==$sag_job->csr){			$csrselectset="selected=selected";				}
				$dataSend .='<option value="'.$myrowsss->csr.'#'.$emailcsr.'" '.$csrselectset.'>'.$myrowsss->csr.'</option>';
			}
		}
       	$dataSend .='</select></td>';
	   

		$dataSend .='<td style="max-width:150px; overflow-wrap: break-word;">'.$sag_job->project_address_line1.'<br>'.
		$sag_job->project_state.'</td>';
		$dataSend .='<td style="max-width:150px; overflow-wrap: break-word;">'.$sag_job->job_name.'</td>';
		$dataSend .='<td>'.$sag_job->project_status.'</td>';
		$dataSend .='<td>'. date("m/d/Y g:i A", strtotime($sag_job->created_date) - 60 * 60 * 4) .'</td>';
		$dataSend .='<td>'.date("m/d/Y g:i A", strtotime($sag_job->modifydate) - 60 * 60 * 4).'</td>';
		$current_user = wp_get_current_user();

		$dataSend .='	<td class="center" >';

		$printUrl = esc_url(add_query_arg('job_id', $sag_job->id, get_permalink( get_page_by_title( 'Print PDF' ) )));
		$dataSend .='<a id="export_pdf_btn" target="_blank" class="btn btn-danger btn-sm" href="'.$printUrl.'" role="button">Pdf</a>';





		 $dataSend .=' </td>
            </tr>';
          
     }
               
    $dataSend .=' <tr>
              <td colspan="9" style="background:#fff"><div class="pagination pull-left" style="margin: 0">';
                
            
	$pages= ceil($counttotal/$showlimit);

	if($counttotal>50)
	{
		for($i=1;$i<=$pages;$i++)
		{	
			$class ="";
			if(($_REQUEST['pagesno']==$i) || ($i==1 && !$_REQUEST['pagesno']))		{		$class="class='active'";		}
			$siteurl=get_permalink($post->ID)."?pagesno=".$i;
			$dataSend .='  <a href="" id="pageno_'.$i.'" '.$class.'>'.$i.'</a>  ';
	
		}
	}

   $dataSend .=' </div></td></tr>';
			
	return json_encode(array('data'=>$dataSend));

 }
 
 public function getUsernameById($id){
	$user_info = get_userdata($id);
	return $user_info->user_login;
 }
function getParticipantsArr($participants){
	$participants = explode(',',$participants);
	$a = array();
	foreach($participants as $key => $participant){
	    $earray = array("EmailAddress" => array("Address" => $participant, "Name" => "test"), "Type" => "Required");
		array_push($a,$earray);
	}
	return $a;

}
function getValidDateTime($datetime){
	if($datetime == "0000-00-00 00:00:00" || $datetime == ""){
		$datetime = date('Y-m-d H:i:s');
	}
	return $datetime;
} 
function syncWithOutlook($token,$id,$delete=''){
	global $wpdb;
    $user_id = get_current_user_id();
	
	$eventData = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."sag_events WHERE id = '$id'" ,ARRAY_A);
	$job = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."sag_jobs where id ='".$eventData['job_id']."'" ,ARRAY_A);
	$stdate = $this->getValidDateTime(date('Y-m-d\TH:i:s',strtotime($eventData['start_time'])));
	$eddate = $this->getValidDateTime(date('Y-m-d\TH:i:s',strtotime($eventData['end_time'])));
	$subject = $job['quote_number'].' '.$eventData['name'];
	$notes = $eventData['notes'];
	
	//$notes = str_replace('\n','<p>&nbsp;</p>',$this->getNotesContent($eventData['job_id'],$eventData['notes']));
	$notes = $this->getNotesContent($eventData['job_id'],$eventData['notes']);
	/*
	echo '<br><br>';
	echo $subject;
	echo $notes;
	die;
    */
    $tz = date_default_timezone_get();
	$data = array(
				"Subject"=>"$subject",
				"Body" => array("ContentType" => "HTML","Content" => $notes),
				"Start" => array("DateTime" => "$stdate","TimeZone" => $tz),
				"End" => array("DateTime" => "$eddate","TimeZone" => $tz)
			);
	if(!empty($eventData['participants']) && $eventData['participants']!=''){
		$a = $this->getParticipantsArr($eventData['participants']);
		$data["Attendees"] = $a;
	}
	$delete_status = 0;
	$data_string = json_encode($data);
	if($delete!=''){
		$delete_status = 1;
	}
	if(!empty($eventData['sync_id']) && $eventData['sync_id']!=''){
		$delete_status = 1;
	}
	if($delete_status){
		//delete the previous event
		$ch = curl_init('https://outlook.office.com/api/v2.0/me/events/'.$eventData['sync_id']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(
				'Authorization:Bearer '.$token,
				'Content-Type: application/json'
			)
		);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$event_delete = curl_exec($ch);
		curl_close ($ch);
    	if($delete!=''){
			$wpdb->delete($wpdb->prefix."sag_events", array('id' => $id));
			$eventData = array();
			echo 'Event Deleted!';
		}
	}
	//echo '<br><br>';
	//print_r($data_string);
	//die;
	if(!empty($eventData)){
		//create event
		$ch = curl_init('https://outlook.office.com/api/v2.0/me/events');
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);        
		curl_setopt($ch, CURLOPT_HTTPHEADER, 
			array(
				'Authorization:Bearer '.$token,
				'Content-Type: application/json'
			)
		);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);				
		$event_curl = curl_exec($ch);
		$event_res = json_decode($event_curl);
		$event_create = 1;
		$wpdb->update($wpdb->prefix."sag_events", array('sync_id' => $event_res->Id ), array('id' => $eventData['id'] ));	
	}
}
  public function save_modal_on_job_list(){
    //date_default_timezone_set('America/New_York');

 	global $wpdb;	global $post;
    $table = $wpdb->prefix . 'sag_events';
    $user_id = get_current_user_id();
	$current_user = wp_get_current_user();
	$form_data = array();
	parse_str($_POST['form_data'], $form_data);
	//$form_data = $_POST['form_data'];


	$event_type = esc_sql($form_data['event_type']);
	$arr = array( 
				'type' => $event_type,
				'notes' => esc_sql($form_data['event_notes']),
				'user_id' => $current_user->ID,
				'job_id' => esc_sql($form_data['jobid'])
			);
	if($event_type=='0'){
		$arr['name'] = esc_sql($form_data['event_name']);
		$arr['participants'] = esc_sql($form_data['event_participants']); 
	    $arr['start_time'] = date('Y-m-d H:i:s', strtotime($form_data['event_start_time']));
        //$arr['start_time'] = $this->getValidDateTime(esc_sql($form_data['event_start_time']));
	    //$arr['end_time'] = $this->getValidDateTime(esc_sql($form_data['event_end_time'])); 
	}
	else{
		$arr['start_time'] = $this->getValidDateTime('');
		//$arr['end_time'] = $this->getValidDateTime(''); 
	}

	
	$strtotime = strtotime($arr['start_time'])+900;
	$arr['end_time'] = $this->getValidDateTime(date('Y-m-d H:i:s', $strtotime));
	
//    var_dump($form_data,$form_data['event_start_time_edit'],$form_data['event_start_time'],$arr);die;	

    //string(19) "2018-05-17 11:52 am" bool(false) string(19) "2018-05-19 17:00 pm" string(19) "1970-01-01 00:00:00"


     //string(19) "2018-05-17 10:54 am" int(1526554440) string(19) "2018-05-29 18:00 pm" string(19) "1970-01-01 00:00:00"

 
	$refreshRow = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."sag_refresh_tokens WHERE user_id ='$user_id'" ); 
	$refresh_token = $refreshRow->refresh_token;

	if(!empty($refreshRow) && $refreshRow!=null){ 
		$URL = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
		$CURLOPT_POSTFIELDS = 'client_id='.APP_ID.'
								&client_secret='.APP_PASSWORD.'
								&grant_type=refresh_token						
								&refresh_token='.$refresh_token.'
								&scope=https%3A%2F%2Foutlook.office.com%2Fcalendars.readwrite+openid+offline_access';
		$server_output = getCurlResponse($URL, $CURLOPT_POSTFIELDS);
		$result = json_decode($server_output);
		if(!empty($result)){
			$wpdb->update( 
				$wpdb->prefix."sag_refresh_tokens", 
				array(
					'refresh_token' => $result->refresh_token,
					'token_expires' => $result->expires_in,
					'modified' => date('Y-m-d H:i:s')
				), 
				array( 'user_id' => $user_id )
			);      
		}
		$token = $result->access_token;
	}
	else{
		echo 'Please sync outlook on settings page for auto sync!<br>';
	}
	
	if(isset($form_data['event_id_for_delete']) && $form_data['event_id_for_delete']!=''){
		$event_id_for_delete = esc_sql($form_data['event_id_for_delete']);
		if(isset($token) && $token!='' && $event_type=='0'){$this->syncWithOutlook($token,$event_id_for_delete,'delete');}
		else{
			$wpdb->delete($table, array('id' => $event_id_for_delete));
			echo 'Event Deleted!';
		}
		echo '<script>setTimeout(function(){jQuery(".events_table_popup").find("tr#'.$event_id_for_delete.'").remove();jQuery("#popup_event_popup").modal("hide")}, 3000);</script>';
	}
	else if(isset($form_data['event_edit_id']) && $form_data['event_edit_id']!=''){
	    $arr['modified'] = $this->getValidDateTime(''); 
		$event_edit_id = esc_sql($form_data['event_edit_id']);
		$st = $wpdb->update($table, $arr, array('id' => $event_edit_id ));
		echo '<script>setTimeout(function(){loadEventModalPopup('.$arr["job_id"].')}, 3000);</script>';
		echo 'Event Updated!';
		if(isset($token) && $token!='' && $event_type=='0'){$this->syncWithOutlook($token,$event_edit_id);}
	}
	else{
	    $arr['created'] = $this->getValidDateTime(''); 
		$st = $wpdb->insert($table, $arr);
		echo 'Event Added!';
		echo '<script>setTimeout(function(){loadEventModalPopup('.$arr["job_id"].')}, 3000);</script>';
		if(isset($token) && $token!='' && $event_type=='0'){$this->syncWithOutlook($token,$wpdb->insert_id);}
		if($event_type!='0'){$this->sendEventMail($wpdb->insert_id);}
	}
	//echo print_r($st);
 }
 public function open_modal_on_job_list(){
 	global $wpdb;	global $post;
    $table_sag_events = $wpdb->prefix . 'sag_events';
    $user_id = get_current_user_id();
	$current_user = wp_get_current_user();
    if(isset($_POST['jobid'])){
		$jobid = $_POST['jobid'];
	}else{
		$jobid='';
	}
	//$resultArr = $wpdb->get_results( "SELECT * FROM $table_sag_events WHERE job_id='$jobid' AND user_id='$current_user->ID' ORDER BY id DESC" );
	$resultArr = $wpdb->get_results( "SELECT * FROM $table_sag_events WHERE job_id='$jobid' ORDER BY id DESC" );
	$html = '<div class="modal-header">
			<button type="button" class="close pull-right" data-dismiss="modal">&times;</button>
			<a class="btn btn-danger right" id="add_event" jobid="'.$jobid.'" data-toggle="modal" data-target="#popup_event_popup" href="javascript:void(0);">+ Add Event</a>
			<h4 class="modal-title">Events</h4>
			</div><div class="clearfix"></div>';
	if(!empty($resultArr)){
	$html .= '<table class="events_table_popup" border="1" cellpadding="4" cellspacing="4">';
	foreach($resultArr as $result){
		#$user = get_user_by( 'ID', $result->user_id );
		$uname = $this->getUsernameById($result->user_id);
		$col_text = '<strong>'.$uname.'</strong>';
		if($result->type=='0'){
			$col_text.= ' setup an appointment';
			$icon = '<i class="fa fa-calendar" aria-hidden="true"></i>';
		}
		else if($result->type=='1'){
			$col_text.= ' made a call';
			$icon = '<i class="fa fa-phone" aria-hidden="true"></i>';
		}
		else if($result->type=='2'){
			$col_text.= ' added a note';
			$icon = '<i class="fa fa-file-o" aria-hidden="true"></i>';
		}
		else if($result->type=='3'){
			$col_text.= ' added a text message';
			$icon = '<i class="fa fa-comment-o" aria-hidden="true"></i>';
		}
		else if($result->type=='4'){
			$col_text.= ' added an email';
			$icon = '<i class="fa fa-envelope-o" aria-hidden="true"></i>';
		}
		$col_text.= '<br><strong>Notes</strong><br>'.$result->notes;
		$html .= '	<tr id="'.$result->id.'">
						<td>'.$icon.'</td>
						<td>'.$col_text.'</td>';
	//	if($result->type=='0'){				
				$html .= '<td><strong>'.date('g:i a m/d/y',strtotime($result->start_time)).'</strong></td>';
	/*	}
		else{
				$html .= '<td><strong>&nbsp;</strong></td>';
		}*/
		
				$html .= '<td>'.date('m/d/y',strtotime($result->created)).'</td>';

		if($current_user->ID==$result->user_id){				
				$html .= '<td><i class="fa fa-pencil edit-event edit_event_popup" aria-hidden="true" eventid="'.$result->id.'"></i>&nbsp;&nbsp;<i class="fa fa-trash delete_event_popup" aria-hidden="true" rel="delete" eventid="'.$result->id.'" data-toggle="modal" data-target="#event_popup"></i></td>';
		}
		else{
				$html .= '<td><strong>&nbsp;</strong></td>';
		}
				$html .= '</tr>';

	}
	$html .= '</table>';
	}
	else{
		$html .= '<div align="center" class="no-event">There are currently no events.<br>Click the add event button above to get started!</div>';
	}
    echo $html;
 }

 public function change_accepted()
 {
 	global $wpdb;
	$sagjobs=$wpdb->prefix."sag_jobs";
	$jobid=$_POST['job_id'];
	$update="update $sagjobs set accepted=1 where id='$jobid' ";
	$wpdb->query($update);
	echo "1";
	exit;


 }
/*
public function chagnesales_name()
{
  
	global $wpdb;
	
	$frmEmail='';
	
	$frmEmail = EMAILFROM;
	$blankAry = array('test','na','TG','rs');
	$emailset =array();
	$link=get_permalink(get_option("acceptjobpage"));

	$sagjobs    = $wpdb->prefix."sag_jobs";
	$csrset     = $_POST['csrname'];
	$csrexplode = explode("#",$csrset);
 	$salesname     = $_POST['salesname'];
	$ownername     = $_POST['ownername'];
	$jobid         = $_POST['job_id'];
	$select        = "select * from $sagjobs where id = $jobid ";
	$resultsget    = $wpdb->get_results($select);

	$salesnameprev = $resultsget[0]->salesman_name;
	$ownernameprev = $resultsget[0]->owner_name;
	$job_version   = $resultsget[0]->job_version;



	$csrprev=$resultsget[0]->csr;
	$quote_number=$resultsget[0]->quote_number;
	$project_description=$resultsget[0]->job_name;
	$project_address_line1=$resultsget[0]->project_address_line1;
	$project_address_line2=$resultsget[0]->project_address_line2;
	$project_city=$resultsget[0]->project_city;
	$project_state=$resultsget[0]->project_state;
	$project_postal_code=$resultsget[0]->project_postal_code;
	$onsite_contact=$resultsget[0]->onsite_contact;
	$contact_number=$resultsget[0]->contact_number;
	$contact_email=$resultsget[0]->contact_email;
	$accepted=$resultsget[0]->accepted;


	
	$salesManMapData = $this->salesManMapData();
	$salesmanIdMap =$salesManMapData['salesmanIdMap'];
	$salesmanEmailMap =$salesManMapData['salesmanEmailMap'];
	$history=$wpdb->prefix."history";

			
	$bodyTxt='';

	
	$bodyTxt .= "Sales Rep A : ".$ownername."\n";
	$bodyTxt .= "Sales Rep B : ".$salesname."\n";
	$bodyTxt .= "CSR : ".$csrexplode[0]."\n";
	$bodyTxt .="Job Description: ".$project_description."\n"."\n";
	$bodyTxt .= "Project Address"."\n";
	$bodyTxt .= $project_address_line1."\n";
	$bodyTxt .= $project_address_line2."\n";
	$bodyTxt .= $project_city.", ".$project_state."-".$project_postal_code."\n"."\n";
	$bodyTxt .= "Project Contact"."\n";
	$bodyTxt .= $onsite_contact."\n";
	$bodyTxt .= $contact_number."\n";
	$bodyTxt .= $contact_email."\n";
	
	$emaillist[]=$salesmanEmailMap[$ownername];  

	$salesManFlag= false;
	
	if($salesname!='na'){		$emaillist[]=$salesmanEmailMap[$salesname]; 				}
	
	$headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" ;	
	
	$headers .="CC: ";
			
	$stevenMailId=false;
	foreach($emaillist as $email){
		if($email=='steven@salbertglass.com'){	        	$stevenMailId=true;	    }
	}
	
			//echo $salesmanEmailMap[$csrexplode[0]];die;
	if(	$stevenMailId==false){
		if($csrexplode[1]=='steven@salbertglass.com'){       	$headers .='steven@salbertglass.com';		        
		}else{	        	$headers .=$csrexplode[1].",appmanager@salbertglass.com";		    }
		
	}else{
		  if($csrexplode[1]!='steven@salbertglass.com'){		$headers .=$csrexplode[1];			  }
		}
			
	if(count($emaillist )>1){			$emaillistTo= $emaillist[0].",".$emaillist[1];		}
	else{					$emaillistTo= $emaillist[0];					}
					

	if((trim($salesnameprev) != trim($salesname)) || (trim($ownernameprev) != trim($ownername)))
		{
			$link=get_permalink(get_option("acceptjobpage"));
			$newsalesManForHistory = ''; $previousUserVal='';
			if(trim($ownernameprev) != trim($ownername)){
				$previousUserVal =$ownernameprev;
				$newsalesManForHistory =$ownername;
			
			}else{
				$previousUserVal =$salesnameprev;
				$newsalesManForHistory  =$salesname;
			}
			
			$subject=  $quote_number. " has been re-assigned from ".$previousUserVal. " and is waiting to be accepted." ;
			$message = $quote_number. " has been re-assigned from ".$previousUserVal. " and is waiting to be accepted."."\n"."\n";
			$message .= $link . "?job_id=" . $jobid."\n"."\n";
			$message .=	$bodyTxt;
			
			sendmailmode($emaillistTo,$subject,$message,$headers);
			$wpdb->insert($table_history,array("jobid"=>$job_id,"salesname"=>$newsalesManForHistory,"date"=>date("Y-m-d H:i:s")));
			$tablest= $wpdb->prefix."sag_jobs";
			$update="update $tablest set accepted='0' where id='$job_id'";
			$wpdb->query($update);
			
			
			
		
		}
				

	

		if(trim($csrprev) != trim($csrexplode[0]))
		{ 	
		
			$subject=   $quote_number. " has been re-assigned from ".$csrprev ;
			$body = $quote_number. " has been re-assigned from ".$csrprev."\n"."\n";
			$body .= get_option("siteurl") . "/add-job/?job_id=" . $jobid."\n"."\n";
			$body .=	$bodyTxt;
			sendmailmode($emaillistTo,$subject,$body,$headers);
	
		}


		  $update="update $sagjobs set  owner_name ='$ownername',salesman_name ='$salesname', csr='$csrexplode[0]',accepted='$accepted' where id='$jobid'";
		$wpdb->query($update);
		echo "1";
		die();
		

}
*/



public function chagnesales_name()
{
  
	global $wpdb;
	
	$frmEmail='';
	
//	if(get_option("select_nmode")==2){	$frmEmail = 'noreply@salbertglass.com';	}else{	$frmEmail = 'development@salbertglass.com';		}
	$frmEmail = EMAILFROM;
	$blankAry = array('test','na','TG','rs');
	/*e-mail body part*/
	
	
	$emailset =array();
	$link=get_permalink(get_option("acceptjobpage"));

	/*e-mail body part*/
	$sagjobs    = $wpdb->prefix."sag_jobs";
	$csrset     = $_POST['csrname'];
	$csrexplode = explode("#",$csrset);
 	$salesname     = $_POST['salesname'];
	$ownername     = $_POST['ownername'];
	$jobid         = $_POST['job_id'];
	$select        = "select * from $sagjobs where id = $jobid ";
	$resultsget    = $wpdb->get_results($select);

	$salesnameprev = $resultsget[0]->salesman_name;
	$ownernameprev = $resultsget[0]->owner_name;
	$job_version   = $resultsget[0]->job_version;



	$csrprev				=	$resultsget[0]->csr;
	$quote_number			=	$resultsget[0]->quote_number;
	$project_description	=	$resultsget[0]->job_name;
	$project_address_line1	=	$resultsget[0]->project_address_line1;
	$project_address_line2	=	$resultsget[0]->project_address_line2;
	$project_city			=	$resultsget[0]->project_city;
	$project_state			=	$resultsget[0]->project_state;
	$project_postal_code	=	$resultsget[0]->project_postal_code;
	$onsite_contact			=	$resultsget[0]->onsite_contact;
	$contact_number			=	$resultsget[0]->contact_number;
	$contact_email			=	$resultsget[0]->contact_email;
	$accepted				=	$resultsget[0]->accepted;
	$project_status			=	$resultsget[0]->project_status;
	$whyreason				=	$resultsget[0]->whyreason;
	
	$salesManMapData = $this->salesManMapData();
	$salesmanIdMap =$salesManMapData['salesmanIdMap'];
	$salesmanEmailMap =$salesManMapData['salesmanEmailMap'];
	$history=$wpdb->prefix."history";

			
	$bodyTxt='';

	
	$bodyTxt .= "Sales Rep A : ".$ownername."\n";
	$bodyTxt .= "Sales Rep B : ".$salesname."\n";
	$bodyTxt .= "CSR : ".$csrexplode[0]."\n";
	$bodyTxt .="Job Description: ".$project_description."\n"."\n";
	$bodyTxt .= "Project Address"."\n";
	$bodyTxt .= $project_address_line1."\n";
	$bodyTxt .= $project_address_line2."\n";
	$bodyTxt .= $project_city.", ".$project_state."-".$project_postal_code."\n"."\n";
	$bodyTxt .= "Project Contact"."\n";
	$bodyTxt .= $onsite_contact."\n";
	$bodyTxt .= $contact_number."\n";
	$bodyTxt .= $contact_email."\n";
	
	$emaillist[]=$salesmanEmailMap[$ownername];  

	$salesManFlag= false;
	
	if($salesname!='na'){		$emaillist[]=$salesmanEmailMap[$salesname]; 				}
	
	$headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" ;	
	
	$headers .="CC: ";
			
	$stevenMailId=false;
	foreach($emaillist as $email){
		if($email=='steven@salbertglass.com'){	        	$stevenMailId=true;	    }
	}
	
			//echo $salesmanEmailMap[$csrexplode[0]];die;
	if(	$stevenMailId==false){
		if($csrexplode[1]=='steven@salbertglass.com'){       	$headers .='steven@salbertglass.com';		        
		}else{	        	$headers .=$csrexplode[1].",appmanager@salbertglass.com";		    }
		
	}else{
		  if($csrexplode[1]!='steven@salbertglass.com'){		$headers .=$csrexplode[1];			  }
		}
			
	if(count($emaillist )>1){			$emaillistTo= $emaillist[0].",".$emaillist[1];		}
	else{					$emaillistTo= $emaillist[0];					}
					

	if((trim($salesnameprev) != trim($salesname)) || (trim($ownernameprev) != trim($ownername)) || (trim($csrprev) != trim($csrexplode[0])))
		{
			$link=get_permalink(get_option("acceptjobpage"));
			$newsalesManForHistory = ''; $previousUserVal='';
			if(trim($ownernameprev) != trim($ownername)){
				$previousUserVal =$ownernameprev;
				$newsalesManForHistory =$ownername;
			
			}else{
				$previousUserVal =$salesnameprev;
				$newsalesManForHistory  =$salesname;
			}
			
			if($project_status=='new ticket'){
				
					$message='';
					$message = $quote_number." A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted."."\n\n";
					$message .= $link . "?job_id=" . $jobid."\n"."\n";
					
					$message .="Job Description: ".$project_description."\n"."\n";
					$message .= "Sales Rep A : ".$ownername."\n";
					if($salesname!='na'){	 	$message .= "Sales Rep B : ".$salesname."\n";		}
					$message .= "CSR : ".$csrexplode[0]."\n\n";
					$message .= "Project Address"."\n";
					$message .= $project_address_line1."\n";
					$message .= $project_address_line2."\n";
					$message .= $project_city.", ".$project_state."-".$project_postal_code."\n"."\n";
					$message .= "Project Contact"."\n";
					$message .= $onsite_contact."\n";
					$message .= $contact_number."\n";
					$message .= $contact_email."\n";
					
					$subject = $quote_number.'  A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted.';
				
				}else if($project_status=='research'){
			
					$subject =  'Research Required ' . $quote_number ;
					$message = $quote_number." has been SAVED.  MORE RESEARCH TO BE DONE.". "\n"."\n";
					$message .= $link . "?job_id=" . $jobid."\n"."\n";
					$message .=	$bodyTxt;

					}
					else if($project_status=='na' || $project_status=='void cancel' || $project_status=='void duplicate' || $project_status=='void lost'){
							$message= ''.$quote_number." is no longer applicable. "."\n"."\n";
							if($project_status!='na'){
							    $message .= ucfirst($project_status)." Reason"."\n";
							}
							else{
							    $message .= "N/A Reason"."\n";
							}
							$message .= $whyreason."\n\n";
							$message .= get_option("siteurl")."/add-job/?job_id=".$jobid."\n"."\n";
							$message .=	$bodyTxt;
							$subject =  $quote_number." is no longer applicable. ";
						
					}else{
			
			
					$subject=  $quote_number. " has been re-assigned from ".$previousUserVal. " and is waiting to be accepted." ;
					$message = $quote_number. " has been re-assigned from ".$previousUserVal. " and is waiting to be accepted."."\n"."\n";
					$message .= $link . "?job_id=" . $jobid."\n"."\n";
					$message .=	$bodyTxt;
				}
				
				//echo $message;
				//die;

			sendmailmode($emaillistTo,$subject,$message,$headers);
		
			$wpdb->insert($table_history,array("jobid"=>$job_id,"salesname"=>$newsalesManForHistory,"date"=>date("Y-m-d H:i:s")));
			$tablest= $wpdb->prefix."sag_jobs";
			$update="update $tablest set accepted='0' where id='$job_id'";
			$wpdb->query($update);
		
		}
				


		  $update="update $sagjobs set  owner_name ='$ownername',salesman_name ='$salesname', csr='$csrexplode[0]',accepted='$accepted' where id='$jobid'";
		$wpdb->query($update);
		echo "1";
		die();
		

}


public function removeslashes($data){
  foreach ($data as $key => $value) {

	    
	     if (is_array($value)) {
            $data[$key] = removeslashes($data[$key]);
        }
        else{
        	$data[$key]=str_replace('\\','',$data[$key]);
		}
	}
	
	return $data;
}


function stripslashes_deep($value)
{
    $value = is_array($value) ?
                array_map('stripslashes_deep', $value) :
                          checkStr($value );

    return $value;
}



function checkStr($strVal ){
	
    $strVal = str_replace('\\', '', $strVal);

	if(strpos($strVal, "\'")>0) {
		
		checkStr($strVal );
	}
	  $strVal = str_replace('\"', '', $strVal);
	return $strVal;
}


  public function get_participants(){
	global $wpdb; // this is how you get access to the database
	$table= $wpdb->prefix."sag_events";
	$job_id = intval( $_POST['job_id'] );

  	if(isset($job_id) && ($job_id!='')){
		$partArr = $wpdb->get_row( "SELECT salesman_name,owner_name,csr FROM ".$wpdb->prefix."sag_jobs WHERE id = '".$job_id."' " ,ARRAY_A);
		//print_r($partArr);
		//die;
		$partArr = array_diff($partArr, array("na"));
		$rsArr = array("appmanager@salbertglass.com");
		foreach($partArr as $part){
			$subPart = $wpdb->get_row( "SELECT user_email,user_nicename FROM ".$wpdb->prefix."users WHERE user_login LIKE '%$part%' OR user_nicename LIKE '%$part%'" ,ARRAY_A);
			if($subPart['user_email']!= NULL){
				array_push($rsArr,$subPart['user_email']);
			}
		}
		$csmail=$this->getcsrmail($partArr['csr']);
		if(!in_array($csmail,$rsArr)){
			array_push($rsArr,$csmail);
		}
		

		$salesmail=$this->getsalesmanmail($partArr['salesman_name']);
		if(!in_array($salesmail,$rsArr)){
			array_push($rsArr,$salesmail);
		}
		
		
		$contact_email = $wpdb->get_row( "SELECT contact_email FROM ".$wpdb->prefix."sag_jobs WHERE id = '".$job_id."' " ,ARRAY_A);
		array_push($rsArr,$contact_email['contact_email']);
		
		if(!in_array('SLA',$partArr)){
			if (($key = array_search('steven@salbertglass.com', $rsArr)) !== false) {
				unset($rsArr[$key]);
			}
		}
		else{
			if (($key = array_search('appmanager@salbertglass.com', $rsArr)) !== false) {
				unset($rsArr[$key]);
			}
		}

		/*if (($key = array_search('steven@salbertglass.com', $rsArr)) !== false) {
			unset($rsArr[$key]);
		}*/
				
		$rsArr=array_filter($rsArr);
		$rsArr= array_unique($rsArr);
		$event_participants = implode(", ",$rsArr);
	}
	
	
    echo $event_participants;
	wp_die();
  }
    
    public function get_notes_content(){
		global $wpdb;
		$job_id = $_POST['job_id'];
		$value=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."sag_jobs where id ='".$job_id."'");
		$content .= get_option("siteurl")."/add-job/?job_id=".$value->id."\n\n";
		$content .= "Notes by ".$value->owner_name."\n\n";
		$content .= "Sales Rep A : ".$value->owner_name."\n";
		$content .= "Sales Rep B : ".$value->salesman_name."\n";
		$content .= "CSR : ".$value->csr."\n\n";
		$content .= "Project Address :" ."\n";
		$content .= $value->project_address_line1 ."\n";
		$content .= $value->project_address_line2 ."\n";
		$content .= $value->project_city.",".  $value->project_state." - ".$value->project_postal_code."\n\n";
		$content .= "Project Contact :"."\n";
		$content .= $value->onsite_contact."\n";
		$content .= $value->contact_number."\n";
		$content .= $value->contact_email."\n";
		echo $content;
	}

   


  public function edit_event_item(){
	global $wpdb; // this is how you get access to the database
	$table= $wpdb->prefix."sag_events";
	$event_id = intval( $_POST['event_id'] );
	$resultRow = $wpdb->get_row( "SELECT * FROM $table WHERE id ='".$event_id."'" );
	#print_r($resultRow);
	$style_cls = ($resultRow->type==0) ? '' : 'style="display:none;"';
	?>
    <div class="form-group">
        <div class="col-md-6 col-sm-6">
          <label for="event_type">Event Type</label>
          <select id="event_type" class="form-control" name="event_type">
            <option value="0" <?php if($resultRow->type==0){echo ' selected="selected"';}?>>Appointment</option>
            <option value="1" <?php if($resultRow->type==1){echo ' selected="selected"';}?>>Call</option>
            <option value="2" <?php if($resultRow->type==2){echo ' selected="selected"';}?>>Notes</option>
            <option value="3" <?php if($resultRow->type==3){echo ' selected="selected"';}?>>Text Message</option>
            <option value="4" <?php if($resultRow->type==4){echo ' selected="selected"';}?>>Email</option>
          </select>
        </div>
        <div class="col-md-6 col-sm-6 event_name" <?php echo $style_cls;?>>
          <label for="event_name">Event Name</label>
          <input type="text" class="form-control" id="event_name" name="event_name" value="<?php echo $resultRow->name;?>"  />
        </div>
        
    </div>
    <div class="form-group">
        <div class="col-md-12 col-sm-12 event_start_time" <?php echo $style_cls;?>>
          <label for="event_start_time">Start Time</label>
          <input type="text" class="form-control" id="event_start_time_edit" placeholder="yyyy-mm-dd hh:mm:ss" name="event_start_time" value="<?php echo date('Y-m-d h:i a', strtotime($resultRow->start_time));?>"/>
        </div>
        <!--<div class="col-md-6 col-sm-6 event_end_time" <?php echo $style_cls;?>>
          <label for="event_end_time">End Time</label>
          <input type="text" class="form-control" id="event_end_time_edit" placeholder="yyyy-mm-dd hh:mm:ss" name="event_end_time" value="<?php echo $resultRow->end_time;?>"  />
        </div>-->
    </div>
    <div class="form-group">
        <div class="col-md-12 col-sm-12 event_participants" <?php echo $style_cls;?>>
          <label for="event_participants">Participants</label>
          <textarea id="event_participants_edit" name="event_participants" class="form-control"><?php echo $resultRow->participants;?></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 col-sm-12">
          <label for="event_notes">Notes</label>
          <textarea id="event_notes_edit" name="event_notes" class="form-control"><?php echo str_replace('\r\n', "\r\n", $resultRow->notes);?></textarea>
        </div>
    </div>
    <input type="hidden" name="event_edit_id" value="<?php echo $resultRow->id;?>" />
    <input type="hidden" name="jobid" value="<?php echo $resultRow->job_id;?>" />
    <input type="hidden" name="event_job_id" value="<?php echo $resultRow->job_id;?>" />
    <script>
		jQuery(document).ready(function(){
			jQuery('#event_start_time_edit').datetimepicker ({
					dateFormat: "yy-mm-dd",
					timeFormat: "hh:mm tt",
					controlType: 'select',
					oneLine: true,
					/*onSelect: function( selectedDate ) {
						jQuery('#event_end_time_edit').datetimepicker("option","minDate",selectedDate);
					}*/
				});
			/*jQuery('#event_end_time_edit').datetimepicker ({
					dateFormat: "yy-mm-dd",
					timeFormat: "HH:mm:ss",
					controlType: 'select',
					oneLine: true
				});*/
		});
	</script>
    <?php
        echo $whatever; 
	wp_die(); // this is required to terminate immediately and return a proper response
  }

  public function add_job_item(){




	session_start();
	global $wpdb;
	$_SESSION['created_new_job']=false;
	 //print_r($_POST);die;
	

	$salesManMapData = $this->salesManMapData();

	$salesmanIdMap =$salesManMapData['salesmanIdMap'];
	$salesmanEmailMap =$salesManMapData['salesmanEmailMap'];
	//return array('salesmanIdMap'=>$salesmanIdMap,'salesmanEmailMap'=>$salesmanEmailMap);
	$blankAry = array('test','na','TG','rs');
    //sleep(5);
 //   print_r($_POST["data"] );
     
   $job_item = stripslashes_deep( $_POST["data"]);
    // $job_item = $this->removeslashes($_POST["data"]);
    $bypassArr = array('daylight_width','daylight_height','go_width','go_height');
    for($j=0;$j<50;$j++){
      for($i=0;$i<count($job_item['gpd_items']);$i++){
         if(!in_array($job_item['gpd_items'][$i],$bypassArr)){
             $job_item['gpd_items'][$i] = stripslashes_deep( $job_item['gpd_items'][$i]);
         }
     }
    }
    //echo "after remove ";
 //    print_r( $job_item );die;
   // $job_item = $_POST["data"];


    $zipcode = $job_item["project_postal_code"];

	if(!empty($zipcode))
	{
		$url     = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
		$details = file_get_contents($url);
		$result  = json_decode($details,true);
	}


    $gpd_items = isset($job_item["gpd_items"]) ? $job_item["gpd_items"] : array();
    $agd_items = isset($job_item["agd_items"]) ? $job_item["agd_items"] : array();
    $lat=$result['results'][0]['geometry']['location']['lat'];
    $lng=$result['results'][0]['geometry']['location']['lng'];
	$job_id = isset($job_item["id"]) ? intval(htmlspecialchars($job_item["id"])) : 0;
    $salesman_name = isset($job_item["salesman_name"]) ? htmlspecialchars($job_item["salesman_name"]) : "";
	$owner_name = isset($job_item["owner_name"]) ? htmlspecialchars($job_item["owner_name"]) : "";
    $quote_number = isset($job_item["quote_number"]) ? htmlspecialchars($job_item["quote_number"]) : "";
	$secondary_quote = isset($job_item["secondary_quote"]) ? htmlspecialchars($job_item["secondary_quote"]) : "";
    $job_name = isset($job_item["job_name"]) ? ($job_item["job_name"]) : "";
	$csr_description = isset($job_item["csr_description"]) ? ($job_item["csr_description"]) : "";
    $onsite_contact = isset($job_item["onsite_contact"]) ? htmlspecialchars($job_item["onsite_contact"]) : "";
    $contact_number = isset($job_item["contact_number"]) ? htmlspecialchars($job_item["contact_number"]) : "";
    $contact_email = isset($job_item["contact_email"]) ? htmlspecialchars($job_item["contact_email"]) : "";
    $person_seen = isset($job_item["person_seen"]) ? htmlspecialchars($job_item["person_seen"]) : "";
    $csr = isset($job_item["csr"]) ? htmlspecialchars($job_item["csr"]) : "";
    $project_address_line1 = isset($job_item["project_address_line1"]) ? htmlspecialchars($job_item["project_address_line1"]) : "";
    $project_address_line2 = isset($job_item["project_address_line2"]) ? htmlspecialchars($job_item["project_address_line2"]) : "";
    $project_email = isset($job_item["project_email"]) ? htmlspecialchars($job_item["project_email"]) : "";
    $project_city = isset($job_item["project_city"]) ? htmlspecialchars($job_item["project_city"]) : "";
    $project_state = isset($job_item["project_state"]) ? htmlspecialchars($job_item["project_state"]) : "";
    $project_postal_code = isset($job_item["project_postal_code"]) ? htmlspecialchars($job_item["project_postal_code"]) : "";
    $billing_address_line1 = isset($job_item["billing_address_line1"]) ? htmlspecialchars($job_item["billing_address_line1"]) : "";
    $billing_address_line2 = isset($job_item["billing_address_line2"]) ? htmlspecialchars($job_item["billing_address_line2"]) : "";
    $billing_email = isset($job_item["billing_email"]) ? htmlspecialchars($job_item["billing_email"]) : "";
    $billing_city = isset($job_item["billing_city"]) ? htmlspecialchars($job_item["billing_city"]) : "";
    $billing_state = isset($job_item["billing_state"]) ? htmlspecialchars($job_item["billing_state"]) : "";
    $billing_postal_code = isset($job_item["billing_postal_code"]) ? htmlspecialchars($job_item["billing_postal_code"]) : "";
    $project_description = isset($job_item["project_description"]) ? htmlspecialchars($job_item["project_description"]) : "";
    $project_status = isset($job_item["project_status"]) ? htmlspecialchars($job_item["project_status"]) : "";
    $labor_hours_type = isset($job_item["labor_hours_type"]) ? $job_item["labor_hours_type"] : "";
    $labor_number_of_hours = isset($job_item["labor_number_of_hours"]) ? $job_item["labor_number_of_hours"] : '';
    $labor_number_of_men = (isset($job_item["labor_number_of_men"])&& !empty($job_item["labor_number_of_men"])) ?  $job_item["labor_number_of_men"] : '';
    //add new line for notes injob
    $labur_notes = isset($job_item["labur_notes"]) ? $job_item["labur_notes"] : "";
    
	$addresslocationname = isset($job_item["addresslocation"]) ? $job_item["addresslocation"] : 0;
	$addressbilllname = isset($job_item["billtoid"]) ? $job_item["billtoid"] : 0;
	$whyreason = isset($job_item["whyreason"]) ? htmlspecialchars($job_item["whyreason"]) : "";
	$parking=isset($job_item["parking"]) ? htmlspecialchars($job_item["parking"]) : "";

	 /**sag-163***/
	$parking_other =isset($job_item["parking_other"]) ? htmlspecialchars($job_item["parking_other"]) : "";
	/**sag-163***/

    

    //serilize data in job
    
    $labor_hours_type = serialize($labor_hours_type);
    $labor_number_of_hours = serialize($labor_number_of_hours);
    $labor_number_of_men = serialize($labor_number_of_men);
    $labur_notes = serialize($labur_notes);
  
    //die("ddd"); 
    //end
	$job_name = str_replace('\\', '', $job_name) ;
	$csr_description = str_replace('\\', '', $csr_description);
	$onsite_contact = str_replace('\\', '',  $onsite_contact);
	$contact_number =str_replace('\\', '', $contact_number) ;
	$contact_email = str_replace('\\', '', $contact_email ) ;
	$person_seen = str_replace('\\', '',$person_seen)  ;
	$csr = str_replace('\\', '',$csr) ;
	$project_address_line1 = str_replace('\\', '',$project_address_line1 );
	$project_address_line2 = str_replace('\\', '', $project_address_line2) ;
	$project_email = str_replace('\\', '',$project_email) ;
	$project_city = str_replace('\\', '',$project_city );
	$project_state = str_replace('\\', '',$project_state  );
	$project_postal_code = str_replace('\\', '',$project_postal_code) ;
	$billing_address_line1 = str_replace('\\', '',$billing_address_line1 ) ;
	$billing_address_line2 = str_replace('\\', '',    $billing_address_line2 ) ;
	$billing_email = str_replace('\\', '', $billing_email );
	$billing_city = str_replace('\\', '',$billing_city) ;
	$billing_state = str_replace('\\', '',$billing_state) ;
	$billing_postal_code = str_replace('\\', '',$billing_postal_code) ;
	$project_description = str_replace('\\', '',$project_description) ;
    $project_status = str_replace('\\', '',$project_status) ;
    //comment
	//$labor_hours_type = str_replace('\\', '',$labor_hours_type) ;
    // $labor_number_of_hours = str_replace('\\', '',$labor_number_of_hours) ;
    // $labur_notes = str_replace('\\', '',$labur_notes) ;
	// $labor_number_of_men = str_replace('\\', '',$labor_number_of_men) ;
	$addresslocationname = str_replace('\\', '',$addresslocationname) ;
	$addressbilllname = str_replace('\\', '',$addressbilllname) ;
	$whyreason = str_replace('\\', '', $whyreason);
    $parking=str_replace('\\', '',$parking) ;
    /**sag-163***/
     $parking_other=str_replace('\\', '',$parking_other) ;
     /**sag-163***/

    $table_sag_jobs = $wpdb->prefix . 'sag_jobs';
    $table_gpd_items = $wpdb->prefix . 'gpd_items';
    $table_gdr_items = $wpdb->prefix . 'gdr_items';
    $table_gdn_items = $wpdb->prefix . 'gdn_items';
    $table_sdbg_items = $wpdb->prefix . 'sdbg_items';
    $table_sfd_items = $wpdb->prefix . 'sfd_items';
    $table_wmd_items = $wpdb->prefix . 'wmd_items';
	$table_notes_items = $wpdb->prefix. 'notes_items';
    $table_temp_upload = $wpdb->prefix. 'temp_upload';
    $table_nwdr_items = $wpdb->prefix. 'nwdr_items';
    $table_nwaluft_items = $wpdb->prefix. 'nwaluft_items';
    $table_nwaluff_items = $wpdb->prefix. 'nwaluff_items';
    $table_nwaluof_items = $wpdb->prefix. 'nwaluof_items';

    $table_hmnd_items = $wpdb->prefix. 'hmnd_items';
    $table_hmnf_items = $wpdb->prefix. 'hmnf_items';
    $table_patio_items = $wpdb->prefix.'nw_patio_items';
	$table_history = $wpdb->prefix. 'history';

	date_default_timezone_set('UTC');
	$created_date = date("Y-m-d H:i:s");
	$modifydate = date("Y-m-d H:i:s");
	
	$salesman_nameexp = explode("#",$salesman_name);
	$owner_nameexp = explode("#",$owner_name);
	$csrexp           = explode("#",$csr);


/*e-mail body part*/
	$bodyTxt='';
	$bodyTxt .= "Sales Rep A : ".$owner_nameexp[0]."\n";
	$bodyTxt .= "Sales Rep B : ".$salesman_nameexp[0]."\n";
	$bodyTxt .= "CSR : ".$csrexp[0]."\n"."\n";
	$bodyTxt .="Job Description: ".$job_name."\n"."\n";
	$bodyTxt .= "Project Address"."\n";
	$bodyTxt .= $project_address_line1."\n";
	$bodyTxt .= $project_address_line2."\n";
	$bodyTxt .= $project_city.", ".$project_state."-".$project_postal_code."\n"."\n";
	$bodyTxt .= "Project Contact"."\n";
	$bodyTxt .= $onsite_contact."\n";
	$bodyTxt .= $contact_number."\n";
	$bodyTxt .= $contact_email."\n";
/*e-mail body part*/

	$_SESSION['update_job']=false;
	$_SESSION['EMAILDATA']['csrexp']=$csrexp[1];
	$_SESSION['EMAILDATA']['job_id']=$job_id;
	$_SESSION['EMAILDATA']['job_name']=$job_name;
	$_SESSION['EMAILDATA']['quote_number']=$quote_number;
	//	$_SESSION['EMAILDATA']['hiddensett']=$job_item['hiddensett'];
	$_SESSION['EMAILDATA']['emailid']=$salesman_nameexp[1];
	$_SESSION['EMAILDATA']['emailid2']=$owner_nameexp[1];
	
	$_SESSION['EMAILDATA']['project_status']=$project_status;
	//$_SESSION['EMAILDATA']['csreamilid']=$owner_nameexp[1];
	$_SESSION['EMAILDATA']['owner_nameexp0']=$owner_nameexp[0];
	$_SESSION['EMAILDATA']['salesman_nameexp0']=$salesman_nameexp[0];
	$_SESSION['EMAILDATA']['project_address_line1']=$project_address_line1;
	$_SESSION['EMAILDATA']['project_address_line2']=$project_address_line2;
	$_SESSION['EMAILDATA']['project_city_state_pc']=$project_city.", ".$project_state."-".$project_postal_code;
	$_SESSION['EMAILDATA']['csrexp0']=$csrexp[0];
	
	
	$_SESSION['EMAILDATA']['onsite_contact']= $onsite_contact."\n";
	$_SESSION['EMAILDATA']['contact_number']=  $contact_number."\n";
	$_SESSION['EMAILDATA']['contact_email']=   $contact_email."\n";


	$frmEmail='';
	
//	if(get_option("select_nmode")==2){	$frmEmail = 'noreply@salbertglass.com';	}else{	$frmEmail = 'development@salbertglass.com';		}
	$frmEmail=EMAILFROM;	

    if ($job_id <= 0) {
		$_SESSION['created_new_job']=true;
		$_SESSION['EMAILDATA']['job_version']=1;
      //-------------------------------------------------
      //---------------INSERTING THE JOB-----------------
      //-------------------------------------------------

		$user_id = get_current_user_id();
		$wpdb->show_errors();
		$rows_affected = $wpdb->insert(
        $table_sag_jobs,
			array(
			'salesman_id'   => $salesmanIdMap[$salesman_nameexp[0]],
			'salesman_name' => $salesman_nameexp[0],
			'owner_name' =>  $owner_nameexp[0],
			
			'quote_number'  => $quote_number,
			'secondary_quote'  => $secondary_quote,
			'job_name'      => $job_name,
			'csr_description'      => $csr_description,
			'onsite_contact' => $onsite_contact,
			'contact_number' => $contact_number,
			'contact_email' => $contact_email,
			'person_seen' => $person_seen,
			'csr' => $csrexp[0],
			'project_address_line1' => $project_address_line1,
			'project_address_line2' => $project_address_line2,
			'project_email' => $project_email,
			'project_city' => $project_city,
			'project_state' => $project_state,
			'project_postal_code' => $project_postal_code,
			'billing_address_line1' => $billing_address_line1,
			'billing_address_line2' => $billing_address_line2,
			'billing_email' => $billing_email,
			'billing_city' => $billing_city,
			'billing_state' => $billing_state,
			'billing_postal_code' => $billing_postal_code,
			'project_description' => $project_description,
			'project_status' => $project_status,
			'labor_hours_type' => $labor_hours_type,
			'labor_number_of_hours' => $labor_number_of_hours,
            'labor_number_of_men' => $labor_number_of_men,
			'latitude' => $lat,
			'longitude' => $lng,
			'created_date' => $created_date,
			'modifydate'=> $modifydate,
			'BillToId' =>$addressbilllname,
			'CustomerId'=>$addresslocationname,
			'whyreason' => $whyreason,
			'parking' => $parking,
			'parking_other' => $parking_other,  /*sag-163*/
			'job_version' => 1
        )
      );


      $job_id = $wpdb->insert_id;

	  $wpdb->insert($table_history,array("jobid"=>$job_id,	"salesname"=>$salesman_nameexp[0],"date"=>date("Y-m-d H:i:s")));

	  if($job_item['hiddensett']=="submit") //SUBMITTING THE FORM TO CSR
	  {
			$_SESSION['EMAILDATA']['hiddensett']='submit';
			$emailid = $csrexp[1];
			$_SESSION['EMAILDATA']['csrexp'] =$emailid ;

	  }
	  else //SAVING FORM FOR SALESMAN
	  { //for salesman
	  		$_SESSION['EMAILDATA']['project_status'] = $project_status;
	  		$_SESSION['EMAILDATA']['hiddensett']='save';


			$emailarray=array();
			$emailid= $salesman_nameexp[1];
			$emailid2= $owner_nameexp[1];
			$csreamilid = $csrexp[1];
			array_push($emailarray,$emailid);
			array_push($emailarray,$emailid2);
			array_push($emailarray,$csreamilid);
			
			$linkset=get_permalink(get_option('acceptjobpage'));
			$message= "A new job has been saved " .  $linkset."?job_id=".$job_id;
			$subject = 'New Job Saved  ' .  $quote_number;
			$headers = 'From: S. Albert Glass <'.EMAILFROM.'>';

	   		sendmailmode($emailid,$subject,$message,$headers);
			// wp_mail( 'steven@salbertglass.com', $subject, $message , 'From: S. Albert Glass <development@salbertglass.com>' );

			if($project_status=='na' || $project_status=='void cancel' || $project_status=='void duplicate' || $project_status=='void lost'){
			//if($project_status=="na")
			 //{

				//$message= $quote_number." is no longer applicable. "."\n"."\n";
				$message= $quote_number." is no longer applicable. "."\n"."\n";
				if($project_status!='na'){
				    $message .= ucfirst($project_status)." Reason"."\n";
				}
				else{
				    $message .= "N/A Reason"."\n";
				}
				//$message .= "N/A Reason"."\n";
				$message .= $whyreason."\n\n";
				$message .= get_option("siteurl")."/add-job/?job_id=".$job_id."\n"."\n";
				$message .= $bodyTxt;
				$subject = ''.$quote_number." is no longer applicable. ";
      			$headers = "From: S. Albert Glass <".EMAILFROM.">"."\r\n";
 				if(in_array("steven@salbertglass.com",$emailarray)) {	
				//	array_push($emailarray,"steven@salbertglass.com");			 
					$headers = 'CC: ajay@imajine.us,alex@imajine.us';
				}else{
				$headers = 'CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us';
			 }
//				array_push($emailarray,"gburkhart@salbertglass.com");
				$emailarray=array_filter($emailarray);
				$emailarray= array_unique($emailarray);
				
				$emaillist=implode(",",$emailarray);
				
				sendmailmode($emaillist,$subject,$message,$headers);


			}
	 }

 } else {




  //-------------------------------------------------
  //---------------UPDATING THE JOB-----------------
  //-------------------------------------------------
		$user_id = get_current_user_id();
		$select ="select * from $table_sag_jobs where id = $job_id ";
		$resultsget=$wpdb->get_results($select);
		$salesnameprev=$resultsget[0]->salesman_name;
		$ownernameprev=$resultsget[0]->owner_name;
		$csrrnameprev=$resultsget[0]->csr;
		$job_version=$resultsget[0]->job_version +1;
		$oldProjectStatus = $resultsget[0]->project_status;		
		$salesManDataName = $this->salesManMapData();
		
		if($ownernameprev != $owner_nameexp[0]){$salesman_type = 'Salesman A'; $salesman_name = $ownernameprev;}
		if($salesnameprev != $salesman_nameexp[0]){ $salesman_type = 'Salesman B';$salesman_name = $salesnameprev;}
		if($csrrnameprev != $csrexp[0]){ $salesman_type = 'CSR';$salesman_name = $csrrnameprev;}
		if($salesnameprev != $salesman_nameexp[0] || $ownernameprev != $owner_nameexp[0] || $csrrnameprev != $csrexp[0]){
			$rows_affected = $wpdb->insert(
			'jobs_salesman_history',
				array(
				'assigning_date' => $modifydate,
				'salesman_name'   => $salesman_name,				
				'salesman_email'=>	$salesManDataName['salesmanEmailMap'][$ownernameprev],
				'job_id'=>$job_id, 
				'salesman_type' => $salesman_type			
		
				)
		  );
		}
		
		
	//echo "dgdf".$project_status;

	//echo "dgdf".$job_item['hiddensett'];
	//echo $job_item['hiddensett'];	

if($job_item['hiddensett']== "submit"){
    
   
   $job_data = PHP_EOL." job id " . $job_id . '  quote number :' . $quote_number . '  Date&Time ' . date("d/m/Y h:i:sa").' submit button pressessed.';
   
    
    $pluginlog = plugin_dir_path(__FILE__).'jobs_process_log.log';
 
    error_log($job_data, 3, $pluginlog);
    
    
     
    
		
if($job_item['hiddensett']== "submit" &&
	(
	trim(strtolower($project_status))=='new ticket' || 
	trim(strtolower($project_status))=='submitted' || 
	trim(strtolower($project_status))=='quote' || 
	trim(strtolower($project_status))=='research' || 
	trim(strtolower($project_status))=='on hold' )

){	$project_status_update = 'submitted';		} else {

	$project_status_update = 'na';
}
}
//echo "--1--".$project_status_update;

if($job_item['hiddensett']== "save"){	
if($job_item['hiddensett']=="save" &&
	(
	trim(strtolower($project_status))=='new ticket' || 
	trim(strtolower($project_status))=='quote' || 
	trim(strtolower($project_status))=='research' || 
	trim(strtolower($project_status))=='on hold' )

){	$project_status_update = 'research';		} else {

	$project_status_update = 'na';
}

if($job_item['hiddensett']=="save" && trim(strtolower($project_status))=='submitted'){	$project_status_update='submitted';		}
}

$_com_job_name = strcmp($job_name , $resultsget[0]->job_name);

if($_com_job_name >= 0){

	$_SESSION["job_name_com"] = 'changed';
} else {

	$_SESSION["job_name_com"] = 'same';
}



	$csrprev = $resultsget[0]->csr;
		$wpdb->show_errors();
      	$rows_affected = $wpdb->update(
        $table_sag_jobs,
        array(
			'salesman_id' => $salesmanIdMap[$salesman_nameexp[0]],
			'salesman_name' => $salesman_nameexp[0],
			'owner_name' =>  $owner_nameexp[0],
			'quote_number' => $quote_number,
			'secondary_quote' => $secondary_quote,
			'job_name' => $job_name,
			'csr_description' => $csr_description,
			'onsite_contact' => $onsite_contact,
			'contact_number' => $contact_number,
			'contact_email' => $contact_email,
			'person_seen' => $person_seen,
			'csr' => $csrexp[0],
			'project_address_line1' => $project_address_line1,
			'project_address_line2' => $project_address_line2,
			'project_email' => $project_email,
			'project_city' => $project_city,
			'project_state' => $project_state,
			'project_postal_code' => $project_postal_code,
			'billing_address_line1' => $billing_address_line1,
			'billing_address_line2' => $billing_address_line2,
			'billing_email' => $billing_email,
			'billing_city' => $billing_city,
			'billing_state' => $billing_state,
			'billing_postal_code' => $billing_postal_code,
			'project_description' => $project_description,
			'project_status' => $project_status_update,
			'labor_hours_type' => $labor_hours_type,
			'labor_number_of_hours' =>$labor_number_of_hours,
			'labor_number_of_men' =>$labor_number_of_men,
			'labor_notes' => $labur_notes,
			'modifydate'=> $modifydate,
			'BillToId' =>$addressbilllname,
			'CustomerId'=>$addresslocationname,
			'whyreason' => $whyreason,
			'parking' => $parking,
			'parking_other' => $parking_other,/*sag-163*/
			'job_version'=>$job_version
        ),
        array(
          'id' => $job_id
        )
      );
	 //print_r($job_item);
	 
		 $eol = "\n\n"; 


if(((trim($salesnameprev) != trim($salesman_nameexp[0])) || ( trim($ownernameprev) != trim($owner_nameexp[0]))) && $project_status!='na')
		{
			$link=get_permalink(get_option("acceptjobpage"));
			$newsalesManForHistory = ''; $previousUserVal='';
			if(trim($ownernameprev) != trim($owner_nameexp[0])){
				$previousUserVal =$ownernameprev;
				$newsalesManForHistory =$owner_nameexp[0];
			
	 		}else{
				$previousUserVal =$salesnameprev;
				$newsalesManForHistory  =$salesman_nameexp[0];
			}
			
			$subject =  $quote_number. " has been re-assigned from ".$previousUserVal. " and is waiting to be accepted." ;
			$message = $quote_number. " has been re-assigned from ".$previousUserVal. " and is waiting to be accepted."."\n"."\n";
			$message .= $link . "?job_id=" . $job_id."\n"."\n";
			$message .=	$bodyTxt;
		//	echo $csrexp[1]." >> " .$owner_nameexp[1] ." >> " .$salesman_nameexp[1]." >> " .$frmEmail." >> " .$blankAry." <br> " ;die;
			$mailData=$this->commanMail($csrexp[1],$owner_nameexp[0] ,$salesman_nameexp[0],$frmEmail,$blankAry);
		//	echo $mailData['emaillist']." >> " ." >> " .$mailData['header'];die;
			sendmailmode($mailData['emaillist'],$subject,$message,$mailData['header']);
			$wpdb->insert($table_history,array("jobid"=>$job_id,"salesname"=>$newsalesManForHistory,"date"=>date("Y-m-d H:i:s")));
			$tablest= $wpdb->prefix."sag_jobs";
			$update="update $tablest set accepted='0' where id='$job_id'";
			$wpdb->query($update);
		
		}



	if($job_item['hiddensett'] == 'save' && ($project_status=="research" || $project_status=="new ticket" || $project_status=="submitted" || $project_status=="quote" || $project_status=="on hold"   )){
		

	    $message='';
		$link=get_option("siteurl") . "/add-job/?job_id=" . $job_id;
		if($project_status=="new ticket" ){
			//echo $project_status;
		
			 
       		$bodyText='';
			$bodyText = $quote_number." A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted."."\n\n";
			$bodyText .= $link."\n\n";
			$bodyText .="Job Description: ".$job_name."\n"."\n";
			$bodyText .= "Sales Rep A : ".$owner_nameexp[0]."\n";
			if($salesman_nameexp[0]!='na'){		$bodyText .= "Sales Rep B : ".$salesman_nameexp[0]."\n";		}
			$bodyText .= "CSR : ".$csrexp[0]."\n\n";
			$bodyText .= "Project Address"."\n";
			$bodyText .= $project_address_line1."\n";
			$bodyText .= $project_address_line2."\n";
			$bodyText .= $project_city.", ".$project_state."-".$project_postal_code."\n"."\n";
			$bodyText .= "Project Contact"."\n";
			$bodyText .= $onsite_contact."\n";
			$bodyText .= $contact_number."\n";
			$bodyText .= $contact_email."\n";
		  
			$subject = $quote_number.'  A NEW SERVICE TICKET has been Assigned to you and is waiting to be Accepted.';
			
		}else{
			
			$subject =  'Research Required ' . $quote_number ;
			$message = $quote_number." has been SAVED.  MORE RESEARCH TO BE DONE.". $eol;
			
		}
		$message .=$link . $eol;
		$message .=	$bodyTxt;
		
		if($project_status=="new ticket" ){
			$headers='';
			$headers = "From: S. Albert Glass <".EMAILFROM.">". "\r\n"  ;
			$headers .=   "CC: ";
			$ccMailList =array($csrexp[1],$salesman_nameexp[1]) ;
			$stevenMailId = false;
			foreach($ccMailList as $email){
			   if($email=='steven@salbertglass.com' || $email=='appmanager@salbertglass.com'){
			        	$stevenMailId=true;
			    }
			}
			
			if(	$stevenMailId==false){
			    if($owner_nameexp[1]=='steven@salbertglass.com' || $owner_nameexp[1]=='appmanager@salbertglass.com'){       	$headers .=$ccMailList[0].','.$ccMailList[1];	    }
				else{			        	$headers .=$ccMailList[0].','.$ccMailList[1].",appmanager@salbertglass.com";	    }
			    
			}else{
			    
			    	$headers .=$ccMailList[0].','.$ccMailList[1];
			}
		
			$headers .=	  "\r\n" ;

			//print_r($owner_nameexp[1]);

			//die;

			sendmailmode($owner_nameexp[1],$subject,$bodyText,$headers);
			$bodyText='';
				
		}else{
			$mailData= $this->commanMail($csrexp[1],$owner_nameexp[0] ,$salesman_nameexp[0],$frmEmail,$blankAry);
		//print_r($mailData);
		//die;
	//		echo "subject >> ".$subject." >> message ". $message ." tester lin no ".__LINE__;  die;
	
	     $job_data = PHP_EOL." job id " . $job_id . '  quote number :' . $quote_number . '  Date&Time ' . date("d/m/Y h:i:sa").' save process. Header'.$mailData['header'].' email list : '.$mailData['emaillist'];
   
    
        $pluginlog = plugin_dir_path(__FILE__).'jobs_process_log.log';
     
        error_log($job_data, 3, $pluginlog);
	
			
			sendmailmode($mailData['emaillist'],$subject,$message,$mailData['header']);
		}
		$headers='';$message ='';
			
	}
	
	$emailarray=array();
	$emailid = $csrexp[1];
	$salesmanEmailId = $salesman_nameexp[1];
	$ownerEmailId = $owner_nameexp[1];
	array_push($emailarray,$emailid);
	array_push($emailarray,$salesmanEmailId);
	array_push($emailarray,$ownerEmailId);
	
	
	if($project_status=='na' || $project_status=='void cancel' || $project_status=='void duplicate' || $project_status=='void lost'){
	//if($project_status=="na")
	//{
	
		$message= ''.$quote_number." is no longer applicable. "."\n"."\n";
		if($project_status!='na'){
		    $message .= ucfirst($project_status)." Reason"."\n";
		}
		else{
		    $message .= "N/A Reason"."\n";
		}
		//$message .= "N/A Reason"."\n";
		$message .= $whyreason."\n\n";
		$message .= get_option("siteurl")."/add-job/?job_id=".$job_id."\n"."\n";
		$message .=	$bodyTxt;
		$subject =  $quote_number." is no longer applicable. ";
		$headers = "From: S. Albert Glass <".EMAILFROM.">". "\r\n";
		
		if(in_array("steven@salbertglass.com",$emailarray))
		{
			//array_push($emailarray,"steven@salbertglass.com");
			$headers .= "CC: ajay@imajine.us,alex@imajine.us". "\r\n";
		}else{
			$headers .= "CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us";
			}
//		array_push($emailarray,"gburkhart@salbertglass.com");
		$emailarray=array_filter($emailarray);
		$emailarray= array_unique($emailarray);
		$emailimpolde=implode(",",$emailarray);
		sendmailmode($emailimpolde,$subject,$message,$headers);
		// wp_mail("steven@salbertglass.com", $subject, $message , 'From: S. Albert Glass <development@salbertglass.com>' );
		
	}

	if( $job_item['hiddensett']=="submit" &&   $project_status!="na" && $project_status!="void cancel" && $project_status!="void duplicate" && $project_status!="void lost" ) //SUBMITTING THE FORM TO CSR
		{

			
		   $_SESSION['EMAILDATA']['hiddensett']='submit';
			$_SESSION['EMAILDATA']['job_version']=$job_version;
			
			    //$current = file_get_contents('jobs_process_log.txt');
           $job_data = PHP_EOL." job id " . $job_id . '  quote number :' . $quote_number . '  Date&Time ' . date("d/m/Y h:i:sa").' Added Data to session. and pdf processing';
           //  $data_update = $current . ' ' . PHP_EOL . ' ' . $job_data;
             $pluginlog = plugin_dir_path(__FILE__).'jobs_process_log.log';
 
    error_log($job_data, 3, $pluginlog);
    
		
		}
    
		
	
		if(trim($csrprev) != trim($csrexp[0]))
		{ 	
			$headers='';
			$subjectset=   $quote_number. " has been re-assigned from ".$csrprev ;
			$body = $quote_number. " has been re-assigned from ".$csrprev."\n"."\n";
			$body .= get_option("siteurl") . "/add-job/?job_id=" . $job_id."\n"."\n";
			$body .=	$bodyTxt;
			$emailid=$csrexp[1];
			
			if($csrexp[1]!='steven@salbertglass.com'){
			$headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .	
		"CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us";
			}else{
				$headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .	
		"CC: ajay@imajine.us,alex@imajine.us";
				}
			sendmailmode($emailid,$subjectset,$body,$headers);
	
		}
					
				
   }

	if($secondary_quote !=''){
		$selecChild ="select id from $table_sag_jobs where quote_number = '".$secondary_quote."'";
		$resultsgetchild=$wpdb->get_results($selecChild);
		$childJobId=$resultsgetchild[0]->id;
	}

    //NEW DOOR WALA SECTION
  //  if (!empty($job_item['nwdr_items'])) {
   if (is_int($rows_affected) && ($rows_affected > 0 || ($rows_affected == 0 && $job_id > 0))) {
	$wpdb->show_errors();
	$nwdr_items = $job_item['nwdr_items'];
	
	
	
        $nwdr_items = stripslashes_deep($nwdr_items);
        for($j=0;$j<50;$j++){
          for($k=0;$k<count($nwdr_items);$k++){
             $nwdr_items[$k] = stripslashes_deep($nwdr_items[$k]);
          }
        }
				
	
	$num_items = count($nwdr_items);
	if ($job_id <= 0) {
		 $job_id = $wpdb->insert_id;
		}
	else{
			$wpdb->delete(	$table_nwdr_items,		array('job_id' => $job_id	));
			$wpdb->delete(	$table_nwaluof_items,	array('job_id' => $job_id	));
			$wpdb->delete(	$table_nwaluft_items,	array('job_id' => $job_id	));
			$wpdb->delete(	$table_nwaluff_items,	array('job_id' => $job_id	));
			$wpdb->delete(  $table_hmnd_items,		array('job_id' => $job_id	));
			$wpdb->delete(  $table_hmnf_items,		array('job_id' => $job_id	));
			$wpdb->delete(  $table_patio_items,		array('job_id' => $job_id	));
			
		
		}
		
$toErrorLog = 'ajay@imajine.us';		
$headersForLog = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" ;
$subjectForLog = "fail sql error log ";				
		
//print_r($nwdr_items);

//die;

if(!empty($nwdr_items))
{
      foreach ($nwdr_items as $value) {

		  foreach($value as $key=>$tt){
				$tt = preg_replace('/,+/', ',', $tt);				$tt = trim($tt,",");		$value[$key]=str_replace('\"','"',$tt);
			}

        switch ($value['category']) {

          case 'Gdoor':
            unset($value['id']);
			foreach($value as $key=>$ss){
					$ss = preg_replace('/,+/', ',', $ss);			$ss = trim($ss,",");		$value[$key]=str_replace('\"','"',$ss);
				}
		    $value['job_id'] = $job_id;
			$value['glass_type'] = str_replace('\"','"',$value['glass_type']);
			$value['pull_hardware'] = str_replace('\"','"',$value['pull_hardware']);
			$value['header'] = str_replace('\"','"',$value['header']);
			$value['newdoor_threshold'] = str_replace('\"','"',$value['newdoor_threshold']);
			$value['sidelight_henad_condition'] = str_replace('\"','"',$value['sidelight_henad_condition']);
			$value['sidelight_sill_condition'] = str_replace('\"','"',$value['sidelight_sill_condition']);
			//echo $table_nwdr_items;
			//
			$wpdb->last_error='';
			try{	
					$wpdb->query('START TRANSACTION');
					$this->deleteRecord($this->table_nwdr_items_flag,$table_nwdr_items,$job_id);
					//$wpdb->delete(	$table_nwdr_items,		array('job_id' => $job_id	));
					$wpdb->show_errors();
					$wpdb->insert($table_nwdr_items, $value);
					//$wpdb->last_error
					if($wpdb->last_error){		sendmailmode($toErrorLog,$subjectForLog,$wpdb->last_error,$headersForLog);				}
					//	
					$wpdb->query('COMMIT');
				}catch(Exception $e){
					 //some error handling code here
					$wpdb->query('ROLLBACK');
				  //    $wpdb->query(‘ROLLBACK’);
				
				}

           // print_r($wpdb->insert($table_nwdr_items, $value));
          break;

          case 'Aludoor':
            if ($value['type']=='Aludnfsig' || $value['type']=='Aludnfpar') {
                unset($value['id']);
                $wpdb->show_errors();
                $value['job_id'] = $job_id;
				$value['type_of_glass'] = str_replace('\"','"',$value['type_of_glass']);
				$value['frame_system'] = str_replace('\"','"',$value['frame_system']);
				$value['threshold'] = str_replace('\"','"',$value['threshold']);
				$value['mid_panel'] = str_replace('\"','"',$value['mid_panel']);
				$value['bottom_rail'] = str_replace('\"','"',$value['bottom_rail']);
				$value['opening_width'] = str_replace('\"','"',$value['opening_width']);
				$wpdb->last_error='';
				//$value['rough_opening_width'] = str_replace('\"','"',$value['rough_opening_width']);
				//$value['rough_opening_height'] = str_replace('\"','"',$value['rough_opening_height']);
				try{
					$wpdb->query('START TRANSACTION');
				//	$wpdb->delete(	$table_nwaluft_items,	array('job_id' => $job_id	));
					$this->deleteRecord($this->table_nwaluft_items_flag,$table_nwaluft_items,$job_id);
					$wpdb->insert($table_nwaluft_items, $value);
					if($wpdb->last_error){		sendmailmode($toErrorLog,$subjectForLog,$wpdb->last_error,$headersForLog);				}
					$wpdb->query('COMMIT');
				}catch(Exception $e){
					 //some error handling code here
					$wpdb->query('ROLLBACK');
				  //    $wpdb->query(‘ROLLBACK’);
				
				}

				
            } else  if ($value['type']=='Alufsig' || $value['type']=='Alufpar'){
                unset($value['id']);
                $wpdb->show_errors();
                $value['job_id'] = $job_id;
				$value['type_of_glass'] = str_replace('\"','"',$value['type_of_glass']);
				$value['frame_system'] = str_replace('\"','"',$value['frame_system']);
				$value['mid_panel'] = str_replace('\"','"',$value['mid_panel']);
				$value['bottom_rail'] = str_replace('\"','"',$value['bottom_rail']);
				$value['threshould'] = str_replace('\"','"',$value['threshould']);
				$value['opening_width'] = str_replace('\"','"',$value['opening_width']);
				$value['hanging_hardware'] = str_replace('\"','"',$value['hanging_hardware']);
				$value['opening_height'] = str_replace('\"','"',$value['opening_height']);
				$value['door_width'] = str_replace('\"','"',$value['door_width']);
				$value['door_height'] = str_replace('\"','"',$value['door_height']);
				//print_r($value);
				//die();
				$wpdb->last_error='';
				try {
					$wpdb->query('START TRANSACTION');
					//$wpdb->delete(	$table_nwaluff_items,	array('job_id' => $job_id	));
					$this->deleteRecord($this->table_nwaluff_items_flag,$table_nwaluff_items,$job_id);
					$wpdb->insert($table_nwaluff_items, $value);
					if($wpdb->last_error){		sendmailmode($toErrorLog,$subjectForLog,$wpdb->last_error,$headersForLog);				}
					$wpdb->query('COMMIT');
				}catch(Exception $e){
					 //some error handling code here
					$wpdb->query('ROLLBACK');
				  //    $wpdb->query(‘ROLLBACK’);
				
				}

            }

			else
			{

                unset($value['id']);
                $wpdb->show_errors();
                $value['job_id'] = $job_id;
				$value['type_of_glass'] = str_replace('\"','"',$value['type_of_glass']);
				$value['mid_panel'] = str_replace('\"','"',$value['mid_panel']);
				$value['bottom_rail'] = str_replace('\"','"',$value['bottom_rail']);
				$value['opening_width'] = str_replace('\"','"',$value['opening_width']);
				$value['opening_height'] = str_replace('\"','"',$value['opening_height']);
				$value['door_width'] = str_replace('\"','"',$value['door_width']);
				$value['door_height'] = str_replace('\"','"',$value['door_height']);
				$value['center_intermediate'] = str_replace('\"','"',$value['center_intermediate']);
				$value['center_lock'] = str_replace('\"','"',$value['center_lock']);
				$value['center_pull'] = str_replace('\"','"',$value['center_pull']);
				$value['center_panel'] = str_replace('\"','"',$value['center_panel']);
				$value['door_sweep'] = str_replace('\"','"',$value['door_sweep']);
				
				
				$wpdb->last_error='';
				try { 
						$wpdb->query('START TRANSACTION');
						//$wpdb->delete(	$table_nwaluof_items,	array('job_id' => $job_id	));
						$this->deleteRecord($this->table_nwaluof_items_flag,$table_nwaluof_items,$job_id);
						$wpdb->insert($table_nwaluof_items, $value);
						if($wpdb->last_error){		sendmailmode($toErrorLog,$subjectForLog,$wpdb->last_error,$headersForLog);				}
						$wpdb->query('COMMIT');
					}catch(Exception $e){
						 //some error handling code here
						$wpdb->query('ROLLBACK');
					  //    $wpdb->query(‘ROLLBACK’);
					
					}


			}
          break;

          case 'hmdoor':
            if ($value['type']=='Hmdndsig' || $value['type']=='hmdndpar') {
                unset($value['id']);
                $wpdb->show_errors();
                $value['job_id'] = $job_id;
				$value['hinge_height'] = str_replace('\"','"',$value['hinge_height']);
				$value['thinkness'] = str_replace('\"','"',$value['thinkness']);
				$value['vision_kit'] = str_replace('\"','"',$value['vision_kit']);
				$value['width_out_side'] = str_replace('\"','"',$value['width_out_side']);
				$value['door_height'] = str_replace('\"','"',$value['door_height']);
				$value['the_top_hinge1'] = str_replace('\"','"',$value['the_top_hinge1']);
				$value['the_top_hinge2'] = str_replace('\"','"',$value['the_top_hinge2']);
				$value['the_top_hinge3'] = str_replace('\"','"',$value['the_top_hinge3']);
				$value['the_top_hinge4'] = str_replace('\"','"',$value['the_top_hinge4']);
				$value['line_of_lock'] = str_replace('\"','"',$value['line_of_lock']);
				$value['lock_backset'] = str_replace('\"','"',$value['lock_backset']);
				$value['center_deadbolt'] = str_replace('\"','"',$value['center_deadbolt']);
				$value['dead_bolt'] = str_replace('\"','"',$value['dead_bolt']);
				$value['latch_guard'] = str_replace('\"','"',$value['latch_guard']);
				$value['kick_plate'] = str_replace('\"','"',$value['kick_plate']);
				$value['door_sweep'] = str_replace('\"','"',$value['door_sweep']);
				$value['drip_cap'] = str_replace('\"','"',$value['drip_cap']);
				$value['door_viewer'] = str_replace('\"','"',$value['door_viewer']);
				$wpdb->last_error='';
				try {
					$wpdb->query('START TRANSACTION');
				//$wpdb->delete(  $table_hmnd_items,		array('job_id' => $job_id	));
					$this->deleteRecord($this->table_hmnd_items_flag,$table_hmnd_items,$job_id);
					$wpdb->insert($table_hmnd_items, $value);
					if($wpdb->last_error){		sendmailmode($toErrorLog,$subjectForLog,$wpdb->last_error,$headersForLog);				}
					$wpdb->query('COMMIT');
				}catch(Exception $e){
					 //some error handling code here
					$wpdb->query('ROLLBACK');
				  //    $wpdb->query(‘ROLLBACK’);
				
				}

            } else {
                unset($value['id']);
                $wpdb->show_errors();
                $value['job_id'] = $job_id;
				$value['opening_height_left'] = str_replace('\"','"',$value['opening_height_left']);
				$value['opening_height_right'] = str_replace('\"','"',$value['opening_height_right']);
				$value['opening_width_bootms'] = str_replace('\"','"',$value['opening_width_bootms']);
				$value['opening_width_middel'] = str_replace('\"','"',$value['opening_width_middel']);
				$value['opening_width_top'] = str_replace('\"','"',$value['opening_width_top']);
				$value['depth_or_thickness'] = str_replace('\"','"',$value['depth_or_thickness']);
				$value['freme_width'] = str_replace('\"','"',$value['freme_width']);
				$value['lock_backset'] = str_replace('\"','"',$value['lock_backset']);
				$value['glass_height'] = str_replace('\"','"',$value['glass_height']);
				$value['thickness'] = str_replace('\"','"',$value['thickness']);
				$value['vision_kit'] = str_replace('\"','"',$value['vision_kit']);
				$value['lock_back'] = str_replace('\"','"',$value['lock_back']);
				$value['dead_bolt'] = str_replace('\"','"',$value['dead_bolt']);
				$value['latch_guard'] = str_replace('\"','"',$value['latch_guard']);
				$value['door_sweep'] = str_replace('\"','"',$value['door_sweep']);
				$value['drip_cap'] = str_replace('\"','"',$value['drip_cap']);
				$value['door_viewer'] = str_replace('\"','"',$value['door_viewer']);
				$value['frame_depth'] = str_replace('\"','"',$value['frame_depth']);
				$value['wall_type'] = str_replace('\"','"',$value['wall_type']);
				$value['kick_plate'] = str_replace('\"','"',$value['kick_plate']);
				$wpdb->last_error='';
			
				try {
				
					$wpdb->query('START TRANSACTION');
					$this->deleteRecord($this->table_hmnf_items_flag,$table_hmnf_items,$job_id);
					$wpdb->insert($table_hmnf_items, $value);
					if($wpdb->last_error){		sendmailmode($toErrorLog,$subjectForLog,$wpdb->last_error,$headersForLog);				}
					$wpdb->query('COMMIT');
				}catch(Exception $e){
					 //some error handling code here
					$wpdb->query('ROLLBACK');
				  //    $wpdb->query(‘ROLLBACK’);
				
				}
			
			//	$wpdb->query('START TRANSACTION');
				//$wpdb->delete(  $table_hmnf_items,		array('job_id' => $job_id	));
								
            }
          break;

          

          case 'patio':
          		
          		


          		
          
       //foreach ($value as $key => $val) {
          		

          	//	print_r($val);
          	//}

          	//die('hello111');

          	//if($value[id] == 0){	

          	$wpdb->insert($table_patio_items, $value);

          	



         // }
          	 break;


          default:
            # code...
            break;
        }
      }
	  }
    }

    //NOTES WALA SECTION
    if (isset($job_item['textarea_tgpd_notes'])) {

      $userId = get_current_user_id();

      $imageString = 'image';
      //$wpdb->prepare( "SELECT * FROM table WHERE ID = %d AND name = %s", $id, $name );
      //$queryImg = "SELECT live_url FROM $table_temp_upload WHERE user_id = %d AND type = %s", $userId, $imageString
      $images = $wpdb->get_results($wpdb->prepare("SELECT live_url FROM $table_temp_upload WHERE user_id = %d AND type = %s", $userId, $imageString));

	$imageursl = '';
	$imageCounter = 0;
	
	
	foreach($images as $value){
		if($value->live_url!=''){
			if($imageCounter == 0){
				$imageursl = $value->live_url;
			}else{
				$imageursl = $imageursl.','.$value->live_url;
			}
			$imageCounter++;
		}
	}
	$sketchString = 'sketch';
	$sketch = $wpdb->get_results($wpdb->prepare("SELECT live_url FROM $table_temp_upload WHERE user_id = %d AND type = %s", $userId, $sketchString));
	$sketchursl = '';
	$sketchCounter = 0;

	foreach($sketch as $svalue){
		if($svalue->live_url!=''){
			if($sketchCounter == 0){
				$sketchursl = $svalue->live_url;
			}else{
				$sketchursl = $sketchursl.','.$svalue->live_url;
			}
			$sketchCounter++;
		}
	}



	$imageursl = preg_replace('/,+/', ',', $imageursl);
	$imageursl = trim($imageursl,",");

	$sketchursl = preg_replace('/,+/', ',', $sketchursl);
	$sketchursl = trim($sketchursl,",");
	$alreadyNotes = $wpdb->get_row($wpdb->prepare("SELECT notes_upload_pic,notes_sketch_diagram FROM $table_notes_items WHERE  job_id = %s",  $job_id));
	if($imageursl == '' ){
		$imageursl = $alreadyNotes->notes_upload_pic;
	}
	if($sketchursl == '' ){
		$sketchursl = $alreadyNotes->notes_sketch_diagram;
	}
	if($imageursl!='' || $sketchursl!='' || $job_item['textarea_tgpd_notes']!=''){
	$wpdb->delete($table_notes_items,array('job_id' => $job_id));

		$wpdb->insert( $table_notes_items,  array(
			'user_id' => $userId,
			'job_id' => $job_id,
			'notes_upload_pic' => $imageursl,
			'notes_sketch_diagram' => $sketchursl,
			'note_text' => $job_item['textarea_tgpd_notes']
			)
		);


       $wpdb->delete($table_temp_upload, array('user_id' => $userId));
	}
 }

    if (is_int($rows_affected) && ($rows_affected > 0 || ($rows_affected == 0 && $job_id > 0))) {

      if ($job_id <= 0) {
        $job_id = $wpdb->insert_id;
      } else {
        $wpdb->delete($table_gpd_items,array('job_id' => $job_id));
        $wpdb->delete($table_gdr_items,array('job_id' => $job_id));
        $wpdb->delete($table_gdn_items,array('job_id' => $job_id));
        $wpdb->delete($table_sfd_items,array('job_id' => $job_id));
        $wpdb->delete($table_wmd_items,array('job_id' => $job_id));
        $wpdb->delete($table_sdbg_items,array('job_id' => $job_id));
      }
	  if(isset($childJobId)&& $childJobId !=''){
	  	$wpdb->delete($table_gpd_items,array('job_id' => $childJobId));
	  }



      //Glass Pecies wala section
      if (!empty($gpd_items)) {
        foreach($gpd_items as $gpd_item) {
			$gpd_glass_location = $gpd_item["glass_location"];
			$gpd_coating_exterior_type = $gpd_item["coating_exterior_type"];
			$gpd_coating_interior_type = $gpd_item["coating_interior_type"];

			
			/**sag124***/
			$tgpd_coating_family_exterior_type = $gpd_item["tgpd_coating_family_exterior_type"];
			$tgpd_coating_family_interior_type = $gpd_item["tgpd_coating_family_interior_type"];


			$tgpd_coating_family_color_exterior_type = $gpd_item["tgpd_coating_family_color_exterior_type"];
			$tgpd_coating_family_color_interior_type = $gpd_item["tgpd_coating_family_color_interior_type"];


			$verify_tgpd_exterior_coating_family_glass_type = $gpd_item["verify_tgpd_exterior_coating_family_glass_type"];


			$verify_tgpd_interior_coating_family_glass_type = $gpd_item["verify_tgpd_interior_coating_family_glass_type"];


			$verify_tgpd_interior_coating_family_color_glass_type = $gpd_item["verify_tgpd_interior_coating_family_color_glass_type"];

			$verify_tgpd_exterior_coating_family_color_glass_type = $gpd_item["verify_tgpd_exterior_coating_family_color_glass_type"];
			/***sag 124***/



			$gpd_glass_quantity = ($gpd_item["glass_quantity"]!='')?$gpd_item["glass_quantity"]:0;
			$gpd_glass_servicetype = $gpd_item["glass_servicetype"];
			$gpd_glass_shape = $gpd_item["glass_shape"];
			$gpd_glass_shape_left_leg = $gpd_item["glass_shape_left_leg"];
			$gpd_glass_shape_right_leg = $gpd_item["glass_shape_right_leg"];
			$gpd_glass_shape_width = $gpd_item["glass_shape_width"];
			$gpd_glass_shape_base = $gpd_item["glass_shape_base"];
			$gpd_glass_shape_side = $gpd_item["glass_shape_side"];
			$gpd_glass_shape_diameter = $gpd_item["glass_shape_diameter"];
			$gpd_glass_shape_radius = $gpd_item["glass_shape_radius"];
			$gpd_glass_shape_radius_one = $gpd_item["glass_shape_radius_one"];
			$gpd_glass_shape_radius_two = $gpd_item["glass_shape_radius_two"];
			$gpd_glass_shape_length = $gpd_item["glass_shape_length"];
			$gpd_glass_shape_leg = $gpd_item["glass_shape_leg"];
			$gpd_glass_shape_left_side = $gpd_item["glass_shape_left_side"];
			$gpd_glass_shape_right_side = $gpd_item["glass_shape_right_side"];
			$gpd_glass_shape_right_offset = $gpd_item["glass_shape_right_offset"];
			$gpd_glass_shape_left_offset = $gpd_item["glass_shape_left_offset"];
			$gpd_glass_shape_top_offset = $gpd_item["glass_shape_top_offset"];
			$gpd_glass_shape_height = $gpd_item["glass_shape_height"];
			$gpd_glass_shape_top = $gpd_item["glass_shape_top"];
			$gpd_glass_type = $gpd_item["glass_type"];
			$gpd_lite_thickness = $gpd_item["lite_thickness"];
			$gpd_spacer_color = $gpd_item["spacer_color"];
			$gpd_glass_treatment = $gpd_item["glass_treatment"];
			$gpd_glass_color = $gpd_item["glass_color"];
			$gpd_glass_color_note = $gpd_item["glass_color_note"];
			$gpd_glass_lift = $gpd_item["glass_lift"];
			$gpd_glass_inside_lift_with_glass_type = $gpd_item["glass_inside_lift_with_glass_type"];
			$gpd_glass_outside_lift_with_glass_type = $gpd_item["glass_outside_lift_with_glass_type"];
			$gpd_glass_set = $gpd_item["glass_set"];
			$gpd_daylight_width = $gpd_item["daylight_width"];
			$gpd_daylight_height = $gpd_item["daylight_height"];
			$gpd_go_width = $gpd_item["go_width"];
			$gpd_go_height = $gpd_item["go_height"];
			$gpd_size_verification_status = $gpd_item["size_verification_status"];
			$gpd_overall_thickness = stripslashes($gpd_item["overall_thickness"]);
			$gpd_exterior_glass_type = stripslashes($gpd_item["exterior_glass_type"]);
			$gpd_interior_glass_type =stripslashes($gpd_item["interior_glass_type"]);
			$gpd_interior_glass_type_non_igu = stripslashes($gpd_item["interior_glass_type_non_igu"]);
			$gpd_treatment_exterior_glass = stripslashes($gpd_item["treatment_exterior_glass"]);
			$gpd_treatment_interior_glass = stripslashes($gpd_item["treatment_interior_glass"]);
			$gpd_exterior_color_glass_type = $gpd_item["exterior_color_glass_type"];
			$gpd_interior_color_glass_type = $gpd_item["interior_color_glass_type"];
			$gpd_sag_or_quote = $gpd_item["sag_or_quote"];
			$gpd_disclamers = $gpd_item["disclamers"];
			$gpd_manufacturer = $gpd_item["manufacturer"];
			$gpd_solar_film = $gpd_item["solar_film"];
			$gpd_solar_film_responsibility = $gpd_item["solar_film_responsibility"];
			$gpd_solar_film_type = $gpd_item["solar_film_type"];
			$gpd_solar_film_source = $gpd_item["solar_film_source"];
			$gpd_wet_seal = $gpd_item["wet_seal"];
			$gpd_wet_seal_responsibility = $gpd_item["wet_seal_responsibility"];
			$gpd_furniture_to_move = $gpd_item["furniture_to_move"];
			$gpd_furniture_to_move_comment = $gpd_item["furniture_to_move_comment"];
			$gpd_damage_waiver_select = $gpd_item["damage_waiver_select"];
			$gpd_disclamers_select = $gpd_item["disclamers_select"];
			$gpd_damage_waiver_text = $gpd_item["damage_waiver_text"];
			$gpd_disclamers_text = $gpd_item["disclamers_text"];
			$gpd_walls_or_ceilings_to_cut = $gpd_item["walls_or_ceilings_to_cut"];
			$gpd_walls_or_ceilings_to_cut_responsibility = $gpd_item["walls_or_ceilings_to_cut_responsibility"];
			$gpd_walls_or_ceilings_to_cut_comment = $gpd_item["walls_or_ceilings_to_cut_comment"];
			$gpd_blind_needs_removing = $gpd_item["blind_needs_removing"];
			$gpd_glass_fits_elevator = $gpd_item["glass_fits_elevator"];
			$gpd_pictures_download_url = $gpd_item["pictures_download_url"];
			$gpd_sketches_download_url = $gpd_item["sketches_download_url"];
			$gpd_pictures_download_url = preg_replace('/,+/', ',', $gpd_pictures_download_url);
			$gpd_pictures_download_url = trim($gpd_pictures_download_url,",");
			$gpd_sketches_download_url = preg_replace('/,+/', ',', $gpd_sketches_download_url);
			$gpd_sketches_download_url = trim($gpd_sketches_download_url,",");

			$gpd_instructions = $gpd_item["instructions"];
			
		
		
			for($i=0;$i<count($gpd_item["caulk_amount"]);$i++){
			
				if(empty($gpd_item["caulk_amount"][$i]) && $gpd_item["caulk_type"][$i]=='not_applicable' ){
						
						unset($gpd_item["caulk_amount"][$i]);
						unset($gpd_item["caulk_type"][$i]);
						
				}else{
						$gpd_item["caulk_amount"][$i]= 	stripslashes($gpd_item["caulk_amount"][$i]);
					}
			}
			
			
			for($i=0;$i<count($gpd_item["quantity_type"]);$i++){
				if(empty($gpd_item["quantity_type"][$i]) && $gpd_item["scaffolding_type"][$i]=='not_applicable' ){
						unset($gpd_item["quantity_type"][$i]);
						unset($gpd_item["scaffolding_type"][$i]);
					
				}else{
						$gpd_item["quantity_type"][$i]= 	stripslashes($gpd_item["quantity_type"][$i]);
					}
							
			}



			/****sag 129 start****/


			for($i=0;$i<count($gpd_item["quantity_channel"]);$i++){
				if(empty($gpd_item["quantity_channel"][$i]) && $gpd_item["channel"][$i]=='not_applicable' ){
						unset($gpd_item["quantity_channel"][$i]);
						unset($gpd_item["channel"][$i]);
					
				}else{
						$gpd_item["quantity_channel"][$i]= 	stripslashes($gpd_item["quantity_channel"][$i]);
					}
							
			}

			/******* sag  129 end*****/
			
			
			$gpd_caulk_amount ='';
			if(is_array($gpd_item["caulk_amount"])){
				$gpd_caulk_amount = array_values($gpd_item["caulk_amount"]);
			//	$gpd_caulk_amount = serialize($gpd_item["caulk_amount"]);
			}
			
			$gpd_caulk_type = '';
			if(is_array($gpd_item["caulk_type"])){
				$gpd_caulk_type = array_values($gpd_item["caulk_type"]);
			//	$gpd_caulk_type = serialize($gpd_item["caulk_type"]);
			
			}
			$gpd_scaffolding_type ='';
			if(is_array($gpd_item["scaffolding_type"])){
			$gpd_scaffolding_type = array_values($gpd_item["scaffolding_type"]);
			//$gpd_scaffolding_type = serialize($gpd_item["scaffolding_type"]);
			}
			
			$gpd_quantity_type ='';
			if(is_array($gpd_item["quantity_type"])){
			$gpd_quantity_type = array_values($gpd_item["quantity_type"]);
			//$gpd_scaffolding_type = serialize($gpd_item["scaffolding_type"]);
			}


			/****sag 129 start****/
			





			$gpd_channel ='';
			if(is_array($gpd_item["channel"])){
			$gpd_channel = array_values($gpd_item["channel"]);
			//$gpd_scaffolding_type = serialize($gpd_item["scaffolding_type"]);
			}
			
			$gpd_quantity_channel ='';
			if(is_array($gpd_item["quantity_channel"])){
			$gpd_quantity_channel = array_values($gpd_item["quantity_channel"]);
			//$gpd_scaffolding_type = serialize($gpd_item["scaffolding_type"]);
			}

			/****sag 129 end****/
			
			
			if(!empty($gpd_caulk_amount)){
			$gpd_caulk_amount = serialize($gpd_caulk_amount);
			}
			if(!empty($gpd_caulk_type)){
			$gpd_caulk_type = serialize($gpd_caulk_type);
			}
			if(!empty($gpd_scaffolding_type)){
			$gpd_scaffolding_type = serialize($gpd_scaffolding_type);
			}
			
			if(!empty($gpd_quantity_type)){
			$gpd_quantity_type = serialize($gpd_quantity_type);
			}
		
				/****sag 129 start****/
			if(!empty($gpd_channel)){
			$gpd_channel = serialize($gpd_channel);
			}
			

			
			if(!empty($gpd_quantity_channel)){
			$gpd_quantity_channel = serialize($gpd_quantity_channel);
			}
			/****sag 129 start****/

				
			for($i=0;$i<count($gpd_item["tape_amount"]);$i++){
			
				if(empty($gpd_item["tape_amount"][$i]) && $gpd_item["tape_type"][$i]=='not_applicable'){
						
						unset($gpd_item["tape_amount"][$i]);
						unset($gpd_item["tape_type"][$i]);
					//	unset($gpd_item["channel"][$i]);
				}else{
					$gpd_item["tape_amount"][$i] =stripslashes($gpd_item["tape_amount"][$i]);
				}
			}
			/*** for($i=0;$i<count($gpd_item["channel"]);$i++){
				$gpd_item["channel"][$i] = stripslashes($gpd_item["channel"][$i]);
				if(!isset($gpd_item["channel"][$i])){
				}
			 	else if(empty($gpd_item["channel"][$i])){
					 unset($gpd_item["channel"][$i]);
				 }
				 
			}****/
			
			$gpd_tape_amount ='';
			
			if(is_array($gpd_item["tape_amount"])){
				$gpd_tape_amount = array_values($gpd_item["tape_amount"]);
				//$gpd_tape_amount = serialize($gpd_item["tape_amount"]);
			 
			 }
			$gpd_tape_type = '';
			if(is_array($gpd_item["tape_type"])){
				$gpd_tape_type = array_values($gpd_item["tape_type"]);
			//	$gpd_tape_type = serialize($gpd_item["tape_type"]);
			 }
			 
			/*$gpd_channel ='';
			if(is_array($gpd_item["channel"])){
				$gpd_channel = array_values($gpd_item["channel"]);
				//$gpd_channel = serialize($gpd_item["channel"]);
			}*/
			
			if(!empty($gpd_tape_amount)){
			$gpd_tape_amount = serialize($gpd_tape_amount);
			}
		/*	if(!empty($gpd_caulk_type)){
			$gpd_caulk_type = serialize($gpd_caulk_type);
			}*/
			
			
			if(!empty($gpd_tape_type)){
			$gpd_tape_type = serialize($gpd_tape_type);
			}
			/*if(!empty($gpd_channel)){
			$gpd_channel = serialize($gpd_channel);
			}*/
			
			
			
			
			/*$gpd_glass_holes_type		 = $gpd_item["glass_holes_type"];
			$gpd_glass_holes_size		 = $gpd_item["glass_holes_size"];
			$gpd_holes_x_location		 = $gpd_item["holes_x_location"];
			$gpd_holes_y_location		 = $gpd_item["holes_y_location"];
			$gpd_holes_x_location_cl		 = $gpd_item["holes_x_location_cl"];
			$gpd_holes_y_location_cl		 = $gpd_item["holes_y_location_cl"];
			$gpd_holes_notes		 = $gpd_item["holes_notes"];*/
					
					
		for($i=0;$i<count($gpd_item["glass_holes_type"]);$i++){
					
					
			if($gpd_item["glass_holes_type"][$i]=='not_applicable' 
				&& empty($gpd_item["glass_holes_size"][$i]) 
				&& $gpd_item["glass_holes_size"][$i]=='undefined'
				&& $gpd_item["holes_x_location"][$i]=='not_applicable' 
				&& $gpd_item["holes_y_location"][$i]=='not_applicable' 
				&& empty($gpd_item["holes_x_location_cl"][$i]) 
				&& $gpd_item["holes_x_location_cl"][$i]=='undefined'
				&& empty($gpd_item["holes_x_location_cl"][$i]) 
				&& $gpd_item["holes_x_location_cl"][$i]=='undefined'
				&& empty($gpd_item["holes_notes"][$i])
				&& $gpd_item["holes_notes"][$i]=='undefined'
				)
					{
						unset($gpd_item["glass_holes_type"][$i]);
						unset($gpd_item["glass_holes_size"][$i]);
						unset($gpd_item["holes_x_location"][$i]);
						unset($gpd_item["holes_y_location"][$i]);
						unset($gpd_item["holes_x_location_cl"][$i]);
						unset($gpd_item["holes_y_location_cl"][$i]);
						unset($gpd_item["holes_notes"][$i]);		
				
				}else{
					$gpd_item["glass_holes_type"][$i]=	stripslashes($gpd_item["glass_holes_type"][$i]);


					if(is_array($gpd_item)){

						$gpd_item["glass_holes_size"][$i]	=  $gpd_item["glass_holes_size"][$i];
					} else {
					$gpd_item["glass_holes_size"][$i]	=  stripslashes($gpd_item["glass_holes_size"][$i]);
				}
					$gpd_item["holes_x_location"][$i]	=stripslashes($gpd_item["holes_x_location"][$i]);
					$gpd_item["holes_y_location"][$i]	=stripslashes($gpd_item["holes_y_location"][$i]);
					$gpd_item["holes_x_location_cl"][$i]	=stripslashes($gpd_item["holes_x_location_cl"][$i]);
					$gpd_item["holes_y_location_cl"][$i]	=stripslashes($gpd_item["holes_y_location_cl"][$i]);
					$gpd_item["holes_notes"][$i]	=stripslashes($gpd_item["holes_notes"][$i]);
				
				}
		
			}
		
		$gpd_glass_holes_type='';
		//echo "<pre> tester";
		//print_r($gpd_item["glass_holes_size"]);die;



		if(is_array($gpd_item["glass_holes_type"])){
				$gpd_glass_holes_type = serialize(array_values($gpd_item["glass_holes_type"]));
			 }
			
			 $gpd_glass_holes_size='';
			 if(is_array($gpd_item["glass_holes_size"])){
				$gpd_glass_holes_size = serialize(array_values($gpd_item["glass_holes_size"]));
			 }


			 $gpd_glass_offset_data='';
			 if(is_array($gpd_item["offset_data"])){
				$gpd_glass_offset_data = serialize(array_values($gpd_item["offset_data"]));
			 }
			
			 $gpd_holes_x_location='';
			 if(is_array($gpd_item["holes_x_location"])){
				$gpd_holes_x_location = serialize(array_values($gpd_item["holes_x_location"]));
			 }
			
			 $gpd_holes_y_location='';
			 if(is_array($gpd_item["holes_y_location"])){
				$gpd_holes_y_location = serialize(array_values($gpd_item["holes_y_location"]));
			 }
			 
			 $gpd_holes_x_location_cl ='';
			 if(is_array($gpd_item["holes_x_location_cl"])){
				$gpd_holes_x_location_cl = serialize(array_values($gpd_item["holes_x_location_cl"]));
			 }
			
			 $gpd_holes_y_location_cl='';
			 if(is_array($gpd_item["holes_y_location_cl"])){
				$gpd_holes_y_location_cl = serialize(array_values($gpd_item["holes_y_location_cl"]));
			 }
			 
			 $gpd_holes_notes ='';
			 if(is_array($gpd_item["holes_notes"])){
				$gpd_holes_notes = serialize(array_values($gpd_item["holes_notes"]));
			 }

	
		
//			if(!empty($gpd_glass_holes_type)){		
				/*$gpd_glass_holes_type = serialize($gpd_glass_holes_type);		
				//	}
			
			//if(!empty($gpd_glass_holes_size)){
							$gpd_glass_holes_size = serialize($gpd_glass_holes_size);			//}
			
			//if(!empty($gpd_holes_x_location)){		
				$gpd_holes_x_location = serialize($gpd_holes_x_location);			//}
			//if(!empty($gpd_holes_y_location)){		
				$gpd_holes_y_location = serialize($gpd_holes_y_location);			//}
			//if(!empty($gpd_holes_x_location_cl)){	
					$gpd_holes_y_location = serialize($gpd_holes_x_location_cl);		//	}
			//if(!empty($gpd_holes_y_location_cl)){	
					$gpd_holes_y_location = serialize($gpd_holes_y_location_cl);			//}
		//	if(!empty($gpd_holes_notes)){	
					$gpd_holes_notes = serialize($gpd_holes_notes);			//}*/
		
			$gpd_miscellaneous = $gpd_item["miscellaneous"];
			$gpd_glass_grids = $gpd_item["glass_grids"];
			$gpd_pattern_horizontal_grids = $gpd_item["pattern_horizontal_grids"];
			$gpd_pattern_vertical_grids = $gpd_item["pattern_vertical_grids"];
			$gpd_grids_thickness = $gpd_item["grids_thickness"];
			$gpd_grids_color = $gpd_item["grids_color"];
			/*sag-158*/
			$gpd_grids_color_custom = $gpd_item["grids_color_custom"];
			/*sag-158*/

			$gpd_glass_fabrication = $gpd_item["glass_fabrication"];
			$gpd_fabrication_polished_edges = $gpd_item["fabrication_polished_edges"];
			$gpd_fabrication_holes = $gpd_item["fabrication_holes"];
			$gpd_fabrication_pattern = $gpd_item["fabrication_pattern"];

			$exterior_light_thickness = $gpd_item["exterior_light_thickness"];
			$interior_light_thickness = $gpd_item["interior_light_thickness"];
			$select_tgpd_innner_layer_thickness = $gpd_item["select_tgpd_innner_layer_thickness"];
			$custom_layer_thickness = $gpd_item["custom_layer_thickness"];
			$daylight_enable  = $gpd_item["daylight_enable"];
			$gosize_enable  = $gpd_item["gosize_enable"];
			$color_viewer   = $gpd_item["color_viewer"];
			$damage_viewer   = $gpd_item["damage_viewer"];
			/*verify code start fomm here @date 02 Nov 2017*/

			$verify_tgpd_overall_thickness   = $gpd_item['verify_tgpd_overall_thickness'] ;
			$verify_tgpd_exterior_glass_type   = $gpd_item['verify_tgpd_exterior_glass_type'] ;
			$verify_tgpd_interior_glass_type   = $gpd_item['verify_tgpd_interior_glass_type'] ;
			$verify_tgpd_treatment_exterior_glass   = $gpd_item['verify_tgpd_treatment_exterior_glass'] ;
			$verify_tgpd_treatment_interior_glass   = $gpd_item['verify_tgpd_treatment_interior_glass'] ;
			$verify_tgpd_exterior_color_glass_type   = $gpd_item['verify_tgpd_exterior_color_glass_type'] ;
			$verify_tgpd_interior_color_glass_type   = $gpd_item['verify_tgpd_interior_color_glass_type'] ;
			$verify_tgpd_coating_exterior_type   = $gpd_item['verify_tgpd_coating_exterior_type'] ;
			$verify_tgpd_coating_interior_type   = $gpd_item['verify_tgpd_coating_interior_type'] ;
			$verify_tgpd_exterior_lite_thickness   = $gpd_item['verify_tgpd_exterior_lite_thickness'] ;
			$verify_tgpd_interior_lite_thickness   = $gpd_item['verify_tgpd_interior_lite_thickness'] ;
			$verify_tgpd_inner_layer_thickness   = $gpd_item['verify_tgpd_inner_layer_thickness'] ;

			$verify_tgpd_lite_thickness   = $gpd_item['verify_tgpd_lite_thickness'];
			$verify_tgpd_glass_treatment   = $gpd_item['verify_tgpd_glass_treatment'] ;
			$verify_tgpd_glass_color   = $gpd_item['verify_tgpd_glass_color'];
			$verify_tgpd_glass_color_note   = $gpd_item['verify_tgpd_glass_color_note'];
			$verify_tgpd_glass_set   = $gpd_item['verify_tgpd_glass_set'] ;
			$verify_tgpd_glass_lift   = $gpd_item['verify_tgpd_glass_lift'];
			$verify_tgpd_glass_inside_lift_with_glass_type = $gpd_item['verify_tgpd_glass_inside_lift_with_glass_type'];
			$verify_tgpd_glass_outside_lift_with_glass_type = $gpd_item['verify_tgpd_glass_outside_lift_with_glass_type'];
			$text_tgpd_text_lift_inside_position = $gpd_item['text_tgpd_text_lift_inside_position'];
			$text_tgpd_text_lift_outside_position = $gpd_item['text_tgpd_text_lift_outside_position'];
			$text_tgpd_fabrication_polished_edges_hide = stripslashes($gpd_item['text_tgpd_fabrication_polished_edges_hide']);
			$text_tgpd_fabrication_pattern_hide = stripslashes($gpd_item['text_tgpd_fabrication_pattern_hide']);
			$text_tgpd_fabrication_holes_hide_new = stripslashes($gpd_item['text_tgpd_fabrication_holes_hide_new']);
			$text_tgpd_fabrication_holes_hide_old = stripslashes($gpd_item['text_tgpd_fabrication_holes_hide_old']);
			$verify_tgpd_spacer_color   = $gpd_item['verify_tgpd_spacer_color'] ;
			$verify_tgpd_daylight_height   = $gpd_item['verify_tgpd_daylight_height'];
			$verify_tgpd_daylight_width   = $gpd_item['verify_tgpd_daylight_width'];
			$verify_set_height   = $gpd_item['verify_set_height'];
			$verify_set_width   = $gpd_item['verify_set_width'];

			$verify_go_size_height   = $gpd_item['verify_go_size_height'];
			$verify_go_size_width   = $gpd_item['verify_go_size_width'];

			$text_tgpd_extra_width   = $gpd_item['text_tgpd_extra_width'];
			$text_tgpd_extra_height   = $gpd_item['text_tgpd_extra_height'];


			/*verify code start fomm here @date 02 Nov 2017*/
			$pictures_download_url = explode(",blob:", $gpd_pictures_download_url);
			$pictures_final_download_url = str_replace(",,",",",$pictures_download_url[0]);

			$sketches_download_url = explode(",blob:", $gpd_sketches_download_url);
			$sketches_final_download_url = str_replace(",,",",",$sketches_download_url[0]);
			// $sketches_download_url = str_replace(",,",",",$gpd_sketches_download_url)
				//	print_r($gpd_glass_holes_type); echo "<br>";
				//		print_r($gpd_holes_x_location_cl);echo "<br>";
//		print_r($gpd_holes_y_location_cl);die;
		//die;
            $gpd_glass_location = str_replace("\\", "", $gpd_glass_location);
            $wpdb->insert(
            $table_gpd_items,
            array(
              'job_id' => $job_id,
				'glass_holes_type'=>  	$gpd_glass_holes_type,
				'glass_holes_size'=>$gpd_glass_holes_size,
				'glass_offset'=>$gpd_glass_offset_data,
				'holes_x_location'=>$gpd_holes_x_location,
				'holes_y_location'=>$gpd_holes_y_location,
				'holes_x_location_cl'=>$gpd_holes_x_location_cl,
				'holes_y_location_cl'=>$gpd_holes_y_location_cl,
				'holes_notes'=>$gpd_holes_notes,
              'glass_location' => $gpd_glass_location,
              'coating_exterior_type' => $gpd_coating_exterior_type,
              'coating_interior_type' => $gpd_coating_interior_type,

               /***sag 124**/
               'tgpd_coating_family_interior_type' => $tgpd_coating_family_interior_type,
              'tgpd_coating_family_exterior_type' => $tgpd_coating_family_exterior_type,
              'tgpd_coating_family_color_interior_type' => $tgpd_coating_family_color_interior_type,
              'tgpd_coating_family_color_exterior_type' => $tgpd_coating_family_color_exterior_type,


              'verify_tgpd_exterior_coating_family_glass_type' => $verify_tgpd_exterior_coating_family_glass_type,

              'verify_tgpd_interior_coating_family_glass_type' => $verify_tgpd_interior_coating_family_glass_type,

              'verify_tgpd_exterior_coating_family_color_glass_type' => $verify_tgpd_interior_coating_family_color_glass_type,

              'verify_tgpd_interior_coating_family_color_glass_type' => $verify_tgpd_interior_coating_family_color_glass_type,
              /***sag 124**/
			  'glass_quantity' => $gpd_glass_quantity,
				'glass_shape' => $gpd_glass_shape,
				'glass_shape_left_leg' => $gpd_glass_shape_left_leg,
				'glass_shape_right_leg' => $gpd_glass_shape_right_leg,
				'glass_shape_width' => $gpd_glass_shape_width,
				'glass_shape_base' => $gpd_glass_shape_base,
				'glass_shape_side' => $gpd_glass_shape_side,
				'glass_shape_diameter' => $gpd_glass_shape_diameter,
				'glass_shape_radius' => $gpd_glass_shape_radius,
				'glass_shape_radius_one' => $gpd_glass_shape_radius_one,
				'glass_shape_radius_two' => $gpd_glass_shape_radius_two,
				'glass_shape_leg' => $gpd_glass_shape_leg,
				'glass_shape_length' => $gpd_glass_shape_length,
				'glass_shape_left_side' => $gpd_glass_shape_left_side,
				'glass_shape_right_side' => $gpd_glass_shape_right_side,
				'glass_shape_top' => $gpd_glass_shape_top,
				'glass_shape_height' => $gpd_glass_shape_height,
				'glass_shape_left_offset' => $gpd_glass_shape_left_offset,
				'glass_shape_top_offset' => $gpd_glass_shape_top_offset,
				'glass_shape_right_offset' => $gpd_glass_shape_right_offset,
			  'glass_servicetype' => $gpd_glass_servicetype,
              'glass_type' => $gpd_glass_type,
              'lite_thickness' => $gpd_lite_thickness,
              'spacer_color' => $gpd_spacer_color,
              'glass_treatment' => $gpd_glass_treatment,
              'glass_color' => $gpd_glass_color,
              'glass_color_note' => $gpd_glass_color_note,
              'glass_lift' => $gpd_glass_lift,
              'glass_inside_lift_with_glass_type' => $gpd_glass_inside_lift_with_glass_type,
              'glass_outside_lift_with_glass_type' => $gpd_glass_outside_lift_with_glass_type,
              'glass_set' => $gpd_glass_set,
              'daylight_width' => $gpd_daylight_width,
              'daylight_height' => $gpd_daylight_height,
              'go_width' => $gpd_go_width,
              'go_height' => $gpd_go_height,
              'size_verification_status' => $gpd_size_verification_status,
              'overall_thickness' => stripslashes($gpd_overall_thickness),
              'exterior_glass_type' => stripslashes($gpd_exterior_glass_type),
              'interior_glass_type' => stripslashes($gpd_interior_glass_type),
              'interior_glass_type_non_igu' =>stripslashes($gpd_interior_glass_type_non_igu),
              'treatment_exterior_glass' => $gpd_treatment_exterior_glass,
              'treatment_interior_glass' => $gpd_treatment_interior_glass,
              'exterior_color_glass_type' => $gpd_exterior_color_glass_type,
              'interior_color_glass_type' => $gpd_interior_color_glass_type,
              'sag_or_quote' => $gpd_sag_or_quote,
              'disclamers' => $gpd_disclamers,
              'manufacturer' => $gpd_manufacturer,
              'solar_film' => $gpd_solar_film,
              'solar_film_responsibility' => $gpd_solar_film_responsibility,
              'solar_film_type' => $gpd_solar_film_type,
              'solar_film_source' => $gpd_solar_film_source,
              'wet_seal' => $gpd_wet_seal,
              'wet_seal_responsibility' => $gpd_wet_seal_responsibility,
              'furniture_to_move' => $gpd_furniture_to_move,
              'furniture_to_move_comment' => $gpd_furniture_to_move_comment,
              'damage_waiver_select' => $gpd_damage_waiver_select,
              'disclamers_select' => $gpd_disclamers_select,
              'damage_waiver_text' => $gpd_damage_waiver_text,
              'disclamers_text' => $gpd_disclamers_text,
              'walls_or_ceilings_to_cut' => $gpd_walls_or_ceilings_to_cut,
              'walls_or_ceilings_to_cut_responsibility' => $gpd_walls_or_ceilings_to_cut_responsibility,
              'walls_or_ceilings_to_cut_comment' => $gpd_walls_or_ceilings_to_cut_comment,
              'blind_needs_removing' => $gpd_blind_needs_removing,
              'glass_fits_elevator' => $gpd_glass_fits_elevator,
              'pictures_download_url' => $pictures_final_download_url,
              'sketches_download_url' => $sketches_final_download_url,
              'instructions' => $gpd_instructions,
              'caulk_amount' => $gpd_caulk_amount,
              'caulk_type' => $gpd_caulk_type,
              'scaffolding_type' => $gpd_scaffolding_type,
              'quantity_type' => $gpd_quantity_type,
              'tape_amount' => $gpd_tape_amount,
              'tape_type' => $gpd_tape_type,
              'channel' => $gpd_channel,
              'quantity_channel' => $gpd_quantity_channel,
              'miscellaneous' => $gpd_miscellaneous,
              'glass_grids' => $gpd_glass_grids,
              'pattern_horizontal_grids' => $gpd_pattern_horizontal_grids,
              'pattern_vertical_grids' => $gpd_pattern_vertical_grids,
              'grids_thickness' => $gpd_grids_thickness,
              /*sag-158*/
              'grids_color' => $gpd_grids_color,
              'grids_color_custom' => $gpd_grids_color_custom,
              /*sag-158*/
              'glass_fabrication' => $gpd_glass_fabrication,
              'fabrication_polished_edges' => $gpd_fabrication_polished_edges,
              'fabrication_holes' => $gpd_fabrication_holes,
              'fabrication_pattern' => $gpd_fabrication_pattern,
              'exterior_light_thickness'=>$exterior_light_thickness,
              'interior_light_thickness'=>$interior_light_thickness,
              'select_tgpd_innner_layer_thickness'=>$select_tgpd_innner_layer_thickness,
              'custom_layer_thickness'=> $custom_layer_thickness,
			  'daylight_enable'=>$daylight_enable,
			   'gosize_enable'=>$gosize_enable,
			   'color_viewer' => $color_viewer,
			   'damage_viewer' => $damage_viewer,
				'verify_tgpd_overall_thickness' => $verify_tgpd_overall_thickness,
				'verify_tgpd_exterior_glass_type' => $verify_tgpd_exterior_glass_type,
				'verify_tgpd_interior_glass_type' => $verify_tgpd_interior_glass_type,
				'verify_tgpd_treatment_exterior_glass' => $verify_tgpd_treatment_exterior_glass,
				'verify_tgpd_treatment_interior_glass' => $verify_tgpd_treatment_interior_glass,
				'verify_tgpd_exterior_color_glass_type' => $verify_tgpd_exterior_color_glass_type,
				'verify_tgpd_interior_color_glass_type' => $verify_tgpd_interior_color_glass_type,
				'verify_tgpd_coating_exterior_type' => $verify_tgpd_coating_exterior_type,
				'verify_tgpd_coating_interior_type' => $verify_tgpd_coating_interior_type,
				'verify_tgpd_lite_thickness' => $verify_tgpd_lite_thickness,
				'verify_tgpd_glass_treatment' => $verify_tgpd_glass_treatment,
				'verify_tgpd_glass_color' => $verify_tgpd_glass_color,
				'verify_tgpd_glass_color_note' => $verify_tgpd_glass_color_note,
				'verify_tgpd_glass_set' => $verify_tgpd_glass_set,
				'verify_tgpd_glass_lift' => $verify_tgpd_glass_lift,
				'verify_tgpd_glass_inside_lift_with_glass_type' => $verify_tgpd_glass_inside_lift_with_glass_type,
				'verify_tgpd_glass_outside_lift_with_glass_type' => $verify_tgpd_glass_outside_lift_with_glass_type,
				'text_tgpd_text_lift_inside_position' => $text_tgpd_text_lift_inside_position,
				'text_tgpd_text_lift_outside_position' => $text_tgpd_text_lift_outside_position,
				'text_tgpd_fabrication_polished_edges_hide' => $text_tgpd_fabrication_polished_edges_hide,
				'text_tgpd_fabrication_pattern_hide' => $text_tgpd_fabrication_pattern_hide,
				'text_tgpd_fabrication_holes_hide_new' => $text_tgpd_fabrication_holes_hide_new,
				'text_tgpd_fabrication_holes_hide_old' => $text_tgpd_fabrication_holes_hide_old,
				'verify_tgpd_spacer_color' => $verify_tgpd_spacer_color,
				'verify_tgpd_daylight_height' => $verify_tgpd_daylight_height,
				'verify_tgpd_daylight_width' => $verify_tgpd_daylight_width,
				'text_tgpd_extra_width'   => $text_tgpd_extra_width,
				'text_tgpd_extra_height'   => $text_tgpd_extra_height,
				'verify_go_size_height' => $verify_go_size_height,
				'verify_set_height'   => $verify_set_height,
		 		 'verify_set_width'   => $verify_set_width,
				'verify_tgpd_exterior_lite_thickness'   =>$verify_tgpd_exterior_lite_thickness,
				'verify_tgpd_interior_lite_thickness'   => $verify_tgpd_interior_lite_thickness,
				'verify_tgpd_inner_layer_thickness'   => $verify_tgpd_inner_layer_thickness,
				'verify_go_size_width' => $verify_go_size_width
            )
          );

		  if(isset($childJobId)&& $childJobId !=''){
		  	$wpdb->insert(
            $table_gpd_items,
            array(
				'job_id' => $childJobId,
				'glass_location' => $gpd_glass_location,
				'coating_exterior_type' => $gpd_coating_exterior_type,
				'coating_interior_type' => $gpd_coating_interior_type,
				/***sag 124**/
               'tgpd_coating_family_interior_type' => $tgpd_coating_family_interior_type,
              'tgpd_coating_family_exterior_type' => $tgpd_coating_family_exterior_type,
               'tgpd_coating_family_color_interior_type' => $tgpd_coating_family_color_interior_type,
              'tgpd_coating_family_color_exterior_type' => $tgpd_coating_family_color_exterior_type,


              'verify_tgpd_exterior_coating_family_glass_type' => $verify_tgpd_exterior_coating_family_glass_type,

              'verify_tgpd_interior_coating_family_glass_type' => $verify_tgpd_interior_coating_family_glass_type,

              'verify_tgpd_exterior_coating_family_color_glass_type' => $verify_tgpd_interior_coating_family_color_glass_type,

              'verify_tgpd_interior_coating_family_color_glass_type' => $verify_tgpd_interior_coating_family_color_glass_type,

              /***sag 124**/
				'glass_quantity' => $gpd_glass_quantity,
				'glass_shape' => $gpd_glass_shape,
				'glass_shape_left_leg' => $gpd_glass_shape_left_leg,
				'glass_shape_right_leg' => $gpd_glass_shape_right_leg,
				'glass_shape_width' => $gpd_glass_shape_width,
				'glass_shape_base' => $gpd_glass_shape_base,
				'glass_shape_side' => $gpd_glass_shape_side,
				'glass_shape_diameter' => $gpd_glass_shape_diameter,
				'glass_shape_radius' => $gpd_glass_shape_radius,
				'glass_shape_radius_one' => $gpd_glass_shape_radius_one,
				'glass_shape_radius_two' => $gpd_glass_shape_radius_two,
				'glass_shape_leg' => $gpd_glass_shape_leg,
				'glass_shape_length' => $gpd_glass_shape_length,
				'glass_shape_left_side' => $gpd_glass_shape_left_side,
				'glass_shape_right_side' => $gpd_glass_shape_right_side,
				'glass_shape_top' => $gpd_glass_shape_top,
				'glass_shape_height' => $gpd_glass_shape_height,
				'glass_shape_left_offset' => $gpd_glass_shape_left_offset,
				'glass_shape_top_offset' => $gpd_glass_shape_top_offset,
				'glass_shape_right_offset' => $gpd_glass_shape_right_offset,
				'glass_servicetype' => $gpd_glass_servicetype,
				'glass_type' => $gpd_glass_type,
				'lite_thickness' => $gpd_lite_thickness,
				'spacer_color' => $gpd_spacer_color,
				'glass_treatment' => $gpd_glass_treatment,
				'glass_color' => $gpd_glass_color,
				'glass_color_note' => $gpd_glass_color_note,
				'glass_lift' => $gpd_glass_lift,
				'glass_inside_lift_with_glass_type' => $gpd_glass_inside_lift_with_glass_type,
				'glass_outside_lift_with_glass_type' => $gpd_glass_outside_lift_with_glass_type,
				'glass_set' => $gpd_glass_set,
				'daylight_width' => $gpd_daylight_width,
				'daylight_height' => $gpd_daylight_height,
				'go_width' => $gpd_go_width,
				'go_height' => $gpd_go_height,
				'size_verification_status' => $gpd_size_verification_status,
				'overall_thickness' => stripslashes($gpd_overall_thickness),
				'exterior_glass_type' => stripslashes($gpd_exterior_glass_type),
				'interior_glass_type' => stripslashes($gpd_interior_glass_type),
				'interior_glass_type_non_igu' =>stripslashes($gpd_interior_glass_type_non_igu),
				'treatment_exterior_glass' => $gpd_treatment_exterior_glass,
				'treatment_interior_glass' => $gpd_treatment_interior_glass,
				'exterior_color_glass_type' => $gpd_exterior_color_glass_type,
				'interior_color_glass_type' => $gpd_interior_color_glass_type,
				'sag_or_quote' => $gpd_sag_or_quote,
				'disclamers' => $gpd_disclamers,
				'manufacturer' => $gpd_manufacturer,
				'solar_film' => $gpd_solar_film,
				'solar_film_responsibility' => $gpd_solar_film_responsibility,
				'solar_film_type' => $gpd_solar_film_type,
				'solar_film_source' => $gpd_solar_film_source,
				'wet_seal' => $gpd_wet_seal,
				'wet_seal_responsibility' => $gpd_wet_seal_responsibility,
				'furniture_to_move' => $gpd_furniture_to_move,
				'furniture_to_move_comment' => $gpd_furniture_to_move_comment,
				'damage_waiver_select' => $gpd_damage_waiver_select,
				'disclamers_select' => $gpd_disclamers_select,
				'damage_waiver_text' => $gpd_damage_waiver_text,
				'disclamers_text' => $gpd_disclamers_text,
				'walls_or_ceilings_to_cut' => $gpd_walls_or_ceilings_to_cut,
				'walls_or_ceilings_to_cut_responsibility' => $gpd_walls_or_ceilings_to_cut_responsibility,
				'walls_or_ceilings_to_cut_comment' => $gpd_walls_or_ceilings_to_cut_comment,
				'blind_needs_removing' => $gpd_blind_needs_removing,
				'glass_fits_elevator' => $gpd_glass_fits_elevator,
				'pictures_download_url' => str_replace(",,",",",$gpd_pictures_download_url),
				'sketches_download_url' => str_replace(",,",",",$gpd_sketches_download_url),
				'instructions' => $gpd_instructions,
				'caulk_amount' => $gpd_caulk_amount,
				'caulk_type' => $gpd_caulk_type,
				'scaffolding_type' => $gpd_scaffolding_type,
				'quantity_type' => $gpd_quantity_type,
				'tape_amount' => $gpd_tape_amount,
				'tape_type' => $gpd_tape_type,
				'channel' => $gpd_channel,
				'quantity_channel' => $gpd_quantity_channel,
				'miscellaneous' => $gpd_miscellaneous,
				'glass_grids' => $gpd_glass_grids,
				'pattern_horizontal_grids' => $gpd_pattern_horizontal_grids,
				'pattern_vertical_grids' => $gpd_pattern_vertical_grids,
				'grids_thickness' => $gpd_grids_thickness,
				'grids_color' => $gpd_grids_color,
				/*sag-158*/
				'grids_color_custom' => $gpd_grids_color_custom,
				/*sag-158*/
				'glass_fabrication' => $gpd_glass_fabrication,
				'fabrication_polished_edges' => $gpd_fabrication_polished_edges,
				'fabrication_holes' => $gpd_fabrication_holes,
				'fabrication_pattern' => $gpd_fabrication_pattern,
				'exterior_light_thickness'=>$exterior_light_thickness,
				'interior_light_thickness'=>$interior_light_thickness,
				'select_tgpd_innner_layer_thickness'=>$select_tgpd_innner_layer_thickness,
				'custom_layer_thickness'=> $custom_layer_thickness,
				'daylight_enable'=>$daylight_enable,
				'gosize_enable'=>$gosize_enable,
				'color_viewer' => $color_viewer,
				'damage_viewer' => $damage_viewer,
				'verify_tgpd_overall_thickness' => $verify_tgpd_overall_thickness,
				'verify_tgpd_exterior_glass_type' => $verify_tgpd_exterior_glass_type,
				'verify_tgpd_interior_glass_type' => $verify_tgpd_interior_glass_type,
				'verify_tgpd_treatment_exterior_glass' => $verify_tgpd_treatment_exterior_glass,
				'verify_tgpd_treatment_interior_glass' => $verify_tgpd_treatment_interior_glass,
				'verify_tgpd_exterior_color_glass_type' => $verify_tgpd_exterior_color_glass_type,
				'verify_tgpd_interior_color_glass_type' => $verify_tgpd_interior_color_glass_type,
				'verify_tgpd_coating_exterior_type' => $verify_tgpd_coating_exterior_type,
				'verify_tgpd_coating_interior_type' => $verify_tgpd_coating_interior_type,
				'verify_tgpd_lite_thickness' => $verify_tgpd_lite_thickness,
				'verify_tgpd_glass_treatment' => $verify_tgpd_glass_treatment,
				'verify_tgpd_glass_color' => $verify_tgpd_glass_color,
				'verify_tgpd_glass_color_note' => $verify_tgpd_glass_color_note,
				'verify_tgpd_glass_set' => $verify_tgpd_glass_set,
				'verify_tgpd_glass_lift' => $verify_tgpd_glass_lift,
				'verify_tgpd_glass_inside_lift_with_glass_type' => $verify_tgpd_glass_inside_lift_with_glass_type,
				'verify_tgpd_glass_outside_lift_with_glass_type' => $verify_tgpd_glass_outside_lift_with_glass_type,
				'text_tgpd_text_lift_inside_position' => $text_tgpd_text_lift_inside_position,
				'text_tgpd_text_lift_outside_position' => $text_tgpd_text_lift_outside_position,
				'text_tgpd_fabrication_polished_edges_hide' => $text_tgpd_fabrication_polished_edges_hide,
				'text_tgpd_fabrication_pattern_hide' => $text_tgpd_fabrication_pattern_hide,
				'text_tgpd_fabrication_holes_hide_new' => $text_tgpd_fabrication_holes_hide_new,
				'text_tgpd_fabrication_holes_hide_old' => $text_tgpd_fabrication_holes_hide_old,
				'verify_tgpd_spacer_color' => $verify_tgpd_spacer_color,
				'verify_tgpd_daylight_height' => $verify_tgpd_daylight_height,
				'verify_tgpd_daylight_width' => $verify_tgpd_daylight_width,
				'verify_go_size_height' => $verify_go_size_height,
				'verify_go_size_width' => $verify_go_size_width,
				'verify_tgpd_exterior_lite_thickness'   =>$verify_tgpd_exterior_lite_thickness,
				'verify_tgpd_interior_lite_thickness'   => $verify_tgpd_interior_lite_thickness,
				'verify_tgpd_inner_layer_thickness'   => $verify_tgpd_inner_layer_thickness,
				'verify_set_height'   => $verify_set_height,
				'verify_set_width'   => $verify_set_width,
				'text_tgpd_extra_width'   => $text_tgpd_extra_width,
				'text_tgpd_extra_height'   => $text_tgpd_extra_height
            )
          );
		  }
        }
      }



      

    

      //Repair door wala section
      if (!empty($agd_items)) {
      $agd_items = stripslashes_deep($agd_items);
        for($j=0;$j<50;$j++){
          for($k=0;$k<count($agd_items);$k++){
             $agd_items[$k] = stripslashes_deep($agd_items[$k]);
          }
        }
        


        
        foreach ($agd_items as $value) {
            
            
      			//print_r($value);
				foreach($value as $key=>$ss)
				{
					$ss = preg_replace('/,+/', ',', $ss);
					$ss = trim($ss,",");
					$value[$key]=str_replace('\"','"',$ss);

				}

				//print_r($value);

				//die('111');

          switch ($value['category']) {
            case 'gdr':
              $value['job_id'] = $job_id;
			    $value['top_pivot'] = str_replace('\"','"',$value['top_pivot']);
			   $value['header_door_stop'] = str_replace('\"','"',$value['header_door_stop']);
			    $value['type_of_glass'] = str_replace('\"','"',$value['type_of_glass']);
				$value['edge_seal_weatherstrip'] = str_replace('\"','"',$value['edge_seal_weatherstrip']);
				$value['threshold'] = str_replace('\"','"',$value['threshold']);
              $wpdb->show_errors();
              $wpdb->insert( $table_gdr_items, $value);
              
              		//die( $wpdb->show_errors());

              break;



            case 'gdn':
              $value['job_id'] = $job_id;
			  $value['glass_thickness'] = str_replace('\"','"',$value['glass_thickness']);
			   $value['hole_size'] = str_replace('\"','"',$value['hole_size']);
			   if(isset($value['handle_a']) && !empty($value['handle_a'])){
			   		$value['handle_a'] = str_replace('\"','"',$value['handle_a']);
			   }
			   if(isset($value['handle_b']) && !empty($value['handle_b'])){
			   		$value['handle_b'] = str_replace('\"','"',$value['handle_b']);
			   }
			   if(isset($value['handle_c']) && !empty($value['handle_c'])){
			   		$value['handle_c'] = str_replace('\"','"',$value['handle_c']);
			   }
			   if(isset($value['handle_d']) && !empty($value['handle_d'])){
			   		$value['handle_d'] = str_replace('\"','"',$value['handle_d']);
			   }
			   if(isset($value['handle_e']) && !empty($value['handle_e'])){
			   		$value['handle_e'] = str_replace('\"','"',$value['handle_e']);
			   }

			
              $wpdb->show_errors();
              $wpdb->insert( $table_gdn_items, $value);
              break;

            case 'sfd':
              $value['job_id'] = $job_id;
			   $value['top_pivot_frame'] = str_replace('\"','"',$value['top_pivot_frame']);
			   $value['top_pivot_door'] = str_replace('\"','"',$value['top_pivot_door']);
			   $value['type_of_glass'] = str_replace('\"','"',$value['type_of_glass']);
			   $value['bottom_pivot'] = str_replace('\"','"',$value['bottom_pivot']);
			    $value['spindle'] = str_replace('\"','"',$value['spindle']);
			   $value['threshold'] = str_replace('\"','"',$value['threshold']);
			   $value['type_of_beads'] = str_replace('\"','"',$value['type_of_beads']);
			    $value['lock_type'] = str_replace('\"','"',$value['lock_type']);
			  // $value['handles'] = str_replace('\"','"',$value['hung_handles']);
			    $value['continuous_hinge'] = str_replace('\"','"',$value['continuous_hinge']);




              $wpdb->show_errors();
              $wpdb->insert( $table_sfd_items, $value);
              break;

            case 'wmd':
              $value['job_id'] = $job_id;

			  
			   $value['threshold'] = str_replace('\"','"',$value['threshold']);
			   $value['continuous_hinge'] = str_replace('\"','"',$value['continuous_hinge']);
			   $value['top_hinge'] = str_replace('\"','"',$value['top_hinge']);
			   $value['intermediate_hinge'] = str_replace('\"','"',$value['intermediate_hinge']);
			   $value['bottom_hinge'] = str_replace('\"','"',$value['bottom_hinge']);




              $wpdb->show_errors();
              $wpdb->insert( $table_wmd_items, $value);
              break;


              case 'sdbg':

              if($value['id'] > 0){

              	$wpdb->delete(	$table_sdbg_items,	array('id' => $value['id'],'job_id'=>$job_id	));

              	  $value['job_id'] = $job_id;
				  $wpdb->show_errors();
	              $wpdb->insert( $table_sdbg_items, $value);

              } else {


	              $value['job_id'] = $job_id;
				  $wpdb->show_errors();
	              $wpdb->insert( $table_sdbg_items, $value);
	          }
              break;



            default:
              # code...
              break;
          }
        }
      }


    } else {
      echo json_encode('{ rows_affected:' . $rows_affected . '}');
      wp_die();
    }
	if(isset($childJobId) && $childJobId!=''){
		$redirect_url = esc_url(add_query_arg('job_id', $childJobId, get_permalink( get_page_by_title( 'Add Job' ) )));
	}else{
		$redirect_url = get_permalink( get_page_by_title( 'Current Jobs' ) ). "?printjobtrue=1&jobs_id=" . $job_id;
	}


	//$redirect_url+="?jobs_id=" . $job_id;






    $data = array(
      "redirect_url" => $redirect_url,
    );
    echo json_encode( $data );
    wp_die();
  }


	public function deleteRecord($table_flag,$tableName,$job_id){
		global $wpdb;
		
	
		$table_nwdr_items = $wpdb->prefix. 'nwdr_items';
		$table_nwaluft_items = $wpdb->prefix. 'nwaluft_items';
		$table_nwaluff_items = $wpdb->prefix. 'nwaluff_items';
		$table_nwaluof_items = $wpdb->prefix. 'nwaluof_items';
		$table_hmnd_items = $wpdb->prefix. 'hmnd_items';
		$table_hmnf_items = $wpdb->prefix. 'hmnf_items';
	
		if($tableName==$table_nwdr_items && $table_flag=false)
		{
			$this->table_nwdr_items_flag=true;	
			$wpdb->delete(	$tableName,	array('job_id' => $job_id	));
		}else if($tableName==$table_nwaluof_items && $table_flag=false)
		{
			$this->table_nwaluof_items_flag=true;
			$wpdb->delete(	$tableName,	array('job_id' => $job_id	));
			}
			else if($tableName==$table_nwaluft_items && $table_flag=false)
		{
			$this->table_nwaluft_items_flag=true;
			$wpdb->delete(	$tableName,	array('job_id' => $job_id	));
			}
			else if($tableName==$table_nwaluff_items && $table_flag=false)
			{
				$this->table_nwaluff_items_flag=true;
				$wpdb->delete(	$tableName,	array('job_id' => $job_id	));
			}	
			else if($tableName==$table_hmnd_items && $table_flag=false)
			{
				$this->table_hmnd_items_flag=true;
				$wpdb->delete(	$tableName,	array('job_id' => $job_id	));
			}
			else if($tableName==$table_hmnf_items && $table_flag=false)
			{
				$this->table_hmnf_items_flag=true;
				$wpdb->delete(	$tableName,	array('job_id' => $job_id	));
			}					
		
		
			
		
		
	
	}

  public function get_job_item() {
      
    sleep(5);
    $job_id = isset($_POST["job_id"]) ? intval(htmlspecialchars($_POST["job_id"])) : 0;

    //echo $job_id; die;

    global $wpdb;

    if(isset($_POST["printing"])){
       
      $getdata= $wpdb->get_results('SELECT pdf_version FROM  wp_4twsj5a02k_sag_jobs WHERE id ='.$_POST["job_id"].'');
        $count = $getdata[0]->pdf_version;
        if($count==''){
        $count1=1;
        }else{$count1=$count + 1;}
        $updatestatessubmit = $wpdb->update(
            'wp_4twsj5a02k_sag_jobs',
            array(
              'pdf_version' => $count1,
          ),
            array(   'id' => $_POST["job_id"] )
        );
    }
  	$table_sag_jobs = $wpdb->prefix . 'sag_jobs';
    $table_gpd_items = $wpdb->prefix . 'gpd_items';
    $table_gdr_items = $wpdb->prefix . 'gdr_items';
    $table_gdn_items = $wpdb->prefix . 'gdn_items';
    $table_sdbg_items = $wpdb->prefix . 'sdbg_items';
    $table_sfd_items = $wpdb->prefix . 'sfd_items';
    $table_wmd_items = $wpdb->prefix . 'wmd_items';
    $table_notes_items = $wpdb->prefix. 'notes_items';
    $table_temp_upload = $wpdb->prefix. 'temp_upload';
    $table_nwdr_items = $wpdb->prefix. 'nwdr_items';
    $table_nwaluft_items = $wpdb->prefix. 'nwaluft_items';
    $table_nwaluff_items = $wpdb->prefix. 'nwaluff_items';
	$table_nwaluof_items = $wpdb->prefix. 'nwaluof_items';
    $table_hmnd_items = $wpdb->prefix. 'hmnd_items';
    $table_hmnf_items = $wpdb->prefix. 'hmnf_items';
    $table_patio_new_door = $wpdb->prefix. 'nw_patio_items';

    $jobs_sql = $wpdb->prepare( "SELECT * FROM " . $table_sag_jobs . " WHERE id = %d", $job_id );
    $gpd_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_gpd_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );

    $nwdr_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_nwdr_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );
    $nwaluft_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_nwaluft_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );
    $nwaluff_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_nwaluff_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );

	 $nwaluof_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_nwaluof_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );

    $hmnd_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_hmnd_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );
    $hmnf_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_hmnf_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );

    $notes_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_notes_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );


    $gdr_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_gdr_items . " WHERE job_id = %d ORDER BY id asc ", $job_id );
   

    $gdn_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_gdn_items . " WHERE job_id = %d ORDER BY id asc", $job_id );
  

    $sfd_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_sfd_items . " WHERE job_id = %d ORDER BY id asc", $job_id );
 
    $wmd_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_wmd_items . " WHERE job_id = %d ORDER BY id asc", $job_id );


    $sdbg_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_sdbg_items . " WHERE job_id = %d ORDER BY id asc", $job_id );

    $patio_items_sql = $wpdb->prepare( "SELECT * FROM " . $table_patio_new_door . " WHERE job_id = %d ORDER BY id asc", $job_id );
 

    $sag_jobs = $wpdb->get_results( $jobs_sql, ARRAY_A );
    $gpd_items = $wpdb->get_results( $gpd_items_sql, ARRAY_A );
	$checkDate = explode(" ", $sag_jobs[0]['created_date']);
	//add to unserialize data
	$sag_jobs[0]['labor_hours_type'] = unserialize($sag_jobs[0]['labor_hours_type']);
	$sag_jobs[0]['labor_number_of_hours'] = unserialize($sag_jobs[0]['labor_number_of_hours']);
	$sag_jobs[0]['labor_number_of_men'] = unserialize($sag_jobs[0]['labor_number_of_men']);
	$sag_jobs[0]['labor_notes'] = unserialize($sag_jobs[0]['labor_notes']);
	
	
	
	
	//$gpd_items[0]['caulk_amount']= unserialize($gpd_items[0]['caulk_amount']);
	//$gpd_items[0]['caulk_type']= unserialize($gpd_items[0]['caulk_type']);
	//$gpd_items[0]['scaffolding_type']= unserialize($gpd_items[0]['scaffolding_type']);
	

	
	for($i=0; $i<count($gpd_items);$i++){
		//
		//2018-03-12
		if($checkDate  > '2018-03-12'){
				
				/*
				,,holes_y_location,holes_x_location_cl,holes_y_location_cl,holes_notes
				*/
    	$bypassArr = array('daylight_width','daylight_height','go_width','go_height');
        #$gpd_items[$i] = stripslashes_deep($gpd_items[$i]);
        for($j=0;$j<50;$j++){
          //for($k=0;$k<count($gpd_items[$i]);$k++){
             //$gpd_items[$i][$k] = stripslashes_deep($gpd_items[$i][$k]);
             if(!in_array($gpd_items[$i],$bypassArr)){
                $gpd_items[$i] = stripslashes_deep($gpd_items[$i]);
             }
         //}
        }
				
			
			
				if(unserialize($gpd_items[$i]['holes_notes'])==false ){
					if(empty ($gpd_items[$i]['holes_notes']) ){
						$gpd_items[$i]['holes_notes']=='';
					}else{
					$gpd_items[$i]['holes_notes']= unserialize(serialize(array($gpd_items[$i]['holes_notes'])));
					}
				}else{
				
					$gpd_items[$i]['holes_notes']= unserialize($gpd_items[$i]['holes_notes']);
				}
			
				if(unserialize($gpd_items[$i]['holes_y_location_cl'])==false ){
					if(empty ($gpd_items[$i]['holes_y_location_cl']) ){
						$gpd_items[$i]['holes_y_location_cl']=='';
					}else{
					$gpd_items[$i]['holes_y_location_cl']= unserialize(serialize(array($gpd_items[$i]['holes_y_location_cl'])));
					}
				}else{
				
					$gpd_items[$i]['holes_y_location_cl']= unserialize($gpd_items[$i]['holes_y_location_cl']);
				}
				
				
				if(unserialize($gpd_items[$i]['holes_x_location_cl'])==false ){
					if(empty ($gpd_items[$i]['holes_x_location_cl']) ){
						$gpd_items[$i]['holes_x_location_cl']=='';
					}else{
					$gpd_items[$i]['holes_x_location_cl']= unserialize(serialize(array($gpd_items[$i]['holes_x_location_cl'])));
					}
				}else{
				
					$gpd_items[$i]['holes_x_location_cl']= unserialize($gpd_items[$i]['holes_x_location_cl']);
				}
				
				
				if(unserialize($gpd_items[$i]['holes_y_location'])==false ){
					if(empty ($gpd_items[$i]['holes_y_location']) ){
						$gpd_items[$i]['holes_y_location']=='';
					}else{
					$gpd_items[$i]['holes_y_location']= unserialize(serialize(array($gpd_items[$i]['holes_y_location'])));
					}
				}else{
				
					$gpd_items[$i]['holes_y_location']= unserialize($gpd_items[$i]['holes_y_location']);
				}
				
				
				if(unserialize($gpd_items[$i]['holes_x_location'])==false ){
					if(empty ($gpd_items[$i]['holes_x_location']) ){
						$gpd_items[$i]['holes_x_location']=='';
					}else{
					$gpd_items[$i]['holes_x_location']= unserialize(serialize(array($gpd_items[$i]['holes_x_location'])));
					}
				}else{
				
					$gpd_items[$i]['holes_x_location']= unserialize($gpd_items[$i]['holes_x_location']);
				}
				
				if(unserialize($gpd_items[$i]['glass_holes_size'])==false ){
					if(empty ($gpd_items[$i]['glass_holes_size']) ){
						$gpd_items[$i]['glass_holes_size']=='';
					}else{
					$gpd_items[$i]['glass_holes_size']= unserialize(serialize(array($gpd_items[$i]['glass_holes_size'])));
					}




				}else{
				
					$gpd_items[$i]['glass_holes_size']= unserialize($gpd_items[$i]['glass_holes_size']);


				}


				if(unserialize($gpd_items[$i]['glass_offset'])==false ){
					if(empty ($gpd_items[$i]['glass_offset']) ){
						$gpd_items[$i]['glass_offset']=='';
					}else{
					$gpd_items[$i]['glass_offset']= unserialize(serialize(array($gpd_items[$i]['glass_offset'])));
					}

				}else{
				
					$gpd_items[$i]['glass_offset']= unserialize($gpd_items[$i]['glass_offset']);
				}




				#var_dump(unserialize($gpd_items[$i]['glass_holes_type']));
				#die;
				if(unserialize($gpd_items[$i]['glass_holes_type'])==false ){
					if(empty ($gpd_items[$i]['glass_holes_type']) ){
						$gpd_items[$i]['glass_holes_type']=='';
					}else{
					$gpd_items[$i]['glass_holes_type']= unserialize(serialize(array($gpd_items[$i]['glass_holes_type'])));
					}
				}else{
				
					$gpd_items[$i]['glass_holes_type']= unserialize($gpd_items[$i]['glass_holes_type']);
				}
				
			}else{
                    $gpd_items[$i]['glass_holes_size']='';
                    $gpd_items[$i]['glass_holes_type']='';
                    $gpd_items[$i]['holes_notes']='';
                    $gpd_items[$i]['holes_y_location_cl']='';
                    $gpd_items[$i]['holes_x_location_cl']='';
                    $gpd_items[$i]['holes_y_location']='';
                    $gpd_items[$i]['holes_x_location']='';
			    
			}
		
			if(unserialize($gpd_items[$i]['caulk_amount'])==false ){
					if(empty ($gpd_items[$i]['caulk_amount']) ){
						$gpd_items[$i]['caulk_amount']=='';
					}else{
					$gpd_items[$i]['caulk_amount']= unserialize(serialize(array($gpd_items[$i]['caulk_amount'])));
					}
			}else{
				
					$gpd_items[$i]['caulk_amount']= unserialize($gpd_items[$i]['caulk_amount']);
					
					if(!is_array($gpd_items[$i]['caulk_amount'])){
						$gpd_items[$i]['caulk_amount']= unserialize($gpd_items[$i]['caulk_amount']);
					}
					
			}
		
			if(unserialize($gpd_items[$i]['caulk_type'])==false)
			{
				if(empty ($gpd_items[$i]['caulk_type']) ){
				$gpd_items[$i]['caulk_type']=='';
				}else{
			
				$gpd_items[$i]['caulk_type']= unserialize(serialize(array($gpd_items[$i]["caulk_type"])));
						if(!is_array($gpd_items[$i]['caulk_type'])){
							$gpd_items[$i]['caulk_type']= unserialize($gpd_items[$i]['caulk_type']);
						}
				}
			}
			else{
				//print_r($gpd_items[$i]['caulk_type']);die;
			$gpd_items[$i]['caulk_type']= unserialize($gpd_items[$i]['caulk_type']);
    			if(strpos($gpd_items[$i]['caulk_type'],':')>0){
    			    
    			    	$gpd_items[$i]['caulk_type']= unserialize($gpd_items[$i]['caulk_type']);
    			}
			}
			
			if(unserialize($gpd_items[$i]['scaffolding_type'])==false){
				if(empty ($gpd_items[$i]['scaffolding_type']) ){
				$gpd_items[$i]['scaffolding_type']=='';
				}else{
			$gpd_items[$i]['scaffolding_type'] = unserialize(serialize(array($gpd_items[$i]['scaffolding_type'])));
			
				if(!is_array($gpd_items[$i]['scaffolding_type'])){
							$gpd_items[$i]['scaffolding_type']= unserialize($gpd_items[$i]['scaffolding_type']);
						}
			}
			}
			else{
			$gpd_items[$i]['scaffolding_type']= unserialize($gpd_items[$i]['scaffolding_type']);
				if(!is_array($gpd_items[$i]['scaffolding_type'])){
							$gpd_items[$i]['scaffolding_type']= unserialize($gpd_items[$i]['scaffolding_type']);
						}
			}
			
			
			if(unserialize($gpd_items[$i]['quantity_type'])==false){
				if(empty ($gpd_items[$i]['quantity_type']) ){
				$gpd_items[$i]['quantity_type']=='';
				}else{
			$gpd_items[$i]['quantity_type'] = unserialize(serialize(array($gpd_items[$i]['quantity_type'])));
			
				if(!is_array($gpd_items[$i]['quantity_type'])){
							$gpd_items[$i]['quantity_type']= unserialize($gpd_items[$i]['quantity_type']);
						}
			}
			}
			else{
			$gpd_items[$i]['quantity_type']= unserialize($gpd_items[$i]['quantity_type']);
				if(!is_array($gpd_items[$i]['quantity_type'])){
							$gpd_items[$i]['quantity_type']= unserialize($gpd_items[$i]['quantity_type']);
						}
			}
			


			/****change sag 129****/
			

			if(unserialize($gpd_items[$i]['quantity_channel'])==false){
				if(empty ($gpd_items[$i]['quantity_channel']) ){
				$gpd_items[$i]['quantity_channel']=='';
				}else{
			$gpd_items[$i]['quantity_channel'] = unserialize(serialize(array($gpd_items[$i]['quantity_channel'])));
			
				if(!is_array($gpd_items[$i]['quantity_channel'])){
							$gpd_items[$i]['quantity_channel']= unserialize($gpd_items[$i]['quantity_channel']);
						}
			}
			}
			else{
			$gpd_items[$i]['quantity_channel']= unserialize($gpd_items[$i]['quantity_channel']);
				if(!is_array($gpd_items[$i]['quantity_channel'])){
							$gpd_items[$i]['quantity_channel']= unserialize($gpd_items[$i]['quantity_channel']);
						}
			}

			/****change sag 129 end****/
			if( unserialize($gpd_items[$i]['tape_amount'])==false){
				if(empty ($gpd_items[$i]['tape_amount'])){
					$gpd_items[$i]['tape_amount']=='';
					//die('1');
				}
				else{
					$gpd_items[$i]['tape_amount'] = unserialize(serialize(array($gpd_items[$i]['tape_amount'])));
					
					
					if(!is_array($gpd_items[$i]['tape_amount'])){
							$gpd_items[$i]['tape_amount']= unserialize($gpd_items[$i]['tape_amount']);
						}
					//die('2');
				}
			}
			else{
				$gpd_items[$i]['tape_amount']= unserialize($gpd_items[$i]['tape_amount']);
				
				if(!is_array($gpd_items[$i]['tape_amount'])){
							$gpd_items[$i]['tape_amount']= unserialize($gpd_items[$i]['tape_amount']);
						}
				
			}
			
		//	$gpd_tape_amount = serialize($gpd_item["tape_amount"]);
		//	$gpd_items[$i]['tape_type']= unserialize($gpd_items[$i]['tape_type']);
		//$gpd_items[$i]['channel']= unserialize($gpd_items[$i]['channel']);
		//echo $gpd_items[$i]['tape_amount'];die('>>>>>.ghdsfghsdgfhsdghfgsdf');
	//print_r(unserialize($gpd_items[$i]['tape_type']));die('>>>>>>>>>>>>>>>>>>>>fgffg');
		if(unserialize($gpd_items[$i]['tape_type'])==false){
				if(empty ($gpd_items[$i]['tape_type'])){
			
					$gpd_items[$i]['tape_type']=='';
					
				}
				else{
					$gpd_items[$i]['tape_type'] = unserialize(serialize(array($gpd_items[$i]['tape_type'])));
					
					if(!is_array($gpd_items[$i]['tape_type'])){
							$gpd_items[$i]['tape_type']= unserialize($gpd_items[$i]['tape_type']);
						}
				}
				
			}
			else{
			$gpd_items[$i]['tape_type'] = unserialize($gpd_items[$i]['tape_type']);
			
				if(!is_array($gpd_items[$i]['tape_type'])){
							$gpd_items[$i]['tape_type']= unserialize($gpd_items[$i]['tape_type']);
						}
			
			}
		
			if(unserialize($gpd_items[$i]['channel'])==false ){
				if(empty ($gpd_items[$i]['channel'])){
					$gpd_items[$i]['channel']=='';
				}else{
					$gpd_items[$i]['channel']= unserialize(serialize(array($gpd_items[$i]['channel'])));
					
					if(!is_array($gpd_items[$i]['channel'])){
							$gpd_items[$i]['channel']= unserialize($gpd_items[$i]['channel']);
						}
				}
			}
			else{
				$gpd_items[$i]['channel'] = unserialize($gpd_items[$i]['channel']);
				if(!is_array($gpd_items[$i]['channel'])){
							$gpd_items[$i]['channel']= unserialize($gpd_items[$i]['channel']);
						}
			}
			
		}
			
	
	
//print_r($gpd_items[0]['caulk_amount']);die;
    $nwdr_items['nwdr'] = array();
    $result = $wpdb->get_results( $nwdr_items_sql, ARRAY_A );
    if (!empty($result)) { $nwdr_items['nwdr'] = $result; }


    $nwdr_items['nwaluft'] = array();
    $result = $wpdb->get_results( $nwaluft_items_sql, ARRAY_A );
    if (!empty($result)) { $nwdr_items['nwaluft'] = $result; }

    $nwdr_items['nwaluff'] = array();
    $result = $wpdb->get_results( $nwaluff_items_sql, ARRAY_A );
    if (!empty($result)) { $nwdr_items['nwaluff'] = $result; }

	   $nwdr_items['nwaluof'] = array();
    $result = $wpdb->get_results( $nwaluof_items_sql, ARRAY_A );
    if (!empty($result)) { $nwdr_items['nwaluof'] = $result; }


    $nwdr_items['hmnd'] = array();
    $result = $wpdb->get_results( $hmnd_items_sql, ARRAY_A );
    if (!empty($result)) { $nwdr_items['hmnd'] = $result; }

    $nwdr_items['hmnf'] = array();
    $result = $wpdb->get_results( $hmnf_items_sql, ARRAY_A );
    if (!empty($result)) { $nwdr_items['hmnf'] = $result; }

     $nwdr_items['patio'] = array();
    $result = $wpdb->get_results( $patio_items_sql, ARRAY_A );

   

	if (!empty($result)) { 
	$filter1 = array();
          		foreach ($result as $key => $val) {
          			if ($key != 'tgpd_caulk_amount'){
          				if ($val[tgpd_caulk_amount] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else if($key != 'tgpd_caulk_type'){
          				if ($val[tgpd_caulk_amount] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else if($key != 'tgpd_channel'){
          				if ($val[tgpd_channel] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			}  else if($key != 'tgpd_quantity_channel'){
          				if ($val[tgpd_quantity_channel] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else if($key != 'tgpd_quantity_type'){
          				if ($val[tgpd_quantity_type] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else if($key != 'tgpd_scaffolding_type'){
          				if ($val[tgpd_scaffolding_type] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else if($key != 'tgpd_tape_amount'){
          				if ($val[tgpd_tape_amount] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else if($key != 'tgpd_tape_type'){
          				if ($val[tgpd_tape_type] != ''){
          					$filter1[$key] = unserialize($val);
          				}
          			} else {

          				$filter1[$key] = $val;

          			}
          		}


    	$nwdr_items['patio'] = $result; }


    foreach($nwdr_items as $key=>$nwdr_item){
        $nwdr_items[$key] = stripslashes_deep($nwdr_items[$key]);
        for($j=0;$j<50;$j++){
          for($k=0;$k<count($nwdr_items[$key]);$k++){
             $nwdr_items[$key][$k] = stripslashes_deep($nwdr_items[$key][$k]);
          }
        }
    }


    $notes_items = $wpdb->get_results( $notes_items_sql, ARRAY_A );

    $agd_items['gdr'] = array();
    $result = $wpdb->get_results( $gdr_items_sql, ARRAY_A );
    if (!empty($result)) { $agd_items['gdr'] = $result; }
    /*$gdr_a_single_items = $wpdb->get_results( $gdr_a_single_items_sql, ARRAY_A );
    $gdr_a_pair_items = $wpdb->get_results( $gdr_a_pair_items_sql, ARRAY_A );
    $gdr_f_single_items = $wpdb->get_results( $gdr_f_single_items_sql, ARRAY_A );
    $gdr_f_pair_items = $wpdb->get_results( $gdr_f_pair_items_sql, ARRAY_A );
    $gdr_bp_single_items = $wpdb->get_results( $gdr_bp_single_items_sql, ARRAY_A );
    $gdr_bp_pair_items = $wpdb->get_results( $gdr_bp_pair_items_sql, ARRAY_A );
    $gdr_p_single_items = $wpdb->get_results( $gdr_p_single_items_sql, ARRAY_A );
    $gdr_p_pair_items = $wpdb->get_results( $gdr_p_pair_items_sql, ARRAY_A );
    $gdr_wp_single_items = $wpdb->get_results( $gdr_wp_single_items_sql, ARRAY_A );
    $gdr_wp_pair_items = $wpdb->get_results( $gdr_wp_pair_items_sql, ARRAY_A );
    */

    $agd_items['gdn'] = array();
    $result = $wpdb->get_results( $gdn_items_sql, ARRAY_A );
    if (!empty($result)) { $agd_items['gdn'] = $result; }
    /*$gdn_a_single_items = $wpdb->get_results( $gdn_a_single_items_sql, ARRAY_A );
    $gdn_f_single_items = $wpdb->get_results( $gdn_f_single_items_sql, ARRAY_A );
    $gdn_bp_single_items = $wpdb->get_results( $gdn_bp_single_items_sql, ARRAY_A );
    $gdn_p_single_items = $wpdb->get_results( $gdn_p_single_items_sql, ARRAY_A );
    $gdn_wp_single_items = $wpdb->get_results( $gdn_wp_single_items_sql, ARRAY_A );
    */

    $agd_items['sfd'] = array();
    $result = $wpdb->get_results( $sfd_items_sql, ARRAY_A );
    if (!empty($result)) { $agd_items['sfd'] = $result; }
    //$agd_items[] = $wpdb->get_results( $sfd_items_sql, ARRAY_A );
    /*$sfd_center_hung_items = $wpdb->get_results( $sfd_center_hung_items_sql, ARRAY_A );
    $sfd_offset_pivots_items = $wpdb->get_results( $sfd_offset_pivots_items_sql, ARRAY_A );
    $sfd_continuous_hinge_items = $wpdb->get_results( $sfd_continuous_hinge_items_sql, ARRAY_A );
    */

    $agd_items['wmd'] = array();
    $result = $wpdb->get_results( $wmd_items_sql, ARRAY_A );
    if (!empty($result)) { $agd_items['wmd'] = $result; }
    
    
    


    $agd_items['sdbg'] = array();
    $result = $wpdb->get_results( $sdbg_items_sql, ARRAY_A );
    if (!empty($result)) { $agd_items['sdbg'] = $result; }
    
    
    foreach($agd_items as $key=>$agd_item){
        $agd_items[$key] = stripslashes_deep($agd_items[$key]);
        for($j=0;$j<50;$j++){
          for($k=0;$k<count($agd_items[$key]);$k++){
             $agd_items[$key] = stripslashes_deep($agd_items[$key]);
          }
        }
    } 


    
    //$agd_items[] = $wpdb->get_results( $wmd_items_sql, ARRAY_A );
    /*$wmd_continuous_hinge_items = $wpdb->get_results( $wmd_continuous_hinge_items_sql, ARRAY_A );
    $wmd_butt_hinge_items = $wpdb->get_results( $wmd_butt_hinge_items_sql, ARRAY_A );
    */

    /*$agd_items = array(
      'gdr' => array(
        'a' => array(
          'single' => $gdr_a_single_items[0],
          'pair' => $gdr_a_pair_items[0]
        ),
        'f' => array(
          'single' => $gdr_f_single_items[0],
          'pair' => $gdr_f_pair_items[0]
        ),
        'bp' => array(
          'single' => $gdr_bp_single_items[0],
          'pair' => $gdr_bp_pair_items[0]
        ),
        'p' => array(
          'single' => $gdr_p_single_items[0],
          'pair' => $gdr_p_pair_items[0]
        ),
        'wp' => array(
          'single' => $gdr_wp_single_items[0],
          'pair' => $gdr_wp_pair_items[0]
        )
      ),
      'gdn' => array(
        'a' => array(
          'single' => $gdn_a_single_items[0]
        ),
        'f' => array(
          'single' => $gdn_f_single_items[0]
        ),
        'bp' => array(
          'single' => $gdn_bp_single_items[0]
        ),
        'p' => array(
          'single' => $gdn_p_single_items[0]
        ),
        'wp' => array(
          'single' => $gdn_wp_single_items[0]
        )
      ),
      'sfd' => array(
        'center_hung' => $sfd_center_hung_items[0],
        'offset_pivots' => $sfd_offset_pivots_items[0],

        'continuous_hinge' => $sfd_continuous_hinge_items[0]
      ),
      'wmd' => array(
        'continuous_hinge' => $wmd_continuous_hinge_items[0],
        'butt_hinge' => $wmd_butt_hinge_items[0]
      )
    );*/
    if(!isset($_POST["printing"])){
      $multi_agd_items = $agd_items;
      unset($agd_items);
      $agd_items = array();
      foreach ($multi_agd_items as  $value) {
        foreach ($value as $innerValue) {
         $agd_items[] = $innerValue;
        }
      }
      $multi_nwdr_items = $nwdr_items;
      unset($nwdr_items);
      $nwdr_items = array();
      foreach ($multi_nwdr_items as  $value) {
        foreach ($value as $innerValue) {
         $nwdr_items[] = $innerValue;
        }
      }
    }

    $sag_jobs[0]["gpd_items"] = $gpd_items;
    $sag_jobs[0]["agd_items"] = $agd_items;
    $sag_jobs[0]["nwdr_items"] = $nwdr_items;

	$sag_jobs[0]["notes_items"] = $notes_items;
	
	
//	print_r($sag_jobs);
	
//	die('test');
	// 31/08/2018 for CORS ERROR
	/*if(isset($_POST["printing"])){
	   if(count($sag_jobs[0]["gpd_items"]) > 0)
	   {
	       foreach($sag_jobs[0]["gpd_items"] as $k=>$v)
	       {
	           if(count($v) > 0)
	           {
	               foreach($v as $key=>$val)
	               {
	                   if (array_key_exists("pictures_download_url",$val))
	                   {
	                       if($val['pictures_download_url']!='' && $val['pictures_download_url'] != null)
	                       {
	                           $type="gpd_items_".$k."_".$key;
	                           $sag_jobs[0]["gpd_items"][$k][$key]["pictures_download_url"] = $this->loadImagesNew($sag_jobs[0]["gpd_items"][$k][$key]["pictures_download_url"],$_POST["job_id"],$type);
	                       }
	                   }
	               }
	           }
	       }
	   }
	   if(count($sag_jobs[0]["agd_items"]) > 0)
	   {
	       foreach($sag_jobs[0]["agd_items"] as $k=>$v)
	       {
	           if(count($v) > 0)
	           {
	               foreach($v as $key=>$val)
	               {
	                   if (array_key_exists("pictures_download_url",$val))
	                   {
	                     if($val['pictures_download_url']!='' && $val['pictures_download_url'] != null)
	                       {
	                           $type="agd_items_".$k."_".$key;
	                           $sag_jobs[0]["agd_items"][$k][$key]["pictures_download_url"] = $this->loadImagesNew($sag_jobs[0]["agd_items"][$k][$key]["pictures_download_url"],$_POST["job_id"],$type);
	                       }
	                   }
	               }
	           }
	       }
	   }
	   if(count($sag_jobs[0]["nwdr_items"]) > 0)
	   {
	       foreach($sag_jobs[0]["nwdr_items"] as $k=>$v)
	       {
	           if(count($v) > 0)
	           {
	               foreach($v as $key=>$val)
	               {
	                   if (array_key_exists("pictures_download_url",$val))
	                   {
	                       if($val['pictures_download_url']!='' && $val['pictures_download_url'] != null)
	                       {
	                           $type="nwdr_items_".$k."_".$key;
	                           $sag_jobs[0]["nwdr_items"][$k][$key]["pictures_download_url"] = $this->loadImagesNew($sag_jobs[0]["nwdr_items"][$k][$key]["pictures_download_url"],$_POST["job_id"],$type);
	                       }
	                   }
	               }
	           }
	       }
	   }
	   if(count($sag_jobs[0]["notes_items"]) > 0)
	   {
	       foreach($sag_jobs[0]["notes_items"] as $k=>$v)
	       {
	           if(count($v) > 0)
	           {
	               
                        if (array_key_exists("notes_upload_pic",$v))
                        {
                            if($v['notes_upload_pic']!='' && $v['notes_upload_pic'] != null)
                            {
                                $type="notes_items_".$k;
                                $sag_jobs[0]["notes_items"][$k]["notes_upload_pic"] = $this->loadImagesNew($sag_jobs[0]["notes_items"][$k]["notes_upload_pic"],$_POST["job_id"],$type);
                            }
                        }
	               
	           }
	       }
	   }
	}*/
	// 31/08/2018 for CORS ERROR
    echo json_encode( $sag_jobs );
    wp_die();
  }
  // 31/08/2018 for CORS ERROR to load images at local
   public function loadImagesNew($imgString,$jobId,$type) {
        $upload_dir   = wp_upload_dir();
        $upDir=$upload_dir['basedir']."/tmp/".$jobId."/";
        $upURL=$upload_dir['baseurl']."/tmp/".$jobId."/";
        $newArr = array();
        $newImgStr='';
        $arrImg=explode(',',$imgString);
        if(!file_exists($upDir)){
            //Directory does not exist, so lets create it.
            mkdir($upDir, 0755, true);
        }
        
        foreach ($arrImg as $k => $v) {
            $imgExtArr = explode('.', $v);
            $extImg = end($imgExtArr);
            $imgcontant = file_get_contents($v);
            $file = $type."_".time()."_".$k . "_". $jobId .".". $extImg;
            file_put_contents($upDir . $file, $imgcontant);
            chmod($file, 0777);
            $newArr[$k] = $upURL.$file;
        }
        $newImgStr=implode(',',$newArr);
        return $newImgStr;
    }
    // 31/08/2018 for CORS ERROR to load images at local
    
  public function setup_db() {
    global $wpdb;
    global $sag_db_version;
    $installed_ver = get_option('sag_db_version', 0);
    if ($installed_ver != $sag_db_version) {
      $table_sag_jobs     = $wpdb->prefix . 'sag_jobs';
      $table_gpd_items    = $wpdb->prefix . 'gpd_items';
      $table_gdr_items    = $wpdb->prefix . 'gdr_items';
      $table_gdn_items    = $wpdb->prefix . 'gdn_items';
      $table_sfd_items    = $wpdb->prefix . 'sfd_items';
      $table_wmd_items    = $wpdb->prefix . 'wmd_items';

      $charset_collate = $wpdb->get_charset_collate();

      $jobs_sql = "CREATE TABLE $table_sag_jobs ( /*------- UNIVERSAL JOB DETAILS -------*/
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        salesman_id	varchar(10) NULL,
        salesman_name varchar(70) NULL,
        quote_number varchar(10) NULL,
        job_name varchar(30) NULL,
        onsite_contact varchar(70) NULL,
        contact_number varchar(15) NULL,
        contact_email varchar(255) NULL,
        person_seen varchar(70) NULL,
        csr varchar(30) NULL,
        project_address_line1 varchar(255) NULL,
        project_address_line2 varchar(255) NULL,
        project_email varchar(255) NULL,
        project_city varchar(30) NULL,
        project_state varchar(15) NULL,
        project_postal_code varchar(10) NULL,
        billing_address_line1 varchar(255) NULL,
        billing_address_line2 varchar(255) NULL,
        billing_email varchar(255) NULL,
        billing_city varchar(30) NULL,
        billing_state varchar(15) NULL,
        billing_postal_code varchar(10) NULL,
        project_description text NULL,
        project_status varchar(20) NULL,
        pdf_version	varchar(10) NULL,
        labor_hours_type text NULL,
        labor_number_of_hours text NULL,
        labor_number_of_men text NULL,
        labor_notes text NULL,
        created_date datetime NULL
      ) $charset_collate;";

      /* FOR NOW THERE ARE 3 TYPES OF ITEMS ASSOCIATED WITH A JOB - MORE WILL BE ADDED */
      /*1. GLASS PIECE DETAILS    */
      /*2. GLASS DOOR REPAIR      */
      /*3. GLASS DOOR REPLACEMENT */
      $gpd_items_sql = "CREATE TABLE $table_gpd_items ( /* ------- GLASS PIECE DETAILS ------- */
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        job_id bigint(20) unsigned NOT NULL,
        glass_location varchar(30) NULL,
        glass_type varchar(30) NULL,
        lite_thickness varchar(20) NULL,
        spacer_color varchar(20) NULL,
        glass_treatment varchar(20) NULL,
        glass_color varchar(30) NULL,
        glass_color_note varchar(150) NULL,
        glass_lift decimal(6,2) NULL,
        glass_set varchar(80) NULL,
        daylight_width varchar(20) NULL,
        daylight_height varchar(20) NULL,
        go_width varchar(20) NULL,
        go_height varchar(20) NULL,
        size_verification_status varchar(20) NULL,
        overall_thickness varchar(20) NULL,
        sag_or_quote tinyint(1) NULL,
        manufacturer varchar(50) NULL,
        solar_film tinyint(1) NULL,
        solar_film_responsibility varchar(20) NULL,
        solar_film_type varchar(20) NULL,
        solar_film_source varchar(20) NULL,
        wet_seal tinyint(1) NULL,
        wet_seal_responsibility varchar(20) NULL,
        furniture_to_move tinyint(1) NULL,
        furniture_to_move_comment varchar(255) NULL,

        walls_or_ceilings_to_cut tinyint(1) NULL,
        walls_or_ceilings_to_cut_responsibility varchar(20) NULL,
        walls_or_ceilings_to_cut_comment varchar(255) NULL,
        blind_needs_removing tinyint(1) NULL,
        glass_fits_elevator tinyint(1) NULL,
        pictures_download_url text NULL,
        sketches_download_url text NULL,
        instructions text NULL,
        caulk_amount decimal(6,2) NULL,
        caulk_type varchar(30) NULL,
        scaffolding_type varchar(30) NULL,
        tape_amount decimal(6,2) NULL,
        tape_type varchar(30) NULL,
        channel decimal(6,2) NULL,
        miscellaneous varchar(255) NULL,
        glass_grids tinyint(1) NULL,
        pattern_horizontal_grids decimal(6,2) NULL,
        pattern_vertical_grids decimal(6,2) NULL,
        grids_thickness varchar(20) NULL,
        grids_color varchar(50) NULL,
        /*sag-158*/
        grids_color_custom varchar(50) NULL,
        /*sag-158*/
        glass_fabrication tinyint(1) NULL,
        fabrication_polished_edges varchar(20) NULL,
        fabrication_holes varchar(20) NULL,
        fabrication_pattern varchar(20) NULL
      ) $charset_collate;";

      $gdr_items_sql = "CREATE TABLE $table_gdr_items ( /* ------- Glass Door Repair Items ------- */
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        job_id bigint(20) unsigned NOT NULL,
        type varchar(30) NULL,
        subtype varchar(30) NULL,
        notes varchar(255) NULL,
        finish varchar(255) NULL,
        door_closer_status varchar(30) NULL,
        door_closer varchar(255) NULL,
        top_pivot_status varchar(30) NULL,
        top_pivot varchar(255) NULL,
        patch_fittings_status varchar(30) NULL,
        patch_fittings varchar(255) NULL,
        top_inserts_status varchar(30) NULL,
        top_inserts varchar(255) NULL,
        header_door_stop_status varchar(30) NULL,
        header_door_stop varchar(255) NULL,
        type_of_glass_status varchar(30) NULL,
        type_of_glass varchar(255) NULL,
        handles_status varchar(30) NULL,
        handles varchar(255) NULL,
        panic_device_status varchar(30) NULL,
        panic_device varchar(255) NULL,
        edge_seal_weatherstrip_status varchar(30) NULL,
        edge_seal_weatherstrip varchar(255) NULL,
        bottom_insert_status varchar(30) NULL,
        bottom_insert varchar(255) NULL,
        bottom_pivot_arm_status varchar(30) NULL,
        bottom_pivot_arm varchar(255) NULL,
        bottom_pivot_status varchar(30) NULL,
        bottom_pivot varchar(255) NULL,
        floor_door_closer_status varchar(30) NULL,
        floor_door_closer varchar(255) NULL,
        threshold_status varchar(30) NULL,
        threshold varchar(255) NULL,
        top_rail_status varchar(30) NULL,
        top_rail varchar(255) NULL,
        patch_lock_status varchar(30) NULL,
        patch_lock varchar(255) NULL,
        bottom_arm_status varchar(30) NULL,
        bottom_arm varchar(255) NULL,
        bottom_rail_status varchar(30) NULL,
        bottom_rail varchar(255) NULL,
        side_rails_status varchar(30) NULL,
        side_rails varchar(255) NULL,
        pictures_download_url text NULL,
        sketches_download_url text NULL
      ) $charset_collate;";

      $gdn_items_sql = "CREATE TABLE $table_gdn_items (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        job_id bigint(20) unsigned NOT NULL,
        type varchar(30) NULL,
        subtype varchar(30) NULL,
        notes varchar(255) NULL,
        glass_width decimal(6,2) NULL,
        glass_height decimal(6,2) NULL,
        glass_thickness varchar(50) NULL,
        glass_color varchar(15) NULL,
        overall_door_height decimal(6,2) NULL,
        daylite varchar(10) NULL,
        top_shoe_size decimal(6,2) NULL,
        top_shoe_pocket decimal(6,2) NULL,
        bottom_shoe_size decimal(6,2) NULL,
        bottom_shoe_pocket decimal(6,2) NULL,
        door_opening decimal(6,2) NULL,
        type_of_set varchar(255) NULL,
        type_of_handle varchar(255) NULL,
        hole_location varchar(255) NULL,
        hole_size varchar(50) NULL,
        patch_fitting_manufacturer varchar(255) NULL,
        bottom_patch_lock_manufacturer varchar(255) NULL,
        lock_notch_height decimal(6,2) NULL,
        lock_notch_width decimal(6,2) NULL,
        side_rail_size decimal(6,2) NULL,
        side_rail_pocket decimal(6,2) NULL,
        pictures_download_url text NULL,
        sketches_download_url text NULL
      ) $charset_collate;";

      $sfd_items_sql = "CREATE TABLE $table_sfd_items (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        job_id bigint(20) unsigned NOT NULL,
        type varchar(30) NULL,
        subtype varchar(30) NULL,
        notes varchar(255) NULL,
        brand varchar(255) NULL,
        coc_closer_status varchar(30) NULL,
        coc_closer varchar(255) NULL,
        top_pivot_frame_status varchar(30) NULL,
        top_pivot_frame varchar(255) NULL,
        top_pivot_door_status varchar(30) NULL,
        top_pivot_door varchar(255) NULL,
        surface_mounted_closer_status varchar(30) NULL,
        surface_mounted_closer varchar(255) NULL,
        offset_arm_status varchar(30) NULL,
        offset_arm varchar(255) NULL,
        drop_plate_status varchar(30) NULL,
        drop_plate varchar(255) NULL,
        type_of_glass_status varchar(30) NULL,
        type_of_glass varchar(255) NULL,
        handles_status varchar(30) NULL,
        handles varchar(255) NULL,
        panic_device_status varchar(30) NULL,
        panic_device varchar(255) NULL,
        weather_stripping_status varchar(30) NULL,
        weather_stripping varchar(255) NULL,
        bottom_pivot_status varchar(30) NULL,
        bottom_pivot varchar(255) NULL,
        spindle_status varchar(30) NULL,
        spindle varchar(255) NULL,
        floor_closer_status varchar(30) NULL,
        floor_closer varchar(255) NULL,
        threshold_status varchar(30) NULL,
        threshold varchar(255) NULL,
        type_of_beads_status varchar(30) NULL,
        type_of_beads varchar(255) NULL,
        thumbturn_status varchar(30) NULL,
        thumbturn varchar(255) NULL,
        key_cylinder_status varchar(30) NULL,
        key_cylinder varchar(255) NULL,
        lock_type_status varchar(30) NULL,
        lock_type varchar(255) NULL,
        path_paddle_status varchar(30) NULL,
        path_paddle varchar(255) NULL,
        top_flush_bolt_status varchar(30) NULL,
        top_flush_bolt varchar(255) NULL,
        bottom_flush_bolt_status varchar(30) NULL,
        bottom_flush_bolt varchar(255) NULL,
        top_pivot_offset_status varchar(30) NULL,
        top_pivot_offset varchar(255) NULL,
        intermediate_pivot_status varchar(30) NULL,
        intermediate_pivot varchar(255) NULL,
        bottom_pivot_offset_status varchar(30) NULL,
        bottom_pivot_offset varchar(255) NULL,
        continuous_hinge_status varchar(30) NULL,
        continuous_hinge varchar(255) NULL,
        pictures_download_url text NULL,
        sketches_download_url text NULL
      ) $charset_collate;";

      $wmd_items_sql = "CREATE TABLE $table_wmd_items (
        id bigint(20) unsigned NOT NULL AUTO_INCREMENT PRIMARY KEY,
        job_id bigint(20) unsigned NOT NULL,
        type varchar(30) NULL,
        subtype varchar(30) NULL,
        notes varchar(255) NULL,
        brand varchar(255) NULL,
        coc_closer_status varchar(30) NULL,
        coc_closer varchar(255) NULL,
        surface_mounted_closer_status varchar(30) NULL,
        surface_mounted_closer varchar(255) NULL,
        offset_arm_status varchar(30) NULL,
        offset_arm varchar(255) NULL,
        drop_plate_status varchar(30) NULL,
        drop_plate varchar(255) NULL,
        type_of_glass_status varchar(30) NULL,
        type_of_glass varchar(255) NULL,
        handles_status varchar(30) NULL,
        handles varchar(255) NULL,
        panic_device_rim_status varchar(30) NULL,
        panic_device_rim varchar(255) NULL,
        panic_device_vertical_rods_status varchar(30) NULL,
        panic_device_vertical_rods varchar(255) NULL,
        weather_stripping_status varchar(30) NULL,
        weather_stripping varchar(255) NULL,
        bottom_arm_status varchar(30) NULL,
        bottom_arm varchar(255) NULL,
        floor_closer_status varchar(30) NULL,
        floor_closer varchar(255) NULL,
        threshold_status varchar(30) NULL,
        threshold varchar(255) NULL,
        type_of_beads_status varchar(30) NULL,
        type_of_beads varchar(255) NULL,
        thumbturn_status varchar(30) NULL,
        thumbturn varchar(255) NULL,
        key_cylinder_status varchar(30) NULL,
        key_cylinder varchar(255) NULL,
        lock_type_status varchar(30) NULL,
        lock_type varchar(255) NULL,
        knob_lock_status varchar(30) NULL,
        knob_lock varchar(255) NULL,
        lever_lock_status varchar(30) NULL,
        lever_lock varchar(255) NULL,
        top_flush_bolt_status varchar(30) NULL,
        top_flush_bolt varchar(255) NULL,
        bottom_flush_bolt_status varchar(30) NULL,
        bottom_flush_bolt varchar(255) NULL,
        continuous_hinge_status varchar(30) NULL,
        continuous_hinge varchar(255) NULL,
        top_hinge_status varchar(30) NULL,
        top_hinge varchar(255) NULL,
        intermediate_hinge_status varchar(30) NULL,
        intermediate_hinge varchar(255) NULL,
        bottom_hinge_status varchar(30) NULL,
        bottom_hinge varchar(255) NULL,
        pictures_download_url text NULL,
        sketches_download_url text NULL
      ) $charset_collate;";

      require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

      dbDelta($jobs_sql);
      dbDelta($gpd_items_sql);
      dbDelta($gdr_items_sql);
      dbDelta($gdn_items_sql);
      dbDelta($sfd_items_sql);
      dbDelta($wmd_items_sql);

      update_option('sag_db_version', $sag_db_version);
    }
  }

  public function update_db() {
    global $sag_db_version;
    if (get_site_option('sag_db_version', 0) != $sag_db_version) {
      $this->setup_db();
    }
  }

  public function query_vars($qvars) {
    $qvars[] = 'job_id';
    return $qvars;
  }

  public function sag_events(){	
    global $wpdb;
	$current_user = wp_get_current_user();
	$table= $wpdb->prefix."sag_events";
	$table_jobs= $wpdb->prefix."sag_jobs";
    $user_id = get_current_user_id();
	if($_POST['event_delete']){
		$event_id_for_delete = esc_sql($_POST['event_id_for_delete']);
		$wpdb->delete($table, array('id' => $event_id_for_delete));
	}
	if($_POST['event_save']){
		$event_type = esc_sql($_POST['event_type']);
		$arr = array( 
					'type' => $event_type,
					'notes' => esc_sql($_POST['event_notes']),
					'user_id' => $current_user->ID,
					'job_id' => esc_sql($_POST['event_job_id'])
				);
		if($event_type=='0'){
			$arr['name'] = esc_sql($_POST['event_name']);
			$arr['participants'] = esc_sql($_POST['event_participants']); 
			//$arr['start_time'] = $this->getValidDateTime(esc_sql($_POST['event_start_time']));
			$arr['start_time'] = $this->getValidDateTime(date('Y-m-d H:i:s', strtotime(esc_sql($_POST['event_start_time']))));
			//$strtotime = strtotime($arr['start_time'])+900;
    		//$arr['end_time'] = $this->getValidDateTime(date('Y-m-d H:i:s', $strtotime));
			//$arr['end_time'] = $this->getValidDateTime(esc_sql($_POST['event_end_time'])); 
		}
		else{
			$arr['start_time'] = $this->getValidDateTime('');
			//$arr['end_time'] = $this->getValidDateTime(''); 
		}
		
		$strtotime = strtotime($arr['start_time'])+900;
		$arr['end_time'] = $this->getValidDateTime(date('Y-m-d H:i:s', $strtotime));
		
		$refreshRow = $wpdb->get_row( "SELECT * FROM ".$wpdb->prefix."sag_refresh_tokens WHERE user_id ='$user_id'" );
		$refresh_token = $refreshRow->refresh_token;
		if(!empty($refreshRow) && $refreshRow!=null){
			$URL = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
			$CURLOPT_POSTFIELDS = 'client_id='.APP_ID.'
									&client_secret='.APP_PASSWORD.'
									&grant_type=refresh_token						
									&refresh_token='.$refresh_token.'
									&scope=https%3A%2F%2Foutlook.office.com%2Fcalendars.readwrite+openid+offline_access';
			$server_output = getCurlResponse($URL, $CURLOPT_POSTFIELDS);
			$result = json_decode($server_output);
			$token = $result->access_token;
			if(!empty($result)){
    			$wpdb->update( 
    				$wpdb->prefix."sag_refresh_tokens", 
    				array(
    					'refresh_token' => $result->refresh_token,
    					'token_expires' => $result->expires_in,
    					'modified' => date('Y-m-d H:i:s')
    				), 
    				array( 'user_id' => $user_id )
    			);      
    		}
		}
		// else{
			// echo 'Please sync outlook on settings page for auto sync!<br>';
		// }
		//	var_dump($result,$user_id,$token,$event_type);
		//	die;
		
		if(isset($_POST['event_edit_id']) && $_POST['event_edit_id']!=''){
		    $arr['modified'] = $this->getValidDateTime('');
		    $event_edit_id = esc_sql($_POST['event_edit_id']);
			$wpdb->update($table, $arr, array('id' => $event_edit_id ));
			if(isset($token) && $token!='' && $event_type=='0'){$this->syncWithOutlook($token,$event_edit_id);}
		}
		else{
		    $arr['created'] = $this->getValidDateTime(''); 
			$wpdb->insert($table, $arr);
			if(isset($token) && $token!='' && $event_type=='0'){$this->syncWithOutlook($token,$wpdb->insert_id);}
			if($event_type!='0'){$this->sendEventMail($wpdb->insert_id);}
		}
	}
	if(isset($_GET['job_id'])){
		$jobid = $_GET['job_id'];
		$job_id_qry = "job_id='".$jobid."' AND ";
	}else{
		$jobid='';
		$job_id_qry = "";
		//$past_qry = "SELECT * FROM $table WHERE created < CURDATE() ORDER BY created DESC";
		//$current_qry = "SELECT * FROM $table WHERE start_time >= CURDATE() ORDER BY id DESC";
		//$_html = '';
	}


//var_dump(date('Y-m-d H:i:s'));
$ctime = trim($_COOKIE['cdate']);
$ctime = (isset($ctime)) ? "'".$ctime."'" : 'NOW()';

	$past_qry = "SELECT * FROM $table WHERE ".$job_id_qry." start_time < $ctime ORDER BY created DESC";
	$current_qry = "SELECT * FROM $table WHERE ".$job_id_qry." start_time >= $ctime ORDER BY id DESC";

   // $serverEpoch = time();
   // $format1 = 'H:i:s';
	//$clientEpoch = '<script type="text/javascript">getClientEpoch();</script>';
	//$serverTime = date($format1, $serverEpoch);


	//echo $clientEpoch = '<script>var now = new Date();document.write(getClientMillis());</script>';
	//echo $clientEpoch = '<script>var now = new Date();</script>';
//	echo $clientTime = date('d-m-Y H:i:s', $clientEpoch);	
	//echo "Server time: $serverTime<br />";
	//echo "PHP Client time: $clientTime<br />";


	//Past Events Show
	/***************************************************************************************************************/
		$resultArr = $wpdb->get_results($past_qry);
		if(!empty($resultArr)){
			$html2 = '<div class="container"><table class="events_table " border="1" cellpadding="4" cellspacing="4">';
			foreach($resultArr as $result){ 
				$user = get_user_by( 'ID', $result->user_id );
				$col_text = '<strong>'.$user->user_login.'</strong>';
				if($result->type=='0'){
					$col_text.= ' setup an appointment';
					$icon = '<i class="fa fa-calendar" aria-hidden="true"></i>';
				}
				else if($result->type=='1'){
					$col_text.= ' made a call';
					$icon = '<i class="fa fa-phone" aria-hidden="true"></i>';
				}
				else if($result->type=='2'){
					$col_text.= ' added a note';
					$icon = '<i class="fa fa-file-o" aria-hidden="true"></i>';
				}
				else if($result->type=='3'){
					$col_text.= ' added a text message';
					$icon = '<i class="fa fa-comment-o" aria-hidden="true"></i>';
				}
				else if($result->type=='4'){
					$col_text.= ' added an email';
					$icon = '<i class="fa fa-envelope-o" aria-hidden="true"></i>';
				}
				if(!isset($_GET['job_id'])){
				    $job_rst = $wpdb->get_row("SELECT * FROM $table_jobs where id ='".$result->job_id."'");
    		        $_html = '<a target="_blank" class="btn btn-danger btn-sm" href="add-job/?job_id='.$job_rst->id.'" role="button">'.$job_rst->quote_number.'</a> &nbsp;&nbsp;';
				}
				else{$_html = '';}
				$col_text.= '<br><strong>Notes</strong><br>'.$result->notes;
				$html2 .= '	<tr> 
								<td>'.$_html.$icon.'</td>
								<td>'.$col_text.'</td>';
				//if($result->type=='0'){				
						$html2 .= '<td><strong>'.date('g:i a m/d/y',strtotime($result->start_time)).'</strong></td>';
				/*}
				else{
						$html2 .= '<td class="visibility"><strong>&nbsp;&nbsp;SagSagSagSag</strong></td>';
				}*/
						$html2 .= '<td>'.date('m/d/y',strtotime($result->created)).'</td>';
				
				if($result->user_id==$user_id){
					$html2 .= '<td><i class="fa fa-pencil edit-event" aria-hidden="true" eventid="'.$result->id.'"></i>&nbsp;&nbsp;<i class="fa fa-trash" aria-hidden="true" rel="delete" eventid="'.$result->id.'" data-toggle="modal" data-target="#event_popup"></i></td>';
				}
				else{
					$html2 .= '<td class="visibility"><strong>&nbsp;&nbsp;SagSagSagSag</strong></td>';
				}
				$html2 .= '</tr>';
			}
				
			$html2 .= '</table></div>';
			 $html2;		
			#die;
		}
		else{
			$html2 = '<div align="center" class="no-event">There are currently no events.<br>Click the add event button above to get started!</div>';
		}
	
		//}
	
	
        //Current Events Show	
	/***************************************************************************************************************/
	
	
	$resultArr = $wpdb->get_results($current_qry);
	if(!empty($resultArr)){
		$html = '<div class="container"><table class="events_table " border="1" cellpadding="4" cellspacing="4">';
		foreach($resultArr as $result){ 
			$user = get_user_by( 'ID', $result->user_id );
			$col_text = '<strong>'.$user->user_login.'</strong>';
			if($result->type=='0'){
				$col_text.= ' setup an appointment';
				$icon = '<i class="fa fa-calendar" aria-hidden="true"></i>';
			}
			else if($result->type=='1'){
				$col_text.= ' made a call';
				$icon = '<i class="fa fa-phone" aria-hidden="true"></i>';
			}
			else if($result->type=='2'){
				$col_text.= ' added a note';
				$icon = '<i class="fa fa-file-o" aria-hidden="true"></i>';
			}
			else if($result->type=='3'){
				$col_text.= ' added a text message';
				$icon = '<i class="fa fa-comment-o" aria-hidden="true"></i>';
			}
			else if($result->type=='4'){
				$col_text.= ' added an email';
				$icon = '<i class="fa fa-envelope-o" aria-hidden="true"></i>';
			}
			if(!isset($_GET['job_id'])){
    				$job_rst = $wpdb->get_row("SELECT * FROM $table_jobs where id ='".$result->job_id."'");
    		        $_html = '<a target="_blank" class="btn btn-danger btn-sm" href="add-job/?job_id='.$job_rst->id.'" role="button">'.$job_rst->quote_number.'</a>&nbsp;&nbsp;';
				}
				else{$_html = '';}
			$col_text.= '<br><strong>Notes</strong><br>'.$result->notes;
			$html .= '	<tr> 
							<td>'.$_html.$icon.'</td>
							<td>'.$col_text.'</td>';
			//if($result->type=='0'){				
					$html .= '<td><strong>'.date('g:i a m/d/y',strtotime($result->start_time)).'</strong></td>';
			/*}
			else{
					$html .= '<td class="visibility"><strong>&nbsp;&nbsp;SagSagSagSag</strong></td>';
			}*/
					$html .= '<td>'.date('m/d/y',strtotime($result->created)).'</td>';
			
			if($result->user_id==$user_id){
				$html .= '<td><i class="fa fa-pencil edit-event" aria-hidden="true" eventid="'.$result->id.'"></i>&nbsp;&nbsp;<i class="fa fa-trash" aria-hidden="true" rel="delete" eventid="'.$result->id.'" data-toggle="modal" data-target="#event_popup"></i></td>';
			}
			else{
				$html .= '<td class="visibility"><strong>&nbsp;&nbsp;SagSagSagSag</strong></td>';
			}
			$html .= '</tr>';
		}
			
		$html .= '</table></div>';
		
		#die;
	}
	else{
		$html = '<div align="center" class="no-event">There are currently no events.<br>Click the add event button above to get started!</div>';
	}
	
	$ev1 = get_permalink(get_page_by_title('Events')).'?job_id='.$_GET['job_id'];
	$ev = site_url().'/add-job/?job_id='.$_GET['job_id'];
	$events = '<a class="btn" href='.$ev.'>Details</a>';
	$event1 = '<a class="btn active" href='.$ev1.'>Events</a>';

	
	
	/****************************************************************************************************************/
	//$resultArr = $wpdb->get_results( "SELECT * FROM $table WHERE user_id='$current_user->ID' ORDER BY id DESC" );
	//$resultArr = $wpdb->get_results( "SELECT * FROM $table WHERE job_id='$jobid' ORDER BY id DESC" );

    
	if(isset($_GET['job_id'])){
  
		echo '<div class="clearfix"><div class="tab-jobs my_cus_btn">'.$events .''.$event1.'</div><a class="btn btn-danger right" id="add_event" jobid="'.$_GET['job_id'].'" data-toggle="modal" data-target="#event_popup" href="javascript:void(0);">+ Add Event</a></div>';
    }
	else{
		echo '<div class="clearfix"><h3>Events</h3></div>';
    }
	echo '</br>'; 
	echo '<div class="container-custom custom_tab_pin">
        	<ul class="nav nav-tabs">
            	<li class="active"><a data-toggle="tab" href="#current_1">Current</a></li>
            	<li><a data-toggle="tab" href="#past_1">Past</a></li>
        	</ul> 
        	<div class="tab-content">
            	<div id="current_1" class="tab-pane fade in active">'.$html.'</div>
            	<div id="past_1" class="tab-pane fade">'.$html2.'</div>
        	</div>
	    </div>';
	/*if(!empty($resultArr)){
	$html = '<div class="container"><table class="events_table" border="1" cellpadding="4" cellspacing="4">';
	foreach($resultArr as $result){
		$user = get_user_by( 'ID', $result->user_id );
		$col_text = '<strong>'.$user->user_login.'</strong>';
		if($result->type=='0'){
			$col_text.= ' setup an appointment';
			$icon = '<i class="fa fa-calendar" aria-hidden="true"></i>';
		}
		else if($result->type=='1'){
			$col_text.= ' made a call';
			$icon = '<i class="fa fa-phone" aria-hidden="true"></i>';
		}
		else if($result->type=='2'){
			$col_text.= ' added a note';
			$icon = '<i class="fa fa-file-o" aria-hidden="true"></i>';
		}
		else if($result->type=='3'){
			$col_text.= ' added a text message';
			$icon = '<i class="fa fa-comment-o" aria-hidden="true"></i>';
		}
		else if($result->type=='4'){
			$col_text.= ' added an email';
			$icon = '<i class="fa fa-envelope-o" aria-hidden="true"></i>';
		}
		$col_text.= '<br><strong>Notes</strong><br>'.$result->notes;
		$html .= '	<tr>
						<td>'.$icon.'</td>
						<td>'.$col_text.'</td>';
		//if($result->type=='0'){				
				$html .= '<td><strong>'.date('g:i a m/d/y',strtotime($result->start_time)).'</strong></td>';
		/*}
		else{
				$html .= '<td class="visibility"><strong>&nbsp;&nbsp;SagSagSagSag</strong></td>';
		}*/
			/*	$html .= '<td>'.date('m/d/y',strtotime($result->created)).'</td>';
		
		if($result->user_id==$user_id){
			$html .= '<td><i class="fa fa-pencil edit-event" aria-hidden="true" eventid="'.$result->id.'"></i>&nbsp;&nbsp;<i class="fa fa-trash" aria-hidden="true" rel="delete" eventid="'.$result->id.'" data-toggle="modal" data-target="#event_popup"></i></td>';
		}
		else{
			$html .= '<td class="visibility"><strong>&nbsp;&nbsp;SagSagSagSag</strong></td>';
		}
		$html .= '</tr>';
	}
		
	$html .= '</table></div>';
	echo $html;		
	#die;
	}
	else{
		echo '<div align="center" class="no-event">There are currently no events.<br>Click the add event button above to get started!</div>';
	}
	*/
	if(isset($_GET['job_id']) && ($_GET['job_id']!='')){
		$partArr = $wpdb->get_row( "SELECT salesman_name,owner_name,csr FROM ".$wpdb->prefix."sag_jobs WHERE id = '".$_GET['job_id']."' " ,ARRAY_N);
		
		$partArr = array_diff($partArr, array("na"));
		$rsArr = array();
		foreach($partArr as $part){
			$subPart = $wpdb->get_row( "SELECT user_email,user_nicename FROM ".$wpdb->prefix."users WHERE user_login LIKE '%$part%' OR user_nicename LIKE '%$part%'" ,ARRAY_A);
			if($subPart['user_email']!= NULL){
				array_push($rsArr,$subPart['user_email']);
				//$rsArr['user_nicename'] = $subPart['user_email'];
			}
		}
		$event_participants = implode(", ",$rsArr);
	}
	
  ?>
	<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.css">
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.js"></script>-->
 <!-- <div id="replace_content_add_event" style="display:none;">
    <div class="form-group">
        <div class="col-md-6 col-sm-6">
          <label for="event_type">Event Type</label>
          <select id="event_type" class="form-control" name="event_type">
            <option value="0">Appointment</option>
            <option value="1">Call</option>
            <option value="2">Notes</option>
			<option value="3">Text Message</option>
			<option value="4">Email</option>
          </select>
        </div>
        <div class="col-md-6 col-sm-6 event_name">
          <label for="event_name">Event Name</label>
          <input type="text" class="form-control" id="event_name" name="event_name"  />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-6 col-sm-6 event_start_time">
          <label for="event_start_time">Start Time</label>
          <input type="text" class="form-control" id="event_start_time_add" placeholder="yyyy-mm-dd hh:mm:ss" name="event_start_time"  />
        </div>
        <div class="col-md-6 col-sm-6 event_end_time">
          <label for="event_end_time">End Time</label>
          <input type="text" class="form-control" id="event_end_time_add" placeholder="yyyy-mm-dd hh:mm:ss" name="event_end_time"  />
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 col-sm-12 event_participants">
          <label for="event_participants">Participants</label>
          <textarea id="event_participants" name="event_participants" class="form-control"><?php echo $event_participants;?></textarea>
        </div>
    </div>
    <div class="form-group">
        <div class="col-md-12 col-sm-12">
          <label for="event_notes">Notes</label>
          <textarea id="event_notes" name="event_notes" class="form-control"></textarea>
        </div>
    </div>

    <script>
		jQuery(document).ready(function(){
		   	jQuery('#event_start_time_add').datetimepicker({
					dateFormat: "yy-mm-dd",
					timeFormat: "HH:mm:ss",
					controlType: 'select',
					oneLine: true
				});
			jQuery('#event_end_time_add').datetimepicker({
					dateFormat: "yy-mm-dd",
					timeFormat: "HH:mm:ss",
					controlType: 'select',
					oneLine: true
				});
		});
		jQuery(window).load(function(){
		    jQuery("#event_type").select2('destroy'); 
		});
	</script>
  </div>-->
  <div class="modal fade event" id="event_popup" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
        <form method="post" id="events_form" name="events_form">	
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" id="closesset">&times;</button>
            <h4 class="modal-title center">Add New Event</h4>
          </div>
          <div class="modal-body">
              <div id="replace_content">
                
              </div>
              <?php if(isset($_GET['job_id'])){?>
	          <input type="hidden" name="event_job_id" value="<?php echo trim($_GET['job_id']);?>" />
              <?php }?>
          </div>
          <div class="clearfix"></div>
          <div class="modal-footer">
          	<input type="submit" name="event_save" value="Save" class="btn btn-md btn-primary pull-right" id="event_save" />
          </div>
        </form>  
        </div>
      </div>
    </div>
  <script>
  jQuery(document.body).on('change',"#event_type",function(){
	  event_type = jQuery(this).attr('value');
	  if(event_type==1 || event_type==2 || event_type==3 || event_type==4){
		  jQuery(".event_name").hide();
		  jQuery(".event_start_time").hide();
		  //jQuery(".event_end_time").hide();
		  jQuery(".event_participants").hide();	  
		  jQuery("#event_notes").val('');
	  }
	  else{
		  jQuery(".event_name").show();
		  jQuery(".event_start_time").show();
		  //jQuery(".event_end_time").show();
		  jQuery(".event_participants").show();	  
	  }
  });
  
        jQuery(document.body).on('click',"#add_event",function(e){
			jobid = jQuery(this).attr("jobid");
			//var hhtml = jQuery("#replace_content_add_event").html();
			var hhtml = '<div class="form-group"><div class="col-md-6 col-sm-6"><label for="event_type">Event Type</label><select id="event_type" class="form-control" name="event_type"><option value="0">Appointment</option><option value="1">Call</option><option value="2">Notes</option><option value="3">Text Message</option><option value="4">Email</option></select></div><div class="col-md-6 col-sm-6 event_name"><label for="event_name">Event Name</label><input type="text" class="form-control" id="event_name" name="event_name"/></div></div><div class="form-group"><div class="col-md-12 col-sm-12 event_start_time"><label for="event_start_time">Start Time</label><input type="text" class="form-control" id="event_start_time_add" placeholder="yyyy-mm-dd hh:mm:ss" name="event_start_time"/></div></div><div class="form-group"><div class="col-md-12 col-sm-12 event_participants"><label for="event_participants">Participants</label><textarea id="event_participants" name="event_participants" class="form-control"></textarea></div></div><div class="form-group"><div class="col-md-12 col-sm-12"><label for="event_notes">Notes</label><textarea id="event_notes" name="event_notes" class="form-control"></textarea></div></div>';
			var $modal = jQuery('#event_popup');
			$modal.on('show.bs.modal', function(e) {
				  jQuery(this).find('#replace_content').html('');
				  jQuery(this).find('#replace_content').html(hhtml);
				  jQuery(this).find('.modal-title').html('Add New Event');
				  jQuery(this).find('.modal-footer').html('<input type="submit" name="event_save" value="Save" class="btn btn-md btn-primary pull-right" id="event_save" />');
				  
				  
				    jQuery.post("<?php echo admin_url("admin-ajax.php");?>",
        				{
        					action:'get_participants',
        					job_id:jobid
        				},
        				function(resp){
        					//alert(resp);
        					jQuery('#replace_content').find('#event_participants').val(resp);
        				}
        			);
        			
				    jQuery(this).find('#replace_content').find('#event_start_time_add').datetimepicker({
						dateFormat: "yy-mm-dd",
						timeFormat: "hh:mm tt",
						controlType: 'select',
						oneLine: true,
    					/*onSelect: function( selectedDate ) {
    						jQuery('#event_end_time_add').datetimepicker("option","minDate",selectedDate);
    					}*/
					});
					/*jQuery(this).find('#replace_content').find('#event_end_time_add').datetimepicker({
						dateFormat: "yy-mm-dd",
						timeFormat: "HH:mm:ss",
						controlType: 'select',
						oneLine: true
					});*/
			});
			$modal.modal('show');			
			e.preventDefault();
		});
  
  
	jQuery(document).ready(function(){
		jQuery('#event_popup').on("hidden.bs.modal", function(){
			jQuery("#replace_content").html("");
		});
		
		jQuery(".edit-event").click(function(){
			eventid = jQuery(this).attr('eventid');
			
			var data = {
				'action': 'edit_event_item',
				'event_id': eventid
			};
	
			// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
			jQuery.post("<?php echo admin_url("admin-ajax.php")?>", data, function(response) {
			
				var $modal = jQuery('#event_popup');
				$modal.on('show.bs.modal', function(e) {
					  jQuery(this).find('#replace_content').html('');
					  jQuery(this).find('#replace_content').html(response);
					  jQuery(this).find('.modal-title').html('Edit Event');
					  jQuery(this).find('.modal-footer').html('<input type="submit" name="event_save" value="Save" class="btn btn-md btn-primary pull-right" id="event_save" />');
				});
				$modal.modal('show');

			});
			
		
			
		});
		
	
		jQuery(".fa-trash").click(function(){
			eventid = jQuery(this).attr('eventid');
			rel = jQuery(this).attr('rel');
			var $modal = jQuery('#event_popup');
			
			// Show loader & then get content when modal is shown
			$modal.on('show.bs.modal', function(e) {
			  	if(rel=='delete'){
				  jQuery(this).find('#replace_content').html('');
				  jQuery(this)
					.addClass('modal-scrollfix')
					.find('#replace_content')
					.html('Are you sure you want to delete? <input type="hidden" name="event_id_for_delete" value="'+eventid+'" />');
					
				  jQuery(this)
					.find('.modal-title')
					.html('Delete Event');

				  jQuery(this)
					.find('.modal-footer')
					.html('<input type="submit" name="event_delete" value="Confirm" class="btn btn-md btn-primary pull-right" id="event_delete" />');
					
				}
			});
		});
	});
	
	jQuery(document.body).on('submit',"#events_form",function(){
		
		/* add case */
		if(jQuery('#event_start_time_add').length){
			if (jQuery("#event_start_time_add").is(':visible')) {
				event_start_time_add = jQuery.trim(jQuery('#event_popup').find("#event_start_time_add").val());
				if(event_start_time_add==''){
					jQuery('#event_popup').find("#event_start_time_add").focus();
					jQuery('#event_popup').find("#event_start_time_add").css('border','1px solid #F00');
					return false;
				}
			}
		}
		/*if(jQuery('#event_end_time_add').length){
			if (jQuery("#event_end_time_add").is(':visible')) {	
				event_end_time_add = jQuery.trim(jQuery('#event_popup').find("#event_end_time_add").val());
				if(event_end_time_add==''){
					jQuery('#event_popup').find("#event_end_time_add").focus();
					jQuery('#event_popup').find("#event_end_time_add").css('border','1px solid #F00');
					return false;
				}
			}
		}*/
		/* edit case */
		if(jQuery('#event_start_time_edit').length){
			if (jQuery("#event_start_time_edit").is(':visible')) {
				event_start_time_edit = jQuery.trim(jQuery('#event_popup').find("#event_start_time_edit").val());
				if(event_start_time_edit=='' || event_start_time_edit=='0000-00-00 00:00:00'){
					jQuery('#event_popup').find("#event_start_time_edit").focus();
					jQuery('#event_popup').find("#event_start_time_edit").css('border','1px solid #F00');
					return false;
				}
			}
		}
		/*if(jQuery('#event_end_time_edit').length){
			if (jQuery("#event_end_time_edit").is(':visible')) {
				event_end_time_edit = jQuery.trim(jQuery('#event_popup').find("#event_end_time_edit").val());
				if(event_end_time_edit=='' || event_end_time_edit=='0000-00-00 00:00:00'){
					jQuery('#event_popup').find("#event_end_time_edit").focus();
					jQuery('#event_popup').find("#event_end_time_edit").css('border','1px solid #F00');
					return false;
				}
			}
		}*/
		if(jQuery('#event_notes').length && jQuery('#event_type').val()!=0){
			event_notes_v = jQuery.trim(jQuery('#event_popup').find("#event_notes").val());
			if(event_notes_v==''){
				jQuery('#event_popup').find("#event_notes").focus();
				jQuery('#event_popup').find("#event_notes").css('border','1px solid #F00');
				return false;
			}
		}
		return true;
			  
	});
  
  </script>
  <?php
  }
  public function sag_settings_form(){
        if ( !is_user_logged_in() ) {
		    wp_redirect(wp_login_url() ); exit;
		}
	  
	  /* Get user info. */
		global $current_user, $wp_roles;
		//get_currentuserinfo(); //deprecated since 3.1
		$flag = false;
		$error = array();    
		/* If profile was saved, update profile. */
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['settings_change_pass_submit'] == 'Update' ) {
			/* Update user password. */
			if ( !empty($_POST['settings_current_pass'] ) && !empty( $_POST['settings_new_pass'] ) ) {
				if ( $_POST['settings_current_pass'] == $_POST['settings_new_pass'] ){
					#do_action( 'set_current_users_password',  $_POST['settings_current_pass'] );
					wp_update_user( array( 'ID' => $current_user->ID, 'user_pass' => esc_attr( $_POST['settings_current_pass'] ) ) );
					echo '<script>location.href="'.wp_login_url().'"</script>';
					#wp_redirect(wp_login_url());
					exit;
					$flag = true;
				}
				else
					$error[] = 'The passwords you entered do not match.  Your password was not updated.';
			}
			if($sts){
				echo '<script>location.href="'.wp_login_url().'"</script>';
				#wp_redirect(wp_login_url());
				exit;
			}
		}
		if ( 'POST' == $_SERVER['REQUEST_METHOD'] && $_POST['settings_submit'] == 'Update' ) {
		  
		
			/* Update user information. */
			if ( !empty( $_POST['settings_email'] ) ){
				if (!is_email(esc_attr( $_POST['settings_email'] )))
					$error[] = 'The Email you entered is not valid.  please try again.';
				elseif(email_exists(esc_attr( $_POST['settings_email'] )) != $current_user->id )
					$error[] = 'This email is already used by another user.  try a different one.';
				else{
					wp_update_user( array ('ID' => $current_user->ID, 'user_email' => esc_attr( $_POST['settings_email'] )));
					$flag = true;
				}
			}
		
			if ( !empty( $_POST['settings_first_name'] ) ){
				update_user_meta( $current_user->ID, 'first_name', esc_attr( $_POST['settings_first_name'] ) );
				$flag = true;
			}
			if ( !empty( $_POST['settings_last_name'] ) ){
				update_user_meta($current_user->ID, 'last_name', esc_attr( $_POST['settings_last_name'] ) );
				$flag = true;
			}
			
			/* Redirect so the page will show updated info.*/
		}
		if ( $flag && count($error) == 0 ) {
			//action hook for plugins and extra fields saving
			do_action('edit_user_profile_update', $current_user->ID);
			echo '<div align="center" class="settings_msg">Updated!</div>';
			#wp_redirect( get_permalink() );
			#exit;
		}
		else{
			echo '<div align="center" class="settings_msg_err">';
			foreach($error as $err){
				echo $err;
				echo '<br>';
			}
			echo '</div>';
		}
  ?>
  <div class="container">
        <form method="post" id="settings_form" action="" class="setting-form">
            <input type="hidden" name="currentuserid" id="currentuserid" value="<?php echo get_current_user_id();?>"/>
            <?php wp_nonce_field( 'sag_settings_form', 'br_user_form' ); ?>
            <div class="col-md-12 col-sm-12 setting-headding"><h2>Update Profile</h2></div>
            <div class="col-md-6 col-sm-6">
                <label for="settings_first_name">First Name</label>
                <input type="text" class="form-control" id="settings_first_name" name="settings_first_name" value="<?php echo $current_user->first_name;?>" />
            </div>
            <div class="col-md-6 col-sm-6">
                <label for="settings_last_name">Last Name</label>
                <input type="text" class="form-control" id="settings_last_name" name="settings_last_name" value="<?php echo $current_user->last_name;?>" />
            </div>
    
            <div class="col-md-6 col-sm-6 lock-input">
                <label for="settings_username">Username</label>
                <input type="text" class="form-control" value="<?php echo $current_user->user_login;?>" disabled="disabled"/><i class="fa fa-lock lock-btn"></i>
            </div>
            <div class="col-md-6 col-sm-6">
                <label for="settings_email">Email</label>
                <input type="text" class="form-control" id="settings_email" name="settings_email" value="<?php echo $current_user->user_email;?>" />
            </div>
    		<div class="clearfix"></div>
            <div class="col-md-6 col-sm-6">
                <input type="submit" class="btn" value="Update" name="settings_submit" />
            </div>
            <div class="clearfix"></div>
            <div class="col-md-3 col-sm-3 outlook "><img class="img-responisve" src="https://uc.uxpin.com/files/644383/638336/Microsoft_Outlook_2013_logo.svg-4c125d.png" /></div>
            <div class="col-md-12 col-sm-12">
                <a href="<?php echo site_url()."/sync-outlook/";?>"><input type="button" class="btn sync" value="Sync Outlook"/></a>
            </div>
            </form>
            
            
            <div class="clearfix"></div>
            <form method="post" id="settings_change_pass_form" action="" class="setting-form">
            <div class="col-md-12 col-sm-12 setting-headding"><h2>Change Password</h2></div>
            <div class="col-md-6 col-sm-6">
                <label for="settings_current_pass">Current Password</label>
                <input type="text" class="form-control" id="settings_current_pass" name="settings_current_pass" />
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-6">
                <label for="settings_new_pass">New Password</label>
                <input type="text" class="form-control" id="settings_new_pass" name="settings_new_pass" />
            </div>
            <div class="clearfix"></div>
            <div class="col-md-6 col-sm-6">
            	<input type="hidden" name="form-action" value="update-user" />
                <input type="submit" class="btn" value="Update" name="settings_change_pass_submit" />
            </div>
        </form>
        </div>

  <?php
  }
  public function job_form($atts = [], $content = null) {
    global $wpdb;
    wp_enqueue_script('jquery');
    wp_enqueue_script('jquery-effects-core');
    //wp_enqueue_script( 'sag-jobs-plugin-bootstrap',  plugins_url( 'js/bootstrap.min.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-plugin-frontend',  plugins_url( 'js/frontend.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-plugin-helpers',  plugins_url( 'js/helpers.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'jquery-validate',  "http://cdn.jsdelivr.net/jquery.validation/1.15.1/jquery.validate.min.js", array( 'jquery' ) );
    wp_enqueue_script( 'jquery-validate-additional-methods',  "http://cdn.jsdelivr.net/jquery.validation/1.15.1/additional-methods.min.js", array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-plugin-js-aws-sdk',  plugins_url( 'js/aws-sdk.min.js', __FILE__ ) );
    wp_enqueue_script( 'sag-jobs-plugin-google-maps-api', "https://maps.googleapis.com/maps/api/js?key=AIzaSyB93bSs0j6v6ty8IdXqJO3CfDQOvyLdwMs", array(), false, true);
    wp_enqueue_script( 'sag-jobs-plugin-sketch',  plugins_url( 'js/lib/sketch.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-plugin-js-webshim',  plugins_url( 'js/js-webshim/minified/polyfiller.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-plugin-babeljs-polyfill',  plugins_url( 'js/babeljs/polyfill.min.js', __FILE__ ), array() );
    wp_enqueue_script( 'sag-jobs-plugin-fraction',  plugins_url( 'js/fraction.min.js', __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'sag-jobs-plugin-canvastoblob',  plugins_url( 'js/canvas-to-blob.min.js', __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'sag-jobs-plugin-imageresize',  plugins_url( 'js/resize.js', __FILE__ ), array( 'jquery' ) );
	wp_enqueue_script( 'sag-jobs-plugin-form_jobs',  plugins_url( 'js/form_jobs.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-plugin-tab_glass_piece_details', plugins_url( 'js/tab_glass_piece_details.js', __FILE__ ), array( 'jquery', 'jquery-effects-core' ) );
	  wp_enqueue_script( 'sag-jobs-plugin-tab_notes_details',  plugins_url( 'js/tab_note_door.js', __FILE__ ), array( 'jquery') );
    wp_enqueue_script( 'sag-jobs-plugin-tab_glass_door_repairs',  plugins_url( 'js/tab_glass_door_repairs.js', __FILE__ ), array( 'jquery', 'jquery-effects-core' ) );
    wp_enqueue_script( 'sag-jobs-plugin-checking',  plugins_url( 'js/checkin/checkin.js', __FILE__ ), array( 'jquery' ) );
	  wp_enqueue_script( 'sag-jobs-plugin-new_doorjs',  plugins_url( 'js/tab_new_door.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script('sag-jobs-spin', plugins_url('js/lib/spin.min.js', __FILE__), array('jquery') );
    wp_localize_script( 'sag-jobs-plugin-form_jobs', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );



    $job_id = get_query_var( 'job_id', 0 );
	
	 $accepted = get_query_var( 'accepted' );
	 
	 if(isset($_REQUEST['accepted'])){
		 if(is_numeric($_REQUEST['accepted']) && $_REQUEST['accepted']>0){
				$tablest= $wpdb->prefix."sag_jobs";
				$update="update $tablest set accepted='0' where id='$job_id'";
				$wpdb->query($update);
			 }
		 }
		 
    ?>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>-->
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>-->

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
#myModalss.custom_pop.modal, #myModal.custom_pop.modal {
	z-index: 9999;
}
#myModalss.custom_pop.modal .modal-dialog, #myModal.custom_pop.modal .modal-dialog {
	background: #ffffff;
	margin-top: 70px;
}
#myModalss.custom_pop.modal .modal-header .close, #myModal.custom_pop.modal .modal-header .close {
	background: #ffffff;
}
#notapplicat.form-control, #savess {
	margin-top: 10px;
}
span.info{display:none;}
.cust-disable{
opacity:1!important;
background: #5cb85c !important;
}

/*input[type=text]{
    color: red;
}
input[type=text]:-moz-placeholder {
    color: green;
}
input[type=text]::-moz-placeholder {
    color: green;
}
input[type=text]:-ms-input-placeholder {
    color: green;
}
input[type=text]::-webkit-input-placeholder {
    color: green;
}*/
.input-group-lg > .input-group-btn > .btn.img_btn{
	padding:5px 6px;
}
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }
</style>
<script type="text/javascript">
function remove_labur(value){
    $(value).parent().parent().remove();
   
}
	jQuery(document).ready(function(){

          //add this for add new row in labor
          jQuery('#btn_new_labor_add').click(function(){
              console.log(jQuery("#btn_new_labor_add #c_remove").clone());
            var data = jQuery("#c_remove").html();
            jQuery("#labur_copy_row").append('<div class=" labur_row"><div class="col-sm-1"><button id="btn_new_labor_remove" onclick="remove_labur(this)" type="button" class="btn btn-default btn_new_labor_remove" aria-label="Left Align" title="sub"><span  class="glyphicon glyphicon-minus" aria-hidden="true"></span></button></div><div class="remove_next">'+data+'</div></div>');

        });

       


		jQuery("#select_project_status").change(function(){

		if(jQuery(this).val()=="na" || jQuery(this).val()=="void cancel" || jQuery(this).val()=="void duplicate" || jQuery(this).val()=="void lost")
		//if(jQuery(this).val()=="na")
		{
			//jQuery("#sssdfsdfsd").click();
		  jQuery("#myModalss").modal();
		    //jQuery("#myModalss").addClass('in');

			}
			//jQuery("#ResultShow").html("sfsdfsd");

		});
		jQuery(".close").click(function(){

			jQuery("#myModalss").hide();
		    jQuery("#myModalss").removeClass('in');
			//jQuery("#ResultShow").html("sfsdfsd");

		});

		jQuery("#savess").click(function(){

		var getvv=jQuery("#notapplicat").val();

			jQuery("#addwhyreason").val(getvv);
			//jQuery("#myModalss").hide();
		   // jQuery("#myModalss").removeClass('in');
			jQuery("#closesset").trigger("click");

		});


	});


	</script>

<button type="button" data-toggle="modal" data-target="#myModalss" style="display:none" id="sssdfsdfsd">Open Modal</button>
   <div class="clearfix">
        </div> <div class="tab-jobs my_cus_btn">
                <a class="btn active" href="<?php echo site_url().'/add-job/?job_id='.$_GET['job_id']?>">Details</a>
                <a class="btn" href="<?php echo get_permalink(get_page_by_title('Events')).'?job_id='.$_GET['job_id'];?>">Events</a>
        </div>
    </div>    
<div class="modal fade custom_pop" id="myModalss" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" id="closesset">&times;</button>
        <h4 class="modal-title">why it's not applicable ?</h4>
      </div>
      <div class="modal-body">
        <div id="dvMap1" style="height: 380px; width: 550px;">
          <div id="ResultShowss">
            <div class="form-group">
              <textarea id="notapplicat" class="form-control"></textarea>
              <input type="button" name="save" value="Save" class="btn btn-md btn-primary pull-right" id="savess" />
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<form method="post" id="addsagjob" action="">
  <input type="hidden" name="addwhyreason" id="addwhyreason" />
  <input type="hidden" name="currentuserid" id="currentuserid" value="<?php echo get_current_user_id();?>"/>
  <!-- Nonce fields to verify visitor provenance -->
  <?php wp_nonce_field( 'add_job_form', 'br_user_form' ); ?>
  <div id="date-wrapper"> <i class="fa fa-calendar"></i><span id="todays-date"></span> </div>

  <div class="form-group row">
    <div class="col-md-12 col-sm-12">
       <label for="text_quote_number">Quote number</label>
      <input id="text_quote_number" class="form-control" type="text" autocomplete="on" maxlength="10" required placeholder="Quote number">
  </div>

</div>


  <div class="form-group row">


    <div class="col-md-6 col-sm-6">
      <input type="hidden" id="ajax_url_image_url" value="<?php echo admin_url('admin-ajax.php'); ?>" >
      <label for="text_salesman_name">Salesman name</label>
      <?php
		  $blogusers = get_users( 'orderby=nicename&role=contributor' );
			?>
			<select id="text_owner_name" class="form-control" type="text" autocomplete="on" autofocus maxlength="70" required >
        <option value="" disabled selected>Please select Salesman name</option>
        <?php //foreach($blogusers as $cont)
		  // {
		    //$user_info = get_userdata($cont->ID);
		   ?>
        <!--<-->
        <?php

		   //}
		  global $wpdb; $flagRS=false;
		   $myrows_owner = $wpdb->get_results( "SELECT owner_name FROM wp_4twsj5a02k_sag_jobs GROUP BY owner_name" );
		   foreach($myrows_owner as $myrowss_owner){

		 if($myrowss_owner->owner_name !=''){
			$email='';
			$email	=	$this->getsalesmanmail($myrowss_owner->owner_name);
		
		 
		 if($myrowss_owner->owner_name=='RS' && $email= 'rscott@salbertglass.com' && $flagRS==false){
			
			$flagRS= true; continue;
		  }
	//	 if(!empty($email)){
		 ?>

        <option value="<?php echo $myrowss_owner->owner_name."#".$email?>"><?php echo $myrowss_owner->owner_name ?></option>
        <?php
		 //	}
		}}?>
       <!-- <option value="<?php echo "test#alex@imajine.us";?>">test salesman</option>-->
       <option value="<?php echo "RS#rscott@salbertglass.com";?>">RS</option>
		<option value="<?php echo "ALEX#alex@imajine.us";?>">ALEX</option>

      </select>

	  <label for="text_salesman_name">2nd Salesman name</label>
      <?php
		  $blogusers = get_users( 'orderby=nicename&role=contributor' );

			?>
      <select id="text_salesman_name" class="form-control" type="text" autocomplete="on" autofocus maxlength="70" required >
        <option value="" disabled selected>Please select 2nd Salesman name</option>
         <option value="na">na</option>
        <?php //foreach($blogusers as $cont)
		  // {
		    //$user_info = get_userdata($cont->ID);
		   ?>
        <!--<-->
        <?php

		   //}
		  global $wpdb; $lastSecondSalesMan=''; $flagRS=false;
		   $myrows = $wpdb->get_results( "SELECT salesman_name,contact_email FROM wp_4twsj5a02k_sag_jobs GROUP BY salesman_name" );
		   foreach($myrows as $myrowss){

		 if($myrowss->salesman_name !=''){
		  $email='';
		  $email	=	$this->getsalesmanmail($myrowss->salesman_name);
		
		// if(!empty($email)){
 		 if($myrowss->salesman_name=='RS' && $email= 'rscott@salbertglass.com' && $flagRS==false){
			$flagRS= true; continue;
		  }
		if($myrowss->salesman_name=='AJ'){
				$lastSecondSalesMan = $myrowss->salesman_name;
				}
			else if($myrowss->salesman_name=='na'){

			}else{
		 ?>
        <option value="<?php echo $myrowss->salesman_name."#".$email?>"><?php echo $myrowss->salesman_name ?></option>
        <?php // }

			}
		}}?>
        <!--<option value="<?php echo "test#alex@imajine.us";?>">test salesman</option>-->
        <option value="<?php echo "RS#rscott@salbertglass.com";?>">RS</option>
		<option value="<?php echo "ALEX#alex@imajine.us";?>">ALEX</option>
 <?php if( $lastSecondSalesMan=='AJ'){?>
			<option value="<?php echo $lastSecondSalesMan;?>#ajay@imajine.us"><?php echo $lastSecondSalesMan;?></option>
            <?php } ?>
      </select>
      <label for="text_csr">CSR</label>
      <?php
		  $blogusersaa = get_users( 'orderby=nicename&role=author' );

			?>
      <select id="text_csr" class="form-control" type="text" autocomplete="on" maxlength="30" required  >
        <option value="" disabled >Please select CSR</option>
        <?php
		    global $wpdb; $flagCsr=false;
		   $myrowsd = $wpdb->get_results( "SELECT csr FROM wp_4twsj5a02k_sag_jobs GROUP BY csr" );
		   foreach($myrowsd as $myrowsss){

		 if($myrowsss->csr !=''){
		  $emailcsr='';
		  $emailcsr= $this->getcsrmail($myrowsss->csr);
	/*	 if($myrowsss->csr=='cosborne'){
		     $emailcsr= 'cosborne@salbertglass.com';
		 }else if($myrowsss->csr=='dblackwell'){
     		 $emailcsr= 'dblackwell@salbertglass.com';
		 }else if($myrowsss->csr=='rs'){
     		 $emailcsr= 'rscott@salbertglass.com';
		 }else if($myrowsss->csr=='kgrant'){
     		 $emailcsr= 'kgrant@salbertglass.com';
		 }else if($myrowsss->csr=='edwin'){
       $emailcsr= 'edwin@salbertglass.com';
		 }else if($myrowsss->csr=='gburkhart'){
			 $emailcsr= 'gburkhart@salbertglass.com';
		 }else if($myrowsss->csr=='sgreenstreet'){
			 $emailcsr='sgreenstreet@salbertglass.com';
		 }else if($myrowsss->csr=='steven'){
			 $emailcsr ='steven@salbertglass.com';
		 }
		  else if(strtolower($myrowsss->csr)=='alex' || strtolower($myrowsss->csr)=='alex@imajine.us'){
			 $emailcsr ='alex@imajine.us';
		 }
		 else{
		 $emailcsr =  'steven@salbertglass.com';
		 }*/
		  if($myrowsss->csr=='RS' && $emailcsr= 'rscott@salbertglass.com' && $flagCsr==false){
			$flagCsr= true; continue;
		  }
		  
		  
		 ?>
        <option value="<?php echo $myrowsss->csr."#".$emailcsr?>"><?php echo $myrowsss->csr ?></option>
        <?php


		   /*?> foreach($blogusersaa as $contss)
		   {
		    $user_infoss = get_userdata($contss->ID);
		   ?>
		   <option value="<?php echo $user_infoss->first_name." ".$user_infoss->last_name; ?>#<?php echo $contss->ID; ?>"><?php echo $user_infoss->first_name." ".$user_infoss->last_name; ?></option><?php */?>
        <?php

		   }}?>
       <!-- <option value="<?php echo "test#alex@imajine.us";?>">test csr</option>-->
       <option value="rs#<?php echo "rscott@salbertglass.com";?>">RS</option>
		 <option value="alex#<?php echo "alex@imajine.us";?>">ALEX</option>

      </select>
      <!-- <input id="text_csr" class="form-control" type="text" autocomplete="on" maxlength="30" required placeholder="CSR">
-->
      <!-- <input id="text_salesman_name" class="form-control" type="text" autocomplete="on" autofocus maxlength="70" required placeholder="Salesman full name">-->
      <label for="text_job_name">Job Description</label>
      <textarea id="text_job_name" class="form-control" type="text" autocomplete="on"  required placeholder="Job Description"></textarea>








    </div>
    <div class="col-md-6 col-sm-6">
      <div class="row sub-row">
        <div class="col-md-8">
          <label for="text_onsite_contact">Onsite contact</label>
          <input id="text_onsite_contact" class="form-control" type="text" autocomplete="on" maxlength="70" required placeholder="Onsite contact full name">
        </div>
        <div class="col-md-4">
          <label for="text_contact_number">Contact number</label>
          <input id="text_contact_number" class="form-control" type="tel" autocomplete="on" maxlength="15" pattern="(?:\(\d{3}\)|\d{3})[- ]?\d{3}[- ]?\d{4}" required placeholder="703-345-9999">
        </div>
      </div>
      <div class="row sub-row">
        <div class="col-md-6">
          <label for="text_contact_email">Onsite contact email</label>
          <input id="text_contact_email" class="form-control" type="email" autocomplete="on" maxlength="255" required placeholder="Contact email">
        </div>
        <div class="col-md-6">
          <label for="text_person_seen">Person seen</label>
          <input id="text_person_seen" class="form-control" type="text" autocomplete="on" maxlength="70" required placeholder="Person seen full name">
        </div>
      </div>

	   <div class="row sub-row">
	       
	        <div class="col-md-12" >
			  <label class="leftalign" for="text_labor_number_of_hours">CSR Notes</label>
				<textarea id="text_csr_description" maxlength="300" class="form-control" type="text" autocomplete="on"  required placeholder="CSR Notes"></textarea>
		     	</div>
	   <?php /*?><div class="form-group col-md-12 col-sm-6">

	  <label for="select_project_status">Job statussdfds</label>
          <?php
           if ( !is_super_admin() ) {
              ?>
              <select disabled id="select_project_status" class="form-control">
                <option value="not applicable" disabled selected>Please select project status</option>
                <option value="new ticket" selected>New Ticket</option>
                <option value="research">Research</option>
                <option value="submitted">Submitted</option>
                <option value="quote">Quote</option>
                <option value="na">N/A</option>
              </select>
              <?php
          } else {
           ?>
           <select id="select_project_status" class="form-control">
                <option value="not applicable" disabled selected>Please select project status</option>
                <option value="new ticket" selected>New Ticket</option>
                <option value="research">Research</option>
                <option value="submitted">Submitted</option>
                <option value="quote">Quote</option>
                 <option value="na">N/A</option>
              </select>
            <?php
          }?>
		</div><?php */?>

	   </div>





       </div>
  </div>
  <?php include 'templates/addresses.php' ?>
  <div class="form-group row">
    <div class="form-section col-sm-12 col-md-12"> <!--START LEFT SECTION -->
      <div class="row">
        <!--<div class="col-sm-7">
          <label for="textarea_project_description" class="control-label">Job Description</label>
          <textarea id="textarea_project_description" class="form-control" placeholder="Give brief description of the job."></textarea>
        </div>-->
        <div class="col-sm-5">

        </div>
      </div>
      <div class="row" >
	  <div class="col-sm-3 col-md-2 col-sm-offset-1 col-md-offset-0 job-status-row">

	   <label for="select_project_status">Job status</label>
          <?php
           //if ( !is_super_admin() ) {
              ?>
              <select  id="select_project_status" class="form-control">
                <option value="not applicable" disabled selected>Please select project status</option>
                <option value="new ticket" selected>New Ticket</option>
                <option value="research">Research</option>
                <option value="submitted">Submitted</option>
                <option value="quote">Quote</option>
                <option value="on hold">On Hold</option>
                <option value="na">N/A</option>
                <option value="void cancel">Void Cancel</option>
                <option value="void duplicate">Void Duplicate</option>
                <option value="void lost">Void Lost</option>
              </select>
              <?php
         /* } else {
           ?>
           <select id="select_project_status" class="form-control">
                <option value="not applicable" disabled selected>Please select project status</option>
                <option value="new ticket" selected>New Ticket</option>
                <option value="research">Research</option>
                <option value="submitted">Submitted</option>
                <option value="quote">Quote</option>
                 <option value="na">N/A</option>
              </select>
            <?php
          }*/?>
		  </div>

          <!-- add parking  here-->
          	<div class="form-group col-sm-5 col-md-6 col-sm-offset-3 col-md-offset-4 parking-row">
        <label for="text_project_postal_code" class="control-label">Parking</label>
        <div class="controls">
        	<!---sag-163 -->
             <select id="text_project_parking" class="form-control" name="text_project_parking">
                <option value="">Parking</option>
                <option value="Loading Dock">Loading Dock</option>
                <option value="Parking Lot">Parking Lot</option>
                <option value="Street">Street</option>
                <option value="Reserved Space">Reserved Space</option>
                 <option value="Other">Other</option>
              </select>



        </div>
        <label for="text_project_postal_code_label" id="text_project_postal_code_label"class="control-label" style="display:none;">Parking Other</label>
        <div class="controls">
             <input style="display:none;" type="text" name="text_project_parking_custom" id="text_project_parking_custom" class="form-control">
  	<!---sag-163 -->


        </div>
      </div>



      </div>
     

	  <div class="row sub-row" id="labur_copy_row">
	 
			<div class="col-sm-1">	
				<button id="btn_new_labor_add" type="button" class="btn btn-default" aria-label="Left Align" title="Add">
					<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
				</button>
			</div>
			<div id="c_remove">
			<div class="col-sm-3 col-md-2">
		
			<label class="leftalign" for="select_labor_hours_type">Labor</label>
			<select id="select_labor_hours_type" name="select_labor_hours_type[]" class="form-control">
				<option value="not_applicable" disabled selected> labor</option>
				<option value="regular">Regular</option>
				<option value="Emergency Service">Emergency Service</option>
				<option value="Night">Night</option>
				<option value="overtime">Overtime</option>
				<option value="Saturday">Saturday</option>
				<option value="weekend">Weekend</option>
				<option value="sunday">Sunday</option>
				<option value="Shop Fab">Shop Fab</option>
				<option value="Contract Labor">Contract Labor</option>
				<option value="Contract Department Labor O.T">Contract Department Labor O.T</option>
				<option value="Sub Labor">Sub Labor</option>
					<!--sag-156-->
				<option value="Delivery">Delivery</option>
 				<option value="Delivery (Salesman)" >Delivery (Salesman)</option>
 				<!--sag-170-->
 				<option value="Field Measure">Field Measure</option>
				 <option value="Field Measure OT" >Field Measure OT</option>
				 <option value="Pick Up">Pick Up</option> 
				 <option value="Pick Up Hardware" >Pick Up Hardware</option>
			</select>
			</div>
			<div class="col-sm-2">
			<label class="leftalign" for="text_labor_number_of_men">Installers</label>
			<div class="input-group">
				<input id="text_labor_number_of_men" name="text_labor_number_of_men[]" class="form-control" type="number" autocomplete="on" min="1" max="9999" maxlength="4" required  step="any" >
				<!--<div class="input-group-addon">Men</div>-->
			</div>
			</div>
			<div class="col-sm-2">
			<label class="leftalign" for="text_labor_number_of_hours">Hours</label>
			<div class="input-group">
				<input id="text_labor_number_of_hours" name="text_labor_number_of_hours[]" class="form-control" type="number" autocomplete="on" min="0" max="9999" maxlength="4" required  >
				<!--<div class="input-group-addon">Hours</div>-->
			</div>
			</div>

			<div class="form-group col-sm-5 col-md-6">
				<label for="text_labur_notes" class="control-label">Notes</label>
				<div class="controls">
					<input id="text_labur_notes" name="text_labur_notes[]"  class="form-control" type="text"  required placeholder="Notes">
				</div>
			</div>


			</div>
	 </div>


    </div>

   <!-- <div class="form-section col-sm-12 col-md-3" style="margin-top: 9px">
	     <div class="row">
		 <div class="col-sm-12">
          <label class="leftalign">&nbsp;</label>
          <div class="">
			&nbsp;
            <div class="">&nbsp;</div>
          </div>
        </div>
		<div class="col-sm-12" >
		<!-- <div class="form-group col-sm-4">
        <label for="text_project_postal_code" class="control-label">Parking</label>
        <div class="controls">
            <input id="text_project_parking" name="text_project_parking" class="form-control" type="text"  required placeholder="Parking">
        </div>
      </div> -->


        <!--     <div class="form-group col-sm-4">
         <label for="text_project_notes" class="control-label">Notes(TEXT)</label>
         <div class="controls">
             <input id="text_project_notes" name="text_project_notes" class="form-control" type="text"  required placeholder="Notes">
         </div>
         </div> -->




    <!--         <div >-->
			 <!-- <label class="leftalign" for="text_labor_number_of_hours">CSR Notes</label>-->
				<!--<textarea id="text_csr_description" class="form-control" type="text" autocomplete="on"  required placeholder="CSR Notes"></textarea>-->
		  <!--   	</div>-->
       <!-- </div>
		</div>
    </div>-->
  </div>
  <?php include 'templates/project_tabs.php' ?>
  <br>
  <button id="myModalssbutton" type="button" data-toggle="modal" data-target="#myModalssee2" style="display:none">Open Modal</button>
  <div id="myModalssee2" class="modal fade custom_pop" role="dialog" aria-hidden="true">
    <div class="modal_outer"> <span class="close cursor"  data-dismiss="modal">&times;</span>


      <div class="modal-content">
        <div class="controls"> <span class="glyphicon glyphicon-chevron-left pull-left"></span> <span class="glyphicon glyphicon-chevron-right pull-right"></span> </div>



        <div class="mainimage"><img src="" /></div>
        <div class="galleryimageset" id="setimagegall"> </div>
        <input type="hidden" name="gallsethid" value="1" id="gallsethid" />
        <!--<div class="mySlides">
				    <div class="numbertext">1 / 4</div>
				    <img src="img_nature_wide.jpg" style="width:100%">
				    </div>

				    -->

      </div>
    </div>
  </div>
  <script>
function openModal() {
  document.getElementById('myModalss').style.display = "block";
}

function closeModal() {
  document.getElementById('myModalss').style.display = "none";
}



function currentSlide(n) {
 // showSlides(slideIndex = n);
 var ht="<img src='"+n+"' style='max-height:256px'>";
 jQuery("#myModalss").find(".modal-content .mainimage").html(ht);
}


function openModasetl() {
  document.getElementById('myModalssee2').style.display = "block";
}

function closeModalset() {
  document.getElementById('myModalssee2').style.display = "none";
 // jQuery("body").find("div").removeClass("modal-backdrop");
}

function test()
{

	//alert("sdfjsdkfj");
}

jQuery( "body" ).delegate( "img", "click", function() {
//alert("sfsdfsdfsdf");

	var get= jQuery(this).parent().closest("div").attr("id");

	//
	//alert(get);
	if(get=== 'undefined')
	{
		return;
	}



			if(get.indexOf("sketch")>=0 || get.indexOf("picture_")>=0)
			{
			jQuery("#myModalssbutton").trigger("click");
				//openModasetl();
				var imgsrc=jQuery(this).attr("src");
					//alert(imgsrc);
				currentSlide(imgsrc);
				var gethtml=jQuery("#"+get).find("ul").html();
				//alert(gethtml);

				jQuery(".galleryimageset").html(gethtml);

			}
	//alert(get);
	//var classset =jQuery(this).parent().closest("div").attr("class");
	//alert(get.indexOf("setimage")>=0);
			//if(get.indexOf("setimage")>=0)
			{
			//alert("iuiuiu");
				var images=jQuery(this).attr("src");
				//alert(images);
				jQuery(".mainimage").find("img").attr("src",images);
			}




});
jQuery("body").delegate(".pull-left","click",function(){

var getvalue=jQuery("#gallsethid").val();
	getvalue=parseInt(getvalue);
	getvalue=getvalue-1;

if(getvalue>0)
	{
	jQuery("#gallsethid").val(getvalue);
	var nthchild=jQuery(".galleryimageset").find("li:nth-child("+getvalue+")");
	var setsrc=nthchild.find("img").attr("src");

	jQuery(".mainimage").find("img").attr("src",setsrc);
	}

});

jQuery("body").delegate(".pull-right","click",function(){

var length=jQuery(".galleryimageset").find("li").length;

	var getvalue=jQuery("#gallsethid").val();
	getvalue=parseInt(getvalue);
	getvalue=getvalue+1;

if(getvalue<=length)
{
	jQuery("#gallsethid").val(getvalue);
	var nthchild=jQuery(".galleryimageset").find("li:nth-child("+getvalue+")");
	var setsrc=nthchild.find("img").attr("src");

	jQuery(".mainimage").find("img").attr("src",setsrc);

}

});

 //jQuery(document).on('click', 'img', function() { alert("hello"); });


</script>
  <style type="text/css">

  /* add thi for margin left right in page*/
.btn_new_labor_remove{
    background-color: #ed261c !important;
    color: #fff !important;
}
.input-group {
    position: relative;
    display: table;
    /* border-collapse: separate; */
    width: 100%;
}
.labur_row{
    position: relative;
    clear: both;
}
.labur_row .col-sm-1 {
    margin-top: 0;
    top: 27px;
}

.container-fluid {
    padding-right: 16px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-left: 4%;
    margin-right: 4%;
}
#labur_copy_row .col-sm-1{
    position: absolute;
    left: -50px;
}
.modal_outer{
    background:#fff;
    left: 50%;
    border-radius:5px;
    max-width: 500px;
    padding: 30px;
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 100%;
	text-align:center;
}
.modal{
  display:none;
  position:fixed;
  z-index:99;
  padding-top:0px;
  left:0;
  top:0;
  width:100%;
  height:100%;
  overflow:auto;
  background:rgba(0, 0, 0, 0.9);
}

/* Modal Content */
.modal-content{
    background:rgba(0, 0, 0, 0);
    border: medium none;
    box-shadow: 0 0 0;
    margin: auto;
    padding: 0;
    position: relative;
}
/* add css for plus icon in labour */
#btn_new_labor_add{
          margin-top: 26px;
          background-color: #5bb85b;
          color: #fff;
      }


.close{
  background:#ffffff;
  border-radius:50%;
  box-shadow:0px 0px 5px #333333;
  color:#000000;
  font-size:30px;
  font-weight:normal;
  height:30px;
  line-height:30px;
  opacity:1!important;
  position:absolute;
  right:-11px;
  text-align:center;
  top:-18px;
  width:30px;
}
.close:hover,
.close:focus{
  background:#ffffff;
  color: #999;
  text-decoration: none;
  cursor: pointer;
  opacity:1!important;
}
.galleryimageset {
  float: left;
  text-align: center;
  width: 100%;
}
.galleryimageset li {
  border: 1px solid #ccc;
  display: inline-block;
  float: left;
  list-style: outside none none;
  margin-right: 10px;
  max-width: 150px;
  padding: 10px;
  text-align: center;
}
.galleryimageset li .info{
    display: block;
    word-break: break-all;
}
.modal-content .controls{
    position: absolute;
    top: 50%;
    width: 100%;
      height: 20px;
	display: block;
}
.modal-content .controls span{
  cursor:pointer;
}

#myModalssee2{
 z-index: 9999;
}

@media(max-width:992px) {
.container-fluid {
    padding-right: 16px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-left: 4%;
    margin-right: 4%;
}
#labur_copy_row .col-sm-1 {
	position: absolute;
	left: -50px;
}
.tab-jobs.my_cus_btn{ width:100%;}
/*#labur_copy_row label {
	min-height: 43px;
}*/
#labur_copy_row .col-sm-1 {position: absolute;left: 0;}
#labur_copy_row {margin-left: 40px;}

#labur_copy_row .labur_row .col-sm-1 { position: absolute; left: -52px;}

}
@media(max-width:767px) {
	#form-header{ height:auto;}
#form-header #logo-wrapper{ width:100% !important;}
#form-header .jobs-tab-icons{ width:100% !important;}
}
@media(max-width:600px) {
.container-fluid {
    padding-right: 16px;
    padding-left: 15px;
    margin-right: auto;
    margin-left: auto;
    margin-left: 4%;
    margin-right: 4%;
}
#labur_copy_row label {min-height: inherit;}


}

</style>

  <input type="hidden" name="seettt" value="" id="seettt"/>
  <button id="button_submit_new_job" type="submit" class="btn btn-primary btn-lg right float-left-mobile button_submit_jobsearch" style="margin-left:5px;">Submit New Job</button>
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
  <button id="button_submit_save_job" type="submit" class="btn btn-primary btn-lg right float-left-mobile button_submit_jobsearch">Save</button>
</form>
<?php
  }

  function export_pdf($atts = [], $content = null) {
      
    wp_enqueue_script( 'sag-jobs-pdf-moment',  plugins_url( 'js/lib/moment.min.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-pdf-moment-timezone',  plugins_url( 'js/lib/moment-timezone.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script('sag-jobs-plugin-pdfkit',  plugins_url( 'js/pdf/pdfkit.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-blob-stream',  plugins_url( 'js/pdf/blob-stream.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-pdf-vars',  plugins_url( 'js/pdf/variables.js', __FILE__), array('jquery'));

    wp_enqueue_script('sag-jobs-plugin-pdf-helpers',  plugins_url( 'js/pdf/helpers.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-general-methods',  plugins_url( 'js/pdf/general/methods.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-glass-methods',  plugins_url( 'js/pdf/glass_pieces/methods.js', __FILE__), array('jquery'));
	 wp_enqueue_script('sag-jobs-plugin-glass-methods-old-view',  plugins_url( 'js/pdf/glass_pieces/methods-old-view.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-gdr-methods',  plugins_url( 'js/pdf/door_repair/gdr.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-gdn-methods',  plugins_url( 'js/pdf/door_repair/gdn.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-sfd-methods',  plugins_url( 'js/pdf/door_repair/sfd.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-wmd-methods',  plugins_url( 'js/pdf/door_repair/wmd.js', __FILE__), array('jquery'));
     wp_enqueue_script('sag-jobs-plugin-sdbg-methods',  plugins_url( 'js/pdf/door_repair/sdbg.js', __FILE__), array('jquery'));

     wp_enqueue_script('sag-jobs-plugin-patio-methods',  plugins_url( 'js/pdf/new_door/patio.js', __FILE__), array('jquery'));

    wp_enqueue_script('sag-jobs-plugin-nwdr-methods',  plugins_url( 'js/pdf/new_door/nwdr.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-nwaluft-methods',  plugins_url( 'js/pdf/new_door/nwaluft.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-nwaluff-methods',  plugins_url( 'js/pdf/new_door/nwaluff.js', __FILE__), array('jquery'));

	    wp_enqueue_script('sag-jobs-plugin-nwaluof-methods',  plugins_url( 'js/pdf/new_door/nwaluof.js', __FILE__), array('jquery'));



    wp_enqueue_script('sag-jobs-plugin-hmnf-methods',  plugins_url( 'js/pdf/new_door/hmnf.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-hmnd-methods',  plugins_url( 'js/pdf/new_door/hmnd.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-notes-methods',  plugins_url( 'js/pdf/notes/notes.js', __FILE__), array('jquery'));
    wp_enqueue_script('sag-jobs-plugin-print_pdf',  plugins_url( 'js/pdf/print_pdf.js', __FILE__), array('jquery'));
    wp_localize_script('sag-jobs-plugin-print_pdf', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );

    ?>
<iframe id="frame_content" src="" frameborder='0' scrolling='auto' marginheight='0' marginwidth='0' allowfullscreen='true' width='100%' height='792px'></iframe>
<!-- 792px 8.5inch page approximately -->
<?php

  }

  public function jobs_list($atts = [], $content = null) {
    //LX TESTING
   // header('Access-Control-Allow-Origin: *'); 
    wp_enqueue_script( 'sag-jobs-plugin-lxIntegration', plugins_url( 'js/lx/methods.js', __FILE__ ), array( 'jquery' ) );
    wp_localize_script('sag-jobs-plugin-lxIntegration', 'ajax_object', array( 'ajax_url' => admin_url( 'admin-ajax.php' ) ) );
    wp_enqueue_script( 'sag-jobs-moment',  plugins_url( 'js/lib/moment.min.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script( 'sag-jobs-moment-timezone',  plugins_url( 'js/lib/moment-timezone.js', __FILE__ ), array( 'jquery' ) );
    wp_enqueue_script('sag-jobs-spin', plugins_url('js/lib/spin.min.js', __FILE__), array('jquery') );
    global $wpdb;

//	  global $wpdb;
		 // $_GET['job_id']=2538;

		  ob_start();
		  session_start();
		  $this->export_pdf();
	//	  echo "session data start from here <hr>";
		  $datasession =  $_SESSION['pdfdata'];
		//  echo "session data end at here <hr>";
		   ob_clean();
//		   die;
		 //   echo "session data start from here";
		//	echo "<pre>";

		   /*subbmitting mail to csr QS031001_SLA_FCSP */





		   /*subbmitting mail to csr*/


    $table_sag_jobs = $wpdb->prefix . 'sag_jobs';
    $user_id = get_current_user_id();
	$current_user = wp_get_current_user();

	   /* if (  $user_id == 17 || $user_id == 16  ||  $user_id ==  13 || $user_id == 15  ){ //Greg and Steven are assigned as admins.
	    	 $sql = "SELECT id, job_name, project_description,quote_number, project_status, salesman_name, csr, project_address_line1,project_state, created_date ,modifydate,accepted FROM " . $table_sag_jobs . " where project_status !='na' ORDER BY created_date DESC";

	    }
	    else{
				 $sql = "SELECT * FROM  ".$table_sag_jobs." WHERE salesman_id = ". $user_id . " and project_status !='na' ORDER BY created_date DESC";
	    }*/

		$is_csr = false;
		 $csr_name = '';
		 $is_salesman = false;
		 $salesman_name = '';
		if ($current_user->user_login == 'SG'){
			$is_csr = true;
			$csr_name = 'sgreenstreet';
		}
		if ($current_user->user_login == 'CAO'){
			$is_csr = true;
			$csr_name = 'cosborne';
		}
		if ($current_user->user_login == 'DB'){
			$is_csr = true;
			$csr_name = 'dblackwell';
		}
		if ($current_user->user_login == 'KG'){
			$is_csr = true;
			$csr_name = 'kgrant';
		}
		if ($current_user->user_login == 'akrakowski'){
			$is_csr = true;
			$csr_name = 'akrakowski';
		}
		//echo $current_user->user_login;die;
		if ($current_user->user_login == 'scoleman@salbertglass.com'){
			$is_csr = true;
			$csr_name = 'scoleman';
		}

		if ($current_user->user_login == 'ehuber@salbertglass.com'){
			$is_csr = true;
			$csr_name = 'ehuber';
		}

		if ($current_user->user_login == 'skurth@salbertglass.com'){
			$is_csr = true;
			$csr_name = 'skurth';
		}

		if ($is_csr){ //Greg and Steven are assigned as admins.
	    	  $sql = "SELECT * FROM " . $table_sag_jobs . " where project_status ='new ticket' and csr = '".$csr_name."' ORDER BY created_date DESC";

	    }
		if ($current_user->user_login == 'SLA' || $current_user->user_login == 'RS'  || $current_user->user_login == 'CW' || $current_user->user_login == 'KL' || $current_user->user_login == 'GRB' || $current_user->user_login == 'EA' || $current_user->user_login == 'AJ' || $current_user->user_login == 'AKr'){
			$is_salesman = true;
			$salesman_name = $current_user->user_login;
			$sql = "SELECT * FROM  ".$table_sag_jobs." WHERE (owner_name = '". $salesman_name . "') and project_status ='new ticket' ORDER BY created_date DESC";
		}
	    if(!$is_csr && !$is_salesman){

				$sql = "SELECT * FROM " . $table_sag_jobs . " where project_status ='new ticket' ORDER BY created_date DESC";
	    }

    $sag_jobsjjjj = $wpdb->get_results( $sql );

	$counttotal=count($sag_jobsjjjj);

	$showlimit=50;

	if(isset($_REQUEST['pagesno']))
	{
		$start=$_REQUEST['pagesno']*$showlimit;

	}
	else
	{
		$start=	0;

	}

	 $sql=$sql." limit ".$start." ,".$showlimit;

	$sag_jobs = $wpdb->get_results( $sql );

 // echo '<pre>'; print_r($sag_jobs); die;

  //  if ( $wpdb->num_rows > 0 ) {
    ?>
	<script type="text/javascript">
	jQuery(document).ready(function(){

      
		jQuery("#addquoteset").click(function(){

			var gethtml=jQuery("#addquoteset").html();
			var getvalue=jQuery("#quotenumberadd").val();

			if(gethtml.indexOf("Save")>=0)
			{

				if(jQuery("#adding").val()==0)
				{


				if(!getvalue)
					{
						jQuery("#quotenumberadd").focus();

					}
					else
					{
						jQuery.post("<?php echo admin_url("admin-ajax.php");?>",{action:'addquote_api',quotenumber:getvalue},
							function(resp){
								data = JSON.parse(resp);
								//alert(data.message);
								//alert(data.setmessage);

								if(data.message=="success")
								{
									jQuery("#messagesethtml").html(data.setmessage);
									setTimeout(function(){jQuery("#messagesethtml").html(" "); }, 3000);
								}
								else
								{
									jQuery("#quotenumberadd").hide();
									jQuery("#adding").val("1");
									jQuery("#messagessggg").html(data.setmessage);
									//setTimeout(function(){jQuery("#messagesethtml").html(" "); }, 3000);

								}

							}
						);

					}

					}
					else
					{

					jQuery.post("<?php echo admin_url("admin-ajax.php");?>",{action:'insertquote_api',quotenumber:getvalue},function(resp){
						if(resp>0)
							{

								jQuery("#messagesethtml").html("Quote successfully added.");
								jQuery("#messagessggg").html("");
								setTimeout(function(){jQuery("#messagesethtml").html(" "); }, 3000);
							}

					});


					}
			}
			else
			{
				jQuery("#addcodeset").show();
				jQuery("#addquoteset").html('<i class="fa fa-plus"></i> Save Quote');
			}
		});

	});

function getLatestJobs(k){console.log("getLatestJobs  >> " +k); k++;
			jQuery.post("<?php echo admin_url("admin-ajax.php");?>",{action:'change_joblist',jobtype:jQuery('#filter_dropdown').val()},
							function(resp){
							var	data = JSON.parse(resp);
								//alert(data.message);
								//alert(data.setmessage);
								jQuery("#filterdata_by_ajax").html(data.data);
								/*if(data.message=="success")
								{
									jQuery("#messagesethtml").html(data.setmessage);
									setTimeout(function(){jQuery("#messagesethtml").html(" "); }, 3000);
								}
								else
								{
									jQuery("#quotenumberadd").hide();
									jQuery("#adding").val("1");
									jQuery("#messagessggg").html(data.setmessage);
									//setTimeout(function(){jQuery("#messagesethtml").html(" "); }, 3000);

								}*/

							}
						);

	}
	var k=0;
//setInterval(function(){ getLatestJobs(k)}, 5000);
	</script>
	<style>
	#messagesethtml {
    color: #d9534f;
    font-size: 16px;
    padding: 10px 0;
    text-align: left;
}
.ui-timepicker-div .ui-widget-header { margin-bottom: 8px; }
.ui-timepicker-div dl { text-align: left; }
.ui-timepicker-div dl dt { float: left; clear:left; padding: 0 0 0 5px; }
.ui-timepicker-div dl dd { margin: 0 10px 10px 40%; }
.ui-timepicker-div td { font-size: 90%; }
.ui-tpicker-grid-label { background: none; border: none; margin: 0; padding: 0; }
.ui-timepicker-div .ui_tpicker_unit_hide{ display: none; }

.ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input { background: none; color: inherit; border: none; outline: none; border-bottom: solid 1px #555; width: 95%; }
.ui-timepicker-div .ui_tpicker_time .ui_tpicker_time_input:focus { border-bottom-color: #aaa; }

.ui-timepicker-rtl{ direction: rtl; }
.ui-timepicker-rtl dl { text-align: right; padding: 0 5px 0 0; }
.ui-timepicker-rtl dl dt{ float: right; clear: right; }
.ui-timepicker-rtl dl dd { margin: 0 40% 10px 10px; }

/* Shortened version style */
.ui-timepicker-div.ui-timepicker-oneLine { padding-right: 2px; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time, 
.ui-timepicker-div.ui-timepicker-oneLine dt { display: none; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_time_label { display: block; padding-top: 2px; }
.ui-timepicker-div.ui-timepicker-oneLine dl { text-align: right; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd, 
.ui-timepicker-div.ui-timepicker-oneLine dl dd > div { display:inline-block; margin:0; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_minute:before,
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_second:before { content:':'; display:inline-block; }
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_millisec:before,
.ui-timepicker-div.ui-timepicker-oneLine dl dd.ui_tpicker_microsec:before { content:'.'; display:inline-block; }
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide,
.ui-timepicker-div.ui-timepicker-oneLine .ui_tpicker_unit_hide:before{ display: none; }
.ui-timepicker-select{width: 100%;}
	</style>
<div class="form-horizontal">
  <div class="row content-row">
    <!--Yes it is 118-->
    <!--Yes it is 7-->
    <?php if ( is_user_logged_in() ) {?>
    <?php //if (1) {?>
    <div class="container-fluid sag-setting">
	  <div id="form-header" class="row header-job-information">
    	<div class="sathe">
    	    <!--<a class="btn" href="javascript:void(0)"><i class="fa fa-user"></i><?php //$current_user = wp_get_current_user();echo $current_user->user_login; ?></a>-->
    	    <div id="current-jobs-logo" class="col-xs-6 col-md-4">
    	      <img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/albertGlass.png">
    	    </div>
    	    <div class="col-xs-6 col-md-8 jobs-tab-icons">
    	    <a class="btn" href="<?php echo site_url().'/current-jobs/'?>"><i class="fa fa-wrench"></i>Jobs</a>
            <a class="btn" href="<?php echo get_permalink(get_page_by_title('Events'));?>"><i class="fa fa-calendar"></i>Events</a>
            <div class="dropdown right">
              <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><?php echo $current_user->user_login;?>
              <span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="<?php echo get_permalink(get_page_by_title('Sag Settings'))?>">Settings</a></li>
                <li><a href="<?php echo wp_logout_url(); ?>">Logout</a></li>
              </ul>
            </div>
            </div>
        </div>
      </div>
  </div>
  <div class="clearfix"></div>
<?php }?>
<div class="col-md-12">
    <br>
    <div class="page-header">
      <h1>
        <?php the_title(); ?>
      </h1>
    </div>
</div>
<style>
    body.current-jobs #kad-banner > .container,
    body.current-jobs #pageheader{
        display:none;
    }
</style>
	<div class="col-xs-12">
		<div id="messagesethtml"></div>

<form>
<div class="row">
<div id="addcodeset"  style="display:none" class="col-xs-4 quote-number">
<label>Quote Number</label>
<input type="text" class="form-control" value="" id="quotenumberadd">
</div>


<div class="clearfix" style="height: 5px"></div>
<div id="messagessggg"></div>
<input type="hidden" name="adding" value="0" id="adding" />
<div class="col-lg-4 save-quote-btn">
    <a class="btn btn-danger btn-sm" id="addquoteset" type="submit" style="float:left;"><i class="fa fa-plus"></i> Add Quote</a>
</div>
</div>
</form>

	</div>


	<div style="clear:both;" class="clearfix"></div>
	<?php 
	//echo 'test-'.$is_csr; die;
	if($is_csr || $is_salesman){
	
	?>
  <div class="col-xs-12">
<a href="javascript:void(0);" class="btn btn-primary" id="your_quotes"><i class="fa fa-user"></i> Your Quotes</a>&nbsp;&nbsp;

  <a href="javascript:void(0);" class="btn btn-default" id="team_quotes"><i class="fa fa-users" aria-hidden="true"></i> Team Quotes</a>
  </div>
  <?php }?>




    <div class="col-xs-2">
	&nbsp;


	<!-- <a class="btn btn-danger btn-sm" href="<?php echo get_permalink( get_page_by_title( 'Add Job' ) ); ?>" style="float:left;"><i class="fa fa-plus"></i>Add New Job</a> --></div>
    <div class="col-xs-4 col-xs-offset-6 search-qutoe">
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Search" name="search-term" id="search-term">
        <div class="input-group-btn">
          <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
        </div>
      </div>
    </div>
  </div>
  <div class="row">
    <div class="dropdown_style form-group" style="margin: 20px 0px !important;">
      <style>
		    .dropdown_style select{
			max-width: 220px;
			padding: 7px;
			width: 100%;
			height: auto!important;
		    }
			#team_quotes.btn-default > i.fa {
			color: #333;
			}
			#your_quotes.btn-default > i.fa {
			color: #333;
			}
			#team_quotes.btn-default{border:none;}
			#your_quotes.btn-default{border:none;}

	    </style>
      <!--<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Filter By
      <span class="caret"></span></button>-->
	  


      <div class="col-xs-2">
	  <label>Status</label>
        <select id="filter_dropdown">
          <option value="all"  >All</option>
          <option value="na" >N/A</option>
          <option value="new ticket" selected="selected" >New Ticket</option>
          <option value="research" >Research</option>
          <option value="submitted" >Submitted</option>


        </select>
      </div>
      <div class="col-xs-2">
	  <label>Sales Rep A</label>
        <select id="ownernameset">
          <option value="all"  >All Sales Rep A</option>
          <?php
		   //}
		  global $wpdb;
		   $myrows_owner = $wpdb->get_results( "SELECT owner_name FROM wp_4twsj5a02k_sag_jobs GROUP BY owner_name" );
		   foreach($myrows_owner as $myrowss_owner){
		 if($myrowss_owner->owner_name !=''){
				$email='';
				$email	=	$this->getsalesmanmail($myrowss_owner->owner_name);
	
		 ?>
          <option value="<?php echo $myrowss_owner->owner_name; ?>" <?php echo ($salesman_name == $myrowss_owner->owner_name)? 'selected="selected"':'';?>><?php echo $myrowss_owner->owner_name ?></option>
          <?php  }}?>
        </select>
      </div>

      <div class="col-xs-1 center"> &nbsp; </div>
	  <div class="col-xs-2">
	  <label>Sales Rep B</label>
        <select id="salesnameset">
          <option value="all"  >All Sales Rep B</option>
          <?php
		   //}

	//	  echo "session data end at here <hr>";

		   $myrows = $wpdb->get_results( "SELECT salesman_name,contact_email FROM wp_4twsj5a02k_sag_jobs GROUP BY salesman_name" );
		   foreach($myrows as $myrowss){

		 if($myrowss->salesman_name !=''){
				$email='';
				$email	=	$this->getsalesmanmail($myrowss->salesman_name);
	
		 ?>
          <option value="<?php echo $myrowss->salesman_name; ?>"><?php echo $myrowss->salesman_name ?></option>
          <?php  }}?>
        </select>
      </div>
	   <div class="col-xs-1 center"> &nbsp; </div>
      <div class="col-xs-2">
	  <label>CSR</label>
        <select id="csrset">

          <option value="all"  >All CSR</option>
          <?php
				global $wpdb;
			       $myrowsd = $wpdb->get_results( "SELECT csr FROM wp_4twsj5a02k_sag_jobs GROUP BY csr" );
			       foreach($myrowsd as $myrowsss){

			     if($myrowsss->csr !=''){
			      $emailcsr='';
			 
				 $emailcsr= $this->getcsrmail($myrowsss->csr);
			     ?>
          <option value="<?php echo $myrowsss->csr; ?>" <?php echo ($csr_name == $myrowsss->csr)? 'selected="selected"':'';?>><?php echo $myrowsss->csr ?></option>
     
          <?php

			       }}?>
        </select>
      </div>

	   <div class="col-xs-2">
	   <label>Accepted</label>
	   <select id="accept_set">
          <option value="all" selected="selected" >All</option>
		   <option value="1"  >Accepted</option>
		    <option value="0"  >Not Accepted</option>


		 </select>

	   </div>

    </div>
  </div>
  <div class="row content-row">
    <div class="col-xs-12">
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <th>Quote #</th>
              <th>Actions</th>
              <th>Salesman</th>
              <th>CSR</th>

              <th>Address</th>
              <th>Description</th>

              <th>Status</th>
              <th>Date Created <img src="<?php echo plugins_url();?>/jobs/img/down.png" alt="created_date" rel="desc" class="ordering created_date_desc" style="display:none;"/> &nbsp; <img src="<?php echo plugins_url();?>/jobs/img/up.png" alt="created_date" rel="asc" class="ordering created_date_asc"/></th>
              <th>Date Updated <img src="<?php echo plugins_url();?>/jobs/img/down.png" alt="modifydate" rel="desc" class="ordering modifydate_desc" style="display:none;"/> &nbsp; <img src="<?php echo plugins_url();?>/jobs/img/up.png" alt="modifydate" rel="asc" class="ordering modifydate_asc"/></th>
              <th>PDF</th>
            </tr>
          </thead>
          <tbody class="filterdata_by_ajax" id="filterdata_by_ajax">
            <?php 
            foreach ( $sag_jobs as $sag_job ) {
                ?>
            <tr>
              <td>
              <?php
              
			    
			   	$acceptedId=0;
			     if(($sag_job->salesman_name==  $current_user->user_login || $sag_job->owner_name==  $current_user->user_login) && $sag_job->accepted ==0)
				{
					
					$acceptedId = $sag_job->id;
				}
			  ?>
              
              <a  target="_blank" class="btn btn-danger btn-sm"  href="<?php echo esc_url(add_query_arg(array('job_id'=> $sag_job->id,'accepted'=>$acceptedId), get_permalink( get_page_by_title( 'Add Job' ) ))); ?>" role="button"><?php echo $sag_job->quote_number; echo ($sag_job->secondary_quote !='')? ','.$sag_job->secondary_quote : '';?></a>
			   <?php 
			   
			     if($acceptedId>0)
				{
//					$acceptedId = $sag_job->id;
				// echo "ddddd".$sag_job->accepted;

				?>
			    <a id="accepted_<?php echo $sag_job->id; ?>" class="btn btn-danger btn-sm" href="" role="button" style="margin-top:2px;width:78px;">Accept</a>
				<?php }

			  ?>
				</td>
              <td class="center"><span class="dashboard-edit-job glyphicon glyphicon-pencil" id="editjobset_<?php echo  $sag_job->id; ?>"></span>
              &nbsp;
				<a class="dashboard-event" data-jobid="<?php echo  $sag_job->id;?>">
					<span class="glyphicon glyphicon-calendar"></span>
				</a>
				</td>
              <td><?php //echo $sag_job->salesman_name; ?>
                <select style="max-width:100px;" id="ownernamejob_<?php echo $sag_job->id; ?>" disabled="disabled">
                  <?php
		   //}
		  global $wpdb;
		   $myrows_owner = $wpdb->get_results( "SELECT owner_name FROM wp_4twsj5a02k_sag_jobs GROUP BY owner_name" );
		   foreach($myrows_owner as $myrowss_owner){

		 if($myrowss_owner->owner_name !=''){
		  $email='';
		
		 		 $email	=	$this->getsalesmanmail($myrowss_owner->owner_name);
		 		if($sag_job->owner_name==$myrowss_owner->owner_name)
				{
					$selectset="selected=selected";

				}
				else
				{
					$selectset=" ";
				}

		 ?>
                  <option value="<?php echo $myrowss_owner->owner_name; ?>"<?php echo $selectset; ?>><?php echo $myrowss_owner->owner_name ?></option>
                  <?php  }}?>
                </select></br></br>
				<select style="max-width:100px;" id="salesnamejob_<?php echo $sag_job->id; ?>" disabled="disabled">
                 <?php
		//}
		global $wpdb;
		$myrows = $wpdb->get_results( "SELECT salesman_name,contact_email FROM wp_4twsj5a02k_sag_jobs GROUP BY salesman_name" );
		foreach($myrows as $myrowss){
			
			
			if($myrowss->salesman_name !=''){
				$email='';
			
					if($sag_job->salesman_name==$myrowss->salesman_name)
					{
						$selectset="selected=selected";
					}
					else
					{
						$selectset=" ";
					}
					?>
                  <option value="<?php echo $myrowss->salesman_name; ?>"<?php echo $selectset; ?>><?php echo $myrowss->salesman_name ?></option>
                  <?php }}?>
				  
				  
				  
                </select></td>
              <td><?php //echo $sag_job->salesman_name; ?>
                <select style="max-width:100px;" id="csrjob_<?php echo $sag_job->id; ?>" disabled="disabled" >
                  <?php
					global $wpdb;
			       $myrowsd = $wpdb->get_results( "SELECT csr FROM wp_4twsj5a02k_sag_jobs GROUP BY csr" );
			       foreach($myrowsd as $myrowsss){
					if($myrowsss->csr !=''){
						$emailcsr='';
				
					 $emailcsr= $this->getcsrmail($myrowsss->csr);
					
					if($myrowsss->csr==$sag_job->csr){
						$csrselectset="selected=selected";
					}
					else{
						$csrselectset=" ";
					}
			     ?>
                  <option value="<?php echo $myrowsss->csr; ?>#<?php echo $emailcsr; ?>" <?php echo $csrselectset; ?>><?php echo $myrowsss->csr ?></option>
                  <?php  }}?>
                </select></td>

              <td style="max-width:150px; overflow-wrap: break-word;"><?php echo $sag_job->project_address_line1."<br>". 	$sag_job->project_state; ?></td>
              <td style="max-width:150px; overflow-wrap: break-word;"><?php echo $sag_job->job_name; ?></td>
              <td><?php echo $sag_job->project_status; ?></td>
              <td><?php echo  date("m/d/Y g:i A", strtotime($sag_job->created_date) - 60 * 60 * 4) ; // for day light saving we have to replace 4 to 5, without day ligt saving its 4 ?></td>
              <td><?php echo date("m/d/Y g:i A", strtotime($sag_job->modifydate) - 60 * 60 * 4)
              //echo mysql2date("m/d/Y g:i A", strtotime($sag_job->modifydate)  );
              ?>  </td>

			   <?php
			   $current_user = wp_get_current_user();



				?>

             <td class="center" >


			  <a id="export_pdf_btn" target="_blank" class="btn btn-danger btn-sm" href="<?php echo esc_url(add_query_arg('job_id', $sag_job->id, get_permalink( get_page_by_title( 'Print PDF' ) ))); ?>" role="button">Pdf</a>





			  </td>
            </tr>
            <?php
                }
                ?>
            <tr>
              <td colspan="9" style="background:#fff"><div class="pagination pull-left" style="margin: 0">
                  <!--	<a href="#">&laquo;</a>
-->
                  <?php
	$pages= ceil($counttotal/$showlimit);

if($counttotal>50)
{
	for($i=1;$i<=$pages;$i++)
	{
			if($_REQUEST['pagesno']==$i)
			{
				$class="class='active'";
			}
			else
			{
				$class =" ";

			}
			if($i==1 && !$_REQUEST['pagesno'])

			{
				$class="class='active'";
			}

			global $post;
			$siteurl=get_permalink($post->ID)."?pagesno=".$i;
			echo "<a href='' id='pageno_".$i."' ".$class.">".$i."</a>  ";

	}
}
?>
                  <!--	<a href="#">&raquo;</a>
--> </div></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="myModaldddd" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Job is accepted successfully....</h4>
        </div>

      </div>

    </div>
  </div>


<style type="text/css">
.dashboard-event,.dashboard-edit-job{color: #333; cursor:pointer;}
.dashboard-event:hover,.dashboard-edit-job:hover{color: #666;}
.ordering{

cursor:pointer;}
				.pagination a {
				color: black;
				float: left;
				padding: 8px 16px;
				text-decoration: none;
				transition: background-color .3s;
				}

				.pagination a.active {
				background-color: #4CAF50;
				color: white;
				}

				.pagination a:hover:not(.active) {background-color: #ddd;}


	</style>
<script type="text/javascript">

jQuery("body").delegate("[id^=pageno_]","click",function(event){
	event.preventDefault();


	 var opts = {
          lines: 13 // The number of lines to draw
        , length: 15 // The length of each line
        , width: 10 // The line thickness
        , radius: 20 // The radius of the inner circle
        , scale: 1 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#c72c00' // #rgb or #rrggbb or array of colors
        , opacity: 0.25 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 60 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '10%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
        }



			//alert(jQuery(this).attr("id"));
				var idset=jQuery(this).attr("id").split("_");

				var spinTarget = document.getElementById('filterdata_by_ajax');
				var spinner = new Spinner(opts).spin(spinTarget);
				var searchterm =  jQuery("#search-term").val();
				var salesnameset = jQuery('#salesnameset').val();
				var ownernameset = jQuery('#ownernameset').val();
				var csrset = jQuery('#csrset').val();
				console.log("searchterm is " + searchterm);
				var dropdownvalue  = jQuery('#filter_dropdown').val();
				var statusaccept=jQuery("#accept_set").val();

						jQuery.ajax({
						url :'<?php echo admin_url('admin-ajax.php')?>',
						data:{dropdownvalue:dropdownvalue, searchterm: searchterm, action:'search_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,pageno:idset[1],accept:statusaccept},

						type:'POST',
						success:function(res_datafilter){
						console.log("res_datafilter is "+ res_datafilter)
						jQuery('.filterdata_by_ajax').html(res_datafilter);
						spinner.spin(false);
						},
						error: function (xhr, ajaxOptions, thrownError) {
						spinner.spin(false);
						}
						});

						jQuery("a").removeClass("active");
						jQuery(this).addClass("active");




	});

jQuery("body").delegate("[id^=accepted_]","click",function(e){
e.preventDefault();


	var getid=jQuery(this).attr("id");
	var findid=getid.split("_");
	var jobid=findid[1];

	jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{action:'change_accepted',job_id:jobid},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
				//alert(res_datafilter);

						//alert(getid);
						jQuery('#'+getid).hide();
						jQuery("#myModaldddd").show();
						jQuery("#myModaldddd").removeClass("fade");

								setTimeout(function(){
								jQuery("#myModaldddd").hide();
						jQuery("#myModaldddd").addClass("fade");
								}, 2000);


  					}
  				  });



});

jQuery( "body" ).delegate( "[id^=editjobset]", "click", function() {


		//alert(jQuery(this).attr("class"));
		var classs=jQuery(this).attr("class");
    var tablerow = jQuery(this).closest('tr');
    console.log("tablerow is " + tablerow);
    tablerow.addClass('warning');
		var getid=jQuery(this).attr("id");
				var getsplit=getid.split("_");
				var idset=getsplit[1];

		if(classs.indexOf("glyphicon-pencil")>=0) //first editing
		{

				jQuery("#salesnamejob_"+idset).attr("disabled",false);
				jQuery("#ownernamejob_"+idset).attr("disabled",false);
				jQuery("#csrjob_"+idset).attr("disabled",false);
				jQuery(this).removeClass("glyphicon-pencil");
				jQuery(this).addClass("glyphicon-ok");
		}
		else if(classs.indexOf("glyphicon-ok")>=0) //we are submitting changes
		{
			var salesnameset=jQuery("#salesnamejob_"+idset).val();
			var ownernameset=jQuery("#ownernamejob_"+idset).val();
			var csrset=jQuery("#csrjob_"+idset).val();

			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{action:'chagnesales_name',salesname:salesnameset,ownername:ownernameset,csrname:csrset,job_id:idset},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
				//alert(res_datafilter);

						//alert(getid);
								jQuery("#salesnamejob_"+idset).attr("disabled",true);
								jQuery("#ownernamejob_"+idset).attr("disabled",true);
								jQuery("#csrjob_"+idset).attr("disabled",true);
								jQuery("#"+getid).removeClass("glyphicon-ok");
								jQuery("#"+getid).addClass("glyphicon-pencil");
                tablerow.removeClass('warning');
				jQuery("#accepted_"+idset).hide();
					//window.location= "<?php //echo site_url();?>/current-jobs/?printjobtrue=1&jobs_id="+idset;

  					}
  				  });

		}

	});

	</script>
<script>
  	jQuery(function(){
		jQuery('.ordering').on('click', function(){
	  var orderby =  jQuery(this).attr('alt');
	  var ascordesc =  jQuery(this).attr('rel');
	  var toshow = (ascordesc == 'asc')? 'desc' : 'asc';
	  jQuery(this).hide();
	  jQuery('.'+orderby+'_'+toshow).show();
  	  var dropdownvalue =  jQuery('#filter_dropdown').val();
	  var salesnameset = jQuery('#salesnameset').val();
	  var ownernameset = jQuery('#ownernameset').val();
	  var csrset=jQuery('#csrset').val();
	  var searchterm=jQuery('#search-term').val();
	  var statusaccept=jQuery("#accept_set").val();
	  if(jQuery('#your_quotes').hasClass('btn-primary')){
	  	var fromyourquote='1';
	  }else{
	  	var fromyourquote='0';
	  }

  			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept,orderby:orderby,ascordesc:ascordesc,fromyourquote:fromyourquote},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
  					jQuery('.filterdata_by_ajax').html(res_datafilter);
  					}
  				  });
  	  });
  	  jQuery('#filter_dropdown').on('change', function(){

  	  var dropdownvalue =  jQuery(this).val();
	  var salesnameset = jQuery('#salesnameset').val();
	  var ownernameset = jQuery('#ownernameset').val();
	  var csrset=jQuery('#csrset').val();
	  var searchterm=jQuery('#search-term').val();
	  var statusaccept=jQuery("#accept_set").val();

  			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
  					jQuery('.filterdata_by_ajax').html(res_datafilter);
  					}
  				  });
  	  });

	  jQuery('#team_quotes').on('click', function(){
	  	jQuery('#your_quotes').removeClass('btn-primary');
		  jQuery('#your_quotes').addClass('btn-default');
	  	  jQuery(this).removeClass('btn-default');
		  jQuery(this).addClass('btn-primary');
		  jQuery('#filter_dropdown').val('new ticket');
		  jQuery("#salesnameset").val("all");
			jQuery("#ownernameset").val("all");
		  jQuery("#csrset").val("all");
		  var dropdownvalue =  jQuery('#filter_dropdown').val();
		  var salesnameset = jQuery('#salesnameset').val();
		  var ownernameset = jQuery('#ownernameset').val();
		  var csrset=jQuery('#csrset').val();
		  var searchterm=jQuery('#search-term').val();
		  var statusaccept=jQuery("#accept_set").val();

				jQuery.ajax({
					url :'<?php echo admin_url('admin-ajax.php')?>',
					data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept},
					//dataType:'json',
					type:'POST',
					success:function(res_datafilter){
					console.log("res_datafilter is "+ res_datafilter)
						jQuery('.filterdata_by_ajax').html(res_datafilter);
						}
					  });
  	  });

	  jQuery('#your_quotes').on('click', function(){
	  	jQuery('#team_quotes').removeClass('btn-primary');
		  jQuery('#team_quotes').addClass('btn-default');
	  	  jQuery(this).removeClass('btn-default');
		  jQuery(this).addClass('btn-primary');
		  <?php if($is_salesman){?>
		  jQuery("#salesnameset").val("all");
		  jQuery("#csrset").val("all");
		  jQuery("#ownernameset").val("<?php echo $salesman_name?>");
		  jQuery("#accept_set").val("all");
		  <?php }?>

		  <?php if($is_csr){?>
		  jQuery("#csrset").val("<?php echo $csr_name?>");
		  jQuery("#salesnameset").val("all");
		  jQuery("#accept_set").val("all");
		  jQuery("#ownernameset").val("all");
		  <?php }?>
		  jQuery('#filter_dropdown').val('new ticket');
		  var dropdownvalue =  jQuery('#filter_dropdown').val();
		  var salesnameset = jQuery('#salesnameset').val();
		  var ownernameset = jQuery('#ownernameset').val();
		  var csrset=jQuery('#csrset').val();
		  var searchterm=jQuery('#search-term').val();
		  var statusaccept=jQuery("#accept_set").val();
	var fromyourquote='1';
				jQuery.ajax({
					url :'<?php echo admin_url('admin-ajax.php')?>',
					data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept,fromyourquote:fromyourquote},
					//dataType:'json',
					type:'POST',
					success:function(res_datafilter){
					console.log("res_datafilter is "+ res_datafilter)
						jQuery('.filterdata_by_ajax').html(res_datafilter);
						}
					  });
  	  });


	    jQuery('#salesnameset').on('change', function(){

		 jQuery("#csrset").val("all");
		  jQuery("#ownernameset").val("all");
  	  var salesnameset =  jQuery(this).val();
	  var dropdownvalue = jQuery('#filter_dropdown').val();
	   var searchterm=jQuery('#search-term').val();
		var ownernameset=jQuery('#ownernameset').val();
	    var csrset=jQuery('#csrset').val();
		var statusaccept=jQuery("#accept_set").val();

  			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
  					jQuery('.filterdata_by_ajax').html(res_datafilter);
  					}
  				  });
  	  });

 jQuery('#ownernameset').on('change', function(){

	jQuery("#csrset").val("all");
	jQuery("#salesnameset").val("all");
  	  var ownernameset =  jQuery(this).val();
	  var dropdownvalue = jQuery('#filter_dropdown').val();
	   var searchterm=jQuery('#search-term').val();
	   var salesnameset=jQuery('#salesnameset').val();
		var ownernameset=jQuery('#ownernameset').val();
	    var csrset=jQuery('#csrset').val();
		var statusaccept=jQuery("#accept_set").val();

  			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
  					jQuery('.filterdata_by_ajax').html(res_datafilter);
  					}
  				  });
  	  });


	   jQuery('#csrset').on('change', function(){

	    jQuery("#salesnameset").val("all");
		jQuery("#ownernameset").val("all");
  	  var csrset =  jQuery(this).val();
	  var dropdownvalue = jQuery('#filter_dropdown').val();
	    var salesnameset=jQuery('#salesnameset').val();
		var ownernameset=jQuery('#ownernameset').val();
		 var searchterm=jQuery('#search-term').val();
		  var statusaccept=jQuery("#accept_set").val();

  			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
  					jQuery('.filterdata_by_ajax').html(res_datafilter);
  					}
  				  });
  	  });



	  	   jQuery('#accept_set').on('change', function(){


  	  var csrset =  jQuery("#csrset").val();
	  var dropdownvalue = jQuery('#filter_dropdown').val();
	    var salesnameset=jQuery('#salesnameset').val();
		 var ownernameset=jQuery('#ownernameset').val();
		 var searchterm=jQuery('#search-term').val();
		  var statusaccept=jQuery(this).val();

  			jQuery.ajax({
  				url :'<?php echo admin_url('admin-ajax.php')?>',
  				data:{dropdownvalue:dropdownvalue,action:'add_filter_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,searchterm:searchterm,accept:statusaccept},
  				//dataType:'json',
  				type:'POST',
  				success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
  					jQuery('.filterdata_by_ajax').html(res_datafilter);
  					}
  				  });
  	  });





      jQuery('#search-term').on('keyup', function(){
        var opts = {
          lines: 13 // The number of lines to draw
        , length: 15 // The length of each line
        , width: 10 // The line thickness
        , radius: 20 // The radius of the inner circle
        , scale: 1 // Scales overall size of the spinner
        , corners: 1 // Corner roundness (0..1)
        , color: '#c72c00' // #rgb or #rrggbb or array of colors
        , opacity: 0.25 // Opacity of the lines
        , rotate: 0 // The rotation offset
        , direction: 1 // 1: clockwise, -1: counterclockwise
        , speed: 1 // Rounds per second
        , trail: 60 // Afterglow percentage
        , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
        , zIndex: 2e9 // The z-index (defaults to 2000000000)
        , className: 'spinner' // The CSS class to assign to the spinner
        , top: '10%' // Top position relative to parent
        , left: '50%' // Left position relative to parent
        , shadow: false // Whether to render a shadow
        , hwaccel: false // Whether to use hardware acceleration
        , position: 'absolute' // Element positioning
        }
        var spinTarget = document.getElementById('filterdata_by_ajax');
        var spinner = new Spinner(opts).spin(spinTarget);
        var searchterm =  jQuery(this).val();
		 var salesnameset = jQuery('#salesnameset').val();
		 var ownernameset = jQuery('#ownernameset').val();
		  var csrset = jQuery('#csrset').val();
		   var statusaccept=jQuery("#accept_set").val();
        console.log("searchterm is " + searchterm);
        var dropdownvalue  = jQuery('#filter_dropdown').val();

        jQuery.ajax({
          url :'<?php echo admin_url('admin-ajax.php')?>',
          data:{dropdownvalue:dropdownvalue, searchterm: searchterm, action:'search_ajax',salesname:salesnameset,ownername:ownernameset,csrname:csrset,accept:statusaccept},
          type:'POST',
          success:function(res_datafilter){
            console.log("res_datafilter is "+ res_datafilter)
            jQuery('.filterdata_by_ajax').html(res_datafilter);
            spinner.spin(false);
          },
          error: function (xhr, ajaxOptions, thrownError) {
            spinner.spin(false);
          }
        });
      })
  	});
  	</script>
<!-- Button trigger modal -->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title center" id="myModalLabel">Add Yourself as Salesman</h4>
      </div>
      <div class="modal-body">
        <form id="add-salesman">
          <div class="form-group">
            <input id="salesman-name" type="text" class="form-control" placeholder="Full Name">
          </div>
          <div class="form-group">
            <input id="salesman-email" type="email" class="form-control" placeholder="Email">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save</button>
      </div>
    </div>
  </div>
</div>
<div class="modal fade event" id="event_popup_job_list" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg first-modal">
    <div class="modal-content">
    <div class="events_table_popup_wrap"></div>
    </div>
  </div>
</div>
<script>
    
 function loadEventModalPopup(jobid){
	jQuery.post("<?php echo admin_url("admin-ajax.php");?>",
		{
			action:'open_modal_on_job_list',
			jobid:jobid
		},
		function(resp){
			//alert(resp);
			jQuery('#event_popup_job_list').find(".events_table_popup_wrap").html(resp);
			jQuery('#event_popup_job_list').modal('show');
			return false;
		}
	);
}   
	jQuery("body").delegate("[class^=dashboard-event]","click",function(event){
		event.preventDefault();
		var userid = '<?php echo get_current_user_id()?>'; 
		var jobid = jQuery(this).data("jobid"); 
		jQuery.post("<?php echo admin_url("admin-ajax.php");?>",
			{
				action:'open_modal_on_job_list',
				jobid:jobid
			},
			function(resp){
				//alert(resp);
				if(userid==null || userid=='' || userid=='0' || userid==0){
					jQuery('#event_popup_job_list').find(".events_table_popup_wrap").html('<div class="modal-body">Your Outlook login has expired. Please sync your Outlook account from the settings page<button type="button" class="close" data-dismiss="modal" id="closesset">&times;</button></div>');
				}
				else{
				    jQuery('#event_popup_job_list').find(".events_table_popup_wrap").html(resp);
				}
				jQuery('#event_popup_job_list').modal('show');
				return false;
			}
		);
	});
  jQuery(document.body).on('click',".delete_event_popup",function(){
		eventid = jQuery(this).attr('eventid');
		
		var $modal = jQuery('#popup_event_popup');
		$modal.on('show.bs.modal', function(e) {
			  jQuery(this).find('#replace_content_popup').html('');
			  jQuery(this).find('#replace_content_popup').html('<div class="col-lg-12">Are you sure you want to delete? <input type="hidden" name="event_id_for_delete" value="'+eventid+'" /></div>');
			  jQuery(this).find('.modal-title').html('Delete Event');
			  jQuery(this).find('.modal-footer').html('<div class="col-lg-12"><input type="button" name="event_delete" value="Confirm" class="btn btn-md btn-primary pull-right" id="popup_event_save" /></div>');
		});
		$modal.modal('show');
		
		//alert(eventid);

	});

  jQuery(document.body).on('click',".edit_event_popup",function(){
	    eventid = jQuery(this).attr("eventid");
		//alert(eventid);
		//jQuery('#popup_event_popup').on('show.bs.modal', function(e) {
			//get_event_data_on_job_list
			
			jQuery.post("<?php echo admin_url("admin-ajax.php");?>",
				{
					action:'edit_event_item',
					event_id:eventid
				},
				function(resp){
					//alert(resp);
					
					var $modal = jQuery('#popup_event_popup');
					$modal.on('show.bs.modal', function(e) {
						  jQuery(this).find('#replace_content_popup').html('');
						  jQuery(this).find('#replace_content_popup').html(resp);
						  jQuery(this).find('.modal-title').html('Edit Event');
					});
					$modal.modal('show');
					
				}
			);
		//});
  });
  jQuery(document.body).on('click',"#add_event",function(e){
		jobid = jQuery(this).attr("jobid");
		jQuery('#popup_event_popup').on('show.bs.modal', function(e) {
			jQuery(this).find('#replace_content_popup').html('');
			jQuery(this).find('#replace_content_popup').html('<div class="form-group"><div class="col-md-6 col-sm-6"><label for="event_type">Event Type</label><select id="event_type" class="form-control" name="event_type"><option value="0">Appointment</option><option value="1">Call</option><option value="2">Notes</option><option value="3">Text Message</option><option value="4">Email</option></select></div><div class="col-md-6 col-sm-6 event_name"><label for="event_name">Event Name</label><input type="text" required="required" class="form-control" id="event_name" name="event_name"  /></div></div><div class="form-group"><div class="col-md-12 col-sm-12 event_start_time"><label for="event_start_time">Start Time</label><input type="text" class="form-control" id="event_start_time" placeholder="yyyy-mm-dd hh:mm:ss" name="event_start_time"  /></div></div><div class="form-group"><div class="col-md-12 col-sm-12 event_participants"><label for="event_participants">Participants</label><textarea id="event_participants" name="event_participants" class="form-control"></textarea></div></div><div class="form-group"><div class="col-md-12 col-sm-12"><label for="event_notes">Notes</label><textarea id="event_notes" required="required" name="event_notes" class="form-control"></textarea></div></div><input type="hidden" name="jobid" id="jobid" />');		
			  jQuery(this).find('.modal-title').html('Add Event');
			  jQuery(this).find('.modal-footer').html('<div class="col-lg-12"><input type="button" name="event_save" value="Save" class="btn btn-md btn-primary pull-right" id="popup_event_save" /></div>');
			jQuery(this).find('#jobid').val(jobid);
			jQuery(this).find('#popup_events_form_res').html('');
			
			jQuery.post("<?php echo admin_url("admin-ajax.php");?>",
				{
					action:'get_participants',
					job_id:jobid
				},
				function(resp){
					//alert(resp);
					jQuery('#popup_event_popup').find('#event_participants').val(resp);
				}
			);
			/*jQuery.post("<?php //echo admin_url("admin-ajax.php");?>",
				{
					action:'get_notes_content',
					job_id:jobid
				},
				function(resp){
					//alert(resp);
					jQuery('#popup_event_popup').find('#event_notes').val(resp);
				}
			);*/
			jQuery(this).find("#event_start_time").datetimepicker({
				dateFormat: "yy-mm-dd",
				timeFormat: "hh:mm tt",
				controlType: 'select',
				oneLine: true,
				/*onSelect: function( selectedDate ) {
					jQuery('#event_end_time').datetimepicker("option","minDate",selectedDate);
				}*/
			});
			/*
			jQuery(this).find("#event_end_time").datetimepicker({
				dateFormat: "yy-mm-dd",
				timeFormat: "HH:mm:ss",
				controlType: 'select',
				oneLine: true
			});*/
			/*jQuery(this).find("#event_name").val('');
			jQuery(this).find("#event_start_time").val('');
			jQuery(this).find("#event_end_time").val('');
			jQuery(this).find("#event_participants").val('');
			jQuery(this).find("#event_notes").val('');*/
		});
		e.preventDefault();
		/*
		jQuery('body').on('focus',"#event_start_time", function(){
    	    jQuery(this).datetimepicker({
					dateFormat: "yy-mm-dd",
					timeFormat: "HH:mm:ss",
					controlType: 'select',
					oneLine: true,
					onSelect: function( selectedDate ) {
						jQuery('#event_end_time').datetimepicker("option","minDate",selectedDate);
					}
				});
    	});
    
      jQuery('body').on('focus',"#event_end_time", function(){
    	    jQuery(this).datetimepicker({
					dateFormat: "yy-mm-dd",
					timeFormat: "HH:mm:ss",
					controlType: 'select',
					oneLine: true
				});
    	});*/
  });
	
  jQuery(document.body).on('change',"#event_type",function(){
	  event_type = jQuery(this).attr('value');
	  if(event_type==1 || event_type==2 || event_type==3 || event_type==4){
		  jQuery(".event_name").hide();
		  jQuery(".event_start_time").hide();
		  //jQuery(".event_end_time").hide();
		  jQuery("#event_start_time_edit").val('');
		  // jQuery("#event_end_time_edit").val('');
		  jQuery(".event_participants").hide();	  		    
		  jQuery("#event_notes").val('');	  
	  }
	  else{
		  jQuery(".event_name").show();
		  jQuery(".event_start_time").show();
		  //jQuery(".event_end_time").show();
		  jQuery("#event_start_time_edit").val('');
		  // jQuery("#event_end_time_edit").val('');
		  jQuery(".event_participants").show();	  
	  }
  });
  
  	jQuery(document.body).on('click',"#popup_event_save",function(){
		/* add case */
		if(jQuery('#event_start_time').length){
			if (jQuery("#event_start_time").is(':visible')) {
				event_start_time = jQuery.trim(jQuery('#popup_event_popup').find("#event_start_time").val());
				if(event_start_time==''){
					jQuery('#popup_event_popup').find("#event_start_time").focus();
					jQuery('#popup_event_popup').find("#event_start_time").css('border','1px solid #F00');
					return false;
				}
			}
		}
		/*if(jQuery('#event_end_time').length){
			if (jQuery("#event_end_time").is(':visible')) {	
				event_end_time = jQuery.trim(jQuery('#popup_event_popup').find("#event_end_time").val());
				if(event_end_time==''){
					jQuery('#popup_event_popup').find("#event_end_time").focus();
					jQuery('#popup_event_popup').find("#event_end_time").css('border','1px solid #F00');
					return false;
				}
			}
		}*/
		/* edit case */
		if(jQuery('#event_start_time_edit').length){
			if (jQuery("#event_start_time_edit").is(':visible')) {
				event_start_time_edit = jQuery.trim(jQuery('#popup_event_popup').find("#event_start_time_edit").val());
				if(event_start_time_edit=='' || event_start_time_edit=='0000-00-00 00:00:00'){
					jQuery('#popup_event_popup').find("#event_start_time_edit").focus();
					jQuery('#popup_event_popup').find("#event_start_time_edit").css('border','1px solid #F00');
					return false;
				}
			}
		}
		/*if(jQuery('#event_end_time_edit').length){
			if (jQuery("#event_end_time_edit").is(':visible')) {
				event_end_time_edit = jQuery.trim(jQuery('#popup_event_popup').find("#event_end_time_edit").val());
				if(event_end_time_edit=='' || event_end_time_edit=='0000-00-00 00:00:00'){
					jQuery('#popup_event_popup').find("#event_end_time_edit").focus();
					jQuery('#popup_event_popup').find("#event_end_time_edit").css('border','1px solid #F00');
					return false;
				}
			}
		}*/
		
		if(jQuery('#event_notes').length && jQuery('#event_type').val()!=0){
			event_notes_v = jQuery.trim(jQuery('#popup_event_popup').find("#event_notes").val());
			if(event_notes_v==''){
				jQuery('#popup_event_popup').find("#event_notes").focus();
				jQuery('#popup_event_popup').find("#event_notes").css('border','1px solid #F00');
				return false;
			}
		}
		    form_data = jQuery( "#popup_events_form" ).serialize();
			//alert(form_data);
			//return false;
			/*
			jQuery.post("<?php echo admin_url("admin-ajax.php");?>",
				{
					action:'save_modal_on_job_list',
					form_data:form_data
				},
					
				function(resp){
					//alert(resp);
					jQuery('#popup_event_popup').find("#popup_event_save").addClass('disabled');
					//jQuery('#popup_event_popup').find("#popup_event_save").parent().append('saving...');
					jQuery('#popup_events_form_res').addClass('green');
					jQuery('#popup_event_popup').find("#popup_events_form_res").html('');
					jQuery('#popup_event_popup').find("#popup_events_form_res").html(resp);
				    //jQuery('#popup_event_popup').find("#status_div").remove();
				    jQuery('#popup_event_popup').find("#popup_event_save").removeClass('disabled');
					setTimeout(function(){
					   
						jQuery('#popup_event_popup').find("#popup_events_form_res").html('');
						
						jQuery('#popup_events_form_res').fadeOut();
					}, 10000);
					jQuery('#popup_events_form_res').fadeIn();
					if(!jQuery('#popup_event_popup').find("input[name=event_edit_id]").length){
						jQuery('#popup_event_popup').find("#event_name").val('');
						jQuery('#popup_event_popup').find("#event_start_time").val('');
						//jQuery('#popup_event_popup').find("#event_end_time").val('');
						jQuery('#popup_event_popup').find("#event_participants").val('');
						jQuery('#popup_event_popup').find("#event_notes").val('');
					}
					//jQuery('#popup_event_popup').modal('show');
					return false;
				}
			);*/
			
			
			jQuery.ajax({
				url : "<?php echo admin_url("admin-ajax.php");?>",
				type : 'post',
				data : {
					action : 'save_modal_on_job_list',
					form_data : form_data
				},
				beforeSend : function() {
					jQuery('#popup_event_popup').find("#popup_event_save").addClass('disabled');
				},
				success : function( resp ) {
					jQuery('#popup_events_form_res').addClass('green');
					jQuery('#popup_event_popup').find("#popup_events_form_res").html('');
					jQuery('#popup_event_popup').find("#popup_events_form_res").html(resp); 
					//jQuery('#popup_events_form_res').removeClass('green');
					jQuery('#popup_event_popup').find("#popup_event_save").removeClass('disabled');
					setTimeout(function(){
						jQuery('#popup_events_form_res').fadeOut();
						jQuery('#popup_event_popup').find("#popup_events_form_res").html('');
					}, 1000);
					jQuery('#popup_events_form_res').fadeIn();
					if(!jQuery('#popup_event_popup').find("input[name=event_edit_id]").length){
						jQuery('#popup_event_popup').find("#event_name").val('');
						jQuery('#popup_event_popup').find("#event_start_time").val('');
						jQuery('#popup_event_popup').find("#event_end_time").val('');
						jQuery('#popup_event_popup').find("#event_participants").val('');
						jQuery('#popup_event_popup').find("#event_notes").val('');
					}
					setTimeout(function(){
						jQuery('#popup_event_popup').modal('hide');
					}, 3000);
					return false;
				}
			});
		 
	  //alert( "Modal submitted with text: " + form_data);
	});

</script>
<div id="popup_event_popup" class="modal fade sag-setting delete-box" role="dialog" aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
  <div class="modal-body">
    <form method="post" id="popup_events_form" name="events_form">	
      <div class="modal-header">
        <div class="col-lg-12">
            <button type="button" class="close" data-dismiss="modal" id="closesset">&times;</button>
        <h4 class="modal-title center">Add New Event</h4>
        </div>
      </div>
      <div class="modal-body">
          <div class="col-lg-12">
	    <div id="popup_events_form_res" class=" "></div>
	    </div>
      	<div id="replace_content_popup">
            <div id="dvMap1" style="">
              <div id="ResultShowss">
                <div class="form-group">
                    <div class="col-md-6 col-sm-6">
                      <label for="event_type">Event Type</label>
                      <select id="event_type" class="form-control" name="event_type">
                        <option value="0">Appointment</option>
                        <option value="1">Call</option>
                        <option value="2">Notes</option>
                        <option value="3">Text Message</option>
                        <option value="4">Email</option>
                      </select>
                    </div>
                    <div class="col-md-6 col-sm-6 event_name">
                      <label for="event_name">Event Name</label>
                      <input type="text" required="required" class="form-control" id="event_name" name="event_name"  />
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-6 col-sm-6 event_start_time">
                      <label for="event_start_time">Start Time</label>
                      <input type="text" class="form-control" id="event_start_time" placeholder="yyyy-mm-dd hh:mm:ss" name="event_start_time"  />
                    </div>
                    <!--<div class="col-md-6 col-sm-6 event_end_time">
                      <label for="event_end_time">End Time</label>
                      <input type="text" class="form-control" id="event_end_time" placeholder="yyyy-mm-dd hh:mm:ss" name="event_end_time"  />
                    </div>-->
                </div>
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 event_participants">
                      <label for="event_participants">Participants</label>
                      <textarea id="event_participants" name="event_participants" class="form-control"></textarea>
                    </div>
                </div>	
                <div class="form-group">
                    <div class="col-md-12 col-sm-12">
                      <label for="event_notes">Notes</label>
                      <textarea id="event_notes" required="required" name="event_notes" class="form-control"></textarea>
                    </div>
                </div>
              </div>
            </div>
 	  	<input type="hidden" name="jobid" id="jobid" />
        </div>
      </div>
      <div class="clearfix"></div>
      <div class="modal-footer">
        <input type="button" name="event_save" value="Save" class="btn btn-md btn-primary pull-right" id="popup_event_save" />
      </div>
    </form>
  </div>
</div>
</div>
<!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-ui-timepicker-addon/1.4.5/jquery-ui-timepicker-addon.js"></script>
--></div>
<?php 
        
    //}
  }

  public function shortcodes_init() {

  if(isset($_REQUEST['lxdata']))
  {
  	$this->get_lx_data();
  }
  if(isset($_REQUEST['notificationsend']))
  {

  			$this->sennotification_mail($_REQUEST['notificationsend']);



 }


	if(!get_option("acceptjobpage"))
		{

			$my_post = array(
			  'post_title'    => 'Accept Job',
			  'post_content'  => '[accept_job]',
			  'post_status'   => 'publish',
			  'post_type'   => 'page',

			);

// Insert the post into the database
			$pageid=wp_insert_post( $my_post );
			update_option("acceptjobpage",$pageid);

		}

    add_shortcode('sag-settings', array($this, 'sag_settings_form'));
    add_shortcode('sag-events', array($this, 'sag_events'));
    add_shortcode('submit-sag-job', array($this, 'job_form'));
    add_shortcode('sag_export_pdf', array($this, 'export_pdf'));
    add_shortcode('salbertglass_jobs_list', array($this, 'jobs_list'));

  	global $wpdb;
  	$table_sag_jobs  = $wpdb->prefix.'sag_jobs';
    $nwaluff_items   = $wpdb->prefix.'nwaluff_items';
    $nwaluft_items   = $wpdb->prefix.'nwaluft_items';
    $table_gdn_items = $wpdb->prefix.'gdn_items';
    $nwdr_items      = $wpdb->prefix.'nwdr_items';
    $gdn_items       = $wpdb->prefix.'gdn_items';
	  $hmnd_items        = $wpdb->prefix.'hmnd_items';
    $hmnf_items       = $wpdb->prefix.'hmnf_items';
	 $gdr_items       = $wpdb->prefix.'gdr_items';
	  $sfd_items       = $wpdb->prefix.'sfd_items';
	  	  $wmd_items       = $wpdb->prefix.'wmd_items';
		$gpd_items       = $wpdb->prefix.'gpd_items';



	$history=$wpdb->prefix . 'history';
	 $create= "create table if not exists $history (id int NOT NULL AUTO_INCREMENT , jobid varchar(255) not null,salesname varchar(255) not null , date DATETIME not null, PRIMARY KEY (id) )";
	$wpdb->query($create);


	$alter="ALTER TABLE $table_sag_jobs ADD COLUMN IF NOT EXISTS `parking` VARCHAR(50) NULL";
    $wpdb->query($alter);


	$alter="ALTER TABLE $gdr_items ADD COLUMN IF NOT EXISTS `location` VARCHAR(50) NULL";
    $wpdb->query($alter);

		$alter="ALTER TABLE $gdn_items ADD COLUMN IF NOT EXISTS `location` VARCHAR(50) NULL";
    $wpdb->query($alter);

	$alter="ALTER TABLE $sfd_items ADD COLUMN IF NOT EXISTS `location` VARCHAR(50) NULL";
    $wpdb->query($alter);

	$alter="ALTER TABLE $wmd_items ADD COLUMN IF NOT EXISTS `location` VARCHAR(50) NULL";
    $wpdb->query($alter);

	$alter="ALTER TABLE $table_sag_jobs ADD COLUMN IF NOT EXISTS `accepted` TINYINT(1) default 0";
    $wpdb->query($alter);

	$alter="ALTER TABLE $gpd_items ADD COLUMN IF NOT EXISTS `daylight_enable` int(100)";
    $wpdb->query($alter);

	$alter="ALTER TABLE $gpd_items ADD COLUMN IF NOT EXISTS `gosize_enable` int(100)";
    $wpdb->query($alter);

	$alter = "ALTER TABLE $table_sag_jobs CHANGE `labor_number_of_hours` `labor_number_of_hours` text";

    $wpdb->query($alter);


	$alter = "ALTER TABLE $table_sag_jobs CHANGE `labor_number_of_men` `labor_number_of_men` text";

    $wpdb->query($alter);

	 $alter = "ALTER TABLE $table_sag_jobs CHANGE `contact_number` `contact_number` VARCHAR(30)";

    $wpdb->query($alter);

	$alter = "ALTER TABLE $table_sag_jobs CHANGE `job_name` `job_name` LONGTEXT ";

    $wpdb->query($alter);


	$alter="ALTER TABLE $gpd_items ADD COLUMN IF NOT EXISTS `color_viewer` varchar(100)";
    $wpdb->query($alter);

	$alter="ALTER TABLE $gpd_items ADD COLUMN IF NOT EXISTS `damage_viewer` varchar(100)";
    $wpdb->query($alter);



//
//     $alter="ALTER TABLE $gdn_items add `handle_type` VARCHAR(50) NULL";
//     $wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `glass_width` `glass_width` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `glass_height` `glass_height` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `overall_door_height` `overall_door_height` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `top_shoe_size` `top_shoe_size` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `top_shoe_pocket` `top_shoe_pocket` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `bottom_shoe_size` `bottom_shoe_size` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `bottom_shoe_pocket` `bottom_shoe_pocket` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `door_opening` `door_opening` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `lock_notch_height` `lock_notch_height` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `lock_notch_width` `lock_notch_width` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
//
// 	$alter="ALTER TABLE $gdn_items CHANGE `side_rail_size` `side_rail_size` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
// 		$alter="ALTER TABLE $gdn_items CHANGE `side_rail_pocket` `side_rail_pocket` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
//
//
//
//   	$alter="ALTER TABLE $table_sag_jobs CHANGE `job_name` `job_name` VARCHAR(50) NULL";
//   	$wpdb->query($alter);
//
//   	$alter="ALTER TABLE $table_sag_jobs add `modifydate` datetime not NULL";
//   	$wpdb->query($alter);
//
//     $alter = "ALTER TABLE $table_sag_jobs add `quote_id` VARCHAR(50) NULL";
//     $wpdb->query($alter);
//
//
//
//   	$alter= "ALTER TABLE $nwaluft_items CHANGE `image_url` `pictures_download_url` TEXT";
//   	$wpdb->query($alter);
//
//
//   	$alter= "ALTER TABLE $nwaluft_items CHANGE `sketch_url` `sketches_download_url` TEXT";
//   	$wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $nwaluft_items CHANGE `opening_width` `opening_width` VARCHAR(100) NULL DEFAULT NULL";
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $nwaluft_items CHANGE `opening_height` `opening_height` VARCHAR(100) NULL DEFAULT NULL";
//     $wpdb->query($alter);
// 	$alter = "ALTER TABLE $nwaluft_items CHANGE `rought_opening_width` `rought_opening_width` VARCHAR(100) NULL DEFAULT NULL";
// 	$wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $nwaluft_items CHANGE `rought_opening_height` `rought_opening_height` VARCHAR(100) NULL DEFAULT NULL";
//     $wpdb->query($alter);
//
//
//
//
// $alter = "ALTER TABLE $nwaluff_items CHANGE `opening_width` `opening_width` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $nwaluff_items CHANGE `opening_height` `opening_height` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $nwaluff_items CHANGE `door_width` `door_width` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $nwaluff_items CHANGE `door_height` `door_height` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnd_items CHANGE `width_out_side` `width_out_side` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items CHANGE `door_height` `door_height` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items CHANGE `the_top_hinge1` `the_top_hinge1` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items CHANGE `the_top_hinge2` `the_top_hinge2` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items CHANGE `the_top_hinge3` `the_top_hinge3` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnd_items CHANGE `the_top_hinge4` `the_top_hinge4` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items CHANGE `line_of_lock` `line_of_lock` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnd_items CHANGE `lock_backset` `lock_backset` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnd_items add `center_deadbolt` VARCHAR(255) NULL";
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items add `dead_bolt` VARCHAR(255) NULL";
//     $wpdb->query($alter);
//
//
// 	$alter = "ALTER TABLE $hmnd_items add `latch_guard` VARCHAR(255) NULL";
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items add `door_sweep` VARCHAR(255) NULL";
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnd_items add `drip_cap` VARCHAR(255) NULL";
//     $wpdb->query($alter);
//
//
// 	$alter = "ALTER TABLE $hmnd_items add `door_viewer` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
//
// 		$alter= "ALTER TABLE $hmnd_items CHANGE `image_url` `pictures_download_url` TEXT ";
//   	$wpdb->query($alter);
//
//
//   	$alter= "ALTER TABLE $hmnd_items CHANGE `sketch_url` `sketches_download_url` TEXT";
//   	$wpdb->query($alter);

//
//
//
// 	$alter = "ALTER TABLE $hmnf_items add `dead_bolt` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnf_items add `latch_guard` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
// 		$alter = "ALTER TABLE $hmnf_items add `door_sweep` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnf_items add `drip_cap` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnf_items add `door_viewer` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
// 		$alter = "ALTER TABLE $hmnf_items add `frame_depth` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnf_items add `wall_type` VARCHAR(255) NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnf_items CHANGE `opening_width_bootms` `opening_width_bootms` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnf_items CHANGE `opening_width_middel` `opening_width_middel` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnf_items CHANGE `opening_width_top` `opening_width_top` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 	$alter = "ALTER TABLE $hmnf_items CHANGE `opening_height_left` `opening_height_left` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
//
// 	$alter = "ALTER TABLE $hmnf_items CHANGE `opening_height_right` `opening_height_right` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnf_items CHANGE `depth_or_thickness` `depth_or_thickness` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
//
// 		$alter = "ALTER TABLE $hmnf_items CHANGE `freme_width` `freme_width` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
// 		$alter = "ALTER TABLE $hmnf_items CHANGE `kick_plate` `kick_plate` VARCHAR(100) NULL DEFAULT NULL";
//
//     $wpdb->query($alter);
//
//
// 		$alter= "ALTER TABLE $hmnf_items CHANGE `






  }



}

$WP_SAG_Jobs = WP_SAG_Jobs::getInstance();
function timeAgo($time_ago){
  $time_ago =strtotime($time_ago);
  $cur_time   = time();
  $time_elapsed   = $cur_time - $time_ago;
  $seconds  = $time_elapsed ;
  $minutes  = round($time_elapsed / 60 );
  $hours    = round($time_elapsed / 3600);
  $days     = round($time_elapsed / 86400 );
  $weeks    = round($time_elapsed / 604800);
  $months   = round($time_elapsed / 2600640 );
  $years    = round($time_elapsed / 31207680 );

  if($seconds <= 60){
    return "$seconds seconds ago";
  }
  else if($minutes <=60){
    if($minutes==1){
      return "one minute ago";
    }
    else{
      return "$minutes minutes ago";
    }
  }
  else if($hours <=24){
    if($hours==1){
      return "an hour ago";
    }else{
      return "$hours hours ago";
    }
  }
  else if($days <= 7){
    if($days==1){
      return "yesterday";
    }else{
      return "$days days ago";
    }
  }
  else if($weeks <= 4.3){
    if($weeks==1){
      return "a week ago";
    }else{
      return "$weeks weeks ago";
    }
  }
  else if($months <=12){
    if($months==1){
      return "a month ago";
    }else{
      return "$months months ago";
    }
  }
  else{
    if($years==1){
      return "one year ago";
    }else{
      return "$years years ago";
    }
  }
}

add_action('admin_menu', 'custom_date_update_menu');
function custom_date_update_menu(){
	add_options_page('SAG Settings', 'SAG Settings', 'manage_options','date_update_define', 'custom_date_update_option');
}
function custom_date_update_option(){
   if ( !current_user_can( 'manage_options' ) )  {
	wp_die( 'You do not have sufficient permissions to access this page.' );
	}
	 if(isset($_POST['date_update'])){

		update_option('dateupdatecrone', $_POST['date_update_for_crone'], 'yes' );
		//update_option('dateupdateto', $_POST['date_update_for_to'], 'yes' );
	 }
	 if(isset($_POST['select_nmode']))
	 {

		update_option('select_nmode', $_POST['select_nmode']);
	 }

	?>
	<div class="wrap">
	  <h2>Custom Date Update Option</h2>
	  <form enctype="multipart/form-data" method="post" >
		<p> From
		  <input type="date" id="datepicker" name="date_update_for_crone"  class="newtag form-input-tipnewtag form-input-tip" value="<?php echo get_option('dateupdatecrone'); ?>"/>
		</p>
	
			<p> Select Mode : <br />
		  Development Mode : <input type="radio"  name="select_nmode"  class="newtag form-input-tipnewtag form-input-tip" value="1" <?php if(!get_option("select_nmode") || get_option("select_nmode")==1) { echo "checked=checked"; } else {echo " "; }?> />
		Live Mode : <input type="radio"  name="select_nmode"  class="newtag form-input-tipnewtag form-input-tip" value="2" <?php if(get_option("select_nmode")==2) { echo "checked=checked"; } else {echo " "; }?>/>
	
		</p>
	
	
		<input type="submit" name="date_update" value="update"  class="button button-primary button-large" />
	  </form>
	</div>
	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
	<link rel="stylesheet" href="/resources/demos/style.css">
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
	<script>
	   jQuery( function() {
		jQuery( "#datepicker" ).datepicker();
	} );
	</script>
	<?php
}
add_shortcode("accept_job","fun_accept_job");
function fun_accept_job(){
	if(isset($_REQUEST['job_id']))
	{
	?>
	<div style="text-align:center">
	<h1>You have successfully accepted this job</h1>

<a class="btn btn-danger btn-sm" href="<?php echo get_option('siteurl'); ?>/add-job?job_id=<?php echo $_REQUEST['job_id']; ?>">View Job</a>

	</div>
	<style type="text/css">

	#ktmain {
width: 100%;
	}


	</style>

	<?php
	}

}
add_action('the_content',"fun_contetnshow");

function fun_contetnshow($content)
{
	global $post;
	$postid=$post->ID;

		if($postid==get_option("acceptjobpage"))
			{

				if(isset($_REQUEST['job_id']))
					{
						$jobid=$_REQUEST['job_id'];
						global $wpdb;
						$tablest= $wpdb->prefix."sag_jobs";
						 $update="update $tablest set accepted='1' where id='$jobid'";
						$wpdb->query($update);

					}

			}

return $content;
}
// For CORS bbdelete tmp files
function delete_temp(){


	

	$data['data'] = $_POST; 

$upload_dir   = wp_upload_dir();



$data_array = explode('job_id=',$_POST['url']);






$dir = $upload_dir['basedir'].'/tmp/'.$data_array[1];

//print_r($dir);

//die();

unlinkr($dir);

echo $data_array[1];

wp_die();

}

function browse_s3(){
global $_REQUEST;

	//print_r($_REQUEST);
$imgcontant = file_get_contents($_REQUEST['imgUrl']);
echo $imgcontant;
$imgcontant='';
wp_die();

}


function unlinkr($dir, $pattern = "*") {
    // find all files and folders matching pattern
    $files = glob($dir . "/$pattern");
    //interate thorugh the files and folders
    foreach ($files as $file) {
        //if it is a directory then re-call unlinkr function to delete files inside this directory
        if (is_dir($file) and ! in_array($file, array('..', '.'))) {
            unlinkr($file, $pattern);
            //remove the directory itself
            rmdir($file);
        } else if (is_file($file) and ( $file != __FILE__)) {
            // make sure you don't delete the current script
            unlink($file);
        }
    }
    rmdir($dir);
}


function sendmailmode($emailid,$subject,$message,$headers='')
{
 
	if(empty($emailid)){ return false;}

	$eol = PHP_EOL;
	
	//if(get_option("select_nmode")==2){	$frmMail ='noreply@salbertglass.com';	}else{	$frmMail ='development@salbertglass.com';}
	$frmMail =EMAILFROM;
	if(empty($headers)){
		$headers = "From: S. Albert Glass <".EMAILFROM.">" . "\r\n" .	
		"CC: appmanager@salbertglass.com,ajay@imajine.us,alex@imajine.us";

	}

if(get_option("select_nmode")==2)
	{
		//	$subject="Development - ".$subject ; // need to do comment at the timie of live 
	 	// wp_mail($emailid,$subject, $message,$headers);
	
	}
	else
	{
	 
		$subject="Development - ".$subject ;
		 //  $headers = "From: S. Albert Glass <development@salbertglass.com>"  ."\n\n"  ;
		//wp_mail('steven@salbertglass.com',$subject, $message,'From: S. Albert Glass <development@salbertglass.com>');
	//		wp_mail('@gmail.com',$subject, $message,$headers);
	//	wp_mail('sheettesting1234@gmail.com',$subject, $message,'From: S. Albert Glass <development@salbertglass.com>');
	//	wp_mail($emailid,$subject, $message,$headers);

		/*dev_mail_fun*/
		//wp_mail('ajay@imajine.us',$subject, $message,$headers);
	


	//	wp_mail('alex@imajine.us',$subject, $message,$headers);
	}



}

include 'office.php';

/*

add_action('init','check_cron_job');

function check_cron_job(){

    //sleep(5);
    $strLxDate         = get_option( 'lx_last_update'); //this must exist
    $lxDate = date( "Y-m-d",strtotime("-4 hours"));
     update_option('lx_last_update', $lxDate);
    // echo LX_QUOTES_URL . str_replace ( ' ' , '%20' , $strLxDate ) . '&token=' . LX_TOKEN ;

    $lxQuotesURL       = LX_QUOTES_URL . str_replace ( ' ' , '%20' , '2019-04-16' ) . '&token=' . LX_TOKEN ;
    	
    	 print_r($lxQuotesURL);
 

    $lxQuoteDataURL    = LX_QUOTE_DATA_URL . LX_TOKEN;
    $quotesResponse    = wp_remote_get( $lxQuotesURL );


   echo "<pre>";
      print_r(json_decode( $quotesResponse["body"] ));

   	die('okkk');
   
     $result_lx = array();
    
    foreach( json_decode( $quotesResponse["body"] ) as $quoteId ){ //iterate all quotes
    $data_lx= "https://lx.salbertglass.com/GLASPACAPI/WorkOrder/Get/".$quoteId."/?token=".LX_TOKEN."";
    $home_a = file_get_contents($data_lx);

    $result_lx[] = json_decode($home_a);


   	
    }
      echo "<pre>";
      print_r($quotesResponse);

   	die('okkk');
		
		
    
  }*/
 
    function update_log_for_pdf(){


  	 $job_data = PHP_EOL.'  quote number :' . $_POST['quote_number'] . '  Date&Time ' . date("d/m/Y h:i:sa").' Lcation :  '.$_POST['msg'];
   
    
    $pluginlog = plugin_dir_path(__FILE__).'pdf_log.log';
 
    error_log($job_data, 3, $pluginlog);


    echo $_POST['msg'];
    die();

  }


 function check_session_variable(){


  	echo $_SESSION['job_name_com'];
    die();

  }

  
    function get_qoute_number(){

global $wpdb;
$fivesdrafts = $wpdb->get_results( 
	"
	SELECT * 
	FROM wp_4twsj5a02k_sag_jobs
	WHERE id = ".$_REQUEST['job_id']
);
  	
  	print_r($fivesdrafts[0]->quote_number);
  	
  	
  	
    die();

  }



  