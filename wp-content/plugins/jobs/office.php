<?php ob_start();
include 'outlook/vendor/autoload.php';

    add_shortcode('sag-outlook-process', 'sag_outlook_process');
	function sag_outlook_process(){
		echo '<div align="center"><img class="wait_img" src="'.plugins_url('/img/wait.gif', __FILE__).'"></div>';
		$url = getOutlookLoginUrl();
		if (!filter_var($url, FILTER_VALIDATE_URL) === FALSE) {
			echo '<script> window.location.href = "'.$url.'" </script>';
			die;
		}
		##var_dump(!filter_var($url, FILTER_VALIDATE_URL) === FALSE);
		#echo $url;
	}
	
	function storeTokens($access_token, $refresh_token, $expires) {
		$_SESSION['access_token'] = $access_token;
		$_SESSION['refresh_token'] = $refresh_token;
		$_SESSION['token_expires'] = $expires;
	}

	function clearTokens() {
		unset($_SESSION['access_token']);
		unset($_SESSION['refresh_token']);
		unset($_SESSION['token_expires']);
	}
	function getCurlResponse($URL, $CURLOPT_POSTFIELDS){
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $URL);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $CURLOPT_POSTFIELDS);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		return $server_output;
	}
	 
	function getAccessToken() {
		global $wpdb;
		$user_id = get_current_user_id();
		
		// Check if tokens exist
		if (empty($_SESSION['access_token']) ||
			empty($_SESSION['refresh_token']) ||
			empty($_SESSION['token_expires'])) {
		  return '';
		}
		
		$t = $wpdb->prefix."sag_refresh_tokens";
		$refreshRow = $wpdb->get_row( "SELECT * FROM $t WHERE user_id ='".$user_id."'" );
		$refresh_token = $refreshRow->refresh_token;

		// Check if token is expired
		//Get current time + 5 minutes (to allow for time differences)
		$now = time() + 300;
		if ($_SESSION['token_expires'] <= $now) {
		  // Token is expired (or very close to it)
		  // so let's refresh
		  
			//$refresh_token = $_SESSION['refresh_token']

			$URL = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
			$CURLOPT_POSTFIELDS = 'client_id='.APP_ID.'
									&client_secret='.APP_PASSWORD.'
									&grant_type=refresh_token						
									&refresh_token='.$refresh_token.'
									&scope=https%3A%2F%2Foutlook.office.com%2Fcalendars.readwrite+openid+offline_access';
			$server_output = getCurlResponse($URL, $CURLOPT_POSTFIELDS);
			$result = json_decode($server_output);
			storeTokens($result->access_token, $result->refresh_token, $result->expires_in);
			return $result->access_token;
		}
		else {
		  // Token is still valid, just return it
		  return $_SESSION['access_token'];
		}
	  }
	
	function getOutlookLoginUrl(){
		global $wpdb;
		$user_id = get_current_user_id();
		
		$redirectUri =  site_url().'/sync-outlook/';			 
		
		if(isset($_REQUEST['code'])){
			$code = $_REQUEST['code'];
			$URL = "https://login.microsoftonline.com/common/oauth2/v2.0/token";
			$CURLOPT_POSTFIELDS = 'code='.$code.'&redirect_uri='.$redirectUri.'&client_id='.APP_ID.'&client_secret='.APP_PASSWORD.'&scope=https%3A%2F%2Foutlook.office.com%2Fcalendars.readwrite+openid+offline_access&grant_type=authorization_code';
			$server_output = getCurlResponse($URL, $CURLOPT_POSTFIELDS);
			$token_res = json_decode($server_output);
			if(!$token_res->error){
				//save tokens into session
				storeTokens($token_res->access_token, $token_res->refresh_token, $token_res->expires_in);
				
				//save refresh token into database
				$table_refresh_tokens= $wpdb->prefix."sag_refresh_tokens";
		
				$getRow = $wpdb->get_row( "SELECT * FROM $table_refresh_tokens WHERE user_id ='".$user_id."'" );
				if(!empty($getRow)){
					$wpdb->update( 
						$table_refresh_tokens, 
						array( 
							'refresh_token' => $token_res->refresh_token,
							'token_expires' => $token_res->expires_in,
							'modified' => date('Y-m-d H:i:s')
						), 
						array( 'user_id' => $user_id )
					);        
				}
				else{
					$wpdb->insert( 
								$table_refresh_tokens, 
								array(
									'user_id' => $user_id, 
									'refresh_token' => $token_res->refresh_token,
									'token_expires' => $token_res->expires_in
								) 
							);
				}
				
				
			}
		}
		
		if (empty($_SESSION['access_token']) ||
			empty($_SESSION['refresh_token']) ||
			empty($_SESSION['token_expires'])) {

			$url = "https://login.microsoftonline.com/common/oauth2/v2.0/authorize";
			$postfields = "?client_id=".APP_ID."&client_secret=".APP_PASSWORD."&response_type=code&redirect_uri=$redirectUri&scope=https%3A%2F%2Foutlook.office.com%2Fcalendars.readwrite+openid+offline_access&response_mode=query&state=678910&domain_hint=organizations";
			return $url.$postfields;
		}
		else {
			$token = getAccessToken();
		}
		
		
		 
			if (!$token) {
				return $url.$postfields;;
			} else {
				$result = createEventsOnOutlook($token);
		// echo '<pre>';
		// print_r($_REQUEST);
		// print_r($result);
		// print_r($_SESSION);
		// echo '</pre>';
		// die;
		 			

			#$sessionManager->remove();
				//return $result;
				if($result == true){
					$msg = 'Events have been successfully created!';
				}else{
					$msg = 'There have no event to create, please try again!';
				}
				echo '<div align="center">'.$msg.'</div><style>.wait_img{display:none;}</style>';
				sleep(10);
				echo '<script> window.location.href = "'.site_url()."/sag-settings/".'" </script>';
				die;
				#$sessionManager->remove();
			}
		//}
		exit;
	}

function getParticipantsArr($participants){
	$participants = explode(',',$participants);
	$a = array();
	foreach($participants as $key => $participant){
	    $earray = array("EmailAddress" => array("Address" => $participant, "Name" => "test"), "Type" => "Required");
		array_push($a,$earray);
	}
	return $a;

}
function getValidDateTime($datetime){
	if($datetime == "0000-00-00 00:00:00" || $datetime == ""){
		$datetime = date('Y-m-d\TH:i:s');
	}
	//if(DateTime::createFromFormat('Y-m-d', $datetime) === false){
		//$datetime = date('Y-m-d\TH:i:s');
	//}
	return $datetime;
}

function getNotesContent($job_id,$notes){
	global $wpdb;
	$current_user = wp_get_current_user();
	$cuser = $current_user->user_login;
	$value=$wpdb->get_row("SELECT * FROM ".$wpdb->prefix."sag_jobs where id ='".$job_id."'");
	$content .= get_option("siteurl")."/add-job/?job_id=".$value->id."<div>&nbsp;</div>\n\n";
	$content .= "<div>Job Description : ".$value->job_name."</div><div>&nbsp;</div>\n\n";
	$content .= "<div>Quote Number : ".$value->quote_number."</div><div>&nbsp;</div>\n\n";
	$content .= "<div>Notes : ".$notes." By ".$cuser."</div><div>&nbsp;</div>\n\n";
	$content .= "<div>Created By : ".$cuser."</div>\n";
	$content .= "<div>Sales Rep A : ".$value->owner_name."</div>\n";
	//$content .= "<div>Logged In User : ".$value->owner_name."</div>\n";
	$content .= "<div>Sales Rep B : ".$value->salesman_name."</div>\n";
	$content .= "<div>CSR : ".$value->csr."</div><div>&nbsp;</div>\n\n";
	$content .= "<div>Project Address :" ."</div>\n";
	$content .= "<div>".$value->project_address_line1 ."</div>\n";
	$content .= "<div>".$value->project_address_line2 ."</div>\n";
	$content .= "<div>".$value->project_city.",".  $value->project_state." - ".$value->project_postal_code."</div><div>&nbsp;</div>\n\n";
	$content .= "<div>Project Contact :"."</div>\n";
	$content .= "<div>".$value->onsite_contact."</div>\n";
	$content .= "<div>".$value->contact_number."</div>\n";
	$content .= "<div>".$value->contact_email."</div>\n";
	return $content;
}
/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	function createEventsOnOutlook($token = null) {
		global $wpdb;
		$user_id = get_current_user_id();
		$table= $wpdb->prefix."sag_events";
		
		//$eventManager = new \Outlook\Events\EventManager($token);

		// get all events returns each item as Event object
		//$events = $eventManager->all();

		//create event
		// nested key name must be case sensitive correctly according to their docs.
		// only outer properties will be converted to Study case automatically
		
		$resultArr = $wpdb->get_results( "SELECT * FROM $table WHERE user_id = '$user_id' AND type='0'" );
		if(!empty($resultArr)){
			$event_create = 0;
			foreach($resultArr as $result){
				date_default_timezone_set('America/New_York');
				$job = $wpdb->get_row("SELECT * FROM ".$wpdb->prefix."sag_jobs where id ='".$result->job_id."'" ,ARRAY_A);
				
				$stdate = getValidDateTime(date('Y-m-d\TH:i:s',strtotime($result->start_time)));
				$eddate = getValidDateTime(date('Y-m-d\TH:i:s',strtotime($result->end_time)));
				$subject = $job['quote_number'].' '.$result->name;
				//$notes = $result->notes;
				$notes = getNotesContent($result->job_id,$result->notes);
				$tz = date_default_timezone_get();
				$data = array(
							"Subject"=>"$subject",
							"Body" => array("ContentType" => "HTML","Content" => "$notes"),
							"Start" => array("DateTime" => "$stdate","TimeZone" => $tz),
							"End" => array("DateTime" => "$eddate","TimeZone" => $tz)
						);
				if(!empty($result->participants) && $result->participants!=''){
					$a = getParticipantsArr($result->participants);
					$data["Attendees"] = $a;
				}
				
				$data_string = json_encode($data);
				
				
				//$event = ($flag) ? $eventManager->create($event) : $eventManager->update($event);
				 
				if(!empty($result->sync_id) && $result->sync_id!=''){
					//delete the previous event
					$ch = curl_init('https://outlook.office.com/api/v2.0/me/events/'.$result->sync_id);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
					//curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
					curl_setopt($ch, CURLOPT_HTTPHEADER, 
						array(
							'Authorization:Bearer '.$token,
							'Content-Type: application/json'
						)
					);
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
					$event_delete = curl_exec($ch);
					curl_close ($ch);
				}
				 
				
				//create event
				$ch = curl_init('https://outlook.office.com/api/v2.0/me/events');
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
				curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);        
				curl_setopt($ch, CURLOPT_HTTPHEADER, 
					array(
						'Authorization:Bearer '.$token,
						'Content-Type: application/json'
					)
				);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);				
				$event_curl = curl_exec($ch);
				
				
				$event_res = json_decode($event_curl);
				
				$event_create = 1;
				
				$wpdb->update($table, array('sync_id' => $event_res->Id ), array('id' => $result->id ));
				
				// echo '<pre>';
				// print_r($event_curl);
				// print_r($event_res);
				// echo '</pre>';
				curl_close ($ch);
			}
			//die;
			if($event_create == 0){
				return false;
			}else{
				return true;				
			}
		}else{
			return false;
		}
	}
#admin_index();