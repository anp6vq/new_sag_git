<section id="topbar" class="topclass">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 kad-topbar-left">
          <div class="address-holder">
            <h5 class="no-margin" style="color:white; text-align:right;"><img class="contact-header-icon" style="display:inline; position:relative;top:-2px;" src="<?php echo get_site_url(); ?>/wp-content/uploads/Marker-Filled-100.png" >6600 Ammendale Road Beltsville, MD 20705 </h5>

          </div>
          <div class="topbarmenu clearfix">
          <?php if (has_nav_menu('topbar_navigation')) :
              wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu'));
            endif;?>
            <?php if(kadence_display_topbar_icons()) : ?>
            <div class="topbar_social">
              <!-- <h5 class="no-margin">Connect With Us</h5> -->
              <ul>
                <?php global $virtue_premium; $top_icons = $virtue_premium['topbar_icon_menu'];
                $i = 1;
                foreach ($top_icons as $top_icon) {
                  if(!empty($top_icon['target']) && $top_icon['target'] == 1) {$target = '_blank';} else {$target = '_self';}
                  echo '<li><a href="'.$top_icon['link'].'" data-toggle="tooltip" data-placement="bottom" target="'.$target.'" class="topbar-icon-'.$i.'" data-original-title="'.esc_attr($top_icon['title']).'">';
                  if($top_icon['url'] != '') echo '<img src="'.esc_url($top_icon['url']).'"/>' ; else echo '<i class="'.$top_icon['icon_o'].'"></i>';
                  echo '</a></li>';
                $i ++;
                } ?>
              </ul>
            </div>
          <?php endif; ?>
            <?php global $virtue_premium; if(isset($virtue_premium['show_cartcount'])) {
               if($virtue_premium['show_cartcount'] == '1') {
                if (class_exists('woocommerce')) {
                 global $virtue_premium, $woocommerce;
                  ?>
                    <ul class="kad-cart-total">
                      <li>
                      <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php esc_attr_e('View your shopping cart', 'virtue'); ?>">
                          <i class="icon-basket" style="padding-right:5px;"></i> <?php if(!empty($virtue_premium['cart_placeholder_text'])) {echo $virtue_premium['cart_placeholder_text'];} else {echo __('Your Cart', 'virtue');}  ?> <span class="kad-cart-dash">-</span> <?php echo $woocommerce->cart->get_cart_total(); ?>
                      </a>
                    </li>
                  </ul>
                <?php } } }?>
          </div>
        </div><!-- close col-md-6 -->
        <div class="col-md-6 col-sm-6 kad-topbar-right">
          <div id="topbar-search" class="topbar-widget">
            <?php if(kadence_display_topbar_widget()) { if(is_active_sidebar('topbarright')) { dynamic_sidebar('topbarright'); }
              } else { if(kadence_display_top_search()) {get_search_form();}
          } ?>
        </div>
        </div> <!-- close col-md-6-->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </section>
