<?php get_header(); ?>
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <div id="content" class="container">
        <div class="row single-article">
            <div class="col-sm-9">
                <header>
                            <h1 class="entry-title title_h1"><?php the_title(); ?></h1>
                </header>
                 <div class="row">
                    <div class="col-sm-4">
                        <div class="views-user-data">
                            <div class="views-field editor-date">
                                <span class="views-label">Date: </span>
                                <span class="field-content"><?php echo get_the_date(); ?></span>
                            </div>
                            <div class="views-field news-source">
                                <span class="views-label">Source: </span>
                                <span class="field-content"><a target="_blank" href=""><?php echo get_the_author(); ?></a></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <?php if ( has_post_thumbnail() ):?>
                            <div class="featured-image-wrap"><?php the_post_thumbnail(); ?></div>
                        <?php endif; ?>
                        <div class="entry-content clearfix text-formatted">
                          <?php the_content(); ?>
                        </div>
                        <div class="tags">
                            <h4>
                                <span>See more news about:</span>
                            </h4>
                            <div class="views-field views-field-field-tags">
                                <div class="field-content-tag">
                                    <?php
                                        $posttags = get_the_tags();
                                        if ($posttags) {
                                          foreach($posttags as $tag) {
                                            echo '<span>' .$tag->name . '</span>'; 
                                          }
                                        }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
            </div>
            <div class="col-sm-3">
            </div>
        </div>
    </div>
    <style>
ol, ul {
    list-style: outside none none;
}
h1.title_h1{
    color: rgb(14, 55, 106);
    font-size: 33px;
    font-weight: bold;
}
.views-field {
  font-size: 16px;
  margin-bottom: 10px;
  text-transform: capitalize;
}
.tags h4 {
    font-size:16px;
    font-weight:bold;
    margin:0px;
    text-transform:uppercase;
}
.views-label {
    display: inline-block;
    min-width: 60px;
}
.views-user-data{
    -moz-border-bottom-colors: none;
    -moz-border-left-colors: none;
    -moz-border-right-colors: none;
    -moz-border-top-colors: none;
    border-color: rgb(229, 232, 235) rgb(229, 232, 235) rgb(222, 225, 226);
    border-image: none;
    border-style: solid;
    border-width: 1px 1px 3px;
    padding: 13px 15px;
}
.text-formatted{
    font-size:16px;
    line-height:24px;
    margin-top:20px; 
}
.text-formatted p {
    margin: 0 0 20px;
}
.text-formatted ul{
    list-style-type: disc;
    list-style-position: inside;
}
.field-content-tag span {
    background-color: rgb(20, 80, 153);
    color: rgb(255, 255, 255);
    display: block;
    float: left;
    font-size: 1.2rem;
    font-weight: 700;
    height: 23px;
    line-height: 17px;
    margin: 0 5px 5px 23px;
    padding: 4px 10px 6px 4px;
    position: relative;
    text-transform: uppercase;
}
.field-content-tag span::before {
    background-image: url("https://salbertglass.com/wp-content/uploads/tag.png");
    background-repeat: no-repeat;
    content: "";
    display: block;
    height: 23px;
    left: -23px;
    position: absolute;
    top: 0;
    width: 23px;
}
    </style>
<?php endwhile; ?>
<?php endif; ?>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>