<?php
/*
   Template Name: Page - SAG Jobs List
*/
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
         <meta name="format-detection" content="telephone=no">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>SAG Jobs List</title>
      <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
         <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
      <![endif]-->
   </head>
   <body>
      <div class='container'>
         <div id="form-header" class="row">
            <div id="logo-wrapper" class="col-md-2 col-md-offset-1 col-sm-12 col-sm-offset-0">
               <img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/albertGlass.png">
            </div>
            <div class="col-md-10"><h2>SAG Jobs List</h2></div>
         </div>
         <?php
            global $wpdb;
            $sql = "SELECT * FROM " . $wpdb->sag_jobs . " ORDER BY created_date DESC";
            /* $sql = "SELECT * FROM sag_jobs ORDER BY created_date DESC"; */
            $sag_jobs = $wpdb->get_results( $sql );
            if ( $wpdb->num_rows > 0 ) { ?>
               <div class="row col-md-12 col-sm-12">
                  <div class="table-responsive">
                     <table class="table table-striped table-hover">
                        <thead>
                           <tr>
                              <th>#</th>
                              <th>Salesman Name</th>
                              <th>Quote Number</th>
                              <th>Job Name</th>
                              <th></th>
                              <th></th>
                              <th></th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php $i = 1; ?>
                           <?php foreach ( $sag_jobs as $sag_job ) { ?>
                              <tr>
                                 <td><?php echo $i; ?></td>
                                 <td><?php echo $sag_job->salesman_name; ?></td>
                                 <td><?php echo $sag_job->quote_number; ?></td>
                                 <td><?php echo $sag_job->job_name; ?></td>
                                 <td><a class="btn btn-primary" href="#"><span class="glyphicon glyphicon-pencil" aria-hidden="true"></span></a></td>
                                 <td><a class="btn btn-primary" href="#"><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></a></td>
                                 <td><a class="btn btn-primary" href="#">Glasses <span class="badge">4</span></a></td>
                                 <td><a class="btn btn-primary" href="#">Doors <span class="badge">0</span></a></td>
                              </tr>
                              <?php $i = $i + 1; ?>
                           <?php } ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            <?php } 
         ?>
      </div>
   </body>
</html>