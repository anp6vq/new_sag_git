<head>
  <title>S. Albert Glass Jobs</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
</head>

<body>
  <div id="main-container" class="container">
    <div id="form-header" class="row">
      <div id="logo-wrapper" class="col-md-2 col-md-offset-1 col-sm-12 col-sm-offset-0">
        <img id="logo" src="img/albertGlass.png">
      </div>
