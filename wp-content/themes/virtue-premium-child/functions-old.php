<?php

function my_login_logo() { ?>
    <style type="text/css">
#login h1 a, .login h1 a {
 background-image: url(<?php echo get_stylesheet_directory_uri();
?>/img/sag_logo.jpg);
	width: 30%;
	background-size: 100%;
	background-repeat: no-repeat;
	padding-bottom: 30px;
}
</style>
    <?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );
function my_login_redirect( $redirect_to, $request, $user ) {
	//is there a user to check?
	if ( isset( $user->roles ) && is_array( $user->roles ) ) {
		//check for admins
		if ( in_array( 'administrator', $user->roles ) ) {
			// redirect them to the default place
			return esc_url( get_permalink( get_page_by_title( 'Current Jobs' ) ) );
		} else {
			return esc_url( get_permalink( get_page_by_title( 'Current Jobs' ) ) );
		}
	} else {
		return $redirect_to;
	}
}

add_filter( 'login_redirect', 'my_login_redirect', 10, 3 );

/**code by vivek

*
**
add new custom field ***/

function custom_user_profile_fields($user){
?>

<table class="form-table">
  <tr>
    <th><label>Phone Number</label></th>
    <td><input type="text" class="regular-text" name="phone" value="<?php echo esc_attr( get_the_author_meta( 'phone', $user->ID ) ); ?>" id="phone" /></td>
  </tr>
</table>
<?php

}

add_action( 'show_user_profile', 'custom_user_profile_fields' );

add_action( 'edit_user_profile', 'custom_user_profile_fields' );

add_action( "user_new_form", "custom_user_profile_fields" );

function save_custom_user_profile_fields($user_id){
	$user=get_userdata( $user_id );
	if(!current_user_can('manage_options'))

        return false;
 // send mail to user
   		$user_login = stripslashes($user->user_login);
   		$user_pass = stripslashes($user->user_pass);
        $user_email = stripslashes($user->user_email);
		$message  = __('Hi there,') . "\r\n\r\n";
        $message .= sprintf(__("Welcome to %s! Here's how to log in:"), get_option('blogname')) . "\r\n\r\n";
      	$message .= sprintf(__('Email: %s'), $user_email) . "\r\n";
        $message .= sprintf(__('Password: %s'), $user_pass) . "\r\n\r\n";
        $message .= sprintf(__('If you have any problems, please contact me at %s.'), get_option('admin_email')) . "\r\n\r\n";
        $message .= __('Thanks!');
     	wp_mail($user_email, sprintf(__('[%s] Your username and password'), get_option('blogname')), $message);
     	// save my custom field
      	update_usermeta($user_id, 'phone', $_POST['phone']);
      	/*$getauther = get_the_author_meta( 'phone', $user->ID);
      	require_once '/path/to/vendor/autoload.php';
      	use Twilio\Rest\Client;
      	$sid = "AC46f6bcb1434c0d4558aa66d349fc1bb3";
      	$token = "d6969f05ec7136d586b2ed367cc2b9de";
		$client = new Client($sid, $token);
		 $people = array(
        "+" => "Curious George",

        "+" => "vivek"
    );*/    }
add_action('user_register', 'save_custom_user_profile_fields');
add_filter('authenticate', function($user, $email, $password){

    //Check for empty fields
        if(empty($email) || empty ($password)){
            //create new error object and add errors to it.
            $error = new WP_Error();

            if(empty($email)){ //No email
                $error->add('empty_username', __('<strong>ERROR</strong>: Email field is empty.'));
            }
            else if(!filter_var($email, FILTER_VALIDATE_EMAIL)){ //Invalid Email
                $error->add('invalid_username', __('<strong>ERROR</strong>: Email is invalid.'));
            }

            if(empty($password)){ //No password
                $error->add('empty_password', __('<strong>ERROR</strong>: Password field is empty.'));
            }

            return $error;
        }

        //Check if user exists in WordPress database
        $user = get_user_by('email', $email);

        //bad email
        if(!$user){
            $error = new WP_Error();
            $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
            return $error;
        }
        else{ //check password
            if(!wp_check_password($password, $user->user_pass, $user->ID)){ //bad password
                $error = new WP_Error();
                $error->add('invalid', __('<strong>ERROR</strong>: Either the email or password you entered is invalid.'));
                return $error;
            }else{
                return $user; //passed
            }
        }
}, 20, 3);
function login_function() {
    add_filter( 'gettext', 'username_change', 20, 3 );
    function username_change( $translated_text, $text, $domain )
    {
        if ($text === 'Username')
        {
            $translated_text = 'Email';
        }
        return $translated_text;
    }
}
add_action( 'login_head', 'login_function' );

add_action('wp_ajax_add_checkin','add_job_checkin' );
add_action('wp_ajax_nopriv_add_checkin', 'add_job_checkin');

function add_job_checkin()
{
	global $wpdb;
	$zipcode = $_POST['zip_code'];
	$url = "http://maps.googleapis.com/maps/api/geocode/json?address=".$zipcode."&sensor=false";
	$details = file_get_contents($url);
	$result = json_decode($details,true);
	$lat =$result['results'][0]['geometry']['location']['lat'];
	$lng =$result['results'][0]['geometry']['location']['lng'];
	$rad = 10; //0.402336; //Redius in Kilometer (1/4 miles = 0.402336 km)
		$data = $wpdb->get_results("SELECT *, (3959 * acos(cos(radians('".$lat."')) * cos(radians(latitude)) * cos( radians(longitude) - radians('".$lng."')) + sin(radians('".$lat."')) * sin(radians(latitude))))AS distance FROM wp_4twsj5a02k_sag_jobs HAVING distance < '".$rad."' ORDER BY distance LIMIT 0 , 10");

		$arr = '';
		foreach($data as $k1 => $v1){
			$arr[$k1][0] = $v1->job_name;
			$arr[$k1][1] = $v1->latitude;
			$arr[$k1][2] = $v1->longitude;
			$arr[$k1][3] = $v1->salesman_name;
		}
	echo json_encode($arr);
	exit();
}
add_action('wp_ajax_add_distance','add_distance' );
add_action('wp_ajax_nopriv_add_distance', 'add_distance');

	function add_distance()
	{
		global $wpdb;
	    $result = false;
		if(is_user_logged_in())
		{
		  $current_user = wp_get_current_user();
	      $current_id = $current_user->ID;
          $current_name = $current_user->display_name;
	      $distnce = $_POST['distance'];
		  //$sql = "insert into wp_4twsj5a02k_checkIn (user_id,user_name,distance) VALUES ('".$current_id."','".$current_name."','".$distnce."')";

		   $wpdb->insert( $wpdb->prefix.'checkIn', array( 'user_id' => $current_id, 'user_name' => $current_name,  'distance' => $distnce));
			//$wpdb->insert($sql);

		  $result = true;

		}
		 die($result);
	}
	add_action('wp_ajax_show_list','show_list' );
    add_action('wp_ajax_nopriv_show_list', 'show_list');

	function show_list()
	{
		global $wpdb;
	    $result = "No Checkins Found";
		if(is_user_logged_in())
		{

			$myrows = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'checkIn');
		    if(!empty($myrows))
		    {
				$result = '';
				$result .= "<table class='table table-hover'><tr><th>Salesman Name</th><th>CheckIn Time</th><th>Distance</th></tr>";
				 foreach($myrows as $key => $val) {
					  $result .= "<tr><td>".$val->user_name."</td><td>".$val->created."</td><td>".$val->distance."</td></tr>";
					 }
				 $result .= "</table>";
			}
		}
		 echo $result; die;
	}	add_action('wp_ajax_salesmanlist','salesmanlist' );
    add_action('wp_ajax_nopriv_salesmanlist', 'salesmanlist');

	function salesmanlist()
	{
		global $wpdb;
	    $result = "No Salesman Found";
		$job_id=$_POST['job_id'];		if(is_user_logged_in())
		{

			$myrows = $wpdb->get_results( 'SELECT * FROM '.$wpdb->prefix.'history where jobid='.$job_id.' order by date asc');
		    if(!empty($myrows))
		    {
				$result = '';
				$result .= "<table class='table table-hover'><tr><th>Salesman Name</th><th>Date</th></tr>";
				 foreach($myrows as $key => $val) {
					  $result .= "<tr><td>".$val->	salesname."</td><td>".$val->date."</td></tr>";
					 }
				 $result .= "</table>";
			}
		}
		 echo $result; die;
	}  add_action('wp_ajax_search_ajax', 'search_ajax_function');
  add_action('wp_ajax_nopriv_search_ajax', 'search_ajax_function');

  function search_ajax_function(){	$current_user = wp_get_current_user();

  	$value_dropdown = $_POST['dropdownvalue'];
    $value_search   = $_POST['searchterm'];
	$salesname   = $_POST['salesname'];	$ownername   = $_POST['ownername'];
	$csrname   = $_POST['csrname'];	if($_POST['salesname']=="all")
		{
			$salesname="";
		}
		if($_POST['ownername']=="all")
		{
			$ownername="";
		}	if($_POST['csrname']=="all")
		{
			$csrname="";
		}		if(!empty($salesname))
			{
				$andsale=" and salesman_name='$salesname'";			}
			if(!empty($ownername))
			{
				$andowner=" and owner_name='$ownername'";			}		if(!empty($csrname))
			{
				$andcsr=" and csr='$csrname'";			}			$accept=$_POST['accept'];
	if($accept=="all")
		{
			$accept="";
		}

		else

		{

			$acceptand=" and accepted='$accept'";
		}
    global $wpdb;
    $user_id = get_current_user_id();
	$current_user = wp_get_current_user();	$tablesagjob=$wpdb->prefix."sag_jobs";

    if ($value_search == '')
	{
				//not super admin and not a csr
				if ( !is_super_admin() &&  $user_id != 17 && $user_id != 16  &&  $user_id !=  13 && $user_id != 15  )
				{
							if($value_dropdown == "all")
							{

									$query= "SELECT * FROM $tablesagjob WHERE salesman_id = ". $user_id ." AND project_status NOT IN ('na')".$acceptand;
							}
							else
							 {
									$query= "SELECT * FROM $tablesagjob WHERE salesman_id = ". $user_id . " AND project_status = '$value_dropdown' ".$acceptand;
							}
				}
				else
				{
							if($value_dropdown == "all")
							{

								error_log("we are inside our use case", 0 );
							//$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE salesman_id = ". $user_id ." AND project_status = '$value_dropdown' ORDER BY created_date DESC";
								$query= "SELECT * FROM $tablesagjob WHERE project_status NOT IN ('na')".$acceptand;
							}
							else
							{
								$query= "SELECT * FROM $tablesagjob WHERE project_status = '$value_dropdown' ".$acceptand;
							}
				}

    }
	else
	{//NOT BLANK STRING ENTERED
      //not super admin and not a csr
				if ( !is_super_admin() &&  $user_id != 17 && $user_id != 16  &&  $user_id !=  13 && $user_id != 15  )
				{
								if($value_dropdown == "all")
								{

									$query= "SELECT * FROM $tablesagjob WHERE quote_number LIKE '%".$value_search ."%' AND salesman_id = ". $user_id  ." AND project_status NOT IN ('na')".$acceptand;

								}
								else
								 {
									$query= "SELECT * FROM $tablesagjob WHERE quote_number LIKE '%".$value_search ."%' AND salesman_id = ". $user_id . " AND project_status = '$value_dropdown' ".$acceptand;
								}
				}
				else
				{
						if($value_dropdown == "all")
						{
							error_log("we are inside our use case", 0 );
						//$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE salesman_id = ". $user_id ." AND project_status = '$value_dropdown' ORDER BY created_date DESC";

							$query= "SELECT * FROM $tablesagjob WHERE quote_number LIKE '%".$value_search ."%' "." AND project_status NOT IN ('na')".$acceptand;
						}
						else
						{
							$query= "SELECT * FROM $tablesagjob WHERE project_status = '$value_dropdown' AND quote_number LIKE '%" . $value_search . "%' ".$acceptand;
						}
				}
    }   /* //not super admin and not a csr
  	if ( !is_super_admin() &&  $user_id != 17 && $user_id != 16  &&  $user_id !=  13 && $user_id != 15  ){
  			if($value_dropdown == "all"){
  			$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE quote_number LIKE '%".$value_search ."%' AND salesman_id = ". $user_id . " AND project_status = '$value_dropdown' ";

  			}else  {
  			$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE quote_number LIKE '%".$value_search ."%' AND salesman_id = ". $user_id . " AND project_status = '$value_dropdown' ";
  			}
  		}
  	else{
  			if($value_dropdown == "all"){
          error_log("we are inside our use case", 0 );
          //$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE salesman_id = ". $user_id ." AND project_status = '$value_dropdown' ORDER BY created_date DESC";
  			$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE quote_number LIKE '%".$value_search ."%' ";

  			}else  {
  			$query= "SELECT * FROM `wp_4twsj5a02k_sag_jobs` WHERE project_status = '$value_dropdown' AND quote_number LIKE '%" . $value_search . "%' ";
  			}
  		}*/
			if(isset($_REQUEST['pageno']))
			{
				$start=($_REQUEST['pageno']-1)*50;

			}
			else
			{
				$start=0*50;

			}

			 $totalset=$query.$andsale.$andcsr.$andowner." ORDER BY created_date DESC";
		//	echo $totalset="SELECT count(*) as count FROM wp_4twsj5a02k_sag_jobs where 1=1 ORDER BY created_date DESC ";

			$totalset = str_replace("*",'count(*) as counts',$totalset);
			$resulttotlal=$wpdb->get_results($totalset);
			$totalcount=$resulttotlal[0]->counts;			if($value_dropdown == 'submitted'){
				$orderby = 'modifydate';
			}else{
				$orderby = 'created_date';
			}
		   $query = $query.$andsale.$andcsr.$andowner." ORDER BY ".$orderby." DESC limit $start ,50";

  	$result= $wpdb->get_results($query);
  	foreach ( $result as $sag_job ) {
		
		  
			    
			   	$acceptedId=0;
			     if(($sag_job->salesman_name==  $current_user->user_login || $sag_job->owner_name==  $current_user->user_login) && $sag_job->accepted ==0)
				{
					
					$acceptedId = $sag_job->id;
				}
			
              
      echo '<tr>';
      echo  '<td><a target="_blank" class="btn btn-danger btn-sm" style="float:right;" href="'. esc_url(add_query_arg(array('job_id'=> $sag_job->id,'accepted'=>$acceptedId), get_permalink( get_page_by_title( 'Add Job' ) ))).'" role="button">'.$sag_job->quote_number.'</a>';
	    if($acceptedId>0)
				{
            echo   '<a class="btn btn-danger btn-sm" id="accepted_'.$sag_job->id.'" style="float:left;margin-top:2px;width:78px;" href="" role="button">Accept</a>';
			}
	  echo '</td>';
      echo   '<td><span class="dashboard-edit-job glyphicon glyphicon-pencil" id="editjobset_'.$sag_job->id.'"></span></td>';
      echo '<td><select style="max-width:100px;" id="ownernamejob_'. $sag_job->id. '"'. 'disabled="disabled">';
     
		  global $wpdb;
		   $myrows_owner = $wpdb->get_results( "SELECT owner_name FROM $tablesagjob GROUP BY owner_name" );

		   foreach($myrows_owner as $myrowss_owner){

		 if($myrowss_owner->owner_name !=''){
		  $email='';
		 if($myrowss_owner->owner_name=='SLA'){
		     $email= 'steven@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='CW'){
     		 $email= 'cwilliams@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='KL'){
     		 $email= 'klewis@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='GRB'){
     		 $email= 'gburkhart@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='EA'){
     		 $email= 'edwin@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='SD'){
     		 $email= 'steven@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='CAB'){
     		 $email= 'cbaumes@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='CAO'){
     		 $email= 'cosborne@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='MJA'){
     		 $email= 'malbert@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='SG'){
     		 $email= 'sgreenstreet@salbertglass.com';
		 }else if($myrowss_owner->owner_name=='DB'){
     		 $email= 'dblackwell@salbertglass.com';
		 }
		 else if(strtoupper($myrowss_owner->owner_name)=='RS'){
     		 $email= 'rscott@salbertglass.com';
		 }
		 else if($myrowss_owner->owner_name=='KG'){
     		 $email= 'kgrant@salbertglass.com';
		 }
		 else if($myrowss_owner->owner_name=='AJ'){
     		 $email= 'ajay@imajine.us';
		 } else if($myrowss_owner->owner_name=='ALEX'){
     		 $email= 'alex@imajine.us';
		 }
		 else{
		 $email= 'steven@salbertglass.com';
		 }
		 		if($sag_job->owner_name==$myrowss_owner->owner_name)
				{
					$selectset="selected=selected";
				}
				else
				{
					$selectset=" ";
				}

		  echo '<option value="' . $myrowss_owner->owner_name . '"' .  $selectset. '>' . $myrowss_owner->owner_name .'</option>';

		  }}

		  echo '</select></br></br><select style="max-width:100px;" id="salesnamejob_'. $sag_job->id. '"'. 'disabled="disabled">';
		  global $wpdb;
		   $myrows = $wpdb->get_results( "SELECT salesman_name,contact_email FROM $tablesagjob GROUP BY salesman_name" );

		   foreach($myrows as $myrowss){

		 if($myrowss->salesman_name !=''){
		  $email='';
		 if($myrowss->salesman_name=='SLA'){
		     $email= 'steven@salbertglass.com';
		 }else if($myrowss->salesman_name=='CW'){
     		 $email= 'cwilliams@salbertglass.com';
		 }
		  else if(strtoupper($myrowss->salesman_name)=='RS'){
     		 $email= 'rscott@salbertglass.com';
		 }
		 else if($myrowss->salesman_name=='KL'){
     		 $email= 'klewis@salbertglass.com';
		 }else if($myrowss->salesman_name=='GRB'){
     		 $email= 'gburkhart@salbertglass.com';
		 }else if($myrowss->salesman_name=='EA'){
     		 $email= 'edwin@salbertglass.com';
		 }else if($myrowss->salesman_name=='SD'){
     		 $email= 'steven@salbertglass.com';
		 }else if($myrowss->salesman_name=='CAB'){
     		 $email= 'cbaumes@salbertglass.com';
		 }else if($myrowss->salesman_name=='CAO'){
     		 $email= 'cosborne@salbertglass.com';
		 }else if($myrowss->salesman_name=='MJA'){
     		 $email= 'malbert@salbertglass.com';
		 }else if($myrowss->salesman_name=='SG'){
     		 $email= 'sgreenstreet@salbertglass.com';
		 }else if($myrowss->salesman_name=='DB'){
     		 $email= 'dblackwell@salbertglass.com';
		 }else if($myrowss->salesman_name=='KG'){
     		 $email= 'kgrant@salbertglass.com';
		 }
		  else if($myrowss->salesman_name=='AJ'){
     		 $email= 'ajay@imajine.us';
		 } else if($myrowss->salesman_name=='ALEX'){
     		 $email= 'alex@imajine.us';
		 }
		 else{
		 $email= 'steven@salbertglass.com';
		 }
		 		if($sag_job->salesman_name==$myrowss->salesman_name)
				{
					$selectset="selected=selected";
				}
				else
				{
					$selectset=" ";
				}

		  echo '<option value="' . $myrowss->salesman_name . '"' .  $selectset. '>' . $myrowss->salesman_name .'</option>';

		  }}

		  echo '</select></td>' ;
		  
      echo '<td>';
		  echo '<select style="max-width:100px;" id="csrjob_'. $sag_job->id.'"'. 'disabled="disabled" >';
				global $wpdb;
			       $myrowsd = $wpdb->get_results( "SELECT csr FROM $tablesagjob GROUP BY csr" );
			       foreach($myrowsd as $myrowsss){

			     if($myrowsss->csr !=''){
			      	$emailcsr='';
					 if($myrowsss->csr=='cosborne'){
						$emailcsr= 'cosborne@salbertglass.com';
					 }else if(strtolower($myrowsss->csr)=='rs'){
						$emailcsr= 'rscott@salbertglass.com';
					 }else if($myrowsss->csr=='dblackwell'){
						$emailcsr= 'dblackwell@salbertglass.com';
					 }else if($myrowsss->csr=='kgrant'){
						$emailcsr= 'kgrant@salbertglass.com';
					 }else if($myrowsss->csr=='edwin'){
						 $emailcsr= 'edwin@salbertglass.com';
					 }else if($myrowsss->csr=='gburkhart'){
						 $emailcsr= 'gburkhart@salbertglass.com';
					 }else if($myrowsss->csr=='sgreenstreet'){
						 $emailcsr='sgreenstreet@salbertglass.com';
					 }else if((strtolower($myrowsss->csr)=='alex') || (strtolower($myrowsss->csr)=='alex@imajine.us')){
						$emailcsr ='alex@imajine.us';
					}else if((strtolower($myrowsss->csr)=='ajay') || (strtolower($myrowsss->csr)=='ajay@imajine.us')){
						$emailcsr ='ajay@imajine.us';
					}else if($myrowsss->csr=='cwilliams'){
						 $emailcsr= 'cwilliams@salbertglass.com';
					}else if($myrowsss->csr=='steven'){
						 $emailcsr ='steven@salbertglass.com';
					}else{
						$emailcsr =  'steven@salbertglass.com';
					 }
				 	if($myrowsss->csr==$sag_job->csr)
					{
						$csrselectset="selected=selected";
					}
					else
					{
						$csrselectset=" ";
					}

			      echo '<option value="'.$myrowsss->csr. '#'. $emailcsr . '"' . $csrselectset. '>'.$myrowsss->csr.'</option>';

		    }}

		        echo '</select></td>';
				
            echo  '<td style="max-width:150px;">'.$sag_job->project_address_line1.'<br>'.$sag_job->project_state.'</td>';
			echo  '<td style="max-width:150px;">'.$sag_job->job_name.'</td>';
            echo  '<td >'.$sag_job->project_status.'</td>';
            echo   '<td>'.date("m/d/Y g:i A", strtotime($sag_job->created_date) - 60 * 60 * 4).'</td>';
            echo   '<td>'.date("m/d/Y g:i A", strtotime($sag_job->modifydate) - 60 * 60 * 4).'</td>';			$current_user = wp_get_current_user();

            echo   '<td>';
			echo '<a class="btn btn-danger btn-sm" style="float:right;margin-bottom:2px;" href="'.esc_url(add_query_arg('job_id', $sag_job->id, get_permalink( get_page_by_title( 'Print PDF' ) ))).'" role="button">Pdf</a>';
						echo '</td>
            </tr>';
            }
		?>

<!--	<a href="#">&laquo;</a>
-->
<?php
	$pages= ceil($totalcount/50);
	//echo "slkfjskldfj".$totalcount;
if($totalcount>50)
{
?>
<tr>
  <td colspan="9" style="background:#fff"><div class="pagination pull-left" style="margin: 0">
      <?php
	for($i=1;$i<=$pages;$i++)
	{
			if($_REQUEST['pageno']==$i)
			{
				$class="class='active'";
			}
			else
			{
				$class =" ";

			}

			if($i==1 && !$_REQUEST['pageno'])

			{
				$class="class='active'";
			}

			echo "<a href='' id='pageno_".$i."' ".$class.">".$i."</a>  ";

	}
	?>
    </div></td>
</tr>
<?php
}
?>

<!--	<a href="#">&raquo;</a>
-->
<?php
  	exit();
  }

add_action('wp_ajax_add_filter_ajax','add_filter_ajax_function' );
add_action('wp_ajax_nopriv_add_filter_ajax', 'add_filter_ajax_function');

function add_filter_ajax_function(){

$current_user = wp_get_current_user();

	$value_dropdown= $_POST['dropdownvalue'];

	$accept=$_POST['accept'];
	if($accept=="all")
		{
			$accept="";
		}

		else

		{

			$acceptand=" and accepted='$accept'";
		}

	$salesname=$_POST['salesname'];
	if($_POST['salesname']=="all")
		{
			$salesname="";
		}
		
	$ownername=$_POST['ownername'];
	if($_POST['ownername']=="all")
		{
			$ownername="";
		}	
	if(!empty($ownername))
		{
			$andowner=" and owner_name='$ownername'";

		}	$csrname=$_POST['csrname'];

	if($_POST['csrname']=="all")
		{
			$csrname="";
		}
$andss=" where 1=1";
		if(!empty($csrname))
			{
				$and=" and csr='$csrname'";
				$andss=" where  csr='$csrname'";

			}

		$searchterm=$_POST['searchterm'];
		if(!empty($searchterm))
		{
			$andtermse=" and quote_number LIKE '%".$searchterm ."%' ";

		}
	global $wpdb;
$tablesagjob=$wpdb->prefix."sag_jobs";

	//if ( !is_super_admin() ){
	if ( 0){
	$user_id = get_current_user_id();

			if($value_dropdown == "all"){
			$query= "SELECT * FROM $tablesagjob WHERE salesman_id = ". $user_id .$and.$acceptand;

			}else  {
			$query= "SELECT * FROM $tablesagjob WHERE salesman_id = ". $user_id ." AND project_status = '$value_dropdown'".$and.$acceptand;
			}
		}
	else{
	$user_id = get_current_user_id();
			if($value_dropdown == "all"){

				if(!empty($salesname))
				{					/*if(isset($_POST['fromyourquote']) && $_POST['fromyourquote'] == '1'){
						$query= "SELECT * FROM $tablesagjob where (salesman_name='$salesname' OR owner_name='$salesname') ".$and.$acceptand;
					}else{
						$query= "SELECT * FROM $tablesagjob where salesman_name='$salesname' ".$and.$acceptand;
						}*/
						$query= "SELECT * FROM $tablesagjob where salesman_name='$salesname' ".$and.$acceptand;
				}
				else
				{
					$query= "SELECT * FROM $tablesagjob WHERE project_status NOT IN ('na')".$and.$acceptand;

				}

			}else  {
					if(!empty($salesname))
						{

				/*if(isset($_POST['fromyourquote']) && $_POST['fromyourquote'] == '1'){
						$query= "SELECT * FROM $tablesagjob WHERE project_status = '$value_dropdown' and (salesman_name='$salesname' OR owner_name='$salesname') ".$and.$acceptand;
					}else{
						$query= "SELECT * FROM $tablesagjob WHERE project_status = '$value_dropdown' and salesman_name='$salesname' ".$and.$acceptand;
						}*/
						$query= "SELECT * FROM $tablesagjob WHERE project_status = '$value_dropdown' and salesman_name='$salesname' ".$and.$acceptand;						

						}
					else
						{

						$query= "SELECT * FROM $tablesagjob WHERE project_status = '$value_dropdown' ".$and.$acceptand;

						}			}
		}
//echo $query;
		if(isset($_REQUEST['pageno']))
			{
				$start=($_REQUEST['pageno']-1)*50;

			}
			else
			{
				$start=0*50;

			}			$totalset=$query.$andtermse.$andowner." ORDER BY created_date DESC";

			 $totalset=str_replace("*","count(*) as counts",$totalset);
			$resulttotlal=$wpdb->get_results($totalset);
			$totalcount=$resulttotlal[0]->counts;		if($value_dropdown == 'submitted'){
				$orderby = 'modifydate';
			}else{
				$orderby = 'created_date';
			}
			if(isset($_POST['orderby'])){
				$orderby = $_POST['orderby'];
			}
			if(isset($_POST['ascordesc'])){
				$ascordesc = $_POST['ascordesc'];
			}else{
				$ascordesc = "DESC";
			}

		 $finalquery= $query.$andtermse.$andowner." ORDER BY ".$orderby." ".$ascordesc."  LIMIT ".$start.",50 ";

	$result= $wpdb->get_results($finalquery);
	foreach ( $result as $sag_job ) {

   				$acceptedId=0;
			     if(($sag_job->salesman_name==  $current_user->user_login || $sag_job->owner_name==  $current_user->user_login) && $sag_job->accepted ==0)
				{
					
					$acceptedId = $sag_job->id;
				}
				
                echo '<tr>';
                echo  '<td><a target="_blank" class="btn btn-danger btn-sm" style="float:left;" href="'. esc_url(add_query_arg(array('job_id'=> $sag_job->id,'accepted'=>$acceptedId), get_permalink( get_page_by_title( 'Add Job' ) ))).'" role="button">'.$sag_job->quote_number.'</a>';
				
  if($acceptedId>0)
				{
								  echo   '<a class="btn btn-danger btn-sm" id="accepted_'.$sag_job->id.'" style="float:left;margin-top:2px;width:78px;" href="" role="button">Accept</a>';

				 }
				 echo '</td>';
                echo   '<td><span class="dashboard-edit-job glyphicon glyphicon-pencil" id="editjobset_'.$sag_job->id.'"></span></td>';
                echo '<td><select style="max-width:100px;" id="ownernamejob_'. $sag_job->id. '"'. 'disabled="disabled">';
                echo '<option value="NA">' . 'NA' .'</option>';
                
     //}
    global $wpdb;
     $myrows_owner = $wpdb->get_results( "SELECT owner_name FROM $tablesagjob GROUP BY owner_name" );
     foreach($myrows_owner as $myrowss_owner){

   if($myrowss_owner->owner_name !=''){
    $email='';
   if($myrowss_owner->owner_name=='SLA'){
       $email= 'steven@salbertglass.com';
   }else if($myrowss_owner->owner_name=='CW'){
       $email= 'cwilliams@salbertglass.com';
   }else if($myrowss_owner->owner_name=='KL'){
       $email= 'klewis@salbertglass.com';
   }else if($myrowss_owner->owner_name=='GRB'){
       $email= 'gburkhart@salbertglass.com';
   }else if($myrowss_owner->owner_name=='EA'){
       $email= 'edwin@salbertglass.com';
   }else if($myrowss_owner->owner_name=='SD'){
       $email= 'steven@salbertglass.com';
   }else if($myrowss_owner->owner_name=='CAB'){
       $email= 'cbaumes@salbertglass.com';
   }else if($myrowss_owner->owner_name=='CAO'){
       $email= 'cosborne@salbertglass.com';
   }else if(strtoupper($myrowss_owner->owner_name)=='RS'){
     		 $email= 'rscott@salbertglass.com';
	}else if($myrowss_owner->owner_name=='MJA'){
       $email= 'malbert@salbertglass.com';
   }else if($myrowss_owner->owner_name=='SG'){
       $email= 'sgreenstreet@salbertglass.com';
   }else if($myrowss_owner->owner_name=='DB'){
       $email= 'dblackwell@salbertglass.com';
   }else if($myrowss_owner->owner_name=='KG'){
       $email= 'kgrant@salbertglass.com';
   }else if($myrowss_owner->owner_name=='AJ'){
       $email= 'ajay@imajine.us';
   }else if($myrowss_owner->owner_name=='ALEX'){
       $email= 'alex@imajine.us';
   }else{
   $email= 'steven@salbertglass.com';
   }
   
  
      if($sag_job->owner_name==$myrowss_owner->owner_name)
      {
        $selectset="selected=selected";

      }
      else
      {
        $selectset=" ";
      }

    echo '<option value="' . $myrowss_owner->owner_name . '"' .  $selectset. '>' . $myrowss_owner->owner_name .'</option>';

    }}

    echo '</select></br></br><select style="max-width:100px;" id="salesnamejob_'. $sag_job->id. '"'. 'disabled="disabled">';
     //}
    global $wpdb;
     $myrows = $wpdb->get_results( "SELECT salesman_name,contact_email FROM $tablesagjob GROUP BY salesman_name" );
     foreach($myrows as $myrowss){

   if($myrowss->salesman_name !=''){
    $email='';
   if($myrowss->salesman_name=='SLA'){
       $email= 'steven@salbertglass.com';
   }else if($myrowss->salesman_name=='CW'){
       $email= 'cwilliams@salbertglass.com';
   }else if($myrowss->salesman_name=='KL'){
       $email= 'klewis@salbertglass.com';
   }else if($myrowss->salesman_name=='GRB'){
       $email= 'gburkhart@salbertglass.com';
   }else if($myrowss->salesman_name=='EA'){
       $email= 'edwin@salbertglass.com';
   }else if($myrowss->salesman_name=='SD'){
       $email= 'steven@salbertglass.com';
   }else if($myrowss->salesman_name=='CAB'){
       $email= 'cbaumes@salbertglass.com';
   }else if($myrowss->salesman_name=='CAO'){
       $email= 'cosborne@salbertglass.com';
   }
    else if(strtoupper($myrowss->salesman_name)=='RS'){
     		 $email= 'rscott@salbertglass.com';
		 }
   else if($myrowss->salesman_name=='MJA'){
       $email= 'malbert@salbertglass.com';
   }else if($myrowss->salesman_name=='SG'){
       $email= 'sgreenstreet@salbertglass.com';
   }else if($myrowss->salesman_name=='DB'){
       $email= 'dblackwell@salbertglass.com';
   }else if($myrowss->salesman_name=='KG'){
       $email= 'kgrant@salbertglass.com';
   }else if($myrowss->salesman_name=='AJ'){
       $email= 'ajay@imajine.us';
   }else if($myrowss->salesman_name=='ALEX'){
       $email= 'alex@imajine.us';
   }else{
   $email= 'steven@salbertglass.com';
   }
   
   
 
      if($sag_job->salesman_name==$myrowss->salesman_name)
      {
        $selectset="selected=selected";

      }
      else
      {
        $selectset=" ";
      }

    echo '<option value="' . $myrowss->salesman_name . '"' .  $selectset. '>' . $myrowss->salesman_name .'</option>';

    }}

    echo '</select></td>' ;
    echo '<td>';
    echo '<select style="max-width:100px;" id="csrjob_'. $sag_job->id.'"'. 'disabled="disabled" >';

      global $wpdb;
           $myrowsd = $wpdb->get_results( "SELECT csr FROM $tablesagjob GROUP BY csr" );
           foreach($myrowsd as $myrowsss){

         if($myrowsss->csr !=''){
	         $emailcsr='';
			 if($myrowsss->csr=='cosborne'){
				$emailcsr= 'cosborne@salbertglass.com';
			 }else if($myrowsss->csr=='dblackwell'){
				 $emailcsr= 'dblackwell@salbertglass.com';
			 }else if($myrowsss->csr=='kgrant'){
				$emailcsr= 'kgrant@salbertglass.com';
			 }else if($myrowsss->csr=='edwin'){
				$emailcsr= 'edwin@salbertglass.com';
			 }else if($myrowsss->csr=='gburkhart'){
			   $emailcsr= 'gburkhart@salbertglass.com';
			 }else if($myrowsss->csr=='sgreenstreet'){
			   $emailcsr='sgreenstreet@salbertglass.com';
			 }else if($myrowsss->csr=='steven'){
				$emailcsr ='steven@salbertglass.com';
			 }else if((strtolower($myrowsss->csr)=='alex') || (strtolower($myrowsss->csr)=='alex@imajine.us')){
				$emailcsr ='alex@imajine.us';
			}else if((strtolower($myrowsss->csr)=='ajay') || (strtolower($myrowsss->csr)=='ajay@imajine.us')){
				$emailcsr ='ajay@imajine.us';
			}else if($myrowsss->csr=='cwilliams'){
				 $emailcsr= 'cwilliams@salbertglass.com';
			}
			 else{
				$emailcsr =  'steven@salbertglass.com';
			 }

        if($myrowsss->csr==$sag_job->csr)
        {
          $csrselectset="selected=selected";
        }
        else
        {
          $csrselectset=" ";
        }

          echo '<option value="'.$myrowsss->csr. '#'. $emailcsr . '"' . $csrselectset. '>'.$myrowsss->csr.'</option>';

      }}

          echo '</select></td>';
                echo  '<td style="max-width:150px;">'.$sag_job->project_address_line1.'<br>'.$sag_job->project_state.'</td>';
				echo  '<td style="max-width:150px;">'.$sag_job->job_name.'</td>';
                echo  '<td >'.$sag_job->project_status.'</td>';
                echo   '<td>'.date("m/d/Y g:i A", strtotime($sag_job->created_date) - 60 * 60 * 5).'</td>';

                echo   '<td>'.date("m/d/Y g:i A", strtotime($sag_job->modifydate) - 60 * 60 * 5).'</td>';				$current_user = wp_get_current_user();

                echo   '<td>';
				echo '<a class="btn btn-danger btn-sm" target="_blank" href="'.esc_url(add_query_arg('job_id', $sag_job->id, get_permalink( get_page_by_title( 'Print PDF' ) ))).'" role="button">Pdf</a>';
				                echo '</td></tr>';

                }
	$pages= ceil($totalcount/50);
	//echo "slkfjskldfj".$totalcount;
if($totalcount>50)
{
?>
<tr>
  <td colspan="9" style="background:#fff"><div class="pagination pull-left" style="margin: 0">
      <?php
	for($i=1;$i<=$pages;$i++)
	{
			if($_REQUEST['pageno']==$i)
			{
				$class="class='active'";
			}
			else
			{
				$class =" ";

			}			if($i==1 && !$_REQUEST['pageno'])

			{
				$class="class='active'";
			}

			echo "<a href='' id='pageno_".$i."' ".$class.">".$i."</a>  ";
	}
	?>
    </div></td>
</tr>
<?php
}
?>
<?php
	exit();
}
