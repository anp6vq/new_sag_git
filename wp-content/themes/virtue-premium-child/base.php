<?php get_template_part('templates/head'); ?>
<?php global $virtue_premium; 
  if(isset($virtue_premium["smooth_scrolling"]) && $virtue_premium["smooth_scrolling"] == '1') {$scrolling = '1';} else {$scrolling = '0';}
  if(isset($virtue_premium["smooth_scrolling_hide"]) && $virtue_premium["smooth_scrolling_hide"] == '1') {$scrolling_hide = '1';} else {$scrolling_hide = '0';} 
  if(isset($virtue_premium['virtue_animate_in']) && $virtue_premium['virtue_animate_in'] == '1') {$animate = '1';} else {$animate = '0';}
  if(isset($virtue_premium['sticky_header']) && $virtue_premium['sticky_header'] == '1') {$sticky = '1';} else {$sticky = '0';}
  if(isset($virtue_premium['product_tabs_scroll']) && $virtue_premium['product_tabs_scroll'] == '1') {$pscroll = '1';} else {$pscroll = '0';}
  if(isset($virtue_premium['header_style'])) {$header_style = $virtue_premium['header_style'];} else {$header_style = 'standard';}
  if(isset($virtue_premium['select2_select'])) {$select2_select = $virtue_premium['select2_select'];} else {$select2_select = '1';}
  ?>
<body <?php body_class(); ?> data-smooth-scrolling="<?php echo $scrolling;?>" data-smooth-scrolling-hide="<?php echo $scrolling_hide;?>" data-jsselect="<?php echo $select2_select;?>" data-product-tab-scroll="<?php echo $pscroll; ?>" data-animate="<?php echo $animate;?>" data-sticky="<?php echo $sticky;?>">
<div id="wrapper" class="container">
  <!--[if lt IE 8]><div class="alert"> <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'virtue'); ?></div><![endif]-->

  <?php
    do_action('get_header');
    if($header_style == 'center') {
      get_template_part('templates/header-style-two');
    } else if ($header_style == 'shrink') {
      get_template_part('templates/header-style-three');
    } else {
      get_template_part('templates/header');
    }
  ?>

  <div class="wrap contentclass" role="document">

        <?php include kadence_template_path(); ?>
        
      <?php if (kadence_display_sidebar()) : ?>
      <aside id="ktsidebar" class="<?php echo kadence_sidebar_class(); ?> kad-sidebar" role="complementary">
        <div class="sidebar">
          <?php include kadence_sidebar_path(); ?>
        </div><!-- /.sidebar -->
      </aside><!-- /aside -->
      <?php endif; ?>
      </div><!-- /.row-->
    </div><!-- /.content -->
  </div><!-- /.wrap -->

  <?php do_action('get_footer');
  get_template_part('templates/footer'); ?>
</div><!--Wrapper-->
<script type="text/javascript" src="//cdn.callrail.com/companies/913362921/fa2908f37ad0cef6388e/12/swap.js"></script>
</body>
<!--<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-102524462-1', 'auto');
  ga('send', 'pageview');

</script>
<script>
var x = 0;
var myVar = setInterval(function(){
  if(x == 0){
    if(jQuery('.wpcf7-mail-sent-ok').length > 0)
    {
    var label = window.location.pathname.split('/').slice(-2)[0];
    ga('send','event','form','submit',label);
    clearInterval(myVar);
    x = 1;
    }
  }
}, 1000);
</script>-->
<!-- Google Code for Remarketing Tag -->
<!--------------------------------------------------
Remarketing tags may not be associated with personally identifiable information or placed on pages related to sensitive categories. See more information and instructions on how to setup the tag on: http://google.com/ads/remarketingsetup
--------------------------------------------------->
<!--<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 845963437;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>-->
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/845963437/?guid=ON&amp;script=0"/>
</div>
</noscript>

</html>
