<?php
/*
Template Name: Add Job
*/
?>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js">		</script>
<script src="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/jquery-ui.js" type="text/javascript"></script>
<script>

$(document).ready(function() {
	    $("#loding").show();
	    var urlcheck =  $(location).attr('href');

        var pathname = window.location.pathname;
    <?php if(!is_user_logged_in()){ ?>
		   if(pathname =='/salbertglassjobs/add-job'){
	            window.location.replace("<?php echo get_home_url(); ?>/#wpum-login-form-3");
			  }else{
				  $("#loding").hide();
				  }

	<?php } ?>


});
</script>
<?php if(is_user_logged_in()){  ?>
<input type="hidden" id="letajax"   value="" />
<input type="hidden" id="logajax"   value="" />
<input type="hidden" id="ajaxurlset"   value="<?php echo admin_url('admin-ajax.php')?>" />

<link href="https://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css"
		rel="stylesheet" type="text/css" />

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">All CheckIns List</h4>
        </div>
        <div class="modal-body">
          <div id="dvMap1" style="height: 380px; width: 550px;">
           <div id="ResultShow">
           </div>
          </div>
        </div>
      </div>

    </div>
  </div>

</div>


  <div class="container-fluid">
	  <div id="form-header" class="row">
	    <div id="logo-wrapper" class="col-xs-8 col-md-4">
	      <img id="logo" src="<?php echo get_stylesheet_directory_uri(); ?>/img/albertGlass.png">
	      <h4 class="header-with-image">Job Information</h4>
	    </div>
			<div class="col-xs-4 col-md-8">
				<a  class="btn btn-danger right" href="<?php echo get_permalink( get_page_by_title( 'Current Jobs' ) ) ?>">All Jobs</a>
			</div>
		</div>
	<?php if(isset($_GET['job_id'])) : ?>
	<div class="row">
		<div class="col-xs-4 col-sm-2 col-sm-offset-6 col-md-2 col-md-offset-6">
			<a id="checkin" class="btn btn-info right"><i class="glyphicon glyphicon-map-marker btn-icon"></i>Check-In</a>
		</div>
		<div class="col-xs-4 col-sm-2 col-sm-offset-0 col-md-2 col-md-offset-0">
			<!--<a id="checkins-list" data-toggle="modal" data-target="#myModal" class="btn btn-info right"><i class="glyphicon glyphicon-list-alt btn-icon" aria-hidden="true"></i>Check Ins</a>-->
			<a id="checkins-list" class="btn btn-default right" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-list-alt btn-icon" aria-hidden="true"></span>Check Ins</a>
		</div>
		<div class="col-xs-4 col-sm-2 col-sm-offset-0 col-md-2 col-md-offset-0">
			<a id="" class="btn btn-success right" href="<?php echo esc_url(add_query_arg('job_id', $_GET['job_id'], get_permalink( get_page_by_title( 'Print PDF' ) ))); ?>">Print New PDF</a>
		</div>
  </div>
	<?php endif; ?>
  <div class="row">
    <div id="form-title" class="col-md-12 col-sm-12 ">
      <?php if ( have_posts() ) : ?>
          <?php while ( have_posts() ) : the_post(); ?>
              <?php the_content(); ?>
          <?php endwhile; ?>
      <?php endif; ?>
    </div>
  </div>
<hr>
<?php
include 'jobs-footer.php';
}else{ ?>
	<div id="loading"></div>
<?php	}

?>
</div> <!--end container-fluid -->


