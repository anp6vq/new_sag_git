<?php
if(has_term('blogs', 'category', $post)) {      
    get_template_part('templates/content', 'blogs');
} else {
    get_template_part('templates/content', 'single');
}
?>